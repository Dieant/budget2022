<?php

/**
 * entri actions.
 *
 * @package    budgeting
 * @subpackage entri
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class entriActions extends autoentriActions {

    protected function kirimServiceControlling($kirimControlling) {
        $this->setLayout(false);

        $url = "http://eproject.surabaya.go.id/index.php/detail_kegiatan/terimaKomponenHpsp.shtml?dataku=";
        $url .= json_encode($kirimControlling);
        $url = str_replace(" ", '%20', $url);
        
        set_time_limit(0);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $output = curl_exec($ch);

        if (curl_errno($ch) === 0) {
            return $output . "<br/>";
        } else {
            return curl_error($ch);
        }
        curl_close($ch);
    }

    public function executeGantiRekeningRevisi() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $detail_no = $this->getRequestParameter('detail_no');
        $rekening_code = $this->getRequestParameter('rek_' . $detail_no);

        $totNilaiSwakelola = 0;
        $totNilaiKontrak = 0;
        $totNilaiAlokasi = 0;

//        if (sfConfig::get('app_tahap_edit') <> 'murni') {
//            $rd = new DinasRincianDetail();
//            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
//            $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
//            $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
//        }
        //if ($rekening_code && $totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0) {
        $con = Propel::getConnection();

        $con->begin();
        try {
            $queryUpdate = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
            . "set rekening_code = '$rekening_code', ob = true  "
            . "where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and detail_no = '$detail_no'";
            $stmt = $con->prepareStatement($queryUpdate);
            $stmt->executeQuery();

            $con->commit();
        } catch (Exception $e) {
            echo $e;
            $con->rollback();
        }
        //} else {
        //    echo 'Gagal, karena memiliki nilai alokasi atau kontrak';
        //}
    }

    public function executeTerimaArahan() {
        $this->setLayout(false);

        $data_arahan = $this->getRequestParameter('arahan');
        $arahans = json_decode($data_arahan, TRUE);
        $ada_error = FALSE;

        $con = Propel::getConnection();
        $con->begin();
        foreach ($arahans as $id_arahan => $arahan) {
            $kode_kegiatan = $arahan['kode_kegiatan'];
            $kode_belanja = $arahan['kode_belanja'];
            $unit_id = $arahan['unit_id'];
            $uraian = $arahan['uraian'];
            $anggaran = $arahan['anggaran'];

            try {
                $query_insert_arahan = 
                "INSERT INTO ".sfConfig::get('app_default_schema').".arahan_belanja_kegiatan
                (id, unit_id, kegiatan_code, kode_belanja, uraian, anggaran, status, created_at)
                VALUES (".$id_arahan.", '".$unit_id."', '".$kode_kegiatan."', '".$kode_belanja."', '".$uraian."', ".$anggaran.", 0, '".date('Y-m-d H:i:s')."')";

                $stmt = $con->prepareStatement($query_insert_arahan);
                $stmt->executeQuery();
            } catch(Exception $e) {
                $ada_error = TRUE;
                break;
            }
        }

        if($ada_error) {
            $con->rollback();
            return $this->renderText('Gagal');
        } else {
            $con->commit();
            return $this->renderText('OK');
        }
    }

    public function executeHapusRequest() {
        $id = $this->getRequestParameter('id');
        $query = "update " . sfConfig::get('app_default_schema') . ".log_request_dinas set status=4, updated_at='" . date('Y-m-d H:i:s') . "' where id='$id'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        $this->setFlash('berhasil', 'Request Telah Dihapus');
        return $this->redirect('entri/requestDinasList');
    }

    public function executeRequestDinasList() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $pagers = new sfPropelPager('LogRequestDinas', 20);
        $b = new Criteria;
        $b->add(UserHandleV2Peer::USER_ID, $this->getUser()->getNamaLogin());
        $es = UserHandleV2Peer::doSelectOne($b);
        // $satuan_kerja = $es;
        // $unit_kerja = Array();
        // foreach ($satuan_kerja as $x) {
        //     $unit_kerja[] = $x->getUnitId();
        // }
        // cek untuk yang sudah di hapus
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->add(LogRequestDinasPeer::STATUS, 4, Criteria::NOT_EQUAL);
        $c->add(LogRequestDinasPeer::UNIT_ID, $es->getUnitId());
        // for ($i = 1; $i < count($unit_kerja); $i++) {
        //     $c->addOr(LogRequestDinasPeer::UNIT_ID, $unit_kerja[$i]);
        // }
        //$c->addDescendingOrderByColumn(LogRequestDinasPeer::STATUS);
        $c->addDescendingOrderByColumn(LogRequestDinasPeer::UPDATED_AT);
        //$c->addAscendingOrderByColumn(LogRequestDinasPeer::UNIT_ID);
        $this->addFiltersCriteriaRequestDinasList($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executePilihKegiatan() {
        $unit_id = $this->getRequestParameter('id');
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::USER_ID, $this->getUser()->getNamaLogin());
        $c->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
        $this->master_kegiatan = DinasMasterKegiatanPeer::doSelect($c);
    }

    public function executeBuatRequest() {
        $c = new Criteria();
        $c->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->unit_kerja = UnitKerjaPeer::doSelect($c);
    }

    public function executeBuatRequestSave() {
        if (!$this->getRequestParameter('user_id')) {
            $this->setFlash('gagal', 'User Id salah');
            return $this->redirect('entri/buatRequest');
        }
        if (!$this->getRequestParameter('catatan') && strlen($this->getRequestParameter('catatan')) < 15) {
            $this->setFlash('gagal', 'Alasan minimal 15 karakter');
            return $this->redirect('entri/buatRequest');
        }

        if (!($this->getRequestParameter('unit_id') && $this->getRequestParameter('kode_kegiatan'))) {
            $this->setFlash('gagal', 'Pilih SKPD dan Kegiatan terlebih dahulu');
            return $this->redirect('entri/buatRequest');
        }

        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = implode('|', $this->getRequestParameter('kode_kegiatan'));
        $catatan = $this->getRequestParameter('catatan');
        $tipe = $this->getRequestParameter('tipe');
        $user_id = $this->getRequestParameter('user_id');

        $khusus = false;
        if ($this->getRequest()->getFileName('file')) {
            $array_file_pdf = explode('.', $this->getRequest()->getFileName('file'));
            if (!in_array($array_file_pdf[count($array_file_pdf) - 1], array('zip', 'rar', 'jpg', 'pdf', 'png', 'xls', 'xlsx'))) {
                $this->setFlash('gagal', 'Gunakan file dengan format zip/rar/jpg/png/pdf');
                return $this->redirect('entri/buatRequest');
            }

            $fileName_compress = 'file_' . $unit_id . '_' . $penyelia . '_' . date('Y-m-d_H-i') . '.' . $array_file_pdf[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/log/' . $fileName_compress);

            $file_path_pdf = sfConfig::get('sf_root_dir') . '/web/uploads/log/' . $fileName_compress;

            //$saving_filepath_pdf = sfConfig::get('app_path_default_sf') . 'uploads/log/' . $fileName_compress;

            if (!file_exists($file_path_pdf)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/log/' . $fileName_compress);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_compress);
                return $this->redirect('entri/buatRequest');
            }
        } else {
            $khusus = true;
        }

        //insert ke tabel
        $id = 1;
        $c = new Criteria();
        $c->setLimit(1);
        $c->addDescendingOrderByColumn(LogRequestDinasPeer::ID);
        if ($rs_id = LogRequestDinasPeer::doSelectOne($c)) {
            $id = $rs_id->getId() + 1;
        }

        $new_upload = new LogRequestDinas();
        $new_upload->setId($id);
        $new_upload->setUnitId($unit_id);
        $new_upload->setKegiatanCode($kode_kegiatan);
        $new_upload->setUserId($user_id);
        $new_upload->setTipe($tipe);
        $new_upload->setStatus(0);
        $new_upload->setCatatan($catatan);
        $new_upload->setIsKhusus($khusus);
        $new_upload->setPath($fileName_compress);
        $new_upload->setCreatedAt(date('Y-m-d H:i:s'));
        $new_upload->setUpdatedAt(date('Y-m-d H:i:s'));
        $new_upload->save();

        $this->setFlash('berhasil', 'Telah berhasil upload ' . $fileName_compress);
        return $this->redirect('entri/buatRequest');
    }

    public function executeBatalRevisiKomponen() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $detail_no = $this->getRequestParameter('detail_no');

        $con = Propel::getConnection();
        $con->begin();

        $c = new Criteria();
        
        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
        
        // ketika tahap pak maka cek tabel pak_bukuputih_rincian_detail
        if($tahap == 10 && sfConfig::get('app_tahap_detail') != 'pak_bukuputih') {
            $c->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(PakBukuPutihRincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_drd = PakBukuPutihRincianDetailPeer::doSelectOne($c);
        }
        else{
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_drd = RincianDetailPeer::doSelectOne($c);
        }

        if ($rs_drd) {
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            if ($rs_rd = DinasRincianDetailPeer::doSelectOne($c)) {

                //cek untuk mengisi status_komponen_baru
                $status_komponen_berubah = FALSE;
                $status_komponen_baru = FALSE;
                $tahap_cek = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                $c_pembanding_kegiatan = new Criteria();
                $c_pembanding_kegiatan->add(PembandingKegiatanPeer::UNIT_ID, $unit_id);
                $c_pembanding_kegiatan->add(PembandingKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_pembanding_kegiatan->add(PembandingKegiatanPeer::TAHAP, $tahap_cek);
                $c_pembanding_kegiatan->addDescendingOrderByColumn(PembandingKegiatanPeer::ID);
                if ($rs_pembanding_kegiatan = PembandingKegiatanPeer::doSelectOne($c_pembanding_kegiatan)) {
                    $id_pembanding = $rs_pembanding_kegiatan->getId();
                    $c_pembanding_komponen = new Criteria();
                    $c_pembanding_komponen->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $id_pembanding);
                    $c_pembanding_komponen->add(PembandingKomponenPeer::DETAIL_NO, $detail_no);
                    if ($rs_pembanding_komponen = PembandingKomponenPeer::doSelectOne($c_pembanding_komponen)) {
                        $pembanding_rekening_code = $rs_pembanding_komponen->getRekeningCode();
                        $pembanding_komponen_name = $rs_pembanding_komponen->getKomponenName();
                        $pembanding_satuan = $rs_pembanding_komponen->getSatuan();
                        $pembanding_subtitle = $rs_pembanding_komponen->getSubtitle();
                        $pembanding_detail_name = $rs_pembanding_komponen->getDetailName();
                        $pembanding_volume = $rs_pembanding_komponen->getVolume();
                        $pembanding_keterangan_koefisien = $rs_pembanding_komponen->getKeteranganKoefisien();

                        if ($rs_drd->getSubtitle() <> $pembanding_subtitle || $rs_drd->getDetailName() <> $pembanding_detail_name ||
                            $rs_drd->getVolume() <> $pembanding_volume || $rs_drd->getKeteranganKoefisien() <> $pembanding_keterangan_koefisien ||
                            $rs_drd->getRekeningCode() <> $pembanding_rekening_code || $rs_drd->getKomponenName() <> $pembanding_komponen_name ||
                            $rs_drd->getSatuan() <> $pembanding_satuan) {
                            $level_tolak = $rs_rd->getStatusLevelTolak();
                        if ($level_tolak >= 4) {
                            $status_komponen_berubah = TRUE;
                        } else {
                            $status_komponen_baru = TRUE;
                        }
                    }
                } else {
                    $status_komponen_baru = TRUE;
                }
            }
                //cek untuk mengisi status_komponen_baru

            $rd_function = new DinasRincianDetail();
                //cek pagu
            $status_pagu_rincian = 0;
            $array_buka_pagu_dinas_khusus = array('');
            $array_buka_pagu_kegiatan_khusus = array('');
            if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforKembalikan($unit_id, $kode_kegiatan, $detail_no);
            } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforKembalikan($unit_id, $kode_kegiatan, $detail_no);
            } else {
                if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                    if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                        $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforKembalikan($unit_id, $kode_kegiatan, $detail_no);
                    } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                        $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforKembalikan($unit_id, $kode_kegiatan, $detail_no);
                    }
                } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                    $status_pagu_rincian = 0;
                }
            }
            if ($status_pagu_rincian == '1') {
                $con->rollback();
                $this->setFlash('gagal', 'Komponen tidak berhasil dikembalikan karena nilai total RKA Melebihi total Pagu.');
                return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
                //cek realisasi dkk
            $volume = $rs_drd->getVolume();
            $pajak = $rs_drd->getPajak();
            $harga = $rs_drd->getKomponenHargaAwal();

                //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
            $nilaiBaru = $rs_drd->getNilaiAnggaran();
            if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $lelang = $rd_function->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_drd->getNilaiAnggaran());
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $totNilaiSwakelola = $rd_function->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiKontrak = $rd_function->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiRealisasi = $rd_function->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                        $totVolumeRealisasi = $rd_function->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);

                            //jika dia is_potong_bpjs maka tambahi cekvolumespk

                        $totNilaiHps = $rd_function->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                        $ceklelangselesaitidakaturanpembayaran = $rd_function->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                        $totNilaiKontrakTidakAdaAturanPembayaran = $rd_function->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                    }
                }
            }
            if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
                if ($totNilaiKontrak == 0) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                } else if ($totNilaiSwakelola == 0) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                } else {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                }
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($nilaiBaru < $totNilaiHps) {
                $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($lelang > 0) {
                $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($nilaiBaru < $totNilaiRealisasi) {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else if ($volume < $totVolumeRealisasi && $rs_rd->getStatusLelang() != 'lock') {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini melebihi volume realisasi di edelivery, sejumlah : ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                $con->rollback();
                    //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else {
                $rs_rd->setTipe($rs_drd->getTipe());
                $rs_rd->setRekeningCode($rs_drd->getRekeningCode());
                $rs_rd->setKomponenId($rs_drd->getKomponenId());
                $rs_rd->setDetailName($rs_drd->getDetailName());
                $rs_rd->setVolume($rs_drd->getVolume());
                $rs_rd->setKeteranganKoefisien($rs_drd->getKeteranganKoefisien());
                $rs_rd->setSubtitle($rs_drd->getSubtitle());
                $rs_rd->setKomponenHarga($rs_drd->getKomponenHarga());
                $rs_rd->setKomponenHargaAwal($rs_drd->getKomponenHargaAwal());
                $rs_rd->setKomponenName($rs_drd->getKomponenName());
                $rs_rd->setSatuan($rs_drd->getSatuan());
                $rs_rd->setPajak($rs_drd->getPajak());
                $rs_rd->setFromSubKegiatan($rs_drd->getFromSubKegiatan());
                $rs_rd->setSub($rs_drd->getSub());
                $rs_rd->setKodeSub($rs_drd->getKodeSub());
                $rs_rd->setLastUpdateUser($rs_drd->getLastUpdateUser());
                $rs_rd->setLastUpdateTime($rs_drd->getLastUpdateTime());
                $rs_rd->setLastUpdateIp($rs_drd->getLastUpdateIp());
                $rs_rd->setTahap($rs_drd->getTahap());
                $rs_rd->setTahapEdit($rs_drd->getTahapEdit());
                $rs_rd->setTahapNew($rs_drd->getTahapNew());
                $rs_rd->setStatusLelang($rs_drd->getStatusLelang());
                $rs_rd->setNomorLelang($rs_drd->getNomorLelang());
                $rs_rd->setKoefisienSemula($rs_drd->getKoefisienSemula());
                $rs_rd->setVolumeSemula($rs_drd->getVolumeSemula());
                $rs_rd->setHargaSemula($rs_drd->getHargaSemula());
                $rs_rd->setTotalSemula($rs_drd->getTotalSemula());
                $rs_rd->setLockSubtitle($rs_drd->getLockSubtitle());
                $rs_rd->setStatusHapus($rs_drd->getStatusHapus());
                $rs_rd->setTahun($rs_drd->getTahun());
                $rs_rd->setKodeLokasi($rs_drd->getKodeLokasi());
                $rs_rd->setKecamatan($rs_drd->getKecamatan());
                $rs_rd->setRekeningCodeAsli($rs_drd->getRekeningCodeAsli());
                $rs_rd->setNilaiAnggaran($rs_drd->getNilaiAnggaran());
                $rs_rd->setNoteSkpd($rs_drd->getNoteSkpd());
                $rs_rd->setNotePeneliti($rs_drd->getNotePeneliti());
                $rs_rd->setIsBlud($rs_drd->getIsBlud());
                $rs_rd->setOb($rs_drd->getOb());
                $rs_rd->setObFromId($rs_drd->getObFromId());
                $rs_rd->setIsPerKomponen($rs_drd->getIsPerKomponen());
                $rs_rd->setKegiatanCodeAsal($rs_drd->getKegiatanCodeAsal());
                $rs_rd->setThKeMultiyears($rs_drd->getThKeMultiyears());
                $rs_rd->setLokasiKecamatan($rs_drd->getLokasiKecamatan());
                $rs_rd->setLokasiKelurahan($rs_drd->getLokasiKelurahan());
                $rs_rd->setHargaSebelumSisaLelang($rs_drd->getHargaSebelumSisaLelang());
                $rs_rd->setIsMusrenbang($rs_drd->getIsMusrenbang());
                $rs_rd->setSubIdAsal($rs_drd->getSubIdAsal());
                $rs_rd->setSubtitleAsal($rs_drd->getSubtitleAsal());
                $rs_rd->setKodeSubAsal($rs_drd->getKodeSubAsal());
                $rs_rd->setSubAsal($rs_drd->getSubAsal());
                $rs_rd->setLastEditTime($rs_drd->getLastEditTime());
                $rs_rd->setIsPotongBpjs($rs_drd->getIsPotongBpjs());
                $rs_rd->setIsIuranBpjs($rs_drd->getIsIuranBpjs());
                $rs_rd->setStatusOb($rs_drd->getStatusOb());
                $rs_rd->setObParent($rs_drd->getObParent());
                $rs_rd->setObAlokasiBaru($rs_drd->getObAlokasiBaru());
                $rs_rd->setIsHibah($rs_drd->getIsHibah());
                $rs_rd->setStatusLevel(0);
                $rs_rd->setStatusSisipan(false);
                $rs_rd->setStatusLevelTolak(0);
                $rs_rd->setIsTapdSetuju(false);
                $rs_rd->setIsBappekoSetuju(false);
                $rs_rd->setIsBagianHukumSetuju(false);
                $rs_rd->setIsInspektoratSetuju(false);
                $rs_rd->setIsBadanKepegawaianSetuju(false);
                $rs_rd->setIsLppaSetuju(false);
                $rs_rd->setIsBagianOrganisasiSetuju(false);
                $rs_rd->setAkrualCode($rs_drd->getAkrualCode());
                $rs_rd->setTipe2($rs_drd->getTipe2());
                $rs_rd->setIsPenyeliaSetuju(false);
                $rs_rd->setStatusKomponenBerubah($status_komponen_berubah);
                $rs_rd->setStatusKomponenBaru($status_komponen_baru);
                $rs_rd->save();
                $con->commit();
                $this->setFlash('berhasil', 'Komponen telah dikembalikan ke semula');
            }
        }
    } else {
        $nilaiBaru = 0;
        $volume = 0;
        $rd_function = new DinasRincianDetail();
        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
            if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                $lelang = $rd_function->getCekLelang($unit_id, $kode_kegiatan, $detail_no, 0);
                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                    $totNilaiSwakelola = $rd_function->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                    $totNilaiKontrak = $rd_function->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                    $totNilaiRealisasi = $rd_function->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                    $totVolumeRealisasi = $rd_function->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);

                    $totNilaiHps = $rd_function->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    $ceklelangselesaitidakaturanpembayaran = $rd_function->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                    $totNilaiKontrakTidakAdaAturanPembayaran = $rd_function->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                }
            }
        }
        if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
            if ($totNilaiKontrak == 0) {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
            } else if ($totNilaiSwakelola == 0) {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
            } else {
                $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
            }
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else if ($nilaiBaru < $totNilaiHps) {
            $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
            $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else if ($lelang > 0) {
            $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
            $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else if ($nilaiBaru < $totNilaiRealisasi) {
            $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else if ($volume < $totVolumeRealisasi && $rs_rd->getStatusLelang() != 'lock') {
            $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini melebihi volume realisasi di edelivery, sejumlah : ' . number_format($totVolumeRealisasi, 0, ',', '.'));
            $con->rollback();
                //return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        } else {
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            if ($rs_rd = DinasRincianDetailPeer::doSelectOne($c)) {
                $rs_rd->setStatusHapus(TRUE);
                $rs_rd->save();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Komponen telah dikembalikan ke semula');
        }
    }
    return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
}

public function executeGetKomponenSerupa() {
    if ($this->getRequestParameter('unit') && $this->getRequestParameter('komponen') && $this->getRequestParameter('rekening')) {
        $unit_id = $this->getRequestParameter('unit');
        $komponen_id = $this->getRequestParameter('komponen');
        $rekening_code = $this->getRequestParameter('rekening');

        $con = Propel::getConnection();
        $query = "select k.kode_kegiatan, k.nama_kegiatan, rd.subtitle, rd.sub, rd.rekening_code, r.rekening_name, rd.komponen_id, rd.komponen_name, rd.keterangan_koefisien, rd.note_skpd
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".rekening r
        where (rd.unit_id,rd.komponen_id,rd.rekening_code) in
        (select unit_id, komponen_id, rekening_code
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
        where unit_id='$unit_id' and status_hapus=false and tipe<>'FISIK' and tipe<>'EST' and 
        rekening_code='$rekening_code' and komponen_id='$komponen_id'
        group by unit_id, komponen_id, rekening_code
        having count(*)>1)
        and k.unit_id=rd.unit_id and k.kode_kegiatan=rd.kegiatan_code and r.rekening_code=rd.rekening_code and rd.nilai_anggaran>0
        order by komponen_name, rekening_code, kode_kegiatan, subtitle, sub ";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->list = $rs;
        $this->setLayout('kosong');
    }
}

public function executeGetKomponenSerupaFisik() {
    if ($this->getRequestParameter('komponen')) {
        $komponen_id = $this->getRequestParameter('komponen');

        $query = "select kode_rka from history_pekerjaan_v2
        where (jalan, komponen) in
        (select jalan, komponen from history_pekerjaan_v2
        where jalan<>'' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "' and substring(kode_rka for 3)<>'XXX' and substring(kode_rka for 4)<>'9999'
        group by jalan, komponen
        having count(*)>1) and jalan<>'' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "' and substring(kode_rka for 3)<>'XXX' and substring(kode_rka for 4)<>'9999'
        order by jalan";
        $congis = Propel::getConnection('gis');
        $stmt = $congis->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $arr_kode_rka = array();
        while ($rs->next()) {
            $arr_kode_rka[] = "'" . $rs->getString('kode_rka') . "'";
        }
        if (!$arr_kode_rka) {
            $kode_rka = "''";
        } else {
            $kode_rka = implode(',', $arr_kode_rka);
        }

        $query = "select u.unit_id, u.unit_name, k.kode_kegiatan, k.nama_kegiatan, rd.subtitle, rd.sub, rd.rekening_code, r.rekening_name, rd.komponen_id, rd.komponen_name, rd.detail_name, rd.keterangan_koefisien, rd.note_skpd
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".rekening r, unit_kerja u
        where (rd.komponen_id) in
        (select komponen_id
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
        where status_hapus=false and nilai_anggaran>0 and unit_id||'.'||kegiatan_code||'.'||detail_no in ($kode_rka)
        group by komponen_id
        having count(*)>1) and k.unit_id=rd.unit_id and k.kode_kegiatan=rd.kegiatan_code and r.rekening_code=rd.rekening_code and rd.nilai_anggaran>0
        and rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no in ($kode_rka) and komponen_id = '$komponen_id' and u.unit_id=k.unit_id
        order by rekening_code, komponen_name, detail_name, kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->list = $rs;
        $this->setLayout('kosong');
    }
}

public function executeGetKomponenTahunLalu() {
    if ($this->getRequestParameter('komponen')) {
        $komponen_id = $this->getRequestParameter('komponen');

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $komponen = KomponenPeer::doSelectOne($c);

        $nama = str_replace(' ', '%', trim($komponen->getKomponenName()));

        $con = Propel::getConnection();
        $query = "select komponen_name, komponen_harga, komponen_non_pajak
        from " . sfConfig::get('app_default_schema') . ".komponen_tahun_lalu
        where komponen_name ilike '%$nama%'";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->list = $rs;
        $this->setLayout('kosong');
    }
}

public function executeRequestlist() {
    $this->processSort();
    $this->processFilters();
    $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

    $pagers = new sfPropelPager('LogRequestPenyelia', 20);


    $c = new Criteria();
    $this->addSortCriteria($c);
    $user_id = $this->getUser()->getNamaUser();

    $d = new Criteria();
    $d->add(UnitKerjaPeer::UNIT_NAME, $user_id);
    $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
    if ($rs_unitkerja) {
        $unit_id = $rs_unitkerja->getUnitId();
    }

    $this->unit_kerja = $unit_id;
    $c->add(LogRequestPenyeliaPeer::STATUS, 4, Criteria::NOT_EQUAL);
    $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit_id);
    $c->addDescendingOrderByColumn(LogRequestPenyeliaPeer::UPDATED_AT);
        //$c->addAscendingOrderByColumn(LogRequestPenyeliaPeer::UNIT_ID);
    $this->addFiltersCriteriaRequestlist($c);

    $pagers->setCriteria($c);
    $pagers->setPage($this->getRequestParameter('page', 1));
    $pagers->init();

    $this->pager = $pagers;
}

protected function addFiltersCriteriaRequestlist($c) {
    if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
        $unit = $this->filters['unit_id'];
        $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit);
    }
    if (isset($this->filters['tipe']) && $this->filters['tipe'] !== '') {
        $c->add(LogRequestPenyeliaPeer::TIPE, $this->filters['tipe']);
    }
}

protected function addFiltersCriteriaRequestDinasList($c) {
    if (isset($this->filters['tipe']) && $this->filters['tipe'] !== '') {
        $c->add(LogRequestDinasPeer::TIPE, $this->filters['tipe']);
    }
}

public function executeAutocompleteJalan() {
    $searchTerm = str_replace(' ', '%', $this->getRequestParameter('term'));
    $kode_rka = $this->getRequestParameter('b');

    $query = "(SELECT DISTINCT jalan FROM history_pekerjaan_v2 
    WHERE jalan ilike '%$searchTerm%' AND kode_rka ilike '%$kode_rka%' AND 
    status_hapus=false AND jalan NOT ILIKE '% no.%' AND 
    jalan NOT ILIKE '% no %' AND jalan NOT ILIKE '% gg %' AND 
    jalan NOT ILIKE '% gg.%' AND jalan NOT ILIKE '% rt %' AND 
    jalan NOT ILIKE '% rw %' AND jalan NOT ILIKE '% rt.%' AND 
    jalan NOT ILIKE '% rw.%' AND jalan NOT ILIKE '% gang %' AND 
    jalan NOT ILIKE '%(%' AND jalan NOT ILIKE '%/%' AND 
    jalan NOT ILIKE '% kec.%' AND jalan NOT ILIKE '% kel.%' 
    ORDER BY jalan)
    UNION ALL
    (SELECT DISTINCT jalan FROM history_pekerjaan_v2 
    WHERE jalan ilike '%$searchTerm%' AND kode_rka not ilike '%$kode_rka%' AND 
    status_hapus=false AND jalan NOT ILIKE '% no.%' AND 
    jalan NOT ILIKE '% no %' AND jalan NOT ILIKE '% gg %' AND 
    jalan NOT ILIKE '% gg.%' AND jalan NOT ILIKE '% rt %' AND 
    jalan NOT ILIKE '% rw %' AND jalan NOT ILIKE '% rt.%' AND 
    jalan NOT ILIKE '% rw.%' AND jalan NOT ILIKE '% gang %' AND 
    jalan NOT ILIKE '%(%' AND jalan NOT ILIKE '%/%' AND 
    jalan NOT ILIKE '% kec.%' AND jalan NOT ILIKE '% kel.%' 
    ORDER BY jalan)";
    $con = Propel::getConnection('gis');
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    while ($rs->next()) {
        $data[] = strtoupper($rs->getString('jalan'));
    }

    echo json_encode($data);
    exit;
}

public function executeBuatNoteSkpd() {
    if ($this->getRequestParameter('key') == md5('BuatNote')) {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $note = trim($this->getRequestParameter('note'));
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $kegiatan->setCatatan($note);
        $kegiatan->save();
        $this->setFlash('berhasil', 'Catatan telah disimpan');
        return $this->redirect("entri/list");
    } else {
        $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $unit_id = $this->unit_id = $this->getRequestParameter('unitid');
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $this->note = $kegiatan->getCatatan();
    }
}

    //tiket #59
    //11 Maret 2016 - proses dari entri ke PPTK
public function executeProsesentripptk() {
    $catatan = trim($this->getRequestParameter('catatan'));
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        //$ubah_f1_dinas = $this->getRequestParameter('ubah_f1_dinas');
        // $sisa_lelang_dinas = $this->getRequestParameter('sisa_lelang_dinas');
        //$catatan_ubah_f1_dinas = trim($this->getRequestParameter('catatan_ubah_f1_dinas'));

        // bypass validasi delivery
    $status_bypass_validasi = FALSE;
    
    if(sfConfig::get('app_tahap_edit') == 'murni') {
        $status_bypass_validasi = TRUE;
    }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
    if (in_array($kode_kegiatan, array(''))) {
        $status_bypass_validasi = TRUE;
    }

     
        // bypass validasi devplan nilai belanja
        // $bypass_devplan = TRUE;
        // if(sfConfig::get('app_tahap_edit') == 'pak'){
        //     if (in_array($unit_id, $array_skpd) && (in_array($kode_kegiatan, $array_kegiatan))) {
        //         $bypass_devplan = TRUE;
        //     }
        // }

    if ($unit_id != '' && $kode_kegiatan != '') {
        if ($this->getRequestParameter('proses')) {
            $con = Propel::getConnection();
            $con->begin();
            $pilihan = $this->getRequestParameter('pilihaction');


                // VALIDASI JK EVIN
            $arr_iuran_jk = array("2.1.1.01.01.01.004.016", "2.1.1.01.01.01.004.017", "2.1.1.01.01.01.004.018");
            $arr_rekening_jk = array("5.2.1.02.02", "5.1.02.02.01.080");
            $arr_komponen_err = array();

            $arr_buka_approve_jk = array('2.2.2.01.01.0069');
            $query =
            "SELECT kegiatan_code
            FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_jk";
            $con_jk = Propel::getConnection();
            $stmt = $con_jk->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) array_push($arr_buka_approve_jk, $rs->getString('kegiatan_code'));

            if(!in_array($kode_kegiatan, $arr_buka_approve_jk)) {
                foreach($arr_rekening_jk as $rekening_id) {
                    $belanja_pegawai = 0;
                    $query =
                    "SELECT SUM(nilai_anggaran) AS nilai
                    FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                    WHERE status_hapus = FALSE
                    AND unit_id = '$unit_id'
                    AND kegiatan_code = '$kode_kegiatan'
                    AND rekening_code = '$rekening_id'
                    AND is_potong_bpjs = TRUE";
                    $con_belanja_pegawai = Propel::getConnection();
                    $stmt = $con_belanja_pegawai->prepareStatement($query);
                    $rs_belanja_pegawai = $stmt->executeQuery();
                    while($rs_belanja_pegawai->next()) $belanja_pegawai = $rs_belanja_pegawai->getFloat('nilai');

                    foreach($arr_iuran_jk as $komponen_id) {
                            // $c = new Criteria();
                            // $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                            // $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                            // $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $subtitle);
                            // $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);

                        $c = new Criteria();
                        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            // $c->add(DinasRincianDetailPeer::SUBTITLE, '%'.$rs_subtitle->getSubtitle().'%', Criteria::ILIKE);
                        $c->add(DinasRincianDetailPeer::KOMPONEN_ID, $komponen_id);
                        $c->add(DinasRincianDetailPeer::REKENING_CODE, $rekening_id);
                        $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
                        $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                        $rs_count_komponen = DinasRincianDetailPeer::doCount($c);

                        if($rs_count_komponen > 1) {
                            $c = new Criteria();
                            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                            $data_komponen = KomponenPeer::doSelectOne($c);
                            array_push($arr_komponen_err, $data_komponen->getKomponenName());
                        }
                    }                            
                    if(count($arr_komponen_err) > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, komponen - komponen berikut terambil lebih dari sekali di rekening yang sama di kegiatan ini, mohon diambil sekali saja:<br/>' . implode('<br/>', $arr_komponen_err));
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }

                    if($belanja_pegawai > 0) {
                        foreach($arr_iuran_jk as $komponen_id) {
                                // hitung perbandingan gaji dan iuran jk
                            $iuran_jk = 0;

                            $query =
                            "SELECT SUM(volume) AS nilai
                            FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            WHERE status_hapus = FALSE
                            AND unit_id = '$unit_id'
                            AND kegiatan_code = '$kode_kegiatan'
                            AND komponen_id = '$komponen_id'
                            AND rekening_code = '$rekening_id'";
                            $con_belanja_pegawai = Propel::getConnection();
                            $stmt = $con_belanja_pegawai->prepareStatement($query);
                            $rs_iuran_jk = $stmt->executeQuery();
                            while($rs_iuran_jk->next()) $iuran_jk = $rs_iuran_jk->getFloat('nilai');

                            $c = new Criteria();
                            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                            $data_komponen = KomponenPeer::doSelectOne($c);

                                // if($rekening_id == '5.1.02.02.01.080')
                                // {
                                //     $florjk = floor($iuran_jk);
                                //     $florpgw = floor($belanja_pegawai);
                                //     $selisihJk = $florjk - $florpgw;
                                //     if($selisihJk < 0) {
                                //         $this->setFlash('gagal', 'Mohon maaf, perbandingan gaji Tenaga Operasional belum balance dengan '.$data_komponen->getKomponenName().' di Rekening '.$rekening_id.', minimal volume yang diharapkan: ( '.$belanja_pegawai.' )');
                                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                                //     }
                                // }
                        }
                    }
                }
            }
                // END OF VALIDASI JK EVIN

            if ($pilihan == null) {
                if ($catatan && strlen(strip_tags($catatan)) > 20) {
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                    $kegiatan->setCatatan($catatan);
                        // if (strlen(strip_tags($catatan_ubah_f1_dinas)) > 20)
                        //     $kegiatan->setCatatanUbahF1Dinas($catatan_ubah_f1_dinas);
                        // // update nilai pertanyaan ubah f1 dinas
                        // if ($ubah_f1_dinas != '') {
                        //     if ($ubah_f1_dinas == "ya")
                        //         $kegiatan->setUbahF1Dinas(true);
                        //     if ($ubah_f1_dinas == "tidak")
                        //         $kegiatan->setUbahF1Dinas(false);
                        // }

                        // if ($sisa_lelang_dinas != '') {
                        //     if ($sisa_lelang_dinas == "ya")
                        //         $kegiatan->setSisaLelangDinas(true);
                        //     if ($sisa_lelang_dinas == "tidak")
                        //         $kegiatan->setSisaLelangDinas(false);
                        // }

                    if (!$this->getRequestParameter('ada_komponen')) {
                        $kegiatan->setStatusLevel(1);
                    }

                    $kegiatan->save();

                    if (!$this->getRequestParameter('ada_komponen'))
                        $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi PPTK');
                    else
                        $this->setFlash('berhasil', 'Telah berhasil menyimpan catatan');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                    $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui atau catatan kurang panjang');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

                // berikan alert jika komponen PSP belum dimanfaatkan, sesuai dengan permintaan
            $query =
            "SELECT
            drd.kegiatan_code,
            drd.unit_id,
            drd.detail_no,
            drd.komponen_id,
            drd.komponen_name,
            drd.detail_name,
            rka.nilai_anggaran - drd.nilai_anggaran AS sisa,
            drd.detail_kegiatan
            FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
            INNER JOIN " . sfConfig::get('app_default_schema') . ".rincian_detail rka ON drd.unit_id = rka.unit_id AND drd.kegiatan_code = rka.kegiatan_code AND drd.detail_no = rka.detail_no
            WHERE drd.unit_id = '$unit_id'           
            AND drd.satuan = 'Paket'
            AND drd.volume = 1
            AND drd.status_lelang = 'lock'
            AND drd.status_hapus = FALSE
            AND rka.nilai_anggaran > drd.nilai_anggaran";
             // AND drd.kegiatan_code = '$kode_kegiatan' pt dub krg d ganti per PD
            $con_alert = Propel::getConnection();
            $stmt = $con_alert->prepareStatement($query);
            $rs_sisapengadaan = $stmt->executeQuery();

            $sisa_pengadaan = 0;
            while ($rs_sisapengadaan->next()) {
                $sisa_pengadaan += $rs_sisapengadaan->getFloat('sisa');
            }

            $query =
            "SELECT SUM(sisa_anggaran) AS nilai_hpsp
            FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
            WHERE unit_id = '".$unit_id."'";           
            //AND  kegiatan_code = '".$kode_kegiatan."'"; tutup per sub jadi per PD cek validasi psp

            $con_alert = Propel::getConnection();
            $stmt = $con_alert->prepareStatement($query);
            $rs_total_hpsp = $stmt->executeQuery();


            $sub_lepas_psp = array('');

            $nilai_hpsp = 0;
            if($rs_total_hpsp->next())
                $nilai_hpsp += $rs_total_hpsp->getFloat('nilai_hpsp');

            if($sisa_pengadaan - $nilai_hpsp > sfConfig::get('app_default_bataspsp') && !in_array($kegiatan_code, $sub_lepas_psp) ) {
                $this->setFlash('gagal', 'Mohon maaf, masih ada komponen PSP yang belum ada pemanfaatannya Sebesar.'.$sisa_pengadaan.' - '.$nilai_hpsp.'='.$sisa_pengadaan-$nilai_hpsp);
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
                // end of berikan alert

            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
            if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                $tabel_prev = 'prev_';
            }
                //tambahan untuk yang sudah dihapus
            $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
            where a.status_hapus=true and b.status_hapus=false and 
            a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
            a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            $rs_dihapus = $stmt->executeQuery();
            while ($rs_dihapus->next()) {
                array_push($pilihan, $rs_dihapus->getString('detail_no'));
            }

            //tambahan generate array baru untuk lepas validasi permakanan kecamatan
            $lepas = array();   
            array_push($lepas,$pilihan);     
            $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
            where  status_hapus=true and 
            unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
            $stmt = $con->prepareStatement($query);
            $rs_det_lepas = $stmt->executeQuery();
            while ($rs_det_lepas ->next()) {
                if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                    unset($lepas[$key]);                    
                }               
            }            

            //tambahan generate array baru untuk lepas validasi psp pak 030921
            $lepas_psp = array();   
            array_push($lepas_psp,$pilihan);     
            $query1 = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
            where  status_hapus=false and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_lelang='lock' and satuan='Paket' and volume=1 and tahap='pak'";            
            $stmt1 = $con->prepareStatement($query1);
            $rs_det_lepas_psp = $stmt1->executeQuery();
            while ($rs_det_lepas_psp ->next()) {
                if (($key = array_search( $rs_det_lepas_psp->getString('detail_no'), $lepas_psp)) !== false) {
                    unset($lepas_psp[$key]);                    
                }               
            }

            $rd = new DinasRincianDetail();
            if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                if ($status_pagu_rincian == '1') {
                    $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

                // lintas belanja
            if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 0, $pilihan) && $unit_id != '9999') {
                $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }

            $c_master_kegiatan = new Criteria();
            $c_master_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_master_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $rs_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_master_kegiatan);
            $tahap = $rs_master_kegiatan->getTahap();

                // if($tahap == "murni" && sfConfig::get('app_tahap_detail') != 'murni_bukuputih'){
                //     // variabel validasi delivery
                //     $status_bypass_validasi = TRUE; 

                //     $arr_skpd_buka_cek_belanja_murni = array('');

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 0, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni )) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }

            if($tahap != "murni"){
                if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 0, $pilihan)) {
                    $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

            if($tahap == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
             $arr_skpd_buka_cek_belanja = array();
             if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 0, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                 $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                 $con->rollback();
                 return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
             }
         }

                //khusus devplan pak
                // if($tahap == 'pak' && !$bypass_devplan){
                //     if ($rd->cekPerBelanjaDevplan($unit_id, $kode_kegiatan)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari nilai devplan');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }
                //khusus devplan

                // kunci per belanja murni
                // if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 0, $pilihan)) {
                //     $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari buku biru');
                //     $con->rollback();
                //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                // }

         $arr_skpd_buka_cek_rekening = array('');
                // ganti pakai kode kegiatan

                //cek sisipan rekening murni pakai tabel murni_rincian_detail
                if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'murni')) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_rekening) && sfConfig::get('app_tahap_detail') != 'murni_bukuputih') {
                   $pesan = '';
                   foreach ($rek as $kode => $isi) {
                       $nilai = explode('|', $isi);
                       $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                   }
                   $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Sebelumnya');
                   $con->rollback();
                   return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                //cek sisipan rekening revisi1 pakai tabel revisi1_rincian_detail
                // if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi1'))) {
                //     $pesan = '';
                //     foreach ($rek as $kode => $isi) {
                //         $nilai = explode('|', $isi);
                //         $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                //     }
                //     //pesan untuk revisi 1
                //     $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 1 Sebelumnya');
                //     $con->rollback();
                //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                // }

                //cek sisipan rekening revisi2 pakai tabel revisi2_rincian_detail
         if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi2'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }
                    //pesan untuk revisi 2
            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 2 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }
                //cek sisipan rekening revisi2 pakai tabel revisi2_1_rincian_detail
        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi2_1'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }
                    //pesan untuk revisi 2
            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 2 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }
                //cek sisipan rekening revisi2 pakai tabel revisi2_2_rincian_detail
        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi2_2'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }
                    //pesan untuk revisi 2
            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Penyesuaian Komponen 2 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

          //      cek sisipan rekening revisi3 pakai tabel revisi3_rincian_detail
        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi3'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }
                    //pesan untuk revisi 3
            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 3 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi3_0'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            } 
            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 2 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi3_1'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            } 
            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 3_1 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi4'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 4 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi5'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 5 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi6'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 6 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi7'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 7 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi8'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 8 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi9'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 9 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'revisi10'))) {
            $pesan = '';
            foreach ($rek as $kode => $isi) {
                $nilai = explode('|', $isi);
                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
            }

            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 10 Sebelumnya');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

                // cek pak rekening pak sementara pakai tabel pak_bukuputih_rincian_detail
                // if (($rek = $rd->cekPerRekeningTahap($unit_id, $kode_kegiatan, 'pak')) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_rekening)) {
                //     $pesan = '';
                //     foreach ($rek as $kode => $isi) {
                //         $nilai = explode('|', $isi);
                //         $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                //     }
                //     //pesan untuk revisi 3
                //     $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari PAK Buku Putih');
                //     $con->rollback();
                //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                // }

         //validasi komponen covid jika berkurang 220321
        $arr_buka_approve_covid = array(''); //kode_kegiatan          

        if(in_array($kode_kegiatan, $arr_buka_approve_covid)) {
             if (($rek = $rd->cekPerCovidTahap($unit_id, $kode_kegiatan, sfConfig::get('app_tahap_detail')))) {
                $pesan = '';
                foreach ($rek as $kode => $isi) {
                    $nilai = explode('|', $isi);
                    $pesan .= '- semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                }
                        //pesan untuk revisi 2
                $this->setFlash('gagal', 'Ada pengurangan anggaran covid: ' . $pesan . ' dari  Sebelumnya');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }            
        }
       
        
        #validasi komponen blud jika berubah 270821
        $arr_buka_approve_blud = array(''); //unit id             

        if(!in_array($unit_id, $arr_buka_approve_blud)) {
             if (($rek = $rd->cekPerBLUDTahap($unit_id, $kode_kegiatan, sfConfig::get('app_tahap_detail')))) {
                $pesan = '';
                foreach ($rek as $kode => $isi) {
                    $nilai = explode('|', $isi);
                    $pesan .= '- semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                }
                        //pesan untuk revisi 2
                $this->setFlash('gagal', 'Ada perubahan anggaran BLUD: ' . $pesan . ' dari  Sebelumnya');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }            
        }

        //validasi komponen dobel 2022
        $arr_buka_approve_komponen_dobel = array(''); 
// $arr_buka_approve_komponen_dobel = array('');//kode_kegiatan 
                $query =
                "SELECT kode_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='9999'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_approve_komponen_dobel, $rs->getString('kode_kegiatan'));
                }    

        if(!in_array($kode_kegiatan, $arr_buka_approve_komponen_dobel)) {
             if (($rek = $rd->cekKomponenDobel($unit_id, $kode_kegiatan, $pilihan))) {
                $pesan = '';
                foreach ($rek as $kode => $isi) {
                    $nilai = explode('|', $isi);
                    $pesan .= '- Komponen Name=' . $nilai[0] . '  Sebanyak =' . $nilai[1];
                }
                        //pesan untuk revisi 2
                $this->setFlash('gagal', 'Terdapat Komponen Dobel : ' . $pesan . ' ');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }            
        }


        $apakah_murni = 0;
        if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'tutup') {
            $apakah_murni = 1;
        } else {
            $apakah_murni = 0;
        }

        if ($rd->cekCatatanKosong($unit_id, $kode_kegiatan, $pilihan) && $apakah_murni == 0) {
            $this->setFlash('gagal', 'Masih ada komponen yang tidak ada catatan pergeseran');
            $con->rollback();
            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }

        $status_pagu_rincian = '0';

//                $arr_skpd_buka_cek_rekening = array();
//                if (($rek = $rd->cekPerRekeningRevisi1($unit_id, $kode_kegiatan, 0, $pilihan)) && !in_array($unit_id, $arr_skpd_buka_cek_rekening)) {
//                    $pesan = '';
//                    foreach ($rek as $kode => $isi) {
//                        $nilai = explode('|', $isi);
//                        $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
//                    }
//                    $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari Revisi 1 sebelumnya');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
                // cek untuk rekening yang di freeze (no insert & update)
        $c_rekening = new Criteria();
        $c_rekening->add(BukaRekeningBangunanPeer::UNIT_ID, $unit_id);
        $rekening_bangunan = BukaRekeningBangunanPeer::doCount($c_rekening);

        if($tahap != "murni"){
            $arr_rekening_tertentu = array('5.1.02.01.01.0001');
            if (($rek = $rd->cekPerRekeningTertentu($unit_id, $kode_kegiatan, 0, $pilihan, $arr_rekening_tertentu)) && $rekening_bangunan <= 0) {
                $pesan = '';
                foreach ($rek as $kode => $isi) {
                    $nilai = explode('|', $isi);
                    $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                }
                $this->setFlash('gagal', 'Ada pergeseran rekening Bahan Bangunan : ' . $pesan . ' dari RKA');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 0, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
//                if ($rd->cekPerBelanjaPakDinas($unit_id)) {
//                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
        // ganti kode kegiatan
        $arr_skpd_tutup = array('');
        if ($tahap != "murni") {
            if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 0, $pilihan)) {
                    $pesan = '';
                    foreach ($rek as $kode => $isi) {
                        $nilai = explode('|', $isi);
                        $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                    }
                    $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

            }
                } //tutup  if tahap != murni

                //cek delivery dan lelang baru
                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);

                             if (strpos($unit_id, '12') === FALSE) {
                             $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                             $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                             }
                            else
                             {
                                 $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                             $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                             } 

                            //$totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                           // $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan);            

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);                        
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);                           
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);              
                        }
                    }
                }
                $validasi = FALSE;
                $error_validasi = array();
                $status_validasi = array();
                foreach ($pilihan as $detail_no) {
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    }

                    if (strlen(strip_tags($catatan)) > 20) {
                        $c_kegiatan = new Criteria();
                        $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                        $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                        $kegiatan->setCatatan($catatan);
                        // if (strlen(strip_tags($catatan_ubah_f1_dinas)) > 20)
                        //     $kegiatan->setCatatanUbahF1Dinas($catatan_ubah_f1_dinas);
                        $kegiatan->save();
                        //$con->commit();
                    }
                    // cek pertanyaan ubah F1
                    // if ($ubah_f1_dinas == '') {
                    //     $this->setFlash('gagal', 'Belum menjawab pertanyaan perubahan F1');
                    //     $con->rollback();
                    //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    // }
                    // else {
                    //     if ($ubah_f1_dinas == "ya") {
                    //         // $catatan_ubah_f1_dinas = trim($this->getRequestParameter('catatan_ubah_f1_dinas'));
                    //         if (strlen(strip_tags($catatan_ubah_f1_dinas)) == 0) {
                    //             $this->setFlash('gagal', 'Belum mengisi catatan perubahan F1');
                    //             $con->rollback();
                    //             return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    //         }
                    //     }
                    // }

                    // // if jawab ya, ada sisa lelang
                    // if ($sisa_lelang_dinas == '') {
                    //     $this->setFlash('gagal', 'Belum menjawab pertanyaan penggunaan sisa lelang');
                    //     $con->rollback();
                    //     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    // } 
                    // else {
                    //     if ($sisa_lelang_dinas == "ya") {
                    //         // $catatan_ubah_f1_dinas = trim($this->getRequestParameter('catatan_ubah_f1_dinas'));
                    //         if (strlen(strip_tags($catatan_ubah_f1_dinas)) == 0) {
                    //             $this->setFlash('gagal', 'Belum mengisi catatan penggunaan sisa lelang');
                    //             $con->rollback();
                    //             return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    //         }
                    //     }
                    // }
                    // tutup dulu validasi eproj & edel karena masih murni 
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);

                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan])) && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu 1');
                            array_push($status_validasi,0);

                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu 2');
                             array_push($status_validasi,0);
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu 3');
                             array_push($status_validasi,0);
                        }
                        $validasi = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu 4');
                         array_push($status_validasi,0);
                    } else if ($lelang[$kode_detail_kegiatan] > 0 && !$status_bypass_validasi ) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang 5');
                         array_push($status_validasi,0);
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu 6.');
                         array_push($status_validasi,0);
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan] && !$status_bypass_validasi ) {
                        $validasi = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu 7');
                         array_push($status_validasi,0);
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock' && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume chek' . $totVolumeRealisasi[$kode_detail_kegiatan]);
                         array_push($status_validasi,0);
                    } else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan].'8');
                        array_push($status_validasi,0);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery 9');
                         array_push($status_validasi,0);
                    } else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                                $orang_semula = $rs_rka->getVolumeOrang();                                
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                                $orang_semula = 0;
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            $orang_menjadi = $rs_rd->getVolumeOrang();
                            if ($semula <> $menjadi) {
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $tahapchar = DinasMasterKegiatanPeer::getTahapDetail($unit_id, $kode_kegiatan);
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                $detailkegiatan = $unit_id.".".$kode_kegiatan.".".$detail_no;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setStatus(0);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setStatus(0);
                                    $log->setTahapChar($tahapchar);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setDetailKegiatan($detailkegiatan);
                                    $log->save();
                                }

                            }
                            //untuk log volume orang
                            if ($orang_semula <> $orang_menjadi) {
                                $detail_kegiatan = $unit_id.".".$kode_kegiatan.".".$detail_no;
                                $c_log_org = new Criteria();
                                $c_log_org ->add(LogVolumeOrangPeer::DETAIL_KEGIATAN, $detail_kegiatan);
                                $c_log_org ->add(LogVolumeOrangPeer::TAHAP,$tahapchar); 
                                if ($logs = LogVolumeOrangPeer::doSelectOne($c_log_org)) {
                                    $logs->setVolumeSemula();
                                    $logs->setVolumeMenjadi();
                                    $logs->setTahap($tahapchar);
                                    $logs->setWaktuEdit(date('Y-m-d H:i:s'));
                                    $logs->save();
                                }
                                else
                                {
                                   $logs = new LogVolumeOrang();
                                   $logs->setDetailKegiatan($detail_kegiatan);
                                   $logs->setVolumeSemula($orang_semula);
                                   $logs->setVolumeMenjadi($orang_menjadi);
                                   $logs->setWaktuEdit(date('Y-m-d H:i:s'));
                                   $logs->setTahap($tahapchar);
                                   $logs->save();

                               }                           

                           }
                       }
                       $rs_rd->setStatusLevel(1);
                       $rs_rd->save();
                       array_push($status_validasi,1);
                   }
               }
            //tutup validasi karena masih murni
            if ($validasi) {
                $this->setFlash('gagal', implode(' --- ', $error_validasi));
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }

            $kumpulan_detail_no = implode('|', $pilihan);
            $log = new LogApproval();
            $log->setUnitId($unit_id);
            $log->setKegiatanCode($kode_kegiatan);
            $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
            $log->setUserId($this->getUser()->getNamaLogin());
            $log->setKumpulanDetailNo($kumpulan_detail_no);
            $log->setWaktu(date('Y-m-d H:i:s'));
            $log->setSebagai('Entri');
            $log->save();

            if ($catatan && strlen(strip_tags($catatan)) > 20) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setCatatan($catatan);
                $kegiatan->setStatusLevel(1);
                    // if ($ubah_f1_dinas == "ya")
                    //     $kegiatan->setUbahF1Dinas(true);
                    // if ($ubah_f1_dinas == "tidak")
                    //     $kegiatan->setUbahF1Dinas(false);
                    // if ($sisa_lelang_dinas == "ya") {
                    //     $kegiatan->setSisaLelangDinas(true);
                    //     $kegiatan->setCatatanUbahF1Dinas($catatan_ubah_f1_dinas);
                    // }
                    // if ($sisa_lelang_dinas == "tidak")
                    //     $kegiatan->setSisaLelangDinas(false);
                $kegiatan->save();
            } else {
                $this->setFlash('gagal', 'Catatan berisi minimal 20 karakter ');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
            
            if(in_array(0, $status_validasi))
            {
                 $this->setFlash('gagal', 'Ada Komponen yang disesuaikan');
                 $con->rollback();
            }
            else
            {
                 $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi PPTK');
                 $con->commit();
            }

            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
        }
    } else {
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }
}

    //tiket #59
    //11 Maret 2016 - proses dari entri ke PPTK
public function executeProsesentripptk_LAMA() {
    $catatan = trim($this->getRequestParameter('catatan'));
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

    if ($unit_id != '' && $kode_kegiatan != '') {
        $ada_error = FALSE;
        $error = array();
        if ($this->getRequestParameter('proses')) {
            $con = Propel::getConnection();
            $con->begin();
//                $query = "select detail_no
//                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
//                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and 
//                        rd.status_hapus=false and rd.status_level=0 and 
//                        (detail_no in (select detail_no as nilai 
//                            from " . sfConfig::get('app_default_schema') . ".rincian_detail drd 
//                            where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and 
//                            drd.status_hapus=false and drd.nilai_anggaran<>rd.nilai_anggaran) 
//                            or 
//                            detail_no not in (select detail_no as nilai 
//                            from " . sfConfig::get('app_default_schema') . ".rincian_detail drd 
//                            where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and 
//                            drd.status_hapus=false))";
//                $stmt = $con->prepareStatement($query);
//                $rs = $stmt->executeQuery();
//                $pilihan = array();
//                while ($rs->next()) {
//                    array_push($pilihan, $rs->getInt('detail_no'));
//                }
            $pilihan = $this->getRequestParameter('pilihaction');
            if ($pilihan == null) {
                if ($catatan && strlen(strip_tags($catatan)) > 20) {
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                    $kegiatan->setCatatan($catatan);
                    if (!$this->getRequestParameter('ada_komponen')) {
                        $kegiatan->setStatusLevel(1);
                    }
                    $kegiatan->save();
                    if (!$this->getRequestParameter('ada_komponen'))
                        $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi PPTK');
                    else
                        $this->setFlash('berhasil', 'Telah berhasil menyimpan catatan');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                    $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui atau catatan kurang panjang');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
            if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                $tabel_prev = 'prev_';
            }
                //tambahan untuk yang sudah dihapus
            $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
            where a.status_hapus=true and b.status_hapus=false and 
            a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
            a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            $rs_dihapus = $stmt->executeQuery();
            while ($rs_dihapus->next()) {
                array_push($pilihan, $rs_dihapus->getString('detail_no'));
            }
            $rd = new DinasRincianDetail();
            if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 0, $pilihan) && $unit_id != '9999') {
                $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 0, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
//                $arr_skpd_buka_cek_belanja = array('2600');
//                if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 0, $pilihan) && !in_array($unit_id, $arr_skpd_buka_cek_belanja)) {
//                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
//                if ($rd->cekPerBelanjaPakDinas($unit_id)) {
//                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
            $arr_skpd_tutup = array('');
            if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($unit_id, $arr_skpd_tutup))) {
                if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 0, $pilihan)) {
                    $pesan = '';
                    foreach ($rek as $kode => $isi) {
                        $nilai = explode('|', $isi);
                        $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                    }
                    $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

            foreach ($pilihan as $detail_no) {
                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                $nilaiBaru = round($rs_rd->getKomponenHargaAwal() * $rs_rd->getVolume() * (100 + $rs_rd->getPajak()) / 100);

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                    //$totVolumeDelivery=0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_rd->getNilaiAnggaran());
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                            $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);


                            $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                        }
                    }
                }
                if (($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) {
                    if ($totNilaiKontrak == 0) {
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                            //$this->setFlash('gagal', 'Mohon maaf , untuk komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                    } else if ($totNilaiSwakelola == 0) {
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                            //$this->setFlash('gagal', 'Mohon maaf , untuk komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                    } else if ($totVolumeDelivery == 0) {
                        array_push($error, 'Volume Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') ,Kurang dari volume di edelivery sebesar.' . number_format($totVolumeDelivery, 0, ',', '.'));
                            //$this->setFlash('gagal', 'Mohon maaf , untuk komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                    } else {
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                            //$this->setFlash('gagal', 'Mohon maaf , untuk komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                    }
                    $ada_error = TRUE;

//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($nilaiBaru < $totNilaiHps) {
                    $ada_error = TRUE;
                    array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $ada_error = TRUE;
                    array_push($error, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $this->setFlash('gagal', 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($lelang > 0) {
                    $ada_error = TRUE;
                    array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') Sedang dalam Proses Lelang');
//                        $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ')');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                    $ada_error = TRUE;
                    array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                } else if ($nilaiBaru < $totNilaiRealisasi) {
                    $ada_error = TRUE;
                    array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                } else if ($rs_rd->getVolume() < $totVolumeRealisasi && $rs_rd->getStatusLelang() != 'lock') {
                        //echo $rs_rd->getDetailNo() . ' ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getKeteranganKoefisien() . ' '. $rs_rd->getStatusLelang(); $con->rollback(); exit;
                    $ada_error = TRUE;
                    array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' (' . $rs_rd->getDetailName() . ') sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                } else {
                    $rs_rd->setStatusLevel(1);
                    $rs_rd->save();
                }
            }

            $kumpulan_detail_no = implode('|', $pilihan);
            $log = new LogApproval();
            $log->setUnitId($unit_id);
            $log->setKegiatanCode($kode_kegiatan);
                //$tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
            $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
            $log->setUserId($this->getUser()->getNamaLogin());
            $log->setKumpulanDetailNo($kumpulan_detail_no);
            $log->setWaktu(date('Y-m-d H:i:s'));
            $log->setSebagai('Entri');
            $log->save();

            if ($catatan && strlen(strip_tags($catatan)) > 20) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setCatatan($catatan);
                $kegiatan->setStatusLevel(1);
                $kegiatan->save();
            } else {
                $ada_error = TRUE;
                array_push($error, 'Catatan berisi minimal 20 karakter');
//                    $this->setFlash('gagal', 'Catatan berisi minimal 20 karakter ');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
            if ($ada_error) {
                $this->setFlash('gagal', implode(' --- ', $error));
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            } else {
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi PPTK');
                $con->commit();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
    } else {
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }
}

    //tiket #59
    //11 Maret 2016 - proses dari PPTK ke KPA
public function executeProsespptkkpa() {
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
    $pilihan = $this->getRequestParameter('pilihaction');

    $status_bypass_validasi = FALSE;
    if(sfConfig::get('app_tahap_edit') == 'murni') {
        $status_bypass_validasi = TRUE;
    }

        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
    if (in_array($kode_kegiatan, array(''))) {
        $status_bypass_validasi = TRUE;
    }
    if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
        $ada_error = FALSE;
        $error = array();
        if ($this->getRequestParameter('proses')) {
            $con = Propel::getConnection();
            $con->begin();

            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
            if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                $tabel_prev = 'prev_';
            }
                //tambahan untuk yang sudah dihapus
            $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
            where a.status_hapus=true and b.status_hapus=false and 
            a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
            a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            $rs_dihapus = $stmt->executeQuery();
            while ($rs_dihapus->next()) {
                array_push($pilihan, $rs_dihapus->getString('detail_no'));
            }

            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

            $rd = new DinasRincianDetail();

            if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                if ($status_pagu_rincian == '1') {
                    $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

            if($kegiatan->getTahap() != "murni"){
                if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 1, $pilihan)) {
                    $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }

            if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 1, $pilihan) && $unit_id != '9999') {
                $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }

                // if($kegiatan->getTahap() == "murni"){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array('');

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 1, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }

                // }

            if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak ketika tahapnya pak
                    // ganti per kode kegiatan 
                $arr_skpd_buka_cek_belanja = array();
                if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 1, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                 $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                 $con->rollback();
                 return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
             }
         }

//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 1, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
         $arr_skpd_tutup = array('');
         if ($kegiatan->getTahap() != "murni") {
            if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 1, $pilihan)) {
                    $pesan = '';
                    foreach ($rek as $kode => $isi) {
                        $nilai = explode('|', $isi);
                        $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                    }
                    $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            }
                } //tutup  if tahap != murni

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
                    $lepas = array();   
                    array_push($lepas,$pilihan);     
                    $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                    where  status_hapus=true and 
                    unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
                    $stmt = $con->prepareStatement($query);
                    $rs_det_lepas = $stmt->executeQuery();
                    while ($rs_det_lepas ->next()) {
                        if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                            unset($lepas[$key]);                    
                        }               
                    }

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);

                             if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            }  

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);   
                        }
                    }
                }

                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 1);
                foreach ($pilihan as $detail_no) {
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    }

                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);
                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan])) && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');

                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $ada_error = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0 && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan] && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock' && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . $totVolumeRealisasi[$kode_detail_kegiatan]);
                    }                    
                    else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    } else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setStatus(0);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                        }
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(2);
                        $rs_rd->save();
                    }
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                
                if ($kegiatan->getStatusLevel() >= 1) {
                    $kegiatan->setStatusLevel(2);
                    $kegiatan->save();
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PPTK');
                $log->save();

                // tutup dulu karena masih murni

                if ($ada_error) {
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {

                    $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi KPA');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } //tutupnya if ada error


            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(1);
                    $rs_rd->save();
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PPTK|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                // reset jawaban pertanyaan pada dinas
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);
                // reset jawaban pertanyaan pada dinas
                $kegiatan->save();

                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 1 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(2);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi KPA');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    //tiket #59
    //11 Maret 2016 - proses dari KPA ke PA
    public function executeProseskpapa() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        
        $status_bypass_validasi = FALSE;
        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kode_kegiatan, array(''))) {
            $status_bypass_validasi = TRUE;
        }

            //by pass by detail_kegiatan
    $arr_buka_validasi_realisasi = array();
                $query =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
                }              
        if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
            $status_bypass_validasi = TRUE;
    }
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            $ada_error = FALSE;
            $error = array();
            if ($this->getRequestParameter('proses')) {
                $con = Propel::getConnection();
                $con->begin();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $c = new Criteria();
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                if($kegiatan->getTahap() != "murni"){

                    if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 2, $pilihan)) {
                        $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }

                }


                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 2, $pilihan) && $unit_id != '9999') {
                    $con->rollback();
                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                // if($kegiatan->getTahap() == 'murni'){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array('');

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 2, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }

                if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                    $arr_skpd_buka_cek_belanja = array();
                    if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 2, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                     $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                     $con->rollback();
                     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                 }
             }
//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 2, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
             $arr_skpd_tutup = array('');

             if ($kegiatan->getTahap() != "murni") {
                if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                    if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 2, $pilihan)) {
                        $pesan = '';
                        foreach ($rek as $kode => $isi) {
                            $nilai = explode('|', $isi);
                            $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                        }
                        $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                } //tutup  if tahap != murni
                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 2);

                $id_baru = 0;

                if ($kegiatan->getTahap() == 'pak') {
                    //buat Print RKA
                    $query_qr = " select max(id) as id from " . sfConfig::get('app_default_schema') . ".print_rka_pak";
                    $stmt_qr = $con->prepareStatement($query_qr);
                    $rs_id = $stmt_qr->executeQuery();
                    if ($rs_id->next()) {
                        $id_baru = $rs_id->getInt('id') + 1;
                    } else {
                        $id_baru = 1;
                    }
                    $token = md5(rand());
                    $rs_qr = new PrintRkaPak();
                    $rs_qr->setId($id_baru);
                    $rs_qr->setUnitId($unit_id);
                    $rs_qr->setKegiatanCode($kode_kegiatan);
                    $rs_qr->setWaktu(date('Y-m-d H:i:s'));
                    $rs_qr->setToken($token);
                    $rs_qr->save();
                }

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
                $lepas = array();   
                array_push($lepas,$pilihan);     
                $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                where  status_hapus=true and 
                unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
                $stmt = $con->prepareStatement($query);
                $rs_det_lepas = $stmt->executeQuery();
                while ($rs_det_lepas ->next()) {
                    if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                        unset($lepas[$key]);                    
                    }               
                }

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                           
                            if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            }  

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);
                        }
                    }
                }

                foreach ($pilihan as $detail_no) {
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    }

                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);
                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan])) && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');

                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $ada_error = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0 && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan] && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock' && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . $totVolumeRealisasi[$kode_detail_kegiatan]);
                    }                     
                    else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    }else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                        }
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(3);
                        $rs_rd->save();
                    }

                    //untuk PAK
                    if ($id_baru > 0) {
                        $detail = new PrintRkaPakDetail();
                        $detail->setIdPrintRkaPak($id_baru);
                        $detail->setSubtitle($rs_rd->getSubtitle());
                        $detail->setRekeningCode($rs_rd->getRekeningCode());
                        $detail->setSubsubtitle($rs_rd->getSub());
                        $detail->setNamaKomponen($rs_rd->getKomponenName());
                        $detail->setSatuan($rs_rd->getSatuan());
                        $detail->setKoefisien($rs_rd->getKeteranganKoefisien());
                        $detail->setHarga($rs_rd->getKomponenHarga());
                        $detail->setHasil($rs_rd->getVolume() * $rs_rd->getKomponenHarga());
                        $detail->setPajak($rs_rd->getPajak());
                        $detail->save();
                    }
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                if ($kegiatan->getStatusLevel() >= 2) {
                    $kegiatan->setStatusLevel(3);
                    $kegiatan->save();
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('KPA');
                $log->save();

                // tutup dulu karena masih murni

                if ($ada_error) {
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {

                    $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi PA');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } //tutup if ada error
            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(2);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('KPA|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);

                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 2 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(3);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi PA');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    public function executeProsespptkpa() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        
        $status_bypass_validasi = FALSE;
        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kode_kegiatan, array(''))) {
            $status_bypass_validasi = TRUE;
        }

            //by pass by detail_kegiatan
    $arr_buka_validasi_realisasi = array();
                $query =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
                }              
        if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
            $status_bypass_validasi = TRUE;
    }
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            $ada_error = FALSE;
            $error = array();
            if ($this->getRequestParameter('proses')) {
                $con = Propel::getConnection();
                $con->begin();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $c = new Criteria();
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                if($kegiatan->getTahap() != "murni"){

                    if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 1, $pilihan)) {
                        $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }

                }


                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 1, $pilihan) && $unit_id != '9999') {
                    $con->rollback();
                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                // if($kegiatan->getTahap() == 'murni'){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array('');

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 2, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }

                if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                    $arr_skpd_buka_cek_belanja = array();
                    if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 1, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                     $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                     $con->rollback();
                     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                 }
             }
//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 2, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
             $arr_skpd_tutup = array('');

             if ($kegiatan->getTahap() != "murni") {
                if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                    if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 1, $pilihan)) {
                        $pesan = '';
                        foreach ($rek as $kode => $isi) {
                            $nilai = explode('|', $isi);
                            $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                        }
                        $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                } //tutup  if tahap != murni
                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 1);

                $id_baru = 0;

                if ($kegiatan->getTahap() == 'pak') {
                    //buat Print RKA
                    $query_qr = " select max(id) as id from " . sfConfig::get('app_default_schema') . ".print_rka_pak";
                    $stmt_qr = $con->prepareStatement($query_qr);
                    $rs_id = $stmt_qr->executeQuery();
                    if ($rs_id->next()) {
                        $id_baru = $rs_id->getInt('id') + 1;
                    } else {
                        $id_baru = 1;
                    }
                    $token = md5(rand());
                    $rs_qr = new PrintRkaPak();
                    $rs_qr->setId($id_baru);
                    $rs_qr->setUnitId($unit_id);
                    $rs_qr->setKegiatanCode($kode_kegiatan);
                    $rs_qr->setWaktu(date('Y-m-d H:i:s'));
                    $rs_qr->setToken($token);
                    $rs_qr->save();
                }

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
                $lepas = array();   
                array_push($lepas,$pilihan);     
                $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                where  status_hapus=true and 
                unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
                $stmt = $con->prepareStatement($query);
                $rs_det_lepas = $stmt->executeQuery();
                while ($rs_det_lepas ->next()) {
                    if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                        unset($lepas[$key]);                    
                    }               
                }

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                           
                            if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            }  

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);
                        }
                    }
                }

                foreach ($pilihan as $detail_no) {
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    }

                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);
                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan])) && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');

                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $ada_error = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0 && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan] && !$status_bypass_validasi ) {
                        $ada_error = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock' && !$status_bypass_validasi) {
                        $ada_error = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . $totVolumeRealisasi[$kode_detail_kegiatan]);
                    }                     
                    else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    }else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                        }
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(3);
                        $rs_rd->save();
                    }

                    //untuk PAK
                    if ($id_baru > 0) {
                        $detail = new PrintRkaPakDetail();
                        $detail->setIdPrintRkaPak($id_baru);
                        $detail->setSubtitle($rs_rd->getSubtitle());
                        $detail->setRekeningCode($rs_rd->getRekeningCode());
                        $detail->setSubsubtitle($rs_rd->getSub());
                        $detail->setNamaKomponen($rs_rd->getKomponenName());
                        $detail->setSatuan($rs_rd->getSatuan());
                        $detail->setKoefisien($rs_rd->getKeteranganKoefisien());
                        $detail->setHarga($rs_rd->getKomponenHarga());
                        $detail->setHasil($rs_rd->getVolume() * $rs_rd->getKomponenHarga());
                        $detail->setPajak($rs_rd->getPajak());
                        $detail->save();
                    }
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                if ($kegiatan->getStatusLevel() >= 1) {
                    $kegiatan->setStatusLevel(3);
                    $kegiatan->save();
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PPTK');
                $log->save();

                // tutup dulu karena masih murni

                if ($ada_error) {
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {

                    $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi PA');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } //tutup if ada error
            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(1);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PPTK|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);

                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 1 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(3);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi PA');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    //tiket #59
    //11 Maret 2016 - proses PA ke tim anggaran
    public function executeProsespaanggaran() {
        $unit_id = $this->getRequestParameter('unit_id');
        // echo $unit_id;die(); 
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');

        $status_bypass_validasi = FALSE;
        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }

            //by pass by detail_kegiatan
        $arr_buka_validasi_realisasi = array();
                $query =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
                }              
        if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
            $status_bypass_validasi = TRUE;
      }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kode_kegiatan, array(''))) {
            $status_bypass_validasi = TRUE;
        }
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            $ada_error = FALSE;
            $error = array();
            if ($this->getRequestParameter('proses')) {
                // echo $unit_id;die();
                $con = Propel::getConnection();
                $con->begin();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                if($kegiatan->getTahap() != "murni"){

                    if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 3, $pilihan)) {
                        $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }

                }
                // echo $unit_id;die();
                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 3, $pilihan) && $unit_id != '9999') {
                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                // if($kegiatan->getTahap() == 'murni'){
                //     // echo $unit_id;die();
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array();
                //     // echo $unit_id;die();
                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 3, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }

                if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                    // ganti per kode kegiatan
                    $arr_skpd_buka_cek_belanja = array();
                    if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 3, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                     $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                     $con->rollback();
                     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                 }
             }
//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 3, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
                // ganti per kode kegiatan
             $arr_skpd_tutup = array('');

             if ($kegiatan->getTahap() != "murni") {
                    # code...
                if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                        // echo $unit_id;die();
                    if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 3, $pilihan)) {
                        $pesan = '';
                        foreach ($rek as $kode => $isi) {
                            $nilai = explode('|', $isi);
                            $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                        }
                        $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                } //tutup  if tahap != murni

                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 3);
                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
            $lepas = array();   
            array_push($lepas,$pilihan);     
            $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
            where  status_hapus=true and 
            unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
            $stmt = $con->prepareStatement($query);
            $rs_det_lepas = $stmt->executeQuery();
            while ($rs_det_lepas ->next()) {
                if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                    unset($lepas[$key]);                    
                }               
            }

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);

                             if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            }  

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);
                        }
                    }
                }
                // echo $unit_id;die();
                $validasi = FALSE;
                $error_validasi = array();
                foreach ($pilihan as $detail_no) {
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    }

                    // echo $unit_id;die();
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);

                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);

                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan]))  && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $validasi = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan])  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ( ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan]) && ($rs_rd->getStatusLelang() != 'lock')  && !$status_bypass_validasi ) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                    } 
                    else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    } else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                        }
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(4);
                        $rs_rd->save();
                    }
                }
                // tutup dulu karena masih murni
                if ($validasi) {
                    $this->setFlash('gagal', implode(' --- ', $error_validasi));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                
                if ($kegiatan->getStatusLevel() >= 3) {
                    $kegiatan->setStatusLevel(4);
                    $kegiatan->save();
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PA (TA)');
                $log->save();

                if (!BeritaAcaraPeer::saveUsulanDinas($unit_id, $kode_kegiatan)) {
                    $ada_error = TRUE;
                    array_push($error, 'Error: Gagal menyimpan usulan ke database 1');
                }

                
                if (!PembandingKegiatanPeer::savePembandingUsulan($unit_id, $kode_kegiatan)) {
                    $ada_error = TRUE;
                    array_push($error, 'Error: Gagal menyimpan usulan ke database 2');
                }
                
                if ($ada_error) {
                    // echo 'sini';
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                    // echo $unit_id;die();
                    $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi Tim Anggaran');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(3);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PA|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            // echo $unit_id;die();
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 3 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(4);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi Tim Anggaran');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    //tiket #59
    //11 Maret 2016 - proses kpa ke tim anggaran
    public function executeProseskpaanggaran() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            $ada_error = FALSE;
            $status_bypass_validasi = FALSE;
            if(sfConfig::get('app_tahap_edit') == 'murni') {
                $status_bypass_validasi = TRUE;
            }
            // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
            // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
            if (in_array($kode_kegiatan, array(''))) {
                $status_bypass_validasi = TRUE;
            }

            //by pass by detail_kegiatan
            $arr_buka_validasi_realisasi = array();
            $query =
            "SELECT detail_kegiatan
            FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
            }              
            if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
                $status_bypass_validasi = TRUE;
            }

            $error = array();
            if ($this->getRequestParameter('proses')) {
                $con = Propel::getConnection();
                $con->begin();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                if($kegiatan->getTahap() != "murni"){
                    if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 2, $pilihan)) {
                        $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }

                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 2, $pilihan) && $unit_id != '9999') {
                    $con->rollback();
                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                }

                // if($kegiatan->getTahap() == "murni"){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array();

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 2, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }

                if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                    // ganti per kode kegiatan
                    $arr_skpd_buka_cek_belanja = array();
                    if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 2, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                     $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                     $con->rollback();
                     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                 }
             }

            //    if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 2, $pilihan)) {
            //        $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
            //        $con->rollback();
            //        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            //    }
                // ganti per kode kegiatan
             $arr_skpd_tutup = array('');

             if ($kegiatan->getTahap() != "murni") {
                if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                    if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 2, $pilihan)) {
                        $pesan = '';
                        foreach ($rek as $kode => $isi) {
                            $nilai = explode('|', $isi);
                            $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                        }
                        $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                } //tutup  if tahap != murni

                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 2);

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
                $lepas = array();   
                array_push($lepas,$pilihan);     
                $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                where  status_hapus=true and 
                unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
                $stmt = $con->prepareStatement($query);
                $rs_det_lepas = $stmt->executeQuery();
                while ($rs_det_lepas ->next()) {
                    if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                        unset($lepas[$key]);                    
                    } 
                } 

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                          
                            if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            } 

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);
                        }
                    }
                }
                $validasi = FALSE;
                $error_validasi = array();
                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    //$nilaiBaru = round($rs_rd->getKomponenHargaAwal() * $rs_rd->getVolume() * (100 + $rs_rd->getPajak()) / 100);

                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);

                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan])) && !$status_bypass_validasi  ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $validasi = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan] && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock' && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                    } else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    }  else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                        }
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(4);
                        $rs_rd->save();
                    }
                }
                // tutup dulu karena masih murni
                if ($validasi) {
                    $this->setFlash('gagal', implode(' --- ', $error_validasi));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                if ($kegiatan->getStatusLevel() >= 2) {
                    $kegiatan->setStatusLevel(4);
                    $kegiatan->save();
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('KPA (TA)');
                $log->save();

                if (!BeritaAcaraPeer::saveUsulanDinas($unit_id, $kode_kegiatan)) {
                    $ada_error = TRUE;
                    array_push($error, 'Error: Gagal menyimpan usulan ke database');
                }

                if (!PembandingKegiatanPeer::savePembandingUsulan($unit_id, $kode_kegiatan)) {
                    $ada_error = TRUE;
                    array_push($error, 'Error: Gagal menyimpan usulan ke database');
                }

                if ($ada_error) {
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                    $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi Tim Anggaran');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(2);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('KPA|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                // reset jawaban pertanyaan f1 dan sisa lelang pada dinas
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);

                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 2 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(4);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi Tim Anggaran');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    public function executeProsespptkanggaran() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
            $ada_error = FALSE;
            $status_bypass_validasi = FALSE;
            if(sfConfig::get('app_tahap_edit') == 'murni') {
                $status_bypass_validasi = TRUE;
            }
            // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
            // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
            if (in_array($kode_kegiatan, array(''))) {
                $status_bypass_validasi = TRUE;
            }

            //by pass by detail_kegiatan
            $arr_buka_validasi_realisasi = array();
            $query =
            "SELECT detail_kegiatan
            FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
            }              
            if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
                $status_bypass_validasi = TRUE;
            }

            $error = array();
            if ($this->getRequestParameter('proses')) {
                $con = Propel::getConnection();
                $con->begin();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                if($kegiatan->getTahap() != "murni"){
                    if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 1, $pilihan)) {
                        $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }

                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 1, $pilihan) && $unit_id != '9999') {
                    $con->rollback();
                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                }

                // if($kegiatan->getTahap() == "murni"){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array();

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 2, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }

                if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                    // ganti per kode kegiatan
                    $arr_skpd_buka_cek_belanja = array();
                    if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 1, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                     $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                     $con->rollback();
                     return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                 }
             }

            //    if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 2, $pilihan)) {
            //        $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
            //        $con->rollback();
            //        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            //    }
                // ganti per kode kegiatan
             $arr_skpd_tutup = array('');

             if ($kegiatan->getTahap() != "murni") {
                if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                    if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 1, $pilihan)) {
                        $pesan = '';
                        foreach ($rek as $kode => $isi) {
                            $nilai = explode('|', $isi);
                            $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                        }
                        $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                } //tutup  if tahap != murni

                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 1);

                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
                $lepas = array();   
                array_push($lepas,$pilihan);     
                $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                where  status_hapus=true and 
                unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
                $stmt = $con->prepareStatement($query);
                $rs_det_lepas = $stmt->executeQuery();
                while ($rs_det_lepas ->next()) {
                    if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                        unset($lepas[$key]);                    
                    } 
                } 

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                          
                            if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            } 

                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);
                        }
                    }
                }
                $validasi = FALSE;
                $error_validasi = array();
                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    //$nilaiBaru = round($rs_rd->getKomponenHargaAwal() * $rs_rd->getVolume() * (100 + $rs_rd->getPajak()) / 100);

                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);

                    if ( (($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan])) && !$status_bypass_validasi  ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $validasi = TRUE;
                    } else if ($ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1 && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan] && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock' && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                    } else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    }  else {
                        if ($unit_id != '9999') {
                            $c = new Criteria();
                            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                            if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                $semula = $rs_rka->getNilaiAnggaran();
                                $volume_semula = $rs_rka->getVolume();
                                $satuan_semula = $rs_rka->getSatuan();
                            } else {
                                $semula = 0;
                                $volume_semula = 0;
                                $satuan_semula = '';
                            }
                            $menjadi = $rs_rd->getNilaiAnggaran();
                            $volume_menjadi = $rs_rd->getVolume();
                            $satuan_menjadi = $rs_rd->getSatuan();
                            if ($semula <> $menjadi) {
                                $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                if($satuan_menjadi=='Paket' and $status_lelang == 'lock' and $volume_menjadi=='1')
                                    $nilai_psp=$menjadi-$semula;
                                $c_log = new Criteria();
                                $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                } else {
                                    $log = new LogPerubahanRevisi();
                                    $log->setUnitId($unit_id);
                                    $log->setKegiatanCode($kode_kegiatan);
                                    $log->setDetailNo($detail_no);
                                    $log->setTahap($tahap);
                                    $log->setNilaiAnggaranSemula($semula);
                                    $log->setNilaiAnggaranMenjadi($menjadi);
                                    $log->setVolumeSemula($volume_semula);
                                    $log->setVolumeMenjadi($volume_menjadi);
                                    $log->setSatuanSemula($satuan_semula);
                                    $log->setSatuanMenjadi($satuan_menjadi);
                                    $log->setNilaiPsp($nilai_psp);
                                    $log->setStatus(0);
                                    $log->save();
                                }
                            }
                        }
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(4);
                        $rs_rd->save();
                    }
                }
                // tutup dulu karena masih murni
                if ($validasi) {
                    $this->setFlash('gagal', implode(' --- ', $error_validasi));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                if ($kegiatan->getStatusLevel() >= 1) {
                    $kegiatan->setStatusLevel(4);
                    $kegiatan->save();
                }

                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PPTK (TA)');
                $log->save();

                if (!BeritaAcaraPeer::saveUsulanDinas($unit_id, $kode_kegiatan)) {
                    $ada_error = TRUE;
                    array_push($error, 'Error: Gagal menyimpan usulan ke database');
                }

                if (!PembandingKegiatanPeer::savePembandingUsulan($unit_id, $kode_kegiatan)) {
                    $ada_error = TRUE;
                    array_push($error, 'Error: Gagal menyimpan usulan ke database');
                }

                if ($ada_error) {
                    $this->setFlash('gagal', implode(' --- ', $error));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                } else {
                    $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi Tim Anggaran');
                    $con->commit();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }
            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(1);
                    $rs_rd->save();

                    $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                    $c_log = new Criteria();
                    $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                    $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                    $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                    if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                        $log->setStatus(-1);
                        $log->save();
                    }
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('KPA|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                // reset jawaban pertanyaan f1 dan sisa lelang pada dinas
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);

                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 2 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(4);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi Tim Anggaran');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    //tiket #59
    //11 Maret 2016 - proses kpa ke tim anggaran
    public function executeProsespapenyelia() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $pilihan = $this->getRequestParameter('pilihaction');
        $status_bypass_validasi = FALSE;
        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kode_kegiatan, array(''))) {
            $status_bypass_validasi = TRUE;
        }

            //by pass by detail_kegiatan
    $arr_buka_validasi_realisasi = array();
                $query =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
                }              
        if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
            $status_bypass_validasi = TRUE;
    }
        // if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
        if ($unit_id != '' && $kode_kegiatan != '') {
            $ada_error = FALSE;
            $error = array();
            if ($this->getRequestParameter('proses')) {
                $con = Propel::getConnection();
                $con->begin();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }

                $rd = new DinasRincianDetail();

                if(sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka'){
                    $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforApprove($unit_id, $kode_kegiatan);
                    if ($status_pagu_rincian == '1') {
                        $this->setFlash('gagal', 'Jumlah rincian pada kegiatan ini tidak sama dengan jumlah pagu');
                        $con->rollback();
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                    }
                }
                
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

                if($pilihan != NULL){

                    if($kegiatan->getTahap() != 'murni'){

                        if ($rd->cekKomponenTertinggal($unit_id, $kode_kegiatan, 5, $pilihan)) {
                            $this->setFlash('gagal', 'Masih ada komponen yang tertinggal');
                            $con->rollback();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }

                    }

                    if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 5, $pilihan) && $unit_id != '9999') {
                        $con->rollback();
                        $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                    }

                // if($kegiatan->getTahap() == 'murni'){
                //     $status_bypass_validasi = TRUE;

                //     $arr_skpd_buka_cek_belanja_murni = array();

                //     if ($rd->cekPerBelanjaMurni($unit_id, $kode_kegiatan, 5, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja_murni)) {
                //         $this->setFlash('gagal', 'Masih ada belanja yang belum balance dari Murni Buku Biru (Ada Geser Nilai Belanja)');
                //         $con->rollback();
                //         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     }
                // }


                    if($kegiatan->getTahap() == 'pak' && sfConfig::get('app_tahap_detail') != 'pak_bukuputih'){
                    // cekperbelanjaPak
                        $arr_skpd_buka_cek_belanja = array();
                        if ($rd->cekPerBelanjaPak($unit_id, $kode_kegiatan, 5, $pilihan) && !in_array($kode_kegiatan, $arr_skpd_buka_cek_belanja)) {
                         $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                         $con->rollback();
                         return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                     }
                 }

                 $arr_buka_approve_covid = array(''); //kode_kegiatan          

            if(in_array($kode_kegiatan, $arr_buka_approve_covid)) {
                 if (($rek = $rd->cekPerCovidTahap($unit_id, $kode_kegiatan, sfConfig::get('app_tahap_detail')))) {
                    $pesan = '';
                    foreach ($rek as $kode => $isi) {
                        $nilai = explode('|', $isi);
                        $pesan .= '- semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                    }
                            //pesan untuk revisi 2
                    $this->setFlash('gagal', 'Ada pengurangan anggaran covid: ' . $pesan . ' dari  Sebelumnya');
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }            
            }
                    
            #validasi komponen blud jika berubah 270821
        $arr_buka_approve_blud = array(''); //unit id         

        if(!in_array($unit_id, $arr_buka_approve_blud)) {
             if (($rek = $rd->cekPerBLUDTahap($unit_id, $kode_kegiatan, sfConfig::get('app_tahap_detail')))) {
                $pesan = '';
                foreach ($rek as $kode => $isi) {
                    $nilai = explode('|', $isi);
                    $pesan .= '- semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                }
                        //pesan untuk revisi 2
                $this->setFlash('gagal', 'Ada perubahan anggaran BLUD: ' . $pesan . ' dari  Sebelumnya');
                $con->rollback();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }            
        }
//                if ($rd->cekPerKegiatanPak($unit_id, $kode_kegiatan, 5, $pilihan)) {
//                    $this->setFlash('gagal', 'Ada pergeseran anggaran dari PAK Buku Biru, silahkan cek pada halaman edit');
//                    $con->rollback();
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
                // ganti per kode kegiatan
                 $arr_skpd_tutup = array('');

                 if ($kegiatan->getTahap() != "murni") {
                    if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka' && !(in_array($kode_kegiatan, $arr_skpd_tutup))) {
                        if ($rek = $rd->cekPerRekening($unit_id, $kode_kegiatan, 5, $pilihan)) {
                            $pesan = '';
                            foreach ($rek as $kode => $isi) {
                                $nilai = explode('|', $isi);
                                $pesan .= '- ' . $kode . ' semula=' . $nilai[0] . ' menjadi=' . $nilai[1] . ' -';
                            }
                            $this->setFlash('gagal', 'Ada pergeseran rekening : ' . $pesan . ' dari RKA');
                            $con->rollback();
                            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                    }
                } //tutup  if tahap != murni

                $cek_sisipan = $rd->cekSisipan($unit_id, $kode_kegiatan, 5);
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                //tambahan generate array baru untuk lepas validasi permakanan kecamatan
            $lepas = array();   
            array_push($lepas,$pilihan);     
            $query = "select detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
            where  status_hapus=true and 
            unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and komponen_id in ('1.1.7.01.07.05.001.012','1.1.7.01.07.05.001.012.B') and unit_id ilike '12%'";
            $stmt = $con->prepareStatement($query);
            $rs_det_lepas = $stmt->executeQuery();
            while ($rs_det_lepas ->next()) {
                if (($key = array_search( $rs_det_lepas->getString('detail_no'), $lepas)) !== false) {
                    unset($lepas[$key]);                    
                }               
            }


                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rd->getCekNilaiAlokasiProjectArray($unit_id, $kode_kegiatan, $pilihan);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rd->getCekLelangArray($unit_id, $kode_kegiatan, $pilihan);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrak = $rd->getCekNilaiKontrakDeliveryArray($unit_id, $kode_kegiatan, $pilihan);
                            if (strpos($unit_id, '12') === FALSE) {
                            $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $pilihan); 
                            }
                            else 
                            {
                                $totNilaiRealisasi = $rd->getCekRealisasiArray($unit_id, $kode_kegiatan, $lepas);
                            $totVolumeRealisasi = $rd->getCekVolumeRealisasiArray($unit_id, $kode_kegiatan, $lepas); 
                            }  
                            $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaranArray($unit_id, $kode_kegiatan, $pilihan);
                            $totVolumeSPK = $rd->getCekVolumeSPKArray($unit_id, $kode_kegiatan, $pilihan);
                        }
                    }
                }

                } //tutup if pilihan != NULL
                $validasi = FALSE;
                $error_validasi = array();
                foreach ($pilihan as $detail_no) {
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kode_kegiatan . '.' . $detail_no);
                    }

                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    //$nilaiBaru = round($rs_rd->getKomponenHargaAwal() * $rs_rd->getVolume() * (100 + $rs_rd->getPajak()) / 100);

                    $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no;
                    $nilaiBaru = $rs_rd->getNilaiAnggaran();
                    $koRek = substr($rs_rd->getKomponenId(), 0, 18);
                    
                    if ( ( ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]) || ($nilaiBaru < $totNilaiSwakelola[$kode_detail_kegiatan]) )  && !$status_bypass_validasi ) {
                        if ($totNilaiKontrak[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else if ($totNilaiSwakelola[$kode_detail_kegiatan] == 0) {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        } else {
                            // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                            array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                        }
                        $validasi = TRUE;
                    } else if ( $ceklelangselesaitidakaturanpembayaran[$kode_detail_kegiatan] == 1  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Proses Lelang untuk  komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                    } else if ($lelang[$kode_detail_kegiatan] > 0  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' Sedang dalam Proses Lelang');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran[$kode_detail_kegiatan] == 1  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($nilaiBaru < $totNilaiRealisasi[$kode_detail_kegiatan]  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        // array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, silahkan cek eDelivery terlebih dahulu');
                    } else if ($rs_rd->getVolume() < $totVolumeRealisasi[$kode_detail_kegiatan] && $rs_rd->getStatusLelang() != 'lock'  && !$status_bypass_validasi) {
                        $validasi = TRUE;
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi[$kode_detail_kegiatan], 0, ',', '.'));
                    } else if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and $rs_rd->getSatuan() == 'Orang Bulan' and ($rs_rd->getVolume() < $totVolumeSPK[$kode_detail_kegiatan]) && !$status_bypass_validasi ) 
                    {
                        $validasi = TRUE;                        
                        array_push($error_validasi, 'Komponen ' . $rs_rd->getKomponenName() . ' ' . $rs_rd->getDetailName() . ' sudah terpakai di edelivery, dengan volume SPK ' . $totVolumeSPK[$kode_detail_kegiatan]);
                    } else if( ($getCekNilaiKontrak) > $nilaiBaru && ($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rd->getSatuan() == 'Orang Bulan')) {
                        $ada_error = TRUE;
                        array_push($error, 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                    } else {
                        if ($rs_rd->getStatusLevelTolak() == $rs_rd->getStatusLevel()) {
                            $rs_rd->setStatusLevelTolak(null);
                        }
                        if ($cek_sisipan) {
                            $rs_rd->setStatusSisipan(false);
                        }
                        $rs_rd->setStatusLevel(6);
                        $rs_rd->save();
                    }
                }
                // tutup dulu karena masih murni
                if ($validasi) {
                    $this->setFlash('gagal', implode(' --- ', $error_validasi));
                    $con->rollback();
                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                if ($kegiatan->getStatusLevel() >= 5) {
                    $kegiatan->setStatusLevel(6);
                    $kegiatan->save();
                }

                if($pilihan != NULL){

                    $kumpulan_detail_no = implode('|', $pilihan);
                    $log = new LogApproval();
                    $log->setUnitId($unit_id);
                    $log->setKegiatanCode($kode_kegiatan);
                    $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                    $log->setUserId($this->getUser()->getNamaLogin());
                    $log->setKumpulanDetailNo($kumpulan_detail_no);
                    $log->setWaktu(date('Y-m-d H:i:s'));
                    $log->setSebagai('PA (Proses RKA)');
                    $log->save();

                }

                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi Penyelia II');
                $con->commit();
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
            } elseif ($this->getRequestParameter('balik')) {
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $c_kegiatan->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
                if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan)) {
                    $tabel_prev = 'prev_';
                }
                //tambahan untuk yang sudah dihapus
                $query = "select a.detail_no from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . "." . $tabel_prev . "rincian_detail b 
                where a.status_hapus=true and b.status_hapus=false and 
                a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and 
                a.unit_id='$unit_id' and a.kegiatan_code='$kode_kegiatan'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_dihapus = $stmt->executeQuery();
                while ($rs_dihapus->next()) {
                    array_push($pilihan, $rs_dihapus->getString('detail_no'));
                }
                foreach ($pilihan as $detail_no) {
                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
                    $rs_rd->setStatusLevel(0);
                    $rs_rd->setStatusLevelTolak(5);
                    $rs_rd->setIsTapdSetuju(FALSE);
                    $rs_rd->setIsBappekoSetuju(FALSE);
                    $rs_rd->setIsBagianHukumSetuju(false);
                    $rs_rd->setIsInspektoratSetuju(false);
                    $rs_rd->setIsBadanKepegawaianSetuju(false);
                    $rs_rd->setIsLppaSetuju(false);
                    $rs_rd->setIsBagianOrganisasiSetuju(false);
                    $rs_rd->setIsPenyeliaSetuju(false);
                    $rs_rd->save();
                }

                $catatan_tolak = $this->getRequestParameter('catatan_tolak');
                $kumpulan_detail_no = implode('|', $pilihan);
                $log = new LogApproval();
                $log->setUnitId($unit_id);
                $log->setKegiatanCode($kode_kegiatan);
                $log->setUserId($this->getUser()->getNamaLogin());
                $log->setKumpulanDetailNo($kumpulan_detail_no);
                $log->setTahap(LogApprovalPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
                $log->setWaktu(date('Y-m-d H:i:s'));
                $log->setSebagai('PA|kembali');
                $log->setCatatan($catatan_tolak);
                $log->save();

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
                $kegiatan->setStatusLevel(0);
                $kegiatan->setUbahF1Dinas(NULL);
                $kegiatan->setSisaLelangDinas(NULL);
                $kegiatan->setIsTapdSetuju(FALSE);
                $kegiatan->setIsBappekoSetuju(FALSE);
                $kegiatan->setIsBagianHukumSetuju(false);
                $kegiatan->setIsInspektoratSetuju(false);
                $kegiatan->setIsBadanKepegawaianSetuju(false);
                $kegiatan->setIsLppaSetuju(false);
                $kegiatan->setIsBagianOrganisasiSetuju(false);
                $kegiatan->setIsPenyeliaSetuju(false);

                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
            }
        } elseif ($unit_id != '' && $kode_kegiatan != '' && $this->getRequestParameter('proses')) {
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);
            if ($kegiatan->getStatusLevel() >= 5 && !$this->getRequestParameter('ada_komponen')) {
                $kegiatan->setStatusLevel(6);
                $kegiatan->save();
                $this->setFlash('berhasil', 'Telah berhasil memproses catatan ke posisi Penyelia II');
                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
            }
        }
        $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

    //tiket #59
    //11 Maret 2016 - proses ke RKA
//    public function executeProsesrka() {
//        $unit_id = $this->getRequestParameter('unit_id');
//        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
//        $pilihan = $this->getRequestParameter('pilihaction');
//        if ($unit_id != '' && $kode_kegiatan != '' && $pilihan != null) {
//            if ($this->getRequestParameter('proses')) {
//                $con = Propel::getConnection();
//                $con->begin();
//                $rd = new DinasRincianDetail();
//                if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 6, $pilihan)) {
//                    $con->rollback();
//                    $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
//                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
//                }
//                foreach ($pilihan as $detail_no) {
//                    $c = new Criteria();
//                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
//                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
//                    $rs_drd = DinasRincianDetailPeer::doSelectOne($c);
//                    $nilaiBaru = round($rs_drd->getKomponenHargaAwal() * $rs_drd->getVolume() * (100 + $rs_drd->getPajak()) / 100);
//                    $totNilaiSwakelola = 0;
//                    $totNilaiKontrak = 0;
//                    $totNilaiAlokasi = 0;
//                    $totNilaiHps = 0;
//                    $ceklelangselesaitidakaturanpembayaran = 0;
//                    $lelang = 0;
//
//                    if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
//                        $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
//                        if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
//                            $lelang = $rd->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_drd->getNilaiAnggaran());
//                            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
//                                $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
//                                $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
//
//                                $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
//                                $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
//                            }
//                        }
//                    }
//                    if (($nilaiBaru < $totNilaiKontrak) || ($nilaiBaru < $totNilaiSwakelola)) {
//                        if ($totNilaiKontrak == 0) {
//                            $this->setFlash('gagal', 'Mohon maaf , untuk komponen '.$rs_rd->getKomponenName().' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
//                        } else if ($totNilaiSwakelola == 0) {
//                            $this->setFlash('gagal', 'Mohon maaf , untuk komponen '.$rs_rd->getKomponenName().' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
//                        } else {
//                            $this->setFlash('gagal', 'Mohon maaf , untuk komponen '.$rs_rd->getKomponenName().' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
//                        }
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//                    } else if ($nilaiBaru < $totNilaiHps) {
//                        $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//                    } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
//                        $this->setFlash('gagal', 'Proses Lelang untuk  komponen '.$rs_rd->getKomponenName().' telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//                    } else if ($lelang > 0) {
//                        $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk  komponen '.$rs_rd->getKomponenName().'');
//                        $con->rollback();
//                        return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//                    } else {
//                        if ($nilaiBaru < $totNilaiAlokasi) {
//                            $I = new Criteria();
//                            $I->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
//                            $I->add(RincianDetailBpPeer::KEGIATAN_CODE, $kode_kegiatan);
//                            $I->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
//                            $rd_bp = RincianDetailBpPeer::doSelectOne($I);
//                            if ($rd_bp) {
//                                try {
//                                    $rd_bp->setKeteranganKoefisien($rs_drd->getKeteranganKoefisien());
//                                    $rd_bp->setVolume($rs_drd->getVolume());
//                                    $rd_bp->setDetailName($rs_drd->getDetailName());
//                                    $rd_bp->setSubtitle($rs_drd->getSubtitle());
//                                    $rd_bp->setSub($rs_drd->getSub());
//                                    $rd_bp->setNoteSkpd($rs_drd->getNoteskpd());
//                                    $rd_bp->setKodeSub($rs_drd->getKodeSub());
//                                    $rd_bp->setKecamatan($rs_drd->getKodeJasmas());
//                                    $rd_bp->setIsPerKomponen('true');
//                                    $rd_bp->setTahap($rs_drd->getTahap());
//                                    $rd_bp->save();
//
//                                    budgetLogger::log('Mengupdate volume eProject untuk komponen menjadi ' . $rs_drd->getVolume() . ' dari unit id :' . $unit_id . ' dengan kode :' . $kode_kegiatan . '; detail_no :' . $detail_no . '; komponen_id:' . $rs_drd->getKomponenId() . '; komponen_name:' . $rs_drd->getKomponenName());
//                                } catch (Exception $ex) {
//                                    $this->setFlash('gagal', 'Gagal karena' . $ex);
//                                    $con->rollback();
//                                    return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//                                }
//                            }
//                        }
//
//                        $c = new Criteria();
//                        $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
//                        $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                        $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
//                        if ($rs_rd = RincianDetailPeer::doSelectOne($c)) {
//                            $rs_rd->setTipe($rs_drd->getTipe());
//                            $rs_rd->setRekeningCode($rs_drd->getRekeningCode());
//                            $rs_rd->setKomponenId($rs_drd->getKomponenId());
//                            $rs_rd->setDetailName($rs_drd->getDetailName());
//                            $rs_rd->setVolume($rs_drd->getVolume());
//                            $rs_rd->setKeteranganKoefisien($rs_drd->getKeteranganKoefisien());
//                            $rs_rd->setSubtitle($rs_drd->getSubtitle());
//                            $rs_rd->setKomponenHarga($rs_drd->getKomponenHarga());
//                            $rs_rd->setKomponenHargaAwal($rs_drd->getKomponenHargaAwal());
//                            $rs_rd->setKomponenName($rs_drd->getKomponenName());
//                            $rs_rd->setSatuan($rs_drd->getSatuan());
//                            $rs_rd->setPajak($rs_drd->getPajak());
//                            $rs_rd->setFromSubKegiatan($rs_drd->getFromSubKegiatan());
//                            $rs_rd->setSub($rs_drd->getSub());
//                            $rs_rd->setKodeSub($rs_drd->getKodeSub());
//                            $rs_rd->setLastUpdateUser($rs_drd->getLastUpdateUser());
//                            $rs_rd->setLastUpdateTime($rs_drd->getLastUpdateTime());
//                            $rs_rd->setLastUpdateIp($rs_drd->getLastUpdateIp());
//                            $rs_rd->setTahap($rs_drd->getTahap());
//                            $rs_rd->setTahapEdit($rs_drd->getTahapEdit());
//                            $rs_rd->setTahapNew($rs_drd->getTahapNew());
//                            $rs_rd->setStatusLelang($rs_drd->getStatusLelang());
//                            $rs_rd->setNomorLelang($rs_drd->getNomorLelang());
//                            $rs_rd->setKoefisienSemula($rs_drd->getKoefisienSemula());
//                            $rs_rd->setVolumeSemula($rs_drd->getVolumeSemula());
//                            $rs_rd->setHargaSemula($rs_drd->getHargaSemula());
//                            $rs_rd->setTotalSemula($rs_drd->getTotalSemula());
//                            $rs_rd->setLockSubtitle($rs_drd->getLockSubtitle());
//                            $rs_rd->setStatusHapus($rs_drd->getStatusHapus());
//                            $rs_rd->setTahun($rs_drd->getTahun());
//                            $rs_rd->setKodeLokasi($rs_drd->getKodeLokasi());
//                            $rs_rd->setKecamatan($rs_drd->getKecamatan());
//                            $rs_rd->setRekeningCodeAsli($rs_drd->getRekeningCodeAsli());
//                            $rs_rd->setNilaiAnggaran($rs_drd->getNilaiAnggaran());
//                            $rs_rd->setNoteSkpd($rs_drd->getNoteSkpd());
//                            $rs_rd->setNotePeneliti($rs_drd->getNotePeneliti());
//                            $rs_rd->setIsBlud($rs_drd->getIsBlud());
//                            $rs_rd->setOb($rs_drd->getOb());
//                            $rs_rd->setObFromId($rs_drd->getObFromId());
//                            $rs_rd->setIsPerKomponen($rs_drd->getIsPerKomponen());
//                            $rs_rd->setKegiatanCodeAsal($rs_drd->getKegiatanCodeAsal());
//                            $rs_rd->setThKeMultiyears($rs_drd->getThKeMultiyears());
//                            $rs_rd->setLokasiKecamatan($rs_drd->getLokasiKecamatan());
//                            $rs_rd->setLokasiKelurahan($rs_drd->getLokasiKelurahan());
//                            $rs_rd->setHargaSebelumSisaLelang($rs_drd->getHargaSebelumSisaLelang());
//                            $rs_rd->setIsMusrenbang($rs_drd->getIsMusrenbang());
//                            $rs_rd->setSubIdAsal($rs_drd->getSubIdAsal());
//                            $rs_rd->setSubtitleAsal($rs_drd->getSubtitleAsal());
//                            $rs_rd->setKodeSubAsal($rs_drd->getKodeSubAsal());
//                            $rs_rd->setSubAsal($rs_drd->getSubAsal());
//                            $rs_rd->setLastEditTime($rs_drd->getLastEditTime());
//                            $rs_rd->setIsPotongBpjs($rs_drd->getIsPotongBpjs());
//                            $rs_rd->setIsIuranBpjs($rs_drd->getIsIuranBpjs());
//                            $rs_rd->setStatusOb($rs_drd->getStatusOb());
//                            $rs_rd->setObParent($rs_drd->getObParent());
//                            $rs_rd->setObAlokasiBaru($rs_drd->getObAlokasiBaru());
//                            $rs_rd->setIsHibah($rs_drd->getIsHibah());
//                            $rs_rd->save();
//                        } else {
//                            $con = Propel::getConnection();
//                            $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail "
//                                    . " select kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, "
//                                    . " keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, "
//                                    . " pajak, unit_id, from_sub_kegiatan, sub, kode_sub, last_update_user, last_update_time, last_update_ip, "
//                                    . " tahap, tahap_edit, tahap_new, status_lelang, nomor_lelang, koefisien_semula, volume_semula, harga_semula, "
//                                    . " total_semula, lock_subtitle, status_hapus, tahun, kode_lokasi, kecamatan, rekening_code_asli, "
//                                    . " nilai_anggaran, note_skpd, note_peneliti, asal_kegiatan, is_blud, ob, ob_from_id, "
//                                    . " is_per_komponen, kegiatan_code_asal, th_ke_multiyears, lokasi_kecamatan, lokasi_kelurahan, "
//                                    . " harga_sebelum_sisa_lelang, is_musrenbang, sub_id_asal, subtitle_asal, kode_sub_asal, sub_asal, "
//                                    . " last_edit_time, is_potong_bpjs, is_iuran_bpjs, is_kapitasi_bpjs, is_iuran_jkn, is_iuran_jkk, "
//                                    . " status_ob, ob_parent, ob_alokasi_baru, is_hibah "
//                                    . " from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
//                                    . " where status_hapus = false "
//                                    . " and unit_id = '" . $unit_id . "' "
//                                    . " and kegiatan_code = '" . $kode_kegiatan . "' "
//                                    . " and detail_no = '" . $detail_no . "' ";
//                            $stmt = $con->prepareStatement($query);
//                            $stmt->executeQuery();
//                        }
//
//                        $c = new Criteria();
//                        $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
//                        $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                        $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
//                        if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
//                            $semula = $rs_rka->getNilaiAnggaran();
//                        } else {
//                            $semula = 0;
//                        }
//                        $menjadi = $rs_rd->getNilaiAnggaran();
//                        if ($semula <> $menjadi) {
//                            $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
//                            $c_log = new Criteria();
//                            $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
//                            $c_log->add(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
//                            $c_log->add(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
//                            $c_log->add(LogPerubahanRevisiPeer::TAHAP, $tahap);
//
//                            if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
//                                $log->setNilaiAnggaranSemula($semula);
//                                $log->setNilaiAnggaranMenjadi($menjadi);
//                                $log->setStatus(1);
//                                $log->save();
//                            } else {
//                                $log = new LogPerubahanRevisi();
//                                $log->setUnitId($unit_id);
//                                $log->setKegiatanCode($kode_kegiatan);
//                                $log->setDetailNo($detail_no);
//                                $log->setTahap($tahap);
//                                $log->setNilaiAnggaranSemula($semula);
//                                $log->setNilaiAnggaranMenjadi($menjadi);
//                                $log->setStatus(1);
//                                $log->save();
//                            }
//                        }
//
//                        $rs_drd->setStatusLevel(7);
//                        $rs_drd->save();
//                    }
//                }
//
//                $kumpulan_detail_no = implode('|', $pilihan);
//                $log = new LogApproval();
//                $log->setUnitId($unit_id);
//                $log->setKegiatanCode($kode_kegiatan);
//                $log->setUserId($this->getUser()->getNamaLogin());
//                $log->setKumpulanDetailNo($kumpulan_detail_no);
//                $log->setWaktu(date('Y-m-d H:i:s'));
//                $log->setSebagai('PA (RKA)');
//                $log->save();
//
//                $this->setFlash('berhasil', 'Telah berhasil memproses masuk ke dalam RKA');
//                $con->commit();
//                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//            } elseif ($this->getRequestParameter('balik')) {
//                foreach ($pilihan as $detail_no) {
//                    $c = new Criteria();
//                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
//                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
//                    $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
//                    $rs_rd->setStatusLevel(0);
//                    $rs_rd->save();
//                }
//                $this->setFlash('berhasil', 'Telah berhasil memproses ke posisi entri');
//                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//            }
//        } else {
//            if ($this->getRequestParameter('balikall')) {
//                $c = new Criteria();
//                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
//                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//                $c->add(DinasRincianDetailPeer::STATUS_LEVEL, 5);
//                $rs_rd = DinasRincianDetailPeer::doSelectOne($c);
//                foreach ($rs_rd as $item) {
//                    $item->setStatusLevel(0);
//                    $item->save();
//                }
//                $this->setFlash('berhasil', 'Telah berhasil memproses semua komponen ke posisi entri');
//                return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//            }
//            $this->setFlash('gagal', 'Mohon memberi tanda cek (v) pada komponen yang disetujui');
//            return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
//        }
//    }
//tiket #58
//23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaitingEdit() {
        $this->unit_id = $this->getRequestParameter('unitid');
        $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $this->detail_no = $this->getRequestParameter('detailno');
    }

//tiket #58
//23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaiting() {
        $sekarang = date('Y-m-d H:i:s');
        $detail_no = $this->getRequestParameter('detail_no');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $keterangan = trim($this->getRequestParameter('keterangan'));
        if (is_null($detail_no) || is_null($unit_id) || is_null($kode_kegiatan) || is_null($keterangan) || ($this->getRequest()->hasParameter('key') != md5('kembali_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
            return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else if (strlen($keterangan) < 20) {
            $this->setFlash('gagal', 'Keterangan minimal berisi 20 karakter');
            return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else {
//ambil rincian detail yang dikembalikan
            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_rincian_detail->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $c_rincian_detail->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rd = DinasRincianDetailPeer::doSelectOne($c_rincian_detail);
            $kode_rka = $rd->getUnitId() . '.' . $rd->getKegiatanCode() . '.' . $rd->getDetailNo();
            $komponen_name = $rd->getKomponenName();
            $detail_name = $rd->getDetailName();
//ambil waitinglist pu
            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::KODE_RKA, $kode_rka);
            $waiting = WaitingListPUPeer::doSelectOne($c_waiting);
            $id_waiting = $waiting->getIdWaiting();
            $kode_rka_waiting = $waiting->getUnitId() . '.' . $waiting->getKegiatanCode() . '.' . $waiting->getIdWaiting();
            $prioritas = $waiting->getPrioritas();

//ambil geojsonlokasi rev1
            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiRev1Peer::DETAIL_NO, $detail_no);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::UNIT_ID, $unit_id);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiRev1Peer::doSelect($c_cari_geojson);

//ambil geojsonlokasi waiting
            $c_hapus_geojson = new Criteria();
            $c_hapus_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, 'XXX' . $unit_id);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $hapus_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_hapus_geojson);

//ambil nama skpd
            $c_unit = new Criteria();
            $c_unit->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

//cek lelang+eproject+edelivery
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totVolumeSPK = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;
                $rd2 = new DinasRincianDetail();
                $totNilaiSwakelola = $rd2->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiKontrak = $rd2->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiRealisasi = $rd2->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                $totVolumeRealisasi = $rd2->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);
                $totVolumeSPK = $rd2->getCekVolumeSPK($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiAlokasi = $rd2->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                    //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                    //$totNilaiKontrak = $totNilaiKontrak + 10;
                }
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $totNilaiHps = $rd2->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    $lelang = $rd2->getCekLelang($unit_id, $kode_kegiatan, $detail_no, 0);
                    $ceklelangselesaitidakaturanpembayaran = $rd2->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                    $totNilaiKontrakTidakAdaAturanPembayaran = $rd2->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                }
                if (0 < $totNilaiKontrak || 0 < $totNilaiSwakelola) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                        return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    }
                } else if (0 < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                    $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if (0 < $totNilaiRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if (0 < $totVolumeRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if (0 < $totVolumeSPK) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, dengan volume SPK' . number_format($totVolumeSPK, 0, ',', '.'));
                    return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                }
            }

//begin transaction
            $con = Propel::getConnection();
            $con->begin();
            try {
//delete lokasi waiting lama
                $c_hapus_lokasi = new Criteria();
                $c_hapus_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_waiting);
                $lokasi_hapus = HistoryPekerjaanV2Peer::doSelect($c_hapus_lokasi);
                foreach ($lokasi_hapus as $lokasi_value) {
                    $lokasi_value->setStatusHapus(true);
                    $lokasi_value->save();
                }

//delete geojsonlokasi waiting
                foreach ($hapus_geojson as $value_geojson) {
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

//ambil lokasi rincian detail di history pekerjaan V2
                $c_buat_lokasi = new Criteria();
                $c_buat_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $c_buat_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                $lokasi_lama = HistoryPekerjaanV2Peer::doSelect($c_buat_lokasi);

                foreach ($lokasi_lama as $dapat_lokasi_lama) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                    $gang_lama = $dapat_lokasi_lama->getGang();
                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                    $rw_lama = $dapat_lokasi_lama->getRw();
                    $rt_lama = $dapat_lokasi_lama->getRt();
                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                    $tempat_lama = $dapat_lokasi_lama->getTempat();
                    if ($jalan_lama <> '') {
                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                    }
                    if ($tempat_lama <> '') {
                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                    }
                    if ($gang_lama <> '') {
                        $gang_fix = $gang_lama . ' ';
                    }
                    if ($nomor_lama <> '') {
                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                    }
                    if ($rw_lama <> '') {
                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                    }
                    if ($rt_lama <> '') {
                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                    }
                    if ($keterangan_lama <> '') {
                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                    }
                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

//insert lokasi waiting
                    $komponen_lokasi_fix = $rd->getKomponenName() . ' ' . $rd->getDetailName();
                    $kecamatan_lokasi_fix = $rd->getLokasiKecamatan();
                    $kelurahan_lokasi_fix = $rd->getLokasiKelurahan();
                    $lokasi_per_titik_fix = $lokasi_baru;

                    $c_insert_gis = new HistoryPekerjaanV2();
                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                    $c_insert_gis->setKodeRka($kode_rka_waiting);
                    $c_insert_gis->setStatusHapus(FALSE);
                    $c_insert_gis->setJalan(strtoupper($jalan_lama));
                    $c_insert_gis->setGang(strtoupper($gang_lama));
                    $c_insert_gis->setNomor(strtoupper($nomor_lama));
                    $c_insert_gis->setRw(strtoupper($rw_lama));
                    $c_insert_gis->setRt(strtoupper($rt_lama));
                    $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                    $c_insert_gis->setTempat(strtoupper($tempat_lama));
                    $c_insert_gis->setKomponen($komponen_lokasi_fix);
                    $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                    $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                    $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                    $c_insert_gis->save();
                    $dapat_lokasi_lama->setStatusHapus(true);
                    $dapat_lokasi_lama->save();
                }

                foreach ($dapat_geojson as $value_geojson) {

//insert geojsonlokasi waiting
                    $geojson_baru = new GeojsonlokasiWaitinglist();
                    $geojson_baru->setUnitId('XXX' . $unit_id);
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setIdWaiting($id_waiting);
                    $geojson_baru->setSatuan($rd->getSatuan());
                    $geojson_baru->setVolume($rd->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd->getKomponenName() . ' ' . $rd->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->save();

//delete geojsonlokasi rev1
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

//update prioritas
                $total_aktif = 0;
                $query = "select max(prioritas) as total "
                . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                . "where status_hapus = false and status_waiting = 0 "
                . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }
                for ($index = $prioritas; $index <= $total_aktif; $index++) {
                    $index_tambah_satu = $index + 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, 'XXX' . $unit_id);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_WAITING, 0);
                    if ($waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas)) {
                        $waiting_prio->setPrioritas($index_tambah_satu);
                        $waiting_prio->save();
                    }
                }

//update waitinglist pu
                $waiting->setSubtitle($rd->getSubtitle());
                $waiting->setKomponenId($rd->getKomponenId());
                $waiting->setKomponenName($rd->getKomponenName());
                $waiting->setKomponenRekening($rd->getRekeningCode());
                $waiting->setKomponenLokasi('(' . $lokasi_baru . ')');
                $waiting->setKomponenHargaAwal($rd->getKomponenHargaAwal());
                $waiting->setPajak($rd->getPajak());
                $waiting->setKomponenSatuan($rd->getSatuan());
                $waiting->setKoefisien($rd->getKeteranganKoefisien());
                $waiting->setVolume($rd->getVolume());
                $waiting->setTahunInput(sfConfig::get('app_tahun_default'));
                $waiting->setUpdatedAt($sekarang);
                $waiting->setKecamatan($rd->getLokasiKecamatan());
                $waiting->setKelurahan($rd->getLokasiKelurahan());
                $waiting->setIsMusrenbang($rd->getIsMusrenbang());
                $waiting->setNilaiAnggaran($rd->getNilaiAnggaran());
                $waiting->setKeterangan($keterangan);
                $waiting->setStatusHapus(false);
                $waiting->setStatusWaiting(0);
                $waiting->setKodeRka(null);
                $waiting->save();

//update rincian detail
                $rd->setStatusHapus(true);
                $rd->save();

                budgetLogger::log('Mengembalikan komponen ' . $komponen_name . ' ' . $detail_name . ' ke waitinglist');
                $con->commit();
                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' kembali ke Waiting List');
                return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            } catch (Exception $ex) {
                $con->rollback();
                //$this->setFlash('gagal', 'Gagal Karena ' . $ex->getMessage());
                $this->setFlash('gagal', 'Gagal');
                return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            }
        }
    }

//irul 29sept2015 - hapusLokasi
    public function executeHapusLokasi() {
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('no');

        $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $con = Propel::getConnection();
        $con->begin();
        try {
            $sekarang = date('Y-m-d H:i:s');
            $sql = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
            . "set status_hapus = TRUE, last_edit_time = '$sekarang' "
            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no ";
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $con->commit();
            $this->setFlash('berhasil', 'Berhasil menghapus Lokasi untuk kode ' . $kode_rka);
            budgetLogger::log('menghapus GIS untuk kode RKA  ' . $kode_rka);

            historyUserLog::hapus_lokasi($unit_id, $kegiatan_code, $detail_no);
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Hapus lokasi gagal');
            //$this->setFlash('gagal', 'Hapus lokasi gagal karena ' . $exc->getMessage());
        }
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

//irul 29sept2015 - hapusLokasi    

    public function executePindahKegiatan() {
        $act = $this->getRequestParameter('act');
        $detail_no = $this->getRequestParameter('detail_no');
        $sub_id = $this->getRequestParameter('id');
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $kegiatanTujuan = $this->getRequestParameter('kegTujuan');

        if ($kegiatanTujuan == '0003') {
            $subtitle = 'Monitoring Pembangunan/Rehab Jalan';
        } else if ($kegiatanTujuan == '0011') {
            $subtitle = 'Monitoring Pembangunan/Rehab Pematusan';
        } else if ($kegiatanTujuan == '0022') {
            $subtitle = 'Monitoring Pembangunan/Rehab Jembatan';
        } else if ($kegiatanTujuan == '0023') {
            $subtitle = 'Monitoring Pembangunan/Rehab Jaringan Air Bersih';
        } else if ($kegiatanTujuan == '0033') {
            $subtitle = 'Monitoring Pembangunan Prasarana Pematusan';
        } else if ($kegiatanTujuan == '0034') {
            $subtitle = 'Monitoring Pembangunan Jembatan';
        }


        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
//$c->add(DinasRincianDetailPeer::KODE_SUB,$sub_id);

        $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);
//print_r($unit_id.' kd '.$kegiatan_code.' dn '.$detail_no.' sub '.$sub_id.' end');
//var_dump($rincian_detail);exit;
        if ($rincian_detail) {
            $rd_function = new DinasRincianDetail();
            $newDetailNo = $rd_function->getMaxDetailNo($unit_id, $kegiatanTujuan);
            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                komponen_name, satuan, pajak, unit_id,last_update_time,  tahap_edit,status_hapus,tahun)
                values
                ('" . $kegiatanTujuan . "', '" . $rincian_detail->getTipe() . "', " . $newDetailNo . ", '" . $rincian_detail->getRekeningCode() . "', '" . $rincian_detail->getKomponenId() . "', '" . $rincian_detail->getDetailName() . "', 
                " . $rincian_detail->getVolume() . ", '" . $rincian_detail->getKeteranganKoefisien() . "', '" . $rincian_detail->getSubtitle() . "',
                " . $rincian_detail->getKomponenHarga() . ", " . $rincian_detail->getKomponenHargaAwal() . ",'" . $rincian_detail->getKomponenName() . "', '" . $rincian_detail->getSatuan() . "', 
                " . $rincian_detail->getPajak() . ",'" . $rincian_detail->getUnitId() . "','now()', '" . sfConfig::get('app_tahap_edit') . "',
                'false','" . sfConfig::get('app_tahun_default') . "')";

//print_r($query);exit;
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Komponen sudah berhasil dipindah dari kegiatan ' . $kegiatan_code . ' ke kegiatan ' . $kegiatanTujuan);
                $con->commit();
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }

            $rincian_detail->setStatusHapus('TRUE');
            $rincian_detail->save();
        }

        budgetLogger::log('Memindah komponen dari  ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
    }

    public function executeCatatanGender() {
        $this->sub_id = $this->getRequestParameter('sub_id');
        $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->catatanSubtitle = $this->getRequestParameter('catatanSubtitle');
    }

    public function executeCatatanGenderDetail() {
        $catatan = $this->getRequestParameter('catatanGender');
        $sub_id = $this->getRequestParameter('sub_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');

        $con = Propel::getConnection();
        $con->begin();
        try {
            $catatanGender = DinasSubtitleIndikatorPeer::retrieveByPK($sub_id);
            $catatanGender->setCatatan($catatan);
            $catatanGender->save($con);
            $this->setFlash('berhasil', 'Catatan Gender sudah berhasil di ubah');
            $con->commit();
        } catch (Exception $e) {
            $this->setFlash('gagal', 'Catatan gagal di ubah karena ' . $e);
            $con->rollback();
        }
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeSetPrioritas() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $prioritas = $this->getRequestParameter('prioritas');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $unit_id = $this->getRequestParameter('unit_id');
            $sub_id = $this->getRequestParameter('sub_id');
//print_r($prioritas.' kode '.$kode_kegiatan.' unit '.$unit_id.' sub id '.$sub_id);
            $c_cekPrioritas = new Criteria();
            $c_cekPrioritas->addAsColumn('prioritas', DinasSubtitleIndikatorPeer::PRIORITAS);
//$c_cekPrioritas->addSelectColumn(DinasSubtitleIndikatorPeer::PRIORITAS);
            $c_cekPrioritas->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c_cekPrioritas->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cekPrioritas->add(DinasSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
            $c_cekPrioritas->add(DinasSubtitleIndikatorPeer::PRIORITAS, $prioritas);
            $rs_cekPrioritas = DinasSubtitleIndikatorPeer::doCount($c_cekPrioritas);
//var_dump($rs_cekPrioritas);exit;
            if ($rs_cekPrioritas == '1') {
                $this->setFlash('gagal', 'Prioritas gagal dirubah, karena sudah ada prioritas yang sama pada kegiatan ini');
            } else {
                $con = Propel::getConnection();
                $con->begin();
                try {
                    $subIndikator = DinasSubtitleIndikatorPeer::retrieveByPK($sub_id);
                    $subIndikator->setPrioritas($prioritas);
                    $subIndikator->save($con);
                    $this->setFlash('berhasil', 'Prioritas sudah berhasil dirubah');
                    $con->commit();

                    historyUserLog::set_prioritas_subtitle($unit_id, $kode_kegiatan, $sub_id, $prioritas);
                } catch (Exception $e) {
                    $this->setFlash('gagal', 'Prioritas gagal dirubah karena ' . $e);
                    $con->rollback();
                }
            }
            $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }
    }

    public function executeUbahProfil() {

        $namaDinas = $this->getUser()->getNamaUser();
        $username = $this->getUser()->getNamaLogin();
        $this->unit_id = $this->getRequestParameter('unit_id');

        //echo $this->getRequest()->getMethod();

        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $namaUser = $this->getRequestParameter('nama');
            $nip = $this->getRequestParameter('nip');
            $email = $this->getRequestParameter('email');
            $telepon = $this->getRequestParameter('telepon');
            $jenisKelamin = $this->getRequestParameter('jenisKelamin');
            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $username);
            $rs_user = MasterUserV2Peer::doSelectOne($c_user);
            if ($rs_user) {
                $con = Propel::getConnection();
                $con->begin();
                try {
                    $newSetting = MasterUserV2Peer::retrieveByPK($username);
                    $newSetting->setUserName($namaUser);
                    $newSetting->setNip($nip);
                    $newSetting->setEmail($email);
                    $newSetting->setTelepon($telepon);
                    $newSetting->setJenisKelamin($jenisKelamin);
                    $newSetting->save($con);
                    budgetLogger::log('Username ' . $username . ' mengganti profil pada e-budgeting');
                    $this->setFlash('berhasil', 'Profil sudah berhasil dirubah');
                    $con->commit();
                } catch (Exception $e) {
                    $this->setFlash('gagal', 'Profil gagal dirubah karena ' . $e);
                    $con->rollback();
                }
            }
        }

        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
    }

    public function handleErrorUbahProfil() {
        return sfView::SUCCESS;
    }

    public function executeHapusHeaderPekerjaans() {
        if ($this->getRequestParameter('no')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail set sub='', kode_sub='' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
// print_r($sql3);exit;
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();
                budgetLogger::log('Menghapus Header dari Unit=' . $unit_id . ' dan kegiatan=' . $kegiatan_code . ' dan kode_sub=' . $kode_sub);
                $con->commit();
                $this->setFlash('berhasil', "Header Sudah Berhasil Dihapus");
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            } catch (Exception $e) {
                $this->setFlash('gagal', "Gagal ");
                //$this->setFlash('gagal', "Gagal karena " . $e->getMessage());
                $con->rollback();
            }
        }
    }

    public function executeEula() {
        $this->unit_id = $this->getRequestParameter('unit_id');
        if ($this->getRequestParameter('act') == 'simpan') {
//print_r($this->getRequestParameter('unit_id').' '. $this->getRequestParameter('eula').' '. $this->getRequestParameter('eula_tolak'));exit;
            if ($this->getRequestParameter('eula_tolak') == 'Tidak Setuju') {
                return $this->redirect('login/logoutDinas');
            } else if ($this->getRequestParameter('eula') == 'Setuju') {
                return $this->redirect('entri/list?menu=Belanja&unit_id=' . $this->getRequestParameter('unit_id'));
            }
        }
        $this->setLayout('layouteula');
    }
    //yogie
    public function executeAjaxEditHeader() {
        $kode_sub = $this->getRequestParameter('kodeSub');
        $unit_id = $this->getRequestParameter('unitId');
        $kode_kegiatan = $this->getRequestParameter('kegiatanCode');

        // var_dump($kode_sub);die();
        if ($this->getRequestParameter('act') == 'editHeader') {
            // var_dump($rkambr);die();
            // c_rkam
            $c_rkam = new Criteria();
            $c_rkam->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
            $c_rkam->add(DinasRkaMemberPeer::UNIT_ID, $unit_id);
            $c_rkam->add(DinasRkaMemberPeer::KEGIATAN_CODE, $kode_kegiatan);
            $rkam = DinasRkaMemberPeer::doSelectOne($c_rkam);

            if ($rkam) {
                $tamp = $rkam->getKomponenName() . ' ' . $rkam->getDetailName();
                //Kpk Fair (kosong)
                $this->rkam = $rkam->getKodeSub();
                //RKAM09545
            }
            $this->nama = $tamp;
            $this->kodeSub = $kode_sub;
            $this->unit_id = $unit_id;
            $this->kegiatan_code = $kode_kegiatan;
        }

        if ($this->getRequestParameter('act') == 'simpan') {
            $header = $this->getRequestParameter('editHeader_' . $kode_sub);
            
            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql2 = "update " . sfConfig::get('app_default_schema') . ".dinas_rka_member set komponen_name='$header', detail_name='' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                $stmt = $con->prepareStatement($sql2);
                $stmt->executeQuery();


                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail set sub='$header' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
//print_r($sql3);exit;
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();
                $con->commit();
                $this->setFlash('berhasil', "Nama Header sudah berhasil di ubah");
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }

    public function executeAjaxKomponenPenyusun() {
// print_r($this->getRequestParameter('kegiatan_code'));exit;
    }

    public function executeTampillaporan() {
        if ($this->getRequestParameter('unit_id')) {
            $this->executeBandingaslikpa();
            $this->setTemplate('bandingaslikpa');
        }
    }

    public function executeAmbilKegiatan() {//dj
        try {
            $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->user_id = $this->getRequestParameter('user_id');
            $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set user_id = '$user_id' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set user_id = '$user_id' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' diambil oleh ' . $user_id);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan berhasil di ambil oleh ( $user_id )");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('entri/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
// echo $unit_id.''.$kode_kegiatan;
    }

    public function executeLepasKegiatan() {//dj
        try {
            $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->user_id = $this->getRequestParameter('user_id');
            $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set user_id = '' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and user_id='$user_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set user_id = '' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and user_id='$user_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' dilepas  oleh ' . $user_id);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan berhasil dilepas oleh ( $user_id )");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('entri/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
// echo $unit_id.''.$kode_kegiatan;
    }

    public function executePersonil() { //dj
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = $this->subtitle = $this->getRequestParameter('subtitle');
        $act = $this->act = $this->getRequestParameter('act');

        if ($act == 'tambah') {

            $c = new Criteria();
            $c->add(PersonilRkaPeer::UNIT_ID, $unit_id);
            $c->add(PersonilRkaPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(PersonilRkaPeer::SUBTITLE, $subtitle);
            $x = PersonilRkaPeer::doSelect($c);
            $this->x = $x;
        }
    }

    public function executeTambahPersonilRka() {
//dj
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = $this->subtitle = $this->getRequestParameter('subtitle');
        $act = $this->act = $this->getRequestParameter('act');
        $nip = $this->nip = $this->getRequestParameter('nip');
        $nama = $this->nama = $this->getRequestParameter('nama');
        $buton = $this->buton = $this->getRequestParameter('addPersonil');
        $pegawai = $this->pegawai = $this->getRequestParameter('pegawai');

        $hit = count($pegawai);

        if ($buton == 'save') {
            foreach ($pegawai as $peg) {
                $d = new Criteria();
                $d->add(PegawaiPeer::NIP, $peg); //$peg adalah array yang isinya NIP
                $d->addAscendingOrderByColumn(PegawaiPeer::NIP);
                $x = PegawaiPeer::doSelect($d);
                foreach ($x as $staf) {
                    $namabaru = $staf->getNama();
                    $nipbaru = $staf->getNip();
                    $unitIdbaru = $staf->getUnitId();


                    $query = "insert into " . sfConfig::get('app_default_schema') . ".personil_rka (nip,nama,unit_id,kegiatan_code,subtitle) values('$nipbaru','$namabaru','$unitIdbaru','$kegiatan_code','$subtitle');";
//echo $query;
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    $this->setFlash('berhasil', "Personil  telah berhasi di tambahkan ke subtitle = $subtitle");
                }
            }
            return $this->redirect('entri/edit?unit_id=' . $unitIdbaru . '&kode_kegiatan=' . $kegiatan_code);
        }
        if ($act == 'deletePersonil') {
            $query = "delete from " . sfConfig::get('app_default_schema') . ".personil_rka where nip='$nip' and nama='$nama' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Personil $nama telah berhasi di dihapus dari subtitle = $subtitle");
            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executePrintBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'x.xx') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
//print_r($kode_program22);
//exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".master_program kp
            where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
            where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
            where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail rd
            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }

            $this->setLayout('kosong');
            $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
        }
    }

    public function executeBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'x.xx') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
//print_r($kode_program22);
//exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".master_program kp
            where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
            where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
            from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
            where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

//$query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail_bp rd
//               where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
//$query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".prev_rincian_detail rd
//      where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail rd
            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

//bisma $query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail rd
//        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
//$query2="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail rd
//                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."' and status_hapus=FALSE";
            $con = Propel::getConnection();

            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }
        }
    }

    public function executeUbahPass() {
        $namaDinas = $this->getUser()->getNamaUser();
        $username = $this->getUser()->getNamaLogin();

        if ($this->getRequest()->getMethod() == sfRequest::POST) {
//$namaUser=$this->getRequestParameter('nama');
//$nip=$this->getRequestParameter('nip');
            $pass_lama = $this->getRequestParameter('pass_lama');
            $pass_baru = $this->getRequestParameter('pass_baru');
            $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
            $md5_pass_lama = md5($pass_lama);
            $md5_pass_baru = md5($pass_baru);

            // Validate password strength
            $uppercase = preg_match('@[A-Z]@', $pass_baru);
            $lowercase = preg_match('@[a-z]@', $pass_baru);
            $number    = preg_match('@[0-9]@', $pass_baru);
            //$specialChars = preg_match('@[^\w]@', $pass_baru);

            if(!$uppercase || !$lowercase || !$number || strlen($pass_baru) < 8) {
                $this->setFlash('gagal', 'Mohon maaf, Inputan password minimal 8 karakter dan harus mencakup setidaknya satu huruf besar, satu angka.');
            } else {

                $c_user = new Criteria();
                $c_user->add(MasterUserV2Peer::USER_ID, $username);
                $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
                $rs_user = MasterUserV2Peer::doSelectOne($c_user);
                if ($rs_user) {
                    if ($pass_baru == $ulang_pass_baru) {
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $newSetting = MasterUserV2Peer::retrieveByPK($username);
                            $newSetting->setUserPassword($md5_pass_baru);
                            $newSetting->save($con);
                            budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                            $this->setFlash('berhasil', 'Password sudah berhasil dirubah');
                            $con->commit();
                        } catch (Exception $e) {
                            $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                            $con->rollback();
                        }
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);


    }

    public function executeUbahPassLogin() {
        $username = $this->getUser()->getNamaLogin();
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if ($this->getRequestParameter('keluar')) {

                if ($this->getUser()->hasCredential('dinas'))
                    return $this->redirect('login/logoutDinas');
                // else if ($this->getUser()->hasCredential('dewan'))
                //     return $this->redirect('login/logoutDewan');
                // else if ($this->getUser()->hasCredential('viewer'))
                //     return $this->redirect('login/logoutViewer');
                // else if ($this->getUser()->hasCredential('peneliti'))
                //     return $this->redirect('login/logoutPeneliti');
                // else if ($this->getUser()->hasCredential('data'))
                //     return $this->redirect('login/logoutData');
                else
                    return $this->redirect('login/logout');
            }
            if ($this->getRequestParameter('simpan')) {
                $pass_lama = $this->getRequestParameter('pass_lama');
                $pass_baru = $this->getRequestParameter('pass_baru');
                $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
                $md5_pass_lama = md5($pass_lama);
                $md5_pass_baru = md5($pass_baru);

                // Validate password strength
                $uppercase = preg_match('@[A-Z]@', $pass_baru);
                $lowercase = preg_match('@[a-z]@', $pass_baru);
                $number    = preg_match('@[0-9]@', $pass_baru);
                //$specialChars = preg_match('@[^\w]@', $pass_baru);

                if(!$uppercase || !$lowercase || !$number || strlen($pass_baru) < 8) {
                    $this->setFlash('gagal', 'Mohon maaf, Inputan password minimal 8 karakter dan harus mencakup setidaknya satu huruf besar, satu angka.');
                } else {
                    $c_user = new Criteria();
                    $c_user->add(MasterUserV2Peer::USER_ID, $username);
                    $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
                    $rs_user = MasterUserV2Peer::doSelectOne($c_user);
                    if ($rs_user) {
                        if ($pass_baru == $ulang_pass_baru) {
                            $con = Propel::getConnection();
                            $con->begin();
                            try {
                                $newSetting = MasterUserV2Peer::retrieveByPK($username);
                                //$newSetting->setUserName($namaUser);
                                //$newSetting->setNip($nip);
                                $newSetting->setUserPassword($md5_pass_baru);
                                $newSetting->save($con);
                                budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                                $this->setFlash('berhasil', 'Password sudah berhasil dirubah, silahkan keluar dan login kembali');
                                // $con->commit();

                                $c = new Criteria();
                                $c->add(SchemaAksesV2Peer::USER_ID, $username);
                                $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                                $cs = SchemaAksesV2Peer::doSelectOne($c);
                                $cs->setIsUbahPass(true);
                                $cs->save($con);
                                $con->commit();
                                // return $this->redirect('entri/eula?unit_id=' . $unit_id);
                            } catch (Exception $e) {
                                $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                                $con->rollback();
                            }
                        }
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
        $this->setLayout('layouteula');
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

    public function executeSetting() {
        $dinas = $this->getRequest()->getCookie('nama');
        $c = new Criteria();
        $c->add(UnitKerjaPeer::UNIT_ID, $dinas);
        $x = UnitKerjaPeer::doSelectOne($c);
        if ($x) {
            $this->kepala_nama = $x->getKepalaNama();
            $this->kepala_pangkat = $x->getKepalaPangkat();
            $this->kepala_nip = $x->getKepalaNip();
        }
        $this->set = 3;
        return sfView::SUCCESS;
    }

    public function executeSimpanset() {
        $dinas = $this->getRequest()->getCookie('nama');
        $nama_kepala = $this->getRequestParameter('kepala_nama');
        $kepala_pangkat = $this->getRequestParameter('kepala_pangkat');
        $kepala_nip = $this->getRequestParameter('kepala_nip');

        $query = "update unit_kerja set kepala_nama='" . $nama_kepala . "', kepala_pangkat='" . $kepala_pangkat . "', kepala_nip='" . $kepala_nip . "' where unit_id='" . $dinas . "'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        budgetLogger::log('Username  ' . $user_id . ' telah mengganti setting pada e-budgeting');

        $this->setFlash('berhasil', 'Data Profil SKPD telah berhasil diganti.');
        $stmt->executeQuery();
        return $this->redirect('entri/setting');
    }

    public function executeSabomedit() {
        $coded = $this->getRequestParameter('coded', '');
        return sfView::SUCCESS;
    }

    public function executeSabomlist() {

        $this->processSortsabom();

        $this->processFilterssabom();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

// pager
        $this->pager = new sfPropelPager('Komponen', 20);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'OM');

        $this->addSortCriteriasabom($c);
        $this->addFiltersCriteriasabom($c);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilterssabom() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortsabom() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {

        }
    }

    protected function addFiltersCriteriasabom($c) {
        $cek = $this->getRequestParameter('search_option');
        if ($cek == 'shsd_id') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addSortCriteriasabom($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeDetailsublist() {
        $coded = $this->getRequestParameter('coded', '');
        if ($coded != '') {
            $this->setLayout('kosong');
        }
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
//$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
        sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
        from 
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
        where 
        m.sub_kegiatan_id = '" . $sub_id . "'";
//print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
        SELECT 
        rekening.rekening_code,
        detail.detail_name as detail_name,
        detail.komponen_name,
        detail.komponen_name || ' ' || detail.detail_name as detail_name2,
        detail.komponen_harga_awal as detail_harga,
        detail.pajak,
        detail.komponen_id,
        detail.subtitle ,
        detail_no,koefisien,param,
        detail.satuan as detail_satuan,
        replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
        detail.volume * detail.komponen_harga_awal as hasil,
        (detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,


        (SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
        as x,
        (SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
        as bahaya,
        substring(kb.belanja_name,8) as belanja_name

        FROM 
        " . sfConfig::get('app_default_schema') . ".rekening rekening ,
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
        " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb

        WHERE 
        rekening.rekening_code = detail.rekening_code and
        detail.sub_kegiatan_id = '" . $sub_id . "' and 
        kb.belanja_id=rekening.belanja_id 

        ORDER BY 

        belanja_urutan,
        komponen_name

        ";
//echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
//$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
//echo $r['param'];
            $input[$i] = $inputs;
//print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
//$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeSublist() {
        $this->processSortsub();

        $this->processFilterssub();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sub_kegiatan/filters');

// pager
        $this->pager = new sfPropelPager('SubKegiatan', 25);
        $c = new Criteria();
        $c->add(SubKegiatanPeer::STATUS, 'Close');
        $this->addSortCriteriasub($c);
        $this->addFiltersCriteriasub($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processSortsub() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/sub_kegiatan/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/sub_kegiatan/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {

        }
    }

    protected function addFiltersCriteriasub($c) {
        if (isset($this->filters['sub_kegiatan_id_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_id']) && $this->filters['sub_kegiatan_id'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_id'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['sub_kegiatan_name_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_name']) && $this->filters['sub_kegiatan_name'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_name'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasub($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            $sort_column = SubKegiatanPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/sub_kegiatan/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    protected function processFilterssub() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sub_kegiatan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sub_kegiatan/filters');
        }
    }

    public function executeSabfisikedit() {
        $coded = $this->getRequestParameter('coded', '');

        return sfView::SUCCESS;
    }

    public function executeSablist() {
        $this->processSortsab();

        $this->processFilterssab();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

// pager
        $this->pager = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
        $c->add(KomponenPeer::KOMPONEN_SHOW, 'TRUE');
        $this->addSortCriteriasab($c);
        $this->addFiltersCriteriasab($c);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilterssab() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortsab() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {

        }
    }

    protected function addFiltersCriteriasab($c) {
        if (isset($this->filters['komponen_id_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
            $kata = '%' . $this->filters['komponen_id'] . '%';
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_name_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '') {
            $kata = '%' . $this->filters['komponen_name'] . '%';
            $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasab($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeHspkedit() {
        return sfView::SUCCESS;
    }

    public function executeHspklist() {
        $this->processSorthspk();

        $this->processFiltershspk();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

// pager
        $this->pager = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $this->addSortCriteriahspk($c);
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK');
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addFiltersCriteriahspk($c);


        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltershspk() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSorthspk() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {

        }
    }

    protected function addFiltersCriteriahspk($c) {
        if (isset($this->filters['komponen_id_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
            $kata = '%' . $this->filters['komponen_id'] . '%';
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->add(KomponenPeer::KOMPONEN_ID, $kata, Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_name_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '') {
            $kata = '%' . $this->filters['komponen_name'] . '%';
            $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_barang']) && $this->filters['komponen_barang'] !== '0') {
            $kode = trim($this->filters['komponen_barang'] . '%');
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kode, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriahspk($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeShsdlist() {
        $this->processSortshsd();

        $this->processFiltersshsd();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/shsd/filters');

// pager
        $this->pager = new sfPropelPager('Shsd', 20);
        $c = new Criteria();
//$this->addSortCriteria($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');
//print_r($dinas_array['nama']);exit;
        if (($dinas_array['nama'] != '0305') and ( $dinas_array['nama'] != '0600') and ( $dinas_array['nama'] != '0900')) {
//print_r($dinas_array['nama']);exit;
            $c->add(ShsdPeer::SHSD_ID, '23.01.01.05.01.08', Criteria::NOT_EQUAL);
        }
        $this->addFiltersShsdCriteria($c);
        $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersshsd() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/shsd/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/shsd/filters');
        }
    }

    protected function processSortshsd() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/shsd/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/shsd/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/shsd/sort')) {

        }
    }

    protected function addFiltersShsdCriteria($c) {
        if (isset($this->filters['shsd_id_is_empty'])) {
            $criterion = $c->getNewCriterion(ShsdPeer::SHSD_ID, '');
            $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['shsd_id']) && $this->filters['shsd_id'] !== '') {
            $kata = '%' . $this->filters['shsd_id'] . '%';
            $c->add(ShsdPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        }
        if (isset($this->filters['shsd_name_is_empty'])) {
            $criterion = $c->getNewCriterion(ShsdPeer::SHSD_NAME, '');
            $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_NAME, null, Criteria::ISNULL));
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
            $c->add($criterion);
        } else if (isset($this->filters['shsd_name']) && $this->filters['shsd_name'] !== '') {
            $kata = '%' . $this->filters['shsd_name'] . '%';
            $c->add(ShsdPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        }
    }

    public function executeRekeningList() {
        $this->processFiltersrekening();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/rekening/filters');

        $this->pager = new sfPropelPager('Rekening', 25);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $c->setDistinct();
        $this->addFiltersCriteriarekeningList($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersrekening() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/rekening/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/rekening/filters');
        }
    }

    protected function addFiltersCriteriarekeningList($c) {
        $cek = $this->getRequestParameter('search_option');

        if ($cek == 'rekening_code') {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_CODE, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_CODE, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } elseif ($cek == 'rekening_name') {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    public function executeShsdlockedlist() {
        $this->processSortshsdlocked();

        $this->processFiltersshsdlocked();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

// pager
        $this->pager = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD');
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addSortCriteriashsdlocked($c);
        $this->addFiltersCriteriashsdlocked($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');

//print_r($dinas_array['nama']);exit;
        if (($dinas_array['nama'] <> '0305') and ( $dinas_array['nama'] <> '0600') and ( $dinas_array['nama'] <> '0900')) {
            $c->add(KomponenPeer::KOMPONEN_ID, '23.01.01.05.01.08', Criteria::NOT_EQUAL);
        }
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersshsdlocked() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortshsdlocked() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {

        }
    }

    protected function addFiltersCriteriashsdlocked($c) {
        $cek = $this->getRequestParameter('search_option');
//print_r($cek);exit;
        if ($cek == 'shsd_id') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } elseif ($cek == 'shsd_name') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addSortCriteriashsdlocked($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeLihatSsh() { //fungsi mbulet sesuai mas punya mas bayu. *maafkan*
        $id = $this->getRequestParameter('komponen_id');
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $id);
        $ssh = $this->cs = KomponenPeer::doSelectOne($c);

        $shsd_id = $ssh->getShsdId();
        $c_spek = new Criteria();
        $c_spek->add(ShsdPeer::SHSD_ID, $shsd_id);
        $this->spek = SHsdPeer::doSelectOne($c_spek);
        $this->setLayout('popup');
    }

    public function executeLihatSAB() {
//$coded='';
        $coded = $this->getRequestParameter('coded', '');

        $komponen_id = $this->getRequestParameter('komponen_id');
//print_r(substr(trim($komponen_id),0,2));exit;
        if (substr(trim($komponen_id), 0, 2) <= 28) {
            return $this->redirect('entri/lihatsabfisik?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) < 30) {
            return $this->redirect('entri/lihatsublist?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) >= 30) {

            return $this->redirect('entri/lihatsabom?id=' . $komponen_id . '&coded=' . $coded);
        }
        $this->setLayout('popup');
    }

    public function executeLihatsabfisik() {
        $coded = $this->getRequestParameter('coded', '');
        $this->komponen_id = $this->getRequestParameter('komponen_id');
        $this->setLayout('popup');
    }

    public function executeLihatsublist() {
        $coded = $this->getRequestParameter('coded', '');
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
//$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
        sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
        from 
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
        where 
        m.sub_kegiatan_id = '" . $sub_id . "'";
//print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
        SELECT 
        rekening.rekening_code,
        detail.detail_name as detail_name,
        detail.komponen_name,
        detail.komponen_name || ' ' || detail.detail_name as detail_name2,
        detail.komponen_harga_awal as detail_harga,
        detail.pajak,
        detail.komponen_id,
        detail.subtitle ,
        detail_no,koefisien,param,
        detail.satuan as detail_satuan,
        replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
        detail.volume * detail.komponen_harga_awal as hasil,
        (detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,


        (SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
        as x,
        (SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
        as bahaya,
        substring(kb.belanja_name,8) as belanja_name

        FROM 
        " . sfConfig::get('app_default_schema') . ".rekening rekening ,
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
        " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb

        WHERE 
        rekening.rekening_code = detail.rekening_code and
        detail.sub_kegiatan_id = '" . $sub_id . "' and 
        kb.belanja_id=rekening.belanja_id 

        ORDER BY 

        belanja_urutan,
        komponen_name

        ";
//echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
//$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
//echo $r['param'];
            $input[$i] = $inputs;
//print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
//$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeLihatsabom() {
        $coded = $this->getRequestParameter('coded', '');
//return sfView::SUCCESS;
        $this->setLayout('popup');
    }

    public function executeSimpanSubKegiatan() {
        $sub_kegiatan_id = $this->getRequestParameter('sub_kegiatan_id');
        $kodesub = $this->getRequestParameter('kodesub');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $unit_id = $this->getRequestParameter('unit_id');
//djiebrats begin
        if ($this->getRequestParameter('cari') == 'cari') {
//print_r($sub_kegiatan_id);exit;
//print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
//$user->setAttribute('nama',$this->getRequestParameter('lokasi'),'lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan1'), 'lokasi');
            $user->setAttribute('id', $sub_kegiatan_id, 'lokasi');
            $user->setAttribute('kegiatan', $kegiatan_code, 'lokasi');
            $user->setAttribute('unit', $unit_id, 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('barusub', 'barusub', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
//djiebrats end
        if ($this->getRequestParameter('lokasipeta') == 'lokasi') {
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan'), 'lokasi');
            $user->setAttribute('id', $sub_kegiatan_id, 'lokasi');
            $user->setAttribute('kegiatan', $kegiatan_code, 'lokasi');
            $user->setAttribute('unit', $unit_id, 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('barusub', 'barusub', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpansub') == 'simpan') {
            $rd_function = new DinasRincianDetail();

            $jumlah = $this->getRequestParameter('jumlah_koef');
            $total = $this->getRequestParameter('total_sub_kegiatan');

            $perkalian_keoef = 1;
            for ($q = 0; $q < $jumlah; $q++) {
                $perkalian_keoef_baru = $this->getRequestParameter('param_' . $q);
                $perkalian_keoef = $perkalian_keoef * $perkalian_keoef_baru;
            }
//
//            echo $total . ' ' . $jumlah . ' ' . $perkalian_keoef;
//            exit();

            $array_buka_pagu_dinas_khusus = array('9999');
            $array_buka_pagu_kegiatan_khusus = array('');
            if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                $status_pagu_rincian = $rd_function->getBatasPaguPerDinasSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
            } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
            } else {
                if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                    $kecamatan_unit_id = substr($unit_id, 0, 2);
                    if (1 == 0) {
                        $status_pagu_rincian = 0;
                    } else {
                        if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
//batas pagu per dinas
                            $status_pagu_rincian = $rd_function->getBatasPaguPerDinasSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
                        } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
//batas pagu per kegiatan 
                            $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
                        }
                    }
                } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                    $status_pagu_rincian = 0;
                }
            }

//menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.
//            $status_pagu_rincian = $rd_function->getNilaiPaguDanSelisih($unit_id);
            if ($status_pagu_rincian == '1') {
                $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
//  else if ($status_pagu_rincian == '0') { 
// $this->setFlash('berhasil', 'berhasil disimpan.');
// return $this->redirect('entri/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
//end of fasilitas
            $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter where kode_sub ilike 'RSUB%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $kodesub = $rs->getString('kode_sub');
            }
            $kode = substr($kodesub, 4, 5);
            $kode+=1;
            if ($kode < 10) {
                $kodesub = 'RSUB0000' . $kode;
            } elseif ($kode < 100) {
                $kodesub = 'RSUB000' . $kode;
            } elseif ($kode < 1000) {
                $kodesub = 'RSUB00' . $kode;
            } elseif ($kode < 10000) {
                $kodesub = 'RSUB0' . $kode;
            } elseif ($kode < 100000) {
                $kodesub = 'RSUB' . $kode;
            }

            if ($this->getRequestParameter('keterangan')) {
                $m_lokasi_ada = 'tidak ada';
                $lokasi = $this->getRequestParameter('keterangan');
                $query = "select (1) as ada from v_lokasi where nama ilike '$lokasi'";
//print_r($query);exit;
                $con = Propel::getConnection(VLokasiPeer::DATABASE_NAME);
                $stmt = $con->prepareStatement($query);
                $rs_cek = $stmt->executeQuery();
                while ($rs_cek->next()) {
//print_r($m_lokasi_ada);
                    if ($rs_cek->getString('ada') == '1') {
                        $m_lokasi_ada = 'ada';
                    }
                }
            }
//print_r($m_lokasi_ada);exit;

            if ($m_lokasi_ada == 'tidak ada') {
                $this->setFlash('error_lokasi', 'Lokasi yang dimasukkan tidak ada dalam G.I.S');
                return $this->redirect("entri/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
            }

            if ($this->getRequestParameter('subtitle')) {
                $kode_sub = $this->getRequestParameter('subtitle');
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_sub);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $subtitle = $rs_subtitle->getSubtitle();
                }
            }

            $keter = $this->getRequestParameter('keterangan1') . ' ' . $this->getRequestParameter('keterangan');
            $detail_name = str_replace("'", " ", $keter);
//$subtitle = $this->getRequestParameter('subtitle');
            $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".sub_kegiatan where sub_kegiatan_id='" . $sub_kegiatan_id . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();

            while ($rs->next()) {
                $sub_kegiatan_name = $rs->getString('sub_kegiatan_name');
                $subtitle2 = $sub_kegiatan_name . " " . $keter;
                $param = $rs->getString('param');
                $satuan = $rs->getString('satuan');
                $pembagi = $rs->getString('pembagi');
                $new_subtitle = $sub_kegiatan_name . " " . $keter . " " . $subtitle;
                if (($sub_kegiatan_id != '') && ($new_subtitle != '')) {
                    $query = "select kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle' and from_sub_kegiatan='$sub_kegiatan_id' and detail_name='$detail_name'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_cek = $stmt->executeQuery();
                    while ($rs_cek->next()) {
                        if ($rs_cek->getString('kode_sub')) {
                            $kodesub = $rs_cek->getString('kode_sub');
                            $this->setFlash('error_lokasi', 'ASB Non Fisik dengan Subtitle dengan Kegiatan Sudah Ada. Mohon Diperhatikan Kembali');
                            return $this->redirect("entri/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
                        }
                    }
                }
            }
            $arrparamsub = explode("|", $param);
            $arr_pembagi = explode("|", $pembagi);
            $keterangan = '';
            $arr_input = '';
            $jumlah = count($arrparamsub);
//$j=0;
            for ($i = 0; $i < $jumlah; $i++) {
//print_r(str_replace(' ','',$arrparamsub[$i]).'pppppp<br>');
//echo $this->getRequestParameter('param_'.str_replace(' ','',$arrparamsub[$i]));

                if ($this->getRequestParameter('param_' . $i) != '') {
                    if ($i == 0) {
                        $arr_input = $this->getRequestParameter('param_' . $i);
//print_r($arr_input).'<br>';
                    }
                    if ($i != 0) {
                        $arr_input = $arr_input . '|' . $this->getRequestParameter('param_' . $i);
                    }
                }
            }
//print_r($arr_input);exit;
            $arrsatuansub = explode("|", $satuan);
//$arr_input=implode("|",$inp);
            $inp = array();
            $inp = explode("|", $arr_input);
            $arr_pembagi = explode("|", $pembagi);
            $pembagi = 1;
            $ket_pembagi = '';
//$pembagi='';
            for ($j = 0; $j < count($arrparamsub); $j++) {
                if ($j >= 0) {
                    if (isset($inp[$j])) {
                        if ($inp[$j] != 0)
                            $keterangan = $keterangan . '<tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                    }else {
                        if ((isset($arrparamsub[$j])) && (isset($arrsatuansub[$j])) && (isset($inp[$j]))) {
                            $keterangan = '<table><tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                        }
                    }
                }
                if (isset($arr_pembagi[$j])) {
                    if ($arr_pembagi[$j] == 't') {
                        if ($inp[$j] and $inp[$j] != 0)
                            $pembagi = $pembagi * $inp[$j];
                        $ket_pembagi = $ket_pembagi . "," . $arrparamsub[$j];
                    }
                }
            }
            $keterangan = $keterangan . '</table>';
            $ket_pembagi = substr($ket_pembagi, 1);

            $query = "INSERT INTO " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter(unit_id,kegiatan_code,from_sub_kegiatan,sub_kegiatan_name,subtitle,detail_name,new_subtitle,param,keterangan,ket_pembagi,pembagi,kode_sub) VALUES(
            '" . $unit_id . "','" . $kegiatan_code . "','" . $sub_kegiatan_id . "','" . $sub_kegiatan_name . "','" . $subtitle . "','" . $keter . "', '" . $new_subtitle . "','" . $arr_input . "','" . $keterangan . "','" . $ket_pembagi . "'," . $pembagi . ",'" . $kodesub . "')";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
//echo $query.'<BR>';
// budgetLogger::log($deskripsi);
            $stmt->executeQuery();

            $query = "SELECT *, satuan as satuan_asli from " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member where sub_kegiatan_id='" . $sub_kegiatan_id . "'";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
//$volume=1;
            while ($rs->next()) {
                if ($rs->getString('param') == '') {
                    $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    $volume = $rs->getString('volume');
                } else {
                    $arrparam = array();
                    $arrparam = explode("|", $rs->getString('param'));

                    $volume = $rs->getString('volume');

                    $keterangan_koefisien = '';
                    if ($rs->getString('keterangan_koefisien')) {
                        $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    }
                    for ($j = 0; $j < count($arrparam); $j++) {

                        for ($i = 0; $i < count($arrparamsub); $i++) {

                            if (str_replace(' ', '', $arrparam[$j]) == str_replace(' ', '', $arrparamsub[$i])) {

                                $volume = $volume * $inp[$i];

                                if ($keterangan_koefisien != '') {

                                    $keterangan_koefisien = $keterangan_koefisien . ' X ' . $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                } else {

                                    $keterangan_koefisien = $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                }
                            }
                        }
                    }
                }


                if (stristr($keterangan_koefisien, "'")) {
                    $keterangan_koefisien = addslashes($keterangan_koefisien);
                    $detail_name = addslashes($rs->getString('detail_name'));
                }
                $query1 = "SELECT max(detail_no) AS maxno FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                WHERE kegiatan_code='" . $kegiatan_code . "'  and unit_id='" . $unit_id . "'";


                $con = Propel::getConnection();
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
//$maxno=0;
//echo $query1;exit; 
                while ($rs1->next()) {
                    $maxno = $rs1->getInt('maxno');
                    if (!$maxno) {
                        $maxno = 1;
                    } else {
                        $maxno = $maxno + 1;
                    }
                }
                $pajakawal = $rs->getInt('pajak');

                if (!$pajakawal) {
                    $pajakawal = 0;
                }

                $rekening_code = $rs->getString('rekening_code');
                $komponen_id = $rs->getString('komponen_id');
                $detail_name = $rs->getString('detail_name');
                $komponen_harga_awal = $rs->getString('komponen_harga_awal');
                $komp = new Criteria();
                $komp->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $terpilih_komponen = KomponenPeer::doSelectOne($komp);
                if ($terpilih_komponen) {
                    $komponen_name = $terpilih_komponen->getKomponenName();
                }

                if ($this->getRequestParameter('subtitle')) {
                    $kode_sub = $this->getRequestParameter('subtitle');
                    $c = new Criteria();
                    $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_sub);
                    $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }
                }
                $user = $this->getUser();
                $dinas = $user->setAttribute('nama', '', 'dinas');

                $query2 = "
                INSERT INTO " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                (kegiatan_code,tipe,unit_id,detail_no,rekening_code,komponen_id,detail_name,volume,keterangan_koefisien,subtitle,komponen_harga,pajak,komponen_harga_awal,
                komponen_name,satuan,from_sub_kegiatan,sub,kode_sub,last_update_user,tahap_edit,tahun)
                VALUES 
                ('" . $kegiatan_code . "','SUB','" . $unit_id . "'," . $maxno . ",'" . $rekening_code . "','" . $komponen_id . "','" . $detail_name . "'," . $volume . ",'" . $keterangan_koefisien . "','" . $subtitle . "',
                " . $komponen_harga_awal * (($pajakawal + 100) / 100) . "," . $pajakawal . "," . $komponen_harga_awal . ",'" . $komponen_name . "','" . $rs->getString('satuan_asli') . "',
                '" . $sub_kegiatan_id . "','" . $new_subtitle . "','" . $kodesub . "','" . $dinas . "','" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "')
                ";

                if (($volume > 0) and ( $keterangan_koefisien != '')) {
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
//$tampQuery=$tampQuery.'<br>'.$query2;
                    $stmt->executeQuery();
                }
            }
//echo $tampQuery;exit;
            $this->setFlash('berhasil', 'Sub Kegiatan Baru Telah Tersimpan');
            return $this->redirect("entri/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kegiatan_code);
// }//eof status
        }//eof simpan
    }

    public function executePilihSubKegiatan() {
//sholeh begin
        $user = $this->getUser();
        $lokasiSession = $user->getAttribute('nama', '', 'lokasi');

        $this->lokasiSession = $lokasiSession;

        $user->setAttribute('nama', '', 'lokasi');
        $user->setAttribute('nama', '', 'lokasi_baru');
//sholeh end

        $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where kode_sub ilike 'RSUB' and sub<>''";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $kodesub = $rs->getString('kode_sub');
        }
        $kode = substr($kodesub, 4, 5);
        $kode+=1;
        if ($kode < 10) {
            $kodesub = 'RSUB0000' . $kode;
        } elseif ($kode < 100) {
            $kodesub = 'RSUB000' . $kode;
        } elseif ($kode < 1000) {
            $kodesub = 'RSUB00' . $kode;
        } elseif ($kode < 10000) {
            $kodesub = 'RSUB0' . $kode;
        } elseif ($kode < 100000) {
            $kodesub = 'RSUB' . $kode;
        }
        $this->kodesub = $kodesub;
        return sfView::SUCCESS;
    }

    public function executeHapusSubPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $tahap = '';
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            /*
              if($tahap=='')
              {
              $query = "delete from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();


              $query = "delete from ". sfConfig::get('app_default_schema') .".dinas_rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(DinasRincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
              else if($tahap!='')
              {
              $query = "update ". sfConfig::get('app_default_schema') .".dinas_rincian_detail set volume=0,keterangan_koefisien='0'
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              $con=Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "update ". sfConfig::get('app_default_schema') .".dinas_rincian_detail set sub='',kode_sub=''
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".dinas_rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(DinasRincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".dinas_rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(DinasRincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
             */


              $c = new Criteria();
              $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
              $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
              $c->add(DinasRincianDetailPeer::KODE_SUB, $kode_sub);
              $jumlah = DinasRincianDetailPeer::doCount($c);
              $volume = 0;
              $keterangan_koefisien = '0';
              $sub = '';
              $kode_sub = '';
              $kode_jasmas = '';
              $nilaiBaru = 0;
              if ($jumlah == 1) {
                $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);
                if ($rincian_detail) {
                    $detail_no = $rincian_detail->getDetailNo();
                    $pajak = $rincian_detail->getPajak();
                    $harga = $rincian_detail->getKomponenHargaAwal();
                    //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                    $nilaiBaru = $rincian_detail->getNilaiAnggaran();
//sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = '.$nilaiBaru);
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
//$rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);

                    $rincian_detail->setStatusHapus(TRUE);
//djieb: start edit from here
//melihat apakah komponen sudah terpakai belum di eproject
                    $nilaiTerpakai = 0; //default 0
                    $query = "SELECT
                    s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting, sum(dp.ALOKASI) as jml
                    from
                    " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                    " . sfConfig::get('app_default_eproject') . ".pekerjaan p,
                    " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk,
                    " . sfConfig::get('app_default_eproject') . ".kegiatan k,
                    " . sfConfig::get('app_default_eproject') . ".skpd s
                    where
                    ((p.STATE<>0 and p.STATE IS NOT NULL) or p.pernah_realisasi=1 or pernah_konfirmasi=1) and
                    dp.pekerjaan_id=p.id  and p.kegiatan_id=k.id and s.kode='$unit_id' and k.kode='$kegiatan_code' and dk.id_budgeting=$detail_no
                    and k.skpd_id=s.id and dp.detail_kegiatan_id=dk.id
                    group by s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting";
//print_r($query);exit;
//diambil nilai terpakai
                    $con = Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_eprojects = $statement->executeQuery();
                    $this->rs_eproject = $rs_eproject;
                    while ($rs_eprojects->next()) {
                        $nilaiTerpakai = $rs_eprojects->getString('jml');
                        $jumlahRows = $rs_eprojects->getRow();
                    }

                    if ($nilaiBaru < $nilaiTerpakai) {

                        $this->setFlash('gagal', 'Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah ' . number_format($nilaiTerpakai, 0, ',', '.'));
                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                    budgetLogger::log('Menghapus Sub Kegiatan dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . ' dan kode sub :' . $this->getRequestParameter('no') . ' dan detail no (' . $detail_no . '); komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else {
                    sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                    $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
            } elseif ($jumlah > 1) {
                $rincian_detail = DinasRincianDetailPeer::doSelect($c);

                $gagal = 'salah';
                foreach ($rincian_detail as $rincian_details) {
                    $detail_no = $rincian_details->getDetailNo();
                    $query = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail set status_hapus=true,   tahap_edit='" . sfConfig::get('app_tahap_edit') . "' where
                    unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
//echo $query;
                    $con = Propel::getConnection(DinasRincianSubParameterPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    budgetLogger::log('Menghapus Sub Kegiatan dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . ' dan kode sub :' . $this->getRequestParameter('no') . ' dan detail no (' . $detail_no . '); komponen_id:' . $rincian_details->getKomponenId() . '; komponen_name:' . $rincian_details->getKomponenName());
                    $statement->executeQuery();

//cek paketan
                    $d = new Criteria();
                    $d->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $d->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $d->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $cari_rincian_detail = DinasRincianDetailPeer::doSelectOne($d);
                    if ($cari_rincian_detail) {
                        /*
                          $host='pengendalian.surabaya2excellence.or.id/eproject2009';
                          $port=8080;
                          $body='';
                          $headers='';
                          $tahun=sfConfig::get('app_sinkronisasi_tahun',date('Y'));


                          $tahun=substr($tahun,2,2);//ambil 2 digit terakhir;
                          $kode_detail_kegiatan=sprintf("%s.%s.%s.%s",$unit_id,$kegiatan_code,'09',$detail_no);
                          $url='http://pengendalian.surabaya2excellence.or.id/eproject2010/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan='.$kode_detail_kegiatan;
                          sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject '.$kode_detail_kegiatan);
                          $cek_eproject=HttpHelper::httpGet($host,$port,$url,$body,$headers); //hasilnya bisa true bisa false

                          //hasil xml dari eproject masuk ke variabel body
                          $nilaiTerpakai=0;
                          $httpOK=false;
                          if ($cek_eproject && ( strpos($body,'<?xml')!==false ) && ( strpos($body,'nilai="')!==false ) )
                          {
                          $doc=new DomDocument("1.0");
                          $doc->loadXml($body);
                          $detail_kegiatan=$doc->documentElement;//root xml
                          $nilaiTerpakai=$detail_kegiatan->getAttribute('nilai');
                          $httpOK=true;
                          }
                          sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = '.$nilaiTerpakai);
                          if ($nilaiBaru<$nilaiTerpakai)
                          {
                          $gagal = 'benar';
                          }
                         */
                      }
                  }

                  if ($gagal == 'benar') {
                    $this->setFlash('gagal', 'Mohon maaf , untuk Rincian ASB Non Fisik Kegiatan ini sudah terpakai di pekerjaan');
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } elseif ($gagal == 'salah') {
//echo $keterangan_koefisien;
//print_r($rincian_detail); 
                    /*
                      $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                      $rincian_detail->setVolume($volume);
                      $rincian_detail->setDetailName($detail_name);
                      $rincian_detail->setSubtitle($subtitle);
                      $rincian_detail->setSub($sub);
                      $rincian_detail->setKodeSub($kode_sub);
                      $rincian_detail->setKecamatan($kode_jasmas);
                      $rincian_detail->save();
                     */
                    /*
                      $query="update ". sfConfig::get('app_default_schema') .".dinas_rincian_detail set volume=0, keterangan_koefisien='0 ' || satuan where
                      unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                      echo $query;
                      $con=Propel::getConnection(DinasRincianSubParameterPeer::DATABASE_NAME);
                      $statement=$con->prepareStatement($query);
                      $statement->executeQuery();
                     */
//$this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
//return $this->redirect('entri/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
                  }
              } else {
                sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }
            /*
              $query = "select *
              from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";
             */

              $query = "select *
              from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus=false order by sub,rekening_code,komponen_name";
              $con = Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
              $statement = $con->prepareStatement($query);
              $rs_rinciandetail = $statement->executeQuery();
              $this->rs_rinciandetail = $rs_rinciandetail;
              $this->id = $sub_id;
              $this->rinciandetail = 'ada';
              $this->setLayout('kosong');
          }
      }

      public function executeHapusPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $detail_no = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
            $tahap = '';
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
//$volume=0;
//$keterangan_koefisien='0';
//$sub='';
//$kode_sub='';
            $kode_jasmas = '';

            $sekarang = date('Y-m-d H:i:s');

            $ada_waitinglist = 0;
            $rd_cari_waitinglist = new Criteria();
            $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
            $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);

            if ($ada_waitinglist > 0) {
                $this->setFlash('gagal', 'Mohon maaf, rincian RKA tidak dapat dihapus karena komponen waiting list. Silahkan mengenolkan saja.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }


            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);
            if ($rincian_detail) {
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;

                $lelang = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rincian_detail->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rincian_detail->getCekLelang($unit_id, $kegiatan_code, $detail_no, 0);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rincian_detail->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrak = $rincian_detail->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiRealisasi = $rincian_detail->getCekRealisasi($unit_id, $kegiatan_code, $detail_no);
                            $totVolumeRealisasi = $rincian_detail->getCekVolumeRealisasi($unit_id, $kegiatan_code, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rincian_detail->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrakTidakAdaAturanPembayaran = $rincian_detail->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kegiatan_code, $detail_no);
                        }
                    }
                }

//irul 27 feb 2014 - hapus komponen
                if ($totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0 && $totNilaiRealisasi == 0 && $totVolumeRealisasi == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0 && $totNilaiKontrakTidakAdaAturanPembayaran == 0) {

                    //cek jika komponen gaji
                    $query = " select  count(*) as hasil from ebudget.dinas_rincian_detail where status_hapus=false and detail_gaji='$detail_kegiatan'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    if ($rs->next()) {                               
                        $hasil = $rs->getInt('hasil');                      
                    }
                    //die($detail_kegiatan.'-'.$hasil);
                    if ($hasil > 0)
                    {
                        $c1 = new Criteria();
                        $c1->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                        $c1->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                        $c1->add(DinasRincianDetailPeer::DETAIL_GAJI, $detail_kegiatan);
                        $rincian_details = DinasRincianDetailPeer::doSelect($c1);
                        foreach ($rincian_details as $key) {

                            $key->setStatusHapus('TRUE');
                            $key->setLastEditTime($sekarang);
                            $key->setTahap($tahap);                           
                            historyUserLog::hapus_komponen($unit_id, $kegiatan_code,$key->getDetailNo());
                             # code...
                            $key->save();
                         } 

                        // $key->save();
                        
                    }

                    $rincian_detail->setStatusHapus('TRUE');
                    $rincian_detail->setLastEditTime($sekarang);
                    $rincian_detail->setTahap($tahap);

                    if(strpos($rincian_detail->getKomponenName(),'Gaji Pokok') !== false)
                    { 
                        $this->setFlash('berhasil', 'Komponen ' . $rincian_detail->getKomponenName() . ' dan Tunjangannya berhasil dihapus ');
                    }
                    else
                    $this->setFlash('berhasil', 'Komponen ' . $rincian_detail->getKomponenName() . ' berhasil dihapus ');
                    //budgetLogger::log('Menghapus komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
//irul 10 september 2014 - status hapus data gmap diset true
                    $con = Propel::getConnection();
                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                    . "set status_hapus = true, last_edit_time = '$sekarang' "
                    . "where unit_id='" . $rincian_detail->getUnitId() . "' and kegiatan_code='" . $rincian_detail->getKegiatanCode() . "' and detail_no ='" . $rincian_detail->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
//irul 10 september 2014 - status hapus data gmap diset true

                    $rincian_detail->save();


                    historyUserLog::hapus_komponen($unit_id, $kegiatan_code, $detail_no);

                    $kode_rka_fix = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                    $c_update_history = new Criteria();
                    $c_update_history->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                    $dapat_history = HistoryPekerjaanV2Peer::doSelectOne($c_update_history);
                    if ($dapat_history) {
                        $dapat_history->setStatusHapus(TRUE);
                        $dapat_history->save();
                    }

                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else {
                    if ($totNilaiKontrak > 0 || $totNilaiSwakelola > 0) {
                        if ($totNilaiKontrak == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        } else if ($totNilaiSwakelola == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiKontrak, 0, ',', '.'));
                        } else {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        }
                    } else if ($totNilaiAlokasi > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah dialokasikan di eProject sebesar Rp ' . number_format($totNilaiAlokasi, 0, ',', '.'));
                    } else if ($lelang > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, Komponen sedang dalam proses lelang');
                    } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                        $this->setFlash('gagal', 'Proses lelang selesai + Belum ada Aturan Pembayaran. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                        $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    } else if ($totNilaiRealisasi > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    } else if ($totVolumeRealisasi > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery dengan volume ' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    }
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
//irul 27 feb 2014 - hapus komponen
            }
        } else {
//sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = '.$nilaiBaru};
            $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeBaruKegiatan() {
//print_r($this->getRequest());exit;
        if ($this->getRequestParameter('cari') == 'cari') {
//print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('sub', $this->getRequestParameter('sub'), 'lokasi');
            $user->setAttribute('pajak', $this->getRequestParameter('pajak'), 'lokasi');
            $user->setAttribute('rekening', $this->getRequestParameter('rekening'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('baru', 'baru', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simbada') == 'simbada') {
//print_r($this->getRequestParameter('simbada'));exit;
            $user = $this->getUser();
            $user->removeCredential('simbada');
            $user->addCredential('simbada');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan'), 'simbada');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'simbada');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'simbada');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'simbada');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'simbada');
            $user->setAttribute('sub', $this->getRequestParameter('sub'), 'simbada');
            $user->setAttribute('pajak', $this->getRequestParameter('pajak'), 'simbada');
            $user->setAttribute('rekening', $this->getRequestParameter('rekening'), 'simbada');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'simbada');
            $user->setAttribute('baru', 'baru', 'simbada');

//return $this->forward('simbada','list');
            return $this->forward('perlengkapan ', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $subtitle = $this->getRequestParameter('subtitle');
            $sub = $this->getRequestParameter('sub');
            $tipe = $this->getRequestParameter('tipe');
            $rekening = $this->getRequestParameter('rekening');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('id');
            $isBlud = $this->getRequestParameter('blud');
            $isKapitasi = $this->getRequestParameter('kapitasi');
            $isBos = $this->getRequestParameter('bos');
            $isBopda = $this->getRequestParameter('bopda');
            $isMusrenbang = $this->getRequestParameter('musrenbang');
            $sisaLelang = $this->getRequestParameter('lelang');
            $isHibah = $this->getRequestParameter('hibah');
            $akrual_code = $this->getRequestParameter('akrual_code');
            $satuanx = $this->getRequestParameter('satuanx');
            $volume_orang = $this->getRequestParameter('volume_orang');
            $detail_name = $this->getRequestParameter('keterangan');   
            $sumber_dana_id= 11;      
            $jumlah_suami_istri = $this->getRequestParameter('jumlah_suami_istri');  
            $jumlah_anak = $this->getRequestParameter('jumlah_anak');  
            $accres = 0;
            $komponen_name = $this->getRequestParameter('komponen_name');
            $harga = $this->getRequestParameter('harga');

            // bypass validasi devplan per nilai belanja
            $bypass_devplan = TRUE;
            // if(sfConfig::get('app_tahap_edit') == 'pak'){
            //     if (in_array($unit_id, $array_skpd) && (in_array($kegiatan_code, $array_kegiatan))) {
            //         $bypass_devplan = TRUE;
            //     }
            // }            
            $arr_buka_approve_jk = array('');
            $query = 
            "SELECT kegiatan_code
            FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_jk";
            $con_jk = Propel::getConnection();
            $stmt = $con_jk->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) array_push($arr_buka_approve_jk, $rs->getString('kegiatan_code'));

            if(!in_array($kegiatan_code, $arr_buka_approve_jk)) {
                $arr_iuran_jk = array("2.1.1.01.01.01.004.016", "2.1.1.01.01.01.004.017", "2.1.1.01.01.01.004.018");
                if(in_array($komponen_id, $arr_iuran_jk)) {
                    $c = new Criteria();
                    $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $subtitle);
                    $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);

                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(DinasRincianDetailPeer::SUBTITLE, '%'.$rs_subtitle->getSubtitle().'%', Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::KOMPONEN_ID, $komponen_id);
                    $c->add(DinasRincianDetailPeer::REKENING_CODE, $rekening);
                    $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                    $rs_count_komponen = DinasRincianDetailPeer::doCount($c);

                    if($rs_count_komponen > 0) {
                        $c = new Criteria();
                        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                        $data_komponen = KomponenPeer::doSelectOne($c);

                        $this->setFlash('gagal', 'Mohon maaf, komponen '.$data_komponen->getKomponenName().' sudah terambil di kegiatan dan rekening ini, silahkan edit komponen tersebut.');
                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                }
            }

            //tutup sementara
            // $koRek = substr($komponen_id , 0, 18);
            // if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($satuanx == 'Orang Bulan') and !$volume_orang )
            // {
            //     $this->setFlash('gagal', 'Volume tenaga kontrak per komponen harus diisi');
            //     return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            // }

            // if(($rekening == '5.1.02.02.01.080' or $rekening == '5.2.1.02.02') and ($satuanx == 'Orang Bulan' or $satuanx == 'OrangBulan' or $satuanx == 'Orang/Bulan') and !$volume_orang)
            // {
            //     $this->setFlash('gagal', 'Volume tenaga kontrak per komponen harus diisi');
            //     return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            // }
            

            $koRek = substr($komponen_id , 0, 18);
            if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($satuanx == 'Orang Bulan'))
            {
                // $volume_orang = $this->getRequestParameter('volume_orang');
                $volume_orang = 0;
            }           
            else
            {
               $volume_orang = 0;
           }

           $c = new Criteria();
           $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
           if ($rs_est_fisik = KomponenPeer::doSelectOne($c)) {
            $est_fisik = $rs_est_fisik->getIsEstFisik();
            $tipe2 = $rs_est_fisik->getKomponenTipe2();
        }

        $lokasi_baru = '';
        $lokasi_array = array();

        if (!$this->getRequestParameter('subtitle')) {
            $this->setFlash('gagal', 'Subtitle Belum Dipilih');
            return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
        }
#51 - GMAP simpan
//            if ($tipe == 'FISIK' && !$this->getRequestParameter('lokasi')) {
//                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
//                return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//            }
#51 - GMAP simpan            
        if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) && (!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan') )) {
            $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
            return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
        }
#51 - GMAP simpan
        if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) && !$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
            $this->setFlash('gagal', 'Lokasi Belum Dipilih');
            return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
        }

        $ada_fisik = 'FALSE';
        if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) ) {

            $ada_fisik = 'TRUE';
            $keisi = 0;

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
            }

//                    buat baru
            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }

                    if ($keisi == 0) {
                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    } else {
                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    }
                }
            }
            if ($keisi == 0) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }
        }

            //validasi 1 komponen dalam 1 subtitle 
        $arr_buka_komp_sub = array();           

        if(!in_array($subtitle, $arr_buka_komp_sub))
        {
            if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                $detail_name = $lokasi_baru;
            } else if ($tipe2 != 'KONSTRUKSI' && $tipe != 'FISIK' && !$est_fisik) {
                $detail_name = $this->getRequestParameter('keterangan');      
            }
            $c1 = new Criteria();
            $c1->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c1->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $c1->add(DinasSubtitleIndikatorPeer::SUB_ID, $subtitle);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c1);

            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianDetailPeer::SUBTITLE, '%'.$rs_subtitle->getSubtitle().'%', Criteria::ILIKE);
            $c->add(DinasRincianDetailPeer::REKENING_CODE, $rekening);           
            $c->add(DinasRincianDetailPeer::KOMPONEN_ID, $komponen_id);         
            $c->add(DinasRincianDetailPeer::VOLUME, 0, Criteria::GREATER_THAN);       
            $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->add(DinasRincianDetailPeer::DETAIL_NAME, '%'.$detail_name.'%', Criteria::ILIKE);            
            $rs_count_komponen = DinasRincianDetailPeer::doCount($c);

            if($rs_count_komponen > 0) 
            {

                $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $data_komponen = KomponenPeer::doSelectOne($c);

                $this->setFlash('gagal', 'Mohon maaf, komponen '.$data_komponen->getKomponenName().' sudah terambil di subtitle '.$rs_subtitle->getSubtitle().' dan hanya 1 komponen saja yang diperbolehkan di dalam 1 subtitle kegiatan, silahkan edit komponen tersebut sesuai dengan volume dan nilai anggaran yang dibutuhkan.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

        }

#51 - GMAP simpan            
//irul - ambil tahap tabel master_kegiatan
        $c_master_kegiatan = new Criteria();
        $c_master_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c_master_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
        $rs_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_master_kegiatan);
        $tahap = $rs_master_kegiatan->getTahap();
//irul - ambil tahap tabel master_kegiatan
//        irul 31januari2015 - fungsi set BPJS
//            $isPotongBpjs = $this->getRequestParameter('is_potong_bpjs');
//            $isIuranBpjs = $this->getRequestParameter('is_iuran_bpjs');
        $c_cek_komponen = new Criteria();
        $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $data_komponen = KomponenPeer::doSelectOne($c_cek_komponen);

        $potongBPJS = 'FALSE';
        $iuranBPJS = 'FALSE';
        $ini_komponen_jkn = 'FALSE';
        $ini_komponen_jkk = 'FALSE';
        $is_persediaan = 'FALSE';
        $is_narsum = 'FALSE';
        $is_rab = 'FALSE';
        $is_bbm = 'FALSE';
        $is_koma= 'FALSE';
        $is_pengecualian ='FALSE';

        if ($data_komponen) {
            if ($data_komponen->getIsPotongBpjs() == true) {
                $potongBPJS = 'TRUE';
            } else {
                $potongBPJS = 'FALSE';
            }

            if ($data_komponen->getIsIuranBpjs() == true) {
                $iuranBPJS = 'TRUE';
            } else {
                $iuranBPJS = 'FALSE';
            }

            if ($data_komponen->getIsIuranJkn() == true) {
                $ini_komponen_jkn = 'TRUE';
            } else {
                $ini_komponen_jkn = 'FALSE';
            }

            if ($data_komponen->getIsIuranJkk() == true) {
                $ini_komponen_jkk = 'TRUE';
            } else {
                $ini_komponen_jkk = 'FALSE';
            }

            if ($data_komponen->getIsNarsum() == true) {
                $is_narsum = 'TRUE';
            } else {
                $is_narsum = 'FALSE';
            }

            if ($data_komponen->getIsPengecualian() == true) {
                $is_pengecualian = 'TRUE';
            } else {
               $is_pengecualia = 'FALSE';
           }
                // if ($data_komponen->getIsRab() == true) {
                //     $is_rab = 'TRUE';
                // } else {
                //     $is_rab = 'FALSE';
                // }

           if ($data_komponen->getIsBbm() == true) {
            $is_bbm = 'TRUE';
        } else {
            $is_bbm = 'FALSE';
        }

                //tambahan set satuan koma
        $satuan_koma = array("%","Bulan","Cm","Cm2","Gram","Ha","Jam","Kg","Kwh","Liter","Lsp","Lusin","M","M2","M2/Bulan","M2/Hari","M3","M3/Hari","Menit","Ons","Poin","Rp","Rp/Km","Ton","Ton.Km","VA","Watt");
        if (in_array($data_komponen->getSatuan(), $satuan_koma)) {
            $is_koma = 'TRUE';
        } else {
            $is_koma = 'FALSE';
        }
                // if((strpos($data_komponen->getRekening(), '5.2.1.02.02') !== FALSE || strpos($data_komponen->getRekening(), '5.1.02.02.01.080') !== FALSE) && ($data_komponen->getSatuan() == 'Orang Bulan' || $data_komponen->getSatuan() == '%')) {
                //     if(strpos(strtolower($data_komponen->getKomponenName()), 'jk') === FALSE
                //     && strpos(strtolower($data_komponen->getKomponenName()), 'anggota tim') === FALSE
                //     && strpos(strtolower($data_komponen->getKomponenName()), 'ketua tim') === FALSE
                //     && strpos(strtolower($data_komponen->getKomponenName()), 'anggota kominda') === FALSE
                //     && strpos(strtolower($data_komponen->getKomponenName()), 'wakil ketua/sekretaris tim') === FALSE
                //     && strpos(strtolower($data_komponen->getKomponenName()), 'honorarium ketua dewan pengawas blud') === FALSE) {
                //         $potongBPJS = 'TRUE';
                //         $iuranBPJS = 'FALSE';
                //         $ini_komponen_jkn = 'FALSE';
                //         $ini_komponen_jkk = 'FALSE';
                //     } else if($komponen_id == '23.01.01.04.18') {
                //         $potongBPJS = 'FALSE';
                //         $iuranBPJS = 'TRUE';
                //         $ini_komponen_jkn = 'FALSE';
                //         $ini_komponen_jkk = 'TRUE';
                //     } else if($komponen_id == '23.01.01.04.17') {
                //         $potongBPJS = 'FALSE';
                //         $iuranBPJS = 'TRUE';
                //         $ini_komponen_jkn = 'TRUE';
                //         $ini_komponen_jkk = 'FALSE';
                //     } else if($komponen_id == '23.01.01.04.19') {
                //         $potongBPJS = 'FALSE';
                //         $iuranBPJS = 'TRUE';
                //         $ini_komponen_jkn = 'FALSE';
                //         $ini_komponen_jkk = 'FALSE';
                //     }
                // } else {
                //     $potongBPJS = 'FALSE';
                // }

                // persediaan dan rab 061219
        $query_persediaan =
        "(SELECT kode
        FROM " . sfConfig::get('app_default_eproject') . ".rekening
        WHERE is_persediaan = TRUE)
        UNION
        (SELECT rekening_code
        FROM " . sfConfig::get('app_default_schema') . ".rekening
        WHERE rekening_code ILIKE '5.1.02.%')";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query_persediaan);
        $rs_persediaan = $stmt->executeQuery();
        $arr_persediaan = array();
        while($rs_persediaan->next()) {
            array_push($arr_persediaan, $rs_persediaan->getString('kode'));
        }

        if(in_array($rekening, $arr_persediaan)) {
            $is_persediaan = 'TRUE';
            if($data_komponen->getSatuan() == 'Paket')
                $is_rab = 'TRUE';
        }
    } else {
        $this->setFlash('gagal', 'Komponen Tidak Ada');
        return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
    }
//        irul 31januari2015 - fungsi set BPJS
//            tambahan untuk cek JKN atau JKK
            // if ($komponen_id == '23.01.01.04.17') {
            //     $ini_komponen_jkn = 'TRUE';
            // } else {
            //     $ini_komponen_jkn = 'FALSE';
            // }
            // if ($komponen_id == '23.01.01.04.18') {
            //     $ini_komponen_jkk = 'TRUE';
            // } else {
            //     $ini_komponen_jkk = 'FALSE';
            // }
//            tambahan untuk cek JKN atau JKK
// irul 18maret 2014 - simpan catatan
    $note_skpd = '';
    $note_skpd = $this->getRequestParameter('catatan');
    $apakah_murni = 0;
    if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'tutup') {
        $apakah_murni = 1;
    } else {
        $apakah_murni = 0;
    }

//            <?php if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka') {

    if (strlen(str_replace(' ', '', $note_skpd)) < 15 && $apakah_murni == 0) {
        $this->setFlash('gagal', 'Mohon maaf, Inputan catatan usulan Anggaran minimal 15 karakter');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    } elseif (strlen(str_replace(' ', '', $note_skpd)) < 15 && $apakah_murni == 0) {
        $this->setFlash('gagal', 'Mohon maaf, Inputan Catatan Pergeseran Anggaran minimal 15 karakter');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }
// irul 18maret 2014 - simpan catatan   

    if ($isMusrenbang == 1) {
        $isMusrenbang = 'TRUE';
    } else if ($isMusrenbang <> 1) {
        $isMusrenbang = 'FALSE';
    }
    $this->isMusrenbang = $isMusrenbang;

    if ($sisaLelang) {
        $isLelang = 'TRUE';
        $sisaLelang = 1;
    } else {
        $isLelang = 'FALSE';
        $sisaLelang = 0;
    }
    $this->isLelang = $isLelang;            

    if ($isHibah == 1) {
        $isHibah = 'TRUE';
    } else if ($isHibah <> 1) {
        $isHibah = 'FALSE';
    }
    $this->isHibah = $isHibah;

    if ($isBlud == 1 || strpos($note_skpd, '(F)') !== FALSE) {
        $isBlud = 'TRUE';
        $sumber_dana_id= 6;
        $this->isBlud = $isBlud;
    } else if ($isBlud <> 1) {
        $isBlud = 'FALSE';
        $this->isBlud = $isBlud;
    }
    if ($isKapitasi == 1) {
        $isKapitasi = 'TRUE';
        $this->isKapitasi = $isKapitasi;
    } else if ($isKapitasi <> 1) {
        $isKapitasi = 'FALSE';
        $this->isKapitasi = $isKapitasi;
    }
    if ($isBos == 1) {
        $isBos = 'TRUE';
        $this->isBos = $isBos;
    } else if ($isBos <> 1) {
        $isBos = 'FALSE';
        $this->isBos = $isBos;
    }
    if ($isBopda == 1) {
        $isBopda = 'TRUE';
        $this->isBopda = $isBopda;
    } else if ($isBopda <> 1) {
        $isBopda = 'FALSE';
        $this->isBopda = $isBopda;
    }

    if ($isPotongBPJS == 1) {
        $isPotongBPJS = 'TRUE';
        $this->isPotongBPJS = $isPotongBPJS;
    } else if ($isPotongBPJS <> 1) {
        $isPotongBPJS = 'FALSE';
        $this->isPotongBPJS = $isPotongBPJS;
    }
//begin of kecamatan kelurahan
    if ($this->getRequestParameter('kecamatan')) {
        $lokasi_kec = $this->getRequestParameter('kecamatan');
        $lokasi_kel = $this->getRequestParameter('kelurahan');
        $kec = new Criteria();
        $kec->add(KecamatanPeer::ID, $lokasi_kec);
        $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
        $rs_kec = KecamatanPeer::doSelectOne($kec);
        if ($rs_kec) {
            $kecamatan = $rs_kec->getNama();
        }

        $kel = new Criteria();
        $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
        $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
        $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
        if ($rs_kel) {
            $kelurahan = $rs_kel->getNamaKelurahan();
//                          $rincian_detail->setLokasiKecamatan($kecamatan);
//                          $rincian_detail->setLokasiKelurahan($kelurahan);
        } else {
            $kecamatan = '';
            $kelurahan = '';
//                            $rincian_detail->setDetailName($detail_name);
        }
                //echo $lokasi_kec.'--'.$lokasi_kel.'--'.$kecamatan.'--'.$kelurahan;exit;
    }

    if (strpos($this->getRequestParameter('vol1'), ',') || strpos($this->getRequestParameter('vol2'), ',') || strpos($this->getRequestParameter('vol3'), ',') || strpos($this->getRequestParameter('vol4'), ',')) {
        $this->setFlash('gagal', 'Mohon maaf, Pengisian Pecahan pada Volume Menggunakan Titik.');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

    $vol1 = $this->getRequestParameter('vol1');
    $sat1 = $this->getRequestParameter('volume1');
    if (!(empty($vol1) || $vol1 == 0) && empty($sat1)) {
        $this->setFlash('gagal', 'Mohon maaf, Satuan Koefisien Tidak Boleh Kosong.');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }
    if(empty($sat1)) {
        $this->setFlash('gagal', 'Mohon maaf, Satuan Koefisien Tidak Boleh Kosong.');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

            // berikan alert jika komponen PSP belum dimanfaatkan, sesuai dengan permintaan
    if(!$sisaLelang || !isset($sisaLelang) || empty($sisaLelang)) {
        $query =
        "SELECT
        drd.kegiatan_code,
        drd.unit_id,
        drd.detail_no,
        drd.komponen_id,
        drd.komponen_name,
        drd.detail_name,
        rka.nilai_anggaran - drd.nilai_anggaran AS sisa,
        drd.detail_kegiatan
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
        INNER JOIN " . sfConfig::get('app_default_schema') . ".rincian_detail rka ON drd.unit_id = rka.unit_id AND drd.kegiatan_code = rka.kegiatan_code AND drd.detail_no = rka.detail_no
        WHERE drd.unit_id = '$unit_id'
        AND drd.satuan = 'Paket'
        AND drd.volume = 1
        AND drd.status_lelang = 'lock'
        AND drd.status_hapus = FALSE
        AND rka.nilai_anggaran > drd.nilai_anggaran";
        //AND drd.kegiatan_code = '$kegiatan_code'sub kg tutup ganti per PD
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_sisapengadaan = $stmt->executeQuery();

        $sisa_pengadaan = 0;
        while ($rs_sisapengadaan->next()) {
            $sisa_pengadaan += $rs_sisapengadaan->getFloat('sisa');
        }

        $query =
        "SELECT SUM(sisa_anggaran) AS nilai_hpsp
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
        WHERE unit_id = '".$unit_id."'";
        //AND kegiatan_code = '".$kegiatan_code."'"; sub kg tutup ganti per PD
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_total_hpsp = $stmt->executeQuery();

        $nilai_hpsp = 0;
        if($rs_total_hpsp->next())
            $nilai_hpsp += $rs_total_hpsp->getFloat('nilai_hpsp');

        if($sisa_pengadaan - $nilai_hpsp > sfConfig::get('app_default_bataspsp')) {
             $this->setFlash('gagal', 'Mohon maaf, masih ada komponen PSP yang belum ada pemanfaatannya Sebesar.'.$sisa_pengadaan.' - '.$nilai_hpsp.'='.$sisa_pengadaan-$nilai_hpsp);
            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    if($sisaLelang) {
        $total = $this->getRequestParameter('total');
        $totalLelang = 0;

        $isi_chx = $this->getRequestParameter('chkPSP');
        foreach ($isi_chx as $value) {
            $sisa_anggaran = $this->getRequestParameter($value);
            $totalLelang += $sisa_anggaran;
        }

                // if($totalLelang != round($total) && $totalLelang != $total) {
                //     $this->setFlash('gagal', 'Mohon maaf, Pengisian Anggaran Sisa Pengadaan Harus Sama Nominalnya dengan Total Anggaran Komponen.');
                //     return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                // }

        if(!isset($totalLelang) || empty($totalLelang) || $totalLelang < 0) {
            $this->setFlash('gagal', 'Mohon maaf, pengisian anggaran sisa pengadaan tidak boleh kurang dari 0.');
            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }

        $lebihDariLelang = $this->getRequestParameter('lebihDariLelang');
        if($lebihDariLelang) {
            $this->setFlash('gagal', 'Mohon maaf, Pengisian Anggaran Sisa Pengadaan Tidak Boleh Lebih Dari Nominal Sisa Pengadaan Sebenarnya.');
            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }
//print_r($sub);exit;
//untuk komponen penyusun $pegawai=$this->pegawai=$this->getRequestParameter('pegawai');
    $komponen_penyusun = $this->getRequestParameter('penyusun');
//print_r($this->getRequestParameter('penyusun'));exit;

    $volumePenyusun = array();
    foreach ($komponen_penyusun as $penyusun) {
        $penyusun_id = $this->getRequestParameter('komponenPenyu_' . $penyusun);
        $cekPenyusun = new Criteria();
        $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun_id);
        $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
        foreach ($rs_cekPenyusun as $komPenyusun) {
            $nilaiDariWeb = $this->getRequestParameter('volPenyu_' . $penyusun);
            $volumePenyusun[$komPenyusun->getKomponenId()] = $nilaiDariWeb;
        }
    }

    $user = $this->getUser();
    $dinas = sfContext::getInstance()->getUser()->getNamaLogin();

    $vol1 = str_replace(' ', '', $this->getRequestParameter('vol1'));
    $vol2 = str_replace(' ', '', $this->getRequestParameter('vol2'));
    $vol3 = str_replace(' ', '', $this->getRequestParameter('vol3'));
    $vol4 = str_replace(' ', '', $this->getRequestParameter('vol4'));
//menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.
    $rd_function = new DinasRincianDetail();

    $status_pagu_uk = 0;
            // $array_buka_pagu_uk_khusus = array();
            // if (!in_array($unit_id, $array_buka_pagu_uk_khusus)) {
            //     if ($rekening == '5.1.02.02.01.080') {
            //         $status_pagu_uk = $rd_function->getBatasPaguPerDinasUK($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
            //         $nilai_maks_uk = $rd_function->getNilaiPaguUKMaks($unit_id);
            //         if ($status_pagu_uk == 1) {
            //             $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks UK untuk SKPD Anda sebesar ' . number_format($nilai_maks_uk));
            //             return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            //         }
            //     }
            // }


            // kunci pagu uk per kegiatan

    $array_buka_pagu_uk_kegiatan_khusus = array('');
    if (!in_array($unit_id, $array_buka_pagu_uk_kegiatan_khusus)) {
     if ($rekening == '5.1.02.02.01.080') {
         $status_pagu_uk = $rd_function->getBatasPaguPerKegiatanUk($unit_id, $kegiatan_code ,$komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
         $nilai_maks_uk = $rd_function->getNilaiPaguUkMaksKegiatan($unit_id, $kegiatan_code);
         if ($status_pagu_uk == 1) {
             $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai maksimal uang kinerja untuk OPD anda pada kegiatan ini sebesar ' . number_format($nilai_maks_uk));
             return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
         }
     }
 }

            //validasi devplan
            // if($tahap == "pak" && !$bypass_devplan){
            //     $kode_belanja = substr($rekening, 0, 5);
            //     $c = new Criteria();
            //     $c->add(KuaKegiatanPeer::UNIT_ID, $unit_id);
            //     $c->add(KuaKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            //     $rs_belanja_devplan = KuaKegiatanPeer::doSelectOne($c);
            //     if ($kode_belanja == '5.2.1') {
            //         $nama_belanja = 'Pegawai';
            //         $nilai_maks_belanja = $rs_belanja_devplan->getPaguPegawai();
            //     } elseif ($kode_belanja == '5.2.2') {
            //         $nama_belanja = 'Barang dan Jasa';
            //         $nilai_maks_belanja = $rs_belanja_devplan->getPaguBarjas();
            //     } elseif ($kode_belanja == '5.2.3') {
            //         $nama_belanja = 'Modal';
            //         $nilai_maks_belanja = $rs_belanja_devplan->getPaguModal();
            //     }
            //     $status_pagu_belanja_devplan = $rd_function->getBatasPaguBelanjaDevplan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun, $kode_belanja);
            //     if ($status_pagu_belanja_devplan == 1) {
            //         $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Belanja ' . $nama_belanja . ' untuk SKPD Anda sebesar ' . number_format($nilai_maks_belanja));
            //         return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            //     }
            // }
            //validasi devplan

//            $status_pagu_mamin = 0;
//            $array_buka_pagu_mamin_khusus = array();
//            if (!in_array($unit_id, $array_buka_pagu_mamin_khusus)) {
//                if (substr($rekening, 0, 8) == '5.2.2.12') {
//                    $status_pagu_mamin = $rd_function->getBatasPaguPerDinasMamin($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
//                    $nilai_maks_mamin = $rd_function->getNilaiPaguMaminMaks($unit_id);
//                    if ($status_pagu_mamin == 1) {
//                        $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Makan dan Minum untuk SKPD Anda sebesar ' . number_format($nilai_maks_mamin));
//                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
//                    }
//                }
//            }
//            $status_pagu_atk = 0;
//            $array_buka_pagu_atk_khusus = array();
//            if (!in_array($unit_id, $array_buka_pagu_atk_khusus)) {
//                if ($rekening == '5.2.2.01.01') {
//                    $status_pagu_atk = $rd_function->getBatasPaguPerDinasAtk($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
//                    $nilai_maks_atk = $rd_function->getNilaiPaguAtkMaks($unit_id);
//                    if ($status_pagu_atk == 1) {
//                        $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Alat Tulis Kantor untuk SKPD Anda sebesar ' . number_format($nilai_maks_atk));
//                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
//                    }
//                }
//            }
//irul 19juli 2014 - ENTRI BUDGET 2015 BATAS PAGU DINAS REQUEST    

// entri batas pagu atk per kegiatan
            //'3100','3200','2101'
        // $array_buka_pagu_atk_kegiatan_khusus = array();
        //     if (!in_array($unit_id, $array_buka_pagu_atk_kegiatan_khusus)) {
        //        if ($rekening == '5.2.2.01.01') {
        //            $status_pagu_atk = $rd_function->getBatasPaguPerKegiatanAtk($unit_id, $kegiatan_code ,$komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
        //            $nilai_maks_atk = $rd_function->getNilaiPaguAtkMaksKegiatan($unit_id, $kegiatan_code);
        //            if ($status_pagu_atk == 1) {
        //                $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Alat Tulis Kantor untuk SKPD Anda pada kegiatan ini sebesar ' . number_format($nilai_maks_atk));
        //                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        //             }
        //         }
        //     }
            // $array_buka_pagu_dinas = array('');
            //     $query =
            //     "SELECT distinct unit_id
            //     FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id ilike '12%";
            //     $con_jk = Propel::getConnection();
            //     $stmt = $con_jk->prepareStatement($query);
            //     $rs = $stmt->executeQuery();
            //     while ($rs->next()) array_push($array_buka_pagu_dinas, $rs->getString('unit_id'));
            //BUKA kelurahan sementara 2021

 $array_buka_pagu_dinas_khusus = array('');
 $array_buka_pagu_kegiatan_khusus = array('');
 $status_pagu_rincian = 0;

 if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
    $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
} else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
    $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
} else {
    if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {

        if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                    //batas pagu per dinas
            $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id,$kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
        } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                    //batas pagu per kegiatan 
            $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
        }
    } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
        $status_pagu_rincian = 0;
    }
}

//batasan pagu
if ($status_pagu_rincian == '1') {
    $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                //$this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Nilai Buku Biru.');
    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
} else if ($status_pagu_rincian == '0') {
    if ($akrual_code) {
        $akrual_code_baru = $akrual_code . '|01';
        $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
        $akrual_code_baru = $akrual_code_baru . $no_akrual_code;
    }

// $this->setFlash('berhasil', 'berhasil disimpan.');
// return $this->redirect('entri/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
//end of fasilitas 
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
    $rs_komponen = KomponenPeer::doSelectOne($c);
    if ($rs_komponen) {
        $komponen_harga = $rs_komponen->getKomponenHarga();
        if ($tipe == 'FISIK' && sfConfig::get('app_tahun_default') != '2016') {
            $komponen_harga = $rs_komponen->getKomponenHargaBulat();
        }
        $komponen_name = $rs_komponen->getKomponenName();
        $accres_komp = $rs_komponen->getAccres();
        $satuan = $rs_komponen->getSatuan();

        if (!empty($harga))
        {
            $komponen_harga=$harga;
        
        }

                    // validasi koma untuk satuan paket
        if (strpos($vol1, '.') || strpos($vol2, '.') || strpos($vol3, '.') || strpos($vol4, '.')) {
            if($satuan == 'Paket') {
                $this->setFlash('gagal', 'Mohon maaf, Pengisian Volume Komponen Dengan Satuan Paket Harus Bulat.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
        }
    }

    if (!empty($accres_komp))
    {
        $accres=$accres_komp;
    }


    $detail_no = 0;
    $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs_max = $stmt->executeQuery();
    while ($rs_max->next()) {
        $detail_no = $rs_max->getString('nilai');
    }
    $detail_no+=1;

    $querySisipan = "select max(status_level) as nilai "
    . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
    $stmt = $con->prepareStatement($querySisipan);
    $rs_level = $stmt->executeQuery();
    while ($rs_level->next()) {
        $posisi_terjauh = $rs_level->getInt('nilai');
    }
    $sisipan = 'false';
    if ($posisi_terjauh > 0) {
        $sisipan = 'true';
    }

    $detail_name = '';
    $kode_jasmas = '';

    if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
#51 - GMAP simpan                        
//                        $detail_name = $this->getRequestParameter('lokasi');
        $detail_name = $lokasi_baru;
        $ada_fisik = 'TRUE';
#51 - GMAP simpan                        
        $kode_jasmas = $this->getRequestParameter('jasmas');
    } else if ($tipe2 != 'KONSTRUKSI' && $tipe != 'FISIK' && !$est_fisik) {
//irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
        $estimasi_opsi_lokasi = array('');
//irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
        if (in_array($komponen_id, $estimasi_opsi_lokasi)) {
#51 - GMAP simpan                        
//                        $detail_name = $this->getRequestParameter('lokasi');
            $detail_name = $lokasi_baru;
#51 - GMAP simpan                        
            $kode_jasmas = $this->getRequestParameter('jasmas');
        } else {
//irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan    
            $detail_name = $this->getRequestParameter('keterangan');
//irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan                            
        }
//irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
    }

    $kode_sub = '';
    $sub = '';
#51 - GMAP comment                    
//                    if ($this->getRequestParameter('lokasi')) {
//                        $kode_lokasi = '';
//                        $detail_name = $this->getRequestParameter('lokasi');
//                        //sholeh begin
//                        $detail_names = explode(',', $detail_name);
//                        $adaSalah = false;
//                        foreach ($detail_names as $name) {
//                            $c = new Criteria();
//                            $c->add(VLokasiPeer::NAMA, trim($name));
//                            $rs_lokasi = VLokasiPeer::doSelectOne($c);
//                            if (!rs_lokasi)
//                                $adaSalah = true;
//                        }
//
//                        if ($adaSalah) {
//                            $this->setFlash('lokasitidakada', 'Lokasi Tidak Ada atau Tidak Valid dengan Data G.I.S');
//                            return $this->redirect("entri/buatbaru?lokasi=$detail_name&kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//                        }
//                        //sholeh end
//                    }
#51 - GMAP comment                    
//ditutup sementara simbada
                /* if($rs_komponen->getMaintenance()=='true' &&  $this->getRequestParameter('keterangan')=='' && $rekening !='5.2.2.27.01')
                  {
                  $this->setFlash('lokasitidakada', 'Data Simbada belum diisi');
                  return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=".md5('terbaru')."&commit=Pilih");
              } */

#51 - GMAP comment                                        
//irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
//                    $estimasi_opsi_lokasi = array('');
//                    //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
//                    if ($rekening == '5.2.2.27.01' && $tipe == 'FISIK' || in_array($komponen_id, $estimasi_opsi_lokasi)) {
//                        if (!$this->getRequestParameter('lokasi')) {
//                            $this->setFlash('lokasitidakada', 'Untuk Komponen Fisik diharapkan Mengisi Data Lokasi');
//                            return $this->redirect("entri/buatbaru?kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//                        }
//                    }
#51 - GMAP comment 
                //simpan note koefisien                                       
              if ($this->getRequestParameter('catatan_koefisien')) {
                $note_koefisien = $this->getRequestParameter('catatan_koefisien');
            }
            if ($this->getRequestParameter('keterangan')) {
                $detail_name = $this->getRequestParameter('keterangan');
            }

            if ($this->getRequestParameter('sub')) {
                $kode_sub = $this->getRequestParameter('sub');

                $d = new Criteria();
                $d->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
                $d->add(DinasRincianSubParameterPeer::KEGIATAN_CODE, $kegiatan_code);
                $d->add(DinasRincianSubParameterPeer::UNIT_ID, $unit_id);
                $rs_rinciansubparameter = DinasRincianSubParameterPeer::doSelectOne($d);
                if ($rs_rinciansubparameter) {
                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                    $kodesub = $rs_rinciansubparameter->getKodeSub();
                }
//print_r($kode_sub);exit;
            }
            $kode_subtitle = $this->getRequestParameter('subtitle');

            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $volume = 0;
            $keterangan_koefisien = '';
            $user = $this->getUser();
            $dinas = $user->setAttribute('nama', '', 'dinas');
            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1=$this->getRequestParameter('vol1');
                $vol2=0;
                $vol3=0;
                $vol4=0;
                if ($this->getRequestParameter('vol2') == '') {                   
                    $vol2 = 1;
                    if ($this->getRequestParameter('volume1') == '%' && (strpos($komponen_name, 'Iuran JK') === FALSE) && $satuan != '%')
                    $vol1 = ($this->getRequestParameter('vol1')/100);
                    else
                    $vol1 = $this->getRequestParameter('vol1');
                    $volume = $vol1*$vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if(!$this->getRequestParameter('vol2') == '') {
                    if ($this->getRequestParameter('volume2') == '%' && (strpos($komponen_name, 'Iuran JK') === FALSE) && $satuan != '%')
                    $vol2 = $this->getRequestParameter('vol2')/100;
                    else
                    $vol2 =  $this->getRequestParameter('vol2');
                    
                   // $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $volume = $vol1*$vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;                   
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    if ($this->getRequestParameter('volume3') == '%' && (strpos($komponen_name, 'Iuran JK') === FALSE) && $satuan != '%')
                    $vol3 = $this->getRequestParameter('vol3')/100;
                    else
                    $vol3 =  $this->getRequestParameter('vol3');

                    // $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $volume = $vol1*$vol2*$vol3;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    if ($this->getRequestParameter('volume4') == '%' && (strpos($komponen_name, 'Iuran JK') === FALSE) && $satuan != '%')
                    $vol4 = $this->getRequestParameter('vol3')/100;
                    else
                    $vol4 =  $this->getRequestParameter('vol4');

                    $volume = $vol1*$vol2*$vol3*$vol4;
                    #$volume = $volume*$vol4;
                    // $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            //die($volume.'-'.$keterangan_koefisien);

                //cek untuk mengisi status_komponen_baru
            $tahap_cek = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kegiatan_code);
            $c_pembanding_kegiatan = new Criteria();
            $c_pembanding_kegiatan->add(PembandingKegiatanPeer::UNIT_ID, $unit_id);
            $c_pembanding_kegiatan->add(PembandingKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $c_pembanding_kegiatan->add(PembandingKegiatanPeer::TAHAP, $tahap_cek);
            if (PembandingKegiatanPeer::doSelectOne($c_pembanding_kegiatan)) {
                $status_komponen_baru = 'TRUE';
            } else {
                $status_komponen_baru = 'FALSE';
            }
                //cek untuk mengisi status_komponen_baru
//                    tambahan untuk gmap                    
            $detailno_fisik = 0;
//                    tambahan untuk gmap           

            if ($this->getRequestParameter('status') == 'pending') {
                $detail_no = 0;
                $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_max = $stmt->executeQuery();
                while ($rs_max->next()) {
                    $detail_no = $rs_max->getString('nilai');
                }
                $detail_no+=1;

                $sql = "select rekening_asli, komponen_name, detail_name from " . sfConfig::get('app_default_schema') . ".dinas_rka_member where kode_sub='$kode_sub'";
//print_r($sql);exit;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($sql);
                $rs2 = $stmt->executeQuery();
                while ($rs2->next()) {
                    $komponen_name2 = $rs2->getString('komponen_name');
                    $detail_name2 = $rs2->getString('detail_name');
                    $rekening_asli = $rs2->getString('rekening_asli');
                }
                $subSubtitle = $komponen_name2 . ' ' . $detail_name2;
                $subSubtitle = trim($subSubtitle);

                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah 
                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time,tahun)
                values
                ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "',
                '" . str_replace("'", "''", $subtitle) . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahun_default') . "')";

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                budgetLogger::log('Menambah komponen baru(bermasalah) dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Penyimpanan Telah Berhasil, Tetapi Tidak Masuk Ke RKA, Harap Hubungi Penyelia Anda');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            } else {
//bisma : 21-5-2012 : Mulai IF dari sini. 
//jika tidak OK dan tidak pending
//bisma : contreng
//print_r('bisma');exit;
                $kode_detail_kegiatan = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                $sub_koRek = substr($rekening, 0, 5);

                if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                    $ada_fisik = 'TRUE';
                }

                if ($sub_koRek == '5.2.3' || $sub_koRek == '5.2.2') {
                    $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                    . "where kode_sub ilike 'RKAM%'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($sql);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $kodesub = $rs->getString('kode_sub');
                    }
                    $kode = substr($kodesub, 4, 5);
                    $kode+=1;
                    if ($kode < 10) {
                        $kodesub = 'RKAM0000' . $kode;
                    } elseif ($kode < 100) {
                        $kodesub = 'RKAM000' . $kode;
                    } elseif ($kode < 1000) {
                        $kodesub = 'RKAM00' . $kode;
                    } elseif ($kode < 10000) {
                        $kodesub = 'RKAM0' . $kode;
                    } elseif ($kode < 100000) {
                        $kodesub = 'RKAM' . $kode;
                    }

                    if ($this->getRequestParameter('sub') == '') {
                        $kodesub = '';
                    }

                    $con = Propel::getConnection();
                    $con->begin();
                    try {
                        $detail_no = 0;
                        $queryDetailNo = "select max(detail_no) as nilai "
                        . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                        $stmt = $con->prepareStatement($queryDetailNo);
                        $rs_max = $stmt->executeQuery();
                        while ($rs_max->next()) {
                            $detail_no = $rs_max->getString('nilai');
                        }
                        $detail_no+=1;
                        $detail_no_suami = $detail_no+2;
                        $detail_no_anak = $detail_no+3;


                        $querySisipan = "select max(status_level) as nilai "
                        . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                        $stmt = $con->prepareStatement($querySisipan);
                        $rs_level = $stmt->executeQuery();
                        while ($rs_level->next()) {
                            $posisi_terjauh = $rs_level->getInt('nilai');
                        }
                        $sisipan = 'false';
                        if ($posisi_terjauh > 0) {
                            $sisipan = 'true';
                        }

//                                tambahan untuk gmap
                        if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                            $ada_fisik = 'TRUE';
                            $detailno_fisik = $detail_no;
                        }
//                                tambahan untuk gmap 
                        if ($detail_name != '')
                            $detail_name_baru = "(" . $detail_name . ")";
                        else
                            $detail_name_baru = $detail_name;

                            //ambil kegiatan id
                        $co = new Criteria();
                        $co->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $co->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($co);

                        $kegiatan_id=$rs_kegiatan->getKegiatanId();
                            //echo $lokasi_kec.'--'.$lokasi_kel.'--'.$kecamatan.'--'.$kelurahan;exit;
                        if ($kode_sub && $kode_sub != 'nama') {
//$kode_sub : berasal dari sub subtitle                             

                            $sql = "select rekening_asli, komponen_name, detail_name "
                            . "from " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                            . "where kode_sub='$kode_sub'";
//print_r($sql);exit;
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($sql);
                            $rs2 = $stmt->executeQuery();
                            while ($rs2->next()) {
                                $komponen_name2 = $rs2->getString('komponen_name');
                                $detail_name2 = $rs2->getString('detail_name');
                                $rekening_asli = $rs2->getString('rekening_asli');
                            }
                            $subSubtitle = $komponen_name2 . ' ' . $detail_name2;
                            $subSubtitle = trim($subSubtitle);                                

                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,is_musrenbang,is_hibah,
                            lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru,is_hpsp,is_persediaan,is_rab,is_narsum,is_bbm,note_koefisien,volume_orang,volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "','" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', $sisipan, '$akrual_code_baru', '$tipe2', '$kode_detail_kegiatan', $status_komponen_baru, $isLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '". $note_koefisien ."',".$volume_orang.",".$volume_orang.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id .",$accres)";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();
                            //insert tunjangan suam/istri
                            if ($jumlah_suami_istri > 0)
                            {
                                die('masuk 1');                               

                            }

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no);
                        } else {  

                            if ($kode_sub == 'nama')
                                $subSubtitle = trim($komponen_name . ' ' . $detail_name);
                            else
                                $subSubtitle = '';

                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,is_musrenbang,is_hibah,
                            lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru, is_hpsp, is_persediaan, is_rab,is_narsum,is_bbm,note_koefisien,volume_orang,volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $subSubtitle . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', $sisipan, '$akrual_code_baru', '$tipe2', '$kode_detail_kegiatan', $status_komponen_baru, $isLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '". $note_koefisien ."',".$volume_orang.",".$volume_orang.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",$accres)";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no);

                             //insert tunjangan suam/istri
                            if ($jumlah_suami_istri > 0)
                            {
                                 die('masuk 2');                               
                            }


                            if ($kodesub != '') {
                                $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".dinas_rka_member (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun )
                                values ('$kodesub','$unit_id','$kegiatan_code',$detail_no,'$komponen_id','$komponen_name','$detail_name','$rekening','" . sfConfig::get('app_tahun_default') . "')";
//print_r($queryInsert2RkaMember);exit;
                                $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                                $stmt2->executeQuery();
                            }
                            $detailNamePenyusun = '';
                                //1 Juni 2016 -> pakai kode akrual -> tidak jadi
                            $rekening_induk = $rekening;
                            $temp_lain = 8;
                            foreach ($komponen_penyusun as $penyusun) {
                                $penyusun_id = $this->getRequestParameter('komponenPenyu_' . $penyusun);
                                $cekPenyusun = new Criteria();
                                $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun_id);
                                $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
//print_r($rs_cekPenyusun);
                                foreach ($rs_cekPenyusun as $komponenPenyusun) {
                                    if ($akrual_code) {
                                        if ($komponenPenyusun->getKodeAkrualKomponenPenyusun()) {
                                            $akrual_code_penyusun = $akrual_code . '|02|' . $komponenPenyusun->getKodeAkrualKomponenPenyusun() . $no_akrual_code;
                                        } else {
                                            if ($temp_lain < 10)
                                                $akrual_code_penyusun = $akrual_code . '|02|0' . $temp_lain . $no_akrual_code;
                                            else
                                                $akrual_code_penyusun = $akrual_code . '|02|' . $temp_lain . $no_akrual_code;
                                            $temp_lain++;
                                        }
                                    }
                                    $detail_no = 0;
                                    $query = "select max(detail_no) as nilai "
                                    . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query);
                                    $rs_max = $stmt->executeQuery();
                                    while ($rs_max->next()) {
                                        $detail_no = $rs_max->getString('nilai');
                                    }
                                    $detail_no+=1;
                                    $detail_no_suami = $detail_no+2;
                                    $detail_no_anak = $detail_no+3;

                                    $kode_detail_kegiatan_penyusun = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                                    $querySisipan = "select max(status_level) as nilai "
                                    . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                    $stmt = $con->prepareStatement($querySisipan);
                                    $rs_level = $stmt->executeQuery();
                                    while ($rs_level->next()) {
                                        $posisi_terjauh = $rs_level->getInt('nilai');
                                    }
                                    $sisipan = 'false';
                                    if ($posisi_terjauh > 0) {
                                        $sisipan = 'true';
                                    }

                                    if ($komponenPenyusun->getKomponenNonPajak() == TRUE) {
                                        $pajakPenyusun = 0;
                                    } else {
                                        $pajakPenyusun = 10;
                                    }

                                        //$kodeChekBox = str_replace(".", "_", $komponenPenyusun->getKomponenId());

                                    $subSubtitle = $komponen_name . ' ' . $detail_name;
                                    $subSubtitle = trim($subSubtitle);

                                    $co = new Criteria();
                                    $co->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                                    $co->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($co);

                                    $kegiatan_id=$rs_kegiatan->getKegiatanId();

                                    $query2 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                    (kegiatan_code, kegiatan_id, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                    komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,rekening_code_asli,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien,volume_orang,volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres)
                                    values
                                    ('" . $kegiatan_code . "','" . $kegiatan_id . "', 'SSH', " . $detail_no . ", '" . $rekening_induk . "', '" . $komponenPenyusun->getKomponenId() . "', '" . $detailNamePenyusun . "', " . $this->getRequestParameter('volPenyu_' . $penyusun) . ", '" . $this->getRequestParameter('volPenyu_' . $penyusun) . ' ' . $komponenPenyusun->getSatuan() . "', '" . str_replace("'", "''", $subtitle) . "',
                                    " . $komponenPenyusun->getKomponenHarga() . ", " . $komponenPenyusun->getKomponenHarga() . ",'" . $komponenPenyusun->getKomponenName() . "', '" . $komponenPenyusun->getSatuan() . "', " . $pajakPenyusun . ",'" . $unit_id . "','" . $kodesub . "', '" . $subSubtitle . "',
                                    '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "', $sisipan, '$akrual_code_penyusun', '" . $komponenPenyusun->getKomponenTipe2() . "', '$kode_detail_kegiatan_penyusun', $status_komponen_baru, $sisaLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',".$volume_orang.",".$volume_orang.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",$accres)";
                                    $stmt = $con->prepareStatement($query2);
                                    budgetLogger::log('Menambah Komponen penyusun baru (eRevisi) dengan komponen name ' . $komponenPenyusun->getKomponenName() . '(' . $komponenPenyusun->getKomponenId() . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code . ' pada KodeSub:' . $kodesub);
                                   
                                    $stmt->executeQuery();

                                    historyUserLog::tambah_komponen_penyusun_revisi($unit_id, $kegiatan_code, $detail_no);

                             //insert tunjangan suam/istri
                            if ($jumlah_suami_istri > 0)
                            {
                                 die('masuk 3');                                
                            }



                                }
                            }
                        }

//untuk komponen fisik dan maintenance = true, insert ke db gis_budgeting tabel master_lokasi simbada 
//jadi semua yang bertipe fisik dan maintenance = true akan mengambil data dari simbada dan harus didata atau di catat di gis
                        if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) && $rs_komponen->getMaintenance() == 'true') {
                            $kodeSimbada = $this->maxKodeSimbada();
                            $con2 = propel::getConnection('gis');
                            $con2->begin();
                            try {
                                $masterLokasiSimbada = new masterLokasiSimbada();
                                $masterLokasiSimbada->setKodeLokasiSimbada($kodeSimbada);
                                $masterLokasiSimbada->setNamaLokasiSimbada($detail_name);
                                $masterLokasiSimbada->setCatatan($unit_id . '||' . $kegiatan_code . '||' . $detail_no . '||' . $komponen_name);
                                $masterLokasiSimbada->save($con2);
                                $con2->commit();
                            } catch (Exception $e) {
                                $this->setFlash('gagal', 'data simbada gagal ditambahkan ke gis karena ' . $e);
                                $con2->rollback();
                            }
                        }
//end komponen fisik dan maintenance

                        $con->commit();

                        budgetLogger::log('Menambah Komponen baru (eRevisi) dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);

                        $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
                    } catch (Exception $e) {
                        $con->rollback();
                        $this->setFlash('gagal', 'Tidak berhasil tersimpan');
                            //$this->setFlash('gagal', 'Tidak berhasil tersimpan karena ' . $e->getMessage());
                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                } else {
//jika tidak $sub_koRek=='5.2.3' || $sub_koRek=='5.2.2'
//print_r('aneh'.$kode_sub);exit;                         
                    if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                        $ada_fisik = 'TRUE';
                    }
                    if ($detail_name != '')
                        $detail_name_baru = "(" . $detail_name . ")";
                    else
                        $detail_name_baru = $detail_name;

                    if ($kode_sub && $kode_sub != 'nama') {
                        $sql = "select rekening_asli, komponen_name, detail_name from " . sfConfig::get('app_default_schema') . ".dinas_rka_member where kode_sub='$kode_sub'";
//print_r($sql);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($sql);
                        $rs2 = $stmt->executeQuery();
                        while ($rs2->next()) {
                            $komponen_name2 = $rs2->getString('komponen_name');
                            $detail_name2 = $rs2->getString('detail_name');
                            $rekening_asli = $rs2->getString('rekening_asli');
                        }
                        $subSubtitle = $komponen_name2 . ' ' . $detail_name2;
                        $subSubtitle = trim($subSubtitle);

                        $co = new Criteria();
                        $co->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $co->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($co);

                        $kegiatan_id=$rs_kegiatan->getKegiatanId();

                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code,kegiatan_id, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,is_musrenbang,is_hibah,
                            lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien,volume_orang,volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres)
                            values
                            ('" . $kegiatan_code . "', '" . $kegiatan_id . "','" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', $sisipan, '$akrual_code_baru', '$tipe2', '$kode_detail_kegiatan', $status_komponen_baru, $sisaLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',".$volume_orang.",".$volume_orang.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",$accres)";
                            $stmt = $con->prepareStatement($query);
                            budgetLogger::log('Menambah komponen baru (eRevisi) dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                            $stmt->executeQuery();
                            $con->commit();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no);

                             //insert tunjangan suam/istri
                            if ($jumlah_suami_istri > 0)
                            {
                                 die('masuk 4');

                            }


                        } catch (Exception $e) {
                            echo $e;
                            $con->rollback();
                        }
                    } else {                             
                        if ($kode_sub == 'nama') {
                            $sub = $komponen_name2 . ' ' . $detail_name2;
                            $sub = trim($sub);
                        }
                        $con = Propel::getConnection();
                        $con->begin();
                        $co = new Criteria();
                        $co->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $co->add(DinasMasterKegiatanPeer::KODE_KEGIATAN,$kegiatan_code);
                        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($co);

                        $kegiatan_id=$rs_kegiatan->getKegiatanId();

                        //cek jika komponen gaji isian harus orang dan bulan
                        if(strpos( $komponen_name,'Gaji Pokok') !== false && (($this->getRequestParameter('volume1') != 'Orang' && $this->getRequestParameter('volume2') != 'Bulan') || ($this->getRequestParameter('volume1') == 'Orang' && $this->getRequestParameter('volume2') != 'Bulan')))
                        {

                            $this->setFlash('gagal', 'Tidak berhasil tersimpan Untuk isian volume gaji adalah jumlah Orang x jumlah bulan Bulan . Contoh : 2 Orang x 14 Bulan');
                                //$this->setFlash('gagal', 'Tidak berhasil tersimpan karena ' . $e->getMessage());
                            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                        }

                        try {
                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,is_musrenbang,is_hibah,
                            lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn, is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien, volume_orang, volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', $sisipan, '$akrual_code_baru', '$tipe2', '$kode_detail_kegiatan', $status_komponen_baru, $sisaLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',".$volume_orang.",".$volume_orang.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",$accres)";
                            $stmt = $con->prepareStatement($query);
                            budgetLogger::log('Menambah komponen baru (eRevisi) dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                            $stmt->executeQuery();
                            $con->commit();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no);

                            //insert tunjangan suam/istri
                            if ($jumlah_suami_istri > 0)
                            {
                                $detail_no_suami = $detail_no+1;
                                $detail_gaji= $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
                                $kode_detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no_suami;
                                //die('masuk 5'.$detail_no_suami.'-'.$detail_no);
                                $nama_cari = 'Tunjangan Suami/Istri ('.TRIM($komponen_name).')';
                                $c = new Criteria();
                                $c->add(KomponenPeer::KOMPONEN_NAME, '%'.$nama_cari.'%', Criteria::ILIKE);
                                $rs_komponen = KomponenPeer::doSelectOne($c);
                                if ($rs_komponen) {
                                    $komponen_id_suami = $rs_komponen->getKomponenId();
                                    $komponen_harga_suami = $rs_komponen->getKomponenHarga();                                   
                                    $komponen_name_suami = $rs_komponen->getKomponenName();
                                    $accres_komp = $rs_komponen->getAccres();
                                    $satuan = $rs_komponen->getSatuan();
                                }

                                if ($this->getRequestParameter('volume1') == 'Orang' && $this->getRequestParameter('volume2') == 'Bulan')
                                {
                                    $volume_suami = $jumlah_suami_istri*$this->getRequestParameter('vol2')*0.1;
                                    $keterangan_koefisien_suami= '10 % X '.$jumlah_suami_istri.' Orang X '.$this->getRequestParameter('vol2').' Bulan';
                                    $volume_orang_suami = $jumlah_suami_istri;
                                }
                                //$volume=$volume * 0.1;
                                // die($detail_no_suami.'-'. $nama_cari.'-'.$komponen_id_suami.'-'.$komponen_harga_suami.'-'.$komponen_name_suami.'-'.$accres_komp.'-'.$satuan.'-'.$volume_suami.'-'. $keterangan_koefisien_suami);
                            
                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,is_musrenbang,is_hibah,
                            lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn, is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien, volume_orang, volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres,detail_gaji)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "', '" . $tipe . "', " . $detail_no_suami . ", '5.1.01.01.02.0001', '" . $komponen_id_suami . "', '" . $detail_name_baru . "', " . $volume_suami . ", '" . $keterangan_koefisien_suami . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga_suami . ", " . $komponen_harga_suami . ",'" . $komponen_name_suami . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', $sisipan, '$akrual_code_baru', '$tipe2', '$kode_detail_kegiatan', $status_komponen_baru, $sisaLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',". $volume_orang_suami.",". $volume_orang_suami.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",2.5,'$detail_gaji')";
                            $stmt = $con->prepareStatement($query);
                            budgetLogger::log('Menambah komponen baru (eRevisi) dengan komponen name ' . $komponen_name_suami . '(' . $komponen_id_suami . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                            $stmt->executeQuery();
                            $con->commit();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no_suami);

                            }

                            //insert tunjangan anak
                           if ($jumlah_anak > 0)
                            {
                                $detail_no_anak = $detail_no+2;
                                $detail_gaji= $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
                                $kode_detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no_anak;
                                //die('masuk 5'.$detail_no_anak.'-'.$detail_no);
                                $nama_cari = 'Tunjangan Anak ('.TRIM($komponen_name).')';
                                $c = new Criteria();
                                $c->add(KomponenPeer::KOMPONEN_NAME, '%'.$nama_cari.'%', Criteria::ILIKE);
                                $rs_komponen = KomponenPeer::doSelectOne($c);
                                if ($rs_komponen) {
                                    $komponen_id_anak = $rs_komponen->getKomponenId();
                                    $komponen_harga_anak = $rs_komponen->getKomponenHarga();                                   
                                    $komponen_name_anak = $rs_komponen->getKomponenName();
                                    $accres_komp = $rs_komponen->getAccres();
                                    $satuan = $rs_komponen->getSatuan();
                                }

                                if ($this->getRequestParameter('volume1') == 'Orang' && $this->getRequestParameter('volume2') == 'Bulan')
                                {
                                    $volume_anak = $jumlah_anak*$this->getRequestParameter('vol2')*0.02;
                                    $keterangan_koefisien_anak= '2 % X '.$jumlah_anak.' Orang X '.$this->getRequestParameter('vol2').' Bulan';
                                    $volume_orang_anak = $jumlah_anak;
                                }
                                //$volume=$volume * 0.1;
                                // die($detail_no_anak.'-'. $nama_cari.'-'.$komponen_id_anak.'-'.$komponen_harga_anak.'-'.$komponen_name_anak.'-'.$accres_komp.'-'.$satuan.'-'.$volume_anak.'-'. $keterangan_koefisien_anak);
                                
                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_kapitasi_bpjs,is_bos,is_bobda,is_musrenbang,is_hibah,
                            lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn, is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien, volume_orang, volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres,detail_gaji)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "', '" . $tipe . "', " . $detail_no_anak . ", '5.1.01.01.02.0001', '" . $komponen_id_anak . "', '" . $detail_name_baru . "', " . $volume_anak . ", '" . $keterangan_koefisien_anak . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga_anak . ", " . $komponen_harga_anak . ",'" . $komponen_name_anak . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isKapitasi . "','" . $isBos . "','" . $isBopda . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', $sisipan, '$akrual_code_baru', '$tipe2', '$kode_detail_kegiatan', $status_komponen_baru, $sisaLelang, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',". $volume_orang_anak.",". $volume_orang_anak.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",2.5,'$detail_gaji')";
                            $stmt = $con->prepareStatement($query);
                            budgetLogger::log('Menambah komponen baru (eRevisi) dengan komponen name ' . $komponen_name_anak . '(' . $komponen_id_anak . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                            $stmt->executeQuery();
                            $con->commit();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no_anak);

                            }

                        } catch (Exception $e) {
                            $con->rollback();
                            $this->setFlash('gagal', 'Tidak berhasil tersimpan');
                                //$this->setFlash('gagal', 'Tidak berhasil tersimpan karena ' . $e->getMessage());
                            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                        }
                    }
                    $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
                    }//eof else $sub_koRek=='5.2.3' || $sub_koRek=='5.2.2'
                }

                //untuk insert ke tabel dinas_rincian_hpsp
                if ($sisaLelang) {
                    $kirimControlling = array();
                    $kirimControlling['kode_kegiatan'] = $kegiatan_code;
                    $kirimControlling['unit_id'] = $unit_id;
                    $kirimControlling['rekening_code'] = $rekening;
                    $kirimControlling['pajak'] = $pajak;
                    $kirimControlling['volume'] = $volume;
                    $kirimControlling['detail_no'] = $detail_no;
                    $kirimControlling['harga'] = $komponen_harga;
                    $kirimControlling['nama'] = $komponen_name;
                    $kirimControlling['kode_detail_kegiatan_asal'] = '';

                    $isi_chx = $this->getRequestParameter('chkPSP');
                    $count = 0;

                    foreach ($isi_chx as $value) {
                        $uid = $this->getRequestParameter('unit');
                        $keg_code = $this->getRequestParameter('kegiatan');
                        $kom_id = $this->getRequestParameter("idPSP".$value);
                        $det_no = $value;
                        $sisa_anggaran = $this->getRequestParameter($value);
                        $catatan = $this->getRequestParameter("catatanPSP".$value);
                        $query_ambil_tahap =
                        "SELECT DISTINCT tahap
                        FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan
                        WHERE unit_id = '$uid'
                        AND kode_kegiatan = '$keg_code'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query_ambil_tahap);
                        $rs_ambil_tahap = $stmt->executeQuery();
                        while($rs_ambil_tahap->next()) {
                            $tahap = $rs_ambil_tahap->getString('tahap');
                        }
                        // $tahap = sfConfig::get('app_tahap_edit');

                        // generate detail kegiatan asal
                        $kom_id_asal = $this->getRequestParameter("kom_id");
                        $d_no = 1;
                        $que = 
                        "SELECT MAX(detail_no) AS nilai
                        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        WHERE unit_id = '$uid'
                        AND kegiatan_code = '$keg_code'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($que);
                        $rs_dn_max = $stmt->executeQuery();
                        while ($rs_dn_max->next()) {
                            $d_no = $rs_dn_max->getInt('nilai');
                        }
                        
                        // $det_no = $d_no;
                        $det_keg = $this->getRequestParameter('unit').".".$this->getRequestParameter('kegiatan').".".$d_no;

                        //id max
                        // $maxid = 0;
                        // $quemaxid = "select max(id_komponen_lelang) as maxid "
                        //         . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp";
                        // $con = Propel::getConnection();
                        // $stmt = $con->prepareStatement($quemaxid);
                        // $rs_maxid = $stmt->executeQuery();
                        // while ($rs_maxid->next()) {
                        //     $maxid = $rs_maxid->getString('maxid');
                        // }
                        // $maxid+=1;
                        // $id_kom_lelang = $maxid;
                        // $catatan = "catatan saja";

                        // $j++;

                        //insertnya dah bisa masuk
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $que_ins = "insert into ".sfConfig::get('app_default_schema').".dinas_rincian_hpsp
                            (unit_id, kegiatan_code, komponen_id, detail_no, id_komponen_lelang, sisa_anggaran, catatan, tahap, detail_kegiatan)
                            values
                            ('".$uid."', '".$keg_code."', '".$kom_id."', ".$det_no.", '".$det_keg."', ".$sisa_anggaran.", '".$catatan."', '".$tahap."','".$uid.".".$keg_code.".".$det_no."')";

                            $stmt = $con->prepareStatement($que_ins);
                            $stmt->executeQuery();

                            $con->commit();

                            if($count > 0)
                                $kirimControlling['kode_detail_kegiatan_asal'] .= '|';
                            $kirimControlling['kode_detail_kegiatan_asal'] .= $uid.'.'.$keg_code.'.'.$det_no;
                            $count++;
                        } catch (Exception $e) {
                            $con->rollback();
                        }
                    }

                    // update status arahan
                    $arahan = $this->getRequestParameter('arahan');
                    $kirimControlling['id_arahan'] = '';
                    $count = 0;

                    foreach ($arahan as $arahan_id) {
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $que_updt = 
                            "UPDATE ".sfConfig::get('app_default_schema').".arahan_belanja_kegiatan
                            SET status = 1
                            WHERE id = ".$arahan_id;

                            $stmt = $con->prepareStatement($que_updt);
                            $stmt->executeQuery();

                            $con->commit();

                            if($count > 0)
                                $kirimControlling['id_arahan'] .= '|';
                            $kirimControlling['id_arahan'] .= $arahan_id;
                            $count++;
                        } catch (Exception $e) {
                            $con->rollback();
                        }
                    }
                    if(count($arahan) == 0) {
                        $kirimControlling['id_arahan'] = null;
                    }
                    $this->kirimServiceControlling($kirimControlling);
                }

                // insert penyusun tenaga operasional
                $penyusun_opr = $this->getRequestParameter('penyusun_opr');
                foreach ($penyusun_opr as $value) {
                    $opr_kegiatan_code = $this->getRequestParameter('kegiatan');
                    $opr_tipe = 'SSH';
                    $opr_det_no = 0;
                    $opr_rek_code = '5.1.02.02.01.080';
                    $opr_komponen_id = $this->getRequestParameter('kodeOpr'.$value);
                    $opr_detail_name = '('.$this->getRequestParameter('keteranganOpr'.$value).')';
                    $opr_volume = $this->getRequestParameter('volOprOrang'.$value) * $this->getRequestParameter('volOprBulan'.$value) * $this->getRequestParameter('volOprRupiah'.$value);
                    $opr_keterangan_koefisien = $this->getRequestParameter('volOprOrang'.$value).' Orang X '.$this->getRequestParameter('volOprBulan'.$value).' Bulan X '.$this->getRequestParameter('volOprRupiah'.$value).' Rupiah';
                    $opr_subtitle = str_replace("'", "''", $subtitle);
                    $opr_harga = $this->getRequestParameter('hargaOpr'.$value);
                    $opr_harga_awal = $this->getRequestParameter('hargaOpr'.$value);
                    $opr_komponen_name = $this->getRequestParameter('namaOpr'.$value);
                    $opr_satuan = $this->getRequestParameter('satuanOpr'.$value);
                    $opr_pajak = 0;
                    $opr_unit_id = $this->getRequestParameter('unit');
                    $opr_sub = $subSubtitle;
                    $opr_kode_sub = $kode_sub;

                    $que = 
                    "SELECT MAX(detail_no) AS nilai
                    FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                    WHERE unit_id = '$opr_unit_id'
                    AND kegiatan_code = '$opr_kegiatan_code'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($que);
                    $rs_dn_max = $stmt->executeQuery();
                    while ($rs_dn_max->next()) {
                        $opr_det_no = $rs_dn_max->getInt('nilai');
                    }
                    $opr_det_no += 1;

                    $c_master_kegiatan = new Criteria();
                    $c_master_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $opr_unit_id);
                    $c_master_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $opr_kegiatan_code);
                    $rs_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_master_kegiatan);
                    $opr_tahap = $rs_master_kegiatan->getTahap();

                    $opr_last_update = date("Y-m-d H:i:s");
                    $opr_tahap_edit = sfConfig::get('app_tahap_edit');
                    $opr_tahun = sfConfig::get('app_tahun_default');
                    if(strpos($opr_komponen_name, 'JKN')) $opr_jkn = 'TRUE'; else $opr_jkn = 'FALSE';
                    if(strpos($opr_komponen_name, 'JKK')) $opr_jkk = 'TRUE'; else $opr_jkk = 'FALSE';
                    $opr_detail_kegiatan = $opr_unit_id.'.'.$opr_kegiatan_code.'.'.$opr_det_no;

                    $query_insert_operasional = 
                    "INSERT INTO ".sfConfig::get('app_default_schema').".dinas_rincian_detail
                    (kegiatan_code, tipe, detail_no, rekening_code, komponen_id,
                    detail_name, volume, keterangan_koefisien, subtitle, komponen_harga,
                    komponen_harga_awal, komponen_name, satuan, pajak, unit_id,
                    kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit, tahun,
                    is_blud, is_musrenbang, is_hibah, lokasi_kecamatan, lokasi_kelurahan, note_skpd,
                    is_potong_bpjs, is_iuran_bpjs, tahap, is_iuran_jkn, is_iuran_jkk,
                    status_sisipan, akrual_code, tipe2, detail_kegiatan, status_komponen_baru, is_hpsp)
                    VALUES
                    ('$opr_kegiatan_code', '$opr_tipe', $opr_det_no, '$opr_rek_code', '$opr_komponen_id',
                    '$opr_detail_name', $opr_volume, '$opr_keterangan_koefisien', '$opr_subtitle', $opr_harga,
                    $opr_harga_awal, '$opr_komponen_name', '$opr_satuan', $opr_pajak, '$opr_unit_id',
                    '$opr_kode_sub', '$opr_sub', '', '', '$opr_last_update', '$opr_tahap_edit', $opr_tahun,
                    FALSE, FALSE, FALSE, '', '', '$note_skpd',
                    FALSE, TRUE, '$opr_tahap', $opr_jkn, $opr_jkk,
                    FALSE, '', '', '$opr_detail_kegiatan', TRUE, FALSE)";
                    $con = Propel::getConnection();
                    $con->begin();
                    try {
                        $stmt = $con->prepareStatement($query_insert_operasional);
                        $stmt->executeQuery();

                        $con->commit();
                    } catch (Exception $e) {
                        $con->rollback();
                        // die('Error nih: '.$e);
                    }
                }

//tambahan untuk gmap 9juni
                if ($ada_fisik == 'TRUE') {
//                        $this->setFlash('gagal', 'Komponen Pekerjaan Fisik akan muncul setelah Anda memetakan lokasi pada Maps yang disediakan.');
                    $rd_cari = new Criteria();
                    $rd_cari->add(DinasRincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $rd_cari->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                    $rd_cari->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no, Criteria::EQUAL);
                    $rd_dapat = DinasRincianDetailPeer::doSelectOne($rd_cari);
                       // $rd_dapat->setStatusHapus(TRUE);
                       // $rd_dapat->save();

                    $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                    budgetLogger::log('Komponen Fisik' . $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName() . ' disembunyikan apabila tidak melakukan pemetaan dengan kode ' . $kode_rka);

                    $keisi = 0;

//                    ambil lama
                    $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                    if (count($lokasi_lama) > 0) {
                        foreach ($lokasi_lama as $value_lokasi_lama) {
                            $c_cari_lokasi = new Criteria();
                            $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                            $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                            $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                            if ($dapat_lokasi_lama) {

                                $jalan_fix = '';
                                $gang_fix = '';
                                $nomor_fix = '';
                                $rw_fix = '';
                                $rt_fix = '';
                                $keterangan_fix = '';
                                $tempat_fix = '';

                                $jalan_lama = $dapat_lokasi_lama->getJalan();
                                $gang_lama = $dapat_lokasi_lama->getGang();
                                $nomor_lama = $dapat_lokasi_lama->getNomor();
                                $rw_lama = $dapat_lokasi_lama->getRw();
                                $rt_lama = $dapat_lokasi_lama->getRt();
                                $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                                $tempat_lama = $dapat_lokasi_lama->getTempat();

                                if ($jalan_lama <> '') {
                                    $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                                }

                                if ($tempat_lama <> '') {
                                    $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                                }

                                if ($gang_lama <> '') {
                                    $gang_fix = $gang_lama . ' ';
                                }

                                if ($nomor_lama <> '') {
                                    $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                                }

                                if ($rw_lama <> '') {
                                    $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                                }

                                if ($rt_lama <> '') {
                                    $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                                }

                                if ($keterangan_lama <> '') {
                                    $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                                }

                                $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detailno_fisik;
                                $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                $lokasi_per_titik = $lokasi_baru;

                                $c_insert_gis = new HistoryPekerjaanV2();
                                $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                $c_insert_gis->setKodeRka($rka_lokasi);
                                $c_insert_gis->setStatusHapus(FALSE);
                                $c_insert_gis->setJalan(strtoupper($jalan_lama));
                                $c_insert_gis->setGang(strtoupper($gang_lama));
                                $c_insert_gis->setNomor(strtoupper($nomor_lama));
                                $c_insert_gis->setRw(strtoupper($rw_lama));
                                $c_insert_gis->setRt(strtoupper($rt_lama));
                                $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                                $c_insert_gis->setTempat(strtoupper($tempat_lama));
                                $c_insert_gis->setKomponen($komponen_lokasi);
                                $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                $c_insert_gis->setLokasi($lokasi_per_titik);
                                $c_insert_gis->save();
                            }
                        }
                    }

//                    buat baru
                    $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                    $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                    $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                    $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                    $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                    $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                    $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                    $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                    $total_array_lokasi = count($lokasi_jalan);

                    for ($i = 0; $i < $total_array_lokasi; $i++) {
                        $jalan_fix = '';
                        $gang_fix = '';
                        $tipe_gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';
                        if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                            if (trim($lokasi_jalan[$i]) <> '') {
                                $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                            }

                            if (trim($lokasi_tempat[$i]) <> '') {
                                $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                            }

                            if (trim($tipe_gang[$i]) <> '') {
                                $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                            } else {
                                $tipe_gang_fix = 'GG. ';
                            }

                            if (trim($lokasi_gang[$i]) <> '') {
                                $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                            }

                            if (trim($lokasi_nomor[$i]) <> '') {
                                $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                            }

                            if (trim($lokasi_rw[$i]) <> '') {
                                $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                            }

                            if (trim($lokasi_rt[$i]) <> '') {
                                $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                            }

                            if (trim($lokasi_keterangan[$i]) <> '') {
                                $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                            }


                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detailno_fisik;
                            $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                            $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                            $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                            $lokasi_per_titik = $lokasi_baru;

                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka($rka_lokasi);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                            $c_insert_gis->setGang(strtoupper($gang_fix));
                            $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                            $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                            $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                            $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                            $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                            $c_insert_gis->setKomponen($komponen_lokasi);
                            $c_insert_gis->setKecamatan($kecamatan_lokasi);
                            $c_insert_gis->setKelurahan($kelurahan_lokasi);
                            $c_insert_gis->setLokasi($lokasi_per_titik);
                            $c_insert_gis->save();
                        }
                    }

                    $con = Propel::getConnection();
                    $query2 = "select * from master_kelompok_gmap where '" . $rd_dapat->getKomponenId() . "' ilike kode_kelompok||'%'";
                    $stmt2 = $con->prepareStatement($query2);
                    $rs2 = $stmt2->executeQuery();
                    while ($rs2->next()) {
                        $id_kelompok = $rs2->getString('id_kelompok');
                    }

                    if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                        $id_kelompok = 19;

                        if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                            $id_kelompok = 100;
                        } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                            $id_kelompok = 101;
                        }
                    }

                    historyUserLog::tambah_komponen_fisik_no_lokasi_revisi($unit_id, $kegiatan_code, $detail_no);

                    return $this->redirect(sfConfig::get('app_path_gmap') . 'insertBaru_revisi.php?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code . '&detail_no=' . $detail_no . '&satuan=' . $rd_dapat->getSatuan() . '&volume=' . $rd_dapat->getVolume() . '&nilai_anggaran=' . $rd_dapat->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=2&nm_user=' . $this->getUser()->getNamaLogin() . '&lokasi_ke=1');
                } else {
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
//tambahan untuk gmap 9juni
            }
//eof status_pagu_rincian
//irul -> UNTUK REVISI
// irul 18maret 2014 - simpan catatan   
//irul -> UNTUK REVISI
        }//EOF simpan
    }

    protected function maxKodeSimbada() {
        $c = new Criteria();
        $c->clearSelectColumns();
        $c->addSelectColumn('MAX(' . masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA . ') ');
        $c->add(masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA, '%SIM%', Criteria::ILIKE);
        $rs = masterLokasiSimbadaPeer::doSelectRS($c);
        $rs->next();
//print_r($max);exit;
        $kodesimbada = $rs->get(1);
        ;
        /*

          $sql = "select max(kode_lokasi_simbada) as kode_simbada from gis_budgeting.public.master_lokasi_simbada where kode_sub ilike 'SIM%'";
          $con=Propel::getConnection();
          $stmt=$con->prepareStatement($sql);
          $rs=$stmt->executeQuery();
          while($rs->next())
          {
          $kodesimbada = $rs->getString('kode_simbada');
      } */
      $kode = substr($kodesimbada, 3, 5);
      $kode+=1;
      if ($kode < 10) {
        $kodesimbada = 'SIM0000' . $kode;
    } elseif ($kode < 100) {
        $kodesimbada = 'SIM000' . $kode;
    } elseif ($kode < 1000) {
        $kodesimbada = 'SIM00' . $kode;
    } elseif ($kode < 10000) {
        $kodesimbada = 'SIM0' . $kode;
    } elseif ($kode < 100000) {
        $kodesimbada = 'SIM' . $kode;
    }
    return $kodesimbada;
}

public function executeLokasiBaru() {
    $kec = new Criteria();
    $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
    $rs_kec = KecamatanPeer::doSelect($kec);
    $this->nama_kecamatan_kel = $rs_kec;

    if ($this->getRequest()->getMethod() == sfRequest::POST) {
        $lokasi_jalan = $this->getRequestParameter('lokasi_jalan');
        $tipe_gang = $this->getRequestParameter('tipe_gang');
        $lokasi_gang = $this->getRequestParameter('lokasi_gang');
        $lokasi_nomor = $this->getRequestParameter('lokasi_nomor');
        $lokasi_rw = $this->getRequestParameter('lokasi_rw');
        $lokasi_rt = $this->getRequestParameter('lokasi_rt');
        $lokasi_tempat = $this->getRequestParameter('lokasi_tempat');
        $lokasi_keterangan = $this->getRequestParameter('lokasi_keterangan');
        $lokasi_kec = $this->getRequestParameter('kecamatan');
        $lokasi_kel = $this->getRequestParameter('kelurahan');

        $unit_id = $this->getUser()->getUnitId();

        if ($lokasi = $this->gabungLokasi($lokasi_jalan, $lokasi_gang, $tipe_gang, $lokasi_nomor, $lokasi_rw, $lokasi_rt, $lokasi_keterangan, $lokasi_tempat)) {
            $kec = new Criteria();
            $kec->add(KecamatanPeer::ID, $lokasi_kec);
            $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
            $rs_kec = KecamatanPeer::doSelectOne($kec);
            if ($rs_kec) {
                $kecamatan = $rs_kec->getNama();
            }

            $kel = new Criteria();
            $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
            $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
            $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
            if ($rs_kel) {
                $kelurahan = $rs_kel->getNamaKelurahan();
            } else {
                $kecamatan = '';
                $kelurahan = '';
            }

            if ($lokasi == '') {
                $this->setFlash('gagal', 'Nama Jalan atau Nama Bangunan harus terisi salah satu');
                $this->redirect('entri/lokasiList');
            } elseif ($kecamatan == '' || $kelurahan == '') {
                $this->setFlash('gagal', 'Kecamatan dan Kelurahan belum dipilih');
                $this->redirect('entri/lokasiList');
            }

            if (trim($tipe_gang) <> '') {
                $tipe_gang_fix = strtoupper(trim($tipe_gang)) . '. ';
            } else {
                $tipe_gang_fix = 'GG. ';
            }
            if (trim($lokasi_gang) <> '') {
                $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang)) . ' ';
            }

            $new_lokasi = new MasterLokasi();
            $new_lokasi->setTahun(sfConfig::get('app_default_tahun'));
            $new_lokasi->setStatusHapus(false);
            $new_lokasi->setJalan(strtoupper(trim($lokasi_jalan)));
            $new_lokasi->setGang($gang_fix);
            $new_lokasi->setNomor(strtoupper(trim($lokasi_nomor)));
            $new_lokasi->setRw(strtoupper(trim($lokasi_rw)));
            $new_lokasi->setRt(strtoupper(trim($lokasi_rt)));
            $new_lokasi->setKeterangan(strtoupper(trim($lokasi_keterangan)));
            $new_lokasi->setTempat(strtoupper(trim($lokasi_tempat)));
            $new_lokasi->setKecamatan($kecamatan);
            $new_lokasi->setKelurahan($kelurahan);
            $new_lokasi->setLokasi($lokasi);
            $new_lokasi->setStatusVerifikasi(false);
            $new_lokasi->setUsulanSkpd($unit_id);
            $new_lokasi->save();

            $this->setFlash('berhasil', 'Lokasi ' . $lokasi . ' telah tersimpan');
            $this->redirect('entri/lokasiList');
        }
        $this->setFlash('gagal', 'Jalan/tempat harus diisi salah satu');
        $this->redirect('entri/lokasiList');
    }
}

public function executePreviewLokasi() {
    $lokasi_jalan = $this->getRequestParameter('lokasi_jalan');
    $tipe_gang = $this->getRequestParameter('tipe_gang');
    $lokasi_gang = $this->getRequestParameter('lokasi_gang');
    $lokasi_nomor = $this->getRequestParameter('lokasi_nomor');
    $lokasi_rw = $this->getRequestParameter('lokasi_rw');
    $lokasi_rt = $this->getRequestParameter('lokasi_rt');
    $lokasi_tempat = $this->getRequestParameter('lokasi_tempat');
    $lokasi_keterangan = $this->getRequestParameter('lokasi_keterangan');
    $lokasi_kec = $this->getRequestParameter('kecamatan');
    $lokasi_kel = $this->getRequestParameter('kelurahan');

    $kec = new Criteria();
    $kec->add(KecamatanPeer::ID, $lokasi_kec);
    $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
    $rs_kec = KecamatanPeer::doSelectOne($kec);
    if ($rs_kec) {
        $kecamatan = $rs_kec->getNama();
    }

    $kel = new Criteria();
    $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
    $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
    $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
    if ($rs_kel) {
        $kelurahan = $rs_kel->getNamaKelurahan();
    } else {
        $kecamatan = '';
        $kelurahan = '';
    }

    $lokasi = $this->gabungLokasi($lokasi_jalan, $lokasi_gang, $tipe_gang, $lokasi_nomor, $lokasi_rw, $lokasi_rt, $lokasi_keterangan, $lokasi_tempat);

    if ($lokasi <> '' && $kecamatan <> '' && $kelurahan <> '') {
        echo $lokasi . '<br>Kecamatan ' . $kecamatan . ', Kelurahan ' . $kelurahan;
    } elseif ($lokasi == '') {
        echo 'Nama Jalan atau Nama Bangunan harus terisi salah satu';
    } elseif ($kecamatan == '' || $kelurahan == '') {
        echo 'Kecamatan dan Kelurahan belum dipilih';
    }
    exit;
}

protected function gabungLokasi($lokasi_jalan, $lokasi_gang, $tipe_gang, $lokasi_nomor, $lokasi_rw, $lokasi_rt, $lokasi_keterangan, $lokasi_tempat) {
    $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $lokasi_jalan));
    $lokasi_gang = str_replace('\'', '', str_replace('"', '', $lokasi_gang));
    $tipe_gang = str_replace('\'', '', str_replace('"', '', $tipe_gang));
    $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $lokasi_nomor));
    $lokasi_rw = str_replace('\'', '', str_replace('"', '', $lokasi_rw));
    $lokasi_rt = str_replace('\'', '', str_replace('"', '', $lokasi_rt));
    $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $lokasi_keterangan));
    $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $lokasi_tempat));

    $jalan_fix = '';
    $gang_fix = '';
    $tipe_gang_fix = '';
    $nomor_fix = '';
    $rw_fix = '';
    $rt_fix = '';
    $keterangan_fix = '';
    $tempat_fix = '';
    if (trim($lokasi_jalan) <> '' || trim($lokasi_tempat) <> '') {
        if (trim($lokasi_jalan) <> '') {
            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan)) . ' ';
        }
        if (trim($lokasi_tempat) <> '') {
            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat)) . ') ';
        }
        if (trim($tipe_gang) <> '') {
            $tipe_gang_fix = strtoupper(trim($tipe_gang)) . '. ';
        } else {
            $tipe_gang_fix = 'GG. ';
        }
        if (trim($lokasi_gang) <> '') {
            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang)) . ' ';
        }
        if (trim($lokasi_nomor) <> '') {
            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor)) . ' ';
        }
        if (trim($lokasi_rw) <> '') {
            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw)) . ' ';
        }
        if (trim($lokasi_rt) <> '') {
            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt)) . ' ';
        }
        if (trim($lokasi_keterangan) <> '') {
            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan)) . ' ';
        }
        return $tempat_fix . $jalan_fix . $gang_fix . $nomor_fix . $rw_fix . $rt_fix . $keterangan_fix;
    }
    return null;
}

public function executeLokasiHapus() {
    $id_lokasi = $this->getRequestParameter('id_lokasi');
    $c = new Criteria();
    $c->add(MasterLokasiPeer::ID_LOKASI, $id_lokasi);
    if ($dapat_lokasi = MasterLokasiPeer::doSelectOne($c)) {
        $lokasi = $dapat_lokasi->getLokasi();

        $dapat_lokasi->setStatusHapus(TRUE);
        $dapat_lokasi->save();

        $this->setFlash('berhasil', 'Lokasi ' . $lokasi . ' telah dihapus');
        $this->redirect('entri/lokasiList');
    }
}

public function executeLokasiList() {
    $this->processSort();
    $this->processFilters();
    $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

    $this->unit_id = $unit_id = $this->getUser()->getUnitId();


    $pagers = new sfPropelPager('MasterLokasi', 20);
    $c = new Criteria();
    $c->add(MasterLokasiPeer::STATUS_HAPUS, FALSE);
    $c->addAnd(MasterLokasiPeer::USULAN_SKPD, $unit_id);
    $c->addDescendingOrderByColumn(MasterLokasiPeer::STATUS_VERIFIKASI);
    $c->addAscendingOrderByColumn(MasterLokasiPeer::LOKASI);
    $c->addAscendingOrderByColumn(MasterLokasiPeer::KECAMATAN);
    $c->addAscendingOrderByColumn(MasterLokasiPeer::KELURAHAN);
    $this->addFiltersCriteriaLokasiList($c);

    $pagers->setCriteria($c);
    $pagers->setPage($this->getRequestParameter('page', 1));
    $pagers->init();
    $this->pager = $pagers;
}

protected function addFiltersCriteriaLokasiList($c) {
    if (isset($this->filters['jalan']) && $this->filters['jalan'] !== '') {
        $c->add(MasterLokasiPeer::JALAN, '%' . $this->filters['jalan'] . '%', Criteria::ILIKE);
    }
    if (isset($this->filters['lokasi']) && $this->filters['lokasi'] !== '') {
        $c->add(MasterLokasiPeer::LOKASI, '%' . $this->filters['lokasi'] . '%', Criteria::ILIKE);
    }
}

public function executeBuatbaru() {
    if ($this->getRequestParameter('baru') == md5('terbaru')) {
        $user = $this->getUser();

//sholeh begin
        $lokasiSession = $user->getAttribute('nama', '', 'lokasi');
        $this->lokasiSession = $lokasiSession;

        $user->setAttribute('nama', '', 'lokasi');
        $user->setAttribute('nama', '', 'lokasi_baru');
//sholeh end
//bisma begin
        $simbadaSession = $user->getAttribute('nama', '', 'simbada');
        $this->simbadaSession = $simbadaSession;

        $user->setAttribute('nama', '', 'simbada');
        $user->setAttribute('nama', '', 'simbada_baru');
//bisma end

        $user->removeCredential('lokasi');
        $user->removeCredential('simbada');

        $this->kode_kegiatan = $kode_kegiatan = $this->getRequestParameter('kegiatan');
        $this->unit_id = $unit_id = $this->getRequestParameter('unit');
        $pajak = $this->getRequestParameter('pajak');
        $komponen_id = $this->getRequestParameter('komponen');
        $volume_orang = $this->getRequestParameter('volume_orang');            
        $satuan = $this->getRequestParameter('satuan');
//print_r($komponen_id);exit;
        $tipe = $this->getRequestParameter('tipe');
        $kode_rekening = $this->getRequestParameter('rekening');

        if ($kode_rekening == '0') {
            $this->setFlash('gagal', 'Kode rekening belum dipilih');
            $cari = $this->getRequestParameter('cari');
            $this->redirect("entri/carikomponen?filters[nama_komponen]=$cari&kegiatan=$kode_kegiatan&unit=$unit_id&filter=cari");
        }

        $komponen_id = trim($komponen_id);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id, Criteria::ILIKE);
        $rs_komponen = KomponenPeer::doSelectOne($c);
        if ($rs_komponen) {

            $this->rs_komponen = $rs_komponen;
        }
        $arr_buka_approve_jk = array('');
        $arr_iuran_jk = array("2.1.1.01.01.01.004.016", "2.1.1.01.01.01.004.017", "2.1.1.01.01.01.004.018");
        if(!in_array($kode_kegiatan, $arr_buka_approve_jk)) {
            if(in_array($komponen_id, $arr_iuran_jk)) {
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);

                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasRincianDetailPeer::KOMPONEN_ID, $komponen_id);
                $c->add(DinasRincianDetailPeer::REKENING_CODE, $kode_rekening);
                $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                $rs_count_komponen = DinasRincianDetailPeer::doCount($c);

                if($rs_count_komponen > 0) {
                    $this->setFlash('gagal', 'Mohon maaf, komponen '.$rs_komponen->getKomponenName().' sudah terambil di kegiatan ini, silahkan edit komponen tersebut.');    
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                }
            }
        }

//print_r($kode_rekening);exit;
        $sub_koRek = substr($kode_rekening, 0, 5);
//bisma : contreng
            /*  if($sub_koRek=='5.2.3' || $kode_rekening=='5.2.2.01.01' || $kode_rekening=='5.2.2.01.03' || $kode_rekening=='5.2.2.01.10' || $kode_rekening=='5.2.2.01.10' || $kode_rekening=='5.2.2.01.14'
              || $kode_rekening=='5.2.2.01.15' || $kode_rekening=='5.2.2.01.16' || $kode_rekening=='5.2.2.01.18' || $kode_rekening=='5.2.2.01.19' || $kode_rekening=='5.2.2.02.01'
              || $kode_rekening=='5.2.2.02.02' || $kode_rekening=='5.2.2.02.05' || $kode_rekening=='5.2.2.19.01' || $kode_rekening=='5.2.2.19.02' || $kode_rekening=='5.2.2.19.03'
              || $kode_rekening=='5.2.2.19.04')

             */
//if($sub_koRek=='5.2.3')
              if ($sub_koRek == '5.2.3' || $sub_koRek == '5.2.2' || $kode_rekening == '5.2.2.01.01' || $kode_rekening == '5.2.2.01.03' || $kode_rekening == '5.2.2.01.10' || $kode_rekening == '5.2.2.01.10' || $kode_rekening == '5.2.2.01.14' || $kode_rekening == '5.2.2.01.15' || $kode_rekening == '5.2.2.01.16' || $kode_rekening == '5.2.2.01.18' || $kode_rekening == '5.2.2.01.19' || $kode_rekening == '5.2.2.02.01' || $kode_rekening == '5.2.2.02.02' || $kode_rekening == '5.2.2.02.05' || $kode_rekening == '5.2.2.19.01' || $kode_rekening == '5.2.2.19.02' || $kode_rekening == '5.2.2.19.03' || $kode_rekening == '5.2.2.19.04') {

                $c_penyusun = new Criteria();
                $c_penyusun->add(KomponenPeer::RKA_MEMBER, TRUE);
                $c_penyusun->add(KomponenPeer::KOMPONEN_ID, '23.01.01.03.%', Criteria::ILIKE);
                $c_penyusun->setLimit(25);
                $c_penyusun->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
                $rs_penyusun = KomponenPeer::doSelect($c_penyusun);
                $this->rs_penyusun = $rs_penyusun;


                //tambahan: pakai akrual komponen penyusun
                $c_komponen_penyusun = new Criteria();
                $c_komponen_penyusun->addAscendingOrderByColumn(AkrualKomponenPenyusunanPeer::KODE_AKRUAL_KOMPONEN_PENYUSUN);
                $this->rs_komponen_penyusun = $rs_komponen_penyusun = AkrualKomponenPenyusunanPeer::doSelect($c_komponen_penyusun);
            }

            // $query_ambil_detno = 
            // "SELECT detail_no
            // FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
            // WHERE unit_id  = '".$unit_id."'
            // AND kegiatan_code = '".$kode_kegiatan."'";
            // $con = Propel::getConnection();
            // $stmt = $con->prepareStatement($query_ambil_detno);
            // $rs_data_existed = $stmt->executeQuery();

            // $detail_no_terpakai = array();
            // while ($rs_data_existed->next()) {
            //     array_push($detail_no_terpakai, $rs_data_existed->getInt('detail_no'));
            // }

            // ambil data sisa lelang
            $query_sisa_pengadaan = 
            "SELECT
            drd.kegiatan_code,
            drd.unit_id,
            drd.detail_no,
            drd.komponen_id,
            drd.komponen_name,
            drd.detail_name,
            rka.nilai_anggaran - drd.nilai_anggaran AS sisa,
            drd.detail_kegiatan
            FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
            INNER JOIN " . sfConfig::get('app_default_schema') . ".rincian_detail rka ON drd.unit_id = rka.unit_id AND drd.kegiatan_code = rka.kegiatan_code AND drd.detail_no = rka.detail_no
            WHERE drd.unit_id = '$unit_id'            
            AND drd.satuan = 'Paket'
            AND drd.volume = 1
            AND drd.status_lelang = 'lock'
            AND drd.rekening_code ILIKE '$sub_koRek%'
            AND drd.status_hapus = FALSE
            AND rka.nilai_anggaran > drd.nilai_anggaran";
            //AND drd.kegiatan_code = '$kode_kegiatan' tutup dulu

            // if(count($detail_no_terpakai))
            //     $query_sisa_pengadaan .= " AND drd.detail_no NOT IN (".implode(',', $detail_no_terpakai).")";
            
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_sisa_pengadaan);
            $rs_sisapengadaan = $stmt->executeQuery();
            $this->rs_sisapengadaan = $rs_sisapengadaan;

            $i = 0;
            while ($rs_sisapengadaan->next()) {
                // count sisa
                $query =
                "SELECT SUM(sisa_anggaran) AS nilai_hpsp
                FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
                WHERE unit_id = '".$rs_sisapengadaan->getString('unit_id')."'
                AND kegiatan_code = '".$rs_sisapengadaan->getString('kegiatan_code')."'
                AND detail_no = ".$rs_sisapengadaan->getString('detail_no')."";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_total_hpsp = $stmt->executeQuery();
                $nilai_hpsp = 0;
                if ($rs_total_hpsp->next()) {
                    $nilai_hpsp = $rs_total_hpsp->getString('nilai_hpsp');
                }

                if($rs_sisapengadaan->getString('sisa') - $nilai_hpsp > sfConfig::get('app_default_bataspsp')) {
                    $data_oper_arr[$i]['uid'] = $rs_sisapengadaan->getString('unit_id');
                    $data_oper_arr[$i]['keg_code'] = $rs_sisapengadaan->getString('kegiatan_code');
                    $data_oper_arr[$i]['det_no'] = $rs_sisapengadaan->getString('detail_no');
                    $data_oper_arr[$i]['kom_id'] = $rs_sisapengadaan->getString('komponen_id');
                    $data_oper_arr[$i]['kom_name'] = $rs_sisapengadaan->getString('komponen_name');
                    $data_oper_arr[$i]['det_name'] = $rs_sisapengadaan->getString('detail_name');
                    $data_oper_arr[$i]['sisa'] = $rs_sisapengadaan->getString('sisa') - $nilai_hpsp;
                    $data_oper_arr[$i]['det_keg_asal'] = $rs_sisapengadaan->getString('detail_kegiatan');
                }

                $i++;
            }
            $this->data_oper = $data_oper_arr;

            $query_arahan = 
            "SELECT id, unit_id, kegiatan_code, kode_belanja, uraian, anggaran, status
            FROM " . sfConfig::get('app_default_schema') . ".arahan_belanja_kegiatan
            WHERE unit_id = '".$unit_id."'
            AND kegiatan_code = '".$kode_kegiatan."'
            AND status = 0";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_arahan);
            $rs_arahan = $stmt->executeQuery();
            if($rs_arahan->next())
                $this->rs_arahan = $rs_arahan;
            // end of query sisa pengadaan

            $f = new Criteria();
            $f->addAscendingOrderByColumn(MasterSumberDanaPeer::ID);
            $rs_sumberdana = MasterSumberDanaPeer::doSelect($f);
            $this->$rs_sumber = $rs_sumberdana;

            // ambil komponen JK, JKK, JKN
            $crit = new Criteria();
            $crit->add(KomponenPeer::IS_IURAN_BPJS, TRUE);
            $data_tenaga_opr = KomponenPeer::doSelect($crit);
            $this->data_tenaga_opr = $data_tenaga_opr;

            $d = new Criteria();
            $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            // $d->add(DinasSubtitleIndikatorPeer::PRIORITAS, '0', Criteria::NOT_EQUAL);
            $d->add(DinasSubtitleIndikatorPeer::LOCK_SUBTITLE, FALSE);
            $d->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = DinasSubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $g = new Criteria();
            $g->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $g->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $g->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($g);

            // $g = new Criteria();
            // $g->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            // $sub = "char_length(lokasi)>10";
            // $g->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            // $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            // $rs_lokasi = HistoryPekerjaanV2Peer::doSelect($g);
            // // $lokasi = array();
            // // foreach ($rs_lokasi as $key => $value) {
            // //     array_push($lokasi, $value->getLokasi());
            // // }
            // $this->lokasi = $rs_lokasi;

            $kec = new Criteria();
            $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
            $rs_kec = KecamatanPeer::doSelect($kec);
            $this->nama_kecamatan_kel = $rs_kec;

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                $this->est_fisik = $rs_est_fisik->getIsEstFisik();

            $c = new Criteria();
            $c->add(RekeningPeer::REKENING_CODE, $kode_rekening);
            if ($rs_rekening = RekeningPeer::doSelectOne($c)) {
                $akrual_rek = $rs_rekening->getAkrualKonstruksi();
            }
            $sub = "char_length(akrual_code)>10";
            $c = new Criteria();
            $c->add(AkrualPeer::AKRUAL_CODE, $sub, Criteria::CUSTOM);
            if ($akrual_rek) {
                $c->addAnd(AkrualPeer::AKRUAL_CODE, $akrual_rek . '%', Criteria::ILIKE);
            } elseif ($rs_komponen->getKomponenTipe2() == 'ATRIBUSI' || $rs_komponen->getKomponenTipe2() == 'PERENCANAAN' || $rs_komponen->getKomponenTipe2() == 'PENGAWASAN') {
                $c->addAnd(AkrualPeer::AKRUAL_CODE, '1.3.1%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.3%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.4%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.5%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.5.3%', Criteria::ILIKE);
            }
            $c->addAscendingOrderByColumn(AkrualPeer::AKRUAL_CODE);
            $rs_akrualcode = AkrualPeer::doSelect($c);
            $this->rs_akrualcode = $rs_akrualcode;
        }
    }

    public function executeDetailKomponenPenyusun() {
        $komponen_id = $this->getRequestParameter('komponen');
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $dapat = KomponenPeer::doSelectOne($c);
        echo $dapat->getKomponenHarga() . '|' . $dapat->getSatuan();
        exit;
    }

    public function executeCarikomponen() {
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');

        $this->filters = $this->getRequestParameter('filters');
        if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
            $cari = $this->filters['nama_komponen'];
        }
        $this->cari = $cari;

//print_r($this->getRequestParameter('kegiatan'));exit;
    }

    public function executeSearchkomponenedit() {
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');

        $this->filters = $this->getRequestParameter('filters');
        if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
            $cari = $this->filters['nama_komponen'];
        }
        $this->cari = $cari;               
          $queryku = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        where komponen_name ilike '%" . $cari . "%' and kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and status_hapus=false order by detail_kegiatan";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();         
            $this->rs_rd = $rcsb;
    }

    public function executePilihsubx() {
        $x = new Criteria();
        $x->addSelectColumn(DinasRincianDetailPeer::SUBTITLE);
        $x->add(DinasRincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
        $x->add(DinasRincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
        if ($this->getRequestParameter('b') != '') {
            $s = new Criteria();
            $s->add(DinasRincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
            $s->add(DinasRincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
            $s->setDistinct();
            $r = DinasRincianDetailPeer::doselect($s);
            $kodekegiatan = $this->getRequestParameter('kegiatan_code');
            $unitid = $this->getRequestParameter('unit_id');
            $kode_sub = $this->getRequestParameter('b');

            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_sub);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $queryku = "select distinct kode_sub,sub from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            where subtitle ilike '" . $subtitle . "%' and kegiatan_code='$kodekegiatan' and unit_id='$unitid' order by sub";


            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();
            $arr_tampung[''] = '---Pilih Subtitle Dulu---';
            $arr_tampung['nama'] = '[Sub Subtitle Baru]';

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('sub') != '') {
                    $arr_tampung[$rcsb->getString('kode_sub')] = $rcsb->getString('sub');
                }
            }
            $this->isi_baru = $arr_tampung;
        }
    }

    public function executeEditKegiatan() {

        $user = $this->getUser();
        $user->removeCredential('lokasi');
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('id');
        $komponen = $this->getRequestParameter('komponen');
        $detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no;

        $status_bypass_validasi = FALSE;
        if(sfConfig::get('app_tahap_edit') == 'murni') {
            $status_bypass_validasi = TRUE;
        }
        // SEMENTARA SAJA, INGAT DICOMMENT KEMBALI!!!!!
        // DINAS PU, MAU GANTI REKENING Biaya Counter Mesin Fotocopy DAN Biaya Counter Mesin Fotocopy Warna DARI (5.2.2.20.06 Belanja Pemeliharaan Alat Kantor dan Rumah Tangga) KE (   5.2.2.10.01 Belanja Sewa Perlengkapan / Peralatan Kantor)
        if (in_array($kegiatan_code, array(''))) {
            $status_bypass_validasi = TRUE;
        }

        $arr_buka_validasi_realisasi = array();
                $query =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".bypass_detail";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_validasi_realisasi, $rs->getString('detail_kegiatan'));
                }              
        if (in_array($detail_kegiatan,  $arr_buka_validasi_realisasi)) {
            $status_bypass_validasi = TRUE;
        }
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            // var_dump($status_bypass_validasi);die();

            // flag utk by pass validasi edelivery

            $sub_koRek = '';

            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_rinciandetail = DinasRincianDetailPeer::doSelectOne($c);
            if ($rs_rinciandetail) {
                // validasi output, musrenbang, prioritas
                $arr_buka_edit_output = array();
                $query =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    array_push($arr_buka_edit_output, $rs->getString('detail_kegiatan'));
                }
                if(!in_array($rs_rinciandetail->getDetailKegiatan(), $arr_buka_edit_output)) {
                    $is_output = $rs_rinciandetail->getIsOutput();
                    $is_musrenbang = $rs_rinciandetail->getIsMusrenbang();
                    $is_urgent = $rs_rinciandetail->getPrioritasWali();
                    if($is_output) {
                        $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen output, tidak bisa diedit.');
                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                    // if($is_musrenbang) {
                    //     $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen musrenbang, tidak bisa diedit.');
                    //     return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    // }
                    if($is_urgent) {
                        $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen prioritas, tidak bisa diedit.');
                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                }

                $sub_koRek = substr($rs_rinciandetail->getRekeningCode(), 0, 5);
                $volumeDiBudgeting = $rs_rinciandetail->getVolume();
                $rekening_code = $rs_rinciandetail->getRekeningCode();
                $koRek = substr($rs_rinciandetail->getKomponenId() , 0, 18);
                if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($rs_rinciandetail->getSatuan() == 'Orang Bulan'))
                {                        
                // if($rekening_code=='5.1.02.02.01.080' or $rekening_code=='5.2.1.02.02')
                // {
                    $volume_orang=$rs_rinciandetail->getVolumeOrangAnggaran();
                }           
                else
                {
                   $volume_orang = 0;
               }               
               $pajak = $rs_rinciandetail->getPajak();
               $jasmas = $rs_rinciandetail->getKecamatan();
               $harga = $rs_rinciandetail->getKomponenHargaAwal();
               $nilaiDiBudgeting = $harga * $volumeDiBudgeting * (100 + $pajak) / 100;
               $this->nilaiMax = $nilaiDiBudgeting - $warning;
               $this->rs_rinciandetail = $rs_rinciandetail;

               $c = new Criteria();
               $c->add(KomponenPeer::KOMPONEN_ID, $rs_rinciandetail->getKomponenId());
               if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                $this->est_fisik = $rs_est_fisik->getIsEstFisik();
            else
                $this->est_fisik = false;

        }

            //jikamurni saja
            // $c = new Criteria();
            // $c->add(KomponenPeer::KOMPONEN_ID, $komponen);          
            // $rs_cek_komponen = KomponenPeer::doSelectOne($c);
            // $k = count($rs_cek_komponen) ;
            // // print_r($rs_cek_komponen);
            // // die();
            //  if($k == 0) {
            //     $this->setFlash('gagal', 'Mohon maaf, komponen ini sudah tidak digunakan lagi.Mohon hapus dan ambil komponen lain.');
            //     return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            // }


        $this->id = $detail_no;

        $f = new Criteria();
        $f->addAscendingOrderByColumn(MasterSumberDanaPeer::ID);
        $rs_sumberdana = MasterSumberDanaPeer::doSelect($f);
        $this->$rs_sumber = $rs_sumberdana;

        $d = new Criteria();
        $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
        $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
        $d->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
        $rs_subtitleindikator = DinasSubtitleIndikatorPeer::doSelect($d);
        $this->rs_subtitleindikator = $rs_subtitleindikator;

        $z = new Criteria();
        $z->add(DinasRkaMemberPeer::UNIT_ID, $unit_id);
        $z->add(DinasRkaMemberPeer::KEGIATAN_CODE, $kegiatan_code);
        $z->addAscendingOrderByColumn(DinasRkaMemberPeer::KODE_SUB);
        $rs_sub_subtitleindikator = DinasRkaMemberPeer::doSelect($z);
        $this->rs_sub_subtitleindikator = $rs_sub_subtitleindikator;

        $e = new Criteria();
        $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
        $rs_satuan = SatuanPeer::doSelect($e);
        $this->rs_satuan = $rs_satuan;

        $f = new Criteria();
//$f->add(JasmasPeer::KODE_JASMAS, $jasmas);
        $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
        $rs_jasmas = JasmasPeer::doSelect($f);
        $this->rs_jasmas = $rs_jasmas;

        $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $g = new Criteria();
        $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
        $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
        $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
        $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
        $geojson = HistoryPekerjaanV2Peer::doSelect($g);
        if ($geojson) {
            $this->rs_geojson = $geojson;
        }

        $h = new Criteria();
        $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
        $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
        $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
        $sub = "char_length(lokasi)>10";
        $h->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
        $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
        $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);

            // $query_ambil_detno = 
            // "SELECT detail_no
            // FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
            // WHERE unit_id  = '".$unit_id."'
            // AND kegiatan_code = '".$kegiatan_code."'
            // AND id_komponen_lelang <> '".$kode_rka."'";
            // $con = Propel::getConnection();
            // $stmt = $con->prepareStatement($query_ambil_detno);
            // $rs_data_existed = $stmt->executeQuery();

            // $detail_no_terpakai = array();
            // while ($rs_data_existed->next()) {
            //     array_push($detail_no_terpakai, $rs_data_existed->getInt('detail_no'));
            // }

        $query_sisa_pengadaan = 
        "SELECT
        drd.kegiatan_code,
        drd.unit_id,
        drd.detail_no,
        drd.komponen_id,
        drd.komponen_name,
        drd.detail_name,
        rka.nilai_anggaran - drd.nilai_anggaran AS sisa,
        drd.detail_kegiatan
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
        INNER JOIN " . sfConfig::get('app_default_schema') . ".rincian_detail rka ON drd.unit_id = rka.unit_id AND drd.kegiatan_code = rka.kegiatan_code AND drd.detail_no = rka.detail_no
        WHERE drd.unit_id = '$unit_id'        
        AND drd.satuan = 'Paket'
        AND drd.volume = 1
        AND drd.status_lelang = 'lock'
        AND drd.rekening_code ILIKE '$sub_koRek%'
        AND drd.status_hapus = FALSE
        AND rka.nilai_anggaran > drd.nilai_anggaran";
        //AND drd.kegiatan_code = '$kegiatan_code' (psp per pd)

            // if(count($detail_no_terpakai))
            //     $query_sisa_pengadaan .= " AND drd.detail_no NOT IN (".implode(',', $detail_no_terpakai).")";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query_sisa_pengadaan);
        $rs_sisapengadaan = $stmt->executeQuery();
            // $this->rs_sisapengadaan = $rs_sisapengadaan;

        $i = 0;
        $data_oper_arr = array();

        $query_cek_is_psp =
        "SELECT COUNT(*) AS nilai
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
        WHERE detail_kegiatan = '$kode_rka'
        AND drd.satuan = 'Paket'
        AND drd.volume = 1
        AND drd.status_lelang = 'lock'";
        $con_cek_is_psp = Propel::getConnection();
        $stmt_cek_is_psp = $con_cek_is_psp->prepareStatement($query_cek_is_psp);
        $rs_cek_is_psp = $stmt_cek_is_psp->executeQuery();
        $jml_is_psp = 0;
        if($rs_cek_is_psp->next()) {
            $jml_is_psp = $rs_cek_is_psp->getFloat('nilai');
        }
            // if($jml_is_psp <= 0) {
        while ($rs_sisapengadaan->next()) {
                    // jika ada di dalam dinas_rincian_hpsp
                    // count sisa
            $query =
            "SELECT SUM(sisa_anggaran) AS nilai_hpsp
            FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
            WHERE unit_id = '".$rs_sisapengadaan->getString('unit_id')."'
            AND kegiatan_code = '".$rs_sisapengadaan->getString('kegiatan_code')."'
            AND detail_no = ".$rs_sisapengadaan->getString('detail_no')."
            AND id_komponen_lelang <> '".$kode_rka."'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_total_hpsp = $stmt->executeQuery();
            $nilai_hpsp = 0;
            if ($rs_total_hpsp->next()) {
                $nilai_hpsp = $rs_total_hpsp->getFloat('nilai_hpsp');
            }
            if(empty($nilai_hpsp))
                $nilai_hpsp = 0;

            if($rs_sisapengadaan->getFloat('sisa') - $nilai_hpsp > sfConfig::get('app_default_bataspsp')) {
                $data_oper_arr[$i]['uid'] = $rs_sisapengadaan->getString('unit_id');
                $data_oper_arr[$i]['keg_code'] = $rs_sisapengadaan->getString('kegiatan_code');
                $data_oper_arr[$i]['det_no'] = $rs_sisapengadaan->getString('detail_no');
                $data_oper_arr[$i]['kom_id'] = $rs_sisapengadaan->getString('komponen_id');
                $data_oper_arr[$i]['kom_name'] = $rs_sisapengadaan->getString('komponen_name');
                $data_oper_arr[$i]['det_name'] = $rs_sisapengadaan->getString('detail_name');
                $data_oper_arr[$i]['sisa'] = $rs_sisapengadaan->getString('sisa') - $nilai_hpsp;
                $data_oper_arr[$i]['det_keg_asal'] = $rs_sisapengadaan->getString('detail_kegiatan');
            }

            $i++;
        }
            // }
        $this->data_oper = $data_oper_arr;

        $query_hpsp = 
        "SELECT drh.unit_id, drh.kegiatan_code, drh.komponen_id, drd.komponen_name, drd.detail_name, drh.detail_no, id_komponen_lelang, sisa_anggaran, catatan, drh.tahap
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp drh
        INNER JOIN " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd ON drh.unit_id = drd.unit_id AND drh.kegiatan_code = drd.kegiatan_code AND drh.detail_no = drd.detail_no
        WHERE drh.unit_id  = '".$unit_id."'        
        AND id_komponen_lelang = '".$kode_rka."'";
        //AND drh.kegiatan_code = '".$kegiatan_code."' tutup jika per PD
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query_hpsp);
        $rs_data_existed = $stmt->executeQuery();

        $data_existed = array();
        while ($rs_data_existed->next()) {
            $key = $rs_data_existed->getString('unit_id').'.'.$rs_data_existed->getString('kegiatan_code').'.'.$rs_data_existed->getString('detail_no');

            $data_existed[$key]['unit_id'] = $rs_data_existed->getString('unit_id');
            $data_existed[$key]['kegiatan_code'] = $rs_data_existed->getString('kegiatan_code');
            $data_existed[$key]['komponen_id'] = $rs_data_existed->getString('komponen_id');
            $data_existed[$key]['komponen_name'] = $rs_data_existed->getString('komponen_name');
            $data_existed[$key]['detail_name'] = $rs_data_existed->getString('detail_name');
            $data_existed[$key]['detail_no'] = $rs_data_existed->getString('detail_no');
            $data_existed[$key]['id_komponen_lelang'] = $rs_data_existed->getString('id_komponen_lelang');
            $data_existed[$key]['sisa_anggaran'] = $rs_data_existed->getString('sisa_anggaran');
            $data_existed[$key]['catatan'] = $rs_data_existed->getString('catatan');
            $data_existed[$key]['tahap'] = $rs_data_existed->getString('tahap');
        }
        $this->data_existed = $data_existed;

        $query_arahan = 
        "SELECT id, unit_id, kegiatan_code, kode_belanja, uraian, anggaran, status
        FROM " . sfConfig::get('app_default_schema') . ".arahan_belanja_kegiatan
        WHERE unit_id = '".$unit_id."'
        AND kegiatan_code = '".$kegiatan_code."'
        AND status = 0";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query_arahan);
        $rs_arahan = $stmt->executeQuery();
        if($rs_arahan->next())
            $this->rs_arahan = $rs_arahan;
    }

    if ($this->getRequestParameter('cari') == 'cari') {
//print_r($this->getRequestParameter('lokasi'));exit;
        $user = $this->getUser();
        $user->removeCredential('lokasi');
        $user->addCredential('lokasi');
        $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
        $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
        $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
        $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
        $user->setAttribute('ubah', 'ubah', 'lokasi');

        return $this->forward('lokasi', 'list');
        }//end of cari
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');
            $sisaLelang = $this->getRequestParameter('lelang');
            $detail=$unit_id.'.'.$kegiatan_code.'.'.$detail_no; 
            $harga = $this->getRequestParameter('harga');
            $jumlah_suami_istri =  $this->getRequestParameter('jumlah_suami_istri');
            $jumlah_anak =  $this->getRequestParameter('jumlah_anak');
            $komponen_name =  $this->getRequestParameter('nama_komponen');


            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincian_detail->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $data_rincian_detail = DinasRincianDetailPeer::doSelectOne($c_rincian_detail);

            $rekening=$data_rincian_detail->getRekeningCode();
            
            $arr_buka_edit_output = array('');
            $query =
            "SELECT detail_kegiatan
            FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                array_push($arr_buka_edit_output, $rs->getString('detail_kegiatan'));
            }
            if(!in_array($data_rincian_detail->getDetailKegiatan(), $arr_buka_edit_output)) {
                $is_output = $data_rincian_detail->getIsOutput();
                $is_musrenbang = $data_rincian_detail->getIsMusrenbang();
                $is_urgent = $data_rincian_detail->getPrioritasWali();
                if($is_output) {
                    $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen output, tidak bisa diedit.');
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }
                // if($is_musrenbang) {
                //     $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen musrenbang, tidak bisa diedit.');
                //     return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                // }
                if($is_urgent) {
                    $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen prioritas, tidak bisa diedit.');
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }
            }

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $data_rincian_detail->getKomponenId());
            if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
                $est_fisik = $rs_est_fisik->getIsEstFisik();
            else $est_fisik = false;

            $tipe2 = $data_rincian_detail->getTipe2();

//            ambil untuk mbalikin ke WaitingList
            $data_lama_volume = $data_rincian_detail->getVolume();
            $data_lama_komponen_harga_awal = $data_rincian_detail->getKomponenHargaAwal();
            $data_lama_pajak = $data_rincian_detail->getPajak();
            $data_lama_keterangan_koefisien = $data_rincian_detail->getKeteranganKoefisien();
            $data_lama_satuan = $data_rincian_detail->getSatuan();
            $data_lama_komponen_id = $data_rincian_detail->getKomponenId();
            $data_lama_komponen_name = $data_rincian_detail->getKomponenName();
            $data_lama_rekening = $data_rincian_detail->getRekeningCode();
            $data_lama_nilai_anggaran = $data_rincian_detail->getNilaiAnggaran();
           

//            ambil untuk mbalikin ke WaitingList

            $lokasi_baru = '';
            $lokasi_array = array();

            $blud = 'false';
            if ($unit_id == '0300' || $unit_id == '1800' || $unit_id == '9999') {
                if (is_null($this->getRequestParameter('blud'))) {
                    $blud = 'false';
                } else if ($this->getRequestParameter('blud') == 1) {
                    $blud = 'true';
                }
            }

            $kapitasi = 'false';
            if ($unit_id == '0300' || $unit_id == '1800' || $unit_id == '9999') {
                if (is_null($this->getRequestParameter('kapitasi'))) {
                    $kapitasi = 'false';
                } else if ($this->getRequestParameter('kapitasi') == 1) {
                    $kapitasi = 'true';
                }
            }

            $bos = 'false';
            if ($unit_id == '2000' || $unit_id == '9999') {
                if (is_null($this->getRequestParameter('bos'))) {
                    $bos = 'false';
                } else if ($this->getRequestParameter('bos') == 1) {
                    $bos = 'true';
                }
            }

            $bopda = 'false';
            if ($unit_id == '2000' || $unit_id == '9999') {
                if (is_null($this->getRequestParameter('bopda'))) {
                    $bopda = 'false';
                } else if ($this->getRequestParameter('bopda') == 1) {
                    $bopda = 'true';
                }
            }

            $musrenbang = 'false';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $musrenbang = 'false';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $musrenbang = 'true';
            }

            // $isLelang = 0;
            // if ($this->getRequestParameter('lelang') == 1) {
            //     $isLelang = 1;
            // }            

            $hibah = 'false';
            if (is_null($this->getRequestParameter('hibah'))) {
                $hibah = 'false';
            } else if ($this->getRequestParameter('hibah') == 1) {
                $hibah = 'true';
            }

            //untuk komponen yang sudah ada realisasi, tidak harus mengisi lokasi
            $rd = new DinasRincianDetail();
            if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                //$totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $data_lama_nilai_anggaran);
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                        $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                        //$totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                        $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                    }
                }
            }
            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
            } else if (!in_array($kegiatan_code, $array_buka_validasi) && ($lelang > 0)) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
            }

            //cek perubahan subtitle dan keterangan - 28 Sept 2016
            $kode_subtitle = $this->getRequestParameter('subtitle');
            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);

            // lepas cek delivery utk kegiatan tertentu (temporary)
            if(!$status_bypass_validasi){
                if ($rs_subtitle ) {
                    $subtitle = $rs_subtitle->getSubtitle();
                    if (($totNilaiKontrak > 0 || $totNilaiSwakelola > 0 || $lelang > 0) && ($subtitle != $data_rincian_detail->getSubtitle())) {
                        $this->setFlash('gagal', 'Terdapat nilai kontrak/swakelola/lelang, tidak dapat merubah subtitle');
                        return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                    }
                }

                //buka validasi keterangan untuk tagging covid dian 090821              
                $arr_buka_keterangan = array();
                $querys =
                "SELECT detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_tagging";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($querys);
                $rs1 = $stmt->executeQuery();
                while ($rs1->next()) {
                    array_push($arr_buka_keterangan, $rs1->getString('detail_kegiatan'));
                }

                $is_covid_rka=false;
                $detail_name_ = "select detail_name,is_covid from " . sfConfig::get('app_default_schema') . ".rincian_detail                                     
                        WHERE status_hapus=false and detail_kegiatan='$detail'";
                        $stmt4 = $con->prepareStatement($detail_name_);
                        $rs_cek4 = $stmt4->executeQuery();
                        if ($rs_cek4->next()) {
                         $detail_names = $rs_cek4->getString('detail_name');
                         $is_covid_rka = $rs_cek4->getString('is_covid');
                        }

                $detail_name_covid= $detail_names.' ';
                $noteskpd = $this->getRequestParameter('catatan');

                if ($this->getRequestParameter('keterangan')) {
                    if (($totNilaiKontrak > 0 || $totNilaiSwakelola > 0 || $lelang > 0) ) {
                        if ($this->getRequestParameter('keterangan') != $detail_name_covid && (strpos($noteskpd, 'Covid') !== FALSE ))
                        {
                            $this->setFlash('gagal', 'Merubah note skpd covid,keterangan harus di tambah dengan satu spasi dibelakang sendiri (di akhir keterangan)');
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));

                        }                        
                        else if ($this->getRequestParameter('keterangan') !=  $data_rincian_detail->getDetailName() && (strpos($noteskpd, 'Covid') === FALSE ) )
                        {

                            $this->setFlash('gagal', 'Terdapat nilai kontrak/swakelola/lelang, tidak dapat merubah keterangan');
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));

                        }
                    }
                }
               
                //cek perubahan subtitle dan keterangan - 28 Sept 2016

                $detail_kegiatan_kec_kosong_arr = array('2700.1211.125');
                if (($totNilaiSwakelola == 0 && $totNilaiKontrak == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) && ($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) && (!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan') )) {
                    if(!in_array($data_rincian_detail->getDetailKegiatan(), $detail_kegiatan_kec_kosong_arr)){
                        $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                        return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                    }
                }
                if (($totNilaiSwakelola == 0 && $totNilaiKontrak == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) && ($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) && !$this->getRequestParameter('lokasi_jalan') ) {
                    $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }
            }
            // lepas cek delivery untuk kegiatan tertentu (temporary)

            /// simpan catatan koefisien
            $notekoefisien = '';
            $notekoefisien = $this->getRequestParameter('catatan_koefisien');
            $status_lelang = $data_rincian_detail->getStatusLelang();
            if($status_lelang == 'lock' && $notekoefisien == ''){
                $this->setFlash('gagal', 'Mohon maaf, Harap mengisi Catatan Koefisien minimal 15 karakter');
                return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
            }

//irul -> UNTUK REVISI
//irul 8mei2014 - simpan catatan skpd   
            // if($data_rincian_detail->getRekeningCode()=='5.1.02.02.01.080' or $data_rincian_detail->getRekeningCode()=='5.2.1.02.02'){
            $koRek = substr($data_rincian_detail->getKomponenId() , 0, 18);
            if(($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($data_rincian_detail->getSatuan() == 'Orang Bulan'))
            {   

                $volume_orang = $this->getRequestParameter('volume_orang');
            }           
            else
            {
               $volume_orang = 0;
           }

           $noteskpd = '';
           $noteskpd = $this->getRequestParameter('catatan');
           $apakah_murni = 0;
           if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'tutup') {
            $apakah_murni = 1;
        } else {
            $apakah_murni = 0;
        }

        $arr_buka_volume_orang = array('2.1.1.01.01.01.006.017','2.1.1.01.01.01.006.015');

        $vol1 = $this->getRequestParameter('vol1');
        $sat1 = $this->getRequestParameter('volume1');
        if (strlen(str_replace(' ', '', $noteskpd)) < 15 && $apakah_murni == 0) {
            $this->setFlash('gagal', 'Mohon maaf, Inputan Catatan Pergeseran Anggaran minimal 15 karakter');
            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
        } elseif (strlen(str_replace(' ', '', $noteskpd)) < 15 && $apakah_murni == 0) {
            $this->setFlash('gagal', 'Mohon maaf, Inputan catatan usulan Anggaran minimal 15 karakter');
            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
        } elseif (strpos($this->getRequestParameter('vol1'), ',') || strpos($this->getRequestParameter('vol2'), ',') || strpos($this->getRequestParameter('vol3'), ',') || strpos($this->getRequestParameter('vol4'), ',')) {
            $this->setFlash('gagal', 'Mohon maaf, Pengisian Pecahan pada Volume Menggunakan Titik.');
            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
        } elseif (!(empty($vol1) || $vol1 == 0) && empty($sat1)) {
            $this->setFlash('gagal', 'Mohon maaf, Satuan Koefisien Tidak Boleh Kosong.');
            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
        } elseif (($koRek == '2.1.1.01.01.01.008' or $koRek == '2.1.1.03.05.01.001'  or $koRek == '2.1.1.01.01.01.004' or $koRek == '2.1.1.01.01.02.099' or $koRek == '2.1.1.01.01.01.005' or $koRek == '2.1.1.01.01.01.006') and ($data_rincian_detail->getSatuan() == 'Orang Bulan' or $data_rincian_detail->getSatuan() == 'OrangBulan') and !in_array($data_rincian_detail->getKomponenId(),  $arr_buka_volume_orang) and $vol1>0 and !$this->getRequestParameter('volume_orang') ) {
            $this->setFlash('gagal', 'Mohon maaf, Volume Orang untuk Tenaga Kontrak Tidak Boleh Kosong.');
            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
        } 
        else {
                // berikan alert jika komponen PSP belum dimanfaatkan, sesuai dengan permintaan
            if(!$sisaLelang || !isset($sisaLelang) || empty($sisaLelang)) {
                $query =
                "SELECT
                drd.kegiatan_code,
                drd.unit_id,
                drd.detail_no,
                drd.komponen_id,
                drd.komponen_name,
                drd.detail_name,
                rka.nilai_anggaran - drd.nilai_anggaran AS sisa,
                drd.detail_kegiatan
                FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail drd
                INNER JOIN " . sfConfig::get('app_default_schema') . ".rincian_detail rka ON drd.unit_id = rka.unit_id AND drd.kegiatan_code = rka.kegiatan_code AND drd.detail_no = rka.detail_no
                WHERE drd.unit_id = '$unit_id'                
                AND drd.satuan = 'Paket'
                AND drd.volume = 1
                AND drd.status_lelang = 'lock'
                AND drd.status_hapus = FALSE
                AND rka.nilai_anggaran > drd.nilai_anggaran";
                //AND drd.kegiatan_code = '$kegiatan_code' tutup per sub jadi per PD cek validasi psp

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_sisapengadaan = $stmt->executeQuery();

                $sisa_pengadaan = 0;
                while ($rs_sisapengadaan->next()) {
                    $sisa_pengadaan += $rs_sisapengadaan->getFloat('sisa');
                }

                $query =
                "SELECT SUM(sisa_anggaran) AS nilai_hpsp
                FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
                WHERE unit_id = '".$unit_id."'";
                //AND kegiatan_code = '".$kegiatan_code."'"; tutup per sub jadi per PD

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_total_hpsp = $stmt->executeQuery();

                $sub_lepas_psp = array('');

                $nilai_hpsp = 0;
                if($rs_total_hpsp->next())
                    $nilai_hpsp += $rs_total_hpsp->getFloat('nilai_hpsp');

                if($sisa_pengadaan - $nilai_hpsp > sfConfig::get('app_default_bataspsp') && !in_array($kegiatan_code, $sub_lepas_psp)) {
                    $hasil_pengurangan_sisapengadaan = intval($sisa_pengadaan) - intval($nilai_hpsp);
                    $this->setFlash('gagal', 'Mohon maaf, masih ada komponen PSP yang belum ada pemanfaatannya Sebesar Rp ' . number_format($hasil_pengurangan_sisapengadaan, 0, ',', '.') );
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }
            }

                // cek error total sisa lelang harus sama dengan total anggaran komponen
            if($sisaLelang) {
                $total = $this->getRequestParameter('total');
                $totalLelang = 0;

                $isi_chx = $this->getRequestParameter('chkPSP');
                foreach ($isi_chx as $value) {
                    $sisa_anggaran = $this->getRequestParameter($value);
                    $totalLelang += $sisa_anggaran;
                }

                    // if($totalLelang != round($total) && $totalLelang != $total) {
                    //     $this->setFlash('gagal', 'Mohon maaf, Pengisian Anggaran Sisa Pengadaan Harus Sama Nominalnya dengan Total Anggaran Komponen.');
                    //     return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                    // }

                if(!isset($totalLelang) || empty($totalLelang) || $totalLelang < 0) {
                    $this->setFlash('gagal', 'Mohon maaf, pengisian anggaran sisa pengadaan tidak boleh kurang dari 0.');
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }

                $lebihDariLelang = $this->getRequestParameter('lebihDariLelang');
                if($lebihDariLelang) {
                    $this->setFlash('gagal', 'Mohon maaf, Pengisian Anggaran Sisa Pengadaan Tidak Boleh Lebih Dari Nominal Sisa Pengadaan Sebenarnya.');
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }
            }

//irul 8mei2014 - simpan catatan skpd
//irul -> UNTUK REVISI
//untuk cek rekening
            $komponen_id = '';
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_rdRekening = DinasRincianDetailPeer::doSelectOne($c);
            if ($rs_rdRekening) {
                $rekening_lama = $rs_rdRekening->getRekeningCode();
                $vol_Lama = $rs_rdRekening->getVolume();
                $satuan = $rs_rdRekening->getSatuan();
                $detail_kegiatans = $rs_rdRekening ->getDetailKegiatan();

                $arr_kegiatan_koma = array('');
                //(!in_array($kegiatan_code, $arr_kegiatan_koma) && ($satuan == 'Paket')) ||

                $arr_detail_kegiatan_koma = array('');
                    // validasi koma untuk satuan paket
                if (strpos($this->getRequestParameter('vol1'), '.') || strpos($this->getRequestParameter('vol2'), '.') || strpos($this->getRequestParameter('vol3'), '.') || strpos($this->getRequestParameter('vol4'), '.')) {
                    if( (!in_array($detail_kegiatans, $arr_detail_kegiatan_koma) && ($satuan == 'Paket'))) {
                        $this->setFlash('gagal', 'Mohon maaf, Pengisian Volume Komponen Dengan Satuan Paket Harus Bulat.');
                        return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                    }
                    
                }
            }

            $volume = 0;
            $keterangan_koefisien = '';

                if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1=$this->getRequestParameter('vol1');
                $vol2=0;
                $vol3=0;
                $vol4=0;
                if ($this->getRequestParameter('vol2') == '') {                   
                    $vol2 = 1;
                    if ($this->getRequestParameter('volume1') == '%' && (strpos($data_rincian_detail->getKomponenName(), 'Iuran JK') === FALSE) && $data_rincian_detail->getSatuan()!= '%')
                    $vol1 = ($this->getRequestParameter('vol1')/100);
                    else
                    $vol1 = $this->getRequestParameter('vol1');
                    $volume = $vol1*$vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if(!$this->getRequestParameter('vol2') == '') {
                    if ($this->getRequestParameter('volume2') == '%' && (strpos($data_rincian_detail->getKomponenName(), 'Iuran JK') === FALSE) && $data_rincian_detail->getSatuan()!= '%' )
                    $vol2 = $this->getRequestParameter('vol2')/100;
                    else
                    $vol2 =  $this->getRequestParameter('vol2');
                    
                   // $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $volume = $vol1*$vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;                   
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    if ($this->getRequestParameter('volume3') == '%' && (strpos($data_rincian_detail->getKomponenName(), 'Iuran JK') === FALSE) && $data_rincian_detail->getSatuan()!= '%')
                    $vol3 = $this->getRequestParameter('vol3')/100;
                    else
                    $vol3 =  $this->getRequestParameter('vol3');

                    // $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $volume = $vol1*$vol2*$vol3;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    if ($this->getRequestParameter('volume4') == '%' && (strpos($data_rincian_detail->getKomponenName(), 'Iuran JK') === FALSE) && $data_rincian_detail->getSatuan()!= '%')
                    $vol4 = $this->getRequestParameter('vol3')/100;
                    else
                    $vol4 =  $this->getRequestParameter('vol4');

                    $volume = $vol1*$vol2*$vol3*$vol4;
                    #$volume = $volume*$vol4;
                    // $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            //die($volume.'-'.$keterangan_koefisien);

                // validasi mengnolkan komponen output, musrenbang, prioritas
                // $arr_buka_nol_output = array();
                // $query =
                // "SELECT detail_kegiatan
                // FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni_nol";
                // $con = Propel::getConnection();
                // $stmt = $con->prepareStatement($query);
                // $rs = $stmt->executeQuery();
                // while ($rs->next()) {
                //     array_push($arr_buka_nol_output, $rs->getString('detail_kegiatan'));
                // }
                // if(!in_array($data_rincian_detail->getDetailKegiatan(), $arr_buka_nol_output)) {
                //     $is_output = $data_rincian_detail->getIsOutput();
                //     $is_musrenbang = $data_rincian_detail->getIsMusrenbang();
                //     $is_urgent = $data_rincian_detail->getPrioritasWali();
                //     if($is_output && $volume <= 0) {
                //         $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen output, tidak bisa dinolkan.');
                //         return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                //     }
                //     if($is_musrenbang && $volume <= 0) {
                //         $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen musrenbang, tidak bisa dinolkan.');
                //         return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                //     }
                //     if($is_urgent && $volume <= 0) {
                //         $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen prioritas, tidak bisa dinolkan.');
                //         return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                //     }
                // }

            $con = Propel::getConnection();
//                $temp_sub = $this->getRequestParameter('subtitle');
//                $query = "select is_perangkaan from ebudget.dinas_subtitle_indikator "
//                        . " where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$temp_sub'";
//                $stmt = $con->prepareStatement($query);
//                $rs = $stmt->executeQuery();
//                if ($rs->next()) {
//                    $cek_subtitle = $rs->getBoolean('is_perangkaan');
//                }
//                if ($volume < $vol_Lama && $cek_subtitle) {
//                    $this->setFlash('gagal', 'Mohon maaf, komponen perangkaan tidak boleh mengurangi volume');
//                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
//                }
//menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.

            $rd_function = new DinasRincianDetail();

            $status_pagu_uk = 0;
                // $array_buka_pagu_uk_khusus = array();
                // if (!in_array($data_rincian_detail->getUnitId(), $array_buka_pagu_uk_khusus)) {
                //     if ($data_rincian_detail->getRekeningCode() == '5.1.02.02.01.080') {
                //         $status_pagu_uk = $rd_function->getBatasPaguPerDinasforEditUK($unit_id, $kegiatan_code, $detail_no, $volume);
                //         $nilai_maks_uk = $rd_function->getNilaiPaguUKMaks($unit_id);
                //         if ($status_pagu_uk == 1) {
                //             $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks UK untuk SKPD Anda sebesar ' . number_format($nilai_maks_uk));
                //             return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                //         }
                //     }
                // }

            $c_master_kegiatan = new Criteria();
            $c_master_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_master_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_master_kegiatan);
            $tahap = $rs_master_kegiatan->getTahap();
                //validasi devplan
                // if($tahap = "pak"){

                    // $kode_belanja = substr($data_rincian_detail->getRekeningCode(), 0, 5);
                    // $c = new Criteria();
                    // $c->add(KuaKegiatanPeer::UNIT_ID, $unit_id);
                    // $c->add(KuaKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                    // $rs_belanja_devplan = KuaKegiatanPeer::doSelectOne($c);
                    // if ($kode_belanja == '5.2.1') {
                    //     $nama_belanja = 'Pegawai';
                    //     $nilai_maks_belanja = $rs_belanja_devplan->getPaguPegawai();
                    // } elseif ($kode_belanja == '5.2.2') {
                    //     $nama_belanja = 'Barang dan Jasa';
                    //     $nilai_maks_belanja = $rs_belanja_devplan->getPaguBarjas();
                    // } elseif ($kode_belanja == '5.2.3') {
                    //     $nama_belanja = 'Modal';
                    //     $nilai_maks_belanja = $rs_belanja_devplan->getPaguModal();
                    // }
                    // $status_pagu_belanja_devplan = $rd_function->getBatasPaguBelanjaDevplanForEdit($unit_id, $kegiatan_code, $detail_no, $volume, $kode_belanja);
                    // if ($status_pagu_belanja_devplan == 1) {
                    //     $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Belanja ' . $nama_belanja . ' untuk SKPD Anda sebesar ' . number_format($nilai_maks_belanja));
                    //     return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    // }
                    // validasi devplan
                // }

//                $status_pagu_mamin = 0;
//                $array_buka_pagu_mamin_khusus = array();
//                if (!in_array($data_rincian_detail->getUnitId(), $array_buka_pagu_mamin_khusus)) {
//                    if (substr($data_rincian_detail->getRekeningCode(), 0, 8) == '5.2.2.12') {
//                        $status_pagu_mamin = $rd_function->getBatasPaguPerDinasforEditMamin($unit_id, $kegiatan_code, $detail_no, $volume);
//                        $nilai_maks_mamin = $rd_function->getNilaiPaguMaminMaks($unit_id);
//                        if ($status_pagu_mamin == 1) {
//                            $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Makan dan Minum untuk SKPD Anda sebesar ' . number_format($nilai_maks_mamin));
//                            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
//                        }
//                    }
//                }
//                $status_pagu_atk = 0;
//                $array_buka_pagu_atk_khusus = array();
//                if (!in_array($data_rincian_detail->getUnitId(), $array_buka_pagu_atk_khusus)) {
//                    if ($data_rincian_detail->getRekeningCode() == '5.2.2.01.01') {
                       // $status_pagu_atk = $rd_function->getBatasPaguPerDinasforEditAtk($unit_id, $kegiatan_code, $detail_no, $volume);
//                        $nilai_maks_atk = $rd_function->getNilaiPaguAtkMaks($unit_id);
//                        if ($status_pagu_atk == 1) {
//                            $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Alat Tulis Kantor untuk SKPD Anda sebesar ' . number_format($nilai_maks_atk));
//                            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
//                        }
//                    }
//                }
//irul 19juli2014 -ENTRI BUDGET 2015
                // kunci pagu atk per kegiatan
                // $array_buka_pagu_atk_kegiatan_khusus = array();
                // if (!in_array($unit_id, $array_buka_pagu_atk_kegiatan_khusus)) {
                //    if ($rekening == '5.2.2.01.01') {
                //        $status_pagu_atk = $rd_function->getBatasPaguPerKegiatanAtk($unit_id, $kegiatan_code ,$komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                //        $nilai_maks_atk = $rd_function->getNilaiPaguAtkMaksKegiatan($unit_id, $kegiatan_code);
                //        if ($status_pagu_atk == 1) {
                //            $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Alat Tulis Kantor untuk SKPD Anda pada kegiatan ini sebesar ' . number_format($nilai_maks_atk));
                //            return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                //         }
                //     }
                // }

                // kunci pagu uk per kegiatan
            $array_buka_pagu_uk_kegiatan_khusus = array('');
            if (!in_array($kegiatan_code, $array_buka_pagu_uk_kegiatan_khusus)) {
             if ( $rs_rdRekening->getRekeningCode() == '5.1.02.02.01.080') {
                 $status_pagu_uk = $rd_function->getBatasPaguPerKegiatanUk($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                 $nilai_maks_uk = $rd_function->getNilaiPaguUkMaksKegiatan($unit_id, $kegiatan_code);
                 if ($status_pagu_uk == 1) {
                     $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks Rekening Uang Kinerja Kegiatan untuk SKPD Anda pada kegiatan ini sebesar ' . number_format($nilai_maks_uk));
                     return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                 }
             }
         }                

         $array_buka_pagu_dinas_khusus = array('');
         $array_buka_pagu_kegiatan_khusus = array('');
         $status_pagu_rincian = 0;

         if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
            $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
        } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
            $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforEdit($unit_id, $kegiatan_code, $detail_no, $volume, $harga);
        } else {
            if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                            // var_dump("expression");die();
                            //batas pagu per dinas
                    $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
                } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                            //batas pagu per kegiatan 
                    $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
                }
            } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                $status_pagu_rincian = 0;
            }
        }

        if ($status_pagu_rincian == '1') {
            $this->setFlash('gagal', 'Komponen tidak berhasil diubah karena. nilai total RKA Melebihi total Pagu kegiatan pada SKPD.');
            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
        } else if ($status_pagu_rincian == '0') {
//end of fasilitas
            if (($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik)) {

                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama, Criteria::ILIKE);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            if ($keisi == 0) {
                                $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                $keisi++;
                            } else {
                                $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                $keisi++;
                            }
                        }
                    }
                }

//                    buat baru

                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
                if (($totNilaiSwakelola == 0 && $totNilaiKontrak == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) && $keisi == 0 && $volume==0) {
                    $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                    return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                }
            }

            $detail_name = '';
            $kode_sub = '';
            $sub = '';
//$kode_jasmas='';
            $kode_jasmas = $this->getRequestParameter('jasmas');

            if (($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik)) {
                $kode_lokasi = '';
                if ($lokasi_baru == '') {
                    $detail_name = '';
                } else {
                    $detail_name = '(' . $lokasi_baru . ')';
                }
                $kode_jasmas = $this->getRequestParameter('jasmas');
                $c = new Criteria();
                $c->add(VLokasiPeer::NAMA, $detail_name);
                $rs_lokasi = VLokasiPeer::doSelectOne($c);
                if ($rs_lokasi) {
                    $kode_lokasi = $rs_lokasi->getKode();
                }
                        /*
                          if($kode_lokasi=='')
                          {
                          $this->setFlash('lokasitidakada', 'Lokasi Tidak Ada atau Tidak Valid dengan Data G.I.S');
                          return $this->redirect("peneliti/editKegiatan?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=".md5('ubah'));
                          }
                         */
                      } else {
                        $detail_name = $this->getRequestParameter('keterangan');
                        $kode_jasmas = '';
                    }
                    if ($this->getRequestParameter('sub')) {
                        $kode_sub = $this->getRequestParameter('sub');
//print_r($kode_sub);exit;
                        if ($kode_sub == 'nama') {
                            $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                            . "where kode_sub ilike 'RKAM%'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($sql);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $kodesub = $rs->getString('kode_sub');
                            }
                            $kode = substr($kodesub, 4, 5);
                            $kode+=1;
                            if ($kode < 10) {
                                $kodesub = 'RKAM0000' . $kode;
                            } elseif ($kode < 100) {
                                $kodesub = 'RKAM000' . $kode;
                            } elseif ($kode < 1000) {
                                $kodesub = 'RKAM00' . $kode;
                            } elseif ($kode < 10000) {
                                $kodesub = 'RKAM0' . $kode;
                            } elseif ($kode < 100000) {
                                $kodesub = 'RKAM' . $kode;
                            }

                            $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".dinas_rka_member (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun )
                            values ('$kodesub','$unit_id','$kegiatan_code',$detail_no,'$data_lama_komponen_id','$data_lama_komponen_name','$detail_name','$data_lama_rekening','" . sfConfig::get('app_tahun_default') . "')";
                            $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                            $stmt2->executeQuery();
                            $kode_sub = $kodesub;
                            $sub = trim($data_lama_komponen_name);
                        } else {
                            $cekKodeSub = substr($kode_sub, 0, 4);
                            if ($cekKodeSub == 'RKAM') {//RKA Member
                                $C_RKA = new Criteria();
                                $C_RKA->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
                                $rs_rkam = DinasRkaMemberPeer::doSelectOne($C_RKA);
                                if ($rs_rkam) {
                                    $sub = $rs_rkam->getKomponenName();
                                    $sub = trim($sub);
                                }
                            } else {
                                $d = new Criteria();
                                $d->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
                                $rs_rinciansubparameter = DinasRincianSubParameterPeer::doSelectOne($d);
                                if ($rs_rinciansubparameter) {
                                    $sub = $rs_rinciansubparameter->getNewSubtitle();
                                    $sub = trim($sub);
                                }
                            }
                        }
                    }
                    $kode_subtitle = $this->getRequestParameter('subtitle');

                    $c = new Criteria();
                    $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
                    $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }

                    $sekarang = date('Y-m-d H:i:s');

//irul - ambil tahap tabel master_kegiatan
                    $c_master_kegiatan = new Criteria();
                    $c_master_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_master_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                    $rs_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_master_kegiatan);
                    $tahap = $rs_master_kegiatan->getTahap();
//irul - ambil tahap tabel master_kegiatan

                    $c = new Criteria();
                    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);
                    if ($rincian_detail) {
                        $volumeDiBudgeting = $rincian_detail->getVolume();
                        $pajak = $rincian_detail->getPajak();
                        //$harga = $rincian_detail->getKomponenHargaAwal();
                        $harga = $harga;
                        $rekening = $rincian_detail->getRekeningCode();

                        //update tunjangan jika komponen gaji pokok diedit
                        if(strpos($rincian_detail->getKomponenName(),'Gaji Pokok') !== false)
                        {

                            //cari volume tunjangan suami/istri dan anak
                            $volume_suami=0;
                            $volume_anak=0;
                             $detail_gaji=$rincian_detail->getDetailKegiatan();
                             $query = " select  volume_orang,detail_gaji,komponen_name,detail_kegiatan from ebudget.dinas_rincian_detail "
                            . " where status_hapus=false and detail_gaji='$detail_gaji' and komponen_name ilike '%Tunjangan Suami%'";
                             $stmt = $con->prepareStatement($query);
                             $rs = $stmt->executeQuery();
                             if ($rs->next()) {                               
                                $volume_suami = $rs->getInt('volume_orang');
                                $detail_suami = $rs->getString('detail_kegiatan');
                               
                             }
                            $query1 = " select  volume_orang,detail_gaji,komponen_name,detail_kegiatan from ebudget.dinas_rincian_detail "
                            . " where status_hapus=false and detail_gaji='$detail_gaji' and komponen_name ilike '%Tunjangan Anak%'";
                             $stmt1 = $con->prepareStatement($query1);
                             $rs1 = $stmt1->executeQuery();
                             if ($rs1->next()) {                               
                                $volume_anak = $rs1->getInt('volume_orang');
                                $detail_anak = $rs1->getString('detail_kegiatan');
                               
                             }
                             //update tunjangan suami
                            if ($jumlah_suami_istri <> $volume_suami)
                            {
                                $detail_gaji=$rincian_detail->getDetailKegiatan();
                                $volume_suamih = $jumlah_suami_istri*$this->getRequestParameter('vol2')*0.1;
                                $keterangan_koefisien_suami= '10 % X '.$jumlah_suami_istri.' Orang X '.$this->getRequestParameter('vol2').' Bulan';
                                $volume_orang_suami = $jumlah_suami_istri;
                                $c1 = new Criteria();
                                $c1->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                $c1->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                                $c1->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $detail_suami);
                                $rincian_detail1 = DinasRincianDetailPeer::doSelectOne($c1);
                                if($rincian_detail1)
                                {
                                $rincian_detail1->setVolume($volume_suamih);
                                $rincian_detail1->setKeteranganKoefisien($keterangan_koefisien_suami);
                                $rincian_detail1->setVolumeOrang($volume_orang_suami);                                
                                $rincian_detail1->setNoteSkpd($noteskpd);
                                $rincian_detail1->setLastEditTime($sekarang);
                                $rincian_detail1->setTahap($tahap);
                                $rincian_detail1->save();
                                budgetLogger::log('Mengubah komponen (eRevisi) dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $rincian_detail1->getDetailNo() . '; komponen_id:' . $rincian_detail1->getKomponenId() . '; komponen_name:' . $rincian_detail1->getKomponenName());
                                historyUserLog::ubah_komponen_volume_revisi($unit_id, $kegiatan_code,  $rincian_detail1->getDetailNo(),  $rincian_detail1->getVolume());
                                }
                                else //entri kalau belum ada 
                                {

                                $detail_no = 0;
                                $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs_max = $stmt->executeQuery();
                                while ($rs_max->next()) {
                                    $detail_no = $rs_max->getString('nilai');
                                }

                                 $querySisipan = "select max(status_level) as nilai "
                                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                $stmt = $con->prepareStatement($querySisipan);
                                $rs_level = $stmt->executeQuery();
                                while ($rs_level->next()) {
                                    $posisi_terjauh = $rs_level->getInt('nilai');
                                }
                                $sisipan = 'false';
                                if ($posisi_terjauh > 0) {
                                    $sisipan = 'true';
                                }

                                $accres=1;

                                $detail_no+=1;
                                $detail_no_suami = $detail_no;
                               // $detail_gaji= $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
                                $kode_detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no_suami;
                                //die('masuk 5'.$detail_no_suami.'-'.$detail_no);
                                $nama_cari = 'Tunjangan Suami/Istri ('.TRIM($komponen_name).')';
                                $c = new Criteria();
                                $c->add(KomponenPeer::KOMPONEN_NAME, '%'.$nama_cari.'%', Criteria::ILIKE);
                                $rs_komponen = KomponenPeer::doSelectOne($c);
                                if ($rs_komponen) {
                                    $komponen_id_suami = $rs_komponen->getKomponenId();
                                    $komponen_harga_suami = $rs_komponen->getKomponenHarga();                                  
                                    $komponen_name_suami = $rs_komponen->getKomponenName();
                                    $accres = $rs_komponen->getAccres();
                                    $satuan = $rs_komponen->getSatuan();                                    
                                }

                               

                                if ($this->getRequestParameter('volume1') == 'Orang' && $this->getRequestParameter('volume2') == 'Bulan')
                                {
                                    $volume_suami = $jumlah_suami_istri*$this->getRequestParameter('vol2')*0.1;
                                    $keterangan_koefisien_suami= '10 % x '.$jumlah_suami_istri.' Orang X '.$this->getRequestParameter('vol2').' Bulan';
                                    $volume_orang_suami = $jumlah_suami_istri;
                                }
                                //$volume=$volume * 0.1;
                             // die($detail_no_suami.'-'. $nama_cari.'-'.$komponen_id_suami.'-'.$komponen_harga_suami.'-'.$komponen_name_suami.'-'.$accres_komp.'-'.$satuan.'-'.$volume_suami.'-'. $keterangan_koefisien_suami.'-'.$detail_gaji.'.'.$rekening);

                            $status_komponen_baru='TRUE';
                            $is_rab = 'FALSE';
                            $is_persediaan ='FALSE';
                            $is_narsum = 'FALSE';
                            $is_bbm = 'FALSE';
                            $is_pengecualian = 'FALSE';
                            $is_koma = 'FALSE';
                            $sumber_dana_id = 11;
                            $tipe = 'BTL';

                                
                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,note_skpd,tahap,status_sisipan,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien, volume_orang, volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres,detail_gaji)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "', '" . $tipe . "', " . $detail_no_suami . ", '5.1.01.01.02.0001', '" . $komponen_id_suami . "', '" . $detail_name_baru . "', " . $volume_suami . ", '" . $keterangan_koefisien_suami . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga_suami . ", " . $komponen_harga_suami . ",'" . $komponen_name_suami . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $note_skpd . "','" . $tahap . "', $sisipan, '$kode_detail_kegiatan', $status_komponen_baru, 0, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',". $volume_orang_suami.",". $volume_orang_suami.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",2.5,'$detail_gaji')";
                            $stmt = $con->prepareStatement($query);
                            budgetLogger::log('Menambah komponen baru (eRevisi) dengan komponen name ' . $komponen_name_suami . '(' . $komponen_id_suami . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                            $stmt->executeQuery();
                            $con->commit();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no_suami);

                            }
                                

                            }
                            //update tunjangan anak
                            if ($jumlah_anak <> $volume_anak)
                            {
                                $volume_anaks = $jumlah_anak*$this->getRequestParameter('vol2')*0.02;
                                $keterangan_koefisien_anak= '2 % X '.$jumlah_anak.' Orang X '.$this->getRequestParameter('vol2').' Bulan';
                                $volume_orang_anak = $jumlah_anak;
                                $c1 = new Criteria();
                                $c1->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                                $c1->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                                $c1->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $detail_anak);
                                $rincian_detail1 = DinasRincianDetailPeer::doSelectOne($c1);
                                if($rincian_detail1)
                                {
                                $rincian_detail1->setVolume($volume_anaks);
                                $rincian_detail1->setKeteranganKoefisien($keterangan_koefisien_anak);
                                $rincian_detail1->setVolumeOrang($volume_orang_anak);                                
                                $rincian_detail1->setNoteSkpd($noteskpd);
                                $rincian_detail1->setLastEditTime($sekarang);
                                $rincian_detail1->setTahap($tahap);
                                $rincian_detail1->save();
                                budgetLogger::log('Mengubah komponen (eRevisi) dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $rincian_detail1->getDetailNo() . '; komponen_id:' . $rincian_detail1->getKomponenId() . '; komponen_name:' . $rincian_detail1->getKomponenName());
                                historyUserLog::ubah_komponen_volume_revisi($unit_id, $kegiatan_code,  $rincian_detail1->getDetailNo(),  $rincian_detail1->getVolume());
                                }
                                else //tambah baru
                                {

                                $detail_no = 0;
                                $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $rs_max = $stmt->executeQuery();
                                while ($rs_max->next()) {
                                    $detail_no = $rs_max->getString('nilai');
                                }

                                 $querySisipan = "select max(status_level) as nilai "
                                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                $stmt = $con->prepareStatement($querySisipan);
                                $rs_level = $stmt->executeQuery();
                                while ($rs_level->next()) {
                                    $posisi_terjauh = $rs_level->getInt('nilai');
                                }
                                $sisipan = 'false';
                                if ($posisi_terjauh > 0) {
                                    $sisipan = 'true';
                                }

                                $accres=1;

                                $detail_no+=1;
                                $detail_no_anak = $detail_no;
                               // $detail_gaji= $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
                                $kode_detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no_anak;
                                //die('masuk 5'.$detail_no_suami.'-'.$detail_no);
                                $nama_cari = 'Tunjangan Anak ('.TRIM($komponen_name).')';
                                $c = new Criteria();
                                $c->add(KomponenPeer::KOMPONEN_NAME, '%'.$nama_cari.'%', Criteria::ILIKE);
                                $rs_komponen = KomponenPeer::doSelectOne($c);
                                if ($rs_komponen) {
                                    $komponen_id_anak = $rs_komponen->getKomponenId();
                                    $komponen_harga_anak = $rs_komponen->getKomponenHarga();                                  
                                    $komponen_name_anak = $rs_komponen->getKomponenName();
                                    $accres = $rs_komponen->getAccres();
                                    $satuan = $rs_komponen->getSatuan();                                    
                                }

                               

                                if ($this->getRequestParameter('volume1') == 'Orang' && $this->getRequestParameter('volume2') == 'Bulan')
                                {
                                    $volume_anak = $jumlah_anak*$this->getRequestParameter('vol2')*0.02;
                                    $keterangan_koefisien_anak= '2 % X '.$jumlah_anak.' Orang X '.$this->getRequestParameter('vol2').' Bulan';
                                    $volume_orang_anak = $jumlah_anak;
                                }
                                //$volume=$volume * 0.1;
                                // die($detail_no_suami.'-'. $nama_cari.'-'.$komponen_id_suami.'-'.$komponen_harga_suami.'-'.$komponen_name_suami.'-'.$accres_komp.'-'.$satuan.'-'.$volume_suami.'-'. $keterangan_koefisien_suami.'-'.$detail_gaji);
                            $status_komponen_baru='TRUE';
                            $is_rab = 'FALSE';
                            $is_persediaan ='FALSE';
                            $is_narsum = 'FALSE';
                            $is_bbm = 'FALSE';
                            $is_pengecualian = 'FALSE';
                            $is_koma = 'FALSE';
                            $sumber_dana_id = 11;
                            $tipe = 'BTL';

                                
                            $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            (kegiatan_code, kegiatan_id,tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                            komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,note_skpd,tahap,status_sisipan,detail_kegiatan,status_komponen_baru, tipe_lelang, is_persediaan, is_rab, is_narsum, is_bbm, note_koefisien, volume_orang, volume_orang_anggaran,is_koma,is_pengecualian,sumber_dana_id,accres,detail_gaji)
                            values
                            ('" . $kegiatan_code . "','" . $kegiatan_id . "', '" . $tipe . "', " . $detail_no_anak . ", '5.1.01.01.02.0001', '" . $komponen_id_anak . "', '" . $detail_name_baru . "', " . $volume_anak . ", '" . $keterangan_koefisien_anak . "', '" . str_replace("'", "''", $subtitle) . "',
                            " . $komponen_harga_anak . ", " . $komponen_harga_anak . ",'" . $komponen_name_anak . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $sub . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $note_skpd . "','" . $tahap . "', $sisipan, '$kode_detail_kegiatan', $status_komponen_baru, 0, $is_persediaan, $is_rab, $is_narsum, $is_bbm, '".$note_koefisien."',". $volume_orang_anak.",". $volume_orang_anak.",".$is_koma.",'" . $is_pengecualian . "',".$sumber_dana_id.",2.5,'$detail_gaji')";
                            $stmt = $con->prepareStatement($query);
                            budgetLogger::log('Menambah komponen baru (eRevisi) dengan komponen name ' . $komponen_name_anak . '(' . $komponen_id_anak . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                            $stmt->executeQuery();
                            $con->commit();

                            historyUserLog::tambah_komponen_revisi($unit_id, $kegiatan_code, $detail_no_anak);
                                }


                            }

                        }
                        


                        // hapus sub subtitle
                        $detail_name_lama = $rincian_detail->getDetailName();
                        if(empty($detail_name) && !empty($detail_name_lama)) {
                            historyUserLog::hapus_sub_subtitle($unit_id, $kegiatan_code, $rincian_detail->getKomponenName(), $rincian_detail->getDetailName());

                        }

                        $nilaiDiBudgeting = $harga * $volumeDiBudgeting * (100 + $pajak) / 100;
                        $nilaiBaru = floor($harga * $volume * (100 + $pajak) / 100);                        
                        sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                        $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                        $rincian_detail->setDetailName($detail_name);
                        $rincian_detail->setVolume($volume);
                        $rincian_detail->setKomponenHargaAwal($harga);
                        $rincian_detail->setSubtitle($subtitle);
                        // $rincian_detail->setSub($sub);
                        $rincian_detail->setKodeSub($kode_sub);
                        $rincian_detail->setKecamatan($kode_jasmas);
                        $rincian_detail->setNoteKoefisien($notekoefisien);
                        $rincian_detail->setNoteSkpd($noteskpd);
                        $rincian_detail->setLastEditTime($sekarang);
                        $rincian_detail->setTahap($tahap);
                        if($vol1==0)
                            $rincian_detail->setVolumeOrangAnggaran(0);
                        else
                            $rincian_detail->setVolumeOrangAnggaran($volume_orang);

//begin of kecamatan kelurahan
                        $detail_kegiatan_kec_kosong_arr = array('2700.1211.125');
                        if ($this->getRequestParameter('kecamatan')) {
                            $lokasi_kec = $this->getRequestParameter('kecamatan');
                            $lokasi_kel = $this->getRequestParameter('kelurahan');
                            $kec = new Criteria();
                            $kec->add(KecamatanPeer::ID, $lokasi_kec);
                            $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                            $rs_kec = KecamatanPeer::doSelectOne($kec);
                            if ($rs_kec) {
                                $kecamatan = $rs_kec->getNama();
                            }

                            $kel = new Criteria();
                            $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                            $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                            $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                            if ($rs_kel) {
                                $kelurahan = $rs_kel->getNamaKelurahan();
                                
                                $rincian_detail->setLokasiKecamatan($kecamatan);
                                $rincian_detail->setLokasiKelurahan($kelurahan);
                            } else {
                                $kecamatan = '';
                                $kelurahan = '';
                            }
                        }else{
                            if(in_array($rincian_detail->getDetailKegiatan(), $detail_kegiatan_kec_kosong_arr)){
                                $rincian_detail->setLokasiKecamatan(null);
                                $rincian_detail->setLokasiKelurahan(null);
                            }
                        }

                        $totNilaiSwakelola = 0;
                        $totNilaiKontrak = 0;
                        $totNilaiRealisasi = 0;
                        $totVolumeRealisasi = 0;
                        $totNilaiRasionalisasi = 0;
                        $totNilaiAlokasi = 0;
                        $totNilaiHps = 0;
                        $getCekNilaiKontrak=0;
                        $ceklelangselesaitidakaturanpembayaran = 0;
                        $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                        $lelang = 0;

                        $rd = new DinasRincianDetail();
                        $nilai_rasionalisasi=$rincian_detail->getAnggaranRasionalisasi();
                        //detail_kegiatan buka validasi cek eproject dan delivery 030621
                        
                        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                            $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                            if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                 // $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                                $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $nilaiBaru);
                                
                                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                   
                                    $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);                                   

                                    $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                                    $getCekNilaiKontrak = $rd->cekKontrak($unit_id . '.' . $kegiatan_code . '.' . $detail_no);
                                    
                                    //  if (!($rincian_detail->getKomponenId() == '1.1.7.01.07.05.001.012.B' || $rincian_detail->getKomponenId() == '1.1.7.01.07.05.001.012'))
                                    //      $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kegiatan_code, $detail_no);
                                    //  else
                                    //      $totNilaiRealisasi=$nilaiBaru-1;

                                    // if (!($rincian_detail->getKomponenId() == '1.1.7.01.07.05.001.012.B' || $rincian_detail->getKomponenId() == '1.1.7.01.07.05.001.012'))
                                    //      $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kegiatan_code, $detail_no);
                                    //  else
                                    //      $totVolumeRealisasi = $volume-1;     

                                    $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kegiatan_code, $detail_no);
                                    $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kegiatan_code, $detail_no);
                                    

                                    if(is_null($nilai_rasionalisasi))
                                         $totNilaiRasionalisasi= $rincian_detail->getNilaiAnggaran();
                                    else
                                       $totNilaiRasionalisasi = $rincian_detail->getNilaiAnggaran()-$rincian_detail->getAnggaranRasionalisasi();
                                    $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                                    $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                                    $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kegiatan_code, $detail_no);
                                }
                            }
                        }

                        // karena murni jadi validasi delivery di tutup
                        if ((($nilaiBaru < $totNilaiRealisasi) || ($nilaiBaru < $totNilaiSwakelola)) && !$status_bypass_validasi) {
                            if ($totNilaiKontrak == 0) {
                                $this->setFlash('gagal', '1 Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                            } else if ($totNilaiSwakelola == 0) {
                                $this->setFlash('gagal', '2 Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                            } else {
                                $this->setFlash('gagal', '3 Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                            }
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ( ($nilaiBaru < $totNilaiHps)  && !$status_bypass_validasi) {
                            $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ( ($ceklelangselesaitidakaturanpembayaran == 1)  && !$status_bypass_validasi) {
                            $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($lelang > 0  && !$status_bypass_validasi) {
                            $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1  && !$status_bypass_validasi) {
                            $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($nilaiBaru < $totNilaiRealisasi  && !$status_bypass_validasi ) {
                            $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ( ($volume < $totVolumeRealisasi) && ($rincian_detail->getStatusLelang() != 'lock')  && !$status_bypass_validasi && !$status_bypass_validasi_tertentu) {
                            $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini melebihi volume realisasi di edelivery, sejumlah : ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        }
                        // else if ( ($volume > $totVolumeRasionalisasi) && !(is_null($totVolumeRasionalisasi)) && ($rincian_detail->getStatusLelang() != 'lock')  && !$status_bypass_validasi) {
                        //     $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini melebihi volume rasionalisasi yang sudah ditentukan, sejumlah : ' . number_format($totVolumeRasionalisasi, 0, ',', '.'));
                        //     return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        // } 
                        // else if ( ($nilaiBaru - $totNilaiRealisasi < $nilai_rasionalisasi) && !(is_null($nilai_rasionalisasi)) && ($rincian_detail->getStatusLelang() != 'lock')  && !$status_bypass_validasi) {
                        //     $this->setFlash('gagal', 'Mohon maaf , nilai anggaran sisa :'.number_format($nilaiBaru - $totNilaiRealisasi, 0, ',', '.').' untuk komponen ini kurang dari nilai rasionalisasi yang sudah ditentukan, sejumlah : ' . number_format($nilai_rasionalisasi, 0, ',', '.'));
                        //     return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        // }   
                        else if( ($getCekNilaiKontrak) > $nilaiBaru && in_array($rincian_detail->getRekeningCode(), array('5.1.02.02.01.080', '5.2.1.02.02')) ) {
                            $this->setFlash('gagal', 'Mohon maaf, untuk komponen ini sudah ada kontrak senilai ' . number_format($getCekNilaiKontrak, 0, ',', '.') . ', silahkan cek di edelivery');
                            return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else {
                            if ($unit_id == '1800' || $unit_id == '0300' || $unit_id == '9999') {
                                if ($blud == 'false') {
                                    $rincian_detail->setIsBlud(false);
                                    $rincian_detail->setSumberDanaId(11);
                                } else {
                                    $rincian_detail->setIsBlud(true);
                                    $rincian_detail->setSumberDanaId(6);
                                }
                            }
                            
                            if ($unit_id == '1800' || $unit_id == '0300' || $unit_id == '9999') {
                                if ($kapitasi == 'false') {
                                    $rincian_detail->setIsKapitasiBpjs(false);
                                } else {
                                    $rincian_detail->setIsKapitasiBpjs(true);
                                }
                            }
                            
                            if ($unit_id == '2000' || $unit_id == '9999') {
                                if ($bos == 'false') {
                                    $rincian_detail->setIsBos(false);
                                } else {
                                    $rincian_detail->setIsBos(true);
                                }
                            }
                            
                            if ($unit_id == '2000' || $unit_id == '9999') {
                                if ($bopda == 'false') {
                                    $rincian_detail->setIsBobda(false);
                                } else {
                                    $rincian_detail->setIsBobda(true);
                                }
                            }

                            if ($musrenbang == 'false') {
                                $rincian_detail->setIsMusrenbang(false);
                            } else {
                                $rincian_detail->setIsMusrenbang(true);
                            }

                            // if ($isLelang == 1) {
                            //     $rincian_detail->setTipeLelang(intval(1));
                            // } else {
                            //     $rincian_detail->setTipeLelang(intval(0));
                            // }

                            if ($hibah == 'false') {
                                $rincian_detail->setIsHibah(false);
                            } else {
                                $rincian_detail->setIsHibah(true);
                            }

                            if ($this->getRequestParameter('akrual_code')) {
                                $akrual_code = $this->getRequestParameter('akrual_code');
                                $arr_akrual_code = explode('|', $this->getRequestParameter('akrual_code'));
                                $akrual_lama = explode('|', $data_rincian_detail->getAkrualCode());
                                if ($akrual_lama[0] != $arr_akrual_code[0]) {
                                    $akrual_code_baru = $akrual_code . '|01';
                                    $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
                                    $akrual_code_baru = $akrual_code . '|' . $akrual_lama[1] . $no_akrual_code;
                                } else {
                                    $akrual_code_baru = $data_rincian_detail->getAkrualCode();
                                }

                                $rincian_detail->setAkrualCode($akrual_code_baru);
                            }

                            $con = Propel::getConnection();
                            $querySisipan = "select max(status_level) as nilai "
                            . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                            $stmt = $con->prepareStatement($querySisipan);
                            $rs_level = $stmt->executeQuery();
                            while ($rs_level->next()) {
                                $posisi_terjauh = $rs_level->getInt('nilai');
                            }
                            $sisipan = false;
                            if ($posisi_terjauh > 0) {
                                $sisipan = true;
                            }
                            $rincian_detail->setStatusSisipan($sisipan);

                            //cek untuk mengisi status_komponen_baru
                            $status_komponen_berubah = FALSE;
                            $status_komponen_baru = FALSE;
                            $tahap_cek = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kegiatan_code);
                            $c_pembanding_kegiatan = new Criteria();
                            $c_pembanding_kegiatan->add(PembandingKegiatanPeer::UNIT_ID, $unit_id);
                            $c_pembanding_kegiatan->add(PembandingKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                            $c_pembanding_kegiatan->add(PembandingKegiatanPeer::TAHAP, $tahap_cek);
                            $c_pembanding_kegiatan->addDescendingOrderByColumn(PembandingKegiatanPeer::ID);
                            if ($rs_pembanding_kegiatan = PembandingKegiatanPeer::doSelectOne($c_pembanding_kegiatan)) {
                                $id_pembanding = $rs_pembanding_kegiatan->getId();
                                $c_pembanding_komponen = new Criteria();
                                $c_pembanding_komponen->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $id_pembanding);
                                $c_pembanding_komponen->add(PembandingKomponenPeer::DETAIL_NO, $detail_no);
                                if ($rs_pembanding_komponen = PembandingKomponenPeer::doSelectOne($c_pembanding_komponen)) {
                                    //$pembanding_rekening_code = $rs_pembanding_komponen->getRekeningCode();
                                    //$pembanding_komponen_name = $rs_pembanding_komponen->getKomponenName();
                                    //$pembanding_satuan = $rs_pembanding_komponen->getSatuan();
                                    $pembanding_subtitle = $rs_pembanding_komponen->getSubtitle();
                                    $pembanding_detail_name = $rs_pembanding_komponen->getDetailName();
                                    $pembanding_volume = $rs_pembanding_komponen->getVolume();
                                    $pembanding_keterangan_koefisien = $rs_pembanding_komponen->getKeteranganKoefisien();

                                    if ($subtitle <> $pembanding_subtitle || $keterangan_fix <> $pembanding_detail_name ||
                                        $volume <> $pembanding_volume || $keterangan_koefisien <> $pembanding_keterangan_koefisien) {
                                        $level_tolak = $rincian_detail->getStatusLevelTolak();
                                        if ($level_tolak >= 4) {
                                            $status_komponen_berubah = TRUE;
                                        } else {
                                            $status_komponen_baru = TRUE;
                                        }
                                    }
                                } else {
                                    $status_komponen_baru = TRUE;
                                }
                            }
                            $rincian_detail->setStatusKomponenBerubah($status_komponen_berubah);
                            $rincian_detail->setStatusKomponenBaru($status_komponen_baru);
                            //cek untuk mengisi status_komponen_baru

                            sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                            budgetLogger::log('Mengubah komponen (eRevisi) dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                            $rincian_detail->save();

                            if ($rincian_detail->getSatuan()=='Tahun' and $rincian_detail->getVolume()==1 and $rincian_detail->getTipe()=='BTL')
                            {
                                 historyUserLog::ubah_komponen_harga_revisi($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                            }
                            else
                            {
                                historyUserLog::ubah_komponen_volume_revisi($unit_id, $kegiatan_code, $detail_no, $volumeDiBudgeting);
                            }
                            

                            $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan.');

//irul 10 desember 2014 - edit data gmap
                            $sekarang = date('Y-m-d H:i:s');
                            $c2 = new Criteria();
                            $c2->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c2->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                            $c2->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                            $rincian_detail2 = DinasRincianDetailPeer::doSelectOne($c2);
                            if ($rincian_detail2) {
                                $query_cek_gmap = "select count(*) as jumlah "
                                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query_cek_gmap);
                                $rs = $stmt->executeQuery();
                                while ($rs->next()) {
                                    $jumlah = $rs->getString('jumlah');
                                }
                                if ($jumlah > 0) {
                                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                    . "set last_edit_time = '$sekarang', satuan = '" . $rincian_detail2->getSatuan() . "', volume = " . $rincian_detail2->getVolume() . ", nilai_anggaran = " . $rincian_detail2->getNilaiAnggaran() . ", komponen_name = '" . $rincian_detail2->getKomponenName() . "'  "
                                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                    $stmt = $con->prepareStatement($query);
                                    $stmt->executeQuery();
                                }
                            }
//irul 10 desember 2014 - edit data gmap


                            if (($tipe2 == 'KONSTRUKSI' || $tipe2 == 'TANAH' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik)) {
                                $rd_cari = new Criteria();
                                $rd_cari->add(DinasRincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                                $rd_cari->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                                $rd_cari->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no, Criteria::EQUAL);
                                $rd_dapat = DinasRincianDetailPeer::doSelectOne($rd_cari);

                                $kode_rka_fix = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                                $c_cari_history = new Criteria();
                                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                                $dapat_history = HistoryPekerjaanV2Peer::doSelect($c_cari_history);
                                if ($dapat_history) {
                                    foreach ($dapat_history as $value_history) {
                                        $id_history = $value_history->getIdHistory();

                                        $c_cari_hapus_history = new Criteria();
                                        $c_cari_hapus_history->addAnd(HistoryPekerjaanV2Peer::ID_HISTORY, $id_history);
                                        $dapat_history_hapus = HistoryPekerjaanV2Peer::doSelectOne($c_cari_hapus_history);
                                        if ($dapat_history_hapus) {
                                            $dapat_history_hapus->delete();
                                        }
                                    }
                                }

                                $keisi = 0;

//                    ambil lama
                                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                                if (count($lokasi_lama) > 0) {
                                    foreach ($lokasi_lama as $value_lokasi_lama) {
                                        $c_cari_lokasi = new Criteria();
                                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                                        if ($dapat_lokasi_lama) {

                                            $jalan_fix = '';
                                            $gang_fix = '';
                                            $nomor_fix = '';
                                            $rw_fix = '';
                                            $rt_fix = '';
                                            $keterangan_fix = '';
                                            $tempat_fix = '';

                                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                                            $gang_lama = $dapat_lokasi_lama->getGang();
                                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                                            $rw_lama = $dapat_lokasi_lama->getRw();
                                            $rt_lama = $dapat_lokasi_lama->getRt();
                                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                                            if ($jalan_lama <> '') {
                                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                                            }

                                            if ($tempat_lama <> '') {
                                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                                            }

                                            if ($gang_lama <> '') {
                                                $gang_fix = $gang_lama . ' ';
                                            }

                                            if ($nomor_lama <> '') {
                                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                                            }

                                            if ($rw_lama <> '') {
                                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                                            }

                                            if ($rt_lama <> '') {
                                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                                            }

                                            if ($keterangan_lama <> '') {
                                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                                            }

                                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                            $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
                                            $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                            $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                            $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                            $lokasi_per_titik = $lokasi_baru;

                                            $c_insert_gis = new HistoryPekerjaanV2();
                                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                            $c_insert_gis->setKodeRka($rka_lokasi);
                                            $c_insert_gis->setStatusHapus(FALSE);
                                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                                            $c_insert_gis->setGang(strtoupper($gang_lama));
                                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                                            $c_insert_gis->setRw(strtoupper($rw_lama));
                                            $c_insert_gis->setRt(strtoupper($rt_lama));
                                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                                            $c_insert_gis->setKomponen($komponen_lokasi);
                                            $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                            $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                            $c_insert_gis->setLokasi($lokasi_per_titik);
                                            $c_insert_gis->save();
                                        }
                                    }
                                }

//                    buat baru
                                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                                $total_array_lokasi = count($lokasi_jalan);

                                for ($i = 0; $i < $total_array_lokasi; $i++) {
                                    $jalan_fix = '';
                                    $gang_fix = '';
                                    $tipe_gang_fix = '';
                                    $nomor_fix = '';
                                    $rw_fix = '';
                                    $rt_fix = '';
                                    $keterangan_fix = '';
                                    $tempat_fix = '';
                                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                                        if (trim($lokasi_jalan[$i]) <> '') {
                                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                                        }

                                        if (trim($lokasi_tempat[$i]) <> '') {
                                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                                        }

                                        if (trim($tipe_gang[$i]) <> '') {
                                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                                        } else {
                                            $tipe_gang_fix = 'GG. ';
                                        }

                                        if (trim($lokasi_gang[$i]) <> '') {
                                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                                        }

                                        if (trim($lokasi_nomor[$i]) <> '') {
                                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                                        }

                                        if (trim($lokasi_rw[$i]) <> '') {
                                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                                        }

                                        if (trim($lokasi_rt[$i]) <> '') {
                                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                                        }

                                        if (trim($lokasi_keterangan[$i]) <> '') {
                                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                                        }


                                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                        $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
                                        $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                        $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                        $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                        $lokasi_per_titik = $lokasi_baru;

                                        $c_insert_gis = new HistoryPekerjaanV2();
                                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                        $c_insert_gis->setKodeRka($rka_lokasi);
                                        $c_insert_gis->setStatusHapus(FALSE);
                                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                                        $c_insert_gis->setGang(strtoupper($gang_fix));
                                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                                        $c_insert_gis->setKomponen($komponen_lokasi);
                                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                        $c_insert_gis->setLokasi($lokasi_per_titik);
                                        $c_insert_gis->save();
                                    }
                                }

//                                kalo waitinglist dinolin
                                $ada_waitinglist = 0;
                                $rd_cari_waitinglist = new Criteria();
                                $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
                                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
                                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);

                                $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);

                                if ($ada_waitinglist > 0 && ($unit_id == '2600' || $unit_id == '2300') && $rd_dapat->getNilaiAnggaran() == 0) {
                                    $rd_dapat_waitinglist = WaitingListPUPeer::doSelectOne($rd_cari_waitinglist);

                                    $total_aktif = 0;
                                    $query = "select count(*) as total "
                                    . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                                    . "where status_hapus = false and status_waiting = 0 "
                                    . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kegiatan_code . "'";
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    while ($rs->next()) {
                                        $total_aktif = $rs->getString('total');
                                    }
                                    $total_aktif++;

                                    $balik_waitinglist = new WaitingListPU();
                                    $balik_waitinglist->setUnitId('XXX' . $unit_id);
                                    $balik_waitinglist->setKegiatanCode($kegiatan_code);
                                    $balik_waitinglist->setSubtitle($rd_dapat->getSubtitle());
                                    $balik_waitinglist->setKomponenId($rd_dapat->getKomponenId());
                                    $balik_waitinglist->setKomponenName($rd_dapat->getKomponenName());
                                    $balik_waitinglist->setKomponenLokasi($rd_dapat->getDetailName());
                                    $balik_waitinglist->setKomponenHargaAwal($data_lama_komponen_harga_awal);
                                    $balik_waitinglist->setPajak($data_lama_pajak);
                                    $balik_waitinglist->setKomponenSatuan($data_lama_satuan);
                                    $balik_waitinglist->setKomponenRekening($rd_dapat->getRekeningCode());
                                    $balik_waitinglist->setKoefisien($data_lama_keterangan_koefisien);
                                    $balik_waitinglist->setVolume($data_lama_volume);
                                    $balik_waitinglist->setTahunInput($rd_dapat->getTahun());
                                    $balik_waitinglist->setCreatedAt($sekarang);
                                    $balik_waitinglist->setUpdatedAt($sekarang);
                                    $balik_waitinglist->setStatusHapus(FALSE);
                                    $balik_waitinglist->setStatusWaiting(0);
                                    $balik_waitinglist->setKodeJasmas($rd_dapat->getKecamatan());
                                    $balik_waitinglist->setKecamatan($rd_dapat->getLokasiKecamatan());
                                    $balik_waitinglist->setKelurahan($rd_dapat->getLokasiKelurahan());
                                    $balik_waitinglist->setIsMusrenbang($rd_dapat->getIsMusrenbang());
                                    $balik_waitinglist->setPrioritas($total_aktif);
                                    $balik_waitinglist->setNilaiEe($rd_dapat_waitinglist->getNilaiEe());
                                    $balik_waitinglist->setKeterangan($rd_dapat_waitinglist->getKeterangan());
                                    $balik_waitinglist->save();

                                    $id_baru = $balik_waitinglist->getIdWaiting();

                                    $c_cari_history_lama = new Criteria();
                                    $c_cari_history_lama->add(HistoryPekerjaanV2Peer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no);
                                    $c_cari_history_lama->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                                    $dapat_history_lama = HistoryPekerjaanV2Peer::doSelect($c_cari_history_lama);
                                    if ($dapat_history_lama) {
                                        foreach ($dapat_history_lama as $value_history_lama) {
                                            $c_insert_gis = new HistoryPekerjaanV2();
                                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                            $c_insert_gis->setKodeRka('XXX' . $unit_id . '.' . $kegiatan_code . '.' . $id_baru);
                                            $c_insert_gis->setStatusHapus(FALSE);
                                            $c_insert_gis->setJalan($value_history_lama->getJalan());
                                            $c_insert_gis->setGang($value_history_lama->getGang());
                                            $c_insert_gis->setNomor($value_history_lama->getNomor());
                                            $c_insert_gis->setRw($value_history_lama->getRw());
                                            $c_insert_gis->setRt($value_history_lama->getRt());
                                            $c_insert_gis->setKeterangan($value_history_lama->getKeterangan());
                                            $c_insert_gis->setTempat($value_history_lama->getTempat());
                                            $c_insert_gis->setKomponen($value_history_lama->getKomponen());
                                            $c_insert_gis->setKecamatan($value_history_lama->getKecamatan());
                                            $c_insert_gis->setKelurahan($value_history_lama->getKelurahan());
                                            $c_insert_gis->setLokasi($value_history_lama->getLokasi());
                                            $c_insert_gis->save();
                                        }
                                    }
                                }
//                                kalo waitinglist dinolin
                            }
                        } //tutup else sebelum fitur blud
                    // } //tutup if tahap != murni 
                    // lepas validasi edelivery untuk dinas & kegiatan tertentu (temporary)
                    } else {
                        $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                        return $this->redirect('entri/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                    }

                    // delete dinas_rincian_hpsp syek
                    $uid = $this->getRequestParameter('unit');
                    $keg_code = $this->getRequestParameter('kegiatan');

                    $con = Propel::getConnection();
                    $con->begin();
                    try {
                        $id_komponen_lelang = $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
                        $query_delete_psp = 
                        "DELETE FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
                        WHERE id_komponen_lelang = '".$id_komponen_lelang."'";

                        $stmt = $con->prepareStatement($query_delete_psp);
                        $stmt->executeQuery();

                        $con->commit();

                    } catch (Exception $e) {
                        $con->rollback();
                    }

                    //untuk insert ke tabel dinas_rincian_hpsp
                    $count_chx = 0;
                    if ($sisaLelang) {
                        $kirimControlling = array();
                        $kirimControlling['kode_kegiatan'] = $kegiatan_code;
                        $kirimControlling['unit_id'] = $unit_id;
                        $kirimControlling['rekening_code'] = $rekening;
                        $kirimControlling['pajak'] = $pajak;
                        $kirimControlling['volume'] = $volume;
                        $kirimControlling['detail_no'] = $detail_no;
                        $kirimControlling['harga'] = $komponen_harga;
                        $kirimControlling['nama'] = $komponen_name;
                        $kirimControlling['kode_detail_kegiatan_asal'] = '';

                        $isi_chx = $this->getRequestParameter('chkPSP');
                        $count = 0;

                        foreach ($isi_chx as $value) {
                            $count_chx++;
                            $uid = $unit_id;
                            //$keg_code = $kegiatan_code;
                            $keg_code = $this->getRequestParameter("keg_codePSP".$value);
                            $kom_id = $this->getRequestParameter("idPSP".$value);
                            $det_no = $value;
                            $sisa_anggaran = $this->getRequestParameter($value);
                            $catatan = $this->getRequestParameter("catatanPSP".$value);
                            $catatan = $this->getRequestParameter("catatanPSP".$value);
                            $query_ambil_tahap =
                            "SELECT DISTINCT tahap
                            FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan
                            WHERE unit_id = '$uid'
                            AND kode_kegiatan = '$keg_code'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query_ambil_tahap);
                            $rs_ambil_tahap = $stmt->executeQuery();
                            while($rs_ambil_tahap->next()) {
                                $tahap = $rs_ambil_tahap->getString('tahap');
                            }
                            // $tahap = sfConfig::get('app_tahap_edit');
                            $det_no_asal = $detail_no;
                            $det_keg = $this->getRequestParameter('unit').".".$this->getRequestParameter('kegiatan').".".$detail_no;

                            //id max
                            // $maxid = 0;
                            // $quemaxid = "select max(id_komponen_lelang) as maxid "
                            //         . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp";
                            // $con = Propel::getConnection();
                            // $stmt = $con->prepareStatement($quemaxid);
                            // $rs_maxid = $stmt->executeQuery();
                            // while ($rs_maxid->next()) {
                            //     $maxid = $rs_maxid->getString('maxid');
                            // }
                            // $maxid+=1;
                            // $id_kom_lelang = $maxid;
                            // $catatan = "catatan saja";

                            // $j++;

                            //insertnya dah bisa masuk
                            $con = Propel::getConnection();
                            $con->begin();
                            try {
                                $que_ins = "insert into ".sfConfig::get('app_default_schema').".dinas_rincian_hpsp
                                (unit_id, kegiatan_code, komponen_id, detail_no, id_komponen_lelang, sisa_anggaran, catatan, tahap)
                                values
                                ('".$uid."', '".$keg_code."', '".$kom_id."', ".$det_no.", '".$det_keg."', ".$sisa_anggaran.", '".$catatan."', '".$tahap."')";
                                // echo $que_ins;die();
                                $stmt = $con->prepareStatement($que_ins);
                                $stmt->executeQuery();

                                $con->commit();

                                $que_updt =
                                "UPDATE ".sfConfig::get('app_default_schema').".dinas_rincian_detail
                                SET is_hpsp = TRUE
                                WHERE detail_kegiatan = '$det_keg'";
                                // echo $que_ins;die();
                                $stmt = $con->prepareStatement($que_updt);
                                $stmt->executeQuery();

                                $con->commit();

                                if($count > 0)
                                    $kirimControlling['kode_detail_kegiatan_asal'] .= '|';
                                $kirimControlling['kode_detail_kegiatan_asal'] .= $uid.'.'.$keg_code.'.'.$det_no;
                                $count++;
                            } catch (Exception $e) {
                                $con->rollback();
                            }
                        }

                        // update status arahan
                        $arahan = $this->getRequestParameter('arahan');
                        $kirimControlling['id_arahan'] = '';
                        $count = 0;
                        foreach ($arahan as $arahan_id) {
                            $con = Propel::getConnection();
                            $con->begin();
                            try {
                                $que_updt = 
                                "UPDATE ".sfConfig::get('app_default_schema').".arahan_belanja_kegiatan
                                SET status = 1
                                WHERE id = ".$arahan_id;

                                $stmt = $con->prepareStatement($que_updt);
                                $stmt->executeQuery();

                                $con->commit();

                                if($count > 0)
                                    $kirimControlling['id_arahan'] .= '|';
                                $kirimControlling['id_arahan'] .= $arahan_id;
                                $count++;
                            } catch (Exception $e) {
                                $con->rollback();
                            }
                        }
                        if(count($arahan) == 0) {
                            $kirimControlling['id_arahan'] = null;
                        }
                        $this->kirimServiceControlling($kirimControlling);
                    } else if($count_chx == 0) {
                        $con = Propel::getConnection();
                        $con->begin();
                        // jika uncentang psp, di false kan is_hpsp nya
                        $det_keg = $this->getRequestParameter('unit').".".$this->getRequestParameter('kegiatan').".".$detail_no;
                        
                        $que_updt =
                        "UPDATE ".sfConfig::get('app_default_schema').".dinas_rincian_detail
                        SET is_hpsp = FALSE
                        WHERE detail_kegiatan = '$det_keg'";

                        $stmt = $con->prepareStatement($que_updt);
                        $stmt->executeQuery();

                        // hapus semua PSP nya
                        // $query_delete_psp =
                        // "DELETE FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_hpsp
                        // WHERE unit_id = '".$uid."'
                        // AND kegiatan_code = '".$keg_code."'";

                        // $stmt = $con->prepareStatement($query_delete_psp);
                        // $stmt->executeQuery();

                        $con->commit();
                    }
                    return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
//eof fasilitas status_pagu_rincian
//irul -> UNTUK REVISI
//irul 8mei2014 - simpan catatan skpd
            }
//irul 8mei2014 - simpan catatan skpd
//irul -> UNTUK REVISI
//eof if simpan
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');
            if ($tahap == 'pakbp') {
                $c = new Criteria();
                $c->add(PakBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(PakBukuPutihSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = PakBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(PakBukuPutihRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = PakBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'pakbb') {
                $c = new Criteria();
                $c->add(PakBukuBiruSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(PakBukuBiruSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = PakBukuBiruSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(PakBukuBiruRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(PakBukuBiruRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuBiruRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = PakBukuBiruRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'murni') {
                $c = new Criteria();
                $c->add(MurniSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(MurniSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = MurniSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'murnibp') {
                $c = new Criteria();
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(MurniBukuPutihSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniBukuPutihRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'murnibb') {
                $c = new Criteria();
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(MurniBukuBiruSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = MurniBukuBiruSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(MurniBukuBiruRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(MurniBukuBiruRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(MurniBukuBiruRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = MurniBukuBiruRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi1') {
                $c = new Criteria();
                $c->add(Revisi1SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi1SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi1SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi1RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi1RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi1RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi1RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi1_1') {
                $c = new Criteria();
                $c->add(Revisi1bSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi1bSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi1bSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi1bRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi1RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi1bRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi1bRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi1bRincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2') {
                $c = new Criteria();
                $c->add(Revisi2SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi2SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi2SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi2RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi2RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi2RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi2RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi2RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2_1') {
                $c = new Criteria();
                $c->add(Revisi21SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi21SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi21SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi21RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi21RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi21RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi21RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi21RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2_2') {
                $c = new Criteria();
                $c->add(Revisi22SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi22SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi22SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi22RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi22RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi22RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2_2') {
                $c = new Criteria();
                $c->add(Revisi22SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi22SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi22SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi22RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi22RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi22RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi2_2') {
                $c = new Criteria();
                $c->add(Revisi22SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi22SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi22SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi22RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi22RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi22RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi22RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi22RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi3') {
                $c = new Criteria();
                $c->add(Revisi3SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi3SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi3SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi3RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi3RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi3RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi3RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi3_0') {
                $c = new Criteria();
                $c->add(Revisi30SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi30SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi30SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi30RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi30RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi30RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi30RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi30RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi30incianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi30RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi30RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi30RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi30RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi30RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi30RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi3_1') {
                $c = new Criteria();
                $c->add(Revisi31SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi31SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi31SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi31RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi31RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi3RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31incianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi31RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi31RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi31RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi4') {
                $c = new Criteria();
                $c->add(Revisi4SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi4SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi4SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi4RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi4RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi4RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi4RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi4RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi5') {
                $c = new Criteria();
                $c->add(Revisi5SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi5SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi5SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi5RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi5RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi5RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi5RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi5RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi6') {
                $c = new Criteria();
                $c->add(Revisi6SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi6SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi6SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi6RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi6RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi6RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi6RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi7') {
                $c = new Criteria();
                $c->add(Revisi7SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi7SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi7SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi7RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi7RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi7RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi7RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi7RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi8') {
                $c = new Criteria();
                $c->add(Revisi8SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi8SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi8SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi8RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi8RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi8RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi8RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi8RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi9') {
                $c = new Criteria();
                $c->add(Revisi9SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi9SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi9SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi9RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi9RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi9RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi9RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi9RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'revisi10') {
                $c = new Criteria();
                $c->add(Revisi10SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(Revisi10SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = Revisi10SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(Revisi10RincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(Revisi10RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(Revisi6RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(Revisi10RincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(Revisi10RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = Revisi10RincianDetailPeer::doSelect($c_rincianDetail);
            } elseif ($tahap == 'rkua') {
                $c = new Criteria();
                $c->add(RkuaSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(RkuaSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = RkuaSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(RkuaRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(RkuaRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(RkuaRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(RkuaRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(RkuaRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addDescendingOrderByColumn(RkuaRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(RkuaRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = RkuaRincianDetailPeer::doSelect($c_rincianDetail);
            } else {
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $c->add(DinasSubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $this->unit_id = $unit_id = $rs_subtitle->getUnitId();
                    $this->kode_kegiatan = $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(DinasRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(DinasRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(DinasRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::REKENING_CODE);

                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KOMPONEN_NAME);
                $c_rincianDetail->addDescendingOrderByColumn(DinasRincianDetailPeer::NILAI_ANGGARAN);

                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KELURAHAN);
                $rs_rd = DinasRincianDetailPeer::doSelect($c_rincianDetail);
            }
            $query = "select kode_rka from history_pekerjaan_v2
            where (jalan, komponen) in
            (select jalan, komponen from history_pekerjaan_v2
            where jalan<>'' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "' and substring(kode_rka for 3)<>'XXX' and substring(kode_rka for 4)<>'9999'
            group by jalan, komponen
            having count(*)>1) and jalan<>'' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "' and substring(kode_rka for 3)<>'XXX' and substring(kode_rka for 4)<>'9999'
            order by jalan";
            $congis = Propel::getConnection('gis');
            $stmt = $congis->prepareStatement($query);
            $rs = $stmt->executeQuery();
            $arr_kode_rka = array();
            while ($rs->next()) {
                $arr_kode_rka[] = "'" . $rs->getString('kode_rka') . "'";
            }
            if (!$arr_kode_rka) {
                $kode_rka = "''";
            } else {
                $kode_rka = implode(',', $arr_kode_rka);
            }
            $query = "(select rd.detail_no
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            where (rd.unit_id,rd.komponen_id,rd.rekening_code) in
            (select unit_id, komponen_id, rekening_code
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            where unit_id='$unit_id' and status_hapus=false and tipe<>'FISIK' and tipe<>'EST' and 
            (rekening_code like '5.2.2.%' or rekening_code like '5.2.3.%')
            group by unit_id, komponen_id, rekening_code
            having count(*)>1) and 
            status_hapus=false and nilai_anggaran>0 and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'
            )union(
            select rd.detail_no
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
            where (rd.komponen_id) in
            (select komponen_id
            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            where status_hapus=false and nilai_anggaran>0 and unit_id||'.'||kegiatan_code||'.'||detail_no in ($kode_rka)
            group by komponen_id
            having count(*)>1) and rd.nilai_anggaran>0 and rd.status_hapus=false
            and rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no in ($kode_rka) and
            unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'
        )";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $komponen_serupa = array();
        while ($rs->next()) {
            $komponen_serupa[] = $rs->getString('detail_no');
        }

        $this->komponen_serupa = $komponen_serupa;
        $this->rs_rd = $rs_rd;

        $this->id = $sub_id;
        $this->rinciandetail = 'ada';
        $this->setLayout('kosong');
    }
}

public function executeGetPekerjaansMasalah() {
    if ($this->getRequestParameter('id') == '0') {
        $sub_id = $this->getRequestParameter('id');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');

        $query = "select * "
        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah "
        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and tahun='" . sfConfig::get('app_tahun_default') . "' "
        . "order by sub,kode_sub,rekening_code,komponen_name";
//print($query);exit;
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs_rinciandetail = $statement->executeQuery();

        $this->rs_rinciandetail = $rs_rinciandetail;
        $this->id = $sub_id;
        $this->rinciandetail = 'ada';
        $this->setLayout('kosong');
    }
}

public function executeEdit() {
    if ($this->getRequestParameter('unit_id') && $this->getUser()->getUnitId() == $this->getRequestParameter('unit_id')) {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->tahap = $tahap = $this->getRequestParameter('tahap');

        if ($tahap == 'pakbp') {
            $c = new Criteria();
            $c->add(PakBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(PakBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(PakBukuPutihSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(PakBukuPutihSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = PakBukuPutihSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'pak_bukuputih_';
        } elseif ($tahap == 'pakbb') {
            $c = new Criteria();
            $c->add(PakBukuBiruSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(PakBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(PakBukuBiruSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(PakBukuBiruSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = PakBukuBiruSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'pak_bukubiru_';
        } elseif ($tahap == 'murni') {
            $c = new Criteria();
            $c->add(MurniSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(MurniSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(MurniSubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(MurniSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(MurniSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = MurniSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'murni_';
        } elseif ($tahap == 'murnibp') {
            $c = new Criteria();
            $c->add(MurniBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(MurniBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'murni_bukuputih_';
        } elseif ($tahap == 'murnibb') {
            $c = new Criteria();
            $c->add(MurniBukuBiruSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(MurniBukuBiruSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(MurniBukuBiruSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(MurniBukuBiruSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = MurniBukuBiruSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'murni_bukuputih_';
        } elseif ($tahap == 'revisi1') {
            $c = new Criteria();
            $c->add(Revisi1SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi1SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(Revisi1SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(Revisi1SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi1SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi1SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi1_';
        } elseif ($tahap == 'revisi1_1') {
            $c = new Criteria();
            $c->add(Revisi1bSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi1bSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi1bSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi1bSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi1bSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi1_1_';
        } elseif ($tahap == 'revisi2') {
            $c = new Criteria();
            $c->add(Revisi2SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi2SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(Revisi2SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(Revisi2SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi2SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi2SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi2_';
        } elseif ($tahap == 'revisi2_1') {
            $c = new Criteria();
            $c->add(Revisi21SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi21SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi21SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi21SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi21SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi2_1_';
        } elseif ($tahap == 'revisi2_2') {
            $c = new Criteria();
            $c->add(Revisi22SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi22SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi22SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi22SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi22SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi2_2_';
        } elseif ($tahap == 'revisi3') {
            $c = new Criteria();
            $c->add(Revisi3SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi3SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(Revisi3SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(Revisi3SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi3SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi3SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi3_';
        } elseif ($tahap == 'revisi3_0') {
            $c = new Criteria();
            $c->add(Revisi30SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi30SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi30SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi30SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi30SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi3_0_';
        } elseif ($tahap == 'revisi3_1') {
            $c = new Criteria();
            $c->add(Revisi31SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi31SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi31SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi31SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi31SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi3_1_';
        }  elseif ($tahap == 'revisi4') {
            $c = new Criteria();
            $c->add(Revisi4SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi4SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi4SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi4SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi4SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi4_';
        } elseif ($tahap == 'revisi5') {
            $c = new Criteria();
            $c->add(Revisi5SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi5SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi5SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi5SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi5SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi5_';
        } elseif ($tahap == 'revisi6') {
            $c = new Criteria();
            $c->add(Revisi6SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi6SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi6SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi6SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi6SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi6_';
        } elseif ($tahap == 'revisi7') {
            $c = new Criteria();
            $c->add(Revisi7SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi7SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi7SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi7SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi7SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi7_';
        } elseif ($tahap == 'revisi8') {
            $c = new Criteria();
            $c->add(Revisi8SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi8SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi8SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi8SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi8SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi8_';
        } elseif ($tahap == 'revisi9') {
            $c = new Criteria();
            $c->add(Revisi9SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi9SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi9SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi9SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi9SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi9_';
        } elseif ($tahap == 'revisi10') {
            $c = new Criteria();
            $c->add(Revisi10SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(Revisi10SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(Revisi10SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(Revisi10SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = Revisi10SubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'revisi10_';
        } elseif ($tahap == 'rkua') {
            $c = new Criteria();
            $c->add(RkuaSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(RkuaSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(RkuaSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(RkuaSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = RkuaSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'rkua_';
        } else {
                // aminudin 05-07-2017
                // redirect ke form soal
                // cek di tabel rincian_bappeko (unit_id, kode) apakah sudah pernah jawab soal 
                // $c = new Criteria();
                // $c->add(RincianBappekoPeer::UNIT_ID, $unit_id);
                // $c->add(RincianBappekoPeer::KODE_KEGIATAN, $kode_kegiatan);
                // $c->add(RincianBappekoPeer::TAHAP, DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan));

                // $rincian_b = RincianBappekoPeer::doSelectOne($c);
                // $cri = new Criteria();
                // $cri->add(DinasRincianPeer::UNIT_ID, $unit_id);
                // $cri->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
                // $rincian = DinasRincianPeer::doSelectOne($cri);
                // if ($rincian_b == NULL && $rincian->getRincianLevel() == 1) {
                //     $this->redirect("entri/soal?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                //     // cek apakah jawaban sudah ada
                //     // dan state dinas_rincian posisinya bappeko
                // }
                // // aminudin 05-07-2017 (end)

            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = DinasSubtitleIndikatorPeer::doSelect($c);
            $tabel_dpn = 'dinas_';
        }

            //die($tahap.'- tabel depan:'. $tabel_dpn);
        $this->rs_subtitle = $rs_subtitle;
        $this->rinciandetail = '';
        $this->rs_rinciandetail = '';

        if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
//untuk warning
            $query = "select sum(nilai_anggaran) as tot
            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
            where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.1.01%' and status_hapus=false";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_1 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
            where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.1.02%' and status_hapus=false";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_2 = $rs->getString('tot');

            $query = "select sum(nilai_anggaran) as tot
            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
            where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2%' and status_hapus=false";
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs = $statement->executeQuery();
            while ($rs->next())
                $total_menjadi_3 = $rs->getString('tot');

            if ($unit_id == '9999') {
                $query = "select sum(nilai_anggaran) as tot
                from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.1.01%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.1.02%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_3 = $rs->getString('tot');
            } else {
                $query = "select sum(nilai_anggaran) as tot
                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.1.01%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.1.02%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2%' and status_hapus=false";

                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_semula_3 = $rs->getString('tot');
            }



            $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
            $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
            $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

            $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
            $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
            $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

            $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
            $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
            $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
//end of warning
        }
    } else {
        $this->forward404();
    }
}

public function executeList() {

    $menu = $this->getRequestParameter('menu');
    $this->menu = $menu;

    $this->processSort();

    $this->processFilters();

    $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

// pager
    if (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbp') {
        $this->pager = new sfPropelPager('PakBukuPutihMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbb') {
        $this->pager = new sfPropelPager('PakBukuBiruMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murni') {
        $this->pager = new sfPropelPager('MurniMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(MurniMasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibp') {
        $this->pager = new sfPropelPager('MurniBukuPutihMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibb') {
        $this->pager = new sfPropelPager('MurniBukuBiruMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibbpraevagub') {

        $this->pager = new sfPropelPager('MurniBukubiruPraevagubMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);

    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1') {
        $this->pager = new sfPropelPager('Revisi1MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2') {
        $this->pager = new sfPropelPager('Revisi2MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi2MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_1') {
        $this->pager = new sfPropelPager('Revisi21MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi21MasterKegiatanPeer::KODE_KEGIATAN);
    }
    elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_2') {
        $this->pager = new sfPropelPager('Revisi22MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi22MasterKegiatanPeer::KODE_KEGIATAN);
    }
        //revisi 3 start 21-08-2017 yogie =======================
    elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3') {
        $this->pager = new sfPropelPager('Revisi3MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi3MasterKegiatanPeer::KODE_KEGIATAN);
    }  elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3_0') {
        $this->pager = new sfPropelPager('Revisi30MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi30MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi30MasterKegiatanPeer::KODE_KEGIATAN);
    }  elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3_1') {
        $this->pager = new sfPropelPager('Revisi31MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi31MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi4') {
        $this->pager = new sfPropelPager('Revisi4MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi4MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi5') {
        $this->pager = new sfPropelPager('Revisi5MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi5MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi6') {
        $this->pager = new sfPropelPager('Revisi6MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi6MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi7') {
        $this->pager = new sfPropelPager('Revisi7MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi7MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi8') {
        $this->pager = new sfPropelPager('Revisi8MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi8MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi9') {
        $this->pager = new sfPropelPager('Revisi9MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi9MasterKegiatanPeer::KODE_KEGIATAN);
    } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi10') {
        $this->pager = new sfPropelPager('Revisi10MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(Revisi10MasterKegiatanPeer::KODE_KEGIATAN);
    }
    elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'rkua') {
        $this->pager = new sfPropelPager('RkuaMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $c->addAscendingOrderByColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN);
    } else {
        $this->pager = new sfPropelPager('DinasMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        // echo 'nama: '.$nama;
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }

        // $nama_lengkap = $this->getUser()->getNamaLengkap();
        // if ($nama_lengkap != '') {
        //     $e = new Criteria;
        //     $e->add(UserHandleV2Peer::USER_ID, $nama_lengkap);
        //     $es = UserHandleV2Peer::doSelect($e);
        //     $this->satuan_kerja = $es;
        //     $unit_kerja = Array();
        //     foreach ($es as $x) {
        //         $unit_kerja[] = $x->getUnitId();
        //     }

        //     $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
        //     for ($i = 1; $i < count($unit_kerja); $i++) {
        //         $c->addOr(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
        //     }
        // }

        // if ($nama != '') {
        //     $c->add(DinasMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        // }

        if($nama == '1700'){
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, array('0300','1700','1800'), Criteria::IN);
        }else{
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }

        if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
        }
           else
        {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
        }
        $c->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KEGIATAN_ID);
    }
    $this->unit_id = $nama;
    $this->addFiltersCriteria2($c, isset($this->filters['tahap']) ? $this->filters['tahap'] : null);

        // cek apakah masih ada yg dibawah atau sama dengan PA II kegiatannya
    if (isset($this->unit_id)) {
        $query =
        "SELECT kode_kegiatan
        FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan
        WHERE ((catatan IS NOT NULL AND TRIM(catatan) <> '') OR (catatan_pembahasan IS NOT NULL AND TRIM(catatan_pembahasan) <> ''))
        AND unit_id = '".$this->unit_id."'";
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        $jml1 = $rs->getRecordCount();

        $query =
        "SELECT kode_kegiatan
        FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan
        WHERE ((catatan IS NOT NULL AND TRIM(catatan) <> '') OR (catatan_pembahasan IS NOT NULL AND TRIM(catatan_pembahasan) <> ''))
        AND status_level <= 5
        AND unit_id = '".$this->unit_id."'";
        $con = Propel::getConnection();
        $statement = $con->prepareStatement($query);
        $rs = $statement->executeQuery();
        $jml2 = $rs->getRecordCount();

        $this->kegiatan_bawah = $jml2 - $jml1;
    }

    $this->pager->setCriteria($c);
    $this->pager->setPage($this->getRequestParameter('page', 1));
    $this->pager->init();
}

protected function addFiltersCriteria2($c, $tahap) {
    if ($tahap == 'pakbp') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'pakbb') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'murni') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'murnibp') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'murnibb') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'murnibbpraevagub') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi1') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi1_1') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi2') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }

    } elseif ($tahap == 'revisi2_1') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }

    } elseif ($tahap == 'revisi2_2') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }

    } elseif ($tahap == 'revisi3') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi3_0') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi30MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi30MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi30MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi3_1') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi4') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi5') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi6') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi7') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi8') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi9') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'revisi10') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } elseif ($tahap == 'rkua') {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    } else {
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            if(preg_match('~[0-9]+~', $this->filters['nama_kegiatan']))
                {
                     $c->add(DinasMasterKegiatanPeer::USER_ID,'%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
                }  
                else
                {
                    $c->add(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
                }     
                
        }
    }
}

public function executeTemplateRKA() {
//$this->setTemplate('templateRKA');
    $this->setLayout('kosong');
    $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
}

public function executeGetHeader() {
    $this->tahap = $tahap = $this->getRequestParameter('tahap');
    if ($tahap == 'pakbp') {
        $c = new Criteria();
        $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = PakBukuPutihMasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'pakbb') {
        $c = new Criteria();
        $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = PakBukuBiruMasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'murni') {
        $c = new Criteria();
        $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(MurniMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = MurniMasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'murnibp') {
        $c = new Criteria();
        $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = MurniBukuPutihMasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'murnibb') {
        $c = new Criteria();
        $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = MurniBukuBiruMasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi1') {
        $c = new Criteria();
        $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi1MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi1_1') {
        $c = new Criteria();
        $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi1bMasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi2') {
        $c = new Criteria();
        $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi2MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi2_1') {
        $c = new Criteria();
        $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi21MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi2_2') {
        $c = new Criteria();
        $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi22MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi3') {
        $c = new Criteria();
        $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi3MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi3_0') {
        $c = new Criteria();
        $c->add(Revisi30MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi30MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi30MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi3_1') {
        $c = new Criteria();
        $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi31MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi4') {
        $c = new Criteria();
        $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi4MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi5') {
        $c = new Criteria();
        $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi5MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi6') {
        $c = new Criteria();
        $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi6MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi7') {
        $c = new Criteria();
        $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi7MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi8') {
        $c = new Criteria();
        $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi8MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi9') {
        $c = new Criteria();
        $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi9MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'revisi10') {
        $c = new Criteria();
        $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = Revisi10MasterKegiatanPeer::doSelectOne($c);
    } elseif ($tahap == 'rkua') {
        $c = new Criteria();
        $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = RkuaMasterKegiatanPeer::doSelectOne($c);
    } else {
        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
    }
    if ($master_kegiatan) {
        $this->master_kegiatan = $master_kegiatan;
    }
    $this->setLayout('kosong');
}

//irul 14 feb 2014 - ubah harga dasar
public function executeUbahhargadasar() {
        // echo "a";die();
    $unit_id = $this->getRequestParameter('unit_id');
    $detail_no = $this->getRequestParameter('detail_no');
    $kegiatan_code = $this->getRequestParameter('kegiatan_code');
    $detail_kegiatan= $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
    $komponen_harga_awal = $this->getRequestParameter('komponen_harga_awal');
   
    $status_bypass_validasi = FALSE;

    if (in_array($detail_kegiatan, array(''))) {
        $status_bypass_validasi = TRUE;
    }
        // 103169200
        // echo $komponen_harga_awal;die();

    $c_mk = new Criteria();
    $c_mk->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
    $c_mk->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
    $data_master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_mk);
        // print_r($c_mk);die();

    $c = new Criteria();
    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
    $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);

    if ($rincian_detail) {
            // validasi output, musrenbang, prioritas
        $arr_buka_edit_output = array();
        $query =
        "SELECT detail_kegiatan
        FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            array_push($arr_buka_edit_output, $rs->getString('detail_kegiatan'));
        }
        if(!in_array($rincian_detail->getDetailKegiatan(), $arr_buka_edit_output)) {
            $is_output = $rincian_detail->getIsOutput();
            $is_musrenbang = $rincian_detail->getIsMusrenbang();
            $is_urgent = $rincian_detail->getPrioritasWali();
            if($is_output) {
                $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen output, tidak bisa diedit.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
                // if($is_musrenbang) {
                //     $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen musrenbang, tidak bisa diedit.');
                //     return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                // }
            if($is_urgent) {
                $this->setFlash('gagal', 'Mohon maaf, komponen ini merupakan komponen prioritas, tidak bisa diedit.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
        }

        $volumeDiBudgeting = $rincian_detail->getVolume();
        $status_lelang = $rincian_detail->getStatusLelang();
        $pajak = $rincian_detail->getPajak();
        $harga = $rincian_detail->getKomponenHargaAwal();
            //harga awal
            // echo $harga;die();
        $satuan = $rincian_detail->getSatuan();
        $satuan_semula = $rincian_detail->getSatuanSemula();

        $nilaiBaru = $komponen_harga_awal * 1;
            // echo $nilaiBaru;die();
        $rincian_detail->setKeteranganKoefisien('1 Paket');
        $rincian_detail->setVolume(1);
        $rincian_detail->setKomponenHargaAwal($komponen_harga_awal);
        $rincian_detail->setTahapEdit(sfConfig::get('app_tahap_edit'));
        $rincian_detail->setTahap($data_master_kegiatan->getTahap());
        $rincian_detail->setPajak(0);
        $rincian_detail->setSatuan('Paket');
        $rincian_detail->setStatusLelang('lock');
        $rincian_detail->setHargaSebelumSisaLelang($harga);
        if ($satuan_semula == '') {
            $rincian_detail->setVolumeSemula($volumeDiBudgeting);
            $rincian_detail->setKoefisienSemula($volumeDiBudgeting . ' ' . $satuan);
            $rincian_detail->setSatuanSemula($satuan);
        }

        $con = Propel::getConnection();
        $querySisipan = "select max(status_level) as nilai "
        . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
        $stmt = $con->prepareStatement($querySisipan);
        $rs_level = $stmt->executeQuery();
        while ($rs_level->next()) {
            $posisi_terjauh = $rs_level->getInt('nilai');
        }
        $sisipan = false;
        if ($posisi_terjauh > 0) {
            $sisipan = true;
        }
        $rincian_detail->setStatusSisipan($sisipan);

            //cek untuk mengisi status_komponen_baru
        $status_komponen_berubah = FALSE;
        $status_komponen_baru = FALSE;
        $tahap_cek = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kegiatan_code);
        $c_pembanding_kegiatan = new Criteria();
        $c_pembanding_kegiatan->add(PembandingKegiatanPeer::UNIT_ID, $unit_id);
        $c_pembanding_kegiatan->add(PembandingKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
        $c_pembanding_kegiatan->add(PembandingKegiatanPeer::TAHAP, $tahap_cek);
        $c_pembanding_kegiatan->addDescendingOrderByColumn(PembandingKegiatanPeer::ID);
        if ($rs_pembanding_kegiatan = PembandingKegiatanPeer::doSelectOne($c_pembanding_kegiatan)) {
            $id_pembanding = $rs_pembanding_kegiatan->getId();
            $c_pembanding_komponen = new Criteria();
            $c_pembanding_komponen->add(PembandingKomponenPeer::ID_PEMBANDING_KEGIATAN, $id_pembanding);
            $c_pembanding_komponen->add(PembandingKomponenPeer::DETAIL_NO, $detail_no);
            if ($rs_pembanding_komponen = PembandingKomponenPeer::doSelectOne($c_pembanding_komponen)) {
                    //$pembanding_rekening_code = $rs_pembanding_komponen->getRekeningCode();
                    //$pembanding_komponen_name = $rs_pembanding_komponen->getKomponenName();
                    //$pembanding_satuan = $rs_pembanding_komponen->getSatuan();
                    //$pembanding_subtitle = $rs_pembanding_komponen->getSubtitle();
                $pembanding_komponen_harga = $rs_pembanding_komponen->getKomponenHarga();
                $pembanding_volume = $rs_pembanding_komponen->getVolume();
                $pembanding_keterangan_koefisien = $rs_pembanding_komponen->getKeteranganKoefisien();

                if ($komponen_harga_awal <> $pembanding_komponen_harga || 1 <> $pembanding_volume || '1 Paket' <> $pembanding_keterangan_koefisien) {
                    $level_tolak = $rincian_detail->getStatusLevelTolak();
                    if ($level_tolak >= 4) {
                        $status_komponen_berubah = TRUE;
                    } else {
                        $status_komponen_baru = TRUE;
                    }
                }
            } else {
                $status_komponen_baru = TRUE;
            }
        }
        $rincian_detail->setStatusKomponenBerubah($status_komponen_berubah);
        $rincian_detail->setStatusKomponenBaru($status_komponen_baru);
            //cek untuk mengisi status_komponen_baru

        try {
            $rd = new DinasRincianDetail();

            $array_buka_pagu_dinas_khusus = array('');
            $array_buka_pagu_kegiatan_khusus = array('');
            if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                $status_pagu_rincian = $rd->getBatasPaguPerDinasforLelang($unit_id, $kegiatan_code, $detail_no, $nilaiBaru);

            } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforLelang($unit_id, $kegiatan_code, $detail_no, $nilaiBaru);
            } else {

                if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                    if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                        $status_pagu_rincian = $rd->getBatasPaguPerDinasforLelang($unit_id, $kegiatan_code, $detail_no, $nilaiBaru);
                    } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                        $status_pagu_rincian = $rd->getBatasPaguPerKegiatanforLelang($unit_id, $kegiatan_code, $detail_no, $nilaiBaru);
                    }
                } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                    $status_pagu_rincian = 0;
                }
            }
                // var_dump('expression');die();
            if ($status_pagu_rincian == '1') {
                $con->rollback();
                $this->setFlash('gagal', 'Komponen tidak berhasil dikembalikan karena nilai total RKA Melebihi total Pagu.');
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
                // var_dump('expression');die();

                // echo sfConfig::get('app_fasilitas_cekeDelivery');die();
                // echo sfConfig::get('app_fasilitas_cekeProject');die();
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    // var_dump('expression');die();
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totNilaiRealisasiLelang = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                $lelang = 0;

                $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                $totNilaiRealisasiLelang = $rd->getRealisasiMetodeLelang($unit_id, $kegiatan_code, $detail_no);
                $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kegiatan_code, $detail_no);
                $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kegiatan_code, $detail_no);
                $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kegiatan_code, $detail_no);

                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                        //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                        //$totNilaiKontrak = $totNilaiKontrak + 10;
                }

                    // if (!($rincian_detail->getKomponenId() == '27.01.01.01' || $rincian_detail->getKomponenId() == '27.01.01.152' || $rincian_detail->getKomponenId() == '27.03.01.02')) {
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                    $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                    $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                }
                    // }

                    // INGAT
                $array_buka_validasi = array('');

                if (!in_array($detail_kegiatan, $array_buka_validasi) && ($nilaiBaru < $totNilaiRealisasi || $nilaiBaru < $totNilaiSwakelola) ) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                    } else if ($totNilaiSwakelola == 0) {
                            // ubah nilai realisasi dari semula biasa menjadi lelang
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiRealisasi, 2, ',', '.').$status_bypass_validasi);
                    }
                } else 
                if (!in_array($detail_kegiatan, $array_buka_validasi) && ($nilaiBaru < $totNilaiHps)) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                } else if (!in_array($detail_kegiatan, $array_buka_validasi) && ($ceklelangselesaitidakaturanpembayaran == 1)) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                } else if (!in_array($detail_kegiatan, $array_buka_validasi) && ($lelang > 0)) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                } else if (!in_array($detail_kegiatan, $array_buka_validasi) && ($totNilaiKontrakTidakAdaAturanPembayaran == 1)) {
                    $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                } else if (!in_array($detail_kegiatan, $array_buka_validasi) && ($nilaiBaru < $totNilaiRealisasiLelang)) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasiLelang, 0, ',', '.'));
                } else if (!in_array($detail_kegiatan, $array_buka_validasi) && ($volumeDiBudgeting < $totVolumeRealisasi && $status_lelang == 'lock')) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen ini sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                } else {
                    budgetLogger::log('Mengubah harga dasar komponen (eRevisi) dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();

                    historyUserLog::sisa_lelang_komponen_revisi($unit_id, $kegiatan_code, $detail_no, $harga, $volumeDiBudgeting, $satuan);

//irul 10 desember 2014 - edit data gmap
                    $sekarang = date('Y-m-d H:i:s');
                    $c2 = new Criteria();
                    $c2->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                    $c2->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c2->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                    $rincian_detail2 = DinasRincianDetailPeer::doSelectOne($c2);
                    if ($rincian_detail2) {
                        $query_cek_gmap = "select count(*) as jumlah "
                        . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query_cek_gmap);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $jumlah = $rs->getString('jumlah');
                        }
                        if ($jumlah > 0) {
                            $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "set last_edit_time = '$sekarang', satuan = '" . $rincian_detail2->getSatuan() . "', volume = " . $rincian_detail2->getVolume() . ", nilai_anggaran = " . $rincian_detail2->getNilaiAnggaran() . ", komponen_name = '" . $rincian_detail2->getKomponenName() . "'  "
                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();
                        }
                    }
//irul 10 desember 2014 - edit data gmap
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan. Nilai eProject : ' . number_format($nilaiTerpakai, 0, ',', '.'));
                }
                return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
//EOF Cek ke deliveri dan eproject
            }
                // var_dump('expression');die();
                //echo sfConfig::get('app_tahap_edit') . ' - ' . sfConfig::get('app_fasilitas_cekeDelivery') . ' - ' . sfConfig::get('app_fasilitas_cekeProject');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        exit;
    }
}

public function executeNoteSkpd() {
//            return $this->redirect('entri/noteSkpd?unit_id='.$this->getRequestParameter('unit_id'));


    if ($this->getRequestParameter('act') == 'editHeader') {
        $this->unit_id = $this->getRequestParameter('unit');
        $this->id = $this->getRequestParameter('id');
        $this->kegiatan = $this->getRequestParameter('kegiatan');

        $c = new Criteria();
//            $c->addSelectColumn('note_skpd');
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
        $c->add(DinasRincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $this->id);
        $notes = DinasRincianDetailPeer::doSelectOne($c);
        $note = $notes->getNoteSkpd();
//            var_dump($note);
        $this->note_skpd = $note;
    }
    if ($this->getRequestParameter('act') == 'simpan') {
        $catatan = $this->getRequestParameter('catatan');
        $this->unit_id = $this->getRequestParameter('unit');
        $this->id = $this->getRequestParameter('id');
        $this->kegiatan = $this->getRequestParameter('kegiatan');
//                 echo $this->unit_id.'.'.$catatan;
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $this->unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
        $c->add(DinasRincianDetailPeer::DETAIL_NO, $this->id);
        $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);

        if (strlen(str_replace(' ', '', $catatan)) >= 15) {
            if ($rincian_detail) {

                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $this->unit_id);
                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $this->kegiatan);
                $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, $rincian_detail->getSubtitle());
                $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::PRIORITAS);
                $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);

                $rincian_detail->setNoteSkpd($catatan);
                $rincian_detail->save();

//return $this->layout('kosong');
                return $this->redirect('entri/edit?unit_id=' . $rincian_detail->getUnitId() . '&kode_kegiatan=' . $rincian_detail->getKegiatanCode());
            }
        } else {
            $this->setFlash('gagal', 'Mohon maaf , untuk inputan catatan minimal 15 karakter');
            return $this->redirect('entri/edit?unit_id=' . $rincian_detail->getUnitId() . '&kode_kegiatan=' . $rincian_detail->getKegiatanCode());
        }
    }
}

public function executeSimulasi() {
//            $sub_id = $this->getRequestParameter('sub_id');
//            $this->sub_id = $sub_id;

    if ($this->getRequestParameter('sub_id')) {
//                    echo 'masuk';
        $sub_id = $this->getRequestParameter('sub_id');
        $c = new Criteria();
        $c->add(BappekoDinasSubtitleIndikatorPeer::SUB_ID, $sub_id);
        $rs_subtitle = BappekoDinasSubtitleIndikatorPeer::doSelectOne($c);

        if ($rs_subtitle) {
            $unit_id = $rs_subtitle->getUnitId();
            $kegiatan_code = $rs_subtitle->getKegiatanCode();
            $subtitle = $rs_subtitle->getSubtitle();
            $nama_subtitle = trim($subtitle);
//                                echo $unit_id;
        }
        $this->nama_subtitle = $nama_subtitle;
        $query = "select *  from " . sfConfig::get('app_default_schema') . ".bappeko_rincian_detail
        where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '%$nama_subtitle%' and status_hapus=false order by sub,rekening_code,komponen_name";

        $con = Propel::getConnection(BappekoRincianDetailPeer::DATABASE_NAME);
        $statement = $con->prepareStatement($query);
        $rs_rinciandetail = $statement->executeQuery();
        $this->rs_rinciandetail = $rs_rinciandetail;
//                        var_dump($query);
        $c_rincianDetail = new Criteria();
        $c_rincianDetail->add(BappekoRincianDetailPeer::UNIT_ID, $unit_id);
        $c_rincianDetail->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
        $c_rincianDetail->add(BappekoRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
        $c_rincianDetail->add(BappekoRincianDetailPeer::STATUS_HAPUS, false);
        $c_rincianDetail->add(BappekoRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
        $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::KODE_SUB);
        $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::REKENING_CODE);
        $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::KOMPONEN_NAME);
        $rs_rd = BappekoRincianDetailPeer::doSelect($c_rincianDetail);
//                        var_dump($rs_rd);
        $this->rs_rd = $rs_rd;

        $this->id = $sub_id;
        $this->rinciandetail = 'ada';
//          $this->setLayout('kosong');
        $this->setLayout('layoutPop');
    }
}

public function executeHome() {
    if ($this->getRequestParameter('Setuju')) {
//                return $this->redirect('entri/home');
    }
}

public function executePilihKelurahan() {
    $this->id_kecamatan = $this->getRequestParameter('b');
}

//irul 23 jan 2014
public function executeCatatan() {
    $this->unit_id = $this->getRequestParameter('unit_id');
    $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
    $this->catatan = $this->getRequestParameter('catatan');
}

public function executeCatatanDetail() {
    $catatan = $this->getRequestParameter('catatan');
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

    $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
    . "set catatan='$catatan' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";

    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $stmt->executeQuery();
    return $this->redirect('entri/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
}

//irul 23 jan 2014
//irul 18 maret 2014
public function executeNoteSkpdHeader() {
    $catatan = $this->getRequestParameter('catatan');
    $this->unit_id = $this->getRequestParameter('unit');
    $this->id = $this->getRequestParameter('id');
    $this->kegiatan = $this->getRequestParameter('kegiatan');
}

//irul 18 maret 2014

public function executeKonfirmasikegiatan() {
    try {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $user_id = $this->getRequestParameter('user_id');

        $query = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian
        set rincian_confirmed = true, rincian_level = 3, lock = true, rincian_selesai = true 
        where unit_id='$unit_id' and kegiatan_code = '$kode_kegiatan'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();

        budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' telah dikonfirmasi oleh ' . $user_id);
        $this->setFlash('berhasil', "Kegiatan $kode_kegiatan telah berhasil dikonfirmasi dan dikunci oleh $user_id. Untuk perubahan yang bersifat Urgent, silahkan menghubungi Penyelia Anda.");
    } catch (Exception $exc) {
        $this->setFlash('gagal', $exc->getTraceAsString());
    }
    return $this->redirect('entri/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
}

public function executeSimpanCatatan() {
    $unit_id = $this->getRequestParameter('unit_id');
    $kegiatan_code = $this->getRequestParameter('kegiatan_code');
    $detail_no = $this->getRequestParameter('detail_no');
    $catatan_skpd = $this->getRequestParameter('catatan_skpd');
    $ket_skpd = $this->getRequestParameter('keterangan_skpd');
    $detail_kegiatan = $unit_id.'.'.$kegiatan_code.'.'.$detail_no;
    //die("masuk sini :".$ket_skpd);

    if (strlen(str_replace(' ', '', $catatan_skpd)) < 0 ) {
        $this->setFlash('gagal', 'Mohon maaf, Inputan catatan skpd');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }
    if (strlen(str_replace(' ', '', $ket_skpd)) < 0 ) {
        $this->setFlash('gagal', 'Mohon maaf, Inputan keterangan skpd');
        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }
 
    $c = new Criteria();
    $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
    $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
    $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
    $rs_rinciandetail = DinasRincianDetailPeer::doSelectOne($c);

    $data_lama_nilai_anggaran = $rs_rinciandetail->getNilaiAnggaran();
    $data_lama_detail_name = $rs_rinciandetail->getDetailName();


    $query = "select count(dp.pekerjaan_id) as hasil
                    from eproject.detail_kegiatan dk, eproject.detail_pekerjaan dp
                    where dp.detail_kegiatan_id=dk.id and 
                    dk.kode_detail_kegiatan= '".$rs_rinciandetail->getDetailKegiatan()."'";

    $con = Propel::getConnection();
    $statement = $con->prepareStatement($query);
    $list_pekerjaan = $statement->executeQuery();
    while($list_pekerjaan->next())
    { 
     $nilai=$list_pekerjaan->getString('hasil');
    }

    $ket_skpd= $rs_rinciandetail->getDetailName().' '.$ket_skpd;

    if($nilai > 1 && $rs_rinciandetail->getSatuan() != 'Paket' && $rs_rinciandetail->getTipe() != 'EST')
    {
        $ket_skpd= $ket_skpd.', dst.';
    }

    //die("masuk sini");
    $totNilaiKontrak=0;
    $totNilaiSwakelola=0;
    $lelang=0;
    // if($this->getRequestParameter('keterangan_skpd') == ' ')
    //    $ket_skpd = $rs_rinciandetail->getDetailName();
    //untuk cek keterangan terhadap swakelola kontrak dan lelang
       $rd = new DinasRincianDetail();
       if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                //$totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $data_lama_nilai_anggaran);
                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                        $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                        $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                        //$totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                        $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                    }
                }
         }
       
      
         //array simpan catatan
        $array_buka_keterangan= array ('3200','1400','0400','2800','2600','2300','2400','3000','1221','0305','0310','1100','2500','0300','2200','1800','0308','2700','1700','2000');                                         
        if (($totNilaiKontrak > 0 || $totNilaiSwakelola > 0 || $lelang > 0) && $ket_skpd !=  $rs_rinciandetail->getDetailName() &&  (!in_array($unit_id, $array_buka_keterangan)))
            {
                        $this->setFlash('gagal', 'Terdapat nilai kontrak/swakelola/lelang, tidak dapat merubah keterangan');                           
                        return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);

             }
        
         else
         {
              $rs_rinciandetail->setNoteSkpd($catatan_skpd);
              $rs_rinciandetail->setDetailName($ket_skpd);
              $rs_rinciandetail->setIdPekerjaanPsp($data_lama_detail_name);
              $rs_rinciandetail->save();
              $this->setFlash('berhasil', 'Berhasil menyimpan Catatan SKPD.');
              return $this->redirect('entri/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
         }  

    
    
}

public function executeAjaxrefreshjalan() {

}

public function executeSoal() {
    $this->unit_id = $this->getRequestParameter('unit_id');
    $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
    $this->setTemplate('soal');
        // $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
}

    // aminudin 05-07-2017
public function executeSaveSoal() {
    $con = Propel::getConnection();
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

    $jawab1 = $this->getRequestParameter('jawaban1');
    $jawab2 = $this->getRequestParameter('jawaban2');
    $catatan_dinas = $this->getRequestParameter('catatan_dinas');


    if (strlen(strip_tags($catatan_dinas)) == 0) {
        $this->setFlash('gagal', 'Catatan dinas harus diisi.');
        $this->redirect("entri/soal?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
    }

        // $con = Propel::getConnection();
        // $con->begin();
        // $query = "update ". sfConfig::get('app_default_schema') .".rincian_bappeko set jawaban1_dinas = $jawab1, jawaban2_dinas = $jawab2, catatan_dinas = '$catatan_dinas' where unit_id = '$unit_id' and kode_kegiatan = '$kode_kegiatan'";
        // $stmt = $con->prepareStatement($query);
        // $stmt->executeQuery();
        // $con->commit();

    $rin = new RincianBappeko();
    $rin->setUnitId($unit_id);
    $rin->setKodeKegiatan($kode_kegiatan);
    $rin->setTahap(DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan));
    if ($jawab1 == 'false')
        $rin->setJawaban1Dinas(false);
    else
        $rin->setJawaban1Dinas(true);
    if ($jawab2 == 'false')
        $rin->setJawaban2Dinas(false);
    else
        $rin->setJawaban2Dinas(true);
    $rin->setCatatanDinas($catatan_dinas);
    if ($jawab1 == 'false' && $jawab2 == 'false') {
            // buka kunci ke dinas
        $rin->setStatusBuka(true);
        $sql = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian "
        . "set rincian_level='2' "
        . "where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";

        $stmt = $con->prepareStatement($sql);
        $stmt->executeQuery();
    }
    $rin->save();
    $con->commit();
    $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
}

public function executeMenuTarikDevplan() {
    if ($this->getRequestParameter('unit_id') && $this->getUser()->getUnitId() == $this->getRequestParameter('unit_id')) {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $c = new Criteria();
        $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
        $rs_kunci = DinasRincianPeer::doSelectOne($c);
        if($rs_kunci->getLock()) {
            $this->setFlash('gagal', 'Kegiatan masih dikunci admin');
            return $this->redirect('entri/list?unit_id=' . $unit_id);
        } else if($rs_kunci->getRincianLevel() > 2) {
            $this->setFlash('gagal', 'Kegiatan masih dikunci penyelia');
            return $this->redirect('entri/list?unit_id=' . $unit_id);
        }

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $this->rs_kegiatan = $rs_kegiatan;

        $komponen_terambil = array();
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
        $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
        foreach ($rs_budgeting as $key => $budgeting) {
            $komponen = $budgeting->getKomponenId();
            $subtitle = $budgeting->getSubtitle();
            $komponen_terambil["$komponen"] = 1;
            $komponen_terambil["$subtitle"] = 1;
        }
        $this->komponen_terambil = $komponen_terambil;

        $query =
        "SELECT rd.detail_no, rd.komponen_id, rd.subtitle, rd.volume, rd.komponen_name, rd.satuan, rd.pajak, rd.komponen_harga_awal, rd.nilai_anggaran, k.komponen_name AS komponen_name_budgeting, k.satuan AS satuan_budgeting, CASE WHEN k.komponen_non_pajak = FALSE THEN 10 ELSE 0 END AS pajak_budgeting, k.komponen_harga AS komponen_harga_awal_budgeting, (rd.volume * (CASE WHEN k.komponen_tipe = 'FISIK' THEN k.komponen_harga_bulat ELSE k.komponen_harga END) * (CASE WHEN k.komponen_non_pajak = FALSE THEN 1.1 ELSE 1 END)) AS nilai_anggaran_budgeting
        FROM " . sfConfig::get('app_default_schema') . ".bappeko_rincian_detail rd
        LEFT JOIN " . sfConfig::get('app_default_schema') . ".komponen k ON rd.komponen_id = k.komponen_id
        WHERE rd.status_hapus = FALSE
        AND rd.unit_id = '$unit_id'
        AND rd.kegiatan_code = '$kode_kegiatan'
        ORDER BY rd.subtitle, rd.komponen_name";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_rinciandetail = $stmt->executeQuery();
        $rs_copy = $stmt->executeQuery();
        $this->rs_rinciandetail = $rs_copy;

        $arr_rekening = array();
        while($rs_rinciandetail->next()) {
            $query =
            "SELECT kr.rekening_code, r.rekening_name
            FROM " . sfConfig::get('app_default_schema') . ".komponen_rekening kr
            INNER JOIN " . sfConfig::get('app_default_schema') . ".rekening r ON kr.rekening_code = r.rekening_code
            WHERE kr.komponen_id = '" . $rs_rinciandetail->getString('komponen_id') . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_rekening = $stmt->executeQuery();

            $arr_rekening[$rs_rinciandetail->getString('komponen_id')][0] = 'Pilih Rekening';
            while($rs_rekening->next()) {
                $arr_rekening[$rs_rinciandetail->getString('komponen_id')][$rs_rekening->getString('rekening_code')] = $rs_rekening->getString('rekening_code') . ' - ' . $rs_rekening->getString('rekening_name');
            }
        }
        $this->arr_rekening = $arr_rekening;

        $this->unit_id = $unit_id;
        $this->kode_kegiatan = $kode_kegiatan;
    }
}

public function executeMenuViewMusrenbang() {
    if ($this->getRequestParameter('unit_id') && $this->getUser()->getUnitId() == $this->getRequestParameter('unit_id')) {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $this->rs_kegiatan = $rs_kegiatan;

        $komponen_terambil = array();
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
        $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
        foreach ($rs_budgeting as $key => $budgeting) {
            $komponen = $budgeting->getKomponenId();
            $subtitle = $budgeting->getSubtitle();
            $komponen_terambil["$komponen"] = 1;
            $komponen_terambil["$subtitle"] = 1;
        }
        $this->komponen_terambil = $komponen_terambil;

        $query =
        "SELECT rd.musrenbang_id, rd.komponen_id, rd.dari, rd.level, rd.keterangan, rd.catatan, rd.volume, rd.satuan, rd.komponen_name, rd.lokasi, k.komponen_name AS komponen_name_budgeting, k.satuan AS satuan_budgeting, CASE WHEN k.komponen_non_pajak = FALSE THEN 10 ELSE 0 END AS pajak_budgeting, k.komponen_harga AS komponen_harga_awal_budgeting, (rd.volume * (CASE WHEN k.komponen_tipe = 'FISIK' THEN k.komponen_harga_bulat ELSE k.komponen_harga END) * (CASE WHEN k.komponen_non_pajak = FALSE THEN 1.1 ELSE 1 END)) AS nilai_anggaran_budgeting
        FROM " . sfConfig::get('app_default_schema') . ".musrenbang_rincian_detail rd
        LEFT JOIN " . sfConfig::get('app_default_schema') . ".komponen k ON rd.komponen_id = k.komponen_id
        WHERE rd.unit_id = '$unit_id'
        AND rd.kegiatan_code = '$kode_kegiatan'
        ORDER BY rd.komponen_name, rd.lokasi";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_rinciandetail = $stmt->executeQuery();
        $rs_copy = $stmt->executeQuery();
        $this->rs_rinciandetail = $rs_copy;

        $this->unit_id = $unit_id;
        $this->kode_kegiatan = $kode_kegiatan;
    }
}

public function executeUbahVolumeOrang() {
    if ($this->getRequestParameter('unit_id') && $this->getUser()->getUnitId() == $this->getRequestParameter('unit_id')) {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $c = new Criteria();
        $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        $this->rs_kegiatan = $rs_kegiatan;

        $komponen_terambil = array();
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
        $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
        $rs_budgeting = DinasRincianDetailPeer::doSelect($c);
        foreach ($rs_budgeting as $key => $budgeting) {
            $komponen = $budgeting->getKomponenId();
            $subtitle = $budgeting->getSubtitle();
            $komponen_terambil["$komponen"] = 1;
            $komponen_terambil["$subtitle"] = 1;
        }
        $this->komponen_terambil = $komponen_terambil;


        $query =
        "SELECT rd.komponen_name , rd.volume, rd.satuan, rd.detail_name, rd.keterangan_koefisien, rd.komponen_harga, rd.nilai_anggaran, rd.volume_orang, rd.detail_no 
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
        WHERE rd.unit_id = '$unit_id'
        AND rd.kegiatan_code = '$kode_kegiatan'
        AND rd.satuan ILIKE '%Orang Bulan%' OR rd.satuan ILIKE '%Orang/Bulan%'           
        AND (rd.komponen_id ilike '2.1.1.01.01.01.008%' or rd.komponen_id ilike '2.1.1.03.05.01.001%' or rd.komponen_id ilike '2.1.1.01.01.01.004%' or rd.komponen_id ilike '2.1.1.01.01.02.099%' or rd.komponen_id ilike '2.1.1.01.01.01.005%' or rd.komponen_id ilike '2.1.1.01.01.01.006%')
        AND rd.status_hapus=false
        ORDER BY rd.komponen_name";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_rinciandetail = $stmt->executeQuery();
        $rs_copy = $stmt->executeQuery();
        $this->rs_rinciandetail = $rs_copy;

        $this->unit_id = $unit_id;
        $this->kode_kegiatan = $kode_kegiatan;
    }
}

public function executeProsesUbahVolumeOrang() {
    $detail_no = $this->getRequestParameter('detail_no');
    $volume_orang = $this->getRequestParameter('volume_orang');
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

    try {
        $con = Propel::getConnection();
        $con->begin();

        foreach( $detail_no as $key => $no ) {
            if(!empty($volume_orang[$key])) {

                $detail=$unit_id.'.'.$kode_kegiatan.'.'.$no;

                $vol_sebelum="SELECT volume_orang  from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail where detail_kegiatan='".$detail."'";
                $st = $con->prepareStatement($vol_sebelum);
                $rs1=$st->executeQuery();
                while($rs1->next())
                {
                    $cek = "SELECT count(*) as hasil from  " . sfConfig::get('app_default_schema') . ".log_volume_orang where 
                    detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";                   
                    $stmt3=  $con->prepareStatement($cek);
                    $rs=$stmt3->executeQuery();
                    while($rs->next())
                    { 
                        $nilai=$rs->getString('hasil');
                        if($nilai==0)
                        {
                         $insertvol ="INSERT INTO " . sfConfig::get('app_default_schema') . ".log_volume_orang(
                         detail_kegiatan, volume_semula, volume_menjadi, tahap, waktu_edit)
                         VALUES ('" . $detail. "',".$rs1->getString('volume_orang').", $volume_orang[$key],'".sfConfig::get('app_tahap_detail')."','".date('Y-m-d h:i:sa')."')";
                         $stmt2 = $con->prepareStatement($insertvol);
                         $stmt2->executeQuery();
                     }
                     else
                     {
                        $updatevol =
                        "UPDATE " . sfConfig::get('app_default_schema') . ".log_volume_orang
                        SET volume_menjadi=$volume_orang[$key],tahap='".sfConfig::get('app_tahap_detail')."',waktu_edit='".date('Y-m-d h:i:sa')."' where detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";
                        $stmt2 = $con->prepareStatement($updatevol);
                        $stmt2->executeQuery();
                    }

                } 
            }

            $query =
            "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            set volume_orang = $volume_orang[$key]
            WHERE unit_id = '$unit_id'
            AND kegiatan_code = '$kode_kegiatan'
            AND detail_no = $no";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $queryrka =
            "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
            set volume_orang = $volume_orang[$key]
            WHERE unit_id = '$unit_id'
            AND kegiatan_code = '$kode_kegiatan'
            AND detail_no = $no";
            $stmt1 = $con->prepareStatement($queryrka);
            $stmt1->executeQuery();


        }
    }
            //die($query);
} catch(Exception $e) {
    $con->rollback();
    $this->setFlash('gagal', 'Gagal : ' . $e->getMessage());
    return $this->redirect('entri/ubahVolumeOrang?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
}

$con->commit();

$this->setFlash('berhasil', 'Berhasil Ubah Volume Orang.');
return $this->redirect('entri/ubahVolumeOrang?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
}

public function executeProsesTarikDevplan() {
    $komponens = $this->getRequestParameter('pilihAction');
    $unit_id = $this->getRequestParameter('unit_id');
    $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

    $c = new Criteria();
    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);

    $c = new Criteria();
    $c->add(DinasRincianPeer::UNIT_ID, $unit_id);
    $c->add(DinasRincianPeer::KEGIATAN_CODE, $kode_kegiatan);
    $rs_kunci = DinasRincianPeer::doSelectOne($c);
    if($rs_kunci->getLock()) {
        $this->setFlash('gagal', 'Kegiatan masih dikunci admin');
        return $this->redirect('entri/menuTarikDevplan?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    } else if($rs_kunci->getRincianLevel() > 2) {
        $this->setFlash('gagal', 'Kegiatan masih dikunci penyelia');
        return $this->redirect('entri/menuTarikDevplan?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    try {
        $detail_no_akhir = 0;
        $con_cek = Propel::getConnection();
        $query =
        "SELECT MAX(detail_no) AS max
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
        WHERE rd.unit_id = '$unit_id'
        AND rd.kegiatan_code = '$kode_kegiatan'";
        $stmt = $con_cek->prepareStatement($query);
        $rs_detno = $stmt->executeQuery();
        while($rs_detno->next()) {
            $detail_no_akhir = $rs_detno->getInt('max');
            if($detail_no_akhir == NULL)
                $detail_no_akhir = 0;
        }

        $con = Propel::getConnection();
        $con->begin();
        foreach ($komponens as $key => $detail_no) {
            $c = new Criteria();
            $c->add(BappekoRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(BappekoRincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_bappeko = BappekoRincianDetailPeer::doSelectOne($c);

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $rs_bappeko->getKomponenId());
            $rs_komponen = KomponenPeer::doSelectOne($c);

            $tipe = $rs_komponen->getKomponenTipe();
            $rekening_code = $this->getRequestParameter('rekening_'.$detail_no);
            try {
                if(empty($rekening_code)) {
                    $this->setFlash('gagal', 'Isian rekening tidak boleh kosong: '.$rs_komponen->getKomponenName());
                    return $this->redirect('entri/menuTarikDevplan?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                }
            } catch(Exception $e) {
                    // ????????
                    // kalau tidak di die(), dia lolos
                die();
            }
            $komponen_id = trim($rs_bappeko->getKomponenId());
            $detail_name = '';
            $volume = $rs_bappeko->getVolume();
            $keterangan_koefisien = $rs_bappeko->getVolume() . ' ' . $rs_komponen->getSatuan();
            $subtitle = $rs_kegiatan->getNamaKegiatan();
            if($tipe == 'FISIK')
                $komponen_harga = $rs_komponen->getKomponenHargaBulat();
            else
                $komponen_harga = $rs_komponen->getKomponenHarga();
            $komponen_harga_awal = $komponen_harga;
            $komponen_name = $rs_komponen->getKomponenName();
            $satuan = $rs_komponen->getSatuan();
            $pajak = ($rs_komponen->getKomponenNonPajak() == FALSE ? 10 : 0);
            $sub = '';
            $kode_sub = '';
            $last_update_user = '';
            $last_update_time = date('Y-m-d H:i:s');
            $tahap = 'murni';
            $tahap_edit = 'murni';
            $tahun = sfConfig::get('app_tahun_default');
            $kecamatan = '';
            $note_skpd = 'SSH tarikan dari devplan';
            $note_peneliti = '';
            $akrual_code = '';
            $tipe2 = '';
            $note_tapd = '';
            $note_bappeko = '';

            try {
                $array_buka_pagu_dinas_khusus = array('');
                $array_buka_pagu_kegiatan_khusus = array('');
                $status_pagu_rincian = 0;
                $rd_function = new DinasRincianDetail();
                if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id, $kode_kegiatan, $komponen_id, $pajak, $volume, 1, 1, 1, array(''), array(''));
                } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id, $kode_kegiatan, $komponen_id, $pajak, $volume, 1, 1, 1, array(''), array(''));
                } else {
                    if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                        $kecamatan_unit_id = substr($unit_id, 0, 2);
                        if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                            $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id, $kode_kegiatan, $komponen_id, $pajak, $volume, 1, 1, 1, array(''), array(''));
                        } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                            $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id, $kode_kegiatan, $komponen_id, $pajak, $volume, 1, 1, 1, array(''), array(''));
                        }
                    } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                        $status_pagu_rincian = 0;
                    }
                }
                if ($status_pagu_rincian == '1') {
                    $this->setFlash('gagal', 'Komponen tidak berhasil ditarik karena nilai total RKA Melebihi total Pagu SKPD.');
                    return $this->redirect('entri/menuTarikDevplan?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
                }
            } catch(Exception $err) {
                    // ????????
                    // kalau tidak di die(), dia lolos
                die();
            }

                // penanda
            $potongBPJS = 'FALSE';
            $iuranBPJS = 'FALSE';
            $ini_komponen_jkn = 'FALSE';
            $ini_komponen_jkk = 'FALSE';
            if ($rs_komponen->getIsPotongBpjs() == true) {
                $potongBPJS = 'TRUE';
            } else {
                $potongBPJS = 'FALSE';
            }

            if ($rs_komponen->getIsIuranBpjs() == true) {
                $iuranBPJS = 'TRUE';
            } else {
                $iuranBPJS = 'FALSE';
            }

            if((strpos($rs_komponen->getRekening(), '5.2.1.02.02') !== FALSE || strpos($rs_komponen->getRekening(), '5.1.02.02.01.080') !== FALSE) && ($rs_komponen->getSatuan() == 'Orang Bulan' || $rs_komponen->getSatuan() == '%')) {
                if(strpos(strtolower($rs_komponen->getKomponenName()), 'jk') == FALSE) {
                    $potongBPJS = 'TRUE';
                    $iuranBPJS = 'FALSE';
                    $ini_komponen_jkn = 'FALSE';
                    $ini_komponen_jkk = 'FALSE';
                } else if($komponen_id == '23.01.01.04.18') {
                    $potongBPJS = 'FALSE';
                    $iuranBPJS = 'TRUE';
                    $ini_komponen_jkn = 'FALSE';
                    $ini_komponen_jkk = 'TRUE';
                } else if($komponen_id == '23.01.01.04.17') {
                    $potongBPJS = 'FALSE';
                    $iuranBPJS = 'TRUE';
                    $ini_komponen_jkn = 'TRUE';
                    $ini_komponen_jkk = 'FALSE';
                } else if($komponen_id == '23.01.01.04.19') {
                    $potongBPJS = 'FALSE';
                    $iuranBPJS = 'TRUE';
                    $ini_komponen_jkn = 'FALSE';
                    $ini_komponen_jkk = 'FALSE';
                }
            } else {
                $potongBPJS = 'FALSE';
            }
                // end of penanda
            $detail_no_akhir++;
            $detail_kegiatan = $unit_id || '.' || $kode_kegiatan || '.' || $detail_no_akhir;

            $query =
            "INSERT INTO " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
            (unit_id, kegiatan_code, detail_no, tipe, rekening_code,
            komponen_id, detail_name, volume, keterangan_koefisien, subtitle,
            komponen_harga, komponen_harga_awal, komponen_name, satuan, pajak,
            sub, kode_sub, last_update_user, last_update_time, tahap, tahap_edit,
            tahun, kecamatan, note_skpd, note_peneliti, akrual_code, tipe2,
            note_tapd, note_bappeko, is_potong_bpjs, is_iuran_bpjs, is_iuran_jkn,
            is_iuran_jkk, detail_kegiatan)
            VALUES ('$unit_id', '$kode_kegiatan', $detail_no_akhir, '$tipe', '$rekening_code',
            '$komponen_id', '$detail_name', $volume, '$keterangan_koefisien', '$subtitle',
            $komponen_harga, $komponen_harga_awal, '$komponen_name', '$satuan', $pajak,
            '$sub', '$kode_sub', '$last_update_user', '$last_update_time', '$tahap', '$tahap_edit',
            '$tahun', '$kecamatan', '$note_skpd', '$note_peneliti', '$akrual_code', '$tipe2',
            '$note_tapd', '$note_bappeko', $potongBPJS, $iuranBPJS, $ini_komponen_jkn,
            $ini_komponen_jkk, '$detail_kegiatan')";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
        }
    } catch(Exception $e) {
        $con->rollback();
        $this->setFlash('gagal', 'Gagal menarik komponen: ' . $e->getMessage());
        return $this->redirect('entri/menuTarikDevplan?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }
    $con->commit();

    $this->setFlash('berhasil', 'Berhasil menarik komponen dari Devplan.');
    return $this->redirect('entri/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
}

    public function executeCekCatatanRekening(){
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $rekening = $this->getRequestParameter('rekening');

        $c = new Criteria();
        $c->add(CatatanRekeningPeer::UNIT_ID, $unit_id);
        $c->add(CatatanRekeningPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(CatatanRekeningPeer::REKENING_CODE, $rekening);
        $c->add(CatatanRekeningPeer::TAHAP, sfConfig::get('app_tahap_edit'));
        $load_catatan = CatatanRekeningPeer::doSelectOne($c);

        $data = array();
        if(empty($load_catatan)){
            $data['ada'] = 0;
            $data['catatan'] = '';
        }else{
            $data['ada'] = 1;
            $data['catatan'] = $load_catatan->getCatatan();
        }

        return $this->renderText(json_encode($data));
        // return json_encode($data);
    }

    public function executeTambahCatatanRekening(){
        $unit_id = $this->getRequestParameter('catatan_unit_id');
        $kegiatan_code = $this->getRequestParameter('catatan_kegiatan_code');
        $rekening = $this->getRequestParameter('catatan_rekening_code');
        $catatan = $this->getRequestParameter('catatan');

        $con = Propel::getConnection();
        $con->begin();

        $c = new Criteria();
        $c->add(CatatanRekeningPeer::UNIT_ID, $unit_id);
        $c->add(CatatanRekeningPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(CatatanRekeningPeer::REKENING_CODE, $rekening);
        if($load_catatan = CatatanRekeningPeer::doSelectOne($c)){
            // $load_catatan->setCatatan($catatan);
            // $load_catatan->save();
            $queryUpdate = "update ebudget.catatan_rekening set catatan='$catatan' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and rekening_code='$rekening' and tahap='".sfConfig::get('app_tahap_edit')."'";
            // $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryUpdate);
            $rs = $stmt->executeQuery();
            $con->commit();
        }else{
            $simpan_catatan = new CatatanRekening();
            $simpan_catatan->setUnitId($unit_id);
            $simpan_catatan->setKegiatanCode($kegiatan_code);
            $simpan_catatan->setRekeningCode($rekening);
            $simpan_catatan->setCatatan($catatan);
            $simpan_catatan->setTahap(sfConfig::get('app_tahap_edit'));
            $simpan_catatan->save();
            $con->commit();
        }

        $this->setFlash('berhasil', 'Telah berhasil disimpan');
        return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kegiatan_code");
    }

    public function executeHapusCatatan(){
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $rekening = $this->getRequestParameter('rekening_code');

        $queryHapus = "delete from ebudget.catatan_rekening where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and rekening_code='$rekening' and tahap='".sfConfig::get('app_tahap_edit')."'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($queryHapus);
        $rs = $stmt->executeQuery();

        $this->setFlash('berhasil', 'Telah berhasil dihapus');
        return $this->redirect("entri/edit?unit_id=$unit_id&kode_kegiatan=$kegiatan_code");
    }
}

?>
