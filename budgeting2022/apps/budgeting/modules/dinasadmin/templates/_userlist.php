<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <thead class="head_peach">
            <tr>
                <th>ID</th>
                <th>Nama User</th>
                <th>Status</th>
                <th>Action</th>  
            </tr>
        </thead>
        <tbody>
                <?php foreach ($pager->getResults() as $userlist): ?>
                    <tr>
                        <td><?php echo $userlist->getUserId() ?></td>
                        <td><?php echo $userlist->getUserName() ?></td>
                        <?php if ($userlist->getUserEnable() == true) { ?>
                            <td class="text-center text-bold">
                                Aktif
                            </td>    
                        <?php } else { ?>
                            <td class="text-center bg-maroon">
                                Non-Aktif
                            </td>    
                        <?php }
                        ?>
                        </td>
                        <td style="text-align: left">
                            <div class="btn-group">
                                <?php
                                $c = new Criteria();
                                $c->add(SchemaAksesV2Peer::USER_ID, $userlist->getUserId());
                                $c->add(SchemaAksesV2Peer::LEVEL_ID, 16);
                                if (SchemaAksesV2Peer::doSelectOne($c)) {
                                    echo link_to('<i class="fa fa-edit"></i> Edit', 'dinasadmin/editUserBappeko?id=' . $userlist->getUserId(), array('class' => 'btn btn-success btn-flat btn-xs'));
                                } else {
                                    echo link_to('<i class="fa fa-edit"></i> Edit', 'dinasadmin/editUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-success btn-flat btn-xs'));
                                }
                                if ($sf_user->hasCredential('peneliti')) {
                                    if ($userlist->getUserEnable() == true) {
                                        echo link_to('<i class="fa fa-lock"></i> Kunci User', 'dinasadmin/kunciUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-default btn-flat btn-xs'));
                                    } else {
                                        echo link_to('<i class="fa fa-unlock"></i> Buka User', 'dinasadmin/bukaUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-default btn-flat btn-xs'));
                                    }
                                }
                                ?>                            
                            </div>
                        </td>
                    </tr>
                <?php endforeach;
                ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'dinasadmin/userlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "dinasadmin/userlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'dinasadmin/userlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>