<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Ubah User - (<?php echo $data_user->getUserId(); ?>)</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
          <li class="breadcrumb-item active">Ubah User</li>
        </ol>
      </div>
    </div>
    <?php
        echo 'User ' . $data_user->getUserId() . ' mempunyai user role sebagai <strong>';
        if ($level_id == 1) {
            echo 'Dinas Entri';
        } else if ($level_id == 2) {
            echo 'Penyelia';
        } else if ($level_id == 3) {
            echo 'Tim Data';
        } else if ($level_id == 4) {
            echo 'Tim Rekening';
        } else if ($level_id == 7) {
            echo 'Dewan';
        } else if ($level_id == 8) {
            echo 'Bappeko';
        } else if ($level_id == 9) {
            echo 'Administrator';
        } else if ($level_id == 10) {
            echo 'Bappeko-PRK';
        } else if ($level_id == 11) {
            echo 'Bappeko-Mitra';
        } else if ($level_id == 12) {
            echo 'Viewer';
        } else if ($level_id == 13) {
            echo 'Dinas PPTK';
        } else if ($level_id == 14) {
            echo 'Dinas KPA';
        } else if ($level_id == 15) {
            echo 'Dinas PA';
        }
        echo '</strong>';
    ?>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<?php if ($level_id == 1 || $level_id == 13 || $level_id == 14 || $level_id == 15) { ?>
<section class="content">
    <?php include_partial('dinasadmin/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fab fa-foursquare"></i> Form
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('dinasadmin/saveedituser', array('method' => 'post', 'class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>ID User</label>
                            <?php echo input_tag('user', $data_user->getUserId(), array('class' => 'form-control', 'placeholder' => 'Isikan User ID', 'required' => 'required', 'readonly' => 'true')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <?php if ($level_id == 1): ?>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>ID User</label>
                            <?php echo input_tag('user', $data_user->getUserId(), array('class' => 'form-control', 'placeholder' => 'Isikan User ID', 'required' => 'required', 'readonly' => 'true')); ?>
                        </div>
                      </div>
                      <?php endif; ?>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama Sekarang</label>
                            <?php echo input_tag('user_name', $data_user->getUserName(), array('class' => 'form-control', 'placeholder' => 'Isikan Nama', 'required' => 'required', 'readonly' => 'true')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>NIP Sekarang</label>
                            <?php echo input_tag('nip_awal', $data_user->getNip(), array('class' => 'form-control', 'placeholder' => 'Isikan NIP', 'required' => 'required', 'readonly' => 'true')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Pangkat Sekarang</label>
                            <?php echo input_tag('jabatan', $data_user->getJabatan(), array('class' => 'form-control', 'placeholder' => 'Isikan jabatan', 'readonly' => 'true')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Ubah Nama Pengampu</label>
                            <?php
                            $c = new Criteria();
                            $c->addJoin(PegawaiPerformancePeer::SKPD_ID, MasterSkpdPeer::SKPD_ID);
                            $c->addAnd(MasterSkpdPeer::SKPD_KODE, $sf_user->getUnitId());
                            $c->addAnd(PegawaiPerformancePeer::NIP, '%K%', Criteria::NOT_ILIKE);
                            $c->addAnd(PegawaiPerformancePeer::NIP, '%P%', Criteria::NOT_ILIKE);
                            $c->addAnd(PegawaiPerformancePeer::NIP, '%D%', Criteria::NOT_ILIKE);
                            $c->addAscendingOrderByColumn(PegawaiPerformancePeer::NAMA);
                            $rs_pegawai = PegawaiPerformancePeer::doSelect($c);
                            echo select_tag('nip_br', objects_for_select($rs_pegawai, 'getNip', 'getNipNama', null, 'include_custom=---Pilih NIP Pengganti---'), array('class' => 'js-example-basic-single select2', 'style' => 'width:100%'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <?php if ($level_id == 1): ?>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>ID User</label>
                            <?php echo input_tag('user', $data_user->getUserId(), array('class' => 'form-control', 'placeholder' => 'Isikan User ID', 'required' => 'required', 'readonly' => 'true')); ?>
                        </div>
                      </div>
                      <?php endif; ?>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama Baru</label>
                            <?php echo input_tag('user_name_baru', '', array('class' => 'form-control', 'placeholder' => 'Isikan Nama', 'required' => 'required', 'readonly' => 'true')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>Pangkat Baru</label>
                            <?php echo input_tag('jabatan_baru', '', array('class' => 'form-control', 'placeholder' => 'Isikan jabatan', 'readonly' => 'true')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>User Role</label>
                            <select name="role" class="form-control select2" id="role" <?php if ($level_id == 1) echo 'disabled' ?>>
                                <option value="13" <?php if ($level_id == 13) echo 'selected' ?>>PPTK</option>
                                <option value="14" <?php if ($level_id == 14) echo 'selected' ?>>KPA/PPKM</option>
                            </select>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>E-mail</label>
                            <?php echo input_tag('email', $data_user->getEmail(), array('type' => 'email', 'class' => 'form-control', 'placeholder' => 'Isikan e-mail')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                        <div class="form-group">
                            <label>No. Telepon/HP</label>
                            <?php echo input_tag('telepon', $data_user->getTelepon(), array('class' => 'form-control', 'placeholder' => 'Isikan telepon')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label>Sub-Kegiatan yang diAmpu</label><br/>
                            <?php
                                $c = new Criteria();
                                if (substr($sf_user->getNamaLogin(), 0, 6) != 'admin_')
                                    $c->add(DinasMasterKegiatanPeer::USER_ID, $sf_user->getNamaLogin());
                                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $sf_user->getUnitId());
                                $rs_kegiatan = DinasMasterKegiatanPeer::doSelect($c);
                                foreach ($rs_kegiatan as $kegiatan) {
                                    $terpilih = '';
                                    if (($level_id == 13 && $kegiatan->getUserIdPptk() == $data_user->getUserId()) || ($level_id == 14 && $kegiatan->getUserIdKpa() == $data_user->getUserId())) {
                                        $terpilih = 'selected';
                                    }
                                    echo checkbox_tag('kegiatan[]', $kegiatan->getKodeKegiatan(), $terpilih) . ' ' . $kegiatan->getKodeKegiatan() . ' - [' . $kegiatan->getKegiatanId() . ']' . ' - ' . $kegiatan->getNamaKegiatan();
                                    echo ' (PPTK:' . label_for('danger', $kegiatan->getUserIdPptk()) . ', KPA:' . label_for('danger', $kegiatan->getUserIdKpa()) . ')';
                                    echo '<br>';
                                }
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-2">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                            <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                            <?php
                                echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                echo input_hidden_tag('unit_id', $sf_user->getUnitId());
                                echo input_hidden_tag('user_id', $data_user->getUserId());
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>
<?php } ?>
<script>
    $("#nip_br").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dinasadmin/pilihNama/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#user_name_baru').val(msg);
        });
        $.ajax
        ({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dinasadmin/pilihJabatan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) 
        {
            $('#jabatan_baru').val(msg);
        });
    });
</script>