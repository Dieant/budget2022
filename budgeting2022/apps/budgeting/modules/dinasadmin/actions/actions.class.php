<?php

/**
 * dinasadmin actions.
 *
 * @package    budgeting
 * @subpackage dinasadmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class dinasadminActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

    public function executePilihAtasan() {
        $this->role = $this->getRequestParameter('b');
        $this->user_id = $this->getRequestParameter('id');
    }

    public function executePilihNama() {
        $nip = $this->getRequestParameter('b');
        // $usr = $this->getRequestParameter('b');
        $c = new Criteria();
        $c->add(PegawaiPerformancePeer::NIP, $nip);
        $rs = PegawaiPerformancePeer::doSelectOne($c);
        $this->nama = $rs->getNama();
        // $c = new Criteria();
        // $c->add(MasterUserV2Peer::USER_ID, $usr);
        // $rs = MasterUserV2Peer::doSelectOne($c);
        // $this->nama = $rs->getUserName();
    }

    public function executePilihJabatan() {
        $nip = $this->getRequestParameter('b');
        // $usr = $this->getRequestParameter('b');

        $c = new Criteria();
        $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
        $c->add(PegawaiPerformancePeer::NIP, $nip);
        $rs = JabatanStrukturalPeer::doSelectOne($c);
        $this->jabatan = $rs->getNama();
        // $c = new Criteria();
        // $c->add(MasterUserV2Peer::USER_ID, $usr);
        // $rs = MasterUserV2Peer::doSelectOne($c);
        // $this->jabatan = $rs->getJabatan();
    }

    public function executeBuatuser() {
        
    }

    public function executeBuatuserBappeko() {
        
    }

    public function executeSaveuserbaru() {
        $unit_id = $this->getRequestParameter('unit_id');
        // $usr = $this->getRequestParameter('usr');
        $password = '5f4dcc3b5aa765d61d8327deb882cf99';
        $user_id = $this->getRequestParameter('user_id');
        $role = $this->getRequestParameter('role');
        $nip = $this->getRequestParameter('nip');
        //$jk = $this->getRequestParameter('jk');
        // echo $unit_id.'<br>';
        // echo $usr.'<br>';
        // echo $role.'<br>';
        $kegiatan = $this->getRequestParameter('kegiatan');
        if (!$email = $this->getRequestParameter('email')) {
            $email = null;
        }
        if (!$telepon = $this->getRequestParameter('telepon')) {
            $telepon = null;
        }

        $c1 = new Criteria();
        $c1->add(PegawaiPerformancePeer::NIP, $nip);
        $rs1 = PegawaiPerformancePeer::doSelectOne($c1);
        $user_name = $rs1->getNama();
        // $password = $rs1->getPassword();

        $jabatan = '';
        
        if (isset($rs1)){
            $c = new Criteria();
            $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
            $c->add(PegawaiPerformancePeer::NIP, $nip);
            if ($rs = JabatanStrukturalPeer::doSelectOne($c))
                $jabatan = $rs->getNama();
            if ($nip == '' && $user_id == '') {
                $this->setFlash('gagal', 'Parameter kurang');
                return $this->redirect('dinasadmin/buatuser');
            }
        }
        // echo $unit_id.'<br>';
        // echo $user_id.'<br>';
        // // echo $usr.'<br>';
        // echo $role.'<br>';
        // echo $nip.'<br>';
        // echo $user_name.'<br>';
        // echo $jabatan.'<br>';
        // // echo $password.'<br>';
        // die();
        // 5f4dcc3b5aa765d61d8327deb882cf99
        $con = Propel::getConnection();
        $con->begin();
        try {
            $dapat_user = new MasterUserV2();
            $dapat_user->setUserId($user_id);
            $dapat_user->setUserName($user_name);
            $dapat_user->setUserDefaultPassword($password);
            $dapat_user->setUserPassword($password);
            // password melihat di eperformance
            // $dapat_user->setUserDefaultPassword($password);
            // $dapat_user->setUserPassword($password);
            $dapat_user->setUserEnable(TRUE);
            $dapat_user->setNip($nip);
            $dapat_user->setJabatan($jabatan);
            $dapat_user->setEmail($email);
            $dapat_user->setTelepon($telepon);
            //$dapat_user->setJenisKelamin($jk);
            $dapat_user->save();

            $dapat_level = new SchemaAksesV2();
            $dapat_level->setUserId($user_id);
            $dapat_level->setSchemaId(2);
            $dapat_level->setLevelId($role);
            $dapat_level->save();

            $dapat_unit_id = new UserHandleV2();
            $dapat_unit_id->setUserId($user_id);
            $dapat_unit_id->setUnitId($unit_id);
            $dapat_unit_id->setSchemaId(2);
            $dapat_unit_id->save();

            foreach ($kegiatan as $kode_kegiatan) {
                $d = new Criteria();
                $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                if ($role == 13)
                    $rs_kegiatan->setUserIdPptk($user_id);
                else if ($role == 14)
                    $rs_kegiatan->setUserIdKpa($user_id);
                $rs_kegiatan->save();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Data User ' . $user_id . ' berhasil disimpan');
            return $this->redirect('dinasadmin/listuser');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena user sudah ada, silahkan melakukan edit pada user ' . $usr);
            return $this->redirect('dinasadmin/buatuser');
        }
    }

    public function executeSaveuserbarubappeko() {
        $nip = $this->getRequestParameter('nip');
        $user_id = 'b' . $nip;
        $skpd = $this->getRequestParameter('ambil_skpd');
        if (!$email = $this->getRequestParameter('email')) {
            $email = null;
        }
        if (!$telepon = $this->getRequestParameter('telepon')) {
            $telepon = null;
        }
        $c1 = new Criteria();
        $c1->add(PegawaiPerformancePeer::NIP, $nip);
        $rs1 = PegawaiPerformancePeer::doSelectOne($c1);
        $user_name = $rs1->getNama();
        $password = $rs1->getPassword();

        $c = new Criteria();
        $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
        $c->add(PegawaiPerformancePeer::NIP, $nip);
        $jabatan = '';
        if ($rs = JabatanStrukturalPeer::doSelectOne($c))
            $jabatan = $rs->getNama();
        if ($nip == '' && $user_id == '') {
            $this->setFlash('gagal', 'Parameter kurang');
            return $this->redirect('dinasadmin/buatuserBappeko');
        }
        $con = Propel::getConnection();
        $con->begin();
        try {
            $dapat_user = new MasterUserV2();
            $dapat_user->setUserId($user_id);
            $dapat_user->setUserName($user_name);
            $dapat_user->setUserDefaultPassword($password);
            $dapat_user->setUserPassword($password);
            $dapat_user->setUserEnable(TRUE);
            $dapat_user->setNip($nip);
            $dapat_user->setJabatan($jabatan);
            $dapat_user->setEmail($email);
            $dapat_user->setTelepon($telepon);
            $dapat_user->save();

            $dapat_level = new SchemaAksesV2();
            $dapat_level->setUserId($user_id);
            $dapat_level->setSchemaId(2);
            $dapat_level->setLevelId(16);
            $dapat_level->save();

            foreach ($skpd as $unit_id) {
                $dapat_unit_id = new UserHandleV2();
                $dapat_unit_id->setUserId($user_id);
                $dapat_unit_id->setUnitId($unit_id);
                $dapat_unit_id->setSchemaId(2);
                $dapat_unit_id->save();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Data User ' . $user_id . ' berhasil disimpan');
            return $this->redirect('dinasadmin/listuserBappeko');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena user sudah ada, silahkan melakukan edit pada user ' . $nip);
            return $this->redirect('dinasadmin/buatuserBappeko');
        }
    }

    public function executeUserlist() {
        $this->processFiltersuserlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/userlist/filters');

        $pagers = new sfPropelPager('MasterUserV2', 25);
        $c = new Criteria();
        $c->add(MasterUserV2Peer::USER_ENABLE, TRUE, Criteria::EQUAL);
        $c->addDescendingOrderByColumn(MasterUserV2Peer::USER_ENABLE);
        $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
        $c->setDistinct();
        $c->addJoin(MasterUserV2Peer::USER_ID, UserHandleV2Peer::USER_ID);
        $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);

        $crit0 = $c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 13);
        $crit0->addOr($c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 14));
        // $crit0->addOr($c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 15));
        $crit3 = $c->getNewCriterion(UserHandleV2Peer::UNIT_ID, $this->getUser()->getUnitId());
        $crit0->addAnd($crit3);
        if ($this->getUser()->getUnitId() == '0600') {
            $crit4 = $c->getNewCriterion(SchemaAksesV2Peer::LEVEL_ID, 16);
            $crit0->addOr($crit4);
        }
        $c->add($crit0);
        $this->addFiltersCriteriauserlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltersuserlist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/userlist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/userlist/filters');
        }
    }

    protected function addFiltersCriteriauserlist($c) {
        if ((isset($this->filters['userid']) && $this->filters['userid'] != '') || (isset($this->filters['username']) && $this->filters['username'] != '') || (isset($this->filters['level']) && $this->filters['level'] != '')) {

            if (isset($this->filters['userid']) && $this->filters['userid'] != '') {
                $kata = '%' . trim($this->filters['userid']) . '%';
                $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                if (isset($this->filters['level']) && $this->filters['level'] != '') {
                    $level_id = $this->filters['level'];
                    $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                    $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                    $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                }
            } else if (isset($this->filters['level']) && $this->filters['level'] != '') {
                $level_id = $this->filters['level'];
                $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                if (isset($this->filters['userid']) && $this->filters['userid'] != '') {
                    $kata = '%' . trim($this->filters['userid']) . '%';
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }
            }
            if (isset($this->filters['username']) && $this->filters['username'] != '') {
                $username = '%' . str_replace(" ", "%", trim($this->filters['username'])) . '%';
                $c->add(MasterUserV2Peer::USER_NAME, $username, Criteria::ILIKE);
            }
        }
    }

    public function executeEditUser() {
        $user_id = $this->getRequestParameter('id');

        $c_cek_user = new Criteria();
        $c_cek_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_cek_user);

        $c_cek_unit = new Criteria();
        $c_cek_unit->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_cek_unit->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $dapat_unit = UnitKerjaPeer::doSelect($c_cek_unit);

        $pagers = new sfPropelPager('UserHandleV2', 100);
        $c = new Criteria();
        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
        $c->addJoin(UserHandleV2Peer:: UNIT_ID, UnitKerjaPeer::UNIT_ID);
        $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();
        $this->pager = $pagers;


        if ($dapat_user) {
            $this->data_user = $dapat_user;

            $c_user_level = new Criteria();
            $c_user_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
            $dapat_user_level = SchemaAksesV2Peer::doSelectOne($c_user_level);
            if ($dapat_user_level) {
                $this->level_id = $level_id = $dapat_user_level->getLevelId();

                $array_level_butuh_dinas_banyak = array(2, 7, 8, 10, 12);
                $array_level_butuh_dinas_satu = array(1, 11);
                $array_level_tidak_butuh_dinas = array(3, 4, 9);

                if (in_array($level_id, $array_level_butuh_dinas_banyak)) {
                    $this->butuh_dinas = 2;
                } else if (in_array($level_id, $array_level_butuh_dinas_satu)) {
                    $this->butuh_dinas = 1;
                } else if (in_array($level_id, $array_level_tidak_butuh_dinas)) {
                    $this->butuh_dinas = 0;
                }

                if ($level_id == 1) {
                    $skpd = substr($user_id, -4);

                    $c_unit_kerja = new Criteria();
                    $c_unit_kerja->add(UnitKerjaPeer::UNIT_ID, $skpd);
                    $dapat_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit_kerja);
                    if ($dapat_unit_kerja) {
                        $this->unit = $dapat_unit_kerja;
                    } else {
                        $this->setFlash('gagal', 'Tidak ditemukan unit untuk user id ' . $user_id);
                        return $this->redirect('dinasadmin/userlist');
                    }
                }
            } else {
                $this->setFlash('gagal', 'Tidak ditemukan user akses dengan id ' . $user_id);
                return $this->redirect('dinasadmin/userlist');
            }
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user id ' . $user_id);
            return $this->redirect('dinasadmin/userlist');
        }
    }

    public function executeEditUserBappeko() {
        $user_id = $this->getRequestParameter('id');

        $c_cek_user = new Criteria();
        $c_cek_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_cek_user);

        $c_cek_unit = new Criteria();
        $c_cek_unit->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_cek_unit->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $dapat_unit = UnitKerjaPeer::doSelect($c_cek_unit);

        $pagers = new sfPropelPager('UserHandleV2', 100);
        $c = new Criteria();
        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
        $c->addJoin(UserHandleV2Peer:: UNIT_ID, UnitKerjaPeer::UNIT_ID);
        $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();
        $this->pager = $pagers;


        if ($dapat_user) {
            $this->data_user = $dapat_user;

            $c_user_level = new Criteria();
            $c_user_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
            $dapat_user_level = SchemaAksesV2Peer::doSelectOne($c_user_level);
            if ($dapat_user_level) {
                $this->level_id = $level_id = $dapat_user_level->getLevelId();
            } else {
                $this->setFlash('gagal', 'Tidak ditemukan user akses dengan id ' . $user_id);
                return $this->redirect('dinasadmin/userlist');
            }
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user id ' . $user_id);
            return $this->redirect('dinasadmin/userlist');
        }
    }

    public function executeHapusUser() {
        $this->processFiltersuserlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/userlist/filters');

        $pagers = new sfPropelPager('MasterUserV2', 25);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(MasterUserV2Peer::USER_ENABLE);
        $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
        $c->setDistinct();
        $this->addFiltersCriteriauserlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    function executeSaveedituser() {
        // echo 'sini';die();
        $unit_id = $this->getRequestParameter('unit_id');
        $user_id = $this->getRequestParameter('user_id');
        $user_name_baru = $this->getRequestParameter('user_name_baru');
        $jabatan_baru = $this->getRequestParameter('jabatan_baru');
        $nip = $this->getRequestParameter('nip_awal');

        $nip_baru = $this->getRequestParameter('nip_br');
        if ($nip_baru == '') {
            $nip_baru = $nip;
        }

        if($nip == '' && $nip_baru == '')
        {
            $nip_baru = '0';
        }
        
        // var_dump($nip_baru);die;


        
        $role = $this->getRequestParameter('role');
        //$jk = $this->getRequestParameter('jk');
        if (!$email = $this->getRequestParameter('email')) {
            $email = null;
        }
        if (!$telepon = $this->getRequestParameter('telepon')) {
            $telepon = null;
        }
        if ($role != 15) {
            $kegiatan = $this->getRequestParameter('kegiatan');
        }
        if ($nip == '' && $user_id == '') {
            $this->setFlash('gagal', 'Parameter kurang');
            return $this->redirect('dinasadmin/editUser');
        }

        $c = new Criteria();
        $c->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c);
        if ($dapat_user) {
            // echo $nip_baru;die();
            $nip = NULL;
            // $nip = $dapat_user->getNip();
            if(!is_null($nip_baru) || $nip_baru != ''){
                $nip = $nip_baru;
            }

           // die($nip.'-'.$nip_baru);
            if(!is_null($nip) && $nip !='0') {

                $c9 = new Criteria();
                $c9->add(PegawaiPerformancePeer::NIP, $nip_baru);
                if($rs1 = PegawaiPerformancePeer::doSelectOne($c9)){
                    $user_name = $rs1->getNama();
                }else{
                    $user_name = $user_id;
                }
                //yogie
                // echo $user_name;die();

                $c = new Criteria();
                $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
                $c->add(PegawaiPerformancePeer::NIP, $nip_baru);
                $jabatan = '';
                if ($rs = JabatanStrukturalPeer::doSelectOne($c)){
                    $jabatan = $rs->getNama();
                }
                    
                // echo $jabatan;die();

                
                $dapat_user->setUserName($user_name);
                $dapat_user->setNip($nip_baru);
                $dapat_user->setJabatan($jabatan);
                //$dapat_user->setJenisKelamin($jk);
                $dapat_user->setEmail($email);
                $dapat_user->setTelepon($telepon);
                $dapat_user->save();

                $c_level = new Criteria();
                $c_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
                $c_level->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                //$c_level->addDescendingOrderByColumn(SchemaAksesV2Peer::SCHEMA_ID);
                $dapat_level = SchemaAksesV2Peer::doSelectOne($c_level);
                $dapat_level->setLevelId($role);
                $dapat_level->save();

                if ($role != 15) {
                    //die($user_id);
                    $kode_kegiatan_arr = array();
                    foreach ($kegiatan as $kode_kegiatan) {
                        array_push($kode_kegiatan_arr, $kode_kegiatan);
                    }

                    $c = new Criteria();
                    if (substr($this->getUser()->getNamaLogin(), 0, 6) != 'admin_')
                        $c->add(DinasMasterKegiatanPeer::USER_ID, $this->getUser()->getNamaLogin());
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $this->getUser()->getUnitId());
                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelect($c);
                    foreach($rs_kegiatan as $kode_arr){
                        if(in_array($kode_arr->getKodeKegiatan(),$kode_kegiatan_arr)){
                            $d = new Criteria();
                            $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_arr->getKodeKegiatan());
                            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                            if ($role == 1)
                                $rs_kegiatan->setUserId($user_id);
                            else if ($role == 13)
                                $rs_kegiatan->setUserIdPptk($user_id);
                            else if ($role == 14)
                                $rs_kegiatan->setUserIdKpa($user_id);
                            $rs_kegiatan->save();
                        }else{
                            $d = new Criteria();
                            $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_arr->getKodeKegiatan());
                            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                            if ($role == 1){
                                $rs_kegiatan->setUserId(null);
                            }
                            else if ($role == 13){
                                if($rs_kegiatan->getUserIdPptk() == $user_id){
                                    $rs_kegiatan->setUserIdPptk(null);
                                }
                            }
                            else if ($role == 14){
                                $rs_kegiatan->setUserIdKpa(null);
                            }
                            $rs_kegiatan->save();
                        }
                    }


                    // var_dump($kode_kegiatan_arr);die;
                    // foreach ($kegiatan as $kode_kegiatan) {
                    //     $d = new Criteria();
                    //     $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    //     $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    //     $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                    //     if ($role == 1)
                    //         $rs_kegiatan->setUserId($user_id);
                    //     else if ($role == 13)
                    //         $rs_kegiatan->setUserIdPptk($user_id);
                    //     else if ($role == 14)
                    //         $rs_kegiatan->setUserIdKpa($user_id);
                    //     $rs_kegiatan->save();
                    // }
                }
            } //tutupnya null nip
            
            else{
                // tidak ada nip, user approval
                if ($role != 15) {
                   // die($user_id);

                    $kode_kegiatan_arr = array();
                    foreach ($kegiatan as $kode_kegiatan) {
                        array_push($kode_kegiatan_arr, $kode_kegiatan);
                    }

                    $c = new Criteria();
                    if (substr($this->getUser()->getNamaLogin(), 0, 6) != 'admin_')
                        $c->add(DinasMasterKegiatanPeer::USER_ID, $this->getUser()->getNamaLogin());
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $this->getUser()->getUnitId());
                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelect($c);
                    foreach($rs_kegiatan as $kode_arr){
                        if(in_array($kode_arr->getKodeKegiatan(),$kode_kegiatan_arr)){
                            $d = new Criteria();
                            $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_arr->getKodeKegiatan());
                            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                            if ($role == 1)
                                $rs_kegiatan->setUserId($user_id);
                            else if ($role == 13)
                                $rs_kegiatan->setUserIdPptk($user_id);
                            else if ($role == 14)
                                $rs_kegiatan->setUserIdKpa($user_id);
                            $rs_kegiatan->save();
                        }else{
                            $d = new Criteria();
                            $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                            $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_arr->getKodeKegiatan());
                            $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                            if ($role == 1)
                                $rs_kegiatan->setUserId(null);
                            else if ($role == 13)
                                if($rs_kegiatan->getUserIdPptk() == $user_id){
                                    $rs_kegiatan->setUserIdPptk(null);
                                }
                            else if ($role == 14)
                                $rs_kegiatan->setUserIdKpa(null);
                            $rs_kegiatan->save();
                        }
                    }

                    // foreach ($kegiatan as $kode_kegiatan) {
                    //     $d = new Criteria();
                    //     $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    //     $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    //     $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                    //     // if ($role == 1)
                    //     //     $rs_kegiatan->setUserId($user_id);
                    //     if ($role == 13)
                    //         $rs_kegiatan->setUserIdPptk($user_id);
                    //     else if ($role == 14)
                    //         $rs_kegiatan->setUserIdKpa($user_id);
                    //     $rs_kegiatan->save();
                    // }
                }

            }

            $c_user_level = new Criteria();
            $c_user_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
            $dapat_user_level = SchemaAksesV2Peer::doSelectOne($c_user_level);
            if ($dapat_user_level) {
                $dapat_user_level->setLevelId($role);
                $dapat_user_level->save();
            }

            $this->setFlash('berhasil', 'Data user ' . $user_id . ' berhasil diubah');
            return $this->redirect('dinasadmin/editUser?id=' . $user_id);
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
            return $this->redirect('dinasadmin/editUser?id=' . $user_id);
        }
    }

    function executeSaveedituserbappeko() {
        $user_id = $this->getRequestParameter('user_id');
        $nip = $this->getRequestParameter('nip');
        $ambil = $this->getRequestParameter('ambil_skpd');
        $lepas = $this->getRequestParameter('lepas_skpd');
        if (!$email = $this->getRequestParameter('email')) {
            $email = null;
        }
        if (!$telepon = $this->getRequestParameter('telepon')) {
            $telepon = null;
        }
        if ($nip == '' && $user_id == '') {
            $this->setFlash('gagal', 'Parameter kurang');
            return $this->redirect('dinasadmin/editUserBappeko');
        }

        $c1 = new Criteria();
        $c1->add(PegawaiPerformancePeer::NIP, $nip);
        $rs1 = PegawaiPerformancePeer::doSelectOne($c1);
        $user_name = $rs1->getNama();

        $c = new Criteria();
        $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
        $c->add(PegawaiPerformancePeer::NIP, $nip);
        $jabatan = '';
        if ($rs = JabatanStrukturalPeer::doSelectOne($c))
            $jabatan = $rs->getNama();

        $con = Propel::getConnection();
        $con->begin();
        $c = new Criteria();
        $c->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c);
        if ($dapat_user) {
            $dapat_user->setUserName($user_name);
            $dapat_user->setNip($nip);
            $dapat_user->setJabatan($jabatan);
            $dapat_user->setEmail($email);
            $dapat_user->setTelepon($telepon);
            $dapat_user->save();

            foreach ($ambil as $unit_id) {
                $dapat_unit_id = new UserHandleV2();
                $dapat_unit_id->setUserId($user_id);
                $dapat_unit_id->setUnitId($unit_id);
                $dapat_unit_id->setSchemaId(2);
                $dapat_unit_id->save();
            }

            foreach ($lepas as $unit_id) {
                $c = new Criteria();
                $c->add(UserHandleV2Peer::USER_ID, $user_id);
                $c->add(UserHandleV2Peer::UNIT_ID, $unit_id);
                $c->add(UserHandleV2Peer::SCHEMA_ID, 2);
                $cek_lepas = UserHandleV2Peer::doCount($c);
                if ($cek_lepas == 1) {
                    UserHandleV2Peer::doDelete($c);
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Data user ' . $user_id . ' berhasil diubah');
            return $this->redirect('dinasadmin/editUserBappeko?id=' . $user_id);
        } else {
            $con->rollback();
            $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
            return $this->redirect('dinasadmin/editUserBappeko?id=' . $user_id);
        }
    }

    function executeSaveresetpassworduser() {
        $user_id = $this->getRequestParameter('user_id');

        $pass_baru = $this->getRequestParameter('pass_baru');
        $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');

        if ($pass_baru <> $ulang_pass_baru) {
            $this->setFlash('gagal', 'Isian password dan isian ulang password tidak sama');
            return $this->redirect('dinasadmin/editUser?id=' . $user_id);
        } else {
            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
            $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
            if ($dapat_user) {
                $dapat_user->setUserPassword(md5($pass_baru));
                $dapat_user->save();

                $this->setFlash('berhasil', 'Password user ' . $user_id . ' berhasil diubah');
                return $this->redirect('dinasadmin/editUser?id=' . $user_id);
            } else {
                $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
                return $this->redirect('dinasadmin/editUser?id=' . $user_id);
            }
        }
    }

    function executeKunciUser() {
        $user_id = $this->getRequestParameter('id');

        $c_user = new Criteria();
        $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
        if ($dapat_user) {
            $dapat_user->setUserEnable(false);
            $dapat_user->save();

            $this->setFlash('berhasil', 'User ' . $user_id . ' berhasil dikunci');
            return $this->redirect('dinasadmin/userlist');
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
            return $this->redirect('dinasadmin/userlist');
        }
    }

    function executeBukaUser() {
        $user_id = $this->getRequestParameter('id');

        $c_user = new Criteria();
        $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
        if ($dapat_user) {
            $dapat_user->setUserEnable(true);
            $dapat_user->save();

            $this->setFlash('berhasil', 'User ' . $user_id . ' berhasil dibuka');
            return $this->redirect('dinasadmin/userlist');
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
            return $this->redirect('dinasadmin/userlist');
        }
    }

    function executeSaveambillepasskpd() {
        $ambil_skpd = $this->getRequestParameter('ambil_skpd');
        $lepas_skpd = $this->getRequestParameter('lepas_skpd');
        $user_id = $this->getRequestParameter('user_id');
        $con = Propel::getConnection();
        $con->begin();
        try {
            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
            $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
            if ($dapat_user) {
                if (isset($ambil_skpd)) {
                    foreach ($ambil_skpd as $value_ambil) {
                        $c = new Criteria();
                        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
                        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
                        $c->addAnd(UserHandleV2Peer:: UNIT_ID, $value_ambil);
                        $cek_ambil = UserHandleV2Peer::doCount($c);

                        if ($cek_ambil == 0) {
                            $baru_ambil_skpd = new UserHandleV2();
                            $baru_ambil_skpd->setSchemaId(2);
                            $baru_ambil_skpd->setUnitId($value_ambil);
                            $baru_ambil_skpd->setUserId($user_id);
                            $baru_ambil_skpd->save();
                        }
                    }
                }

                if (isset($lepas_skpd)) {
                    foreach ($lepas_skpd as $value_lepas) {
                        $c = new Criteria();
                        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
                        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
                        $c->addAnd(UserHandleV2Peer:: UNIT_ID, $value_lepas);
                        $cek_lepas = UserHandleV2Peer::doCount($c);

                        if ($cek_lepas == 1) {
                            $eksekusi_hapus_lepas = UserHandleV2Peer::doDelete($c);
                        }
                    }
                }
                $con->commit();
                $this->setFlash('berhasil', 'User ' . $user_id . ' berhasil merubah User Handle SKPD');
                return $this->redirect('dinasadmin/editUser?id=' . $user_id);
            } else {
                $con->rollback();
                $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
                return $this->redirect('dinasadmin/editUser?id=' . $user_id);
            }
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getTraceAsString());
            return $this->redirect('dinasadmin/editUser?id=' . $user_id);
        }
    }

}
