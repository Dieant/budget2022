<?php

/**
 * masalah actions.
 *
 * @package    budgeting
 * @subpackage masalah
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class masalahActions extends automasalahActions
{
	public function executeHistory()
	{
		
		$nama_lokasi = base64_decode($this->getRequestParameter('detail_name'));
		$unit_id = $unit_id = $this->getRequestParameter('unit_id');
		$c = new Criteria();
		$c->add(VLokasiPeer::NAMA, $nama_lokasi, Criteria::ILIKE);
		$v_lokasi = VLokasiPeer::doSelectOne($c);
		if($v_lokasi)
		{
			$kode_lokasi = $v_lokasi->getKode();
		}
		//print_r($kode_lokasi);exit;
		if($kode_lokasi)
		{
		$m_kode_lokasi = '%'.$kode_lokasi.'%';
		
		$c = new Criteria();
		$c->add(HistoryPekerjaanPeer::KODE, strtr($m_kode_lokasi,'*','%'), Criteria::ILIKE);
		$c->addAscendingOrderByColumn(HistoryPekerjaanPeer::TAHUN);
		$n_history_pekerjaan = HistoryPekerjaanPeer::doCount($c);
		
		$this->n_history_pekerjaan = $n_history_pekerjaan;		
		if($n_history_pekerjaan >1)
		{
			$this->history_pekerjaan = HistoryPekerjaanPeer::doSelect($c);
		}
		elseif($n_history_pekerjaan ==1)
		{
			$this->history_pekerjaan = HistoryPekerjaanPeer::doSelectOne($c);
		}
		
		$d = new Criteria();
		$d->add(RincianDetailPeer::DETAIL_NAME, $nama_lokasi, Criteria::ILIKE);
		$d->add(RincianDetailPeer::UNIT_ID, $unit_id, Criteria::ILIKE);
		$n_rincian_detail = RincianDetailPeer::doCount($d);
		//print_r($n_rincian_detail);exit;
		if($n_rincian_detail >1)
		{
			$this->rincian_detail = RincianDetailPeer::doSelect($d);
		}
		elseif($n_rincian_detail==1)
		{
			$this->rincian_detail = RincianDetailPeer::doSelectOne($d);
		}
		$this->n_rincian_detail = $n_rincian_detail;
		}
	}
	
	public function executeSetuju()
	{
		$unit_id = $this->getRequestParameter('unit_id');
		$kegiatan_code = $this->getRequestParameter('kegiatan_code');
		$detail_no_masalah = $this->getRequestParameter('detail_no');
		
		$detail_no=0;
		$query = "select max(detail_no) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail where kegiatan_code='$kegiatan_code' and unit_id='$unit_id'";
		//print_r($query);exit;
		$con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
		$stmt=$con->prepareStatement($query);
		$rs=$stmt->executeQuery();
		while($rs->next())
		{
			$detail_no = $rs->getString('nilai');
		}
		$detail_no+=2;
		
		$query = "select * from ". sfConfig::get('app_default_schema') .".rincian_detail_masalah where kegiatan_code='$kegiatan_code' and unit_id='$unit_id' and detail_no!=$detail_no_masalah order by detail_no";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$rs_masalah = $stmt->executeQuery();
		$i = $detail_no + 10;
 		while($rs_masalah->next())
		{
			if($i==$detail_no_masalah)
			{
				$i = $i + 20;
			}
			if($i==$detail_no)
			{
				$i = $i + 20;
			}
			$detail_no_sementara = $rs_masalah->getString('detail_no');
			if($i==$detail_no_sementara)
			{
				$query = "select max(detail_no) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail_masalah where kegiatan_code='$kegiatan_code' and unit_id='$unit_id'";
				//print_r($query);exit;
				$con=Propel::getConnection(RincianDetailMasalahPeer::DATABASE_NAME);
				$stmt=$con->prepareStatement($query);
				$rs=$stmt->executeQuery();
				while($rs->next())
				{
					$detail_no_sama = $rs->getString('nilai');
				}
				$i = $detail_no_sama + 3;

			}
			$komponen_id = $rs_masalah->getString('komponen_id');
			$rekening_code = $rs_masalah->getString('rekening_code');
			$detail_name = $rs_masalah->getString('detail_name');
			$query ="update ". sfConfig::get('app_default_schema') .".rincian_detail_masalah set detail_no=$i where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no_sementara and komponen_id='$komponen_id' and rekening_code='$rekening_code' and detail_name='$detail_name'";
			$con=Propel::getConnection();
			$stmt=$con->prepareStatement($query);
			$stmt->executeQuery();
			$i+=1;

		}
		
		$query ="update ". sfConfig::get('app_default_schema') .".rincian_detail_masalah set detail_no=$detail_no where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no_masalah";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$stmt->executeQuery();
		
		$query ="insert into ". sfConfig::get('app_default_schema') .".rincian_detail select * from ". sfConfig::get('app_default_schema') .".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$stmt->executeQuery();
		
		$query ="delete from ". sfConfig::get('app_default_schema') .".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$stmt->executeQuery();
		$this->setFlash('pindah_simpan', 'Komponen Telah Berhasil Disimpan ke RKA');
		return $this->forward('masalah','list');
		//if($this->getRequestparameter('page'))
		//{
		//	$page = $this->getRequestparameter('page');
		//	return $this->redirect('masalah/list?page='.$page);

	//	}
		//return $this->redirect('masalah/list');
	}
	
	public function executeHapus()
	{
		$unit_id = $this->getRequestParameter('unit_id');
		$kegiatan_code = $this->getRequestParameter('kegiatan_code');
		$detail_no = $this->getRequestParameter('detail_no');
		
		$query ="delete from ". sfConfig::get('app_default_schema') .".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
		$con=Propel::getConnection();
		$stmt=$con->prepareStatement($query);
		$stmt->executeQuery();
		$this->setFlash('pindah_simpan', 'Komponen Telah Berhasil Dihapus');
		if($this->getRequestparameter('page'))
		{
			$page = $this->getRequestparameter('page');
			return $this->redirect('masalah/list?page='.$page);

		}
		return $this->forward('masalah','list');
		//return $this->redirect('masalah/list');
	}
	protected function addFiltersCriteria($c)
	{
		if (isset($this->filters['satuankerja_is_empty']))
		{
		  $criterion = $c->getNewCriterion(RincianDetailMasalahPeer::UNIT_ID, '');
		  $criterion->addOr($c->getNewCriterion(RincianDetailMasalahPeer::UNIT_ID, null, Criteria::ISNULL));
		  $c->add($criterion);
		}
		else if (isset($this->filters['satuankerja']) && $this->filters['satuankerja'] !== '')
		{
		  $c->add(RincianDetailMasalahPeer::UNIT_ID, $this->filters['satuankerja']);
		}
		if (isset($this->filters['kegiatan_code_is_empty']))
		{
		  $criterion = $c->getNewCriterion(RincianDetailMasalahPeer::KEGIATAN_CODE, '');
		  $criterion->addOr($c->getNewCriterion(RincianDetailMasalahPeer::KEGIATAN_CODE, null, Criteria::ISNULL));
		  $c->add($criterion);
		}
		else if (isset($this->filters['kegiatan_code']) && $this->filters['kegiatan_code'] !== '')
		{
		  $c->add(RincianDetailMasalahPeer::KEGIATAN_CODE, strtr($this->filters['kegiatan_code'], '*', '%'), Criteria::LIKE);
		}
		if (isset($this->filters['komponen_name_is_empty']))
		{
		  $criterion = $c->getNewCriterion(RincianDetailMasalahPeer::KOMPONEN_NAME, '');
		  $criterion->addOr($c->getNewCriterion(RincianDetailMasalahPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
		  $c->add($criterion);
		}
		else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '')
		{
		  $nama_komponen = '%'.$this->filters['komponen_name'].'%';
		  $c->add(RincianDetailMasalahPeer::KOMPONEN_NAME, strtr($this->filters['komponen_name'], '*', '%'), Criteria::ILIKE);
		}
		if (isset($this->filters['detail_name_is_empty']))
		{
		  $criterion = $c->getNewCriterion(RincianDetailMasalahPeer::DETAIL_NAME, '');
		  $criterion->addOr($c->getNewCriterion(RincianDetailMasalahPeer::DETAIL_NAME, null, Criteria::ISNULL));
		  $c->add($criterion);
		}
		else if (isset($this->filters['detail_name']) && $this->filters['detail_name'] !== '')
		{
		  $lokasi = '%'.$this->filters['detail_name'].'%';
		  $c->add(RincianDetailMasalahPeer::DETAIL_NAME, strtr($lokasi, '*', '%'), Criteria::ILIKE);
		}
	}
}
