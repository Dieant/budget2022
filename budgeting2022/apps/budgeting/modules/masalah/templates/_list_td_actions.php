<?php use_helper('Object', 'Javascript', 'Number', 'Validation') ?>
<td>
<ul class="sf_admin_td_actions">

<?php
	$nama_lokasi='';
	$detail_name='';
	$nama_lokasi = $rincian_detail_masalah->getDetailName();
	if($sf_params->get('page'))
	{	
		$page = $sf_params->get('page');
	}
	if($nama_lokasi=='')
	{
		
		?>
		<li><?php echo link_to(image_tag('/sf/sf_admin/images/cancel.png', array('alt' => __('Hapus Usulan Ini'), 'title' => __('Hapus Usulan Ini'))), 'masalah/hapus?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$rincian_detail_masalah->getDetailName().'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
		<?php

		
	}
	else
	{
		$detail_name = $rincian_detail_masalah->getDetailName();
		$kode_lokasi='';
		$jumlah_ada = 0;
		$c = new Criteria();
		$c->add(VLokasiPeer::NAMA, $detail_name);
		$v_lokasi = VLokasiPeer::doSelectOne($c);
		if($v_lokasi)
		{
			$kode_lokasi = $v_lokasi->getKode();
			
		}
		//print_r($kode_lokasi);
		if($kode_lokasi!='')
		{
				$m_kode_lokasi = '%'.$kode_lokasi.'%';
				$c = new Criteria();
				$c->add(HistoryPekerjaanPeer::KODE, strtr($m_kode_lokasi,'*','%'), Criteria::ILIKE);
				$n_history_pekerjaan = HistoryPekerjaanPeer::doCount($c);
				if($n_history_pekerjaan >0)
				{
					$jumlah_ada+=1;
				}
			
			
				$unit_id = $rincian_detail_masalah->getUnitId();
				$kegiatan_code = $rincian_detail_masalah->getKegiatanCode();
				$query = "select (1) as ada from ". sfConfig::get('app_default_schema') .".rincian_detail where detail_name = '$detail_name' and unit_id!='$unit_id' and kegiatan_code!='$kegiatan_code'";
				
				$con = Propel::getConnection();
				$stmt = $con->prepareStatement($query);
				$rs1 = $stmt->executeQuery();
				while($rs1->next())
				{
					if($rs1->getString('ada'))
					{
						$jumlah_ada+=1;
					}
				}
				$detail_name = base64_encode($detail_name);
			if($jumlah_ada!=0)
			{
				?>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/add.png', array('alt' => __('Menyetujui Usulan Ini'), 'title' => __('Menyetujui Usulan Ini'))), 'masalah/setuju?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/cancel.png', array('alt' => __('Hapus Usulan Ini'), 'title' => __('Hapus Usulan Ini'))), 'masalah/hapus?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/help.png', array('alt' => __('History Pekerjaan Ini'), 'title' => __('History Pekerjaan Ini'))), 'masalah/history?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.addslashes($detail_name).'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
		<!--		 <li><?php echo link_to(image_tag('/sf/sf_admin/images/edit.png', array('alt' => __('Mengubah Usulan Ini'), 'title' => __('Mengubah Usulan Ini'))), 'masalah/editusulan?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li> -->
				<?php
			}
			else
			{
				?>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/add.png', array('alt' => __('Menyetujui Usulan Ini'), 'title' => __('Menyetujui Usulan Ini'))), 'masalah/setuju?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/cancel.png', array('alt' => __('Hapus Usulan Ini'), 'title' => __('Hapus Usulan Ini'))), 'masalah/hapus?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
				<?php
			}
		}
		else
		{
			$detail_name = base64_encode($detail_name);
			?>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/add.png', array('alt' => __('Menyetujui Usulan Ini'), 'title' => __('Menyetujui Usulan Ini'))), 'masalah/setuju?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
				<li><?php echo link_to(image_tag('/sf/sf_admin/images/cancel.png', array('alt' => __('Hapus Usulan Ini'), 'title' => __('Hapus Usulan Ini'))), 'masalah/hapus?kegiatan_code='.$rincian_detail_masalah->getKegiatanCode().'&detail_no='.$rincian_detail_masalah->getDetailNo().'&komponen_id='.$rincian_detail_masalah->getKomponenId().'&detail_name='.$detail_name.'&unit_id='.$rincian_detail_masalah->getUnitId()) ?></li>
				<?php
		}
	}
?>

  
  
  
 
</ul>
</td>
