<?php use_helper('Object', 'Javascript', 'Number', 'Validation') ?>
<?php

	$kode_kegiatan = $rincian_detail_masalah->getKegiatanCode();
	$unit_id = $rincian_detail_masalah->getUnitId();
	$c = new Criteria();
	$c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
	$c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
	$master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
	if($master_kegiatan)
	{
		echo '('.$kode_kegiatan.') '.$master_kegiatan->getNamaKegiatan();
	}
?>