<?php use_helper('Object', 'Javascript', 'Number', 'Validation') ?>
<?php

	$nama_lokasi='';
	$nama_lokasi = $rincian_detail_masalah->getDetailName();
	$unit_id = $rincian_detail_masalah->getUnitId();
	$kegiatan_code = $rincian_detail_masalah->getKegiatanCode();
	
	if($nama_lokasi=='')
	{
		echo image_tag('/sf/sf_web_debug/images/error.png');
	}
	else
	{
		$kode_lokasi='';
		$jumlah_ada = 0;
		$c = new Criteria();
		$c->add(VLokasiPeer::NAMA, $nama_lokasi);
		$v_lokasi = VLokasiPeer::doSelectOne($c);
		if($v_lokasi)
		{
			$kode_lokasi = $v_lokasi->getKode();
			
		}
		if($kode_lokasi!='')
		{
			$m_kode_lokasi = '%'.$kode_lokasi.'%';
			$c = new Criteria();
			$c->add(HistoryPekerjaanPeer::LOKASI, strtr($m_kode_lokasi,'*','%'), Criteria::ILIKE);
			$n_history_pekerjaan = HistoryPekerjaanPeer::doCount($c);
			
			if($n_history_pekerjaan >0)
			{
				$jumlah_ada+=1;
			}
			
			$query = "select (1) as ada from ". sfConfig::get('app_default_schema') .".rincian_detail where detail_name = '$nama_lokasi' and unit_id!='$unit_id' and kegiatan_code!='$kegiatan_code'";
			$con = Propel::getConnection();
			$stmt = $con->prepareStatement($query);
			$rs1 = $stmt->executeQuery();
			while($rs1->next())
			{
				if($rs1->getString('ada'))
				{
					$jumlah_ada+=1;
				}
			}
							
			if($jumlah_ada!=0)
			{
				echo image_tag('/sf/sf_web_debug/images/warning.png');
			}
			else
			{
				echo image_tag('/sf/sf_web_debug/images/info.png');
			}
		}
		else
		{
			echo image_tag('/sf/sf_web_debug/images/error.png');
		}
	}