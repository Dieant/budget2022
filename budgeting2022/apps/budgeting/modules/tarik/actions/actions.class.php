<?php

/**
 * tarik actions.
 *
 * @package    budgeting
 * @subpackage tarik
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class tarikActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executePerlengkapan()
  {
      
        //echo 'test'.$this->getRequestParameter('unitId');exit;
        //$this->setLayout(false);  
        //$this->getResponse()->setContentType('text/xml'); 
        
        $c_unit_id= new Criteria();
        $c_unit_id->add(UnitKerjaPeer::UNIT_ID,$this->getRequestParameter('unitId'));
        //$c_unit_id->setLimit(40);
        $r_unit_id= UnitKerjaPeer::doSelect($c_unit_id);
        //$this->unit_id=$r_unit_id;
        
       //$this->getResponse()->setContentType('text/xml');

      $output = '<?xml version="1.0" encoding="UTF-8"?>';
      $output .= "\n".'<perlengkapan>';
      $array_keg =array();
      $array_unit_id=array();
      $array_rincian=array();
      foreach($r_unit_id as $unit_id) {
          //print_r($unit_id->getUnitName());exit;
          
          array_push($array_unit_id, array($unit_id->getUnitId(),$unit_id->getUnitName()));
          
          $c_kegiatan= new Criteria();
          $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID,  $this->getRequestParameter('unitId'));
          //$c_kegiatan->add(MasterKegiatanPeer::)
          $r_kegiatan=  MasterKegiatanPeer::doSelect($c_kegiatan);
          foreach($r_kegiatan as $kegiatan) {
              //echo $kegiatan->getKodeKegiatan();
              array_push($array_keg, array($kegiatan->getKodeKegiatan(),$kegiatan->getNamaKegiatan()));
              $c_subtitle = new Criteria();
              $c_subtitle->add(SubtitleIndikatorPeer::UNIT_ID,$this->getRequestParameter('unitId'));
              $c_subtitle->add(SubtitleIndikatorPeer::KEGIATAN_CODE,$kegiatan->getKodeKegiatan());
              $r_subtitle= SubtitleIndikatorPeer::doSelect($c_subtitle);
              
              foreach($r_subtitle as $subtitle) {
                  
                  $c_rincian= new Criteria();
                  $c_rincian->addJoin(RincianDetailPeer::REKENING_CODE, RekeningPeer::REKENING_CODE);
                  $c_rincian->add(RincianDetailPeer::UNIT_ID,$this->getRequestParameter('unitId'));
                  $c_rincian->add(RincianDetailPeer::KEGIATAN_CODE,$kegiatan->getKodeKegiatan());
                  $c_rincian->add(RincianDetailPeer::SUBTITLE,$subtitle->getSubtitle());
                  $c_rincian->add(RincianDetailPeer::STATUS_HAPUS,'FALSE',  Criteria::EQUAL);
                  $c_rincian->addAscendingOrderByColumn(RincianDetailPeer::DETAIL_NO);
                  $r_rincian= RincianDetailPeer::doSelect($c_rincian);
                  
                  foreach($r_rincian as $rincian) {
                    array_push($array_rincian, array($subtitle->getSubtitle(),$rincian->getDetailNo()));
                  }
              
              }
              array_push($array_keg, $array_rincian);
          }
          array_push($array_unit_id, $array_keg);
          // $output .= "\n".'</kegiatan>';
           $output .= "\n".'</unit_kerja>';
      }
     $show=$array_unit_id;
     // $output .= "\n".'</perlengkapan>';
      
      
      var_dump($show);exit;
      echo $output;
      //print_r($output);
      return sfView::NONE;
      
    
  }
  public function executeHspk()
  { //echo 'jet';exit;
      if(sfConfig::get('app_tarik_hspk')=='buka'){
        $c_hspk= new Criteria();
        //kalau mau menampilkan HSPK
        $c_hspk->add(KomponenPeer::KOMPONEN_TIPE,'HSPK',  Criteria::EQUAL);
        //kalau mau menampilkan ASB
        //    $c_hspk->add(KomponenPeer::KOMPONEN_TIPE,'FISIK',  Criteria::EQUAL);
        //$c_hspk->setLimit(30);
        $this->komponens=$r_hspk= KomponenPeer::doSelect($c_hspk);
     
        $this->setLayout(false);
        $this->getResponse()->setContentType('text/xml');
      }else{
          $this->redirect('@403');	
      }
  }
  public function execute403()
  {
      $this->setLayout('layout');      
  }
  public function executeIndex()
  {
   
    //$this->forward('default', 'module');
  }
}