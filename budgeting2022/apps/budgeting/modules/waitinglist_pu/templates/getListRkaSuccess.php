<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$i = 0;
?>
<tr class="keg_<?php echo $id ?>" style="text-align: center; font-weight: bold">
    <th>Subtitle</th>
    <th>Komponen</th>
    <th>Rekening</th>
    <th>Lokasi</th>
    <th>Anggaran</th>
    <th>Action</th>
</tr>
<?php
foreach ($rs_waiting as $waiting) {
    $con = Propel::getConnection();
    ?>
    <tr class="keg_<?php echo $id ?>" style="text-align: center;">
        <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php echo $waiting->getSubtitle() ?></td>
        <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php
        echo $waiting->getKomponenName();
        if ($waiting->getIsMusrenbang() == TRUE) {
            ?>
            <span class="label label-success">Musrenbang</span>
            <?php
        }
        ?>

    </td>
    <td><?php echo $waiting->getRekeningCode() ?></td>
    <td><?php echo $waiting->getDetailName() ?></td>
    <td style="text-align: right;"><?php echo number_format($waiting->getNilaiAnggaran()) ?></td>
    <td>
        <?php if ($sf_user->getNamaLogin() == 'pawang' || $sf_user->getNamaLogin() == 'perancangan_2600' || $sf_user->getNamaLogin() == 'permukiman_2300' || $sf_user->getNamaLogin() == 'admin' || $sf_user->getNamaLogin() == 'gis_2600' || $sf_user->getNamaLogin() == 'sarpras_2800') { ?>
            <div class="btn-group">
                <?php
                ?>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li>
                        <?php
                        $query_ada_gis = "select * "
                        . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                        . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
                        . "and unit_id = '" . $waiting->getUnitId() . "' "
                        . "and detail_no = '" . $waiting->getDetailNo() . "'";
                        $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
                        $rs_ada_gis = $stmt_ada_gis->executeQuery();
                        while ($rs_ada_gis->next()) {
                            $mlokasi = $rs_ada_gis->getString('mlokasi');
                            $id_kelompok = $rs_ada_gis->getString('id_kelompok');
                        }

                        $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                        where unit_id='" . $waiting->getUnitId() . "' and kegiatan_code='" . $waiting->getKegiatanCode() . "' and detail_no ='" . $waiting->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $total_lokasi = $rs->getString('total_lokasi');
                        }
                        echo link_to('<i class="fa fa-pencil"></i> Ubah Marker Lokasi', sfConfig::get('app_path_gmap') . 'updateData.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&detail_no=' . $waiting->getDetailNo() . '&satuan=' . $waiting->getSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahun() . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1&total_lokasi=' . $total_lokasi, array('class' => 'btn btn-default'));
                        $unit_id = str_replace('XXX', '', $waiting->getUnitId());
                        echo link_to('<i class="fa fa-trash"></i> Kembalikan ke Waiting List', 'waitinglist_pu/proseskembalikan?id=' . $waiting->getDetailNo() . '&unitid=' . $unit_id . '&kodekegiatan=' . $waiting->getKegiatanCode() . '&key=' . md5('kembali_waiting'), Array('confirm' => 'Yakin untuk mengembalikan ke Waiting List untuk ' . $waiting->getKomponenName() . ' ' . $waiting->getDetailName() . ' ?', 'class' => 'btn btn-default'));
                        ?>
                    </li>
                </ul>
            </div>
            <?php
        } else if ($sf_user->getNamaLogin() == 'jalan_2600' || $sf_user->getNamaLogin() == 'pematusan_2600' || $sf_user->getNamaLogin() == 'permukiman_2300' || $sf_user->getNamaLogin() == 'sarpras_2800') {
//            tampilkan button proses RKA
            $ada_kegiatannya = 0;
            $d = new Criteria();
                //$d->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
            $d->addAnd(DinasMasterKegiatanPeer::USER_ID, $sf_user->getNamaLogin());
            $d->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $waiting->getKegiatanCode());
            $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
            $ada_kegiatannya = DinasMasterKegiatanPeer::doCount($d);

            if ($ada_kegiatannya > 0) {
                $c_rincian = new Criteria();
                    //$c_rincian->add(RincianPeer::UNIT_ID, '2600');
                $c_rincian->addAnd(DinasRincianPeer::KEGIATAN_CODE, $waiting->getKegiatanCode());
                $data_rincian = DinasRincianPeer::doSelectOne($c_rincian);
//                    echo link_to('<div class="button-green"><i class="fa fa-check"></i> Proses RKA</div>', 'waitinglist_pu/waitingGoRKA?id=' . $waiting->getIdWaiting() . '&cek_rka=' . md5('rka_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju RKA ?'));
//                echo link_to('<div class="button-blue"><i class="fa fa-check"></i> Proses Swakelola</div>', 'waitinglist_pu/waitingGoSwakelola?id=' . $waiting->getIdWaiting() . '&cek_swakelola=' . md5('swakelola_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju swakelola ?'));
//                    if ($waiting->getKomponenId() == '23.05.01.54.A' || $waiting->getKomponenId() == '23.05.01.65.A' || $waiting->getKomponenId() == '23.05.01.72.A' || $waiting->getKomponenId() == '23.05.01.79.A' || $waiting->getKomponenId() == '23.05.01.87') {
//                        
//                    } else {
//                    }
            }
//            tampilkan button proses RKA
        }
        ?>
    </td>
</tr>  
<?php
}
?>
<tr class="keg_<?php echo $id ?>">
    <td colspan="9">&nbsp;</td>
</tr>  