<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Waiting Kertas Kerja</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pekerjaan</a></li>
          <li class="breadcrumb-item active">Waiting Kertas Kerja</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('waitinglist_pu/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead class="head_peach"> 
                                <tr>
                                    <th colspan="4">Nama Sub Kegiatan</th>              
                                    <th colspan="2">Total Pagu List (Lokasi)</th>                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                $con = Propel::getConnection();
                                foreach ($rs_masterkegiatan as $kegiatan) {
                                    $kegiatanId = $kegiatan->getKegiatanId();
                                    $kodeKegiatan = $kegiatan->getKodeKegiatan();
                                    $namaKegiatan = $kegiatan->getNamaKegiatan();
                                    $userid = $kegiatan->getUserId();
                                    $odd = fmod($i++, 2);

                                    $total_pagu = 0;

                                    $query_total_pagu = "select sum(nilai_anggaran) as total_pagu 
                                                from ebudget.dinas_rincian_detail
                                                where kegiatan_code='$kodeKegiatan' and status_hapus = false and unit_id||'.'||kegiatan_code||'.'||detail_no in (select kode_rka from ebudget.waitinglist_pu where status_hapus=false and status_waiting=1
                                                order by kegiatan_code, subtitle, komponen_name, komponen_lokasi) ";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query_total_pagu);
                                    $rs_total_pagu = $stmt->executeQuery();
                                    while ($rs_total_pagu->next()) {
                                        $total_pagu = $rs_total_pagu->getString('total_pagu');
                                    }
                                    
                                    $query_total_lokasi = "select count(*) as total_lokasi 
                                                from ebudget.dinas_rincian_detail
                                                where kegiatan_code='$kodeKegiatan' and status_hapus = false and unit_id||'.'||kegiatan_code||'.'||detail_no in (select kode_rka from ebudget.waitinglist_pu where status_hapus=false and status_waiting=1
                                                order by kegiatan_code, subtitle, komponen_name, komponen_lokasi)  ";
                                    $stmt_lokasi = $con->prepareStatement($query_total_lokasi);
                                    $rs_total_lokasi = $stmt_lokasi->executeQuery();
                                    while ($rs_total_lokasi->next()) {
                                        $total_lokasi = $rs_total_lokasi->getString('total_lokasi');
                                    }
                                    ?>
                                    <tr class="sf_admin_row_1" id="keg_<?php echo str_replace('.', '_', $kodeKegiatan) ?>">
                                        <td colspan="4" class="text-bold"><?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . str_replace('.', '_', $kodeKegiatan), 'id' => 'img_' . str_replace('.', '_', $kodeKegiatan), 'border' => 0)), 'showHideList("' . str_replace('.', '_', $kodeKegiatan) . '")') . ' '; ?><?php echo $kegiatanId . ' - ' . $namaKegiatan.' <br/><span class="badge">'.$userid.'</span>'; ?></td>
                                        <td colspan="2" style="text-align: right"><?php echo number_format($total_pagu) . '<br/><span class="badge">'.$total_lokasi.' Lokasi</span>'; ?></td>
                                    </tr>
                                    
                                    <tr id="indicator_<?php echo str_replace('.', '_', $kodeKegiatan) ?>" style="display:none;" align="center">
                                        <td colspan="6">
                                <dt>&nbsp;</dt>
                                <dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                                </td>
                                </tr>
                            <?php }
                            ?>
                            </tbody>
                            <tfoot>
                                <tr><th colspan="9">&nbsp;</th></tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    image1 = new Image();
    image1.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';

    image2 = new Image();
    image2.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';

    function showHideList(id) {
        var row = $('#keg_' + id);
        var img = $('#img_' + id);
        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus != -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus == -1) {
            var keg_id = 'keg_' + id;
            var usulan = $('.keg_' + id);
            var n = usulan.length;
            if (n > 0) {
                for (var i = 0; i < usulan.length; i++) {
                    var pekerjaan = usulan[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist_pu/getListRka/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#indicator_' + id).after(msg);
                    $('#indicator_' + id).hide();
                });
            }
        } else {
            $('.keg_' + id).remove();
            minus = -1;
        }
    }
</script>