<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Upload Pekerjaan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pekerjaan</a></li>
          <li class="breadcrumb-item active">Upload</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('waitinglist_pu/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-file-upload"></i> Form
                    </h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <?php 
                                echo link_to('Format File Upload', sfConfig::get('app_path_pdf') . 'template/format_upload_waiting_list_2022.xls', array('class' => 'btn btn-outline-primary btn-sm', 'target' => '_blank')); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo form_tag('waitinglist_pu/prosesupload', array('multipart' => true, 'class' => 'form-horizontal')) ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label></label>
                            <?php echo input_file_tag('file',array('class' => 'form-control')) ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Upload <i class="fas fa-upload"></i></button>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col --> 
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>