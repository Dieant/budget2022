<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
if (sfConfig::get('app_tahap_edit') == 'murni') {
  $nama_sistem = 'Pra-RKA';
} elseif (sfConfig::get('app_tahap_edit') == 'pak') {
  $nama_sistem = 'PAK';
} elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
  $nama_sistem = 'Penyesuaian';
} else {
  $nama_sistem = 'Revisi';
}

$i = 0;
?>
<tr class="keg_<?php echo $id ?>" style="text-align: center; font-weight: bold">
  <th>Prioritas</th>
  <th>Subtitle</th>
  <th>Komponen</th>
  <th>Rekening</th>
  <th>Lokasi</th>
  <th>Anggaran</th>
  <th>Nilai EE</th>
  <th>Keterangan</th>
  <th>Action</th>
</tr>
<?php
foreach ($rs_waiting as $waiting) {
  $con = Propel::getConnection();
  $query_ada_gis = "select count(*) as ada  "
  . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
  . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
  . "and unit_id = '" . $waiting->getUnitId() . "' "
  . "and id_waiting = '" . $waiting->getIdWaiting() . "' "
  . "limit 1";
  $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
  $rs_ada_gis = $stmt_ada_gis->executeQuery();
  while ($rs_ada_gis->next()) {
    $ada_gis = $rs_ada_gis->getString('ada');
  }
  ?>
  <tr class="keg_<?php echo $id ?>" style="text-align: center;">
    <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>">

      <?php
      $total_aktif = 0;
      $query = "select count(*) as total "
      . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
      . "where status_hapus = false and status_waiting = 0 "
      . "and kegiatan_code = '" . $waiting->getKegiatanCode() . "'";
      $con = Propel::getConnection();
      $stmt = $con->prepareStatement($query);
      $rs = $stmt->executeQuery();
      while ($rs->next()) {
        $total_aktif = $rs->getString('total');
      }
      if ($sf_user->getNamaUser() != 'binaprogram' && $sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'asisten1' && $sf_user->getNamaUser() != 'asisten2' && $sf_user->getNamaUser() != 'asisten3' && $sf_user->getNamaUser() != 'prk') {
        echo form_tag('waitinglist_pu/setPrioritas');
        ?>
        <div class="col-sm-6">
          <input type="number" name="prioritas" value="<?php echo $waiting->getPrioritas() ?>" style="width: 40px;" required/>            
        </div>
        <div class="col-sm-6">
          <?php
          echo input_hidden_tag('kode_kegiatan', $waiting->getKegiatanCode());
          echo input_hidden_tag('id_waiting', $waiting->getIdWaiting());
          echo input_hidden_tag('prio_sekarang', $waiting->getPrioritas());
          echo ' ' . submit_tag('set', array('name' => 'setPrio', 'id' => 'setPrio', 'class' => 'btn btn-outline-primary btn-xs'));
          echo '</form>';
          ?>
        </div>
      <?php } else { ?>
        <div class="col-sm-6">
          <input type="number" name="prioritas" value="<?php echo $waiting->getPrioritas() ?>" readonly="true"/>            
        </div>
      <?php } ?>
      <div class="clearfix">
      </td>
      <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php echo $waiting->getSubtitle() ?></td>
      <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php
      echo $waiting->getKomponenName();
      if ($waiting->getIsMusrenbang() == TRUE) {
        ?>
        <span class="label label-success">Musrenbang</span>
        <?php
      }
      if ($waiting->getKodeJasmas() <> '') {
        ?>
        <span class="label label-warning">Jasmas</span>
      <?php }
      ?>
    </td>
    <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php echo $waiting->getKomponenRekening() ?></td>
    <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php echo $waiting->getKomponenLokasi() ?></td>
    <td style="text-align: right; <?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php echo number_format($waiting->getNilaiAnggaran()) ?></td>
    <td style="text-align: right; <?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>">
      <?php
      if ($waiting->getNilaiEe() == 0) {
        echo '-';
      } else {
        echo number_format($waiting->getNilaiEe());
      }
      ?>
    </td>
    <td style="<?php echo $ada_gis > 0 ? 'background:#effad3' : ''; ?>"><?php echo $waiting->getKeterangan() ?></td>
    <td>
      <?php if ($sf_user->getNamaLogin() == 'pawang' || $sf_user->getNamaLogin() == 'perancangan_2600' ||  $sf_user->getNamaLogin() == 'superadmin' || $sf_user->getNamaLogin() == 'gis_2600') { ?>
        <div class="btn-group">
          <?php
          echo link_to('<i class="fa fa-edit"></i> Edit', 'waitinglist_pu/waitingedit?id=' . $waiting->getIdWaiting() . '&edit=' . md5('ubah'), array('class' => 'btn btn-outline-primary btn-sm'));
          ?>
          <button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <div class="dropdown-menu dropdown-menu-right" role="menu">
              <?php
              if ($ada_gis > 0) {
                $query_ada_gis = "select * "
                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
                . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
                . "and unit_id = '" . $waiting->getUnitId() . "' "
                . "and id_waiting = '" . $waiting->getIdWaiting() . "'";
                $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
                $rs_ada_gis = $stmt_ada_gis->executeQuery();
                while ($rs_ada_gis->next()) {
                  $mlokasi = $rs_ada_gis->getString('mlokasi');
                  $id_kelompok = $rs_ada_gis->getString('id_kelompok');
                }

                $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist 
                where unit_id='" . $waiting->getUnitId() . "' and kegiatan_code='" . $waiting->getKegiatanCode() . "' and id_waiting ='" . $waiting->getIdWaiting() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                  $total_lokasi = $rs->getString('total_lokasi');
                }

                echo link_to('Ubah Marker Lokasi', sfConfig::get('app_path_gmap') . 'updateData_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'dropdown-item'));
              } else {
                $query2 = "select * from master_kelompok_gmap where '" . $waiting->getKomponenId() . "' ilike kode_kelompok||'%' LIMIT 1";
                $stmt2 = $con->prepareStatement($query2);
                $rs2 = $stmt2->executeQuery();
                while ($rs2->next()) {
                  $id_kelompok = $rs2->getString('id_kelompok');
                }

                if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                  $id_kelompok = 19;

                  if (in_array($waiting->getKomponenSatuan(), array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                    $id_kelompok = 100;
                  } elseif (in_array($waiting->getKomponenSatuan(), array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                    $id_kelompok = 101;
                  }
                }
                echo link_to('Tambah Marker Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'dropdown-item'));
              }
              if ($sf_user->getNamaLogin() == 'pawang' || $sf_user->getNamaLogin() == 'superadmin') {
                echo link_to('Proses ' . $nama_sistem, 'waitinglist_pu/waitingGoRKA?id=' . $waiting->getIdWaiting() . '&cek_rka=' . md5('rka_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju ' . $nama_sistem . ' ?', 'class' => 'dropdown-item'));
                echo link_to('Proses Swakelola', 'waitinglist_pu/waitingGoSwakelola?id=' . $waiting->getIdWaiting() . '&cek_swakelola=' . md5('swakelola_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju Swakelola ?', 'class' => 'dropdown-item'));
              }
              echo link_to('Hapus', 'waitinglist_pu/proseshapus?id=' . $waiting->getIdWaiting() . '&key=' . md5('hapus_waiting'), Array('confirm' => 'Yakin untuk menghapus data Waiting List untuk ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' ?', 'class' => 'dropdown-item'));
              ?>
          </div>
        </div>
        <?php
            }/* else if ($sf_user->getNamaLogin() == 'gis_2600') {
              ?>
              <div class="btn-group">
              <?php
              //                    $con = Propel::getConnection();
              //                    $query_ada_gis = "select count(*) as ada  "
              //                            . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
              //                            . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
              //                            . "and unit_id = '" . $waiting->getUnitId() . "' "
              //                            . "and id_waiting = '" . $waiting->getIdWaiting() . "' "
              //                            . "limit 1";
              //                    $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
              //                    $rs_ada_gis = $stmt_ada_gis->executeQuery();
              //                    while ($rs_ada_gis->next()) {
              //                        $ada_gis = $rs_ada_gis->getString('ada');
              //                    }
              //                    if ($ada_gis > 0) {
              //                        $query_ada_gis = "select * "
              //                                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
              //                                . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
              //                                . "and unit_id = '" . $waiting->getUnitId() . "' "
              //                                . "and id_waiting = '" . $waiting->getIdWaiting() . "'";
              //                        $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
              //                        $rs_ada_gis = $stmt_ada_gis->executeQuery();
              //                        while ($rs_ada_gis->next()) {
              //                            $mlokasi = $rs_ada_gis->getString('mlokasi');
              //                            $id_kelompok = $rs_ada_gis->getString('id_kelompok');
              //                        }
              //
              //                        $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist
              //                                    where unit_id='" . $waiting->getUnitId() . "' and kegiatan_code='" . $waiting->getKegiatanCode() . "' and id_waiting ='" . $waiting->getIdWaiting() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
              //                        $stmt = $con->prepareStatement($query);
              //                        $rs = $stmt->executeQuery();
              //                        while ($rs->next()) {
              //                            $total_lokasi = $rs->getString('total_lokasi');
              //                        }
              //
              //                        echo link_to('<i class="fa fa-pencil"></i> Ubah Marker', sfConfig::get('app_path_gmap') . 'updateData_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default'));
              //                    } else {
              //                        $query2 = "select * from master_kelompok_gmap where '" . $waiting->getKomponenId() . "' ilike kode_kelompok||'%' LIMIT 1";
              //                        $stmt2 = $con->prepareStatement($query2);
              //                        $rs2 = $stmt2->executeQuery();
              //                        while ($rs2->next()) {
              //                            $id_kelompok = $rs2->getString('id_kelompok');
              //                        }
              //
              //                        if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
              //                            $id_kelompok = 19;
              //                        }
              //                        echo link_to('<i class="fa fa-plus-circle"></i> Tambah Marker', sfConfig::get('app_path_gmap') . 'insertBaru_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'btn btn-default'));
              //                    }
              ?>
              </div>
              <?php
            } */ else if ($sf_user->getNamaLogin() == 'jalan_2600' || $sf_user->getNamaLogin() == 'pematusan_2600') {
//            tampilkan button proses RKA
              $ada_kegiatannya = 0;
              $d = new Criteria();
              $d->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
              $d->addAnd(DinasMasterKegiatanPeer::USER_ID, $sf_user->getNamaLogin());
              $d->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $waiting->getKegiatanCode());
              $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
              $ada_kegiatannya = DinasMasterKegiatanPeer::doCount($d);
              if ($ada_kegiatannya > 0) {
                $c_rincian = new Criteria();
                $c_rincian->add(DinasRincianPeer::UNIT_ID, '2600');
                $c_rincian->addAnd(DinasRincianPeer::KEGIATAN_CODE, $waiting->getKegiatanCode());
                $data_rincian = DinasRincianPeer::doSelectOne($c_rincian);
//                    echo link_to('<div class="button-green"><i class="fa fa-check"></i> Proses RKA</div>', 'waitinglist_pu/waitingGoRKA?id=' . $waiting->getIdWaiting() . '&cek_rka=' . md5('rka_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju RKA ?'));
//                echo link_to('<div class="button-blue"><i class="fa fa-check"></i> Proses Swakelola</div>', 'waitinglist_pu/waitingGoSwakelola?id=' . $waiting->getIdWaiting() . '&cek_swakelola=' . md5('swakelola_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju swakelola ?'));
//                    if ($waiting->getKomponenId() == '23.05.01.54.A' || $waiting->getKomponenId() == '23.05.01.65.A' || $waiting->getKomponenId() == '23.05.01.72.A' || $waiting->getKomponenId() == '23.05.01.79.A' || $waiting->getKomponenId() == '23.05.01.87') {
//                        
//                    } else {
                    //if ($data_rincian->getLock() == FALSE && $data_rincian->getRincianLevel() == 2) {
                ?>
          <div class="btn-group">
          <?php
          echo link_to('<i class="fa fa-edit"></i> Edit', 'waitinglist_pu/waitingedit?id=' . $waiting->getIdWaiting() . '&edit=' . md5('ubah'), array('class' => 'btn btn-outline-primary btn-sm'));
          ?>
          <button type="button" class="btn btn-outline-primary btn-sm dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <div class="dropdown-menu dropdown-menu-right" role="menu">
              <?php
              if ($ada_gis > 0) {
                $query_ada_gis = "select * "
                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
                . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
                . "and unit_id = '" . $waiting->getUnitId() . "' "
                . "and id_waiting = '" . $waiting->getIdWaiting() . "'";
                $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
                $rs_ada_gis = $stmt_ada_gis->executeQuery();
                while ($rs_ada_gis->next()) {
                  $mlokasi = $rs_ada_gis->getString('mlokasi');
                  $id_kelompok = $rs_ada_gis->getString('id_kelompok');
                }

                $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist 
                where unit_id='" . $waiting->getUnitId() . "' and kegiatan_code='" . $waiting->getKegiatanCode() . "' and id_waiting ='" . $waiting->getIdWaiting() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                  $total_lokasi = $rs->getString('total_lokasi');
                }

                echo link_to('Ubah Marker Lokasi', sfConfig::get('app_path_gmap') . 'updateData_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'dropdown-item'));
              } else {
                $query2 = "select * from master_kelompok_gmap where '" . $waiting->getKomponenId() . "' ilike kode_kelompok||'%' LIMIT 1";
                $stmt2 = $con->prepareStatement($query2);
                $rs2 = $stmt2->executeQuery();
                while ($rs2->next()) {
                  $id_kelompok = $rs2->getString('id_kelompok');
                }

                if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                  $id_kelompok = 19;

                  if (in_array($waiting->getKomponenSatuan(), array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                    $id_kelompok = 100;
                  } elseif (in_array($waiting->getKomponenSatuan(), array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                    $id_kelompok = 101;
                  }
                }
                echo link_to('Tambah Marker Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'dropdown-item'));
              }
              if ($sf_user->getNamaLogin() == 'pawang' || $sf_user->getNamaLogin() == 'superadmin') {
                echo link_to('Proses ' . $nama_sistem, 'waitinglist_pu/waitingGoRKA?id=' . $waiting->getIdWaiting() . '&cek_rka=' . md5('rka_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju ' . $nama_sistem . ' ?', 'class' => 'dropdown-item'));
                echo link_to('Proses Swakelola', 'waitinglist_pu/waitingGoSwakelola?id=' . $waiting->getIdWaiting() . '&cek_swakelola=' . md5('swakelola_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju Swakelola ?', 'class' => 'dropdown-item'));
              }
              echo link_to('Hapus', 'waitinglist_pu/proseshapus?id=' . $waiting->getIdWaiting() . '&key=' . md5('hapus_waiting'), Array('confirm' => 'Yakin untuk menghapus data Waiting List untuk ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' ?', 'class' => 'dropdown-item'));
              ?>
          </div>
        </div>
                <div class="btn-group">
<!--                   <?php 

                  if ($ada_gis > 0) {
                $query_ada_gis = "select * "
                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
                . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
                . "and unit_id = '" . $waiting->getUnitId() . "' "
                . "and id_waiting = '" . $waiting->getIdWaiting() . "'";
                $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
                $rs_ada_gis = $stmt_ada_gis->executeQuery();
                while ($rs_ada_gis->next()) {
                  $mlokasi = $rs_ada_gis->getString('mlokasi');
                  $id_kelompok = $rs_ada_gis->getString('id_kelompok');
                }

                $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist 
                where unit_id='" . $waiting->getUnitId() . "' and kegiatan_code='" . $waiting->getKegiatanCode() . "' and id_waiting ='" . $waiting->getIdWaiting() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                  $total_lokasi = $rs->getString('total_lokasi');
                }

                echo link_to('Ubah Marker Lokasi', sfConfig::get('app_path_gmap') . 'updateData_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-outline-info btn-xs'));
              } else {
                $query2 = "select * from master_kelompok_gmap where '" . $waiting->getKomponenId() . "' ilike kode_kelompok||'%' LIMIT 1";
                $stmt2 = $con->prepareStatement($query2);
                $rs2 = $stmt2->executeQuery();
                while ($rs2->next()) {
                  $id_kelompok = $rs2->getString('id_kelompok');
                }

                if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                  $id_kelompok = 19;

                  if (in_array($waiting->getKomponenSatuan(), array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                    $id_kelompok = 100;
                  } elseif (in_array($waiting->getKomponenSatuan(), array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                    $id_kelompok = 101;
                  }
                }
                echo link_to('Tambah Marker Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'btn btn-outline-info btn-xs'));
              }

                  ?> -->
                  <?php
                  echo link_to('<i class="fa fa-check"></i> Proses ' . $nama_sistem, 'waitinglist_pu/waitingGoRKA?id=' . $waiting->getIdWaiting() . '&cek_rka=' . md5('rka_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju ' . $nama_sistem . ' ?', 'class' => 'btn btn-outline-primary btn-xs'));
                  echo link_to('<i class="fa fa-check"></i> Proses Swakelola', 'waitinglist_pu/waitingGoSwakelola?id=' . $waiting->getIdWaiting() . '&cek_swakelola=' . md5('swakelola_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju Swakelola ?', 'class' => 'btn btn-outline-info btn-xs'));
                  ?>
                </div>
                <?php
                    //}
//                    }
              }
//            tampilkan button proses RKA
            } elseif ($sf_user->getNamaLogin() == 'permukiman_2300' || $sf_user->getNamaLogin() == 'sarpras_2800') {
              ?>
              <div class = "btn-group">
                <?php
                echo link_to('<i class="fa fa-edit"></i> Edit', 'waitinglist_pu/waitingedit?id=' . $waiting->getIdWaiting() . '&edit=' . md5('ubah'), array('class' => 'btn btn-outline-primary btn-xs'));
                ?>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <?php
                    if ($ada_gis > 0) {
                      $query_ada_gis = "select * "
                      . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
                      . "where kegiatan_code='" . $waiting->getKegiatanCode() . "' "
                      . "and unit_id = '" . $waiting->getUnitId() . "' "
                      . "and id_waiting = '" . $waiting->getIdWaiting() . "'";
                      $stmt_ada_gis = $con->prepareStatement($query_ada_gis);
                      $rs_ada_gis = $stmt_ada_gis->executeQuery();
                      while ($rs_ada_gis->next()) {
                        $mlokasi = $rs_ada_gis->getString('mlokasi');
                        $id_kelompok = $rs_ada_gis->getString('id_kelompok');
                      }

                      $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist 
                      where unit_id='" . $waiting->getUnitId() . "' and kegiatan_code='" . $waiting->getKegiatanCode() . "' and id_waiting ='" . $waiting->getIdWaiting() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                      $stmt = $con->prepareStatement($query);
                      $rs = $stmt->executeQuery();
                      while ($rs->next()) {
                        $total_lokasi = $rs->getString('total_lokasi');
                      }

                      echo link_to('Ubah Marker Lokasi', sfConfig::get('app_path_gmap') . 'updateData_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'dropdown-item'));
                    } else {
                      $query2 = "select * from master_kelompok_gmap where '" . $waiting->getKomponenId() . "' ilike kode_kelompok||'%' LIMIT 1";
                      $stmt2 = $con->prepareStatement($query2);
                      $rs2 = $stmt2->executeQuery();
                      while ($rs2->next()) {
                        $id_kelompok = $rs2->getString('id_kelompok');
                      }

                      if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                        $id_kelompok = 19;

                        if (in_array($waiting->getKomponenSatuan(), array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                          $id_kelompok = 100;
                        } elseif (in_array($waiting->getKomponenSatuan(), array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                          $id_kelompok = 101;
                        }
                      }
                      echo link_to('Tambah Marker Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru_waitinglist.php?unit_id=' . $waiting->getUnitId() . '&kode_kegiatan=' . $waiting->getKegiatanCode() . '&id_waiting=' . $waiting->getIdWaiting() . '&satuan=' . $waiting->getKomponenSatuan() . '&volume=' . $waiting->getVolume() . '&nilai_anggaran=' . $waiting->getNilaiAnggaran() . '&tahun=' . $waiting->getTahunInput() . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'dropdown-item'));
                    }
                    echo link_to('Proses ' . $nama_sistem, 'waitinglist_pu/waitingGoRKA?id=' . $waiting->getIdWaiting() . '&cek_rka=' . md5('rka_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju ' . $nama_sistem . ' ?', 'class' => 'dropdown-item'));
                    echo link_to('Proses Swakelola', 'waitinglist_pu/waitingGoSwakelola?id=' . $waiting->getIdWaiting() . '&cek_swakelola=' . md5('swakelola_waiting'), array('confirm' => 'Yakin untuk memproses ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' menuju Swakelola ?', 'class' => 'dropdown-item'));
                    echo link_to('Hapus', 'waitinglist_pu/proseshapus?id=' . $waiting->getIdWaiting() . '&key=' . md5('hapus_waiting'), Array('confirm' => 'Yakin untuk menghapus data Waiting List untuk ' . $waiting->getKomponenName() . ' ' . $waiting->getKomponenLokasi() . ' ?', 'class' => 'dropdown-item'));
                    ?>
                </div>
              </div>
              <?php
            }
            ?>
          </td>
        </tr>  
        <?php
      }
      ?>
      <tr class="keg_<?php echo $id ?>">
        <td colspan="9">&nbsp;</td>
      </tr>  