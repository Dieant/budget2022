<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<p align="right">
    <?php
    if ( $sf_user->getNamaLogin() == 'admin') {
        echo button_to('Waiting List Pekerjaan Fisik', 'waitinglist_pu/waitinglist') . '  |  ';
//        echo button_to('List Pekerjaan Terproses', 'waitinglist_pu/ambilwaitinglist') . '  |  ';
        echo button_to('Seluruh List Pekerjaan', 'waitinglist_pu/seluruhwaitinglist') . '  |  ';
        echo button_to('Tambah Pekerjaan Fisik Baru', 'waitinglist_pu/waitingcari') . '  |  ';
        echo button_to('Upload Pekerjaan Fisik Baru', 'waitinglist_pu/waitingupload');
    }
    if ($sf_user->getNamaLogin() == 'pematusan_2600' || $sf_user->getNamaLogin() == 'jalan_2600' || $sf_user->getNamaLogin() == 'permukiman_2300' || $sf_user->getNamaLogin() == 'sarpras_2800') {
        echo button_to('Waiting List Pekerjaan Fisik', 'waitinglist_pu/waitinglist') . '  |  ';
//        echo button_to('List Pekerjaan Terproses', 'waitinglist_pu/ambilwaitinglist') . '  |  ';
        echo button_to('Seluruh List Pekerjaan', 'waitinglist_pu/seluruhwaitinglist');
    }
    ?>
</p>