<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah Baru Pekerjaan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Pekerjaan</a></li>
                    <li class="breadcrumb-item active">Tambah Baru</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('waitinglist_pu/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-filter"></i> Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('waitinglist_pu/waitingcari', array('method' => 'get','class'=>'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Komponen</label>
                                    <?php echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Komponen')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i
                                            class="fas fa-search"></i></button>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Tambah
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('waitinglist_pu/baruKegiatan', array('class' => 'form-horizontal')); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kelompok Belanja</label>
                                    <?php
                                $rekening_code = trim($sf_params->get('rekening'));
                                $belanja_code = substr($rekening_code, 0, 6);
                                $c = new Criteria();
                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, $belanja_code);
                                $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                if ($rs_belanja) {
                                    echo "<span class='form-control'>".$rs_belanja->getBelanjaName()."</span>";
                                }
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <?php
                                $c = new Criteria();
                                $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                                $rs_rekening = RekeningPeer::doSelectOne($c);
                                if ($rs_rekening) {
                                    $pajak_rekening = $rs_rekening->getRekeningPpn();
                                    echo "<span class='form-control'>".$rekening_code . ' ' . $rs_rekening->getRekeningName()."</span>";
                                    echo "<input type='hidden' name='rekening' value=".$rekening_code.">";
                                    echo "<input type='hidden' name='tipe' value=".$_GET['tipe'].">";
                                    echo "<input type='hidden' name='id' value=".$_GET['komponen'].">";
                                }
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Komponen</label>
                                    <?php echo "<span class='form-control'>".$rs_komponen->getKomponenName()."</span>"; ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <?php
                                    $komponen_harga = $rs_komponen->getKomponenHarga();
                                    if ($rs_komponen->getSatuan() == '%') {
                                        echo "<span class='form-control'>".$komponen_harga."</span>";
                                    } else {
                                        echo "<span class='form-control'>".number_format($komponen_harga, 0, ',', '.')."</span>";
                                    } echo input_hidden_tag('harga', $komponen_harga);
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <?php
                                echo "<span class='form-control'>".$rs_komponen->getSatuan()."</span>";
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <?php
                                echo "<span class='form-control'>".$sf_params->get('pajak') . '%</span>';
                                echo input_hidden_tag('pajakx', $sf_params->get('pajak'));
                                echo input_hidden_tag('pajak', $sf_params->get('pajak'));
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Sub-Kegiatan</label>
                                    <?php
                                echo input_hidden_tag('unit_id', $sf_params->get('unit'));
                                echo select_tag('kode_kegiatan', objects_for_select($rs_masterkegiatan, 'getKodeKegiatan', 'getNamaKegiatan', $kode_kegiatan, 'include_custom=--Pilih Sub Kegiatan--'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Subtitle</label>
                                    <?php
                                if ($sf_params->get('kode_kegiatan')) {
                                    $d = new Criteria();
                                    $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $sf_params->get('unit'));
                                    $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $sf_params->get('kode_kegiatan'));
                                    $rs_subtitleindikator = DinasSubtitleIndikatorPeer::doSelectOne($d);
                                    if ($rs_subtitleindikator) {
                                        echo select_tag('subtitle', options_for_select(array($rs_subtitleindikator->getSubtitle()), $rs_subtitleindikator->getSubtitle(), 'include_custom=--Pilih Subtitle Dulu--'), Array('id' => 'sub1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                    }
                                } elseif (!$sf_params->get('kode_kegiatan')) {
                                    echo select_tag('subtitle', options_for_select(array(), '', 'include_custom=--Pilih Sub Kegiatan Dulu--'), Array('id' => 'sub1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                }
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <?php
                                echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=--Pilih Kecamatan--'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kelurahan</label>
                                    <?php
                                echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=--Pilih Kecamatan Dulu--'), Array('id' => 'kelurahan1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                            ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Jalan</label>
                                    <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan_isian" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Gang/Blok/Kavling</label>
                                    <select class="form-control select2 tipe_gang_isian" name="tipe_gang[]">
                                        <option value="GG">--Pilih--</option>
                                        <option value="GG">GANG</option>
                                        <option value="BLOK">BLOK</option>
                                        <option value="KAV">KAVLING</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Gang/Blok/Kavling</label>
                                    <input type="text" name="lokasi_gang[]" class="form-control lokasi_gang_isian" placeholder="Nama Gang/Blok/Kavling">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nomor Lokasi</label>
                                    <input type="text" name="lokasi_nomor[]" class="form-control lokasi_nomor_isian" placeholder="Nomor">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RW</label>
                                    <input type="text" name="lokasi_rw[]" class="form-control lokasi_rw_isian" placeholder="RW">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>RT</label>
                                    <input type="text" name="lokasi_rt[]" class="form-control lokasi_rt_isian" placeholder="RT">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Nama Bangunan/Saluran</label>
                                    <input type="text" name="lokasi_tempat[]" class="form-control lokasi_tempat_isian" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)">
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Keterangan Lokasi</label>
                                    <input type="text" name="lokasi_keterangan[]" class="form-control lokasi_keterangan_isian" placeholder="Keterangan Lokasi">
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            $keterangan_koefisien = $sf_params->get('keterangan_koefisien');
                            $pisah_kali = explode('X', $keterangan_koefisien);
                            for ($i = 0; $i < 4; $i++) {
                                $satuan = '';
                                $volume = '';
                                $nama_input = 'vol' . ($i + 1);
                                $nama_pilih = 'volume' . ($i + 1);
                                if (!empty($pisah_kali[$i])) {
                                    $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                    $j = 0;

                                    for ($s = 0; $s < count($pisah_spasi); $s++) {
                                        if ($pisah_spasi[$s] != NULL) {
                                            if ($j == 0) {
                                                $volume = $pisah_spasi[$s];
                                                $j++;
                                            } elseif ($j == 1) {
                                                $satuan = $pisah_spasi[$s];
                                                $j++;
                                            } else {
                                                $satuan.=' ' . $pisah_spasi[$s];
                                            }
                                        }
                                    }
                                }
                                if ($i !== 3) {
                                    ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php 
                                            if($i == 0){
                                                echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()', 'required' => 'true'));
                                            }else{
                                                echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()'));
                                            }
                                        
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>X</label>
                                    </div>
                                </div>
                                <?php
                                } else {
                                ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php echo input_tag($nama_input, $volume, array('class' => 'form-control', 'onChange' => 'hitungTotal()')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2'));?>
                                    </div>
                                </div>
                                <?php
                                }
                            }
                            ?>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nilai EE</label>
                                    <?php echo input_tag('nilai_ee', '', array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Usulan Jasmas</label>
                                    <?php echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', '', 'include_custom=--Pilih Jasmas--'), array('class' => 'js-example-basic-single form-control select2', 'style' => 'width:100%')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Musrenbang</label><br />
                                    <?php echo checkbox_tag('musrenbang', array('class' => 'form-control')); ?>
                                    <font style="color: green"> *Centang jika komponen Musrenbang</font>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <?php echo input_tag('keterangan', '', array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <button type="submit" name="simpan" value="simpan" class="btn btn-outline-primary btn-sm">Simpan <i
                                            class="far fa-save"></i></button>
                                    <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i
                                            class="fa fa-backspace"></i></button>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });

    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' +
                sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });
    $("#kode_kegiatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist_pu/pilihkegiatan/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });
    });
    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist_pu/pilihKelurahan/b/" +
                id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('harga').value;
        var pajakx = $('pajakx').value;
        var vol1 = $('vol1').value;
        var vol2 = $('vol2').value;
        var vol3 = $('vol3').value;
        var vol4 = $('vol4').value;
        var volume;

        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx === 10) {
            var hitung = (harga * volume * (110) / 100);
        } else if (pajakx === 0) {
            var hitung = (harga * volume * 1);
        }

        $('total').value = hitung;

    }
</script>