<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach"> 
            <tr>
                <th>ID - Nama Sub Kegiatan</th>
                <th>Nama Komponen</th>
                <th colspan="2">Nama Lokasi</th>
                <th>Harga (Pajak)</th>
                <th>Koefisien</th>
                <th>Anggaran</th>
                <th>Nilai EE</th>
                <th>Keterangan</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $waitinglist): $odd = fmod( ++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <td><?php echo '<b>' . $waitinglist->getKegiatanCode() . ' - ' . $waitinglist->getSubtitle() . '</b>'; ?></td>
                    <td><?php echo $waitinglist->getKomponenName(); ?></td>
                    <td colspan="2"><?php echo $waitinglist->getKomponenLokasi(); ?></td>
                    <td style="text-align: right"><?php echo number_format($waitinglist->getKomponenHargaAwal(), 2) . ' (' . $waitinglist->getPajak() . ')' ?></td>
                    <td style="text-align: right"><?php echo $waitinglist->getKoefisien(); ?></td>
                    <td style="text-align: right"><?php echo number_format($waitinglist->getNilaiAnggaran(), 0, '.', ','); ?></td>
                    <td style="text-align: right">
                        <?php
                        if ($waitinglist->getNilaiEe() == 0) {
                            echo '-';
                        } else {
                            echo number_format($waitinglist->getNilaiEe(), 0, '.', ',');
                        }
                        ?>
                    </td>
                    <td><?php echo $waitinglist->getKeterangan(); ?></td>
                    <td style="text-align: center">
                        <?php
                        if ($waitinglist->getStatusHapus() == FALSE) {
                            if ($waitinglist->getStatusWaiting() == 0) {
                                echo '<font style="color: blue">Dalam Antrian</font><br/>';
                                echo 'Mulai :: ' . date('d-m-Y H:i', strtotime($waitinglist->getCreatedAt())) . '<br/>';
                            } else if ($waitinglist->getStatusWaiting() == 1) {
                                echo '<font style="color: green">Diproses dalam RKA</font><br/>';
                                echo 'Oleh :: ' . $waitinglist->getUserPengambil() . '<br/>';
                                echo 'Pada :: ' . date('d-m-Y H:i', strtotime($waitinglist->getUpdatedAt())) . '<br/>';
                            } else if ($waitinglist->getStatusWaiting() == 2) {
                                echo '<font style="color: green">Diproses dalam Swakelola</font><br/>';
                                echo 'Oleh :: ' . $waitinglist->getUserPengambil() . '<br/>';
                                echo 'Pada :: ' . date('d-m-Y H:i', strtotime($waitinglist->getUpdatedAt())) . '<br/>';
                            }
                        } else {
                            echo '<font style="color: red">Dibatalkan</font><br/>';
                            echo 'Pada :: ' . date('d-m-Y H:i', strtotime($waitinglist->getUpdatedAt())) . '<br/>';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'waitinglist_pu/seluruhwaitinglist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "waitinglist_pu/seluruhwaitinglist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'waitinglist_pu/seluruhwaitinglist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>