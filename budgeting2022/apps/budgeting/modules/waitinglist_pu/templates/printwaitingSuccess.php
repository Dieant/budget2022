<style>
    table, th, td{
        border: 1px solid black;
        padding: 0px;
    }
    table {
        border-spacing: 0px;
    }
</style>
<table style="width: 100%;" cellspacing="0">    
    <thead>
        <tr>
            <th colspan="7" style="text-align: center">Waiting List Per <?php echo date('d-m-Y H:i',  strtotime($sekarang)) ?></th>
        </tr>
        <tr>
            <th style="width: 10px; text-align: center">&nbsp;</th>
            <th style="width: 10px; text-align: center">&nbsp;</th>
            <th style="text-align: center" >Kegiatan/Subtitle/Komponen</th>
            <th style="text-align: center" >Semula</th>
            <th style="text-align: center" >Sekarang</th>
            <th style="text-align: center" >Posisi</th>
            <th style="text-align: center" >Keterangan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $kegiatan_sebelum = '';
        $subtitle_sebelum = '';
        foreach ($waitinglist as $list) {
            if ($kegiatan_sebelum <> $list['kegiatan_code']) {
                ?>
                <tr>
                    <td colspan="7" style="font-weight: bold"><?php echo $list['kegiatan_code'] . ' - ' . $list['nama_kegiatan']; ?></td>
                </tr>
                <?php
                $kegiatan_sebelum = $list['kegiatan_code'];
            }
            if ($subtitle_sebelum <> $list['subtitle']) {
                ?>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="6" style="font-weight: bold"><?php echo '::' . $list['subtitle']; ?></td>
                </tr>
                <?php
                $subtitle_sebelum = $list['subtitle'];
            }
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><?php echo $list['komponen_name'] . ' ' . $list['komponen_lokasi']; ?></td>
                <td style="text-align: right"><?php echo number_format($list['nilai_anggaran_semula'], 0, ',', '.'); ?></td>
                <td style="text-align: right"><?php echo number_format($list['nilai_anggaran_sekarang'], 0, ',', '.'); ?></td>
                <td style="text-align: center">
                    <?php
                    if ($list['status_waiting'] == 0) {
                        echo 'Waiting List';
                    } else {
                        echo 'RKA eBudgeting';
                    }
                    ?>
                </td>
                <td><?php echo $list['keterangan']; ?></td>
            </tr>
        <?php }
        ?>                    
    </tbody>
</table>