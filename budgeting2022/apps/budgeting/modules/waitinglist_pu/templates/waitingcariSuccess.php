<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Tambah Komponen</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pekerjaan</a></li>
          <li class="breadcrumb-item active">Tambah Komponen</li>
      </ol>
  </div>
</div>
</div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('waitinglist_pu/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('waitinglist_pu/waitingcari', array('method' => 'get','class'=>'form-horizontal')) ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama Komponen</label>
                                <?php echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Komponen')); ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                                <?php
                                echo link_to('Reset <i class="fa fa-backspace"></i>', 'waitinglist_pu/waitingcari?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                                ?>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 stretch-card">
            <div class="card">
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover">
                        <thead class="head_peach">
                            <tr>
                                <th>Komponen</th>
                                <th>Spesifikasi</th>
                                <th>Satuan</th>
                                <th>Tipe</th>
                                <th>Harga</th>
                                <th>Pajak</th>
                                <th>Rekening</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $nama = $sf_user->getNamaUser();
                            $d = new Criteria();
                            $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
                            if ($rs_unitkerja) {
                                $nama = $rs_unitkerja->getUnitId();
                            }
                            $unit_id = $nama;
                            if (isset($filters)) {
                                $query = "
                                (
                                select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name, k.status_masuk 
                                from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                . "where k.komponen_id = kr.komponen_id and k.komponen_tipe = 'FISIK' and k.komponen_name ilike '%" . $cari . "%' and 
                                kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id 
                                ) 
                                union all
                                (
                                select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name, k.status_masuk 
                                from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r "
                                . "where k.komponen_id = kr.komponen_id and k.komponen_tipe = 'EST' and k.komponen_name ilike '%" . $cari . "%' and 
                                kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id 
                                and k.komponen_id in ('23.05.01.54.A','23.05.01.65.A','23.05.01.72.A','23.05.01.79.A','23.05.01.87')
                                )
                                order by komponen_id, rekening_code                                         
                                ";
                            } else {
                                $query = "select kb.belanja_name,kr.rekening_code,r.rekening_name,  k.satuan, k.komponen_harga,  k.komponen_tipe as tipe, k.komponen_non_pajak as pajak, k.komponen_id, k.komponen_name, k.status_masuk 
                                from " . sfConfig::get('app_default_schema') . ".komponen k, " . sfConfig::get('app_default_schema') . ".komponen_rekening kr, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb, " . sfConfig::get('app_default_schema') . ".rekening r 
                                where k.komponen_id = kr.komponen_id and k.komponen_tipe = 'FISIK' and 
                                kr.rekening_code = r.rekening_code and r.belanja_id = kb.belanja_id 
                                order by k.komponen_id, k.rekening 
                                ";
                            }
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($query);

                            $rs = $stmt->executeQuery();
                            $belanja = '';
                            $kode_rekening = '';
                            $shsd_name = '';
                            $keterangan = '';
                            $tipe = '';

                            $status_masuk = '';

                            echo '';

                            while ($rs->next()) {

                            //echo $rs->getString('pajak');
                                if ($rs->getString('status_masuk') == 'baru') {
                                    $status_masuk = ' ' . image_tag('new1.png', array('align' => 'absmiddle', 'alt' => 'Komponen Baru', 'width' => '20', 'height' => '20'));
                                }
                                if ($rs->getBoolean('pajak') == TRUE) {
                                    $pajak = 0;
                                }
                                if ($rs->getBoolean('pajak') == FALSE) {
                                    $pajak = 10;
                                }
                                if ($komponen_sebelum != $rs->getString('komponen_id')) {
                                    $komponen_sebelum = $rs->getString('komponen_id');
                                    ?>
                                    <tr class="sf_admin_row_1" align='center'>
                                        <td colspan="8"><strong><?php echo $rs->getString('komponen_name') ?></strong></td>
                                    </tr>                    
                                    <?php
                                }

                                $satuan = $rs->getString('satuan');
                                $tipe = $rs->getString('tipe');

                                $komponen_id = $rs->getString('komponen_id');

                                if ((substr($rs->getString('rekening_code'), 0, 5) == '5.2.1')) {
                                    $warna_row = "5";
                                    $warna_button = 'btn btn-success';
                                } elseif ((substr($rs->getString('rekening_code'), 0, 5) == '5.2.2')) {
                                    $warna_row = "7";
                                    $warna_button = 'btn btn-danger';
                                } elseif ((substr($rs->getString('rekening_code'), 0, 5) == '5.2.3')) {
                                    $warna_row = "3";
                                    $warna_button = 'btn btn-default';
                                }

                                if (($tipe == 'SAB') || ($tipe == 'FISIK') || ($tipe == 'OM') || ($tipe == 'HSPK')) {
                                    $shsd_name = $rs->getString('komponen_name');
                                } elseif (($tipe != 'SAB') && (substr($rs->getString('rekening_code'), 0, 8) != '5.2.1.01') && (substr($rs->getString('rekening_code'), 0, 8) != '5.2.1.02')) {
                                    $shsd_name = $rs->getString('komponen_name');
                                    $warna_t = '';
                                } else {
                                    $shsd_name = $rs->getString('komponen_name');
                                }

                                $shsd_spec = '';
                                $shsd_hidden_spec = '';
                                ?>
                                <tr>
                                    <td><?php echo $shsd_name . $status_masuk . $keterangan ?> </td>                                
                                    <td><?php echo $shsd_spec . ' ' . $shsd_hidden_spec ?></td>
                                    <td><?php echo '&nbsp;' . $satuan ?> </td>
                                    <?php
                                    if ($tipe == 'SHSD') {
                                        $tipe = 'SSH';
                                    }
                                    ?>
                                    <td><?php echo '&nbsp;' . $tipe ?> </td>
                                    <td align='right'>
                                        <?php
                                        if ($satuan == '%') {
                                            echo '&nbsp;' . $rs->getString('komponen_harga');
                                        } else {
                                            echo '&nbsp;' . number_format($rs->getString('komponen_harga'), 2, ',', '.');
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo '&nbsp;' . $pajak . '%' ?> </td>
                                    <td><?php echo $rs->getString('rekening_code') . '   -   ' . $rs->getString('rekening_name') ?> </td>
                                    <td class="text-center">
                                        <?php
                                            echo form_tag('waitinglist_pu/waitingbaru', array('method' => 'get','class'=>'simpleForm')) .
                                            input_hidden_tag('rekening', $rs->getString('rekening_code')) .
                                            input_hidden_tag('pajak', $pajak) .
                                            input_hidden_tag('unit', $unit_id) .
                                            input_hidden_tag('tipe', $tipe) .
                                            input_hidden_tag('komponen', $komponen_id) .
                                            input_hidden_tag('baru', md5('terbaru')) . '&nbsp;' .
                                            submit_tag('Pilih', array('class' => 'btn btn-outline-primary btn-sm'));
                                            echo '</form>';
                                        ?> 
                                </td>
                            </tr>
                            <?php
                            }
                            ?>            
                        </tbody>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</section>