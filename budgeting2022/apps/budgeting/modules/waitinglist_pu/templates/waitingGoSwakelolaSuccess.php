<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Ubah Waiting List</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Pekerjaan</a></li>
                    <li class="breadcrumb-item active">Ubah Waiting List</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('waitinglist_pu/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Ubah
                        </h3>
                    </div>
                    <div class="card-body">     
                    <?php echo form_tag('waitinglist_pu/waitingGoSwakelola') ?>
                    <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <?php echo select_tag('rekening', objects_for_select($rs_rekening, 'getRekeningCode', 'getRekeningCode', $waitinglist->getKomponenRekening(), 'include_custom=---Pilih Rekening--'), array('class' => 'form control select2', 'style' => 'width:100%')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Komponen</label>
                                    <?php echo "<span class='form-control'>".$waitinglist->getKomponenName()."</span>"; ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <?php
                                    $komponen_harga = $waitinglist->getKomponenHargaAwal();
                                    if ($waitinglist->getKomponenSatuan() == '%') {
                                        echo "<span class='form-control'>".$komponen_harga."</span>";
                                    } else {
                                        echo "<span class='form-control'>".number_format($komponen_harga, 0, ',', '.')."</span>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <?php
                                        echo "<span class='form-control'>".$waitinglist->getKomponenSatuan()."</span>";
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pajak</label>
                                    <?php
                                        echo "<span class='form-control'>".$waitinglist->getPajak() . '%</span>';
                                    ?>
                                </div>
                            </div>
                            <!-- /.col --><div class="col-sm-6">
                                <div class="form-group">
                                    <label>Sub-Kegiatan</label>
                                    <?php
                                        echo input_hidden_tag('unit_id', $sf_params->get('unit'));
                                        $kegiatan_code = $waitinglist->getKegiatanCode();
                                        echo select_tag('kode_kegiatan', objects_for_select($rs_masterkegiatan, 'getKodeKegiatan', 'getNamaKegiatan', $kegiatan_code, 'include_custom=---Pilih Sub Kegiatan---'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Subtitle</label>
                                    <?php
                                        if ($waitinglist->getSubtitle()) {
                                            echo select_tag('subtitle', options_for_select(array($waitinglist->getSubtitle() => $waitinglist->getSubtitle()), $waitinglist->getSubtitle()), Array('id' => 'sub1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                        } else {
                                            echo select_tag('subtitle', options_for_select(array(), '', 'include_custom=---Pilih Sub Kegiatan Dulu---'), Array('id' => 'sub1', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <?php
                                        $kec = new Criteria();
                                        $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                                        $rs_kec = KecamatanPeer::doSelect($kec);
                                        $nama_kecamatan_kel = $rs_kec;
                                        if ($waitinglist->getKecamatan()) {
                                            $kec_ini = new Criteria();
                                            $kec_ini->add(KecamatanPeer::NAMA, $waitinglist->getKecamatan());
                                            $rs_kec_ini = KecamatanPeer::doSelectOne($kec_ini);
                                            echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', $rs_kec_ini->getId(), 'include_custom=---Pilih Kecamatan---'), array('class' => 'form-control select2', 'style' => 'width:100%', 'readonly' => true));
                                        } else {
                                            echo select_tag('kecamatan', objects_for_select($nama_kecamatan_kel, 'getId', 'getNama', '', 'include_custom=---Pilih Kecamatan---'), array('class' => 'form-control select2', 'style' => 'width:100%', 'readonly' => true));
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Kelurahan</label>
                                    <?php
                                        if ($waitinglist->getKelurahan() && $waitinglist->getKecamatan()) {
                                            $kec_kel_ini = new Criteria();
                                            $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KELURAHAN, $waitinglist->getKelurahan());
                                            $kec_kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $waitinglist->getKecamatan());
                                            $rs_kec_kel_ini = KelurahanKecamatanPeer::doSelectOne($kec_kel_ini);
                                            $kel_ini = new Criteria();
                                            $kel_ini->add(KelurahanKecamatanPeer::NAMA_KECAMATAN, $waitinglist->getKecamatan());
                                            $nama_kel_ini = KelurahanKecamatanPeer::doSelect($kel_ini);
                                            $options = array();
                                            foreach ($nama_kel_ini as $kel) {
                                                $options[$kel->getOid()] = $kel->getNamaKelurahan();
                                            }
                                            echo select_tag('kelurahan', options_for_select($options, $rs_kec_kel_ini->getOid(), 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'form-control select2', 'style' => 'width:100%', 'readonly' => true));
                                        } else {
                                            echo select_tag('kelurahan', options_for_select(array(), '', 'include_custom=---Pilih Kecamatan Dulu---'), Array('id' => 'kelurahan1', 'class' => 'form-control select2', 'style' => 'width:100%', 'readonly' => true));
                                        }
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Lokasi Sekarang</label>
                                    <?php echo input_tag('lokasi', $waitinglist->getKomponenLokasi(), array('class' => 'form-control', 'readonly' => 'readonly')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <?php
                            foreach ($rs_geojson as $value_geojson) 
                            {
                                if ($value_geojson->getGang() <> '') {
                                    $array_gang = explode('.', $value_geojson->getGang());
                                    $tipe_gang = $array_gang[0];
                                    $nama_gang = trim($array_gang[1]);
                                }
                                ?>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama Jalan</label>
                                        <input type="text" name="lokasi_jalan[]" class="form-control lokasi_jalan_isian" placeholder="Nama Jalan (*wajib diisi apabila lokasi berupa Jalan)" value="<?php echo $value_geojson->getJalan() ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Gang/Blok/Kavling</label>
                                        <input type="text" name="tipe_gang[]" class="form-control" placeholder="Gang/Blok/Kavling" value="<?php echo $tipe_gang ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama Gang/Blok/Kavling</label>
                                        <input type="text" name="lokasi_gang[]" class="form-control lokasi_gang_isian" placeholder="Nama Gang" value="<?php echo $nama_gang ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nomor Lokasi</label>
                                        <input type="text" name="lokasi_nomor[]" class="form-control lokasi_nomor_isian" placeholder="Nomor" value="<?php echo $value_geojson->getNomor() ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>RW</label>
                                        <input type="text" name="lokasi_rw[]" class="form-control lokasi_rw_isian" placeholder="RW" value="<?php echo $value_geojson->getRw() ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>RT</label>
                                        <input type="text" name="lokasi_rt[]" class="form-control lokasi_rt_isian" placeholder="RT" value="<?php echo $value_geojson->getRt() ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Nama Bangunan/Saluran</label>
                                        <input type="text" name="lokasi_tempat[]" class="form-control lokasi_tempat_isian" placeholder="Nama Bangunan/Saluran (*wajib diisi apabila lokasi berupa bangunan/saluran/tempat)" value="<?php echo $value_geojson->getTempat() ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Keterangan Lokasi</label>
                                        <input type="text" name="lokasi_keterangan[]" class="form-control lokasi_keterangan_isian" placeholder="Keterangan Lokasi" value="<?php echo $value_geojson->getKeterangan() ?>" readonly >
                                    </div>
                                </div>
                                <!-- /.col -->
                            <?php
                            }
                            $keterangan_koefisien = $waitinglist->getKoefisien();
                            $pisah_kali = explode('X', $keterangan_koefisien);
                            for ($i = 0; $i < 4; $i++) {
                                $satuan = '';
                                $volume = '';
                                $nama_input = 'vol' . ($i + 1);
                                $nama_pilih = 'volume' . ($i + 1);
                                ;
                                if (!empty($pisah_kali[$i])) {
                                    $pisah_spasi = explode(' ', $pisah_kali[$i]);
                                    $j = 0;

                                    for ($s = 0; $s < count($pisah_spasi); $s++) {
                                        if ($pisah_spasi[$s] != NULL) {
                                            if ($j == 0) {
                                                $volume = $pisah_spasi[$s];
                                                $j++;
                                            } elseif ($j == 1) {
                                                $satuan = $pisah_spasi[$s];
                                                $j++;
                                            } else {
                                                $satuan.=' ' . $pisah_spasi[$s];
                                            }
                                        }
                                    }
                                }
                                if ($i !== 3) {
                                    ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php echo input_tag($nama_input, $volume, array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>X</label>
                                    </div>
                                </div>
                                <?php
                                } else {
                                ?>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Volume</label>
                                        <?php echo input_tag($nama_input, $volume, array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <?php echo select_tag($nama_pilih, objects_for_select($rs_satuan, 'getSatuanName', 'getSatuanName', $satuan, 'include_custom=--Pilih Satuan--'), array('class' => 'form-control select2'));?>
                                    </div>
                                </div>
                                <?php
                                }
                            }
                            ?>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Total</label>
                                    <?php echo input_tag('total', $waitinglist->getNilaiAnggaran(), array('readonly' => 'true', 'class' => 'form-control')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nilai EE</label>
                                    <?php echo input_tag('nilai_ee', $waitinglist->getNilaiEe(), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Musrenbang</label><br />
                                    <?php
                                        if ($waitinglist->getIsMusrenbang() == TRUE) {
                                            echo checkbox_tag('musrenbang', 1, TRUE, array('class' => 'form-control'));
                                        } else {
                                            echo checkbox_tag('musrenbang', array('class' => 'form-control'));
                                        }
                                    ?>
                                    <font style="color: green"> *Centang jika komponen Musrenbang</font>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <?php echo textarea_tag('keterangan', $waitinglist->getKeterangan(), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <?php
                                        echo input_hidden_tag('id', $sf_params->get('id'));
                                        echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                                        echo submit_tag('Simpan', 'name=simpan') . ' ' . button_to('Kembali', '#', array('onClick' => "javascript:history.back()")); 
                                    ?>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                    <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        padding: 3px 5px 3px 18px;
        margin: 3px 0 3px 5px;
        line-height: 20px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".form control select2").select2();
        $(".js-example-basic-multiple").select2();
    });

    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });

    $("#kode_kegiatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist_pu/pilihkegiatan/unit_id/<?php echo $sf_params->get('unit') ?>/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#sub1').html(msg);
        });

    });
    $("#kecamatan").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/waitinglist_pu/pilihKelurahan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#kelurahan1').html(msg);
        });

    });

    function hitungTotal() {
        var harga = $('harga').value;
        var pajakx = $('pajakx').value;
        var vol1 = $('vol1').value;
        var vol2 = $('vol2').value;
        var vol3 = $('vol3').value;
        var vol4 = $('vol4').value;
        var volume;

        if (vol1 !== '' || vol2 !== '' || vol3 !== '' || vol4 !== '') {
            if (vol2 === '') {
                vol2 = 1;
                volume = vol1 * vol2;
            } else if (vol2 !== '') {
                volume = vol1 * vol2;
            }
            if (vol3 === '') {
                vol3 = 1;
                volume = volume * vol3;
            } else if (vol3 !== '') {
                volume = vol1 * vol2 * vol3;
            }
            if (vol4 === '') {
                vol4 = 1;
                volume = volume * vol4;
            } else if (vol4 !== '') {
                volume = vol1 * vol2 * vol3 * vol4;
            }
        }

        if (pajakx === 10) {
            var hitung = (harga * volume * (110) / 100);
        } else if (pajakx === 0) {
            var hitung = (harga * volume * 1);
        }

        $('total').value = hitung;

    }
</script>