<?php

/**
 * waitinglist_pu actions.
 *
 * @package    budgeting
 * @subpackage waitinglist_pu
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class waitinglist_puActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

    public function executeDetailKomponenPenyusun() {
        $komponen_id = $this->getRequestParameter('komponen');
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $dapat = KomponenPeer::doSelectOne($c);
        echo $dapat->getKomponenHarga() . '|' . $dapat->getSatuan();
        exit;
    }

    public function executeCekUserPU() {
        $json_coba = '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"LineString","coordinates":[[-7.257817077517828,112.74664252996445],[-7.2581789350764,112.74808019399643]]},"properties":{"jarak":163.78835122092661}}],"keterangan":"","keteranganalamat":""}';
        echo 'encode ' . json_encode($json_coba) . '<br/>';
        echo 'decode ' . json_decode($json_coba);
        exit();
    }

//    ticket #32 - waiting list PU posisi antrian
//    created at 23juni2015
    public function executeWaitinglist() {
        $d = new Criteria();
        $user = $this->getUser();
        if ($user->getNamaLogin() == 'perancangan_2600' || $user->getNamaLogin() == 'pematusan_2600' || $user->getNamaLogin() == 'jalan_2600' || $user->getNamaLogin() == 'permukiman_2300' || $user->getNamaLogin() == 'sarpras_2800') {
            $nama = $user->getNamaUser();
            $d = new Criteria();
            $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
            $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
            if ($rs_unitkerja) {
                $nama = $rs_unitkerja->getUnitId();
            }
            $unit_id = $nama;
            $this->unit_id = $unit_id;
            $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        }
        //$d->addOr(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
        //$d->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
        //$d->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
        $cton1 = $d->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
        $cton2 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
        $cton3 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
        $cton1->addOr($cton2);
        $cton1->addOr($cton3);
        $d->addOr($cton1);

        $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
        $rs_masterkegiatan = DinasMasterKegiatanPeer::doSelect($d);
        $this->rs_masterkegiatan = $rs_masterkegiatan;
    }

//    created at 19 Februari 2016
    public function executeWaitinglistrka() {
//        $unit_id = '2600';

        $d = new Criteria();
        $user = $this->getUser();
        if ($user->getNamaLogin() == 'perancangan_2600' || $user->getNamaLogin() == 'pematusan_2600' || $user->getNamaLogin() == 'jalan_2600' || $user->getNamaLogin() == 'permukiman_2300' || $user->getNamaLogin() == 'sarpras_2800') {
            $nama = $user->getNamaUser();
            $d = new Criteria();
            $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
            $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
            if ($rs_unitkerja) {
                $nama = $rs_unitkerja->getUnitId();
            }
            $unit_id = $nama;
            $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
        }
//        $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
//        $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//        $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
        $cton1 = $d->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
        $cton2 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
        $cton3 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
        $cton1->addOr($cton2);
        $cton1->addOr($cton3);
        $d->addOr($cton1);

        $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
        $rs_masterkegiatan = DinasMasterKegiatanPeer::doSelect($d);
        $this->rs_masterkegiatan = $rs_masterkegiatan;
    }

//    created at 22 Februari 2016
    public function executeProseskembalikan() {
        $sekarang = date('Y-m-d H:i:s');
        $detail_no = $this->getRequestParameter('id');
        $unit_id = $this->getRequestParameter('unitid');
        $kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        if (is_null($detail_no) || is_null($unit_id) || is_null($kode_kegiatan) || ($this->getRequest()->hasParameter('key') != md5('kembali_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
            return $this->redirect("waitinglist_pu/waitinglistrka");
        } else {
            //ambil rincian detail yang dikembalikan
            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_rincian_detail->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
            $c_rincian_detail->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $rd = DinasRincianDetailPeer::doSelectOne($c_rincian_detail);
            $kode_rka = $rd->getUnitId() . '.' . $rd->getKegiatanCode() . '.' . $rd->getDetailNo();
            $komponen_name = $rd->getKomponenName();
            $detail_name = $rd->getDetailName();
            //ambil waitinglist pu
            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::KODE_RKA, $kode_rka);
            $waiting = WaitingListPUPeer::doSelectOne($c_waiting);
            $id_waiting = $waiting->getIdWaiting();
            $kode_rka_waiting = $waiting->getUnitId() . '.' . $waiting->getKegiatanCode() . '.' . $waiting->getIdWaiting();
            $prioritas = $waiting->getPrioritas();

            //ambil geojsonlokasi rev1
            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiRev1Peer::DETAIL_NO, $detail_no);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::UNIT_ID, $unit_id);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiRev1Peer::doSelect($c_cari_geojson);

            //ambil geojsonlokasi waiting
            $c_hapus_geojson = new Criteria();
            $c_hapus_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, 'XXX' . $unit_id);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $hapus_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_hapus_geojson);

            //ambil nama skpd
            $c_unit = new Criteria();
            $c_unit->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

            //cek lelang+eproject+edelivery
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiRealisasi = 0;
                $totVolumeRealisasi = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $nilai = $rd->getNilaiAnggaran();
                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;
                $rd2 = new DinasRincianDetail();
                $totNilaiSwakelola = $rd2->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiKontrak = $rd2->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiRealisasi = $rd2->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                $totVolumeRealisasi = $rd2->getCekVolumeRealisasi($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiAlokasi = $rd2->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                    //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                    //$totNilaiKontrak = $totNilaiKontrak + 10;
                }
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {

                    $totNilaiHps = $rd2->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    $lelang = $rd2->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $nilai);
                    $ceklelangselesaitidakaturanpembayaran = $rd2->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                }

                if ($nilai < $totNilaiKontrak || $nilai < $totNilaiSwakelola) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        return $this->redirect("waitinglist_pu/waitinglistrka");
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                        return $this->redirect("waitinglist_pu/waitinglistrka");
                    }
                } else if ($nilai < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect("waitinglist_pu/waitinglistrka");
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    return $this->redirect("waitinglist_pu/waitinglistrka");
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                    return $this->redirect("waitinglist_pu/waitinglistrka");
                } else if ($nilai < $totNilaiRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiRealisasi, 0, ',', '.'));
                    return $this->redirect("waitinglist_pu/waitinglistrka");
                } else if ($rd->getVolume() < $totVolumeRealisasi) {
                    $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, dengan volume ' . number_format($totVolumeRealisasi, 0, ',', '.'));
                    return $this->redirect("waitinglist_pu/waitinglistrka");
                }
            }

            //begin transaction
            $con = Propel::getConnection();
            $con->begin();
            try {
                //delete lokasi waiting lama
                $c_hapus_lokasi = new Criteria();
                $c_hapus_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_waiting);
                $lokasi_hapus = HistoryPekerjaanV2Peer::doSelect($c_hapus_lokasi);
                foreach ($lokasi_hapus as $lokasi_value) {
                    $lokasi_value->setStatusHapus(true);
                    $lokasi_value->save();
                }

                //delete geojsonlokasi waiting
                foreach ($hapus_geojson as $value_geojson) {
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //ambil lokasi rincian detail di history pekerjaan V2
                $c_buat_lokasi = new Criteria();
                $c_buat_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $c_buat_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                $lokasi_lama = HistoryPekerjaanV2Peer::doSelect($c_buat_lokasi);

                foreach ($lokasi_lama as $dapat_lokasi_lama) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                    $gang_lama = $dapat_lokasi_lama->getGang();
                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                    $rw_lama = $dapat_lokasi_lama->getRw();
                    $rt_lama = $dapat_lokasi_lama->getRt();
                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                    $tempat_lama = $dapat_lokasi_lama->getTempat();
                    if ($jalan_lama <> '') {
                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                    }
                    if ($tempat_lama <> '') {
                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                    }
                    if ($gang_lama <> '') {
                        $gang_fix = $gang_lama . ' ';
                    }
                    if ($nomor_lama <> '') {
                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                    }
                    if ($rw_lama <> '') {
                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                    }
                    if ($rt_lama <> '') {
                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                    }
                    if ($keterangan_lama <> '') {
                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                    }
                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                    //insert lokasi waiting
                    $komponen_lokasi_fix = $rd->getKomponenName() . ' ' . $rd->getDetailName();
                    $kecamatan_lokasi_fix = $rd->getLokasiKecamatan();
                    $kelurahan_lokasi_fix = $rd->getLokasiKelurahan();
                    $lokasi_per_titik_fix = $lokasi_baru;

                    $c_insert_gis = new HistoryPekerjaanV2();
                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                    $c_insert_gis->setKodeRka($kode_rka_waiting);
                    $c_insert_gis->setStatusHapus(FALSE);
                    $c_insert_gis->setJalan(strtoupper($jalan_lama));
                    $c_insert_gis->setGang(strtoupper($gang_lama));
                    $c_insert_gis->setNomor(strtoupper($nomor_lama));
                    $c_insert_gis->setRw(strtoupper($rw_lama));
                    $c_insert_gis->setRt(strtoupper($rt_lama));
                    $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                    $c_insert_gis->setTempat(strtoupper($tempat_lama));
                    $c_insert_gis->setKomponen($komponen_lokasi_fix);
                    $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                    $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                    $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                    $c_insert_gis->save();
                    $dapat_lokasi_lama->setStatusHapus(true);
                    $dapat_lokasi_lama->save();
                }

                foreach ($dapat_geojson as $value_geojson) {

                    //insert geojsonlokasi waiting
                    $geojson_baru = new GeojsonlokasiWaitinglist();
                    $geojson_baru->setUnitId('XXX' . $unit_id);
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setIdWaiting($id_waiting);
                    $geojson_baru->setSatuan($rd->getSatuan());
                    $geojson_baru->setVolume($rd->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd->getKomponenName() . ' ' . $rd->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->save();

                    //delete geojsonlokasi rev1
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //update prioritas
                $total_aktif = 0;
                $query = "select max(prioritas) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }
                for ($index = $prioritas; $index <= $total_aktif; $index++) {
                    $index_tambah_satu = $index + 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, 'XXX' . $unit_id);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_WAITING, 0);
                    if ($waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas)) {
                        $waiting_prio->setPrioritas($index_tambah_satu);
                        $waiting_prio->save();
                    }
                }

                //update waitinglist pu
                $waiting->setSubtitle($rd->getSubtitle());
                $waiting->setKomponenId($rd->getKomponenId());
                $waiting->setKomponenName($rd->getKomponenName());
                $waiting->setKomponenRekening($rd->getRekeningCode());
                $waiting->setKomponenLokasi('(' . $lokasi_baru . ')');
                $waiting->setKomponenHargaAwal($rd->getKomponenHargaAwal());
                $waiting->setPajak($rd->getPajak());
                $waiting->setKomponenSatuan($rd->getSatuan());
                $waiting->setKoefisien($rd->getKeteranganKoefisien());
                $waiting->setVolume($rd->getVolume());
                $waiting->setTahunInput(sfConfig::get('app_tahun_default'));
                $waiting->setUpdatedAt($sekarang);
                $waiting->setKecamatan($rd->getLokasiKecamatan());
                $waiting->setKelurahan($rd->getLokasiKelurahan());
                $waiting->setIsMusrenbang($rd->getIsMusrenbang());
                $waiting->setNilaiAnggaran($rd->getNilaiAnggaran());
                $waiting->setStatusHapus(false);
                $waiting->setStatusWaiting(0);
                $waiting->setKodeRka(null);
                $waiting->save();

                //update rincian detail
                $rd->setStatusHapus(true);
                $rd->save();

                $con->commit();
                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' kembali ke Waiting List');
                return $this->redirect("waitinglist_pu/waitinglistrka");
            } catch (Exception $ex) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $ex->getMessage());
                return $this->redirect("waitinglist_pu/waitinglistrka");
            }
        }
    }

//    ticket #32 - waiting list PU posisi antrian    
//    ticket #33 - view insert waiting list PU    
//    created at 24juni2015    
    public function executeWaitingbaru() {
        if ($this->getRequestParameter('baru') == md5('terbaru')) {
            $user = $this->getUser();

            $lokasiSession = $user->getAttribute('nama', '', 'lokasi');
            $this->lokasiSession = $lokasiSession;

            $user->setAttribute('nama', '', 'lokasi');
            $user->setAttribute('nama', '', 'lokasi_baru');

            $simbadaSession = $user->getAttribute('nama', '', 'simbada');
            $this->simbadaSession = $simbadaSession;

            $user->setAttribute('nama', '', 'simbada');
            $user->setAttribute('nama', '', 'simbada_baru');

            $user->removeCredential('lokasi');
            $user->removeCredential('simbada');

            //$unit_id = '2600';
            $komponen_id = trim($this->getRequestParameter('komponen'));
            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id, Criteria::ILIKE);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {
                $this->rs_komponen = $rs_komponen;
            }

            $d = new Criteria();
            $user = $this->getUser();
            if ($user->getNamaLogin() == 'perancangan_2600' || $user->getNamaLogin() == 'pematusan_2600' || $user->getNamaLogin() == 'jalan_2600' || $user->getNamaLogin() == 'permukiman_2300' || $user->getNamaLogin() == 'sarpras_2800') {
                $nama = $user->getNamaUser();
                $d = new Criteria();
                $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
                $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
                if ($rs_unitkerja) {
                    $nama = $rs_unitkerja->getUnitId();
                }
                $unit_id = $nama;
                $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            //$d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $cton1 = $d->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
            $cton2 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
            $cton3 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $cton1->addOr($cton2);
            $cton1->addOr($cton3);
            $d->addOr($cton1);
            $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
            $rs_masterkegiatan = DinasMasterKegiatanPeer::doSelect($d);
            $this->rs_masterkegiatan = $rs_masterkegiatan;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $g = new Criteria();
            $g->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $g->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $g->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($g);

            $kec = new Criteria();
            $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
            $rs_kec = KecamatanPeer::doSelect($kec);
            $this->nama_kecamatan_kel = $rs_kec;
        }
    }

//    ticket #33 - view insert waiting list PU    
//    ticket #33 - memilih komponen Fisik - view insert waiting list PU    
//    created at 24juni2015        
    public function executeWaitingcari() {
        $this->filters = $this->getRequestParameter('filters');
        if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
            $cari = $this->filters['nama_komponen'];
        }
        $this->cari = $cari;
    }

//    ticket #33 - memilih komponen Fisik - view insert waiting list PU    
//    ticket #33 - javascript memilih kegiatan muncul subtitle
//    created at 24juni2015        
    public function executePilihkegiatan() {
        if ($this->getRequestParameter('b') != '') {
            $unitid = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('b');

            $queryku = "select subtitle from " . sfConfig::get('app_default_schema') . ".dinas_subtitle_indikator 
                        where kegiatan_code='$kode_kegiatan' and unit_id='$unitid' order by subtitle";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('subtitle') != '') {
                    $arr_tampung[$rcsb->getString('subtitle')] = $rcsb->getString('subtitle');
                }
            }
            $this->isi_baru = $arr_tampung;
        }
    }

//    ticket #33 - javascript memilih kegiatan muncul subtitle
//    ticket #33 - proses insert waiting list PU
//    created at 24juni2015        
    public function executeBaruKegiatan() {
        if ($this->getRequestParameter('simpan') == 'simpan') {

            $sekarang = date('Y-m-d H:i:s');

            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kode_kegiatan');
            $subtitle = $this->getRequestParameter('subtitle');
            $tipe = $this->getRequestParameter('tipe');
            $rekening = $this->getRequestParameter('rekening');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('id');

            $jasmas = $this->getRequestParameter('jasmas');
            $kecamatan = $this->getRequestParameter('kecamatan');
            $kelurahan = $this->getRequestParameter('kelurahan');

            if ((($kegiatan_code == '1.03.28.0005' && $subtitle == 'Rehabilitasi Saluran Drainase Lingkungan') || ($kegiatan_code == '1.04.22.0001' && $subtitle == 'Rehabilitasi/Pemeliharaan Jalan Lingkungan')) && $jasmas == '') {
                $this->setFlash('gagal', 'Jasmas harus diisi');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }

            $is_musrenbang = 'FALSE';
            if ($this->getRequestParameter('musrenbang') == NULL) {
                $is_musrenbang = 'FALSE';
            } else {
                $is_musrenbang = 'TRUE';
            }

            $nilai_ee = $this->getRequestParameter('nilai_ee');
            if ($nilai_ee == NULL) {
                $nilai_ee = 0;
            }

            $keterangan = $this->getRequestParameter('keterangan');

            if ($keterangan == '' && $nilai_ee == 0) {
                $this->setFlash('gagal', 'Isi keterangan apabila nilai EE tidak ada');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }


            if (!$this->getRequestParameter('kode_kegiatan')) {
                $this->setFlash('gagal', 'Kode Kegiatan Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }

            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }

            if (!$this->getRequestParameter('lokasi_lama') && !$this->getRequestParameter('lokasi_jalan')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            } else {
//lokasi
                $keisi = 0;
                $lokasi_baru = '';

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            if ($keisi == 0) {
                                $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                $keisi++;
                            } else {
                                $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                $keisi++;
                            }
                        }
                    }
                }

//                    buat baru

                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
                if ($keisi == 0) {
                    $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                    return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
                }
//lokasi    
            }

            if ((!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan'))) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            } else {
                $lokasi_kec = $this->getRequestParameter('kecamatan');
                $lokasi_kel = $this->getRequestParameter('kelurahan');
                $kec = new Criteria();
                $kec->add(KecamatanPeer::ID, $lokasi_kec);
                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                $rs_kec = KecamatanPeer::doSelectOne($kec);
                if ($rs_kec) {
                    $kecamatan = $rs_kec->getNama();
                }

                $kel = new Criteria();
                $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                if ($rs_kel) {
                    $kelurahan = $rs_kel->getNamaKelurahan();
                } else {
                    $kecamatan = '';
                    $kelurahan = '';
                }
            }

            $vol1 = $this->getRequestParameter('vol1');
            $vol2 = $this->getRequestParameter('vol2');
            $vol3 = $this->getRequestParameter('vol3');
            $vol4 = $this->getRequestParameter('vol4');

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {
                $komponen_harga = $rs_komponen->getKomponenHargaBulat();
                $komponen_name = $rs_komponen->getKomponenName();
                $komponen_satuan = $rs_komponen->getSatuan();
            }

            $c_cari_lokasi = new Criteria();
            $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $lokasi_baru);
            $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::KOMPONEN, $komponen_name . ' (' . $lokasi_baru . ')');
            $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
            if ($dapat_lokasi_lama) {
                $this->setFlash('gagal', 'Untuk komponen Fisik dengan nama komponen dan lokasi ini telah dimasukkan di aplikasi pada tahun yang sama');
                return $this->redirect("waitinglist_pu/waitingbaru?rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }

            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $query_max_prioritas = "select max(prioritas) as max_prio "
                    . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu  
                        where kegiatan_code='$kegiatan_code' and status_hapus = false and status_waiting = 0 ";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_max_prioritas);
            $rs_max = $stmt->executeQuery();
            while ($rs_max->next()) {
                $maxx = $rs_max->getFloat('max_prio');
            }
            $maxx++;

            $query_max_waitinglist = "select max(id_waiting) as max_id "
                    . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu ";
            $stmt_waiting = $con->prepareStatement($query_max_waitinglist);
            $rs_max_waiting = $stmt_waiting->executeQuery();
            while ($rs_max_waiting->next()) {
                $max_id = $rs_max_waiting->getFloat('max_id');
                if ($max_id == 0) {
                    $max_id = 1;
                }
            }
            $max_id++;

            $new_insert_waitinglist = new WaitingListPU();
            $new_insert_waitinglist->setUnitId('XXX' . $unit_id);
            $new_insert_waitinglist->setKegiatanCode($kegiatan_code);
            $new_insert_waitinglist->setSubtitle($subtitle);
            $new_insert_waitinglist->setKomponenId($komponen_id);
            $new_insert_waitinglist->setKomponenName($komponen_name);
            $new_insert_waitinglist->setKomponenLokasi('(' . $lokasi_baru . ')');
            $new_insert_waitinglist->setKomponenHargaAwal($komponen_harga);
            $new_insert_waitinglist->setPajak($pajak);
            $new_insert_waitinglist->setKomponenSatuan($komponen_satuan);
            $new_insert_waitinglist->setKoefisien($keterangan_koefisien);
            $new_insert_waitinglist->setVolume($volume);
            $new_insert_waitinglist->setTahunInput(sfConfig::get('app_tahun_default'));
            $new_insert_waitinglist->setCreatedAt($sekarang);
            $new_insert_waitinglist->setUpdatedAt($sekarang);
            $new_insert_waitinglist->setNilaiEe($nilai_ee);
            $new_insert_waitinglist->setKeterangan($keterangan);
            $new_insert_waitinglist->setPrioritas($maxx);
            $new_insert_waitinglist->setKecamatan($kecamatan);
            $new_insert_waitinglist->setKelurahan($kelurahan);
            if ($is_musrenbang == 'TRUE') {
                $new_insert_waitinglist->setIsMusrenbang(TRUE);
            } else {
                $new_insert_waitinglist->setIsMusrenbang(FALSE);
            }
            $new_insert_waitinglist->setKodeJasmas($jasmas);
            $new_insert_waitinglist->setKomponenRekening($rekening);
            $new_insert_waitinglist->save();

            $id_new_insert_waitinglist = $new_insert_waitinglist->getIdWaiting();
            $c_update_waitinglist = new Criteria();
            $c_update_waitinglist->add(WaitingListPUPeer::ID_WAITING, $id_new_insert_waitinglist);
            $dapat_waitinglist = WaitingListPUPeer::doSelectOne($c_update_waitinglist);

            $dapat_waitinglist->setNilaiAnggaranSemula($dapat_waitinglist->getNilaiAnggaran());
            $dapat_waitinglist->save();

            budgetLogger::log('menambah Data Waiting List untuk kegiatan ' . $kegiatan_code . ' Komponen ' . $komponen_name . ' (' . $lokasi_baru . ') ');

            $kode_rka_fix = 'XXX' . $unit_id . '.' . $kegiatan_code . '.' . $max_id;

            $c_cari_history = new Criteria();
            $c_cari_history->addAnd(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
            $c_cari_history->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            $dapat_history = HistoryPekerjaanV2Peer::doSelect($c_cari_history);
            if ($dapat_history) {
                foreach ($dapat_history as $value_history) {
                    $id_history = $value_history->getIdHistory();

                    $c_cari_hapus_history = new Criteria();
                    $c_cari_hapus_history->addAnd(HistoryPekerjaanV2Peer::ID_HISTORY, $id_history);
                    $dapat_history_hapus = HistoryPekerjaanV2Peer::doSelectOne($c_cari_hapus_history);
                    if ($dapat_history_hapus) {
                        $dapat_history_hapus->delete();
                    }
                }
            }

            $keisi = 0;

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rka_lokasi = 'XXX' . $unit_id . '.' . $kegiatan_code . '.' . $max_id;
                        $komponen_lokasi = $komponen_name . ' (' . $lokasi_baru . ')';
                        $kecamatan_lokasi = $kecamatan;
                        $kelurahan_lokasi = $kelurahan;
                        $lokasi_per_titik = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper($jalan_lama));
                        $c_insert_gis->setGang(strtoupper($gang_lama));
                        $c_insert_gis->setNomor(strtoupper($nomor_lama));
                        $c_insert_gis->setRw(strtoupper($rw_lama));
                        $c_insert_gis->setRt(strtoupper($rt_lama));
                        $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                        $c_insert_gis->setTempat(strtoupper($tempat_lama));
                        $c_insert_gis->setKomponen($komponen_lokasi);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                        $c_insert_gis->setLokasi($lokasi_per_titik);
                        $c_insert_gis->save();
                    }
                }
            }

//                    buat baru
            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }


                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                    $rka_lokasi = 'XXX' . $unit_id . '.' . $kegiatan_code . '.' . $max_id;
                    $komponen_lokasi = $komponen_name . ' (' . $lokasi_baru . ')';
                    $kecamatan_lokasi = $kecamatan;
                    $kelurahan_lokasi = $kelurahan;
                    $lokasi_per_titik = $lokasi_baru;

                    $c_insert_gis = new HistoryPekerjaanV2();
                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                    $c_insert_gis->setKodeRka($rka_lokasi);
                    $c_insert_gis->setStatusHapus(FALSE);
                    $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                    $c_insert_gis->setGang(strtoupper($gang_fix));
                    $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                    $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                    $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                    $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                    $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                    $c_insert_gis->setKomponen($komponen_lokasi);
                    $c_insert_gis->setKecamatan($kecamatan_lokasi);
                    $c_insert_gis->setKelurahan($kelurahan_lokasi);
                    $c_insert_gis->setLokasi($lokasi_per_titik);
                    $c_insert_gis->save();
                }
            }

            $query2 = "select * from master_kelompok_gmap where '" . $komponen_id . "' ilike kode_kelompok||'%' LIMIT 1";
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            while ($rs2->next()) {
                $id_kelompok = $rs2->getString('id_kelompok');
            }

            if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                $id_kelompok = 19;

                if (in_array($komponen_satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                    $id_kelompok = 100;
                } elseif (in_array($komponen_satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                    $id_kelompok = 101;
                }
            }

            return $this->redirect(sfConfig::get('app_path_gmap') . 'insertBaru_waitinglist.php?unit_id=XXX' . $unit_id . '&kode_kegiatan=' . $kegiatan_code . '&id_waiting=' . $max_id . '&satuan=' . $komponen_satuan . '&volume=' . $volume . '&nilai_anggaran=' . 0 . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $this->getUser()->getNamaLogin() . '&lokasi_ke=1');
//              tambahan cari lokasi
        }//EOF simpan
    }

//    ticket #33 - proses insert waiting list PU
//    ticket #33 - javascript dropdown list data waiting list
//    created at 24juni2015        
    public function executeGetList() {
        if ($this->getRequestParameter('id')) {
            $kode_kegiatan = str_replace('_', '.', $this->getRequestParameter('id'));

            $c = new Criteria();
            $c->add(WaitingListPUPeer::STATUS_HAPUS, FALSE);
            $c->add(WaitingListPUPeer::STATUS_WAITING, 0);
            $c->add(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(WaitingListPUPeer::PRIORITAS);
            $rs_waiting = WaitingListPUPeer::doSelect($c);
            $this->rs_waiting = $rs_waiting;

            $this->id = $this->getRequestParameter('id');
            $this->kode_kegiatan = $kode_kegiatan;
            $this->setLayout('kosong');
        }
    }

//    created at 19 Februari 2016
    public function executeGetListRka() {
        if ($this->getRequestParameter('id')) {
            $kode_kegiatan = str_replace('_', '.', $this->getRequestParameter('id'));

            if (sfConfig::get('app_tahun_default') == '2016') {
                $subsql = " detail_no::text in (select substring(kode_rka from 19 for 3) from ebudget.waitinglist_pu where status_hapus=false and status_waiting=1 and kegiatan_code='$kode_kegiatan')";
            } else {
                $subsql = " detail_no::text in (select substring(kode_rka from 23 for 3) from ebudget.waitinglist_pu where status_hapus=false and status_waiting=1 and kegiatan_code='$kode_kegiatan')";
            }
            $c = new Criteria();
            //$c->addAnd(DinasRincianDetailPeer::UNIT_ID, '2600');
            $cton1 = $c->getNewCriterion(DinasRincianDetailPeer::UNIT_ID, '2600');
            $cton2 = $c->getNewCriterion(DinasRincianDetailPeer::UNIT_ID, '2300');
            $cton1->addOr($cton2);
            $c->addOr($cton1);
            $c->addAnd(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
            $c->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAnd(DinasRincianDetailPeer::DETAIL_NO, $subsql, Criteria::CUSTOM);
            $c->addAscendingOrderByColumn(DinasRincianDetailPeer::SUBTITLE);
            $rs_rka = DinasRincianDetailPeer::doSelect($c);
            $this->rs_waiting = $rs_rka;

            $this->id = $this->getRequestParameter('id');
            $this->kode_kegiatan = $kode_kegiatan;
            $this->setLayout('kosong');
        }
    }

//    ticket #33 - javascript dropdown list data waiting list
//    ticket #34 - proses set prioritas
//    created at 24juni2015        
    public function executeSetPrioritas() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $prioritas = $this->getRequestParameter('prioritas');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $id_waiting = $this->getRequestParameter('id_waiting');
            $prio_sekarang = $this->getRequestParameter('prio_sekarang');

            try {
                $c = new Criteria();
                $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                $waitinglist = WaitingListPUPeer::doSelectOne($c);

                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "set prioritas = " . $prio_sekarang . " "
                        . "where prioritas = " . $prioritas . " "
                        . "and kegiatan_code = '" . $kode_kegiatan . "' ";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "set prioritas = " . $prioritas . " "
                        . "where id_waiting = " . $id_waiting . " "
                        . "and kegiatan_code = '" . $kode_kegiatan . "' ";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Prioritas sudah berhasil dirubah');


                budgetLogger::log('Mengubah set Prioritas data Waiting List PU Kegiatan ' . $waitinglist->getKegiatanCode() . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $waitinglist->getKomponenLokasi() . '  id::' . $waitinglist->getIdWaiting());
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            }
            $this->redirect('waitinglist_pu/waitinglist');
        }
    }

//    ticket #34 - proses set prioritas
//    ticket #35 - view insert waiting list by upload
//    created at 24juni2015        
    public function executeWaitingupload() {
        
    }

//    ticket #35 - view insert waiting list by upload
//    ticket #35 - proses insert waiting list by upload
//    created at 24juni2015        
    public function executeProsesupload() {
        set_time_limit(0);

        $sekarang = date('Y-m-d H:i:s');
        try {


            $fileName = $this->getRequest()->getFileName('file');
            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/waiting_list_pu/' . $fileName);

            $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/waiting_list_pu/' . $fileName;
            if (!file_exists($file_path)) {
                exit("CEK FILE.\n");
            }
            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            $highC = $objWorksheet->getHighestColumn();

            $con = Propel::getConnection();
            $con->begin();


            $total_salah = 0;
            $string_salah = '';
            for ($row = 19; $row <= $highR; $row++) {

//            kode kegiatan
                $kegiatan_kotor = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());

                $array_kegiatan = explode('<=>', $kegiatan_kotor);
                if (count($array_kegiatan) <> 2) {
                    $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom kegiatan, baris ke ' . $row . '++';
                    $total_salah++;
                } else {
                    $kegiatan_code = $array_kegiatan[0];
                    $kegiatan_nama = $array_kegiatan[1];

                    $c_cek_kegiatan = new Criteria();
                    $cton1 = $c_cek_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
                    $cton2 = $c_cek_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
                    $cton3 = $c_cek_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
                    $cton1->addOr($cton2);
                    $cton1->addOr($cton3);
                    $c_cek_kegiatan->add($cton1);
//                    $c_cek_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
                    $c_cek_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
//                    $c_cek_kegiatan->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//                    $c_cek_kegiatan->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
                    $dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan);
                    if (!$dapat_kegiatan) {
                        $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom kegiatan, baris ke ' . $row . '++';
                        $total_salah++;
                    }
                }
//            kode kegiatan            
//            subtitle                        
                $subtitle_kotor = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());

                $array_subtitle = explode('<=>', $subtitle_kotor);
                if (count($array_subtitle) <> 2) {
                    $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom subtitle, baris ke ' . $row . '++';
                    $total_salah++;
                } else {
                    $kegiatan_code_subtitle = $array_subtitle[0];
                    $subtitle = $array_subtitle[1];

                    if ($kegiatan_code <> $kegiatan_code_subtitle) {
                        $string_salah = $string_salah . 'salah karena pemilihan kode kegiatan tidak sama antara kolom Kegiatan & Subtitle, baris ke ' . $row . '++';
                        $total_salah++;
                    } else {
                        $c_cek_subtitle = new Criteria();
//                        $c_cek_subtitle->add(SubtitleIndikatorPeer::UNIT_ID, '2600');
                        $cton1 = $c_cek_subtitle->getNewCriterion(DinasSubtitleIndikatorPeer::UNIT_ID, '2300');
                        $cton2 = $c_cek_subtitle->getNewCriterion(DinasSubtitleIndikatorPeer::UNIT_ID, '2600');
                        $cton1->addOr($cton2);
                        $c_cek_subtitle->add($cton1);
                        $c_cek_subtitle->addAnd(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                        $c_cek_subtitle->addAnd(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                        $dapat_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c_cek_subtitle);
                        if (!$dapat_subtitle) {
                            $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom subtitle, baris ke ' . $row . '++';
                            $total_salah++;
                        }
                    }
                }
//            subtitle
//            komponen            
                $komponen_kotor = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());

                $array_komponen = explode('<=>', $komponen_kotor);
                if (count($array_komponen) <> 2) {
                    $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom komponen, baris ke ' . $row . '++';
                    $total_salah++;
                } else {
                    $komponen_id = $array_komponen[0];
                    $komponen_name_isian = $array_komponen[1];

                    $c_cek_komponen = new Criteria();
                    $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                    $dapat_komponen = KomponenPeer::doSelectOne($c_cek_komponen);
                    if (!$dapat_komponen) {
                        $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom komponen, baris ke ' . $row . '++';
                        $total_salah++;
                    }
                }
//            komponen                        
//            lokasi            
                $nama_jalan = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());

//            salah satu
                $nama_gang = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $nama_kavling = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                $nama_blok = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());

                $ada_gang_kavling_blok = 0;
                if ($nama_gang <> '') {
                    $ada_gang_kavling_blok++;
                }
                if ($nama_kavling <> '') {
                    $ada_gang_kavling_blok++;
                }
                if ($nama_blok <> '') {
                    $ada_gang_kavling_blok++;
                }
                if ($ada_gang_kavling_blok > 1) {
                    $string_salah = $string_salah . 'salah karena hanya diperbolehkan mengisi salah satu diantara kolom Gang, Kavling, atau Blok, baris ke ' . $row . '++';
                    $total_salah++;
                }

//            salah satu

                $nomor_rumah = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                $nomor_RT = trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
                $nomor_RW = trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());

                $nama_tempat = trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue());
                $keterangan_lokasi = trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue());

                if ($nama_jalan == '' && $nama_tempat == '') {
                    $string_salah = $string_salah . 'salah karena kolom Nama Jalan dan Nama Tempat kosong, baris ke ' . $row . '++';
                    $total_salah++;
                }

//            lokasi            
//pagu & nilai ee            
                $pagu = trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue());
                $nilai_ee = trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue());

                $array_pagu_koma = explode(',', $pagu);
                $array_pagu_titik = explode('.', $pagu);

                $array_nilaiee_koma = explode(',', $nilai_ee);
                $array_nilaiee_titik = explode('.', $nilai_ee);

                if ($pagu == 0) {
                    $string_salah = $string_salah . 'salah karena pengisian pagu = 0 , baris ke ' . $row . '++';
                    $total_salah++;
                }

                if (count($array_pagu_koma) <> 1 || count($array_pagu_titik) <> 1 || count($array_nilaiee_koma) <> 1 || count($array_nilaiee_titik) <> 1) {
                    $string_salah = $string_salah . 'salah karena pengisian pagu/nilai ee tidak bulat menggunakan tanda koma atau tanda titik, baris ke ' . $row . '++';
                    $total_salah++;
                }

//pagu & nilai ee            
//            lokasi            
                $keterangan = trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue());

                if ($keterangan == '' && ($nilai_ee == 0 || $nilai_ee == '')) {
                    $string_salah = $string_salah . 'silahkan mengisi keterangan apabila belum terdapat nilai EE, baris ke ' . $row . '++';
                    $total_salah++;
                }


                $kecamatan_kelurahan_kotor = trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue());

                $array_kecamatan_kelurahan = explode('<=>', $kecamatan_kelurahan_kotor);
                if (count($array_kecamatan_kelurahan) <> 2) {
                    $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom kecamatan-kelurahan, baris ke ' . $row . '++';
                    $total_salah++;
                } else {
                    $kecamatan_bersih = $array_kecamatan_kelurahan[0];
                    $kelurahan_bersih = $array_kecamatan_kelurahan[1];

                    $query_cek_kecamatan_kelurahan = "select count(*) as total_ada from kelurahan_kecamatan "
                            . "where nama_kecamatan = '$kecamatan_bersih' and nama_kelurahan = '$kelurahan_bersih' ";
                    $stmt_cek_kecamatan_kelurahan = $con->prepareStatement($query_cek_kecamatan_kelurahan);
                    $rs_cek_kecamatan_kelurahan = $stmt_cek_kecamatan_kelurahan->executeQuery();
                    while ($rs_cek_kecamatan_kelurahan->next()) {
                        $ada_kecamatan_kelurahan = $rs_cek_kecamatan_kelurahan->getString('total_ada');
                    }
                    if ($ada_kecamatan_kelurahan == 0) {
                        $string_salah = $string_salah . 'salah karena tidak menggunakan dropdown list dalam kolom kecamatan-kelurahan, baris ke ' . $row . '++';
                        $total_salah++;
                    }
                }
//            lokasi            
//                    penamaan lokasi
                $lokasi_baru = '';
                $jalan_fix = '';
                $gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($nama_jalan) <> '' || trim($nama_tempat) <> '') {

                    if (trim($nama_jalan) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($nama_jalan)) . ' ';
                    }

                    if (trim($nama_tempat) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($nama_tempat)) . ') ';
                    }

                    if (trim($nama_gang <> '')) {
                        $gang_fix = 'GG. ' . strtoupper(trim($nama_gang)) . ' ';
                    } else if (trim($nama_kavling <> '')) {
                        $gang_fix = 'KAV. ' . strtoupper(trim($nama_kavling)) . ' ';
                    } else if (trim($nama_blok <> '')) {
                        $gang_fix = 'BLOK. ' . strtoupper(trim($nama_blok)) . ' ';
                    }

                    if (trim($nomor_rumah) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($nomor_rumah)) . ' ';
                    }

                    if (trim($nomor_RW) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($nomor_RW)) . ' ';
                    }

                    if (trim($nomor_RT) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($nomor_RT)) . ' ';
                    }

                    if (trim($keterangan_lokasi) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($keterangan_lokasi)) . ' ';
                    }

                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                }
//                    penamaan lokasi     

                $c_cek_komponen = new Criteria();
                $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $dapat_komponen = KomponenPeer::doSelectOne($c_cek_komponen);

                if (!$dapat_komponen) {
                    $string_salah = $string_salah . 'salah karena komponen tidak ditemukan untuk id ' . $komponen_id . ', baris ke ' . $row . '++';
                    $total_salah++;
                } else {
                    $komponen_name = $dapat_komponen->getKomponenName();

                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $lokasi_baru);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::KOMPONEN, $komponen_name . ' (' . $lokasi_baru . ')');
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {
                        $string_salah = $string_salah . 'salah karena lokasi sudah pernah diinputkan dalam Aplikasi pada Tahun yang sama (' . sfConfig::get('app_tahun_default') . '), baris ke ' . $row . '++';
                        $total_salah++;
                    }
                }
            }
        } catch (Exception $exc) {
            $this->setFlash('gagal', 'Gagal Karena file tidak ada atau tidak sesuai ketentuan');
            return $this->redirect("waitinglist_pu/waitingupload");
        }
        if ($total_salah > 0) {
            //                warning kalo salah satu salah
            unlink($file_path);
            $this->string_salah = $string_salah;
        } else {
            try {
                for ($row = 19; $row <= $highR; $row++) {

                    //            kode kegiatan
                    $kegiatan_kotor = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());

                    $array_kegiatan = explode('<=>', $kegiatan_kotor);

                    $kegiatan_code = $array_kegiatan[0];
                    $kegiatan_nama = $array_kegiatan[1];

                    $c_cek_kegiatan = new Criteria();
                    $cton1 = $c_cek_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
                    $cton2 = $c_cek_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
                    $cton3 = $c_cek_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
                    $cton1->addOr($cton2);
                    $cton1->addOr($cton3);
                    $c_cek_kegiatan->add($cton1);
//                    $c_cek_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
                    $c_cek_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
//                    $c_cek_kegiatan->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//                    $c_cek_kegiatan->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
                    $dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan);

                    $unit_id = $dapat_kegiatan->getUnitId();


//            kode kegiatan            
//            subtitle                        
                    $subtitle_kotor = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());

                    $array_subtitle = explode('<=>', $subtitle_kotor);
                    $kegiatan_code_subtitle = $array_subtitle[0];
                    $subtitle = $array_subtitle[1];

                    $c_cek_subtitle = new Criteria();
                    $c_cek_subtitle->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c_cek_subtitle->addAnd(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c_cek_subtitle->addAnd(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                    $dapat_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c_cek_subtitle);
//            subtitle
//            komponen            
                    $komponen_kotor = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());

                    $array_komponen = explode('<=>', $komponen_kotor);

                    $komponen_id = $array_komponen[0];
                    $komponen_name_isian = $array_komponen[1];

                    $c_cek_komponen = new Criteria();
                    $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                    $dapat_komponen = KomponenPeer::doSelectOne($c_cek_komponen);

                    $komponen_name = $dapat_komponen->getKomponenName();
                    $komponen_harga = $dapat_komponen->getKomponenHargaBulat();
                    $boolean_pajak = $dapat_komponen->getKomponenNonPajak();

                    $pajak = 0;
                    $pajak_baru = 1;

                    if ($boolean_pajak == FALSE) {
                        $pajak = 10;
                        $pajak_baru = 1.1;
                    } else {
                        $pajak = 0;
                        $pajak_baru = 1;
                    }
                    $komponen_satuan = $dapat_komponen->getSatuan();



//            komponen                        
//            lokasi            
                    $nama_jalan = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());

//            salah satu
                    $nama_gang = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                    $nama_kavling = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                    $nama_blok = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
//            salah satu

                    $nomor_rumah = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                    $nomor_RT = trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
                    $nomor_RW = trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());

                    $nama_tempat = trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue());
                    $keterangan_lokasi = trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue());

//            lokasi            
//pagu & nilai ee            
                    $pagu = trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue());
                    $nilai_ee = trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue());

//pagu & nilai ee            
//            lokasi            
                    $keterangan = trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue());


                    $kecamatan_kelurahan_kotor = trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue());

                    $array_kecamatan_kelurahan = explode('<=>', $kecamatan_kelurahan_kotor);

                    $kecamatan_bersih = $array_kecamatan_kelurahan[0];
                    $kelurahan_bersih = $array_kecamatan_kelurahan[1];
//            lokasi            

                    $volume = $pagu / ($pajak_baru * $komponen_harga);
                    $keterangan_koefisien = $volume . ' ' . $komponen_satuan;

                    $query_max_prioritas = "select max(prioritas) as max_prio "
                            . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu  "
                            . "where kegiatan_code = '$kegiatan_code' and status_hapus = false and status_waiting = 0 ";
                    $stmt = $con->prepareStatement($query_max_prioritas);
                    $rs_max = $stmt->executeQuery();
                    while ($rs_max->next()) {
                        $maxx = $rs_max->getFloat('max_prio');
                    }
                    $maxx++;

//                    penamaan lokasi
                    $lokasi_baru = '';
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($nama_jalan) <> '' || trim($nama_tempat) <> '') {

                        if (trim($nama_jalan) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($nama_jalan)) . ' ';
                        }

                        if (trim($nama_tempat) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($nama_tempat)) . ') ';
                        }

                        if (trim($nama_gang <> '')) {
                            $gang_fix = 'GG. ' . strtoupper(trim($nama_gang)) . ' ';
                        } else if (trim($nama_kavling <> '')) {
                            $gang_fix = 'KAV. ' . strtoupper(trim($nama_kavling)) . ' ';
                        } else if (trim($nama_blok <> '')) {
                            $gang_fix = 'BLOK. ' . strtoupper(trim($nama_blok)) . ' ';
                        }

                        if (trim($nomor_rumah) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($nomor_rumah)) . ' ';
                        }

                        if (trim($nomor_RW) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($nomor_RW)) . ' ';
                        }

                        if (trim($nomor_RT) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($nomor_RT)) . ' ';
                        }

                        if (trim($keterangan_lokasi) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($keterangan_lokasi)) . ' ';
                        }

                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                    }
//                    penamaan lokasi                    

                    $query_max_waitinglist = "select max(id_waiting) as max_id "
                            . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu ";
                    $stmt_waiting = $con->prepareStatement($query_max_waitinglist);
                    $rs_max_waiting = $stmt_waiting->executeQuery();
                    while ($rs_max_waiting->next()) {
                        $max_id = $rs_max_waiting->getFloat('max_id');
                        if ($max_id == 0) {
                            $max_id = 1;
                        }
                    }
                    $max_id++;

                    $new_insert_waitinglist = new WaitingListPU();
                    $new_insert_waitinglist->setUnitId('XXX' . $unit_id);
                    $new_insert_waitinglist->setKegiatanCode($kegiatan_code);
                    $new_insert_waitinglist->setSubtitle($subtitle);
                    $new_insert_waitinglist->setKomponenId($komponen_id);
                    $new_insert_waitinglist->setKomponenName($komponen_name);
                    $new_insert_waitinglist->setKomponenLokasi('(' . $lokasi_baru . ')');
                    $new_insert_waitinglist->setKomponenHargaAwal($komponen_harga);
                    $new_insert_waitinglist->setPajak($pajak);
                    $new_insert_waitinglist->setKomponenSatuan($komponen_satuan);
                    $new_insert_waitinglist->setKoefisien($keterangan_koefisien);
                    $new_insert_waitinglist->setVolume($volume);
                    $new_insert_waitinglist->setTahunInput(sfConfig::get('app_tahun_default'));
                    $new_insert_waitinglist->setCreatedAt($sekarang);
                    $new_insert_waitinglist->setUpdatedAt($sekarang);
                    $new_insert_waitinglist->setNilaiEe($nilai_ee);
                    $new_insert_waitinglist->setKeterangan($keterangan);
                    $new_insert_waitinglist->setPrioritas($maxx);
                    $new_insert_waitinglist->setKecamatan($kecamatan_bersih);
                    $new_insert_waitinglist->setKelurahan($kelurahan_bersih);
                    $new_insert_waitinglist->setNilaiAnggaranSemula($pagu);
                    $new_insert_waitinglist->setIsMusrenbang(FALSE);
                    $new_insert_waitinglist->save();

                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $lokasi_baru);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {
                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rka_lokasi = 'XXX' . $unit_id . '.' . $kegiatan_code . '.' . $max_id;
                        $komponen_lokasi = $komponen_name . ' ' . '(' . $lokasi_baru . ')';
                        $kecamatan_lokasi = $kecamatan_bersih;
                        $kelurahan_lokasi = $kelurahan_bersih;
                        $lokasi_per_titik = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper($jalan_lama));
                        $c_insert_gis->setGang(strtoupper($gang_lama));
                        $c_insert_gis->setNomor(strtoupper($nomor_lama));
                        $c_insert_gis->setRw(strtoupper($rw_lama));
                        $c_insert_gis->setRt(strtoupper($rt_lama));
                        $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                        $c_insert_gis->setTempat(strtoupper($tempat_lama));
                        $c_insert_gis->setKomponen($komponen_lokasi);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                        $c_insert_gis->setLokasi($lokasi_per_titik);
                        $c_insert_gis->save();
                    } else {
                        $lokasi_baru = '';
                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';
                        if (trim($nama_jalan) <> '' || trim($nama_tempat) <> '') {

                            if (trim($nama_jalan) <> '') {
                                $jalan_fix = 'JL. ' . strtoupper(trim($nama_jalan)) . ' ';
                            }

                            if (trim($nama_tempat) <> '') {
                                $tempat_fix = '(' . strtoupper(trim($nama_tempat)) . ') ';
                            }

                            if (trim($nama_gang <> '')) {
                                $gang_fix = 'GG. ' . strtoupper(trim($nama_gang)) . ' ';
                            } else if (trim($nama_kavling <> '')) {
                                $gang_fix = 'KAV. ' . strtoupper(trim($nama_kavling)) . ' ';
                            } else if (trim($nama_blok <> '')) {
                                $gang_fix = 'BLOK. ' . strtoupper(trim($nama_blok)) . ' ';
                            }

                            if (trim($nomor_rumah) <> '') {
                                $nomor_fix = 'NO ' . strtoupper(trim($nomor_rumah)) . ' ';
                            }

                            if (trim($nomor_RW) <> '') {
                                $rw_fix = 'RW ' . strtoupper(trim($nomor_RW)) . ' ';
                            }

                            if (trim($nomor_RT) <> '') {
                                $rt_fix = 'RT ' . strtoupper(trim($nomor_RT)) . ' ';
                            }

                            if (trim($keterangan_lokasi) <> '') {
                                $keterangan_fix = '' . strtoupper(trim($keterangan_lokasi)) . ' ';
                            }

                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        }

                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rka_lokasi = 'XXX' . $unit_id . '.' . $kegiatan_code . '.' . $max_id;
                        $komponen_lokasi = $komponen_name . ' ' . '(' . $lokasi_baru . ')';
                        $kecamatan_lokasi = $kecamatan_bersih;
                        $kelurahan_lokasi = $kelurahan_bersih;
                        $lokasi_per_titik = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper(trim($nama_jalan)));
                        $c_insert_gis->setGang(strtoupper($gang_fix));
                        $c_insert_gis->setNomor(strtoupper(trim($nomor_rumah)));
                        $c_insert_gis->setRw(strtoupper(trim($nomor_RW)));
                        $c_insert_gis->setRt(strtoupper(trim($nomor_RT)));
                        $c_insert_gis->setKeterangan(strtoupper(trim($keterangan_lokasi)));
                        $c_insert_gis->setTempat(strtoupper(trim($nama_tempat)));
                        $c_insert_gis->setKomponen($komponen_lokasi);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                        $c_insert_gis->setLokasi($lokasi_per_titik);
                        $c_insert_gis->save();
                    }
                }

                budgetLogger::log('Menambahkan data Waiting List PU Kegiatan ' . $kegiatan_code . ' Komponen ' . $komponen_name . ' (' . $lokasi_baru . ')');

                $con->commit();
                $this->setFlash('berhasil', 'Berhasil dimasukkan');
                $this->redirect('waitinglist_pu/waitingupload');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
                $this->redirect('waitinglist_pu/waitingupload');
            }
        }
    }

//    ticket #35 - proses insert waiting list by upload


    public function executeWaitingedit() {
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            $id_waiting = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c);
            if ($waitinglist) {
                $this->waitinglist = $waitinglist;
                $tahunInput = $waitinglist->getTahunInput();
                $this->unit_id = str_replace('XXX', '', $waitinglist->getUnitId());
            }

            $d = new Criteria();
            $user = $this->getUser();
            if ($user->getNamaLogin() == 'perancangan_2600' || $user->getNamaLogin() == 'pematusan_2600' || $user->getNamaLogin() == 'jalan_2600' || $user->getNamaLogin() == 'permukiman_2300' || $user->getNamaLogin() == 'sarpras_2800') {
                $nama = $user->getNamaUser();
                $d = new Criteria();
                $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
                $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
                if ($rs_unitkerja) {
                    $nama = $rs_unitkerja->getUnitId();
                }
                $unit_id = $nama;
                $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $cton1 = $d->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
            $cton2 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
            $cton3 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $cton1->addOr($cton2);
            $cton1->addOr($cton3);
            $d->add($cton1);
//            $d->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
            $rs_masterkegiatan = DinasMasterKegiatanPeer::doSelect($d);
            $this->rs_masterkegiatan = $rs_masterkegiatan;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->add(KomponenRekeningPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $f->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::ALT_NOT_EQUAL);
            $f->addAscendingOrderByColumn(KomponenRekeningPeer::REKENING_CODE);
            $rs_rekening = KomponenRekeningPeer::doSelect($f);
            $this->rs_rekening = $rs_rekening;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $kode_rka = $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCode() . '.' . $waitinglist->getIdWaiting();

            $g = new Criteria();
            $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
            //$g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            if ($tahunInput) {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, $tahunInput);
            } else {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            }
            $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
            $geojson = HistoryPekerjaanV2Peer::doSelect($g);
            if ($geojson) {
                $this->rs_geojson = $geojson;
            }

            $h = new Criteria();
            $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $h->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $id_waiting = $this->getRequestParameter('id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c_waiting);

            $c_komponen = new Criteria();
            $c_komponen->add(KomponenPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $komponen = KomponenPeer::doSelectOne($c_komponen);
            if (!$komponen) {
                $this->setFlash('gagal', 'Komponen tidak ada. Silahkan mengganti komponen ini dengan mengambil komponen baru');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                if (round($waitinglist->getKomponenHargaAwal() - $komponen->getKomponenHargaBulat()) <> 0) {
                    $this->setFlash('gagal', 'Komponen terjadi perubahan harga. Silahkan mengganti komponen ini dengan mengambil komponen baru dan harga baru');
                    return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
                }
            }

            $is_musrenbang = 'FALSE';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $is_musrenbang = 'FALSE';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $is_musrenbang = 'TRUE';
            }

            if (!$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            }

            $nilai_ee = $this->getRequestParameter('nilai_ee');
            if ($nilai_ee == '') {
                $nilai_ee = 0;
            }

            if (!$this->getRequestParameter('kode_kegiatan')) {
                $this->setFlash('gagal', 'Kode Kegiatan Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            }
            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                $subtitle = $this->getRequestParameter('subtitle');
            }
            if (!$this->getRequestParameter('rekening')) {
                $this->setFlash('gagal', 'Rekening Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                $rekening = $this->getRequestParameter('rekening');
            }
            if ((!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan'))) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            } else {
                $lokasi_kec = $this->getRequestParameter('kecamatan');
                $lokasi_kel = $this->getRequestParameter('kelurahan');
                $kec = new Criteria();
                $kec->add(KecamatanPeer::ID, $lokasi_kec);
                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                $rs_kec = KecamatanPeer::doSelectOne($kec);
                if ($rs_kec) {
                    $kecamatan = $rs_kec->getNama();
                }

                $kel = new Criteria();
                $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                if ($rs_kel) {
                    $kelurahan = $rs_kel->getNamaKelurahan();
                } else {
                    $kecamatan = '';
                    $kelurahan = '';
                }
            }

            $jasmas = $this->getRequestParameter('jasmas');

            if ($this->getRequestParameter('keterangan')) {
                $keterangan = $this->getRequestParameter('keterangan');
            } else {
                if ($nilai_ee == 0) {
                    $this->setFlash('gagal', 'Isi keterangan apabila tidak ada nilai ee');
                    return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
                }
            }

//lokasi
            $keisi = 0;
            $lokasi_baru = '';

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
            }

//                    buat baru

            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }

                    if ($keisi == 0) {
                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    } else {
                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    }
                }
            }
            if ($keisi == 0) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingedit?id=$id_waiting&edit=" . md5('ubah'));
            }
//lokasi

            if ($lokasi_baru == '') {
                $detail_name = '';
            } else {
                $detail_name = '(' . $lokasi_baru . ')';
            }

            $volume = 0;
            $keterangan_koefisien = '';
            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1 = $this->getRequestParameter('vol1');
                $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $vol2 = $this->getRequestParameter('vol2');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $vol3 = $this->getRequestParameter('vol3');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $vol4 = $this->getRequestParameter('vol4');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $sekarang = date('Y-m-d H:i:s');

            $c = new Criteria();
            $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c);

            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "set koefisien='$keterangan_koefisien', volume=$volume, komponen_lokasi = '$detail_name', subtitle='$subtitle', "
                        . "kegiatan_code = '$kode_kegiatan', nilai_ee = $nilai_ee, keterangan = '$keterangan',updated_at = '$sekarang', komponen_rekening = '$rekening', "
                        . "kode_jasmas = '$jasmas', kecamatan = '$kecamatan', kelurahan = '$kelurahan', is_musrenbang = '" . $is_musrenbang . "' "
                        . "where id_waiting = $id_waiting";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $con->commit();

                budgetLogger::log('mengedit Data Waiting List untuk kegiatan ' . $kode_kegiatan . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $detail_name . '  id::' . $waitinglist->getIdWaiting());

                $unit_id_waiting = $waitinglist->getUnitId();
                $rd_cari = new Criteria();
                $rd_cari->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                $rd_dapat = WaitingListPUPeer::doSelectOne($rd_cari);

                $kode_rka_fix = $unit_id_waiting . '.' . $kode_kegiatan . '.' . $id_waiting;

                $c_cari_history = new Criteria();
                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                $dapat_history = HistoryPekerjaanV2Peer::doSelect($c_cari_history);
                if ($dapat_history) {
                    foreach ($dapat_history as $value_history) {
                        $id_history = $value_history->getIdHistory();

                        $c_cari_hapus_history = new Criteria();
                        $c_cari_hapus_history->add(HistoryPekerjaanV2Peer::ID_HISTORY, $id_history);
                        $dapat_history_hapus = HistoryPekerjaanV2Peer::doSelectOne($c_cari_hapus_history);
                        if ($dapat_history_hapus) {
                            $dapat_history_hapus->delete();
                        }
                    }
                }

                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rka_lokasi = $unit_id_waiting . '.' . $kode_kegiatan . '.' . $id_waiting;
                            $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getKomponenLokasi();
                            $kecamatan_lokasi = $rd_dapat->getKecamatan();
                            $kelurahan_lokasi = $rd_dapat->getKelurahan();
                            $lokasi_per_titik = $lokasi_baru;

                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka($rka_lokasi);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                            $c_insert_gis->setGang(strtoupper($gang_lama));
                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                            $c_insert_gis->setRw(strtoupper($rw_lama));
                            $c_insert_gis->setRt(strtoupper($rt_lama));
                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                            $c_insert_gis->setKomponen($komponen_lokasi);
                            $c_insert_gis->setKecamatan($kecamatan_lokasi);
                            $c_insert_gis->setKelurahan($kelurahan_lokasi);
                            $c_insert_gis->setLokasi($lokasi_per_titik);
                            $c_insert_gis->save();
                        }
                    }
                }

//                    buat baru
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }


                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rka_lokasi = $unit_id_waiting . '.' . $kode_kegiatan . '.' . $id_waiting;
                        $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getKomponenLokasi();
                        $kecamatan_lokasi = $rd_dapat->getKecamatan();
                        $kelurahan_lokasi = $rd_dapat->getKelurahan();
                        $lokasi_per_titik = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                        $c_insert_gis->setGang(strtoupper($gang_fix));
                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                        $c_insert_gis->setKomponen($komponen_lokasi);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                        $c_insert_gis->setLokasi($lokasi_per_titik);
                        $c_insert_gis->save();
                    }
                }
                $this->setFlash('berhasil', 'Perubahan berhasil tersimpan');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $exc->getMessage());
            }

            return $this->redirect('waitinglist_pu/waitinglist');
        }
    }

    public function executeGo_swakelola() {
        
    }

//    ticket #37 - proses insert waiting list by upload
//    created at 24juni2015      
    public function executeSeluruhwaitinglist() {
        $this->processFiltersseluruhwaitinglist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/seluruhwaitinglist/filters');

        $pagers = new sfPropelPager('WaitingListPU', 20);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(WaitingListPUPeer::UPDATED_AT);
        $this->addFiltersCriteriaseluruhwaitinglist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

//    ticket #37 - proses insert waiting list by upload
//    ticket #37 - proses insert waiting list by upload
//    created at 24juni2015      
    protected function processFiltersseluruhwaitinglist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/seluruhwaitinglist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/seluruhwaitinglist/filters');
        }
    }

//    ticket #37 - proses insert waiting list by upload
//    ticket #37 - proses insert waiting list by upload
//    created at 24juni2015      
    protected function addFiltersCriteriaseluruhwaitinglist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['nama_komponen_is_empty'])) {
                    $criterion = $c->getNewCriterion(WaitingListPUPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPUPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(WaitingListPUPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['nama_lokasi_is_empty'])) {
                    $criterion = $c->getNewCriterion(WaitingListPUPeer::KOMPONEN_LOKASI, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPUPeer::KOMPONEN_LOKASI, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(WaitingListPUPeer::KOMPONEN_LOKASI, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['kode_kegiatan_is_empty'])) {
                    $criterion = $c->getNewCriterion(WaitingListPUPeer::KEGIATAN_CODE, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPUPeer::KEGIATAN_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(WaitingListPUPeer::KEGIATAN_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(WaitingListPUPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(WaitingListPUPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

//    ticket #37 - proses insert waiting list by upload
//    ticket #38 - proses delete waiting list
//    created at 24juni2015          
    function executeProseshapus() {
        $sekarang = date('Y-m-d H:i:s');
        $id_waiting = $this->getRequestParameter('id');
        if (is_null($id_waiting) || ($this->getRequest()->hasParameter('key') != md5('hapus_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
        } else {
            $con = Propel::getConnection();
            $con->begin();
            try {
                $c_waiting = new Criteria();
                $c_waiting->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                $waitinglist = WaitingListPUPeer::doSelectOne($c_waiting);
                if ($waitinglist) {
                    $waitinglist->setUpdatedAt($sekarang);
                    $waitinglist->setStatusHapus(TRUE);
                    $waitinglist->save();

                    budgetLogger::log('Menghapus data Waiting List PU Kegiatan ' . $waitinglist->getKegiatanCode() . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $waitinglist->getKomponenLokasi() . ' id::' . $waitinglist->getIdWaiting());
                }

                $kode_rka = $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCode() . '.' . $id_waiting;

                $c_history = new Criteria();
                $c_history->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $historywaitinglist = HistoryPekerjaanV2Peer::doSelectOne($c_history);
                if ($historywaitinglist) {
                    $historywaitinglist->setStatusHapus(TRUE);
                    $historywaitinglist->save();

                    budgetLogger::log('Menghapus data History Waiting List PU Kegiatan ' . $waitinglist->getKegiatanCode() . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $waitinglist->getKomponenLokasi() . ' kode_rka::' . $kode_rka);
                }

                $query_delete = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_waitinglist "
                        . "set status_hapus = true, last_edit_time = '$sekarang' "
                        . "where id_waiting = $id_waiting";
                $stmt_delete = $con->prepareStatement($query_delete);
                $stmt_delete->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'Menghapus Data Waiting List berhasil');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            }
        }
        $this->redirect('waitinglist_pu/waitinglist');
    }

//    ticket #38 - proses delete waiting list

    public function executeWaitingGoRKA() {

        if ($this->getRequestParameter('cek_rka') == md5('rka_waiting')) {
            $id_waiting = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            if ($waitinglist = WaitingListPUPeer::doSelectOne($c)) {
                $this->waitinglist = $waitinglist;
                $tahunInput = $waitinglist->getTahunInput();
            }

            $d = new Criteria();
            $cton1 = $d->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
            $cton2 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
            $cton3 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $cton1->addOr($cton2);
            $cton1->addOr($cton3);
            $d->add($cton1);
//            $d->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
            $rs_masterkegiatan = DinasMasterKegiatanPeer::doSelect($d);
            $this->rs_masterkegiatan = $rs_masterkegiatan;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->add(KomponenRekeningPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $f->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::ALT_NOT_EQUAL);
            $f->addAscendingOrderByColumn(KomponenRekeningPeer::REKENING_CODE);
            $rs_rekening = KomponenRekeningPeer::doSelect($f);
            $this->rs_rekening = $rs_rekening;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $kode_rka = $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCode() . '.' . $waitinglist->getIdWaiting();

            $g = new Criteria();
            $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
            if ($tahunInput) {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, $tahunInput);
            } else {
                $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            }
            $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
            $geojson = HistoryPekerjaanV2Peer::doSelect($g);
            if ($geojson) {
                $this->rs_geojson = $geojson;
            }

            $h = new Criteria();
            $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $h->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {

            $id_waiting = $this->getRequestParameter('id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $akrual_code = $this->getRequestParameter('akrual_code');

            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c_waiting);

            $unit_id_waiting = $waitinglist->getUnitId();

            $c_komponen = new Criteria();
            $c_komponen->add(KomponenPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $komponen = KomponenPeer::doSelectOne($c_komponen);
            if (!$komponen) {
                $this->setFlash('gagal', 'Komponen tidak ada. Silahkan mengganti komponen ini dengan mengambil komponen baru');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                if (round($waitinglist->getKomponenHargaAwal() - $komponen->getKomponenHargaBulat()) <> 0) {
                    $this->setFlash('gagal', 'Komponen terjadi perubahan harga. Silahkan mengganti komponen ini dengan mengambil komponen baru dan harga baru');
                    return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
                }
            }

            $tipe_komponen = $komponen->getKomponenTipe();

            $is_musrenbang = 'FALSE';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $is_musrenbang = 'FALSE';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $is_musrenbang = 'TRUE';
            }

            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, $unit_id_waiting);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_cari_geojson);
            if (!$dapat_geojson) {
                $this->setFlash('gagal', 'Belum pernah ada Pemetaan pada GMAP. Silahkan koordinasi dengan Bagian Perancangan untuk Pemetaan GMAP.');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            if (!$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            $nilai_ee = $this->getRequestParameter('nilai_ee');
            if ($nilai_ee == '') {
                $nilai_ee = 0;
            }

            if (!$this->getRequestParameter('kode_kegiatan')) {
                $this->setFlash('gagal', 'Kode Kegiatan Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                $subtitle = $this->getRequestParameter('subtitle');
            }
            if (!$this->getRequestParameter('rekening')) {
                $this->setFlash('gagal', 'Rekening Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                $rekening = $this->getRequestParameter('rekening');
            }
            if ((!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan'))) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            } else {
                $lokasi_kec = $this->getRequestParameter('kecamatan');
                $lokasi_kel = $this->getRequestParameter('kelurahan');
                $kec = new Criteria();
                $kec->add(KecamatanPeer::ID, $lokasi_kec);
                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                $rs_kec = KecamatanPeer::doSelectOne($kec);
                if ($rs_kec) {
                    $kecamatan = $rs_kec->getNama();
                }

                $kel = new Criteria();
                $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                if ($rs_kel) {
                    $kelurahan = $rs_kel->getNamaKelurahan();
                } else {
                    $kecamatan = '';
                    $kelurahan = '';
                }
            }

            $jasmas = $this->getRequestParameter('jasmas');

            if ((($kode_kegiatan == '1.03.28.0005' && $subtitle == 'Rehabilitasi Saluran Drainase Lingkungan') || ($kode_kegiatan == '1.04.22.0001' && $subtitle == 'Rehabilitasi/Pemeliharaan Jalan Lingkungan')) && $jasmas == '') {
                $this->setFlash('gagal', 'Jasmas harus diisi');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            if ($this->getRequestParameter('keterangan')) {
                $keterangan = $this->getRequestParameter('keterangan');
            } else {
                if ($nilai_ee == 0) {
                    $this->setFlash('gagal', 'Isi keterangan apabila tidak ada nilai ee');
                    return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
                }
            }

//lokasi
            $keisi = 0;
            $lokasi_baru = '';

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
            }

//                    buat baru

            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }

                    if ($keisi == 0) {
                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    } else {
                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    }
                }
            }
            if ($keisi == 0) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
//lokasi

            if ($lokasi_baru == '') {
                $detail_name = '';
            } else {
                $detail_name = '(' . $lokasi_baru . ')';
            }
            $detail_name_rd = $lokasi_baru;

            $volume = 0;
            $keterangan_koefisien = '';
            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1 = $this->getRequestParameter('vol1');
                $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $vol2 = $this->getRequestParameter('vol2');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $vol3 = $this->getRequestParameter('vol3');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $vol4 = $this->getRequestParameter('vol4');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $komponen_penyusun = $this->getRequestParameter('penyusun');

            $volumePenyusun = array();
            foreach ($komponen_penyusun as $penyusun) {
                $penyusun_id = $this->getRequestParameter('komponenPenyu_' . $penyusun);
                $cekPenyusun = new Criteria();
                $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun_id);
                $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
                foreach ($rs_cekPenyusun as $komPenyusun) {
                    $nilaiDariWeb = $this->getRequestParameter('volPenyu_' . $penyusun);
                    $volumePenyusun[$komPenyusun->getKomponenId()] = $nilaiDariWeb;
                }
            }

            $sekarang = date('Y-m-d H:i:s');

            $dinas = sfContext::getInstance()->getUser()->getNamaLogin();

            $c = new Criteria();
            $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c);

            $unit_id_rd = str_replace('XXX', '', $waitinglist->getUnitId());
            $status_pagu_rincian = 1;
//menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.
            $rd_function = new DinasRincianDetail();
            if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                if (1 == 0) {
                    $status_pagu_rincian = 0;
                } else {
                    if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                        //batas pagu per dinas
                        $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id_rd, $waitinglist->getKomponenId(), $waitinglist->getPajak(), $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                    } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                        //batas pagu per kegiatan 
                        $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id_rd, $kode_kegiatan, $waitinglist->getKomponenId(), $waitinglist->getPajak(), $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                    }
                }
            } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                $status_pagu_rincian = 0;
            }

            if ($status_pagu_rincian == 1) {
                $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }

            if ($akrual_code) {
                $akrual_code_baru = $akrual_code . '|01';
                $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
                $akrual_code_baru = $akrual_code_baru . $no_akrual_code;
            }

            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "set koefisien='$keterangan_koefisien', volume=$volume, komponen_lokasi = '$detail_name', subtitle='$subtitle', "
                        . "kegiatan_code = '$kode_kegiatan', nilai_ee = $nilai_ee, keterangan = '$keterangan',updated_at = '$sekarang', komponen_rekening = '$rekening', "
                        . "kode_jasmas = '$jasmas', kecamatan = '$kecamatan', kelurahan = '$kelurahan', is_musrenbang = '" . $is_musrenbang . "' "
                        . "where id_waiting = $id_waiting";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                budgetLogger::log('mengedit Data Waiting List untuk kegiatan ' . $kode_kegiatan . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $detail_name . '  id::' . $waitinglist->getIdWaiting());

                $rd_cari = new Criteria();
                $rd_cari->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                $rd_dapat = WaitingListPUPeer::doSelectOne($rd_cari);
                $komponen_id = $rd_dapat->getKomponenId();
                $komponen_name = $rd_dapat->getKomponenName();
                $komponen_harga = $rd_dapat->getKomponenHargaAwal();
                $satuan = $rd_dapat->getKomponenSatuan();
                $pajak = $rd_dapat->getPajak();
                $prioritas_pilih = $rd_dapat->getPrioritas();

                $kode_rka_fix = $unit_id_waiting . '.' . $kode_kegiatan . '.' . $id_waiting;

//            cek kode_sub rka-member untuk sub2title
                $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                        . "where kode_sub ilike 'RKAM%' ";
                $stmt = $con->prepareStatement($sql);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $kodesub = $rs->getString('kode_sub');
                }
                $kode = substr($kodesub, 4, 5);
                $kode+=1;
                if ($kode < 10) {
                    $kodesub = 'RKAM0000' . $kode;
                } elseif ($kode < 100) {
                    $kodesub = 'RKAM000' . $kode;
                } elseif ($kode < 1000) {
                    $kodesub = 'RKAM00' . $kode;
                } elseif ($kode < 10000) {
                    $kodesub = 'RKAM0' . $kode;
                } elseif ($kode < 100000) {
                    $kodesub = 'RKAM' . $kode;
                }
//            cek kode_sub rka-member untuk sub2title
//            cek detail_no rincian-detail untuk komponen
                $detail_no = 0;
                $queryDetailNo = "select max(detail_no) as nilai "
                        . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "where unit_id='$unit_id_rd' and kegiatan_code='$kode_kegiatan'";
                $stmt = $con->prepareStatement($queryDetailNo);
                $rs_max = $stmt->executeQuery();
                while ($rs_max->next()) {
                    $detail_no = $rs_max->getString('nilai');
                }
                $detail_no+=1;
                $kode_detail_kegiatan = $unit_id_rd . '.' . $kode_kegiatan . '.' . $detail_no;
//            cek detail_no rincian-detail untuk komponen
//            insert rka-member untuk sub2title                
                $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".dinas_rka_member 
                (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun ) 
                values ('$kodesub','$unit_id_rd','$kode_kegiatan',$detail_no,'$komponen_id','$komponen_name','$detail_name_rd','$rekening','" . sfConfig::get('app_tahun_default') . "')";
                $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                $stmt2->executeQuery();

//            insert rka-member untuk sub2title                
                $subSubtitle = trim($komponen_name . ' ' . $detail_name_rd);

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_rd);
                $c_kegiatan->addAnd(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

//            insert rincian-detail untuk komponen                
                $queryInsert2RincianDetail = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  
                    (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                    komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,lokasi_kecamatan,lokasi_kelurahan, note_skpd, tahap, tipe2, akrual_code, detail_kegiatan) 
                    values 
                    ('" . $kode_kegiatan . "', '" . $tipe_komponen . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '(" . $detail_name_rd . ")', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "', "
                        . "" . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'$unit_id_rd','" . $kodesub . "', '" . $subSubtitle . "', "
                        . "'" . $jasmas . "', '" . $dinas . "', '" . $sekarang . "','" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $kecamatan . "','" . $kelurahan . "','Pengambilan dari Waiting List','" . $dapat_kegiatan->getTahap() . "', 'KONSTRUKSI', '$akrual_code_baru', '$kode_detail_kegiatan')";
                $stmt3 = $con->prepareStatement($queryInsert2RincianDetail);
                $stmt3->executeQuery();

//            insert rincian-detail untuk komponen                  
                $detailNamePenyusun = "''";
                //1 Juni 2016 -> pakai kode akrual -> tidak jadi
                $rekening_induk = $rekening;
                $temp_lain = 8;
                foreach ($komponen_penyusun as $penyusun) {
                    $penyusun_id = $this->getRequestParameter('komponenPenyu_' . $penyusun);
                    $cekPenyusun = new Criteria();
                    $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun_id);
                    $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
                    //print_r($rs_cekPenyusun);
                    foreach ($rs_cekPenyusun as $komponenPenyusun) {
                        if ($akrual_code) {
                            if ($komponenPenyusun->getKodeAkrualKomponenPenyusun()) {
                                $akrual_code_penyusun = $akrual_code . '|02|' . $komponenPenyusun->getKodeAkrualKomponenPenyusun() . $no_akrual_code;
                            } else {
                                if ($temp_lain < 10)
                                    $akrual_code_penyusun = $akrual_code . '|02|0' . $temp_lain . $no_akrual_code;
                                else
                                    $akrual_code_penyusun = $akrual_code . '|02|' . $temp_lain . $no_akrual_code;
                                $temp_lain++;
                            }
                        }
                        $detail_no = 0;
                        $query = "select max(detail_no) as nilai "
                                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='$unit_id_rd' and kegiatan_code='$kode_kegiatan'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs_max = $stmt->executeQuery();
                        while ($rs_max->next()) {
                            $detail_no = $rs_max->getString('nilai');
                        }
                        $detail_no+=1;
                        $kode_detail_kegiatan = $unit_id_rd . '.' . $kode_kegiatan . '.' . $detail_no;

                        $querySisipan = "select max(status_level) as nilai "
                                . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='$unit_id_rd' and kegiatan_code='$kode_kegiatan'";
                        $stmt = $con->prepareStatement($querySisipan);
                        $rs_level = $stmt->executeQuery();
                        while ($rs_level->next()) {
                            $posisi_terjauh = $rs_level->getInt('nilai');
                        }
                        $sisipan = 'false';
                        if ($posisi_terjauh > 0) {
                            $sisipan = 'true';
                        }

                        if ($komponenPenyusun->getKomponenNonPajak() == TRUE) {
                            $pajakPenyusun = 0;
                        } else {
                            $pajakPenyusun = 10;
                        }

                        //$kodeChekBox = str_replace(".", "_", $komponenPenyusun->getKomponenId());

                        $subSubtitle = $komponen_name . ' ' . $detail_name;
                        $subSubtitle = trim($subSubtitle);
                        $query2 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,lokasi_kecamatan,lokasi_kelurahan, note_skpd, tahap, tipe2, akrual_code, detail_kegiatan) 
                                values 
                                ('" . $kode_kegiatan . "', 'SSH', " . $detail_no . ", '" . $rekening_induk . "', '" . $komponenPenyusun->getKomponenId() . "', '" . $detailNamePenyusun . "', " . $this->getRequestParameter('volPenyu_' . $penyusun) . ", '" . $this->getRequestParameter('volPenyu_' . $penyusun) . ' ' . $komponenPenyusun->getSatuan() . "', '" . str_replace("'", "''", $subtitle) . "', "
                                . $komponenPenyusun->getKomponenHarga() . ", " . $komponenPenyusun->getKomponenHarga() . ",'" . $komponenPenyusun->getKomponenName() . "', '" . $komponenPenyusun->getSatuan() . "', " . $pajakPenyusun . ",'$unit_id_rd','" . $kodesub . "', '" . $subSubtitle . "', "
                                . "'" . $jasmas . "', '" . $dinas . "', '" . $sekarang . "','" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $kecamatan . "','" . $kelurahan . "','Pengambilan dari Waiting List','" . $dapat_kegiatan->getTahap() . "', '" . $komponenPenyusun->getKomponenTipe2() . "', '$akrual_code_penyusun', '$kode_detail_kegiatan')";
                        $stmt = $con->prepareStatement($query2);
                        budgetLogger::log('Menambah Komponen penyusun baru (eRevisi) dengan komponen name ' . $komponenPenyusun->getKomponenName() . '(' . $komponenPenyusun->getKomponenId() . ') pada dinas: ' . $unit_id_rd . ' dengan kode kegiatan :' . $kode_kegiatan . ' pada KodeSub:' . $kodesub);

                        $stmt->executeQuery();

                        historyUserLog::tambah_komponen_penyusun_revisi($unit_id_rd, $kode_kegiatan, $detail_no);
                    }
                }
                $total_aktif = 0;
//                $query = "select count(*) as total "
//                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
//                        . "where status_hapus = false and status_waiting = 0 "
//                        . "and unit_id = 'XXX2600' and kegiatan_code = '" . $kode_kegiatan . "'";
                $query = "select max(prioritas) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = '$unit_id_waiting' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }

                $rd_dapat->setStatusWaiting(1);
                $rd_dapat->setKodeRka($unit_id_rd . '.' . $kode_kegiatan . '.' . $detail_no);
                $rd_dapat->setUserPengambil($dinas);
                $rd_dapat->setUpdatedAt($sekarang);
                $rd_dapat->setMetodeWaitinglist(1);
                $rd_dapat->save();

                $prioritas_diatas_satu = $prioritas_pilih + 1;
                for ($index = $prioritas_diatas_satu; $index <= $total_aktif; $index++) {
                    $index_kurang_satu = $index - 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, $unit_id_waiting);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_WAITING, 0);
//echo $index.' '.$index_kurang_satu.' '.$total_aktif.'<br>';
                    if ($waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas)) {
//                    $waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas);
                        $waiting_prio->setPrioritas($index_kurang_satu);
                        $waiting_prio->save();
                    }
//echo $index.' '.$index_kurang_satu.' '.$total_aktif.'<br>';
                }


                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rd_cari_fix = new Criteria();
                            $rd_cari_fix->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_rd);
                            $rd_cari_fix->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $rd_cari_fix->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                            $rd_dapat_fix = DinasRincianDetailPeer::doSelectOne($rd_cari_fix);

                            $rka_lokasi_fix = $unit_id_rd . '.' . $kode_kegiatan . '.' . $detail_no;
                            $komponen_lokasi_fix = $rd_dapat_fix->getKomponenName() . ' ' . $rd_dapat_fix->getDetailName();
                            $kecamatan_lokasi_fix = $rd_dapat_fix->getLokasiKecamatan();
                            $kelurahan_lokasi_fix = $rd_dapat_fix->getLokasiKelurahan();
                            $lokasi_per_titik_fix = $lokasi_baru;

                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka($rka_lokasi_fix);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                            $c_insert_gis->setGang(strtoupper($gang_lama));
                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                            $c_insert_gis->setRw(strtoupper($rw_lama));
                            $c_insert_gis->setRt(strtoupper($rt_lama));
                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                            $c_insert_gis->setKomponen($komponen_lokasi_fix);
                            $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                            $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                            $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                            $c_insert_gis->save();
                        }
                    }
                }

//                    buat baru
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }


                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rd_cari_fix = new Criteria();
                        $rd_cari_fix->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_rd);
                        $rd_cari_fix->addAnd(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $rd_cari_fix->addAnd(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                        $rd_dapat_fix = DinasRincianDetailPeer::doSelectOne($rd_cari_fix);

                        $rka_lokasi_fix = $unit_id_rd . '.' . $kode_kegiatan . '.' . $detail_no;
                        $komponen_lokasi_fix = $rd_dapat_fix->getKomponenName() . ' ' . $rd_dapat_fix->getDetailName();
                        $kecamatan_lokasi_fix = $rd_dapat_fix->getLokasiKecamatan();
                        $kelurahan_lokasi_fix = $rd_dapat_fix->getLokasiKelurahan();
                        $lokasi_per_titik_fix = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi_fix);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                        $c_insert_gis->setGang(strtoupper($gang_fix));
                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                        $c_insert_gis->setKomponen($komponen_lokasi_fix);
                        $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                        $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                        $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                        $c_insert_gis->save();
                    }
                }

                $c_unit = new Criteria();
                $c_unit->add(UnitKerjaPeer::UNIT_ID, $unit_id_rd);
                $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

                foreach ($dapat_geojson as $value_geojson) {
                    $geojson_baru = new GeojsonlokasiRev1();
                    $geojson_baru->setUnitId($unit_id_rd);
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setDetailNo($detail_no);
                    $geojson_baru->setSatuan($rd_dapat_fix->getSatuan());
                    $geojson_baru->setVolume($rd_dapat_fix->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd_dapat_fix->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd_dapat_fix->getKomponenName() . ' ' . $rd_dapat_fix->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->setKodeDetailKegiatan($unit_id_rd . '.' . $kode_kegiatan . '.' . $detail_no);
                    $geojson_baru->save();
                }
                $con->commit();

                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' menjadi eRevisi');
                return $this->redirect("waitinglist_pu/waitinglist");
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $exc->getMessage());
                return $this->redirect("waitinglist_pu/waitinglist");
//                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
        }
    }

    public function executeWaitingGoSwakelola() {
        if ($this->getRequestParameter('cek_swakelola') == md5('swakelola_waiting')) {
            $id_waiting = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c);
            if ($waitinglist) {
                $this->waitinglist = $waitinglist;
            }

            $d = new Criteria();
            $cton1 = $d->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '1.1.1.04.01.0003');
            $cton2 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
            $cton3 = $d->getNewCriterion(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $cton1->addOr($cton2);
            $cton1->addOr($cton3);
            $d->add($cton1);
//            $d->add(DinasMasterKegiatanPeer::UNIT_ID, '2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'jalan_2600');
//            $d->addOr(DinasMasterKegiatanPeer::USER_ID, 'pematusan_2600');
            $d->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KODE_KEGIATAN);
            $rs_masterkegiatan = DinasMasterKegiatanPeer::doSelect($d);
            $this->rs_masterkegiatan = $rs_masterkegiatan;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->add(KomponenRekeningPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $f->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::ALT_NOT_EQUAL);
            $f->addAscendingOrderByColumn(KomponenRekeningPeer::REKENING_CODE);
            $rs_rekening = KomponenRekeningPeer::doSelect($f);
            $this->rs_rekening = $rs_rekening;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $kode_rka = $waitinglist->getUnitId() . '.' . $waitinglist->getKegiatanCode() . '.' . $waitinglist->getIdWaiting();

            $g = new Criteria();
            $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
            $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
            $geojson = HistoryPekerjaanV2Peer::doSelect($g);
            if ($geojson) {
                $this->rs_geojson = $geojson;
            }

            $h = new Criteria();
            $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $sub = "char_length(lokasi)>10";
            $h->addAnd(HistoryPekerjaanV2Peer::LOKASI, $sub, Criteria::CUSTOM);
            $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {

            $id_waiting = $this->getRequestParameter('id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c_waiting);

            $unit_id_waiting = $waitinglist->getUnitId();

            $c_komponen = new Criteria();
            $c_komponen->add(KomponenPeer::KOMPONEN_ID, $waitinglist->getKomponenId());
            $komponen = KomponenPeer::doSelectOne($c_komponen);
            if (!$komponen) {
                $this->setFlash('gagal', 'Komponen tidak ada. Silahkan mengganti komponen ini dengan mengambil komponen baru');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            } else {
                if (round($waitinglist->getKomponenHargaAwal() - $komponen->getKomponenHarga()) <> 0) {
                    $this->setFlash('gagal', 'Komponen terjadi perubahan harga. Silahkan mengganti komponen ini dengan mengambil komponen baru dan harga baru');
                    return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
                }
            }

            $tipe_komponen = $komponen->getKomponenTipe();

            $is_musrenbang = 'FALSE';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $is_musrenbang = 'FALSE';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $is_musrenbang = 'TRUE';
            }

            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, $unit_id_waiting);
            $c_cari_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_cari_geojson);
            if (!$dapat_geojson) {
                $this->setFlash('gagal', 'Belum pernah ada Pemetaan pada GMAP. Silahkan koordinasi dengan Bagian Perancangan untuk Pemetaan GMAP.');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            }

            if (!$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            }

            $nilai_ee = $this->getRequestParameter('nilai_ee');
            if ($nilai_ee == '') {
                $nilai_ee = 0;
            }

            if (!$this->getRequestParameter('kode_kegiatan')) {
                $this->setFlash('gagal', 'Kode Kegiatan Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            }
            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            } else {
                $subtitle = $this->getRequestParameter('subtitle');
            }
            if (!$this->getRequestParameter('rekening')) {
                $this->setFlash('gagal', 'Rekening Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            } else {
                $rekening = $this->getRequestParameter('rekening');
            }
            if ((!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan'))) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            } else {
                $lokasi_kec = $this->getRequestParameter('kecamatan');
                $lokasi_kel = $this->getRequestParameter('kelurahan');
                $kec = new Criteria();
                $kec->add(KecamatanPeer::ID, $lokasi_kec);
                $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                $rs_kec = KecamatanPeer::doSelectOne($kec);
                if ($rs_kec) {
                    $kecamatan = $rs_kec->getNama();
                }

                $kel = new Criteria();
                $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                if ($rs_kel) {
                    $kelurahan = $rs_kel->getNamaKelurahan();
                } else {
                    $kecamatan = '';
                    $kelurahan = '';
                }
            }

            $jasmas = $this->getRequestParameter('jasmas');

            if ($this->getRequestParameter('keterangan')) {
                $keterangan = $this->getRequestParameter('keterangan');
            } else {
                if ($nilai_ee == 0) {
                    $this->setFlash('gagal', 'Isi keterangan apabila tidak ada nilai ee');
                    return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
                }
            }

//lokasi
            $keisi = 0;
            $lokasi_baru = '';

//                    ambil lama
            $lokasi_lama = $this->getRequestParameter('lokasi_lama');

            if (count($lokasi_lama) > 0) {
                foreach ($lokasi_lama as $value_lokasi_lama) {
                    $c_cari_lokasi = new Criteria();
                    $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                    $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                    $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                    if ($dapat_lokasi_lama) {

                        $jalan_fix = '';
                        $gang_fix = '';
                        $nomor_fix = '';
                        $rw_fix = '';
                        $rt_fix = '';
                        $keterangan_fix = '';
                        $tempat_fix = '';

                        $jalan_lama = $dapat_lokasi_lama->getJalan();
                        $gang_lama = $dapat_lokasi_lama->getGang();
                        $nomor_lama = $dapat_lokasi_lama->getNomor();
                        $rw_lama = $dapat_lokasi_lama->getRw();
                        $rt_lama = $dapat_lokasi_lama->getRt();
                        $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                        $tempat_lama = $dapat_lokasi_lama->getTempat();

                        if ($jalan_lama <> '') {
                            $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                        }

                        if ($tempat_lama <> '') {
                            $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                        }

                        if ($gang_lama <> '') {
                            $gang_fix = $gang_lama . ' ';
                        }

                        if ($nomor_lama <> '') {
                            $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                        }

                        if ($rw_lama <> '') {
                            $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                        }

                        if ($rt_lama <> '') {
                            $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                        }

                        if ($keterangan_lama <> '') {
                            $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
            }

//                    buat baru

            $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
            $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
            $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
            $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
            $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
            $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
            $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
            $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

            $total_array_lokasi = count($lokasi_jalan);

            for ($i = 0; $i < $total_array_lokasi; $i++) {
                $jalan_fix = '';
                $gang_fix = '';
                $tipe_gang_fix = '';
                $nomor_fix = '';
                $rw_fix = '';
                $rt_fix = '';
                $keterangan_fix = '';
                $tempat_fix = '';
                if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                    if (trim($lokasi_jalan[$i]) <> '') {
                        $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                    }

                    if (trim($lokasi_tempat[$i]) <> '') {
                        $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                    }

                    if (trim($tipe_gang[$i]) <> '') {
                        $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                    } else {
                        $tipe_gang_fix = 'GG. ';
                    }

                    if (trim($lokasi_gang[$i]) <> '') {
                        $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                    }

                    if (trim($lokasi_nomor[$i]) <> '') {
                        $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                    }

                    if (trim($lokasi_rw[$i]) <> '') {
                        $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                    }

                    if (trim($lokasi_rt[$i]) <> '') {
                        $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                    }

                    if (trim($lokasi_keterangan[$i]) <> '') {
                        $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                    }

                    if ($keisi == 0) {
                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    } else {
                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                        $keisi++;
                    }
                }
            }
            if ($keisi == 0) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            }
//lokasi

            if ($lokasi_baru == '') {
                $detail_name = '';
            } else {
                $detail_name = '(' . $lokasi_baru . ')';
            }
            $detail_name_rd = $lokasi_baru;

            $volume = 0;
            $keterangan_koefisien = '';
            if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                $vol1 = $this->getRequestParameter('vol1');
                $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                if ($this->getRequestParameter('vol2') == '') {
                    $vol2 = 1;
                    $volume = $this->getRequestParameter('vol1') * $vol2;
                } else if (!$this->getRequestParameter('vol2') == '') {
                    $vol2 = $this->getRequestParameter('vol2');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                }
                if ($this->getRequestParameter('vol3') == '') {
                    $vol3 = 1;
                    $volume = $volume * $vol3;
                } else if (!$this->getRequestParameter('vol3') == '') {
                    $vol3 = $this->getRequestParameter('vol3');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                }
                if ($this->getRequestParameter('vol4') == '') {
                    $vol4 = 1;
                    $volume = $volume * $vol4;
                } else if (!$this->getRequestParameter('vol4') == '') {
                    $vol4 = $this->getRequestParameter('vol4');
                    $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                    $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                }
            }

            $sekarang = date('Y-m-d H:i:s');

            $dinas = sfContext::getInstance()->getUser()->getNamaLogin();

            $c = new Criteria();
            $c->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
            $waitinglist = WaitingListPUPeer::doSelectOne($c);

            $unit_id_rd = str_replace('XXX', '', $waitinglist->getUnitId());
            $status_pagu_rincian = 0;

            if ($status_pagu_rincian == 1) {
                $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                return $this->redirect("waitinglist_pu/waitingGoSwakelola?id=$id_waiting&cek_swakelola=" . md5('swakelola_waiting'));
            }

            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "update " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "set koefisien='$keterangan_koefisien', volume=$volume, komponen_lokasi = '$detail_name', subtitle='$subtitle', "
                        . "kegiatan_code = '$kode_kegiatan', nilai_ee = $nilai_ee, keterangan = '$keterangan',updated_at = '$sekarang', komponen_rekening = '$rekening', "
                        . "kode_jasmas = '$jasmas', kecamatan = '$kecamatan', kelurahan = '$kelurahan', is_musrenbang = '" . $is_musrenbang . "' "
                        . "where id_waiting = $id_waiting";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                budgetLogger::log('mengedit Data Waiting List untuk kegiatan ' . $kode_kegiatan . ' Komponen ' . $waitinglist->getKomponenName() . ' ' . $detail_name . '  id::' . $waitinglist->getIdWaiting());

                $rd_cari = new Criteria();
                $rd_cari->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                $rd_dapat = WaitingListPUPeer::doSelectOne($rd_cari);

                $komponen_id = $rd_dapat->getKomponenId();
                $komponen_name = $rd_dapat->getKomponenName();
                $komponen_harga = $rd_dapat->getKomponenHargaAwal();
                $satuan = $rd_dapat->getKomponenSatuan();
                $pajak = $rd_dapat->getPajak();
                $prioritas_pilih = $rd_dapat->getPrioritas();
                $nilai_anggaran_semula = $rd_dapat->getNilaiAnggaran();
                $komponen_lokasi = $rd_dapat->getKomponenLokasi();
                $swa_ismurenbang = $rd_dapat->getIsMusrenbang();

//            insert waiting_swakelola untuk komponen       
                $new_insert_swakelola = new WaitingListSwakelola();
                $new_insert_swakelola->setUnitId('SWA' . $unit_id_rd);
                $new_insert_swakelola->setKegiatanCode($kode_kegiatan);
                $new_insert_swakelola->setSubtitle($subtitle);
                $new_insert_swakelola->setKomponenId($komponen_id);
                $new_insert_swakelola->setKomponenName($komponen_name);
                $new_insert_swakelola->setKomponenLokasi($komponen_lokasi);
                $new_insert_swakelola->setKomponenHargaAwal($komponen_harga);
                $new_insert_swakelola->setPajak($pajak);
                $new_insert_swakelola->setKomponenSatuan($satuan);
                $new_insert_swakelola->setKoefisien($keterangan_koefisien);
                $new_insert_swakelola->setVolume($volume);
                $new_insert_swakelola->setTahunInput(sfConfig::get('app_tahun_default'));
                $new_insert_swakelola->setCreatedAt($sekarang);
                $new_insert_swakelola->setUpdatedAt($sekarang);
                $new_insert_swakelola->setKeterangan($keterangan);
                $new_insert_swakelola->setKecamatan($kecamatan);
                $new_insert_swakelola->setKodeJasmas($jasmas);
                $new_insert_swakelola->setNilaiEe($nilai_ee);
                $new_insert_swakelola->setUserPengambil($dinas);
                $new_insert_swakelola->setKelurahan($kelurahan);
                $new_insert_swakelola->setNilaiAnggaranSemula($nilai_anggaran_semula);
                $new_insert_swakelola->setIsMusrenbang($swa_ismurenbang);
                $new_insert_swakelola->setKomponenRekening($rekening);
                $new_insert_swakelola->save();

                $id_swakelola_baru = $new_insert_swakelola->getIdSwakelola();
//            insert waiting_swakelola untuk komponen                

                $total_aktif = 0;
                $query = "select count(*) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = '$unit_id_waiting' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }

                $rd_dapat->setStatusWaiting(1);
                $rd_dapat->setKodeRka('SWA' . $unit_id_rd . '.' . $kode_kegiatan . '.' . $id_swakelola_baru);
                $rd_dapat->setUserPengambil($dinas);
                $rd_dapat->setUpdatedAt($sekarang);
                $rd_dapat->setMetodeWaitinglist(2);
                $rd_dapat->save();

                $prioritas_diatas_satu = $prioritas_pilih + 1;
                for ($index = $prioritas_diatas_satu; $index <= $total_aktif; $index++) {
                    $index_kurang_satu = $index - 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, $unit_id_waiting);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas);
                    $waiting_prio->setPrioritas($index_kurang_satu);
                    $waiting_prio->save();
                }

                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                            $rka_lokasi_fix = 'SWA' . $unit_id_rd . '.' . $kode_kegiatan . '.' . $id_swakelola_baru;
                            $komponen_lokasi_fix = $komponen_name . ' ' . $detail_name;
                            $lokasi_per_titik_fix = $lokasi_baru;

                            $c_insert_gis = new HistoryPekerjaanV2();
                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                            $c_insert_gis->setKodeRka($rka_lokasi_fix);
                            $c_insert_gis->setStatusHapus(FALSE);
                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                            $c_insert_gis->setGang(strtoupper($gang_lama));
                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                            $c_insert_gis->setRw(strtoupper($rw_lama));
                            $c_insert_gis->setRt(strtoupper($rt_lama));
                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                            $c_insert_gis->setKomponen($komponen_lokasi_fix);
                            $c_insert_gis->setKecamatan($kecamatan);
                            $c_insert_gis->setKelurahan($kelurahan);
                            $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                            $c_insert_gis->save();
                        }
                    }
                }

//                    buat baru
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }


                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                        $rka_lokasi_fix = 'SWA' . $unit_id_rd . '.' . $kode_kegiatan . '.' . $id_swakelola_baru;
                        $komponen_lokasi_fix = $komponen_name . ' ' . $detail_name;
                        $lokasi_per_titik_fix = $lokasi_baru;

                        $c_insert_gis = new HistoryPekerjaanV2();
                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                        $c_insert_gis->setKodeRka($rka_lokasi_fix);
                        $c_insert_gis->setStatusHapus(FALSE);
                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                        $c_insert_gis->setGang(strtoupper($gang_fix));
                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                        $c_insert_gis->setKomponen($komponen_lokasi_fix);
                        $c_insert_gis->setKecamatan($kecamatan);
                        $c_insert_gis->setKelurahan($kelurahan);
                        $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                        $c_insert_gis->save();
                    }
                }

                $con->commit();
                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $komponen_lokasi . ' menjadi Swakelola');
                return $this->redirect("waitinglist_pu/waitinglist");
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $exc->getMessage());
                return $this->redirect("waitinglist_pu/waitinglist");
//                return $this->redirect("waitinglist_pu/waitingGoRKA?id=$id_waiting&cek_rka=" . md5('rka_waiting'));
            }
        }
    }

    public function executePilihKelurahan() {
        $this->id_kecamatan = $this->getRequestParameter('b');
    }

    public function executePrintwaiting() {
        $unit_id = $this->getRequestParameter('unit_id');
        if (!$unit_id) {
            $unit_id = "2600','2300";
        }
        //$kegiatan = $this->getRequestParameter('kegiatan');
        $queryWaitingList = " 
                ( 
                select wp.status_waiting, wp.komponen_id, wp.kegiatan_code, wp.subtitle, wp.komponen_name, wp.komponen_lokasi, 
                wp.nilai_anggaran nilai_anggaran_semula, wp.nilai_anggaran nilai_anggaran_sekarang, mk.nama_kegiatan, wp.keterangan    
                from ebudget.waitinglist_pu wp, ebudget.master_kegiatan mk 
                where wp.status_hapus = false and wp.status_waiting = 0 
                and mk.unit_id in ('$unit_id') and mk.kode_kegiatan = wp.kegiatan_code  
                ) union all ( 
                select wp.status_waiting,  wp.komponen_id, wp.kegiatan_code, wp.subtitle, wp.komponen_name, wp.komponen_lokasi, 
                wp.nilai_anggaran_semula nilai_anggaran_semula, rd.nilai_anggaran nilai_anggaran_sekarang, mk.nama_kegiatan, wp.keterangan   
                from ebudget.waitinglist_pu wp, ebudget.rincian_detail rd, ebudget.master_kegiatan mk  
                where wp.status_hapus = false and wp.status_waiting = 1 
                and mk.unit_id in ('$unit_id') and mk.kode_kegiatan = wp.kegiatan_code 
                and wp.kode_rka = rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no 
                ) 
                order by kegiatan_code ASC, subtitle ASC, komponen_id ASC, komponen_lokasi ASC, status_waiting DESC ";
        $con = Propel::getConnection();
        $stmtWaitingList = $con->prepareStatement($queryWaitingList);
        $this->waitinglist = $rs_WaitingList = $stmtWaitingList->executeQuery();
        $this->sekarang = date('d-m-Y H:i:s');

        $this->setLayout('kosong');
    }

    public function executeBuatPDFPrintwaiting() {
        $sekarang = date('d-m-Y_H:i');

        /* Example code from mPDF site */
        $mpdf = new mPDF('utf-8', 'A4');
        $mpdf->useOnlyCoreFonts = true; // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle("Print Waiting List");
        $mpdf->SetAuthor("e-budgeting");
        $mpdf->SetDisplayMode('fullpage');

        $this->setLayout('kosong');
        $html = $this->getController()->getPresentationFor($this->getModuleName(), 'printwaiting');
        $mpdf->WriteHTML($html);

        $this->namafile = $namafile = 'WaitingList_per_' . $sekarang;
        $mpdf->Output($namafile . '.pdf', 'D');
        exit;
    }

}
