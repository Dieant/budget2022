<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Validation') ?>

<section class="content-header">
    <h1>List Request Admin</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('view_rka/list_messages'); ?>
    <!-- Default box -->    
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('view_rka/requestlist', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Satuan Kerja</label>
                <div class="col-sm-10">
                    <?php
                    $e = new Criteria();
                    $e->add(UnitKerjaPeer::UNIT_ID, $unit_kerja[0], criteria::ILIKE);
                    for ($i = 1; $i < count($unit_kerja); $i++) {
                        $e->addOr(UnitKerjaPeer::UNIT_ID, $unit_kerja[$i], criteria::ILIKE);
                    }
                    //$e->add(UnitKerjaPeer::UNIT_NAME, 'Latihan', criteria::NOT_EQUAL);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    $v = UnitKerjaPeer::doSelect($e);

                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Dinas------')), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tipe</label>
                <div class="col-sm-10">
                    <?php
                    echo select_tag('filters[tipe]', options_for_select(array('0' => 'Buka Kunci Kegiatan', '1' => 'Tambah Pagu', '2' => 'Upload Dokumen'), isset($filters['tipe']) ? $filters['tipe'] : null, array('include_custom' => '------Semua Tipe------')), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo button_to(__('reset'), 'view_rka/requestlist?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
                    <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
    <div class="box box-primary box-solid">
        <div class="box-body">
            <?php if (!$pager->getNbResults()): ?>
                <?php echo __('no result') ?>
            <?php else: ?>
                <?php include_partial('view_rka/requestlist', array('pager' => $pager)) ?>
            <?php endif; ?>
        </div>
    </div>
</section>
