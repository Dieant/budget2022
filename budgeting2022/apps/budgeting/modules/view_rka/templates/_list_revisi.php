<?php
// auto-generated by sfPropelAdmin
// date: 2008/11/12 12:45:05
$menu = $sf_params->get('menu');
if ($tahap == 'pakbp') {
    $tabel_semula = 'revisi6_';
    $tabel_dpn = 'pak_bukuputih_';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_semula = 'murni_bukuputih_';
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_semula = 'murni_bukuputih_';
    $tabel_dpn = 'murni_bukubiru_';
} elseif ($tahap == 'murnibbpraevagub') {
    $tabel_dpn = 'murni_bukubiru_praevagub_';
} elseif ($tahap == 'revisi1') {
    $tabel_semula = 'murni_';
    $tabel_dpn = 'revisi1_';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
} elseif ($tahap == 'revisi2') {
    $tabel_semula = 'revisi1_';
    $tabel_dpn = 'revisi2_';
} elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
} elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
} elseif ($tahap == 'revisi3') {
    $tabel_semula = 'revisi2_';
    $tabel_dpn = 'revisi3_';
} elseif ($tahap == 'revisi3_1') {
    $tabel_semula = 'revisi2_';
    $tabel_dpn = 'revisi3_1_';
} elseif ($tahap == 'revisi4') {
    $tabel_semula = 'revisi3_';
    $tabel_dpn = 'revisi4_';
} elseif ($tahap == 'revisi5') {
    $tabel_semula = 'revisi4_';
    $tabel_dpn = 'revisi5_';
} elseif ($tahap == 'revisi6') {
    $tabel_semula = 'revisi5_';
    $tabel_dpn = 'revisi6_';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
} else {
    $tabel_semula = '';
    $tabel_dpn = 'dinas_';
}

    $n="and kode_kegiatan not ilike '%-%'";
    $m="and kegiatan_code not ilike '%-%'";

    if($menu == 'Pendapatan')
    {
        $n = "and kode_kegiatan ilike '%-%'";
        $m = "and kegiatan_code ilike '%-%'";
    }
?>
<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <?php include_partial('list_th_tabular_revisi') ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod(++$i, 2)
                ?>
                <tr>
                    <?php include_partial('list_td_tabular_revisi', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd, 'tahap' => $tahap)) ?>
                    <?php include_partial('list_td_actions_revisi', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd, 'tahap' => $tahap)) ?>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    if (sfConfig::get('app_tahap_edit') == 'murni')
                        $query = "select sum(alokasi) as nilai from " . sfConfig::get('app_default_schema') . ".pagu where unit_id='$unit_id' $n";
                    else
                        $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_semula . "rincian_detail "
                    . "where unit_id='$unit_id' and status_hapus=FALSE $m";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_awal = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai_awal, 0, ',', '.')."</b>";
                    ?>
                </td>
                <!-- <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan "
                    . "where unit_id='$unit_id'";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $tambahanPagu = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($tambahanPagu, 0, ',', '.')."</b>";
                    ?>
                </td> -->
                <?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
                <td align="right">
                    <?php
                        $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                        $unit_id = $master_kegiatan->getUnitId();
                        $query = "select sum(pagu) as nilai from " . sfConfig::get('app_default_schema') . ".pagu_pak where unit_id='$unit_id' $n";
                        // $query = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id'";

                        $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                        $statement = $con->prepareStatement($query);
                        $rs_nilai = $statement->executeQuery();
                        while ($rs_nilai->next()) {
                            $nilai_awal = $rs_nilai->getString('nilai');
                            $nilai_awal = myTools::round_ganjil_genap($nilai_awal);
                        }
                        echo '<b>'.number_format($nilai_awal, 0, ',', '.').'</b>';
                        ?>
                </td>
                <?php } ?>
                <?php if (sfConfig::get('app_tahap_edit') != 'pak' && sfConfig::get('app_tahap_edit') != 'murni') { ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    if (sfConfig::get('app_tahap_edit') == 'murni') {
                        //digunakan ketika awal anggaran
                        $query = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' $n";
                        //digunakan ketika sudah mulai akan dibandingkan dan tidak awal anggaran namun masih dalam tahap murni
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_semula . "rincian_detail "
                         . "where unit_id='$unit_id' and status_hapus=FALSE $m";
                    }                   

                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_menjadi = $rs_nilai->getString('nilai');
                    }

                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan "
                    . "where unit_id='$unit_id' $n";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $tambahanPagu = $rs_nilai->getString('nilai');
                    }
                    
                    $Nilai_menjadi= $nilai_menjadi + $tambahanPagu;
                    echo "<b>".number_format($Nilai_menjadi, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php } ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
                    . "where unit_id='$unit_id' and status_hapus=FALSE $m";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai, 0, ',', '.')."</b>";
                    ?>
                </td>
                <td align="right">
                    <?php
                    $selisih = ($nilai_awal + $tambahanPagu) - $nilai;
                    echo "<b>".number_format($selisih, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php if (sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') { ?>
                 <td align="right">
                    <?php
                    $tambahan_pagu = $tambahanPagu;
                    echo "<b>".number_format($tambahan_pagu, 0, ',', '.')."</b>";
                    ?>
                </td>
                 <?php } ?>
                <td colspan="3">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
        echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'view_rka/listRevisi?menu='.$menu.'&page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "view_rka/listRevisi?menu=".$menu."&page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'view_rka/listRevisi?menu='.$menu.'&page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
        endif;
        ?>
    </ul>
</div>