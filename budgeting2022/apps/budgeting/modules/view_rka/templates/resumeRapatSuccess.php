<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Pilih Tahap Resume Rapat</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('peneliti/list_messages'); ?>
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Pilih Tahap Resume Rapat</h3>
        </div>
        <div class="box-body">
            <?php echo form_tag('view_rka/resumeRapat', array('method' => 'post', 'class' => 'form-horizontal')) ?>
            <?php echo input_hidden_tag('unit_id', $unit_id) ?>
            <?php echo input_hidden_tag('kode_kegiatan', $kode_kegiatan) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tahap</label>
                <div class="col-sm-10">
                    <select class="form-control" name="tahap">
                        <option value="1">Revisi 1</option>
                        <option value="2">Revisi 2</option>
                        <option value="3">Revisi 3</option>
                        <option value="4">Revisi 4</option>
                        <option value="5">Revisi 5</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <?php echo submit_tag('Print Resume Rapat', 'name=print class=btn btn-flat btn-success'); ?>
                </div>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
</section>