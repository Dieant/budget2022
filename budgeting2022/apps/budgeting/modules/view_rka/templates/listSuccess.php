<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Sub Kegiatan - Kertas Kerja</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Kertas Kerja</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('view_rka/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('view_rka/list', array('method' => 'get', 'class' => 'form-horizontal')) ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Perangkat Daerah</label>
                                <?php
                                    $e = new Criteria();
                                    $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
                                    if($sf_user->getNamaUser()!='walikota'){
                                        $e->addJoin(UnitKerjaPeer::UNIT_ID, UserHandleV2Peer::UNIT_ID);
                                        $e->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaUser(), Criteria::EQUAL);
                                        $e->add(UserHandleV2Peer::SCHEMA_ID, '2', Criteria::EQUAL);
                                    }
                                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $v = UnitKerjaPeer::doSelect($e);
                                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Satuan Kerja------')), Array('class' => 'js-example-basic-single form-control select2'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama sub kegiatan</label>
                                <?php
                                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control'));
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                                <?php
                                    echo link_to('Reset <i class="fa fa-backspace"></i>', 'view_rka/list?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                                ?>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">                 
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial("view_rka/list", array('pager' => $pager)) ?>
                    <?php endif; ?>            
                </div>
            </div>
        </div>
    </div>
</section>