<?php
if ($tahap == 'pakbp') {
    $tabel_semula = 'revisi6_';
    $tabel_dpn = 'pak_bukuputih_';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} elseif ($tahap == 'murnibbpraevagub') {
    $tabel_dpn = 'murni_bukubiru_praevagub_';
} elseif ($tahap == 'revisi1') {
    $tabel_semula = 'murni_';
    $tabel_dpn = 'revisi1_';
} elseif ($tahap == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
} elseif ($tahap == 'revisi2') {
    $tabel_semula = 'revisi1_';
    $tabel_dpn = 'revisi2_';
} elseif ($tahap == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
} elseif ($tahap == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
} elseif ($tahap == 'revisi3') {
    $tabel_semula = 'revisi2_';
    $tabel_dpn = 'revisi3_';
} elseif ($tahap == 'revisi3_1') {
    $tabel_semula = 'revisi2_';
    $tabel_dpn = 'revisi3_1_';
} elseif ($tahap == 'revisi4') {
    $tabel_semula = 'revisi3_';
    $tabel_dpn = 'revisi4_';
} elseif ($tahap == 'revisi5') {
    $tabel_semula = 'revisi4_';
    $tabel_dpn = 'revisi5_';
} elseif ($tahap == 'revisi6') {
    $tabel_semula = 'revisi5_';
    $tabel_dpn = 'revisi6_';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
} else {
    $tabel_semula = '';
    $tabel_dpn = 'dinas_';
}
if(sfConfig::get('app_tahap_edit') == 'murni') {
	$query = "select SUM(alokasi) as nilai
			from " . sfConfig::get('app_default_schema') . ".pagu
			where kode_kegiatan ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' ";
	$con = Propel::getConnection();
	$stmt = $con->prepareStatement($query);
	$rs = $stmt->executeQuery();
	while ($rs->next()) {
	    $pagu = $rs->getFloat('nilai');
	}
	echo number_format($pagu, 0, ',', '.');
} else {	
            $con = Propel::getConnection();
             $c = new Criteria();
             $c->add(DinasMasterKegiatanPeer::UNIT_ID, $master_kegiatan->getUnitId());
             $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $master_kegiatan->getKodeKegiatan());
             $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
              //TAMBAHAN UNTUK KEG DAK    
              if ($kegiatan->getIsDak() == false)
              {
              	$query = "select SUM(nilai_anggaran) as nilai
			from " . sfConfig::get('app_default_schema') . "." . $tabel_semula . "rincian_detail
			where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=FALSE";

              }
              else 
              {
              	$query = "select alokasi as nilai
			from " . sfConfig::get('app_default_schema') . ".pagu
			where kode_kegiatan='" . $master_kegiatan->getKodeKegiatan() ."'";
              }
                
			
	$con = Propel::getConnection();
	$stmt = $con->prepareStatement($query);
	$rs = $stmt->executeQuery();
	while ($rs->next()) {
	    $nilai = $rs->getFloat('nilai');
	}
	echo number_format($nilai, 0, ',', '.');
}
?>