<td class="tombol_actions">
    <?php
        echo link_to('<i class="fa fa-edit"></i>', 'view_rka/editRevisi?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId() . '&tahap=' . $tahap, array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'), 'class' => 'btn btn-outline-info btn-sm')); 
        if (!$tahap) {
                echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Perbandingan Komponen'), 'title' => __('Perbandingan Komponen'), 'class' => 'btn btn-outline-secondary btn-sm'));
                echo link_to('<i class="fa fa-exclamation-triangle"></i>', 'report/penandaPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Penanda Prioritas'), 'title' => __('Penanda Prioritas'), 'class' => 'btn btn-outline-warning btn-sm')); 
                echo link_to('<i class="fa fa-hand-holding-usd"></i>', 'report/editRasionalisasi?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Edit Nilai Rasionalisasi'), 'title' => __('Edit Nilai Rasionalisasi'), 'class' => 'btn btn-outline-info btn-sm')); 

                 if(sfConfig::get('app_tahap_edit') == 'pak') {                   
                      echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan murni bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                     }
                     
                if ($sf_user->getNamaUser() != 'resumeviewer') { 
                    echo link_to('<i class="fa fa-list-ol"></i>', 'report/bandingRekeningAsli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Rekening'), 'title' => __('Laporan Perbandingan Rekening'), 'class' => 'btn btn-outline-info btn-sm'));
                }
                if(sfConfig::get('app_tahap_edit') == 'murni') {
                    echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Perbandingan Komponen Buku Putih'), 'title' => __('Perbandingan Komponen Buku Putih'), 'class' => 'btn btn-outline-info btn-sm'));  
                    echo link_to('<i class="fa fa-chalkboard-teacher"></i>', 'report/menuViewDevplan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . (isset($filters['tahap']) ? $filters['tahap'] : ''), array('alt' => __('Melihat Komponen Devplan'), 'title' => __('Melihat Komponen Devplan'), 'class' => 'btn btn-outline-primary btn-sm'));
                }
                if($sf_user->getNamaUser() == 'evalitbang') {
                    echo link_to('<i class="fa fa-user-tag"></i>', 'report/menuSetPrioritas?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Set Prioritas Komponen'), 'title' => __('Set Prioritas Komponen'), 'class' => 'btn btn-outline-primary btn-sm'));
                    echo link_to('<i class="fa fa-users"></i>', 'report/menuSetMusrenbang?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Set Komponen Musrenbang'), 'title' => __('Set Komponen Musrenbang'), 'class' => 'btn btn-outline-warning btn-sm'));
                }
                if($sf_user->getNamaUser() == 'penanda_bappeko') {
                    $query =
                    "SELECT COUNT(*) AS jumlah
                    FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni_mondalev
                    WHERE kegiatan_code = '" . $master_kegiatan->getKodeKegiatan() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    if($rs->next()) $jml_mondalev = $rs->getString('jumlah');
                    if($jml_mondalev > 0) {
                        echo link_to('<i class="fa fa-lock"></i>', 'report/prosesBukaGembokMondalev?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&aksi=Tutup', array('alt' => __('Tutup Gembok Mondalev'), 'title' => __('Tutup Gembok Mondalev'), 'class' => 'btn btn-outline-danger btn-sm'));
                    } else {
                        echo ' ';
                        echo link_to('<i class="fa fa-unlock"></i>',  'report/prosesBukaGembokMondalev?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&aksi=Buka',  array('alt' => __('Buka Gembok Mondalev'), 'title' => __('Buka Gembok Mondalev'), 'class' => 'btn btn-outline-primary btn-sm'));
                    }
                    echo ''.link_to('<i class="fa fa-donate"></i>', 'anggaran/penandaBtl?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Ubah Tagging Sumber Dana'), 'title' => __('Ubah tagging Sumber Dana'), 'class' => 'btn btn-outline-danger btn-sm'));
                }
                    
            } else if($sf_user->getNamaUser() == 'binaprogram' || $sf_user->getNamaUser() == 'bpk' || $sf_user->getNamaUser() == 'asisten1' || $sf_user->getNamaUser() == 'asisten2' || $sf_user->getNamaUser() == 'asisten3') {
                if(!$tahap){
                    echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanasli?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan'), 'title' => __('laporan perbandingan'), 'class' => 'btn btn-outline-secondary btn-sm')); 
                    if(sfConfig::get('app_tahap_edit') == 'pak') {                   
                      echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporanBukuputih?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan asli-bukuputih'), 'title' => __('laporan perbandingan pak bukuputih-bukubiru'), 'class' => 'btn btn-outline-secondary btn-sm'));
                     }
                }else{
                    echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporanaslisemua?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $tahap, array('alt' => __('laporan perbandingan asli-draft'), 'title' => __('laporan perbandingan asli-draft'), 'class' => 'btn btn-outline-secondary btn-sm')); 
                }
            }
        ?>
</td>