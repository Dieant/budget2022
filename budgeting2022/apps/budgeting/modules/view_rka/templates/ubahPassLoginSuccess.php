<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Ubah Password</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                    <li class="breadcrumb-item active">Ubah Password</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('view_rka/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Silahkan Ganti Password Anda
                        </h3>
                        <div class="card-tools">
                            <div class="input-group input-group-sm">
                            <?php echo form_tag('view_rka/ubahPassLogin', array('class' => 'form-horizontal')) ?>
                            <?php echo submit_tag('Keluar', array('align' => 'center', 'id' => 'keluar', 'name' => 'keluar', 'class' => 'btn btn-outline-danger btn-sm')); ?>
                            <?php echo '</form>'; ?>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('view_rka/ubahPassLogin', array('class' => 'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Password Lama</label>
                                    <?php echo form_error('pass_lama') ?>
                                    <?php echo input_password_tag('pass_lama', '', array('class' => 'form-control', 'placeholder' => 'Password Lama', 'required' => 'required')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <?php echo form_error('pass_baru') ?>
                                    <?php echo input_password_tag('pass_baru', '', array('class' => 'form-control', 'placeholder' => 'Password Baru', 'required' => 'required')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <?php echo form_error('ulang_pass_baru') ?>
                                    <?php echo input_password_tag('ulang_pass_baru', '', array('class' => 'form-control', 'placeholder' => 'Konfirmasi Password Baru', 'required' => 'required')); ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <?php echo input_hidden_tag('referer', $sf_request->getAttribute('referer')) ?>
                                    <?php echo submit_tag('Simpan', array('align' => 'center', 'id' => 'simpan', 'name' => 'simpan', 'class' => 'btn btn-outline-primary btn-sm')); ?>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
</section>