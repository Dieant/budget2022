<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_peneliti');
$i = 0;
$kode_sub = '';
$temp_rekening = '';
foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();

    if ($kode_sub != $rd->getKodeSub()) 
    {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') 
        {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(RkaMemberPeer::KODE_SUB, $kode_sub);
            $rs_rkam = RkaMemberPeer::doSelectOne($C_RKA);
            if ($rs_rkam) 
            {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7">
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <?php
            } 
        } 
        else 
        {
            $c = new Criteria();
            $c->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = RincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) 
            {
            ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7"> 
                        <b>:. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            <?php
            } 
            else 
            {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                            <td colspan="7">
                                <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b>
                            </td>
                            <td align="right">
                                <?php
                                $query2 = "select sum(nilai_anggaran) as hasil_kali "
                                        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query2);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                }
                                $ada = 'ada';
                                ?> 
                            </td>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                        <?php
                    }
                }
                if ($ada == 'tidak') {
                    if ($kode_sub != '') {
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) 
                            {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                                    <td colspan="7">
                                        <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b>
                                    </td>
                                    <td align="right">
                                        <?php
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali "
                                                . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                        }
                                        $ada = 'ada';                                    
                                        ?>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            <?php
                            }
                        }
                    }
                }
            }
        }
    } elseif (!$rd->getKodeSub()) {
        $kode_sub = '';
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) 
    {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        if ($rs_rekening) 
        {
            $rekening_name = $rs_rekening->getRekeningName();
            $subtitle_name = $rd->getSubtitle();
            $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub='$kode_sub' and status_hapus=false";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_rekening);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                $jumlah_rekening = number_format($t->getString('jumlah_rekening'), 0, ',', '.');
                if ($t->getString('jumlah_rekening') == 0) {
                    $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening "
                            . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub isnull and status_hapus=false";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query_rekening);
                    $ts = $stmt->executeQuery();
                    while ($ts->next()) {
                        $jumlah_rekening = number_format($ts->getString('jumlah_rekening'), 0, ',', '.');
                    }
                }
            }
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                <td colspan="7"><i><?php echo $rekening_code . ' ' . $rekening_name; ?></i> </td>
                <td align="right"><?php echo $jumlah_rekening ?></td>
                <td>&nbsp;</td>
            </tr>
            <?php
        }
    }
    ?>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td>
            <?php
                $kegiatan = $rd->getKegiatanCode();
                $unit = $rd->getUnitId();
                $no = $rd->getDetailNo();
                $sub = $rd->getSubtitle();
                $komponen_id = $rd->getKomponenId();

                if (($rd->getTipe() == 'FISIK' || $est_fisik) && $sf_user->getNamaUser() != 'parlemen2') {
                    $id_kelompok = 0;
                    $tot = 0;
                    $con = Propel::getConnection();
                    $query = "select count(*) as tot "
                            . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' "
                            . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $tot = $rs->getString('tot');
                    }
                    if ($tot == 0) {
                        $con = Propel::getConnection();
                        $c2 = new Criteria();
                        $c2->add(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE);
                        $c2->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK', Criteria::EQUAL);
                        $c2->addOr(KomponenPeer::KOMPONEN_TIPE, 'EST', Criteria::EQUAL);
                        $rd2 = KomponenPeer::doSelectOne($c2);
                        if ($rd2) {
                            $komponen_id = $rd2->getKomponenId();
                            $satuan = $rd2->getSatuan();
                        } else {
                            $komponen_id = '0';
                            $satuan = '';
                        }

                        if ($komponen_id == '0') {
                            $query2 = "select * from master_kelompok_gmap where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                        } else {
                            $query2 = "select * from master_kelompok_gmap where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                        }
                        $stmt2 = $con->prepareStatement($query2);
                        $rs2 = $stmt2->executeQuery();
                        while ($rs2->next()) {
                            $id_kelompok = $rs2->getString('id_kelompok');
                        }

                        if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                            $id_kelompok = 19;
                            if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                $id_kelompok = 100;
                            } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                $id_kelompok = 101;
                            }
                        }
                    } else {
                        $con = Propel::getConnection();
                        $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $mlokasi = $rs->getString('mlokasi');
                            $id_kelompok = $rs->getString('id_kelompok');
                        }

                        $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $total_lokasi = $rs->getString('total_lokasi');
                        }
                    }

                    if ($tot > 0) {
                        ?>
                        <div class="btn-group">                            
                            <?php
                            echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                            echo link_to('<i class="fa fa-search"></i> View Lokasi', sfConfig::get('app_path_gmap') . 'viewData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                            ?>                            
                        </div>
                        <?php
                    }
                }
                ?>
        </td>
        <?php 
        if ((($rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL) or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL)) and $rd->getStatusHapus() == false) 
        {
        ?>
        <td style="background: #e8f3f1">
            <?php
                echo $rd->getKomponenName() . ' ';
                if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                    echo ' ' . $rd->getDetailName() . ' ' . $rd->getLokasiKecamatan() . ' ' . $rd->getLokasiKelurahan();
                }
            ?>
        </td>
        <td style="background: #e8f3f1" align="center"><?php echo $rd->getSatuan(); ?></td>
        <td style="background: #e8f3f1" align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $komponen_id = $rd->getKomponenId();
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
            }
            ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td style="background: #e8f3f1" align="right"><?php echo $rd->getPajak() . '%'; ?></td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, '$total,', '.');
            ?>
        </td>
        <td style="background: #e8f3f1" align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <?php
        } 
        else 
        {
        ?>
        <td>
            <?php
                echo $rd->getKomponenName() . ' ';
                if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') 
                {
                    echo ' ' . $rd->getDetailName() . ' ' . $rd->getLokasiKecamatan() . ' ' . $rd->getLokasiKelurahan();
                }
                $rekno = $rd->getRekeningCode();
            ?>
        </td>
        <td align="center"><?php echo $rd->getSatuan(); ?></td>
        <td align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
        <td align="right">
            <?php
            $komponen_id = $rd->getKomponenId();
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
            }
            ?>
        </td>
        <td align="right"><?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td align="right"><?php echo $rd->getPajak() . '%'; ?></td>
        <td align="right">
            <?php
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, ',', '.');
            ?>    
        </td>
        <td align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>           
        <?php 
        } 
        ?>
    </tr>
<?php
endforeach;
?>