<th id="sf_admin_list_th_kodekegiatan">
    <?php echo __('ID') ?>
</th>
<th id="sf_admin_list_th_namakegiatan">
    <?php echo __('Nama Sub Kegiatan') ?>
</th>
<?php
if ($sf_user->getNamaUser() != 'kemenpan_rb') {
?>
<!-- <th id="sf_admin_list_th_alokasidana">
    <?php echo __('Nilai Semula') ?>
</th> -->
<?php if (sfConfig::get('app_tahap_edit') <> 'pak' && sfConfig::get('app_tahap_edit') <> 'murni') { ?>
   <th class="align-middle">
    <?php echo __('Nilai Semula') ?>
</th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
    <th class="align-middle">
        <?php  echo __('Pagu') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <th id="sf_admin_list_th_alokasidana">
        <?php  echo __('Revisi 6') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') == 'pak') { ?>
    <th class="align-middle">
        <?php echo __('Pagu KUA PPAS') ?>
    </th>
<?php } ?>
<?php if (sfConfig::get('app_tahap_edit') != 'pak'  && sfConfig::get('app_tahap_edit') <> 'murni') { ?>
<th id="sf_admin_list_th_alokasidana">
    <?php echo __('Nilai Menjadi') ?>
</th>
<?php } ?>
<?php } ?>
<!-- <th id="sf_admin_list_th_alokasidana">
    <?php echo __('Tambahan') ?>
</th> -->
<th id="sf_admin_list_th_nilairincian">
    <?php echo __('Nilai rincian') ?>
</th>
<?php
if ($sf_user->getNamaUser() != 'kemenpan_rb') {
?>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Selisih') ?>
</th>
<?php if (sfConfig::get('app_tahap_edit') == 'pak' || sfConfig::get('app_tahap_edit') == 'murni') { ?>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Tambahan Pagu') ?>
</th>
<?php } ?>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Posisi') ?>
</th>
<th id="sf_admin_list_th_selisih">
    <?php echo __('Approval') ?>
</th>
<?php } ?>
<th id="sf_admin_list_th_tahap">
    <?php echo __('Action') ?>
</th>
