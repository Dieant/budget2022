<?php

if (sfConfig::get('app_tahap_edit') == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
} elseif (sfConfig::get('app_tahap_edit') == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
} elseif (sfConfig::get('app_tahap_edit') == 'murni') {
    $tabel_dpn = 'murni_';
} elseif (sfConfig::get('app_tahap_edit') == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif (sfConfig::get('app_tahap_edit') == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} elseif (sfConfig::get('app_tahap_edit') == 'murnibbpraevagub') {
    $tabel_dpn = 'murni_bukubiru_praevagub_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi1') {
    $tabel_dpn = 'revisi1_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi1_1') {
    $tabel_dpn = 'revisi1_1_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi2') {
    $tabel_dpn = 'revisi2_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi2_1') {
    $tabel_dpn = 'revisi2_1_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi2_2') {
    $tabel_dpn = 'revisi2_2_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi3') {
    $tabel_dpn = 'revisi3_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi3_1') {
    $tabel_dpn = 'revisi3_1_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi4') {
    $tabel_dpn = 'revisi4_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi5') {
    $tabel_dpn = 'revisi5_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi6') {
    $tabel_dpn = 'revisi6_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi7') {
    $tabel_dpn = 'revisi7_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi8') {
    $tabel_dpn = 'revisi8_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi9') {
    $tabel_dpn = 'revisi9_';
} elseif (sfConfig::get('app_tahap_edit') == 'revisi10') {
    $tabel_dpn = 'revisi10_';
} else {
    $tabel_dpn = 'dinas_';
}
?>
<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <?php include_partial('list_th_tabular') ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod(++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <?php include_partial('list_td_tabular', array('master_kegiatan' => $master_kegiatan)) ?>
                    <?php include_partial('list_td_actions', array('master_kegiatan' => $master_kegiatan)) ?>
                </tr>
            <?php endforeach; ?>
            <tr class="sf_admin_row_3">
                <td colspan="2">&nbsp;</td>
                <?php
                if ($sf_user->getNamaUser() != 'kemenpan_rb') {
                ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "where unit_id='$unit_id' and status_hapus=false";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_awal = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai_awal, 0, ',', '.')."</b>";
                    ?>
                </td>
                <td align="right">
                    <?php                    
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                    . "where unit_id='$unit_id'";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $tambahanPagu = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($tambahanPagu, 0, ',', '.')."</b>";
                    ?>
                </td>                
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "where unit_id='$unit_id' and status_hapus=FALSE";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai, 0, ',', '.')."</b>";
                    ?>
                </td>
                <td align="right">
                    <?php
                    $selisih = $nilai_awal - $nilai;
                    echo "<b>".number_format($selisih, 0, ',', '.')."</b>";
                    ?>
                </td>
                <?php
                      }
                ?>
                <?php 
                 if ($sf_user->getNamaUser() == 'kemenpan_rb') {
                ?>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail "
                    . "where unit_id='$unit_id' and status_hapus=FALSE";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai, 0, ',', '.')."</b>";
                    ?>
                </td>
               <?php } ?>
                <td colspan="1">&nbsp;</td>
            </tr>
        </tbody>        
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
        echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'view_rka/list?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "view_rka/list?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'view_rka/list?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
        endif;
        ?>
    </ul>
</div>