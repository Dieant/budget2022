<td class="tombol_actions">

    <?php
    if ($sf_user->getNamaUser() == 'kemenpan_rb') {

            echo link_to('<i class="fa fa-edit"></i>', 'report/TemplateRKA?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId(). '&tahap=murni', array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'), 'class' => 'btn btn-outline-primary btn-sm')); 
        } 
        else
        {
    	echo link_to('<i class="fa fa-edit"></i>', 'view_rka/edit?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $master_kegiatan->getTahap(), array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'), 'class' => 'btn btn-outline-primary btn-sm')); 
        echo link_to('<i class="fa fa-list-alt"></i>', 'report/tampillaporan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Perbandingan Komponen'), 'title' => __('Perbandingan Komponen'), 'class' => 'btn btn-outline-info btn-sm'));   
        echo link_to('<i class="fa fa-list-ol"></i>', 'report/bandingRekeningSimple?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Rekening'), 'title' => __('Laporan Perbandingan Rekening'), 'class' => 'btn btn-outline-warning btn-sm'));
      }
    ?>
</td>