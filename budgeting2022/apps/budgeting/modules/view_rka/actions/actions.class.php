<?php

/**
 * view_rka actions.
 *
 * @package    budgeting
 * @subpackage view_rka
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class view_rkaActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeApiProgramKegiatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        if(isset($kode_kegiatan) && $kode_kegiatan != ''){
            $queryKodeKegiatan = " and mk.kode_kegiatan='$kode_kegiatan'";
        }else{
            $queryKodeKegiatan = '';
        }

        $query =
        "SELECT uk.unit_id, uk.unit_name,mk.kode_kegiatan, mk.kode_head_kegiatan, mk.kegiatan_id, mk.nama_kegiatan, mu.kode_urusan, mu.nama_urusan, mb.kode_bidang, mb.nama_bidang, mp.kode_program2, mp.nama_program2,
        sum(rd.nilai_anggaran) as alokasi_dana
        FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan mk
        INNER JOIN " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd ON mk.kode_kegiatan = rd.kegiatan_code
        INNER JOIN " . sfConfig::get('app_default_schema') . ".master_urusan mu ON mk.kode_urusan = mu.kode_urusan
        INNER JOIN " . sfConfig::get('app_default_schema') . ".master_bidang mb ON mk.kode_bidang = mb.kode_bidang
        INNER JOIN " . sfConfig::get('app_default_schema') . ".master_program2 mp ON mk.kode_program2 = mp.kode_program2
        INNER JOIN unit_kerja uk ON mk.unit_id = uk.unit_id
        WHERE rd.unit_id <> '9999' and rd.status_hapus = FALSE and uk.unit_id = '$unit_id' ".$queryKodeKegiatan."
        GROUP BY uk.unit_id,  mk.kode_kegiatan, mk.kode_head_kegiatan, mk.kegiatan_id, mk.nama_kegiatan, mu.kode_urusan, mu.nama_urusan, mb.kode_bidang, mb.nama_bidang, mp.kode_program2, mp.nama_program2
        ORDER BY uk.unit_name, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $data = array();
        $idx = 0;
        while($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $kode_kegiatan = $rs->getString('kode_kegiatan');
            $data['data'][$idx]['unit_id'] = $rs->getString('unit_id');
            $data['data'][$idx]['unit_name'] = $rs->getString('unit_name');
            $data['data'][$idx]['id_kegiatan'] = $rs->getString('kode_kegiatan');
            $data['data'][$idx]['kode_urusan'] = $rs->getString('kode_urusan');
            $data['data'][$idx]['nama_urusan'] = $rs->getString('nama_urusan');
            $data['data'][$idx]['kode_bidang'] = $rs->getString('kode_bidang');
            $data['data'][$idx]['nama_bidang'] = $rs->getString('nama_bidang');
            $data['data'][$idx]['kode_program'] = $rs->getString('kode_program2');
            $data['data'][$idx]['nama_program'] = $rs->getString('nama_program2');
            $data['data'][$idx]['kode_head_kegiatan'] = $rs->getString('kode_head_kegiatan');

            $c = new Criteria();
            $c->add(MasterHeadKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->add(MasterHeadKegiatanPeer::KODE_HEAD_KEGIATAN, $rs->getString('kode_head_kegiatan'), Criteria::EQUAL);
            $rs_head = MasterHeadKegiatanPeer::doSelectOne($c);
            $data['data'][$idx]['nama_head_kegiatan'] = $rs_head->getNamaHeadKegiatan();

            $data['data'][$idx]['kode_kegiatan'] = $rs->getString('kegiatan_id');
            $data['data'][$idx]['nama_kegiatan'] = $rs->getString('nama_kegiatan');
            $data['data'][$idx]['anggaran'] = $rs->getString('alokasi_dana');
            
            $c = new Criteria();
            $c->add(TujuanKotaPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(TujuanKotaPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = TujuanKotaPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['tujuan_kota'][$idx_indikator]['id'] = $value->getIdTujuan();
                $data['data'][$idx]['tujuan_kota'][$idx_indikator]['nama_tujuan'] = $value->getTujuanKota();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(IndikatorTujuanKotaPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(IndikatorTujuanKotaPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = IndikatorTujuanKotaPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['indikator_tujuan_kota'][$idx_indikator]['id'] = $value->getId();
                $data['data'][$idx]['indikator_tujuan_kota'][$idx_indikator]['indikator'] = $value->getIndikatorTujuan();
                $data['data'][$idx]['indikator_tujuan_kota'][$idx_indikator]['nilai'] = $value->getNilai();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(TujuanPdPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(TujuanPdPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = TujuanPdPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['tujuan_pd'][$idx_indikator]['id'] = $value->getIdTujuan();
                $data['data'][$idx]['tujuan_pd'][$idx_indikator]['nama_tujuan'] = $value->getTujuanPd();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(IndikatorTujuanPdPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(IndikatorTujuanPdPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = IndikatorTujuanPdPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['indikator_tujuan_pd'][$idx_indikator]['id'] = $value->getId();
                $data['data'][$idx]['indikator_tujuan_pd'][$idx_indikator]['indikator'] = $value->getIndikatorTujuan();
                $data['data'][$idx]['indikator_tujuan_pd'][$idx_indikator]['nilai'] = $value->getNilai();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(SasaranKotaPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(SasaranKotaPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = SasaranKotaPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['sasaran_kota'][$idx_indikator]['id'] = $value->getIdSasaran();
                $data['data'][$idx]['sasaran_kota'][$idx_indikator]['nama_sasaran'] = $value->getSasaranKota();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(IndikatorSasaranKotaPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(IndikatorSasaranKotaPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = IndikatorSasaranKotaPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['indikator_sasaran_kota'][$idx_indikator]['id'] = $value->getId();
                $data['data'][$idx]['indikator_sasaran_kota'][$idx_indikator]['indikator'] = $value->getIndikatorSasaran();
                $data['data'][$idx]['indikator_sasaran_kota'][$idx_indikator]['nilai'] = $value->getNilai();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(SasaranPdPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(SasaranPdPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = SasaranPdPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['sasaran_pd'][$idx_indikator]['id'] = $value->getIdSasaran();
                $data['data'][$idx]['sasaran_pd'][$idx_indikator]['nama_sasaran'] = $value->getSasaranPd();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(IndikatorSasaranPdPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(IndikatorSasaranPdPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = IndikatorSasaranPdPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['indikator_sasaran_pd'][$idx_indikator]['id'] = $value->getId();
                $data['data'][$idx]['indikator_sasaran_pd'][$idx_indikator]['indikator'] = $value->getIndikatorSasaran();
                $data['data'][$idx]['indikator_sasaran_pd'][$idx_indikator]['nilai'] = $value->getNilai();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(IndikatorProgramKotaPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(IndikatorProgramKotaPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = IndikatorProgramKotaPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $data['data'][$idx]['indikator_program_kota'][$idx_indikator]['id'] = $value->getId();
                $data['data'][$idx]['indikator_program_kota'][$idx_indikator]['indikator'] = $value->getIndikatorProgram();
                $data['data'][$idx]['indikator_program_kota'][$idx_indikator]['nilai'] = $value->getNilai();
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(OutputKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(OutputKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = OutputKegiatanPeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $output = $value->getOutput();
                $output_kegiatan = explode("|", $output);
                $data['data'][$idx]['output_kegiatan'][$idx_indikator]['tolak_ukur'] = $output_kegiatan[0];
                $data['data'][$idx]['output_kegiatan'][$idx_indikator]['target'] = $output_kegiatan[1];
                $data['data'][$idx]['output_kegiatan'][$idx_indikator]['satuan'] = $output_kegiatan[2];
                $idx_indikator++;
            }

            $c = new Criteria();
            $c->add(OutputSubtitlePeer::UNIT_ID, $unit_id, Criteria::EQUAL);
            $c->addAnd(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
            $rs_indikator = OutputSubtitlePeer::doSelect($c);
            $idx_indikator = 0;
            foreach($rs_indikator as $key => $value) {
                $kinerja = $value->getOutput();
                $lokasi_pelaksana = $value->getLokasiPelaksana();
                $kinerja_output = explode("|", $kinerja);
                $data['data'][$idx]['output'][$idx_indikator]['tolak_ukur'] = $kinerja_output[0];
                $data['data'][$idx]['output'][$idx_indikator]['target'] = $kinerja_output[1];
                $data['data'][$idx]['output'][$idx_indikator]['satuan'] = $kinerja_output[2];
                $data['data'][$idx]['output'][$idx_indikator]['lokasi_pelaksana'] = $lokasi_pelaksana;
                $idx_indikator++;
            }


            $idx++;
        }

        return $this->renderText(json_encode($data));
    }

    public function executeRekeningList() {
        $this->processFiltersrekening();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/rekening/filters');

        $this->pager = new sfPropelPager('Rekening', 25);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $c->setDistinct();
        $this->addFiltersCriteriarekeningList($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersrekening() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/rekening/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/rekening/filters');
        }
    }
    
     protected function addFiltersCriteriarekeningList($c) {
        $cek = $this->getRequestParameter('search_option');

        if ($cek == 'rekening_code') {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_CODE, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_CODE, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } elseif ($cek == 'rekening_name') {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    public function executeApiKegiatanRevisi() {
        $query =
        "SELECT DISTINCT uk.unit_id, uk.kode_permen, uk.unit_name, mk.kode_kegiatan, mk.nama_kegiatan, mk.catatan_pembahasan
        FROM " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
        INNER JOIN " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan mk ON rd.kegiatan_code = mk.kode_kegiatan
        INNER JOIN " . sfConfig::get('app_default_schema') . ".rekening r ON rd.rekening_code = r.rekening_code
        INNER JOIN unit_kerja uk ON rd.unit_id = uk.unit_id
        WHERE rd.status_hapus = FALSE
        AND uk.unit_id <> '9999'
        AND ((mk.catatan IS NOT NULL AND trim(mk.catatan) <> '') OR (mk.catatan_pembahasan IS NOT NULL AND trim(mk.catatan_pembahasan) <> ''))
        AND ((rd.note_peneliti IS NOT NULL AND rd.note_peneliti <> '') OR (rd.note_skpd IS NOT NULL AND rd.note_skpd <> ''))
        ORDER BY uk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $data = array();
        $idx = 1;
        while($rs->next()) {
            $data['kegiatan'][$idx]['unit_id'] = $rs->getString('unit_id');
            $data['kegiatan'][$idx]['kode_permen'] = $rs->getString('kode_permen');
            $data['kegiatan'][$idx]['unit_name'] = $rs->getString('unit_name');
            $data['kegiatan'][$idx]['kode_kegiatan'] = $rs->getString('kode_kegiatan');
            $data['kegiatan'][$idx]['nama_kegiatan'] = $rs->getString('nama_kegiatan');
            $data['kegiatan'][$idx]['catatan_pembahasan'] = $rs->getString('catatan_pembahasan');
            $idx++;
        }

        return $this->renderText(json_encode($data));
    }
    
    public function executeApiKegiatanRevisi1() {
        $query =
        "SELECT DISTINCT uk.unit_id, uk.kode_permen, uk.unit_name, mk.kode_kegiatan, mk.nama_kegiatan, mk.catatan_pembahasan
        FROM " . sfConfig::get('app_default_schema') . ".revisi1_rincian_detail rd
        INNER JOIN " . sfConfig::get('app_default_schema') . ".revisi1_master_kegiatan mk ON rd.kegiatan_code = mk.kode_kegiatan
        INNER JOIN " . sfConfig::get('app_default_schema') . ".rekening r ON rd.rekening_code = r.rekening_code
        INNER JOIN unit_kerja uk ON rd.unit_id = uk.unit_id
        WHERE rd.status_hapus = FALSE
        AND uk.unit_id <> '9999'
        AND ((mk.catatan IS NOT NULL AND trim(mk.catatan) <> '') OR (mk.catatan_pembahasan IS NOT NULL AND trim(mk.catatan_pembahasan) <> ''))
        AND ((rd.note_peneliti IS NOT NULL AND rd.note_peneliti <> '') OR (rd.note_skpd IS NOT NULL AND rd.note_skpd <> ''))
        ORDER BY uk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $data = array();
        $idx = 1;
        while($rs->next()) {
            $data['kegiatan'][$idx]['unit_id'] = $rs->getString('unit_id');
            $data['kegiatan'][$idx]['kode_permen'] = $rs->getString('kode_permen');
            $data['kegiatan'][$idx]['unit_name'] = $rs->getString('unit_name');
            $data['kegiatan'][$idx]['kode_kegiatan'] = $rs->getString('kode_kegiatan');
            $data['kegiatan'][$idx]['nama_kegiatan'] = $rs->getString('nama_kegiatan');
            $data['kegiatan'][$idx]['catatan_pembahasan'] = $rs->getString('catatan_pembahasan');
            $idx++;
        }

        return $this->renderText(json_encode($data));
    }

    public function executeApiKegiatanRevisi2() {
        $query =
        "SELECT DISTINCT uk.unit_id, uk.kode_permen, uk.unit_name, mk.kode_kegiatan, mk.nama_kegiatan, mk.catatan_pembahasan
        FROM " . sfConfig::get('app_default_schema') . ".revisi2_rincian_detail rd
        INNER JOIN " . sfConfig::get('app_default_schema') . ".revisi2_master_kegiatan mk ON rd.kegiatan_code = mk.kode_kegiatan
        INNER JOIN " . sfConfig::get('app_default_schema') . ".rekening r ON rd.rekening_code = r.rekening_code
        INNER JOIN unit_kerja uk ON rd.unit_id = uk.unit_id
        WHERE rd.status_hapus = FALSE
        AND uk.unit_id <> '9999'
        AND ((mk.catatan IS NOT NULL AND trim(mk.catatan) <> '') OR (mk.catatan_pembahasan IS NOT NULL AND trim(mk.catatan_pembahasan) <> ''))
        AND ((rd.note_peneliti IS NOT NULL AND rd.note_peneliti <> '') OR (rd.note_skpd IS NOT NULL AND rd.note_skpd <> ''))
        ORDER BY uk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $data = array();
        $idx = 1;
        while($rs->next()) {
            $data['kegiatan'][$idx]['unit_id'] = $rs->getString('unit_id');
            $data['kegiatan'][$idx]['kode_permen'] = $rs->getString('kode_permen');
            $data['kegiatan'][$idx]['unit_name'] = $rs->getString('unit_name');
            $data['kegiatan'][$idx]['kode_kegiatan'] = $rs->getString('kode_kegiatan');
            $data['kegiatan'][$idx]['nama_kegiatan'] = $rs->getString('nama_kegiatan');
            $data['kegiatan'][$idx]['catatan_pembahasan'] = $rs->getString('catatan_pembahasan');
            $idx++;
        }

        return $this->renderText(json_encode($data));
    }

    public function executeApiKegiatanRevisiTahap() {

        $this->setLayout(false);
        $this->getResponse()->setContentType('application/json');

        $tahap = $this->getRequestParameter('tahap');
        $unit_id = $this->getRequestParameter('unit_id');
        if($unit_id)
        {
            $unit_id="= '".$unit_id."'";
        }
        else
        {
            $unit_id="<> '9999'";
        }

       
        $arr = explode("_", $tahap, 2);
        $tahap_edit = $arr[0];

        $query =
        "SELECT DISTINCT uk.unit_id, uk.kode_permen, uk.unit_name, mk.kode_kegiatan, mk.nama_kegiatan, mk.catatan_pembahasan
        FROM " . sfConfig::get('app_default_schema') . "." . $tahap . "_rincian_detail rd
        INNER JOIN " . sfConfig::get('app_default_schema') . "." . $tahap . "_master_kegiatan mk ON rd.kegiatan_code = mk.kode_kegiatan
        INNER JOIN " . sfConfig::get('app_default_schema') . ".rekening r ON rd.rekening_code = r.rekening_code
        INNER JOIN unit_kerja uk ON rd.unit_id = uk.unit_id
        WHERE rd.status_hapus = FALSE
        AND uk.unit_id " . $unit_id . "
        AND ((mk.catatan IS NOT NULL AND trim(mk.catatan) <> '') OR (mk.catatan_pembahasan IS NOT NULL AND trim(mk.catatan_pembahasan) <> ''))
        AND ((rd.note_peneliti IS NOT NULL AND rd.note_peneliti <> '') OR (rd.note_skpd IS NOT NULL AND rd.note_skpd <> ''))
        AND rd.tahap='". $tahap_edit ."' and mk.is_btl=false
        ORDER BY uk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $data = array();
        $idx = 1;
        while($rs->next()) {
            $data['kegiatan'][$idx]['unit_id'] = $rs->getString('unit_id');
            $data['kegiatan'][$idx]['kode_permen'] = $rs->getString('kode_permen');
            $data['kegiatan'][$idx]['unit_name'] = $rs->getString('unit_name');
            $data['kegiatan'][$idx]['kode_kegiatan'] = $rs->getString('kode_kegiatan');
            $data['kegiatan'][$idx]['nama_kegiatan'] = $rs->getString('nama_kegiatan');
            $data['kegiatan'][$idx]['catatan_pembahasan'] = $rs->getString('catatan_pembahasan');
            $idx++;
        }

        return $this->renderText(json_encode($data));
    }

    public function executeApiKomponen() {
        $komponen_name = $this->getRequestParameter('q');
        $komponen_id = $this->getRequestParameter('kode');

        $data = array();
        // $query =
        // "(SELECT komponen_id, komponen_name, satuan, komponen_harga,komponen_harga_bulat,komponen_non_pajak, kd.kode_lama
        // FROM " . sfConfig::get('app_default_schema') . ".komponen ko
        // INNER JOIN ebudget.kodefikasi kd ON ko.komponen_id = kd.kode_baru
        // WHERE komponen_name ILIKE '%$komponen_name%'
        // AND komponen_id ILIKE '%$komponen_id%')
        // UNION
        // (SELECT komponen_id, komponen_name, satuan_name, komponen_harga, komponen_harga, non_pajak, kode_lama
        // FROM " . sfConfig::get('app_default_schema') . ".komponen_all ka
        // INNER JOIN ebudget.kodefikasi kd ON ka.komponen_id = kd.kode_baru
        // INNER JOIN ebudget.satuan sa ON ka.satuan_id = sa.satuan_id
        // WHERE komponen_name ILIKE '%$komponen_name%'
        // AND komponen_id ILIKE '%$komponen_id%')
        // ORDER BY komponen_name";

        // UNION
        // (SELECT komponen_id, komponen_name, satuan_name, komponen_harga, komponen_harga, non_pajak
        // FROM " . sfConfig::get('app_default_schema') . ".komponen_all ka        
        // INNER JOIN ebudget.satuan sa ON ka.satuan_id = sa.satuan_id
        // WHERE komponen_name ILIKE '%$komponen_name%'
        // AND komponen_id ILIKE '%$komponen_id%')
        $query =
        "(SELECT komponen_id, komponen_name, satuan, komponen_harga,komponen_harga_bulat,komponen_non_pajak
        FROM " . sfConfig::get('app_default_schema') . ".komponen        
        WHERE komponen_name ILIKE '%$komponen_name%'
        AND komponen_id ILIKE '%$komponen_id%' AND komponen_tipe NOT ILIKE 'HSPK%')
        ORDER BY komponen_name";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $idx = 1;
        while($rs->next()) {
            $id = $rs->getString('komponen_id');
            $data['komponen'][$idx]['komponen_id'] = $id;
            $data['komponen'][$idx]['komponen_name'] = $rs->getString('komponen_name');
            $data['komponen'][$idx]['satuan'] = $rs->getString('satuan');
            $data['komponen'][$idx]['komponen_harga'] = $rs->getString('komponen_harga');
            $data['komponen'][$idx]['komponen_harga_bulat'] = $rs->getString('komponen_harga_bulat');
            $data['komponen'][$idx]['pajak'] = $rs->getBoolean('komponen_non_pajak') ? 0 : 10;
            //$data['komponen'][$idx]['kode_lama'] = $rs->getString('kode_lama');
            $c_rek = new Criteria();
            $c_rek->add(KomponenRekeningPeer::KOMPONEN_ID, $id);
            $reks = KomponenRekeningPeer::doSelect($c_rek);
            $idx_rek = 1;
            foreach($reks as $key_rek => $value_rek) {
                $data['komponen'][$idx]['rekening'][$idx_rek] = $value_rek->getRekeningCode();               
                $r_name = new Criteria();
                $r_name->add(RekeningPeer::REKENING_CODE, $value_rek->getRekeningCode());
                $rnames = RekeningPeer::doSelect($r_name);
                foreach($rnames as $n_rek => $name_rek) {
                $data['komponen'][$idx]['rekening_name'][$idx_rek] = $name_rek->getRekeningName();
                }
                 $idx_rek++;
            }
            $idx++;
        }

        return $this->renderText(json_encode($data));
    }
    
     public function executeApiTarikSabkRincian() {
        $this->setLayout(false);
        $this->getResponse()->setContentType('application/json');

        $tahap = $this->getRequestParameter('tahap');
       // $tahap='dinas';
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        
        
    if ($unit_id && $kode_kegiatan) {
         $query = "SELECT unit.unit_name, unit.unit_id,
            (
                select kelompok.front_code 
                from kelompok_dinas kelompok INNER JOIN  unit_kerja un ON un.kelompok_id = kelompok.kelompok_id 
                where un.unit_id = unit.unit_id
            ) as front_code,
            kegiatan.kode_kegiatan,kegiatan.kegiatan_id,kode_urusan,kode_bidang,kode_program2, kode_sasaran, kegiatan.nama_kegiatan, kegiatan.masukan , kegiatan.output , kegiatan.outcome , kegiatan.benefit , kegiatan.impact ,kegiatan.alokasi_dana, kegiatan.catatan,
            (
                select kode_permen 
                from unit_kerja 
                where unit_id='$unit_id'
            ) as kode_permen                
            FROM 
                " . sfConfig::get('app_default_schema')  . "." . $tahap . "_rincian rincian INNER JOIN  " . sfConfig::get('app_default_schema')  . "." . $tahap . "_master_kegiatan kegiatan ON kegiatan.kode_kegiatan = rincian.kegiatan_code 
                INNER JOIN unit_kerja unit ON unit.unit_id = kegiatan.unit_id and rincian.unit_id=kegiatan.unit_id
            WHERE  
                unit.unit_id = '$unit_id' and  kegiatan.kode_kegiatan='$kode_kegiatan'
            ORDER BY kegiatan.unit_id,kegiatan.kode_kegiatan";
    //echo $query."<BR>";exit;
     $con = Propel::getConnection();
     $stmt = $con->prepareStatement($query);
     $rs = $stmt->executeQuery();

    echo "//<br> kode_permen |kode_urusan.kode_bidang.kode_program2.kode_kegiatan.kode_sub_kegiatan|nama_kegiatan |catatan |subtitle |rekening_code |detail_no |komponen_id |komponen_name |detail_name |komponen_harga_awal |satuan |keterangan_koefisien |pajak |total |kode_sub |sub |volume |note_skpd |sumber_dana |penanda |penanda_dak |detail_kegiatan |accres <br>//<br>\n";

    while ($rs->next()) {       

        if ($rs->getString('front_code'))
           $front_code .= ".";       
        //$kode_program22 = substr($rs->getString('kode_program2'), 9, 2);
        $kode_kegiatan1 = $rs->getString('kegiatan_id');
        $kode_program22 = $rs->getString('kode_program2');
        $kode_urusan    =$rs->getString('kode_urusan');
        // if (substr($rs->getString('kode_program2'), 0, 4) == 'X.XX') {
        //     $kode_urusan = substr($rs->getString('kode_permen'), 0, 4);
        // } else {
        //     $kode_urusan = substr($rs->getString('kode_program2'), 0, 8);
        // }
        
         $kode_kegiatan=$rs->getString('kode_kegiatan');
         $unit_id=$rs->getString('unit_id');
         // die($kode_kegiatan . ' '.$unit_id) ;
//round((det.volume * det.komponen_harga_awal * (det.pajak+100)/100))  as total,
         // untuk yang dihapus agar tetap muncul and det.volume > 0 230621
        $query_result = "
            SELECT 
                det.subtitle,
                det.rekening_code,
                det.komponen_name,
                det.detail_name,
                det.komponen_id,
                det.detail_kegiatan,
                det.accres,
                det.komponen_harga_awal,
                det.satuan,
                det.keterangan_koefisien,det.pajak,
                det.nilai_anggaran  as total,
                                det.note_skpd,
                det.detail_no, det.volume, det.kode_sub,
                (select v.sub_name from " . sfConfig::get('app_default_schema') .".v_sub_pra_rka v where v.unit_id=det.unit_id 
                    and v.kegiatan_code=det.kegiatan_code and v.kode_sub=det.kode_sub limit 1) as sub,
                (select v.detail_name from " . sfConfig::get('app_default_schema') .".v_sub_pra_rka v where v.unit_id=det.unit_id 
                    and v.kegiatan_code=det.kegiatan_code and v.kode_sub=det.kode_sub limit 1) as detaildisub,
                msd.sumber_dana , case when det.sub_sumber_dana_id = 1 then '(BOK)' else '' end as penanda_dak, case when det.prioritas_id IN (select id from master_prioritas where prioritas ilike 'pcovid%') then '(Covid-19)' else '' end as penanda
            FROM 
                 " . sfConfig::get('app_default_schema')  . "." . $tahap . "_rincian_detail det
            INNER JOIN master_sumber_dana msd ON msd.id = det.sumber_dana_id
            WHERE 
                det.kegiatan_code = '$kode_kegiatan' and det.unit_id='$unit_id' and det.status_hapus=false and det.volume > 0
            ORDER BY 
                det.kegiatan_code,det.subtitle,det.kode_sub,det.rekening_code,det.komponen_name
                
            ";
        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query_result);
        $rs1 = $stmt1->executeQuery();
              
        while ($rs1->next()) {
           
            $buang = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","  ","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            $buangs = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            
            $nama_kegiatan = str_replace($buang, "",  $rs->getString('nama_kegiatan'));
            $catatan = str_replace($buang, "", strip_tags($rs->getString('catatan')));
            $subtitle = str_replace($buang, "", $rs1->getString('subtitle'));
            $komponen_name = str_replace($buang, "", $rs1->getString('komponen_name'));
            $detail_name = str_replace($buangs, "", $rs1->getString('detail_name'));
            $detaildisub = str_replace($buang, "", $rs1->getString('detaildisub'));
            $note_skpd = str_replace($buang, "",  $rs1->getString('note_skpd'));
            $detail_kegiatan = str_replace($buang, "",  $rs1->getString('detail_kegiatan'));
            $accres = str_replace($buangs, "", $rs1->getString('accres'));

            if ( $rs1->getString('kode_sub') and !$detail_name)
                // $detail_name = $detaildisub;
            $kode_rkam = substr($rs1->getString('kode_sub'), 0, 4);

            echo $rs->getString('kode_permen') . "|" . $kode_kegiatan1 . "|" . trim($nama_kegiatan) . "|" . trim($catatan) . "|" . trim($subtitle) . "|" . $rs1->getString('rekening_code') . "|" .  $rs1->getString('detail_no') . "|" . $rs1->getString('komponen_id'). "|" . trim($komponen_name) . "|" . trim($detail_name) . "|" . $rs1->getString('komponen_harga_awal') . "|" . $rs1->getString('satuan') . "|" . $rs1->getString('keterangan_koefisien') . "|" . $rs1->getString('pajak') . "|" . $rs1->getString('total') . "|" . $rs1->getString('kode_sub') . "|" . trim($rs1->getString('sub')) . " " . trim($detaildisub) . "|" . $rs1->getString('volume') . "|" . trim($note_skpd) . "|" . $rs1->getString('sumber_dana') .  "|" . $rs1->getString('penanda') . "|". $rs1->getString('penanda_dak') . "|" . $rs1->getString('detail_kegiatan'). "|" . $rs1->getString('accres')."<br>\n";
           
        }
    }
}  else if ($unit_id ) {
         $query = "SELECT unit.unit_name, unit.unit_id,
            (
                select kelompok.front_code 
                from kelompok_dinas kelompok INNER JOIN  unit_kerja un ON un.kelompok_id = kelompok.kelompok_id 
                where un.unit_id = unit.unit_id
            ) as front_code,
            kegiatan.kode_kegiatan,kegiatan.kegiatan_id,kode_urusan,kode_bidang,kode_program2, kode_sasaran, kegiatan.nama_kegiatan, kegiatan.masukan , kegiatan.output , kegiatan.outcome , kegiatan.benefit , kegiatan.impact ,kegiatan.alokasi_dana, kegiatan.catatan,
            (
                select kode_permen 
                from unit_kerja 
                where unit_id='$unit_id'
            ) as kode_permen                
            FROM 
                " . sfConfig::get('app_default_schema')  . "." . $tahap . "_rincian rincian INNER JOIN " . sfConfig::get('app_default_schema')  . "." . $tahap . "_master_kegiatan kegiatan ON kegiatan.kode_kegiatan = rincian.kegiatan_code 
                INNER JOIN unit_kerja unit ON unit.unit_id = kegiatan.unit_id and rincian.unit_id=kegiatan.unit_id
            WHERE  
                unit.unit_id = '$unit_id' 
            ORDER BY kegiatan.unit_id,kegiatan.kode_kegiatan";
    //echo $query."<BR>";exit;
     $con = Propel::getConnection();
     $stmt = $con->prepareStatement($query);
     $rs = $stmt->executeQuery();

    echo "//<br> kode_permen |kode_urusan.kode_program2.kode_kegiatan.kode_sub_kegiatan |nama_kegiatan |catatan |subtitle |rekening_code |detail_no |komponen_id |komponen_name |detail_name |komponen_harga_awal |satuan |keterangan_koefisien |pajak |total |kode_sub |sub |volume |note_skpd |sumber_dana<br>//<br>\n";

    while ($rs->next()) {       

        if ($rs->getString('front_code'))
           $front_code .= ".";       
        // $kode_program22 = substr($rs->getString('kode_program2'), 9, 2);

        // if (substr($rs->getString('kode_program2'), 0, 4) == 'X.XX') {
        //     $kode_urusan = substr($rs->getString('kode_permen'), 0, 4);
        // } else {
        //     $kode_urusan = substr($rs->getString('kode_program2'), 0, 8);
        // }
        $kode_kegiatan1 = $rs->getString('kegiatan_id');
        $kode_program22 = $rs->getString('kode_program2');
        $kode_urusan    =$rs->getString('kode_urusan');
        
         $kode_kegiatan=$rs->getString('kode_kegiatan');
         $unit_id=$rs->getString('unit_id');
         // die($kode_kegiatan . ' '.$unit_id) ;
//round((det.volume * det.komponen_harga_awal * (det.pajak+100)/100))  as total,
        $query_result = "
            SELECT 
                det.subtitle,
                det.rekening_code,
                det.komponen_name,
                det.detail_name,
                det.komponen_id,
                det.komponen_harga_awal,
                det.satuan,
                det.keterangan_koefisien,det.pajak,
                det.nilai_anggaran  as total,
                                det.note_skpd,
                det.detail_no, det.volume, det.kode_sub,
                (select v.sub_name from " . sfConfig::get('app_default_schema') .".v_sub_pra_rka v where v.unit_id=det.unit_id 
                    and v.kegiatan_code=det.kegiatan_code and v.kode_sub=det.kode_sub limit 1) as sub,
                (select v.detail_name from " . sfConfig::get('app_default_schema') .".v_sub_pra_rka v where v.unit_id=det.unit_id 
                    and v.kegiatan_code=det.kegiatan_code and v.kode_sub=det.kode_sub limit 1) as detaildisub,
                msd.sumber_dana
            FROM 
                 " . sfConfig::get('app_default_schema')  . "." . $tahap . "_rincian_detail det
            INNER JOIN master_sumber_dana msd ON msd.id = det.sumber_dana_id
            WHERE 
                det.kegiatan_code = '$kode_kegiatan' and det.unit_id='$unit_id' and det.status_hapus=false and det.volume > 0

            ORDER BY 
                det.kegiatan_code,det.subtitle,det.kode_sub,det.rekening_code,det.komponen_name
                
            ";
        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query_result);
        $rs1 = $stmt1->executeQuery();
             
        while ($rs1->next()) {
            
             $buang = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","  ","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            $buangs = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
             
            $nama_kegiatan = str_replace($buang, "",  $rs->getString('nama_kegiatan'));
            $catatan = str_replace($buang, "", strip_tags($rs->getString('catatan')));
            $subtitle = str_replace($buang, "", $rs1->getString('subtitle'));
            $komponen_name = str_replace($buang, "", $rs1->getString('komponen_name'));
            $detail_name = str_replace($buangs, "", $rs1->getString('detail_name'));
            $detaildisub = str_replace($buang, "", $rs1->getString('detaildisub'));
            $note_skpd = str_replace($buang, "",  $rs1->getString('note_skpd'));

            if ( $rs1->getString('kode_sub') and !$detail_name)
            // $detail_name = $detaildisub;
            $kode_rkam = substr($rs1->getString('kode_sub'), 0, 4);

            echo  $rs->getString('kode_permen') . "|" . $kode_kegiatan1 . "|" . trim($nama_kegiatan) . "|" . trim($catatan) . "|" . trim($subtitle ) . "|" . $rs1->getString('rekening_code') . "|" .  $rs1->getString('detail_no') . "|" . $rs1->getString('komponen_id'). "|" . trim($komponen_name) . "|" . trim($detail_name) . "|" . $rs1->getString('komponen_harga_awal') . "|" . $rs1->getString('satuan') . "|" . $rs1->getString('keterangan_koefisien') . "|" . $rs1->getString('pajak') . "|" . $rs1->getString('total') . "|" . $rs1->getString('kode_sub') . "|" . trim($rs1->getString('sub')) . " " . trim($detaildisub) . "|" . $rs1->getString('volume') . "|" . trim($note_skpd) . "|" . $rs1->getString('sumber_dana') . "<br>\n";
            
        }
    }
}
         return $this->renderText(json_encode());
    }

    public function executeApiTarikSabkRincianJson() {

        $this->setLayout(false);
        $this->getResponse()->setContentType('application/json');

        $tahap = $this->getRequestParameter('tahap');
       // $tahap='dinas';
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $data = array();
        $idx = 1;
          
         $query = "SELECT unit.unit_name, unit.unit_id,
            (
                select kelompok.front_code 
                from kelompok_dinas kelompok INNER JOIN  unit_kerja un ON un.kelompok_id = kelompok.kelompok_id 
                where un.unit_id = unit.unit_id
            ) as front_code,
            kegiatan.kode_kegiatan,kegiatan.kegiatan_id,kode_urusan,kode_bidang,kode_program2, kode_sasaran, kegiatan.nama_kegiatan, kegiatan.masukan , kegiatan.output , kegiatan.outcome , kegiatan.benefit , kegiatan.impact ,kegiatan.alokasi_dana, kegiatan.catatan,
            (
                select kode_permen 
                from unit_kerja 
                where unit_id='$unit_id'
            ) as kode_permen                
            FROM 
                " . sfConfig::get('app_default_schema')  . "." . $tahap . "_rincian rincian INNER JOIN  " . sfConfig::get('app_default_schema')  . "." . $tahap . "_master_kegiatan kegiatan ON kegiatan.kode_kegiatan = rincian.kegiatan_code 
                INNER JOIN unit_kerja unit ON unit.unit_id = kegiatan.unit_id and rincian.unit_id=kegiatan.unit_id
            WHERE  
                unit.unit_id = '$unit_id' and  kegiatan.kode_kegiatan='$kode_kegiatan'
            ORDER BY kegiatan.unit_id,kegiatan.kode_kegiatan";
    //echo $query."<BR>";exit;
     $con = Propel::getConnection();
     $stmt = $con->prepareStatement($query);
     $rs = $stmt->executeQuery();

    // echo "//<br> kode_permen |kode_urusan.kode_bidang.kode_program2.kode_kegiatan.kode_sub_kegiatan|nama_kegiatan |catatan |subtitle |rekening_code |detail_no |komponen_id |komponen_name |detail_name |komponen_harga_awal |satuan |keterangan_koefisien |pajak |total |kode_sub |sub |volume |note_skpd |sumber_dana |penanda |detail_kegiatan <br>//<br>\n";

    while ($rs->next()) {       

        // if ($rs->getString('front_code'))
        //    $front_code .= ".";       
       
        $kode_kegiatan1 = $rs->getString('kegiatan_id');
        $kode_program22 = $rs->getString('kode_program2');
        $kode_urusan    =$rs->getString('kode_urusan');
        
        
        $kode_kegiatan=$rs->getString('kode_kegiatan');
        $unit_id=$rs->getString('unit_id');

        
        $query_result = "
            SELECT 
                det.subtitle,
                det.rekening_code,
                det.komponen_name,
                det.detail_name,
                det.komponen_id,
                det.detail_kegiatan,
                det.komponen_harga_awal,
                det.satuan,
                det.keterangan_koefisien,det.pajak,
                det.nilai_anggaran  as total,
                                det.note_skpd,
                det.detail_no, det.volume, det.kode_sub,
                (select v.sub_name from " . sfConfig::get('app_default_schema') .".v_sub_pra_rka v where v.unit_id=det.unit_id 
                    and v.kegiatan_code=det.kegiatan_code and v.kode_sub=det.kode_sub limit 1) as sub,
                (select v.detail_name from " . sfConfig::get('app_default_schema') .".v_sub_pra_rka v where v.unit_id=det.unit_id 
                    and v.kegiatan_code=det.kegiatan_code and v.kode_sub=det.kode_sub limit 1) as detaildisub,
                msd.sumber_dana , case when det.prioritas_id IN (select id from master_prioritas where prioritas ilike 'pcovid%') then '(Covid-19)' else '' end as penanda,case when det.sub_sumber_dana_id = 1 then '(BOK)' else '' end as penanda_dak
            FROM 
                 " . sfConfig::get('app_default_schema')  . "." . $tahap . "_rincian_detail det
            INNER JOIN master_sumber_dana msd ON msd.id = det.sumber_dana_id
            WHERE 
                det.kegiatan_code = '$kode_kegiatan' and det.unit_id='$unit_id' and det.status_hapus=false and det.volume > 0
            ORDER BY 
                det.kegiatan_code,det.subtitle,det.kode_sub,det.rekening_code,det.komponen_name
                
            ";
        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query_result);
        $rs1 = $stmt1->executeQuery();
        
              
        while ($rs1->next()) {


           
            $buang = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","  ","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            $buangs = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            
            $nama_kegiatan = str_replace($buang, "",  $rs->getString('nama_kegiatan'));
            $catatan = str_replace($buang, "", strip_tags($rs->getString('catatan')));
            $subtitle = str_replace($buang, "", $rs1->getString('subtitle'));
            $komponen_name = str_replace($buang, "", $rs1->getString('komponen_name'));
            $detail_name = str_replace($buangs, "", $rs1->getString('detail_name'));
            $detaildisub = str_replace($buang, "", $rs1->getString('detaildisub'));
            $note_skpd = str_replace($buang, "",  $rs1->getString('note_skpd'));
            $detail_kegiatan = str_replace($buang, "",  $rs1->getString('detail_kegiatan'));


            if ( $rs1->getString('kode_sub') and !$detail_name)
                // $detail_name = $detaildisub;
            $kode_rkam = substr($rs1->getString('kode_sub'), 0, 4);

            // echo "//<br> kode_permen |kode_urusan.kode_bidang.kode_program2.kode_kegiatan.kode_sub_kegiatan|nama_kegiatan |catatan |subtitle |rekening_code |detail_no |komponen_id |komponen_name |detail_name |komponen_harga_awal |satuan |keterangan_koefisien |pajak |total |kode_sub |sub |volume |note_skpd |sumber_dana |penanda |detail_kegiatan <br>//<br>\n";

            // echo $rs->getString('kode_permen') . "|" . $kode_kegiatan1 . "|" . trim($nama_kegiatan) . "|" . trim($catatan) . "|" . trim($subtitle) . "|" . $rs1->getString('rekening_code') . "|" .  $rs1->getString('detail_no') . "|" . $rs1->getString('komponen_id'). "|" . trim($komponen_name) . "|" . trim($detail_name) . "|" . $rs1->getString('komponen_harga_awal') . "|" . $rs1->getString('satuan') . "|" . $rs1->getString('keterangan_koefisien') . "|" . $rs1->getString('pajak') . "|" . $rs1->getString('total') . "|" . $rs1->getString('kode_sub') . "|" . trim($rs1->getString('sub')) . " " . trim($detaildisub) . "|" . $rs1->getString('volume') . "|" . trim($note_skpd) . "|" . $rs1->getString('sumber_dana') .  "|" . $rs1->getString('penanda') . "|" . $rs1->getString('detail_kegiatan')."<br>\n";
           
           //masukkan ke array
            $data['sub_kegiatan'][$idx]['kode_permen'] = $rs->getString('kode_permen');
            $data['sub_kegiatan'][$idx]['kode_urusan.kode_bidang.kode_program2.kode_kegiatan.kode_sub_kegiatan'] = $kode_kegiatan1;
            $data['sub_kegiatan'][$idx]['nama_kegiatan'] = trim($nama_kegiatan);
            $data['sub_kegiatan'][$idx]['catatan'] =  trim($catatan);
            $data['sub_kegiatan'][$idx]['subtitle'] = trim($subtitle);
            $data['sub_kegiatan'][$idx]['rekening_code'] = $rs1->getString('rekening_code');
            $data['sub_kegiatan'][$idx]['detail_no'] = $rs1->getString('detail_no');
            $data['sub_kegiatan'][$idx]['komponen_id'] = $rs1->getString('komponen_id');
            $data['sub_kegiatan'][$idx]['komponen_name'] = trim($komponen_name);
            $data['sub_kegiatan'][$idx]['detail_name'] = trim($detail_name);
            $data['sub_kegiatan'][$idx]['komponen_harga_awal'] = $rs1->getString('komponen_harga_awal');
            $data['sub_kegiatan'][$idx]['satuan'] = $rs1->getString('satuan');
            $data['sub_kegiatan'][$idx]['keterangan_koefisien'] = $rs1->getString('keterangan_koefisien');
            $data['sub_kegiatan'][$idx]['pajak'] = $rs1->getString('pajak');
            $data['sub_kegiatan'][$idx]['total'] = $rs1->getString('total');
            $data['sub_kegiatan'][$idx]['kode_sub'] = $rs1->getString('kode_sub');
            $data['sub_kegiatan'][$idx]['sub'] = trim($rs1->getString('sub')) . " " . trim($detaildisub);
            $data['sub_kegiatan'][$idx]['volume'] = $rs1->getString('volume');
            $data['sub_kegiatan'][$idx]['note_skpd'] = trim($note_skpd);
            $data['sub_kegiatan'][$idx]['sumber_dana'] = $rs1->getString('sumber_dana');
            $data['sub_kegiatan'][$idx]['penanda'] = $rs1->getString('penanda');
            $data['sub_kegiatan'][$idx]['penanda_dak'] = $rs1->getString('penanda_dak');
            $data['sub_kegiatan'][$idx]['detail_kegiatan'] = $rs1->getString('detail_kegiatan');
            $idx++;
        }
    }
    
  
        return $this->renderText(json_encode($data));
    }

    public function executeApiTarikCatatanSubRincianSabk() {

        $this->setLayout(false);
        $this->getResponse()->setContentType('application/json');

        $tahap = $this->getRequestParameter('tahap');
       // $tahap='dinas';
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $data = array();
        $idx = 1;

        if($kode_kegiatan&& $tahap && $unit_id!='')
        {
            $query = "SELECT * from ebudget.catatan_rekening where kegiatan_code='$kode_kegiatan' and unit_id='$unit_id' and tahap='$tahap'";
        }
        else if ( $tahap && $unit_id)
        {
             $query = "SELECT * from ebudget.catatan_rekening where unit_id='$unit_id' and tahap='$tahap'";
        }
        else
        {
             $query = "SELECT * from ebudget.catatan_rekening";
        }
   
     $con = Propel::getConnection();
     $stmt = $con->prepareStatement($query);
     $rs = $stmt->executeQuery();

    while ($rs->next()) {       
  
        $kode_kegiatan = $rs->getString('kegiatan_code');
        $unit_id= $rs->getString('unit_id');
        $subrincian    =$rs->getString('rekening_code');
        $catatan   =$rs->getString('catatan');
        $tahap  =$rs->getString('tahap');
        
        
        
            $buang = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","  ","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            $buangs = array("\t", "\n", "\r", "\0", "\x0B", "|","<li>","<\li>","<ul>","<\ul>","&nbsp;", "&amp;", "Å", "&Aring;", "&#160;", "&#197;","<ol>","</ol>");
            
           
            $catatan = str_replace($buang, "", strip_tags($rs->getString('catatan')));         

           
           //masukkan ke array
            $data['sub_rincian'][$idx]['unit_id'] = $rs->getString('unit_id');
            $data['sub_rincian'][$idx]['kegiatan_code'] = $rs->getString('kegiatan_code');
            $data['sub_rincian'][$idx]['rekening_code'] = $rs->getString('rekening_code');
            $data['sub_rincian'][$idx]['catatan'] = $rs->getString('catatan');
            $data['sub_rincian'][$idx]['tahap'] = $rs->getString('tahap');             
            $idx++;
        
         }
    
  
        return $this->renderText(json_encode($data));
    }



     public function executeApiTarikCapaianProgram() {
        $unit_id = $this->getRequestParameter('unit_id');

        $query = "select u.kode_permen, ipp.kode_kegiatan, ipp.indikator_program, ipp.nilai
            from " . sfConfig::get('app_default_schema') . ".indikator_program_pd ipp, unit_kerja u
            where ipp.unit_id=u.unit_id and ipp.unit_id='$unit_id'
            order by ipp.kode_kegiatan, ipp.indikator_program";

        echo "//<br> kode_permen |kode_kegiatan |capaian program (tolak ukur) |capaian program (target kinerja)  <br>//<BR>\n";

        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query);
        $rs1 = $stmt1->executeQuery();
             
        while ($rs1->next()) {          

        echo $rs1->getString('kode_permen') . "|" . $rs1->getString('kode_kegiatan') . "|" . trim($rs1->getString('indikator_program')) . "|" . trim($rs1->getString('nilai'))  . "<br>\n";
         }
     return $this->renderText(json_encode());
     }

          public function executeApiTarikOutcome() {
        $unit_id = $this->getRequestParameter('unit_id');       

        $query = "select u.kode_permen, k.kode_kegiatan, k.nama_kegiatan,isk.indikator_sasaran, isk.nilai    
                from unit_kerja u, " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".indikator_sasaran_kota isk
                where u.unit_id=k.unit_id and u.unit_id=isk.unit_id and k.kode_kegiatan=isk.kode_kegiatan and k.unit_id=isk.unit_id and isk.unit_id = '$unit_id'
                order by u.kode_permen, k.kode_kegiatan, isk.indikator_sasaran";

        echo "//<br> kode_permen |kode_kegiatan |capaian program (tolak ukur) |capaian program (target kinerja)  <br>//<br>\n";

        $con = Propel::getConnection();
        $stmt1 = $con->prepareStatement($query);
        $rs1 = $stmt1->executeQuery();
             
        while ($rs1->next()) {          
        echo $rs1->getString('kode_permen')  . "|" . $rs1->getString('kode_kegiatan') . "|" . $rs1->getString('nama_kegiatan') . "|" . $rs1->getString('indikator_sasaran') . "|" . $rs1->getString('nilai') . "|" . "<br>\n";
        
         }
     return $this->renderText(json_encode());
     }


    public function executeApiKpaPa() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $data = array();
        $query =
        "SELECT mk.unit_id, mk.kode_kegiatan,mk.nama_kegiatan, mu.user_id, mu.user_name, mu.user_password, mu.nip, mu.jabatan, mk.alokasi_dana+mk.tambahan_pagu as nilai_anggaran
        FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan mk
        LEFT JOIN master_user_v2 mu ON mk.user_id_pptk = mu.user_id
        WHERE 1 = 1";
        if($unit_id) $query .= " AND mk.unit_id = '$unit_id'";
        if($kode_kegiatan) $query .= " AND mk.kode_kegiatan = '$kode_kegiatan'";
        $query .= " ORDER BY mk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $idx = 0;
        while ($rs->next()) {
            $unit = $rs->getString('unit_id');
            $kode = $rs->getString('kode_kegiatan');
            $nama_kegiatan = $rs->getString('nama_kegiatan');
            $anggaran = $rs->getString('nilai_anggaran');
            // $data['kegiatan'][$unit][$kode] = $rs->getString('unit_id');
            // $data['kegiatan'][$unit][$kode] = $rs->getString('kode_kegiatan');
            $data['kegiatan'][$unit][$kode]['nama_kegiatan'] = $nama_kegiatan;
            $data['kegiatan'][$unit][$kode]['anggaran'] = $anggaran;
            $data['kegiatan'][$unit][$kode]['pptk_id'] = trim($rs->getString('user_id'));
            $data['kegiatan'][$unit][$kode]['pptk_name'] = trim($rs->getString('user_name'));
            // $data['kegiatan'][$unit][$kode]['pptk_password'] = trim($rs->getString('user_password'));
            $data['kegiatan'][$unit][$kode]['pptk_nip'] = trim($rs->getString('nip'));
            $data['kegiatan'][$unit][$kode]['pptk_jabatan'] = trim($rs->getString('jabatan'));
            $idx++;
        }
        
        $query =
        "SELECT mk.unit_id, mk.kode_kegiatan,mk.nama_kegiatan, mu.user_id, mu.user_name, mu.user_password, mu.nip, mu.jabatan, mk.alokasi_dana+mk.tambahan_pagu as nilai_anggaran
        FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan mk
        LEFT JOIN master_user_v2 mu ON mk.user_id_kpa = mu.user_id
        WHERE 1 = 1";
        if($unit_id) $query .= " AND mk.unit_id = '$unit_id'";
        if($kode_kegiatan) $query .= " AND mk.kode_kegiatan = '$kode_kegiatan'";
        $query .= " ORDER BY mk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $idx = 0;
        while ($rs->next()) {
            $unit = $rs->getString('unit_id');
            $kode = $rs->getString('kode_kegiatan');
            $nama_kegiatan = $rs->getString('nama_kegiatan');
            $anggaran = $rs->getString('nilai_anggaran');
            // $data['kegiatan'][$unit][$kode] = $rs->getString('unit_id');
            // $data['kegiatan'][$unit][$kode] = $rs->getString('kode_kegiatan');
            $data['kegiatan'][$unit][$kode]['kpa_id'] = trim($rs->getString('user_id'));
            $data['kegiatan'][$unit][$kode]['kpa_name'] = trim($rs->getString('user_name'));
            // $data['kegiatan'][$unit][$kode]['kpa_password'] = trim($rs->getString('user_password'));
            $data['kegiatan'][$unit][$kode]['kpa_nip'] = trim($rs->getString('nip'));
            $data['kegiatan'][$unit][$kode]['kpa_jabatan'] = trim($rs->getString('jabatan'));
            $idx++;
        }
        $query =
        "SELECT mk.unit_id, mk.kode_kegiatan, mu.user_id, mu.user_name, mu.user_password, mu.nip, mu.jabatan
        FROM ebudget.dinas_master_kegiatan mk
        LEFT JOIN user_handle_v2 uh ON mk.unit_id = uh.unit_id
        LEFT JOIN schema_akses_v2 sa ON uh.user_id = sa.user_id
        LEFT JOIN master_user_v2 mu ON sa.user_id = mu.user_id
        WHERE sa.level_id = 15
        AND mu.user_enable = TRUE";
        if($unit_id) $query .= " AND mk.unit_id = '$unit_id'";
        if($kode_kegiatan) $query .= " AND mk.kode_kegiatan = '$kode_kegiatan'";
        $query .= " ORDER BY mk.unit_id, mk.kode_kegiatan";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $idx = 0;
        while ($rs->next()) {
            $unit = $rs->getString('unit_id');
            $kode = $rs->getString('kode_kegiatan');
            
            $data['kegiatan'][$unit][$kode]['pa_id'] = trim($rs->getString('user_id'));
            $data['kegiatan'][$unit][$kode]['pa_name'] = trim($rs->getString('user_name'));
            // $data['kegiatan'][$unit][$kode]['pa_password'] = trim($rs->getString('user_password'));
            $data['kegiatan'][$unit][$kode]['pa_nip'] = trim($rs->getString('nip'));
            $data['kegiatan'][$unit][$kode]['pa_jabatan'] = trim($rs->getString('jabatan'));
            $idx++;
        }
        
        $query = "select d.unit_id,d.kegiatan_code,m.nama_kegiatan,sum(d.nilai_anggaran) as uk
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail d," . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan m 
        where d.unit_id=m.unit_id and d.kegiatan_code=m.kode_kegiatan and 
        d.status_hapus=false and d.subtitle='Penunjang Kinerja' and d.rekening_code='5.1.02.02.01.080'";
        if($unit_id) $query .= " AND d.unit_id = '$unit_id'";
        if($kode_kegiatan) $query .= " AND m.kode_kegiatan = '$kode_kegiatan'";
        $query .= " group by d.unit_id,d.kegiatan_code,m.nama_kegiatan ORDER BY d.unit_id, d.kegiatan_code";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $idx = 0;
        while ($rs->next()) {
            $unit = $rs->getString('unit_id');
            $kode = $rs->getString('kegiatan_code');
            $uk = $rs->getString('uk');
            $data['kegiatan'][$unit][$kode]['anggaran_uk'] = $rs->getString('uk');
            $idx++;
        }

        return $this->renderText(json_encode($data));
    }

    public function executeUbahPassLogin() {
        $username = $this->getUser()->getNamaLogin();
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if ($this->getRequestParameter('keluar')) {

                // if ($this->getUser()->hasCredential('dinas'))
                //     return $this->redirect('login/logoutDinas');
                // else if ($this->getUser()->hasCredential('dewan'))
                //     return $this->redirect('login/logoutDewan');
                // else 
                if ($this->getUser()->hasCredential('viewer'))
                    return $this->redirect('login/logoutViewer');
                // else if ($this->getUser()->hasCredential('peneliti'))
                //     return $this->redirect('login/logoutPeneliti');
                // else if ($this->getUser()->hasCredential('data'))
                //     return $this->redirect('login/logoutData');
                else
                    return $this->redirect('login/logout');
            }
            if ($this->getRequestParameter('simpan')) {
                $pass_lama = $this->getRequestParameter('pass_lama');
                $pass_baru = $this->getRequestParameter('pass_baru');
                $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
                $md5_pass_lama = md5($pass_lama);
                $md5_pass_baru = md5($pass_baru);

                $c_user = new Criteria();
                $c_user->add(MasterUserV2Peer::USER_ID, $username);
                $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
                $rs_user = MasterUserV2Peer::doSelectOne($c_user);
                if ($rs_user) {
                    if ($pass_baru == $ulang_pass_baru) {
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $newSetting = MasterUserV2Peer::retrieveByPK($username);
                            //$newSetting->setUserName($namaUser);
                            //$newSetting->setNip($nip);
                            $newSetting->setUserPassword($md5_pass_baru);
                            $newSetting->save($con);
                            budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                            $this->setFlash('berhasil', 'Password sudah berhasil dirubah, silahkan keluar dan login kembali');
                            // $con->commit();

                            $c = new Criteria();
                            $c->add(SchemaAksesV2Peer::USER_ID, $username);
                            $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                            $cs = SchemaAksesV2Peer::doSelectOne($c);
                            $cs->setIsUbahPass(true);
                            $cs->save($con);
                            $con->commit();
                            // return $this->redirect('entri/eula?unit_id=' . $unit_id);
                        } catch (Exception $e) {
                            $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                            $con->rollback();
                        }
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
        $this->setLayout('layouteula');
    }

    public function executeIndex() {
        $this->forward('default', 'module');
    }

    public function executeRequestlist() {
        //$this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $pagers = new sfPropelPager('LogRequestPenyelia', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        
        $es = UnitKerjaPeer::doSelect(new Criteria);
        $unit_kerja = Array();
        foreach ($es as $x) {
            $unit_kerja[] = $x->getUnitId();
        }
        $this->unit_kerja = $unit_kerja;
        $c->add(LogRequestPenyeliaPeer::STATUS, 4, Criteria::NOT_EQUAL);
        $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit_kerja[0]);
        for ($i = 1; $i < count($unit_kerja); $i++) {
            $c->addOr(LogRequestPenyeliaPeer::UNIT_ID, $unit_kerja[$i]);
        }
        $c->addDescendingOrderByColumn(LogRequestPenyeliaPeer::UPDATED_AT);
        $this->addFiltersCriteriaRequestlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function addFiltersCriteriaRequestlist($c) {
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(LogRequestPenyeliaPeer::UNIT_ID, $unit);
        }
        if (isset($this->filters['tipe']) && $this->filters['tipe'] !== '') {
            $c->add(LogRequestPenyeliaPeer::TIPE, $this->filters['tipe']);
        }
    }

    public function executeGetHeader() {
        $this->tahap = $tahap = $this->getRequestParameter('tahap');
        if ($tahap == 'pakbp') {
            $c = new Criteria();
            $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = PakBukuPutihMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'pakbb') {
            $c = new Criteria();
            $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = PakBukuBiruMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murnibp') {
            $c = new Criteria();
            $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniBukuPutihMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'murnibb') {
            $c = new Criteria();
            $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = MurniBukuBiruMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi1') {
            $c = new Criteria();
            $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi1MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi1_1') {
            $c = new Criteria();
            $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi1bMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2') {
            $c = new Criteria();
            $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi2MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2_1') {
            $c = new Criteria();
            $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi21MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2_2') {
            $c = new Criteria();
            $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi22MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'rkua') {
            $c = new Criteria();
            $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = RkuaMasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2') {
            $c = new Criteria();
            $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi2MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2_1') {
            $c = new Criteria();
            $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi21MasterKegiatanPeer::doSelectOne($c);
        } elseif ($tahap == 'revisi2_2') {
            $c = new Criteria();
            $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = Revisi22MasterKegiatanPeer::doSelectOne($c);
        } else {
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $master_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
        }
        if ($master_kegiatan) {
            $this->master_kegiatan = $master_kegiatan;
        }
        $this->setLayout('kosong');
    }

    public function executeKrka() {

        $this->processFilterskrka();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/krka/filters');

        $pagers = new sfPropelPager('RincianDetail', 50);
        $c = new Criteria();
//$c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $c->addAscendingOrderByColumn(RincianDetailPeer::UNIT_ID);
////schema public
        $c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::LEFT_JOIN);
        $c->add(RincianDetailPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);

        /* $c->addSelectColumn(RincianDetailPeer::UNIT_ID);
          $c->addSelectColumn(UnitKerjaPeer::UNIT_NAME);
          $c->addSelectColumn(RincianDetailPeer::KEGIATAN_CODE);
          $c->addSelectColumn(RincianDetailPeer::KOMPONEN_NAME);
          $c->addSelectColumn(RincianDetailPeer::KOMPONEN_ID);
          $c->addSelectColumn(RincianDetailPeer::KOMPONEN_HARGA_AWAL);
          $c->addSelectColumn(RincianDetailPeer::VOLUME);
          $c->addSelectColumn(RincianDetailPeer::PAJAK); */
        $this->addFiltersCriteriakrka($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');


        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeKrkaRevisi() {
        $this->processFilterskrkaRevisi();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/krkaRevisi/filters');

        $pagers = new sfPropelPager('DinasRincianDetail', 50);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(DinasRincianDetailPeer::UNIT_ID);
////schema public
        $c->addJoin(DinasRincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::LEFT_JOIN);
        $c->add(DinasRincianDetailPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $this->addFiltersCriteriakrkaRevisi($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');


        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterskrka() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/krka/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/krka/filters');
//$searchOption = $this->getRequestParameter('search_option');
//$this->getUser()->setAttribute('searchOption', $searchOption);
//echo $searchOption;exit;
        }
    }

    protected function processFilterskrkaRevisi() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/krkaRevisi/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/krkaRevisi/filters');
        }
    }

    protected function addFiltersCriteriakrka($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(RincianDetailPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
//$c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_id_is_empty'])) {
                    $criterion = $c->getNewCriterion(RincianDetailPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::KOMPONEN_ID, null, Criteria::ISNULL));
//$criterion->addJoin($c->getNewCriterion())
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
//                                echo $kata;
                    $c->add(RincianDetailPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
//$c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(RincianDetailPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(RincianDetailPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
//$c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(RincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
//              $kata = '%'.$this->filters['komponen'].'%';
//              $c->add(RincianDetailPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
// $c->addJoin(RincianDetailPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID);
                }
            }
        }
    }

    protected function addFiltersCriteriakrkaRevisi($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(DinasRincianDetailPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_id_is_empty'])) {
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(DinasRincianDetailPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(DinasRincianDetailPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(DinasRincianDetailPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $c->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
                }
            }
        }
    }

    public function executeResumeRapat() {
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        if ($this->getRequestParameter('print')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $tahap = $this->getRequestParameter('tahap');
            $this->redirect("report/printResumeRapat?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&tahap=$tahap");
        }
    }

    public function executeList() {
        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        //irul 8 november - bukuputih
        $this->pager = new sfPropelPager('MasterKegiatan', 20);
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $nama = $this->getUser()->getNamaUser();
        if ($nama != 'walikota' || $nama != 'bawas' || $nama != 'inspektorat1' || $nama != 'inspektorat2' || $nama != 'inspektorat3') {
            $e = new Criteria;
            $e->add(UserHandleV2Peer::USER_ID, $nama);
            $es = UserHandleV2Peer::doSelect($e);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
        }
        $c->addAscendingOrderByColumn(MasterKegiatanPeer::KEGIATAN_ID);
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->addAscendingOrderByColumn(MasterKegiatanPeer::KEGIATAN_ID);
        }
        $this->addFiltersCriteria($c,$tahap);
        //irul 8 november - bukuputih

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    public function executeListRevisi() {
        $menu = $this->getRequestParameter('menu');
        $this->menu = $menu;
        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        //irul 8 november - bukuputih
        if (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbp') {
            $this->pager = new sfPropelPager('PakBukuPutihMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'pakbb') {
            $this->pager = new sfPropelPager('PakBukuBiruMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murni') {
            $this->pager = new sfPropelPager('MurniMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibp') {
            $this->pager = new sfPropelPager('MurniBukuPutihMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibb') {
            $this->pager = new sfPropelPager('MurniBukuBiruMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'murnibbpraevagub') {
            $this->pager = new sfPropelPager('MurniBukubiruPraevagubMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1') {
            $this->pager = new sfPropelPager('Revisi1MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi1MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi1_1') {
            $this->pager = new sfPropelPager('Revisi1bMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2') {
            $this->pager = new sfPropelPager('Revisi2MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi2MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_1') {
            $this->pager = new sfPropelPager('Revisi21MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi21MasterKegiatanPeer::KODE_KEGIATAN);
        } elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi2_2') {
            $this->pager = new sfPropelPager('Revisi22MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi22MasterKegiatanPeer::KODE_KEGIATAN);
        }
        //revisi 3 start 21-08-2017 yogie =======================
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3') {
//            echo "a";die();
            $this->pager = new sfPropelPager('Revisi3MasterKegiatan', 20);
            //print_r($this->pager);die();           
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
            //print_r($unit_kerja);die();

            $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
//            echo $this->getRequestParameter('unit_id');die();
            if (isset($unit_id)) {
//                echo $unit_id;die();
                $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
//            echo $unit_id;die();
            $c->addAscendingOrderByColumn(Revisi3MasterKegiatanPeer::KODE_KEGIATAN);
        }
        //revisi 3 end 21-08-2017 yogie =======================
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi3_1') {
//            echo "a";die();
            $this->pager = new sfPropelPager('Revisi31MasterKegiatan', 20);
            //print_r($this->pager);die();           
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }
            //print_r($unit_kerja);die();

            $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
//            echo $this->getRequestParameter('unit_id');die();
            if (isset($unit_id)) {
//                echo $unit_id;die();
                $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
//            echo $unit_id;die();
            $c->addAscendingOrderByColumn(Revisi31MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi4') {
            $this->pager = new sfPropelPager('Revisi4MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi4MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi5') {
            $this->pager = new sfPropelPager('Revisi5MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi5MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi6') {
            $this->pager = new sfPropelPager('Revisi6MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi6MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi7') {
            $this->pager = new sfPropelPager('Revisi7MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi7MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi8') {
            $this->pager = new sfPropelPager('Revisi8MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi8MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi9') {
            $this->pager = new sfPropelPager('Revisi9MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi9MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'revisi10') {
            $this->pager = new sfPropelPager('Revisi10MasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }
            
            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(Revisi10MasterKegiatanPeer::KODE_KEGIATAN);
        }
        elseif (isset($this->filters['tahap']) && $this->filters['tahap'] == 'rkua') {
            $this->pager = new sfPropelPager('RkuaMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(RkuaMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            $c->addAscendingOrderByColumn(RkuaMasterKegiatanPeer::KODE_KEGIATAN);
        } else {
            $this->pager = new sfPropelPager('DinasMasterKegiatan', 20);
            $c = new Criteria();
            $es = UnitKerjaPeer::doSelect(new Criteria);
            $this->satuan_kerja = $es;
            $unit_kerja = Array();
            foreach ($es as $x) {
                $unit_kerja[] = $x->getUnitId();
            }

            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[0], Criteria::ILIKE);
            for ($i = 1; $i < count($unit_kerja); $i++) {
                $c->addOr(DinasMasterKegiatanPeer::UNIT_ID, $unit_kerja[$i], Criteria::ILIKE);
            }

            $unit_id = $this->getRequestParameter('unit_id');
            if (isset($unit_id)) {
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            }
            if ($menu == 'Pendapatan') {
                $c->add(DinasMasterKegiatanPeer::IS_BTL,TRUE);
            }
               else
            {
                    $c->add(DinasMasterKegiatanPeer::IS_BTL,FALSE);
            }
            $c->addAscendingOrderByColumn(DinasMasterKegiatanPeer::KEGIATAN_ID);
        }
        $this->addFiltersCriteriaRevisi($c, isset($this->filters['tahap']) ? $this->filters['tahap'] : null);
        //irul 8 november - bukuputih

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilters() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/master_kegiatan');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/master_kegiatan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/master_kegiatan/filters');
        }
    }

    protected function addFiltersCriteria($c) {
        //$c->setDistinct();
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    protected function addFiltersCriteriaRevisi($c, $tahap) {
        if ($tahap == 'pakbp') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(PakBukuPutihRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = PakBukuPutihRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'pakbb') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(PakBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(PakBukuBiruMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(PakBukuBiruRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = PakBukuBiruRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(PakBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(PakBukuBiruMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murni') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibp') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukuPutihRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukuPutihRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibb') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukuBiruMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukuBiruMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukuBiruRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukuBiruRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukuBiruMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukuBiruMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'murnibbpraevagub') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(MurniBukubiruPraevagubMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(MurniBukubiruPraevagubRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = MurniBukubiruPraevagubRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(MurniBukubiruPraevagubMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi1') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi1MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi1MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi1MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi1MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi1RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi1RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi1MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi1MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } 
        elseif ($tahap == 'revisi1_1') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi1MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi1bMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi1bMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi1bMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi1bRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi1bRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi1bMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi1bMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } 
        elseif ($tahap == 'revisi2') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi2MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi2MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi2MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi2MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi2RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi2RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi2MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi2MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi2_1') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi21MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi21MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi21MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi21MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi21RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi21RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi21MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi21MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi2_2') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi2MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi22MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi22MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi22MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi22MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi22RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi22RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi22MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi22MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
//========== yogie 24-08-2017
        elseif ($tahap == 'revisi3') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi3MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi3MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi3MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi3MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi3MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi3RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi3RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi3MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi3MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi3_1') 
            {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(Revisi3MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(Revisi31MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi31MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi31MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi31MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(Revisi31RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi31RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi31MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi31MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi4') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);

                $cton1 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi4MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi4MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi4MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi4MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi4RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi4RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi4MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi4MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi5') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi5MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi5MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi5MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi5MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi5RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi5RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi5MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi5MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi6') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi6MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi6MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi6MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi6MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi6RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi6RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi6MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi6MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi7') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi7MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi7MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi7MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi7MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi7RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi7RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi7MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi7MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi8') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi8MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi8MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi8MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi8MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi8RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi8RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi8MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi8MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi9') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi9MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi9MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi9MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi9MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi9RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi9RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi9MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi9MasterKegiatanPeer::UNIT_ID, $unit);
            }
        } elseif ($tahap == 'revisi10') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                
                $cton1 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);

                if ($jum_array == 2) {
                    $cton3 = $c->getNewCriterion(Revisi10MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(Revisi10MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(Revisi10MasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(Revisi10MasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                $crt1 = new Criteria();
                $crt1->add(Revisi10RincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = Revisi10RincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(Revisi10MasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(Revisi10MasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
        elseif ($tahap == 'rkua') {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(PakBukuBiruMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(RkuaMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                $c->add(RkuaMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(RkuaMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(RkuaMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(RkuaRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = RkuaRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(RkuaMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(RkuaMasterKegiatanPeer::UNIT_ID, $unit);
            }
        } else {
            //print_r($this->getRequestParameter('unit_name'));exit;
            if (isset($this->filters['kode_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
                $kode_kegiatan = $this->filters['kode_kegiatan'];
                $arr = explode('.', $kode_kegiatan);
                $jum_array = count($arr);
                //if($jum_array<2)
                //{
                $cton1 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
                $cton1->addOr($cton2);
                //$c->add($cton1);
                //}
                if ($jum_array == 2) {
                    //$c->add(DinasMasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                    $cton3 = $c->getNewCriterion(DinasMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                    $cton1->addAnd($cton3);
                }
                $c->add($cton1);
            }
            if (isset($this->filters['nama_kegiatan_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
                
                if(preg_match('~[0-9]+~', $this->filters['nama_kegiatan']))
                {
                     $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN,$this->filters['nama_kegiatan'], Criteria::EQUAL);
                }  
                else
                {
                    $c->add(DinasMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
                }             
            }
            if (isset($this->filters['posisi_is_empty'])) {
                $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, '');
                $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::POSISI, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['posisi']) && $this->filters['posisi'] !== '') {
                //$query="select *
                //from ". sfConfig::get('app_default_schema') .".rincian r, ". sfConfig::get('app_default_schema') .".master_kegiatan mk
                //where r.rincian_level=".$this->filters['posisi']." and r.kegiatan_code=mk.kode_kegiatan";
                $crt1 = new Criteria();
                $crt1->add(DinasRincianPeer::RINCIAN_LEVEL, $this->filters['posisi']);
                $rincian = DinasRincianPeer::doSelect($crt1);
                foreach ($rincian as $r) {
                    if ($r->getKegiatanCode() != '') {
                        $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $r->getKegiatanCode());
                    }
                }
            }
//        if (isset($this->filters['tahap_is_empty'])) {
//            $criterion = $c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, '');
//            $criterion->addOr($c->getNewCriterion(DinasMasterKegiatanPeer::TAHAP, null, Criteria::ISNULL));
//            $c->add($criterion);
//        } else if (isset($this->filters['tahap']) && $this->filters['tahap'] !== '') {
//            $c->add(DinasMasterKegiatanPeer::TAHAP, $this->filters['tahap'] . '%', Criteria::LIKE);
//        }
            if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
                $unit = $this->filters['unit_id'];
                $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit);
            }
        }
    }

    public function executeUbahPass() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
                $pass_baru = $this->getRequestParameter('pass_baru');
                // Validate password strength
                $uppercase = preg_match('@[A-Z]@', $pass_baru);
                $lowercase = preg_match('@[a-z]@', $pass_baru);
                $number    = preg_match('@[0-9]@', $pass_baru);
                //$specialChars = preg_match('@[^\w]@', $pass_baru);

                if(!$uppercase || !$lowercase || !$number || strlen($pass_baru) < 8) {
                    $this->setFlash('gagal', 'Mohon maaf, Inputan password minimal 8 karakter dan harus mencakup setidaknya satu huruf besar, satu angka.');
                } else {

                    $member = MasterUserV2Peer::retrieveByPK($this->getUser()->getNamaUser());
                    $member->setUserPassword(md5($pass_baru));
                    $member->save();
                    $this->setFlash('berhasil', 'Password berhasil terganti.');

                    $this->habis_ganti = true;
                }
        }
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

    public function executeEdit() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = SubtitleIndikatorPeer::doSelect($c);

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }

            $query = "select *
                from " . sfConfig::get('app_default_schema') . ".rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;

            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(RincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::LOKASI_KECAMATAN);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::LOKASI_KELURAHAN);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = RincianDetailPeer::doSelect($c_rincianDetail);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }

        if ($this->getRequestParameter('act') == 'editHeader') {
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');

            $c = new Criteria();
            //            $c->addSelectColumn('note_skpd');
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $c->add(RincianDetailPeer::DETAIL_NO, $this->id);
            $notes = RincianDetailPeer::doSelectOne($c);
            $note = $notes->getNotePeneliti();
            //            var_dump($note);
            $this->note_peneliti = $note;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $catatan = $this->getRequestParameter('catatan');
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');
//                 echo $this->unit_id.'.'.$catatan;
            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $this->unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(RincianDetailPeer::DETAIL_NO, $this->id);
            $rincian_detail = RincianDetailPeer::doSelectOne($c);

            if ($rincian_detail) {
                $rincian_detail->setNotePeneliti($catatan);
                $rincian_detail->save();
//                   return $this->redirect('peneliti/edit?unit_id='.$rincian_detail->getUnitId().'&kode_kegiatan='.$rincian_detail->getKegiatanCode());
            }
        }
    }

    public function executeEditRevisi() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');

            if ($tahap == 'pakbp') {
                $c = new Criteria();
                $c->add(PakBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(PakBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->addAscendingOrderByColumn(PakBukuPutihSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = PakBukuPutihSubtitleIndikatorPeer::doSelect($c);
            } else {
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
                $c->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelect($c);
            }

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';
        }
    }

    public function executeGetPekerjaansRevisi() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $this->tahap = $tahap = $this->getRequestParameter('tahap');

            if ($tahap == 'pakbp') {
                $c = new Criteria();
                $c->add(PakBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = PakBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $query = "select *
                from " . sfConfig::get('app_default_schema') . ".pak_bukuputih_rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

                $con = Propel::getConnection(PakBukuPutihRincianDetailPeer::DATABASE_NAME);
                $statement = $con->prepareStatement($query);
                $rs_rinciandetail = $statement->executeQuery();
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(PakBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(PakBukuPutihRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = PakBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
            } else {
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $unit_id = $rs_subtitle->getUnitId();
                    $kegiatan_code = $rs_subtitle->getKegiatanCode();
                    $subtitle = $rs_subtitle->getSubtitle();
                    $nama_subtitle = trim($subtitle);
                }

                $query = "select *
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

                $con = Propel::getConnection(DinasRincianDetailPeer::DATABASE_NAME);
                $statement = $con->prepareStatement($query);
                $rs_rinciandetail = $statement->executeQuery();
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c_rincianDetail = new Criteria();
                $c_rincianDetail->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                $c_rincianDetail->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c_rincianDetail->add(DinasRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
                $c_rincianDetail->add(DinasRincianDetailPeer::STATUS_HAPUS, false);
                $c_rincianDetail->add(DinasRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KECAMATAN);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::LOKASI_KELURAHAN);
                $c_rincianDetail->addAscendingOrderByColumn(DinasRincianDetailPeer::KOMPONEN_NAME);
                $rs_rd = DinasRincianDetailPeer::doSelect($c_rincianDetail);
            }
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }

        if ($this->getRequestParameter('act') == 'editHeader') {
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');

            $c = new Criteria();
            //            $c->addSelectColumn('note_skpd');
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(DinasRincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $this->id);
            $notes = DinasRincianDetailPeer::doSelectOne($c);
            $note = $notes->getNotePeneliti();
            //            var_dump($note);
            $this->note_peneliti = $note;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $catatan = $this->getRequestParameter('catatan');
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');
//                 echo $this->unit_id.'.'.$catatan;
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $this->unit_id);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(DinasRincianDetailPeer::DETAIL_NO, $this->id);
            $rincian_detail = DinasRincianDetailPeer::doSelectOne($c);

            if ($rincian_detail) {
                $rincian_detail->setNotePeneliti($catatan);
                $rincian_detail->save();
//                   return $this->redirect('peneliti/edit?unit_id='.$rincian_detail->getUnitId().'&kode_kegiatan='.$rincian_detail->getKegiatanCode());
            }
        }
    }

    public function executeEditHeader() {
        $this->unit_id = $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $lokasi = $this->getRequestParameter('lokasi');
            $kelompok_sasaran = $this->getRequestParameter('sasaran');

            if ($lokasi == 'Kecamatan') {
                $jumlah = $this->getRequestParameter('jml_kecamatan');
                $lokasi = $jumlah . ' Kecamatan';
            } elseif ($lokasi == 'Kelurahan') {
                $jumlah = $this->getRequestParameter('jml_kelurahan');
                $lokasi = $jumlah . ' Kelurahan';
            }

            $arr_sasaran = array();
            foreach ($kelompok_sasaran as $sasaran) {
                if ($sasaran == 'Masyarakat Kota Surabaya Lainnya') {
                    $lainnya = $this->getRequestParameter('lain');
                    $sasaran = 'Masyarakat Kota Surabaya ' . $lainnya;
                } else if ($sasaran == 'Lainnya') {
                    $lainnya = $this->getRequestParameter('lain2');
                    $sasaran = $lainnya;
                }
                $arr_sasaran[] = $sasaran;
            }
            
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $kegiatan->setLokasi($lokasi);
            $kegiatan->setKelompokSasaran(implode('|', $arr_sasaran));
            $kegiatan->save();
            
            return $this->redirect("view_rka/editHeader?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else {
            $c = new Criteria();
            $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $kegiatan = DinasMasterKegiatanPeer::doSelectOne($c);
            $this->sasaran = $kegiatan->getKelompokSasaran();
            $this->lokasi = $kegiatan->getLokasi();
        }
    }

    public function executeApiTarikSabkKegiatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $tahap = $this->getRequestParameter('tahap');
        $query = "
        SELECT DISTINCT 
        uk.kode_permen,kg.unit_id,kg.kode_kegiatan, kg.kegiatan_id, kg.nama_kegiatan,kg.alokasi_dana, kg.catatan,
        mu.kode_urusan,mu.nama_urusan, mp.kode_program2, mp.nama_program2,o.lokasi,o.lokasi_pelaksana,o.kelompok_sasaran
        FROM
        ebudget.".$tahap."_rincian rc, ebudget.".$tahap."_master_kegiatan kg, unit_kerja uk, ebudget.master_urusan mu, ebudget.master_program2 mp,ebudget.output_subtitle o
        WHERE
        uk.unit_id = kg.unit_id and rc.unit_id=kg.unit_id and o.unit_id=kg.unit_id and
        kg.kode_kegiatan = rc.kegiatan_code and kg.unit_id = rc.unit_id and o.kode_kegiatan=kg.kode_kegiatan and
        mu.kode_urusan = kg.kode_urusan and mp.kode_program2 = kg.kode_program2 and
        uk.unit_id = '$unit_id'          
        ORDER BY kg.unit_id,kg.kode_kegiatan";

        echo "//kode_permen |kode_sub_kegiatan |sub_kegiatan_id|nama_kegiatan |nama_urusan |nama_program |pagu |catatan|lokasi|kelompok_sasaran <br>//<BR>\n";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        
       
        while($rs->next()) {
            //$front_code = $rs->getString('front_code');
            $kode_permen = $rs->getString('kode_permen');
            $kode_kegiatan = $rs->getString('kode_kegiatan');
            $kegiatan_id = $rs->getString('kegiatan_id');
            $nama_kegiatan = $rs->getString('nama_kegiatan');
            $kode_urusan = $rs->getString('kode_urusan');
            $nama_urusan = $rs->getString('nama_urusan');
            $kode_program2 = $rs->getString('kode_program2');
            $nama_program2 = $rs->getString('nama_program2');
            $alokasi_dana = $rs->getString('alokasi_dana');
            //$kode_misi = $rs->getString('kode_misi');
            //$nama_misi = $rs->getString('nama_misi');
            //$kode_tujuan = $rs->getString('kode_tujuan');
            //$nama_tujuan = $rs->getString('nama_tujuan');
            $catatan = $rs->getString('catatan');
            $kelompok_sasaran = $rs->getString('kelompok_sasaran');
            $lokasi = $rs->getString('lokasi');
            $lokasi_pelaksana = $rs->getString('lokasi_pelaksana');

            if($unit_id)

            //if ($front_code)
                //$front_code .= ".";
            $kode_program22 = substr($kode_program2, 9, 2);
            if (substr($kode_program2, 0, 4) == 'x.xx') {
                $kode_urusan = substr($kode_permen, 0, 4);
            } else {
                $kode_urusan = substr($kode_program2, 0, 8);
            }


            $buang = array("\t", "\n", "\r", "\0", "\x0B", "|", "&amp;", "Å", "&Aring;", "&#160;", "&#197;", " ");
            $nama_kegiatan = str_replace($buang, "", $nama_kegiatan);
            $catatan = str_replace($buang, "", $catatan);
            // $subtitle = str_replace($buang, "", $subtitle);
            // $komponen_name = str_replace($buang, "", $komponen_name);
            // $detail_name = str_replace($buang, "", $detail_name);
            // $detaildisub = str_replace($buang, "", $detaildisub);
            $nama_urusan = str_replace($buang, "", $nama_urusan);
            $nama_program2 = str_replace($buang, "", $nama_program2);
            // $input = str_replace($buang, "", $input);
            // $benefit = str_replace($buang, "", $benefit);
            // $impact = str_replace($buang, "", $impact);
            // $misi = str_replace($buang, "", $misi);
            // $tujuan = str_replace($buang, "", $tujuan);
            $kelompok_sasaran = str_replace($buang, "", $kelompok_sasaran);
            $lokasi = str_replace($buang, "", $lokasi);
            $lokasi_pelaksana = str_replace($buang, "", $lokasi_pelaksana);

            echo $kode_permen . "|" . $kegiatan_id . "|" . $kode_kegiatan . "|" . trim($nama_kegiatan) . "|" . trim($nama_urusan) . "|" . trim($nama_program2) . "|" . $alokasi_dana . "|" . trim($catatan) . "|" . trim($lokasi_pelaksana) ."|" . trim($kelompok_sasaran) . "<BR>\n";
           
        }
        return $this->renderText(json_encode());
        
    }

    public function executeApiUserPaKpaPptk() {

        $query =
        "SELECT mu.user_id, mu.user_name, mu.nip, mu.jabatan, ul.level_name, mu.user_enable 
        FROM master_user_v2 mu, schema_akses_v2 sa, user_level ul
        WHERE mu.user_id=sa.user_id
        and sa.level_id=ul.level_id
        and sa.level_id in (13,14,15)
        order by sa.level_id";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();

        $data = array();
        $idx = 0;
        while($rs->next()) {
            $data['data'][$idx]['user_id'] = $rs->getString('user_id');
            $data['data'][$idx]['user_name'] = $rs->getString('user_name');
            $data['data'][$idx]['nip'] = $rs->getString('nip');
            $data['data'][$idx]['jabatan'] = $rs->getString('jabatan');
            $data['data'][$idx]['level'] = $rs->getString('level_name');

            $idx++;
        }

        return $this->renderText(json_encode($data));
    }

}
