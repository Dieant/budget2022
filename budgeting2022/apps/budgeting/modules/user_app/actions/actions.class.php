<?php

/**
 * user_app actions.
 *
 * @package    budgeting
 * @subpackage user_app
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class user_appActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

    public function executePilihNama() {
        $nip = $this->getRequestParameter('b');

        $c = new Criteria();
        $c->add(PegawaiPerformancePeer::NIP, $nip);
        $rs = PegawaiPerformancePeer::doSelectOne($c);
        $this->nama = $rs->getNama();
    }

    public function executePilihNip() {
        $unit_id = $this->getRequestParameter('b');
        $c = new Criteria();
        $rs_pegawai = array();
        $arr_tampung = array();
        if(sfConfig::get('app_fasilitas_cekuserbyeper') == 'buka'){
            $c->addJoin(PegawaiPerformancePeer::SKPD_ID, MasterSkpdPeer::SKPD_ID);
            $c->addAnd(PegawaiPerformancePeer::NIP, '%K%', Criteria::NOT_ILIKE);
            $c->addAnd(PegawaiPerformancePeer::NIP, '%P%', Criteria::NOT_ILIKE);
            $c->addAnd(PegawaiPerformancePeer::NIP, '%D%', Criteria::NOT_ILIKE);
            $c->addAnd(MasterSkpdPeer::SKPD_KODE, $unit_id);
            $c->addAscendingOrderByColumn(PegawaiPerformancePeer::NAMA);
            $rs_pegawai = PegawaiPerformancePeer::doSelect($c);
            foreach ($rs_pegawai as $pegawai) {
            $arr_tampung[$pegawai->getNip()] = $pegawai->getNipNama();
            }
        // $this->nilai = $arr_tampung;
        }
        else if (sfConfig::get('app_fasilitas_cekuserbyeper') == 'tutup'){
            $c->addJoin(MasterUserV2Peer::USER_ID, UserHandleV2Peer::USER_ID);
            $c->addAnd(MasterUserV2Peer::USER_ENABLE, TRUE);
            $c->addAnd(UserHandleV2Peer::UNIT_ID, $unit_id);
            $rs_pegawai = MasterUserV2Peer::doSelect($c);
            foreach ($rs_pegawai as $pegawai) {
                $arr_tampung[$pegawai->getUserId()] = $pegawai->getUserId()." - ".$pegawai->getUserName();
            }
        }
        $this->nilai = $arr_tampung;
    }

    public function executePilihJabatan() {
        $nip = $this->getRequestParameter('b');
        if(sfConfig::get('app_fasilitas_cekuserbyeper') == 'buka'){
            $c = new Criteria();
            $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
            $c->add(PegawaiPerformancePeer::NIP, $nip);
            $rs = JabatanStrukturalPeer::doSelectOne($c);
            $this->jabatan = $rs->getNama();
        }
        else{
            $this->jabatan = "";
        }
    }

    public function executeSaveedituser() {

        $unit_id = $this->getRequestParameter('unit_id');
        $unit_id_lama = $this->getRequestParameter('unit_id_lama');
        $user_id = $this->getRequestParameter('user_id');
        $nip = $this->getRequestParameter('nip');
        $role = $this->getRequestParameter('role');

        //$jk = $this->getRequestParameter('jk');
        if (!$email = $this->getRequestParameter('email')) {
            $email = null;
        }
        if (!$telepon = $this->getRequestParameter('telepon')) {
            $telepon = null;
        }
//        if ($role != 15) {
//            $kegiatan = $this->getRequestParameter('kegiatan');
//        }
        if ($nip == '' && $user_id == '') {
            $this->setFlash('gagal', 'Parameter kurang');
            return $this->redirect('user_app/edituser');
        }

        if(sfConfig::get('app_fasilitas_cekuserbyeper') == 'buka'){
            $c1 = new Criteria();
            $c1->add(PegawaiPerformancePeer::NIP, $nip);
            $c1->add(PegawaiPerformancePeer::TGL_KELUAR, NULL);
            $c1->add(PegawaiPerformancePeer::DELETED_AT, NULL);
            $rs1 = PegawaiPerformancePeer::doSelectOne($c1);
            $user_name = $rs1->getNama();

            $c = new Criteria();
            $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
            $c->add(PegawaiPerformancePeer::NIP, $nip);
            $jabatan = '';
            if ($rs = JabatanStrukturalPeer::doSelectOne($c)) 
                {          
                    $jabatan = $rs->getNama();
                    $pangkat = str_replace(' ', '', trim(strtoupper($rs->getEselon())));
                }
            }

        //konversi pangkat
        if($pangkat == 'IA')
        {
            $pangkat='Juru Muda';
        } else if($pangkat == 'IB')
        {
            $pangkat='Juru Muda Tk. I'; 
        } else if($pangkat == 'IC')
        {
            $pangkat='Juru';
        } else if($pangkat == 'ID')
        {
            $pangkat='Juru Tk. I';
        } else if($pangkat == 'IIA')
        {
            $pangkat='Pengatur Muda';
        } else if($pangkat == 'IIB')
        {
            $pangkat='Pengatur Muda Tk. I';
        } else if($pangkat == 'IIC')
        {
            $pangkat='Pengatur';
        } else if($pangkat == 'IID')
        {
            $pangkat='Pengatur Tk. I';
        } else if($pangkat == 'IIIA')
        {
            $pangkat='Penata Muda';
        }else if($pangkat == 'IIIB')
        {
            $pangkat='Penata Muda Tk. I';
        }else if($pangkat == 'IIIC')
        {
            $pangkat='Penata';
        }else if($pangkat == 'IIID')
        {
            $pangkat='Penata Tk. I';
        }else if($pangkat == 'IVA')
        {
            $pangkat='Pembina';
        }else if($pangkat == 'IVB')
        {
            $pangkat='Pembina Tk. I';
        }else if($pangkat == 'IVC')
        {
            $pangkat='Pembina Utama Muda';
        }else if($pangkat == 'IVD')
        {
            $pangkat='Pembina Utama Madya';
        }else if($pangkat == 'IVE')
        {
            $pangkat='Pembina  Utama ';
        }
        else
        {
            $pangkat=' ';
        }
        $c = new Criteria();
        $c->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c);
        $con = Propel::getConnection();
        $con->begin();
        if ($dapat_user) {
            //echo $unit_id;exit;
            $dapat_user->setUserName($user_name);
            $dapat_user->setNip($nip);
            $dapat_user->setJabatan($jabatan);
            //$dapat_user->setJenisKelamin($jk);
            $dapat_user->setEmail($email);
            $dapat_user->setTelepon($telepon);
            $dapat_user->save();

            $c_level = new Criteria();
            $c_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
            $c_level->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
            //$c_level->addDescendingOrderByColumn(SchemaAksesV2Peer::SCHEMA_ID);
            $dapat_level = SchemaAksesV2Peer::doSelectOne($c_level);
            $dapat_level->setLevelId($role);
            $dapat_level->save();

            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);        
            
            $dapat = UnitKerjaPeer::doSelectOne($c);
            $dapat->setKepalaNama($user_name);
            $dapat->setKepalaNip($nip);
            $dapat->setKepalaPangkat($pangkat);
            $dapat->save();

            if ($unit_id != $unit_id_lama) {
                $query = "update user_handle_v2 set unit_id='$unit_id' where user_id='$user_id' and schema_id=2";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                if ($role == 1) {
                    $d = new Criteria();
                    $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_lama);
                    $d->add(DinasMasterKegiatanPeer::USER_ID, $user_id);
                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                    if ($rs_kegiatan) {
                        $rs_kegiatan->setUserId(NULL);
                        $rs_kegiatan->save();
                    }
                } else if ($role == 13) {
                    $d = new Criteria();
                    $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_lama);
                    $d->add(DinasMasterKegiatanPeer::USER_ID_PPTK, $user_id);
                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                    if ($rs_kegiatan) {
                        $rs_kegiatan->setUserIdPptk(NULL);
                        $rs_kegiatan->save();
                    }
                } else if ($role == 14) {
                    $d = new Criteria();
                    $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_lama);
                    $d->add(DinasMasterKegiatanPeer::USER_ID_KPA, $user_id);
                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                    if ($rs_kegiatan) {
                        $rs_kegiatan->setUserIdKpa(NULL);
                        $rs_kegiatan->save();
                    }
                }
            }


//            if ($role != 15) {
//                foreach ($kegiatan as $kode_kegiatan) {
//                    $d = new Criteria();
//                    $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_lama);
//                    $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
//                    $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
//                    if ($role == 1)
//                        $rs_kegiatan->setUserId($user_id);
//                    else if ($role == 13)
//                        $rs_kegiatan->setUserIdPptk($user_id);
//                    else if ($role == 14)
//                        $rs_kegiatan->setUserIdKpa($user_id);
//                    $rs_kegiatan->save();
//                }
//            }
            $con->commit();
            $this->setFlash('berhasil', 'Data Kepala SKPD ' . $unit_id . ' berhasil diubah');
            return $this->redirect('user_app/editUser?id=' . $user_id);
        } else {
            $con->rollback();
            $this->setFlash('gagal', 'Tidak ditemukan SKPD ' . $unit_id);
            return $this->redirect('user_app/editUser?id=' . $user_id);
        }
    }

    public function executeBuatuser() {
        
    }

    public function executeSaveuserbaru() {
        $unit_id = $this->getRequestParameter('unit_id');
        $nip = $this->getRequestParameter('nip');
        $role = $this->getRequestParameter('role');
        //$jk = $this->getRequestParameter('jk');
        $kegiatan = $this->getRequestParameter('kegiatan');
        if (!$email = $this->getRequestParameter('email')) {
            $email = null;
        }
        if (!$telepon = $this->getRequestParameter('telepon')) {
            $telepon = null;
        }

        if(sfConfig::get('app_fasilitas_cekuserbyeper') == 'buka'){
            $c1 = new Criteria();
            $c1->add(PegawaiPerformancePeer::NIP, $nip);
            $rs1 = PegawaiPerformancePeer::doSelectOne($c1);
            $user_name = $rs1->getNama();

            $c = new Criteria();
            $c->addJoin(PegawaiPerformancePeer::JABATAN_STRUKTURAL_ID, JabatanStrukturalPeer::ID);
            $c->add(PegawaiPerformancePeer::NIP, $nip);
            $jabatan = '';
            if ($rs = JabatanStrukturalPeer::doSelectOne($c))
                $jabatan = $rs->getNama();
            if ($nip == '' && $user_id == '') {
                $this->setFlash('gagal', 'Parameter kurang');
                return $this->redirect('user_app/buatuser');
            }
        }

        $con = Propel::getConnection();
        $con->begin();
        try {
            $dapat_user = new MasterUserV2();
            $dapat_user->setUserId($nip);
            $dapat_user->setUserName($user_name);
            $dapat_user->setUserDefaultPassword($password);
            $dapat_user->setUserPassword(md5($password));
            $dapat_user->setUserEnable(TRUE);
            $dapat_user->setNip($nip);
            $dapat_user->setJabatan($jabatan);
            $dapat_user->setEmail($email);
            $dapat_user->setTelepon($telepon);
            //$dapat_user->setJenisKelamin($jk);
            $dapat_user->save();

            $dapat_level = new SchemaAksesV2();
            $dapat_level->setUserId($nip);
            $dapat_level->setSchemaId(2);
            $dapat_level->setLevelId($role);
            $dapat_level->save();

            $dapat_unit_id = new UserHandleV2();
            $dapat_unit_id->setUserId($nip);
            $dapat_unit_id->setUnitId($unit_id);
            $dapat_unit_id->setSchemaId(2);
            $dapat_unit_id->save();

            foreach ($kegiatan as $kode_kegiatan) {
                $d = new Criteria();
                $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                $d->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                if ($role == 13)
                    $rs_kegiatan->setUserIdPptk($nip);
                else if ($role == 14)
                    $rs_kegiatan->setUserIdKpa($nip);

                $rs_kegiatan->save();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Data User ' . $user_id . ' berhasil diubah');
            return $this->redirect('user_app/listuser');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena user sudah ada, silahkan melakukan edit pada user ' . $nip);
            return $this->redirect('user_app/buatuser');
        }
    }

    public function executeUserlist() {
        $this->processFiltersuserlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/userlist/filters');

        $pagers = new sfPropelPager('MasterUserV2', 25);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(MasterUserV2Peer::USER_ENABLE);
        $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
        $c->setDistinct();
        $this->addFiltersCriteriauserlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltersuserlist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/userlist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/userlist/filters');
        }
    }

    protected function addFiltersCriteriauserlist($c) {
        $k = '%admin';
        $users = $this->getUser()->getNamaUser();
        if ((isset($this->filters['userid']) && $this->filters['userid'] != '') || (isset($this->filters['username']) && $this->filters['username'] != '') || (isset($this->filters['level']) && $this->filters['level'] != '')) {

            if (isset($this->filters['userid']) && $this->filters['userid'] != '') {


                $kata = '%' . trim($this->filters['userid']) . '%';

                if ((strpos($users, 'admin') !== FALSE )) {
                    $c->add(MasterUserV2Peer::USER_ID, '%superadmin%', Criteria::NOT_ILIKE);
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }elseif ((strpos($users, 'survey') !== FALSE )) {
                    $c->add(MasterUserV2Peer::USER_ID, '%survey%', Criteria::ILIKE);
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                } 
                else {

                    if ($kata == '%admin%') {
                        $c->add(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                        $c->addAnd(MasterUserV2Peer::USER_ID, $users, Criteria::ILIKE);
                    } else
                        $c->add(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }
                if (isset($this->filters['level']) && $this->filters['level'] != '') {
                    $level_id = $this->filters['level'];
                    $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                    $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                    $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                }
            } else if (isset($this->filters['level']) && $this->filters['level'] != '') {
                $users = $this->getUser()->getNamaUser();
                $level_id = $this->filters['level'];

                if ($users == 'admin') {
                    $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                    $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                    $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                } else {
                    $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                    $c->addAnd(SchemaAksesV2Peer::LEVEL_ID, $level_id);
                    $c->addAnd(SchemaAksesV2Peer::SCHEMA_ID, 2);
                    if ($level_id == 9)
                        $c->addAnd(MasterUserV2Peer::USER_ID, $users, Criteria::ILIKE);
                }

                if (isset($this->filters['userid']) && $this->filters['userid'] != '') {
                    $kata = '%' . trim($this->filters['userid']) . '%';
                    $c->addAnd(MasterUserV2Peer::USER_ID, $kata, Criteria::ILIKE);
                }
            }

            if (isset($this->filters['username']) && $this->filters['username'] != '') {
                $username = '%' . str_replace(" ", "%", trim($this->filters['username'])) . '%';

                if ($users == 'admin') {
                    $c->add(MasterUserV2Peer::USER_NAME, $username, Criteria::ILIKE);
                } else {


                    if ($username == '%admin%') {
                        $c->addAnd(MasterUserV2Peer::USER_ID, $users, Criteria::ILIKE);
                    } else {
                        $username = '%' . str_replace(" ", "%", trim($this->filters['username'])) . '%';
                        $c->add(MasterUserV2Peer::USER_NAME, $username, Criteria::ILIKE);
                    }
                }
            }
        }
    }

    public function executeEditUser() {
        $user_id = $this->getRequestParameter('id');

        $c_cek_user = new Criteria();
        $c_cek_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_cek_user);

        $c_cek_unit = new Criteria();
        $c_cek_unit->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_cek_unit->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $dapat_unit = UnitKerjaPeer::doSelect($c_cek_unit);

        $pagers = new sfPropelPager('UserHandleV2', 100);
        $c = new Criteria();
        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
        $c->addJoin(UserHandleV2Peer:: UNIT_ID, UnitKerjaPeer::UNIT_ID);
        $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();
        $this->pager = $pagers;


        if ($dapat_user) {
            $this->data_user = $dapat_user;

            $c_user_level = new Criteria();
            $c_user_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
            $c_user_level->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
            $dapat_user_level = SchemaAksesV2Peer::doSelectOne($c_user_level);
            if ($dapat_user_level) {
                $this->level_id = $level_id = $dapat_user_level->getLevelId();

                $array_level_butuh_dinas_banyak = array(2, 7, 8, 10, 12);
                $array_level_butuh_dinas_satu = array(1, 11, 16);
                $array_level_tidak_butuh_dinas = array(3, 4, 9);

                if (in_array($level_id, $array_level_butuh_dinas_banyak)) {
                    $this->butuh_dinas = 2;
                } else if (in_array($level_id, $array_level_butuh_dinas_satu)) {
                    $this->butuh_dinas = 1;
                } else if (in_array($level_id, $array_level_tidak_butuh_dinas)) {
                    $this->butuh_dinas = 0;
                }

                if ($level_id == 1 || $level_id == 13 || $level_id == 14 || $level_id == 15) {
                    $c_skpd = new Criteria();
                    $c_skpd->add(UserHandleV2Peer::USER_ID, $user_id);
                    $c_skpd->add(UserHandleV2Peer::SCHEMA_ID, 2);
                    $dapat_skpd = UserHandleV2Peer::doSelectOne($c_skpd);

                    $this->skpd = $skpd = $dapat_skpd->getUnitId();
                    $c_unit_kerja = new Criteria();
                    $c_unit_kerja->add(UnitKerjaPeer::UNIT_ID, $skpd);
                    $dapat_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit_kerja);
                    if ($dapat_unit_kerja) {
                        $this->unit = $dapat_unit_kerja;
                    } else {
                        $this->setFlash('gagal', 'Tidak ditemukan unit untuk user id ' . $user_id);
                        return $this->redirect('user_app/userlist');
                    }
                }
            } else {
                $this->setFlash('gagal', 'Tidak ditemukan user akses dengan id ' . $user_id);
                return $this->redirect('user_app/userlist');
            }
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user id ' . $user_id);
            return $this->redirect('user_app/userlist');
        }
    }

    public function executeHapusUser() {
        $this->processFiltersuserlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/userlist/filters');

        $pagers = new sfPropelPager('MasterUserV2', 25);
        $c = new Criteria();
        $c->addDescendingOrderByColumn(MasterUserV2Peer::USER_ENABLE);
        $c->addAscendingOrderByColumn(MasterUserV2Peer::USER_ID);
        $c->setDistinct();
        $this->addFiltersCriteriauserlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    function executeSavedatakepalaskpd() {
        $unit_id = $this->getRequestParameter('unit_id');
        $user_id = $this->getRequestParameter('user_id');

        $kepala_nama = $this->getRequestParameter('kepala_nama');
        $kepala_nip = $this->getRequestParameter('kepala_nip');
        $kepala_pangkat = $this->getRequestParameter('kepala_pangkat');

        $c_unit_kerja = new Criteria();
        $c_unit_kerja->add(UnitKerjaPeer::UNIT_ID, $unit_id);
        $dapat_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit_kerja);
        if ($dapat_unit_kerja) {
            $dapat_unit_kerja->setKepalaNama($kepala_nama);
            $dapat_unit_kerja->setKepalaNip($kepala_nip);
            $dapat_unit_kerja->setKepalaPangkat($kepala_pangkat);
            $dapat_unit_kerja->save();

            $this->setFlash('berhasil', 'Data Kepala SKPD ' . $unit_id . ' berhasil diubah');
            return $this->redirect('user_app/editUser?id=' . $user_id);
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan SKPD ' . $unit_id);
            return $this->redirect('user_app/editUser?id=' . $user_id);
        }
    }

    function executeSaveresetpassworduser() {
        $user_id = $this->getRequestParameter('user_id');

        $pass_baru = $this->getRequestParameter('pass_baru');
        $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');

        if ($pass_baru <> $ulang_pass_baru) {
            $this->setFlash('gagal', 'Isian password dan isian ulang password tidak sama');
            return $this->redirect('user_app/editUser?id=' . $user_id);
        } else {
            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
            $dapat_user = MasterUserV2Peer::doSelectOne($c_user);

            $c_user1 = new Criteria();
            $c_user1->add(SchemaAksesV2Peer::USER_ID, $user_id);
            $dapat_user1 = SchemaAksesV2Peer::doSelectOne($c_user1);
            
            if ($dapat_user) {
                $dapat_user->setUserPassword(md5($pass_baru));
                $dapat_user->save();

                $dapat_user1->setIsUbahPass(null);
                $dapat_user1->save();

                $this->setFlash('berhasil', 'Password user ' . $user_id . ' berhasil diubah');
                return $this->redirect('user_app/editUser?id=' . $user_id);
            } else {
                $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
                return $this->redirect('user_app/editUser?id=' . $user_id);
            }
        }
    }

    function executeKunciUser() {
        $user_id = $this->getRequestParameter('id');

        $c_user = new Criteria();
        $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
        if ($dapat_user) {
            $dapat_user->setUserEnable(false);
            $dapat_user->save();

            $this->setFlash('berhasil', 'User ' . $user_id . ' berhasil dikunci');
            return $this->redirect('user_app/userlist');
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
            return $this->redirect('user_app/userlist');
        }
    }

    function executeBukaUser() {
        $user_id = $this->getRequestParameter('id');

        $c_user = new Criteria();
        $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
        $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
        if ($dapat_user) {
            $dapat_user->setUserEnable(true);
            $dapat_user->save();

            $this->setFlash('berhasil', 'User ' . $user_id . ' berhasil dibuka');
            return $this->redirect('user_app/userlist');
        } else {
            $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
            return $this->redirect('user_app/userlist');
        }
    }

    function executeSaveambillepasskpd() {
        $ambil_skpd = $this->getRequestParameter('ambil_skpd');
        $lepas_skpd = $this->getRequestParameter('lepas_skpd');
        $user_id = $this->getRequestParameter('user_id');
        $con = Propel::getConnection();
        $con->begin();
        try {
            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $user_id);
            $dapat_user = MasterUserV2Peer::doSelectOne($c_user);
            if ($dapat_user) {
                if (isset($ambil_skpd)) {
                    foreach ($ambil_skpd as $value_ambil) {
                        $c = new Criteria();
                        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
                        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
                        $c->addAnd(UserHandleV2Peer:: UNIT_ID, $value_ambil);
                        $cek_ambil = UserHandleV2Peer::doCount($c);

                        if ($cek_ambil == 0) {
                            $baru_ambil_skpd = new UserHandleV2();
                            $baru_ambil_skpd->setSchemaId(2);
                            $baru_ambil_skpd->setUnitId($value_ambil);
                            $baru_ambil_skpd->setUserId($user_id);

                            //tambahan untuk penyelia ssh
                            $penyelia_ssh = array('adam.yulian','adhitiya');
                            if(in_array($user_id, $penyelia_ssh)){
                                $baru_ambil_skpd->setStatusUser('shs');
                            }

                            $baru_ambil_skpd->save();
                        }
                    }
                }

                if (isset($lepas_skpd)) {
                    foreach ($lepas_skpd as $value_lepas) {
                        $c = new Criteria();
                        $c->add(UserHandleV2Peer:: SCHEMA_ID, 2);
                        $c->addAnd(UserHandleV2Peer:: USER_ID, $user_id);
                        $c->addAnd(UserHandleV2Peer:: UNIT_ID, $value_lepas);
                        $cek_lepas = UserHandleV2Peer::doCount($c);

                        if ($cek_lepas == 1) {
                            $eksekusi_hapus_lepas = UserHandleV2Peer::doDelete($c);
                        }
                    }
                }
                $con->commit();
                $this->setFlash('berhasil', 'User ' . $user_id . ' berhasil merubah User Handle SKPD');
                return $this->redirect('user_app/editUser?id=' . $user_id);
            } else {
                $con->rollback();
                $this->setFlash('gagal', 'Tidak ditemukan user ' . $user_id);
                return $this->redirect('user_app/editUser?id=' . $user_id);
            }
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getTraceAsString());
            return $this->redirect('user_app/editUser?id=' . $user_id);
        }
    }

}
