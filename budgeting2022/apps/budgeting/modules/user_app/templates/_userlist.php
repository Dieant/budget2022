<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
    <thead class="head_peach">
        <tr>
            <th>ID</th>
            <th>Nama User</th>
            <th>Status</th>
            <th>Action</th>  
        </tr>
    </thead>
    <tbody>
         <?php foreach ($pager->getResults() as $userlist): ?>
                <tr>
                    <td><?php echo $userlist->getUserId() ?></td>
                    <td><?php echo $userlist->getUserName() ?></td>
                    <?php if ($userlist->getUserEnable() == true) { ?>
                        <td class="text-center text-bold">
                            Aktif
                        </td>    
                    <?php } else { ?>
                        <td class="text-center bg-danger">
                            Non-Aktif
                        </td>    
                    <?php }
                    ?>
                    </td>
                    <td style="text-align: left">
                        <div class="btn-group">
                            <?php if($sf_user->getNamaLogin() !== 'tim_shs') { ?>
                            <?php
                            echo link_to('<i class="fas fa-edit"></i> Edit', 'user_app/editUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-success btn-flat btn-xs'));
                            if ($userlist->getUserEnable() == true) {
                                echo link_to('<i class="fas fa-lock"></i> Kunci User', 'user_app/kunciUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-default btn-flat btn-xs'));
                            } else {
                                echo link_to('<i class="fas fa-unlock"></i> Buka User', 'user_app/bukaUser?id=' . $userlist->getUserId(), array('class' => 'btn btn-default btn-flat btn-xs'));
                            }
                            echo link_to('<i class="fas fa-user"></i> Login', 'login/loginmonitoring?send_user=' . $userlist->getUserId() . '&passwordmd5=' . trim($userlist->getUserPassword()) . '&kunci=' . md5('monitor'), array('class' => 'btn btn-success btn-flat btn-xs', 'title' => $userlist->getUserId()));
                            echo link_to('<i class="fas fa-undo"></i> Reset', 'login/resetpassword?send_user=' . $userlist->getUserId() . '&kunci=' . md5('reset'), array('class' => 'btn btn-danger btn-flat btn-xs', 'title' => 'Reset Password', 'onclick' => 'return confirm("Apakah anda yakin reset password user ini? ");'));
                            ?>  
                            <?php } ?>                          
                        </div>
                    </td>
                </tr>
            <?php endforeach;
            ?>
    </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'user_app/userlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "user_app/userlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'user_app/userlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>