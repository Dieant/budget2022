<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah User Dinas</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master</a></li>
                    <li class="breadcrumb-item active">Tambah User Dinas</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('user_app/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form
                        </h3>
                    </div>
                    <div class="card-body">
                    <?php echo form_tag('user_app/saveuserbaru', array('method' => 'post', 'class' => 'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">SKPD</label>
                                    <?php
                                    $c = new Criteria();
                                    $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $rs_unit = UnitKerjaPeer::doSelect($c);
                                    echo select_tag('unit_id', objects_for_select($rs_unit, 'getUnitId', 'getUnitName', null, 'include_custom=---Pilih SKPD---'), array('class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">NIP</label>
                                    <?php
                                    echo select_tag('nip', options_for_select(array(), '', 'include_custom=---Pilih SKPD Dulu---'), Array('id' => 'nip', 'class' => 'form-control select2', 'style' => 'width:100%'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama</label>
                                    <?php echo input_tag('user_name', null, array('class' => 'form-control', 'placeholder' => 'Isikan Nama', 'required' => 'required', 'readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jabatan</label>
                                    <?php echo input_tag('jabatan', null, array('class' => 'form-control', 'placeholder' => 'Isikan jabatan', 'readonly' => 'true')) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">User Role</label>
                                    <select name="role" id="role" class="form-control select2">
                                        <option value="13">PPTK</option>
                                        <option value="14">KPA/PPKM</option>
                                        <option value="15">PA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">E-mail</label>
                                    <?php echo input_tag('email', null, array('type' => 'email', 'class' => 'form-control', 'placeholder' => 'Isikan e-mail')) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Telepon</label>
                                    <?php echo input_tag('telepon', null, array('class' => 'form-control', 'placeholder' => 'Isikan telepon')) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                        <?php echo submit_tag('Simpan', 'class=btn btn-outline-primary btn-sm'); ?>
                                        <?php echo reset_tag('Reset', 'class=btn btn-outline-danger btn-sm'); ?>
                                </div>
                            </div>
                        </div>
                    <?php echo '</form>' ?>
                    </div>
                </div>
            </div>
</section><!-- /.content -->
<script>
    $("#nip").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/user_app/pilihNama/b/" +
                id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#user_name').val(msg);
        });
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/user_app/pilihJabatan/b/" +
                id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#jabatan').val(msg);
        });
    });
    $("#unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/user_app/pilihNip/b/" +
                id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#nip').html(msg);
        });
    });
</script>