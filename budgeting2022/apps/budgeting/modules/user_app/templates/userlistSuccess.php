<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>User</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
          <li class="breadcrumb-item active">User</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('user_app/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                    <div class="card-tools">
                        <div class="input-group input-group-sm">
                            <?php 
                                echo link_to('Daftar Penyelia', sfConfig::get('app_path_pdf') . 'template/penyelia.jpeg', array('class' => 'btn btn-outline-primary btn-sm', 'target' => '_blank')); 
                            ?>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo form_tag('user_app/userlist', array('method' => 'get', 'class' => 'form-horizontal')) ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>ID User</label>
                            <?php echo input_tag('filters[userid]', isset($filters['userid']) ? $filters['userid'] : null, array('class' => 'inpText', 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Nama</label>
                            <?php echo input_tag('filters[username]', isset($filters['username']) ? $filters['username'] : null, array('class' => 'inpText', 'class' => 'form-control')); ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Level User</label>
                            <?php
                            echo select_tag('filters[level]', options_for_select(array(
                                '1' => 'Dinas Entri',
                                '13' => 'Dinas PPTK',
                                '14' => 'Dinas KPA',
                                '15' => 'Dinas PA',
                                '2' => 'Peneliti',
                                '9' => 'Administrator',
                                '16' => 'Bappeko-Mitra',
                                '12' => 'Viewer RKA',
                                '3' => 'Tim Data',
                                '4' => 'Tim Rekening',
                                '7' => 'Dewan'
                                ), isset($filters['level']) ? $filters['level'] : '', array('include_custom' => '--Pilih Level--')), array('class' => 'form-control select2','style' => 'width:100%'));
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                            <?php
                                echo link_to('Reset <i class="fa fa-backspace"></i>', 'user_app/userlist?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'))."&nbsp;";
                                if($sf_user->hasCredential('admin') && $sf_user->getNamaLogin() !== 'tim_shs') 
                                {
                                    echo link_to('New User', 'admin/newuser', array('class' => 'btn btn-outline-info btn-sm')).'&nbsp;';
                                    // echo link_to('New User Dinas', 'user_app/buatuser', array('class' => 'btn btn-outline-secondary btn-sm')).'&nbsp;';
                                }
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial("user_app/userlist", array('pager' => $pager)) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>