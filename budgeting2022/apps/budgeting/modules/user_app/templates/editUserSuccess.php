<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Ubah User - (<?php echo $data_user->getUserId(); ?>)</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
          <li class="breadcrumb-item active">Ubah User</li>
        </ol>
      </div>
    </div>
    <?php
        echo 'User ' . $data_user->getUserId() . ' mempunyai user role sebagai <strong>';
        if ($level_id == 1) {
            echo 'Dinas Entri';
        } else if ($level_id == 2) {
            echo 'Penyelia';
        } else if ($level_id == 3) {
            echo 'Tim Data';
        } else if ($level_id == 4) {
            echo 'Tim Rekening';
        } else if ($level_id == 7) {
            echo 'Dewan';
        } else if ($level_id == 8) {
            echo 'Bappeko';
        } else if ($level_id == 9) {
            echo 'Administrator';
        } else if ($level_id == 10) {
            echo 'Bappeko-PRK';
        } else if ($level_id == 11) {
            echo 'Bappeko-Mitra';
        } else if ($level_id == 12) {
            echo 'Viewer';
        } else if ($level_id == 13) {
            echo 'Dinas PPTK';
        } else if ($level_id == 14) {
            echo 'Dinas KPA';
        } else if ($level_id == 15) {
            echo 'Dinas PA';
        }
        echo '</strong>';
    ?>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->

<section class="content">
    <?php include_partial('user_app/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <?php if ($level_id == 1 || $level_id == 13 || $level_id == 14 || $level_id == 15) { ?>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Ubah Data User
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('user_app/saveedituser', array('method' => 'post', 'class' => 'form-horizontal')); ?>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Perangkat Daerah</label>
                                <?php 
                                $c = new Criteria();
                                $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                $rs_unit = UnitKerjaPeer::doSelect($c);
                                echo select_tag('unit_id', objects_for_select($rs_unit, 'getUnitId', 'getUnitName', $skpd, 'include_custom=--Pilih Perangkat Daerah--'), array('class' => 'js-example-basic-single select2', 'style' => 'width:100%', 'required' => 'required'));
                                ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <?php if ($level_id == 1): ?>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>ID User</label>
                                <?php echo input_tag('user', $data_user->getUserId(), array('class' => 'form-control', 'placeholder' => 'Isikan User ID', 'required' => 'required', 'readonly' => 'true')); ?>
                            </div>
                          </div>
                          <?php endif; ?>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>NIP</label>
                                <?php 
                                $c = new Criteria();
                                $c->addJoin(PegawaiPerformancePeer::SKPD_ID, MasterSkpdPeer::SKPD_ID);
                                $c->add(PegawaiPerformancePeer::TGL_KELUAR, NULL);
                                $c->add(PegawaiPerformancePeer::DELETED_AT, NULL);
                                $c->addAnd(MasterSkpdPeer::SKPD_KODE, $skpd);
                                $c->addAscendingOrderByColumn(PegawaiPerformancePeer::NAMA);
                                $rs_pegawai = PegawaiPerformancePeer::doSelect($c);
                                $arr_tampung = array();
                                foreach ($rs_pegawai as $pegawai) {
                                    $arr_tampung[$pegawai->getNip()] = $pegawai->getNipNama();
                                }
                                echo select_tag('nip', options_for_select($arr_tampung, $data_user->getNip(), 'include_custom=--Pilih Perangkat Daerah Dulu--'), Array('id' => 'nip', 'class' => 'js-example-basic-single select2', 'style' => 'width:100%', 'required' => 'required'));
                                ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama</label>
                                <?php echo input_tag('user_name', $data_user->getUserName(), array('class' => 'form-control', 'placeholder' => 'Isikan Nama', 'required' => 'required', 'readonly' => 'true')) ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Pangkat</label>
                                <?php echo input_tag('jabatan', $data_user->getJabatan(), array('class' => 'form-control', 'placeholder' => 'Isikan jabatan', 'readonly' => 'true')) ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>User Role</label>
                                <select name="role" class="form-control select2" id="role">
                                    <option value="1" <?php if ($level_id == 1) echo 'selected' ?>>Entri</option>
                                    <option value="13" <?php if ($level_id == 13) echo 'selected' ?>>PPTK</option>
                                    <option value="14" <?php if ($level_id == 14) echo 'selected' ?>>KPA/PPKM</option>
                                    <option value="15" <?php if ($level_id == 15) echo 'selected' ?>>PA</option>
                                </select>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <?php echo input_tag('email', $data_user->getEmail(), array('type' => 'email', 'class' => 'form-control', 'placeholder' => 'Isikan e-mail')) ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>No. Telepon/HP</label>
                                <?php echo input_tag('telepon', $data_user->getTelepon(), array('class' => 'form-control', 'placeholder' => 'Isikan telepon')) ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                                <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                                <?php          
                                $c = new Criteria();
                                $c->add(UserHandleV2Peer::USER_ID, $data_user->getUserId());
                                $unit = UserHandleV2Peer::doSelectOne($c);
                                echo input_hidden_tag('unit_id_lama', $unit->getUnitId());
                                echo input_hidden_tag('user_id', $data_user->getUserId());
                                ?>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col -->                      
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Ubah Password
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('user_app/saveresetpassworduser', array('method' => 'post', 'class' => 'form-horizontal')); ?>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Password Baru</label>
                                <?php echo form_error('pass_baru') ?>
                                <?php echo input_password_tag('pass_baru', '', array('class' => 'form-control', 'placeholder' => 'Password Baru', 'required' => 'required')) ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Password Baru</label>
                                <?php echo form_error('ulang_pass_baru') ?>
                                <?php echo input_password_tag('ulang_pass_baru', '', array('class' => 'form-control', 'placeholder' => 'Konfirmasi Password Baru', 'required' => 'required')); ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                                <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                                <?php
                                    echo input_hidden_tag('user_id', $data_user->getUserId());
                                ?>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col -->                      
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
            <?php if ($butuh_dinas > 0) { ?>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form Ubah SKPD
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('user_app/saveambillepasskpd', array('method' => 'post', 'class' => 'form-horizontal')); ?>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                                <label>Perangkat Daerah</label>
                                <?php echo select_tag('ambil_skpd', objects_for_select($list_skpd, 'getUnitId', 'getUnitName', '', 'include_custom=--Pilih Perangkat Daerah--'), array('class' => 'js-example-basic-multiple select2', 'style' => 'width:100%', 'multiple' => true)); ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <?php if (!$pager->getNbResults()): ?>
                                    <?php echo __('no result') ?>
                                <?php else: ?>
                                    <?php include_partial('user_app/pilihlepasskpd', array('pager' => $pager)) ?>
                                <?php endif; ?>
                            </div>
                          </div>
                          <!-- /.col -->
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                                <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                                <?php
                                    echo input_hidden_tag('user_id', $data_user->getUserId());
                                ?>
                            </div>
                            <!-- /.form-group -->
                          </div>
                          <!-- /.col -->                      
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $(".js-example-basic-multiple").select2();
    });
    $("#nip").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/user_app/pilihNama/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#user_name').val(msg);
        });
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/user_app/pilihJabatan/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#jabatan').val(msg);
        });
    });
    $("#unit_id").change(function () {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/user_app/pilihNip/b/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            $('#nip').html(msg);
        });
    });
</script>