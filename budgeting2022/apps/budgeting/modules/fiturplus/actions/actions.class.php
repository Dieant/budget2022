<?php

/**
 * fiturplus actions.
 *
 * @package    budgeting
 * @subpackage fiturplus
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class fiturplusActions extends sfActions {

    public function executePraPak() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query = " select * "
                    . " from ebudget.prapak_rincian_detail_baru "
                    . " order by unit_id, kegiatan_code ";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $unit_id = $rs->getString('unit_id');
                $kode_kegiatan = $rs->getString('kegiatan_code');
                $tipe = $rs->getString('tipe');
                $rekening = $rs->getString('rekening_code');
                $komponen_id = $rs->getString('komponen_id');
                $detail_name_baru = str_replace("'", "''", $rs->getString('detail_name'));
                $volume = $rs->getString('volume');
                $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                $subtitle = $rs->getString('subtitle');
                $komponen_harga = $rs->getString('komponen_harga_awal');
                $komponen_name = str_replace("'", "''", $rs->getString('komponen_name'));
                $satuan = $rs->getString('satuan');
                $pajak = $rs->getString('pajak');
                $kode_sub = $rs->getString('kode_sub');
                $subSubtitle = $rs->getString('sub');
                $kode_jasmas = $rs->getString('kecamatan');
                $dinas = $rs->getString('last_update_user');
                $isBlud = $rs->getString('is_blud');
                $isMusrenbang = $rs->getString('is_musrenbang');
                $isHibah = $rs->getString('is_hibah');
                $kecamatan = $rs->getString('lokasi_kecamatan');
                $kelurahan = $rs->getString('lokasi_kelurahan');
                $note_skpd = $rs->getString('note_skpd');
                $potongBPJS = $rs->getString('is_potong_bpjs');
                $iuranBPJS = $rs->getString('is_iuran_bpjs');
                $tahap = $rs->getString('tahap');
                $ini_komponen_jkn = $rs->getString('is_iuran_jkn');
                $ini_komponen_jkk = $rs->getString('is_iuran_jkk');
                $tipe2 = $rs->getString('tipe2');

                $query = " select max(detail_no) as nilai "
                        . " from ebudget.prapak_rincian_detail "
                        . " where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' ";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_det = $stmt->executeQuery();
                if ($rs_det->next()) {
                    $detail_no_baru = $rs_det->getString('nilai') + 1;
                }

                $kode_detail_kegiatan = $unit_id . '.' . $kode_kegiatan . '.' . $detail_no_baru;

                $query = "insert into " . sfConfig::get('app_default_schema') . ".prapak_rincian_detail
                    (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                    komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                    lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,status_sisipan,akrual_code,tipe2,detail_kegiatan,status_komponen_baru)
                    values
                    ('" . $kode_kegiatan . "', '" . $tipe . "', " . $detail_no_baru . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . str_replace("'", "''", $subtitle) . "',
                        " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                            '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', false, '', '$tipe2', '$kode_detail_kegiatan', false)";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }
            $con->commit();
            echo 'BERHASIL';
        } catch (Exception $ex) {
            $con->rollback();
            echo $ex->getMessage();
        }
        exit;
    }

    public function executeUploadCekSsh() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'sshmurni_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 6; $row <= $highR; $row++) {
                    $kode = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $merk = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $hidden = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                    $harga = str_replace('\'', '', $harga);
                    $harga = str_replace(',', '', $harga);
                    $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                    $status_masuk = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
                    $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
                    $is_survey_bp = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));

                    //echo 'Coba';
                    // echo 'SSH dengan kode '.$kode .' '. $satuan.' Harga '.$harga.'&nbsp';
                    if ($satuan != '') {
                        //echo "$kode | $nama | $merk | $spec | $hidden | $satuan | $harga | $pajak<br>";
                        $query = "select * from ebudget.shsd where shsd_id = '" . $kode . "' and (shsd_name not ilike '%" . $nama . "%' or shsd_harga <>" . $harga . ")";
                        $stmt = $con->prepareStatement($query);


                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            echo 'SSH dengan kode' . $kode . ' ' . $nama . ' Harga ' . $harga . '&nbsp';
                            echo '|| Di Aplikasi : ' . $kode . ' ' . $rs->getString('shsd_name') . ' Harga ' . $rs->getString('shsd_harga') . '<br>';
                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                //$con->commit();
                //$this->setFlash('berhasil', 'SSH telah terupload');
                exit;
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadOutputKegiatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();

            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $indikator = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $target = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));

                if ($kode_kegiatan != '') {
                    $c = new Criteria();
                    $c->add(OutputSubtitlePeer::UNIT_ID, $unit_id);
                    $c->add(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan);
                    if ($kegiatan = OutputSubtitlePeer::doSelectOne($c)) {
                        $kegiatan->setOutput($indikator . '|' . $target . '|' . $satuan);
                        $kegiatan->save();
                    } else {
                        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                        //echo $ex; exit;
                        $con->rollback();
                        $this->setFlash('gagal', 'Gagal karena ' . $kode_kegiatan);
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadOutputSubtitle() {
        $con = Propel::getConnection();
        //$con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();

            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $indikator = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $target = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $nilai = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));

                if ($kode_kegiatan != '') {
                    $c = new Criteria();
                    $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle);
                    if ($sub = DinasSubtitleIndikatorPeer::doSelectOne($c)) {
//                        $sub->setIndikator($indikator);
//                        $sub->setNilai($target);
//                        $sub->setSatuan($satuan);
//                        $sub->setAlokasiDana($nilai);
//                        $sub->save();
                        $query2 = "update ebudget.dinas_subtitle_indikator
                                 set indikator='$indikator', nilai=$target, satuan='$satuan', alokasi_dana=$nilai
                                 where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='$subtitle'";
                        $stmt2 = $con->prepareStatement($query2);
                        $stmt2->executeQuery();
                    } else {
                        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                        //echo $ex; exit;
                        //$con->rollback();
                        $this->setFlash('gagal', 'Gagal karena ' . $kode_kegiatan . ' - ' . $subtitle);
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
//            exit;
            //$con->commit();
            $this->setFlash('berhasil', 'Telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            //$con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadMasterSasaranTujuanMisi() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();

            $id_tujuan = 224;
            $id_sasaran = 120;

            for ($row = 2; $row <= $highR; $row++) {
                $kode_sasaran = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $nama_sasaran = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $kode_tujuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $nama_tujuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $kode_misi = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $nama_misi = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));

                if ($kode_sasaran != '') {
                    $c = new Criteria();
                    $c->add(MasterMisiPeer::KODE_MISI, $kode_misi);
                    if ($misi = MasterMisiPeer::doSelectOne($c)) {
                        $misi->setNamaMisi($nama_misi);
                        $misi->save();
                    } else {
                        $misi = new MasterMisi();
                        $misi->setKodeMisi($kode_misi);
                        $misi->setNamaMisi($nama_misi);
                        $misi->save();
                    }

                    $c = new Criteria();
                    $c->add(MasterTujuanPeer::KODE_TUJUAN, $kode_tujuan);
                    if ($tujuan = MasterTujuanPeer::doSelectOne($c)) {
                        $tujuan->setNamaTujuan($nama_tujuan);
                        $tujuan->setKodeMisi($kode_misi);
                        $tujuan->save();
                    } else {
                        $tujuan = new MasterTujuan();
                        $tujuan->setKodeTujuan($kode_tujuan);
                        $tujuan->setNamaTujuan($nama_tujuan);
                        $tujuan->setKodeMisi($kode_misi);
                        //$tujuan->setId($id_tujuan);
                        $tujuan->save();
                        $id_tujuan++;
                    }

                    $c = new Criteria();
                    $c->add(MasterSasaranPeer::KODE_SASARAN, $kode_sasaran);
                    if ($sasaran = MasterSasaranPeer::doSelectOne($c)) {
                        $sasaran->setNamaSasaran($nama_sasaran);
                        $sasaran->setKodeMisi($kode_misi);
                        $sasaran->setKodeTujuan($kode_tujuan);
                        $sasaran->save();
                    } else {
                        $sasaran = new MasterSasaran();
                        $sasaran->setKodeSasaran($kode_sasaran);
                        $sasaran->setNamaSasaran($nama_sasaran);
                        $sasaran->setKodeMisi($kode_misi);
                        $sasaran->setKodeTujuan($kode_tujuan);
                        //$sasaran->setId($id_sasaran);
                        $sasaran->save();
                        $id_sasaran++;
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Program telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadSasaranTujuanMisi() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();

            $id_tujuan = 224;
            $id_sasaran = 120;

            for ($row = 2; $row <= $highR; $row++) {
                $kode_misi = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kode_tujuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $kode_sasaran = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $unit_id = str_replace('#', '', utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue())));

                if ($kode_sasaran != '') {
                    $c = new Criteria();
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    if ($kegiatan = DinasMasterKegiatanPeer::doSelectOne($c)) {
                        $kegiatan->setKodeMisi($kode_misi);
                        $kegiatan->setKodeTujuan($kode_tujuan);
                        $kegiatan->setKodeSasaran($kode_sasaran);
                        $kegiatan->save();
                    } else {
                        echo 'Kegiatan tidak ada ' . $kode_kegiatan . ' - ' . $unit_id;
                        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                        $con->rollback();
                        exit;
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Program telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executePerbaikiWaitinglist() {
        $con = Propel::getConnection();
        $con->begin();
        $congis = Propel::getConnection('gis');
        $query = "select unit_id, kegiatan_code, id_waiting
                from ebudget.waitinglist_pu
                where (unit_id,kegiatan_code) in (select 'XXX'||unit_id_baru, kegiatan_code_baru from ebudget.konversi_subtitle where unit_id_lama='2600' and kegiatan_code_lama<>kegiatan_code_baru and
                kegiatan_code_lama in
                (select kode_kegiatan from ebudget.rkua_master_kegiatan where user_id='jalan_2600' or user_id='pematusan_2600'))";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $id = $rs->getString('id_waiting');
            $kode_rka = $rs->getString('unit_id') . '.' . $rs->getString('kegiatan_code') . '.' . $rs->getString('id_waiting');
            $query2 = "update history_pekerjaan_v2 set kode_rka='$kode_rka'
                    where kode_rka like 'XXX2600.%.$id' and tahun='2017'";
            $stmt2 = $congis->prepareStatement($query2);
            $stmt2->executeQuery();
//            $query2 = "select kode_rka from history_pekerjaan_v2
//                    where kode_rka like 'XXX2600.%.$id' and tahun='2017'";
//            $stmt2 = $congis->prepareStatement($query2);
//            $rs2 = $stmt2->executeQuery();
//            while ($rs2->next()) {
//                echo $rs2->getString('kode_rka') . ' ' . $kode_rka . '<br>';
//            }
        }
        //exit;
        $con->commit();
        $this->setFlash('berhasil', 'Sudah terupdate');
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadProgramBaru() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $kode = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));

                if ($kode != '') {
                    $c = new Criteria();
                    $c->add(MasterProgram2Peer::KODE_PROGRAM2, $kode);
                    if ($program = MasterProgram2Peer::doSelectOne($c)) {
//                        $program->setNamaProgram2($nama);
//                        $program->save();
                        $query = "update ebudget.master_program2 set nama_program2='$nama' where kode_program2='$kode'";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();
                    } else {
                        $program = new MasterProgram2();
                        $program->setKodeProgram('99');
                        $program->setNamaProgram2($nama);
                        $program->setKodeProgram2($kode);
                        $program->save();
//                        $query = "insert into ebudget.master_program2 values ('99','$kode', '$nama')";
//                        $stmt = $con->prepareStatement($query);
//                        $stmt->executeQuery();
//                        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
//                        //echo $ex; exit;
//                        $con->rollback();
//                        $this->setFlash('gagal', 'Gagal karena kode baru ini ' . $kode);
                    }
                    //echo $kode . ' ' . $nama . '<br>';
                    //echo $query . '<br>';
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Program telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadUrusanProgram() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $kode_urusan1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $nama_urusan1 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $kode_urusan2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $nama_urusan2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $kode_urusan3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $nama_urusan3 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $kode_urusan4 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $nama_urusan4 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $kode_program = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $nama_program = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));

                if ($kode_urusan1 != '') {
                    $c = new Criteria();
                    $c->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan1);
                    if ($urusan = MasterUrusanPeer::doSelectOne($c)) {
                        $urusan->setNamaUrusan($nama_urusan1);
                        $urusan->save();
                    } else {
                        $urusan = new MasterUrusan();
                        $urusan->setKodeUrusan($kode_urusan1);
                        $urusan->setNamaUrusan($nama_urusan1);
                        $urusan->save();
                    }

                    $c = new Criteria();
                    $c->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan2);
                    if ($urusan2 = MasterUrusanPeer::doSelectOne($c)) {
                        $urusan2->setNamaUrusan($nama_urusan2);
                        $urusan2->save();
                    } else {
                        $urusan2 = new MasterUrusan();
                        $urusan2->setKodeUrusan($kode_urusan2);
                        $urusan2->setNamaUrusan($nama_urusan2);
                        $urusan2->save();
                    }

                    $c = new Criteria();
                    $c->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan3);
                    if ($urusan3 = MasterUrusanPeer::doSelectOne($c)) {
                        $urusan3->setNamaUrusan($nama_urusan3);
                        $urusan3->save();
                    } else {
                        $urusan3 = new MasterUrusan();
                        $urusan3->setKodeUrusan($kode_urusan3);
                        $urusan3->setNamaUrusan($nama_urusan3);
                        $urusan3->save();
                    }

                    $c = new Criteria();
                    $c->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan4);
                    if ($urusan4 = MasterUrusanPeer::doSelectOne($c)) {
                        $urusan4->setNamaUrusan($nama_urusan4);
                        $urusan4->save();
                    } else {
                        $urusan4 = new MasterUrusan();
                        $urusan4->setKodeUrusan($kode_urusan4);
                        $urusan4->setNamaUrusan($nama_urusan4);
                        $urusan4->save();
                    }

                    $c = new Criteria();
                    $c->add(MasterProgram2Peer::KODE_PROGRAM2, $kode_program);
                    if ($program = MasterProgram2Peer::doSelectOne($c)) {
                        $program->setNamaProgram2($nama_program);
                        $program->save();
                    } else {
                        $program = new MasterProgram2();
                        $program->setKodeProgram('99');
                        $program->setKodeProgram2($kode_program);
                        $program->setNamaProgram2($nama_program);
                        $program->save();
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Program telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKegiatanBaru() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = str_replace('#', '', utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $nama_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $output_old = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $target = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $nilai = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $output_subtitle_old = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $target_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                $satuan_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                $nilai_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));

                $arr_error = array();
                if ($kode_kegiatan != '') {
                    $output = $output_old . '|' . $target . '|' . $satuan;
                    $output_subtitle = $output_subtitle_old . '|' . $target_subtitle . '|' . $satuan_subtitle;
                    $kode_lama = '';
                    $subtitle_lama = '';
                    $query = "select kode_kegiatan from ebudget.dinas_master_kegiatan
                        where unit_id='$unit_id' and nama_kegiatan ilike '$nama_kegiatan'";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $kode_lama = $rs->getString('kode_kegiatan');
                        $query = "select sub_id from ebudget.dinas_subtitle_indikator
                        where unit_id='$unit_id' and kegiatan_code='$kode_lama' and subtitle ilike '$subtitle'";
                        $stmt = $con->prepareStatement($query);
                        $rs2 = $stmt->executeQuery();
                        while ($rs2->next()) {
                            $subtitle_lama = $rs2->getString('sub_id');
                        }
                    }
                    $query = "insert into ebudget.baru
                        values('$unit_id','$kode_kegiatan', '$nama_kegiatan', '$output', $nilai, '$subtitle', '$output_subtitle', $nilai_subtitle)";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();

                    if ($kode_lama != '' && $subtitle_lama != '') {
                        $c = new Criteria();
                        $c->add(KonversiSubtitlePeer::UNIT_ID_LAMA, $unit_id);
                        $c->add(KonversiSubtitlePeer::KEGIATAN_CODE_LAMA, $kode_kegiatan);
                        $c->add(KonversiSubtitlePeer::SUBTITLE_LAMA, $subtitle);
                        if (!KonversiSubtitlePeer::doSelectOne($c)) {
                            $query = "insert into ebudget.konversi_subtitle
                                        values('$unit_id','$kode_lama', '$subtitle_lama', '$unit_id','$kode_kegiatan', '$subtitle')";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();
                        }
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kode kegiatan baru telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKegiatanBaru2() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = str_replace('#', '', utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $nama_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $output_old = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $target = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $nilai = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $output_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $target_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                $satuan_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                $nilai_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));

                $arr_error = array();
                if ($kode_kegiatan != '') {
                    $output = $output_old . '|' . $target . '|' . $satuan;
                    //$output_subtitle = $output_subtitle_old . '|' . $target_subtitle . '|' . $satuan_subtitle;
                    $kode_lama = '';
                    $subtitle_lama = '';
                    $query = "select kode_kegiatan from ebudget.dinas_master_kegiatan
                        where unit_id='$unit_id' and nama_kegiatan ilike '$nama_kegiatan'";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $kode_lama = $rs->getString('kode_kegiatan');
                        $query = "select sub_id, subtitle from ebudget.dinas_subtitle_indikator
                        where unit_id='$unit_id' and kegiatan_code='$kode_lama' and subtitle ilike '$subtitle'";
                        $stmt = $con->prepareStatement($query);
                        $rs2 = $stmt->executeQuery();
                        while ($rs2->next()) {
                            $subtitle_lama = $rs2->getString('sub_id');
                            $nama_subtitle_lama = $rs2->getString('subtitle');
                        }
                    }
                    $query = "insert into ebudget.baru2
                        values('$unit_id','$kode_kegiatan', '$nama_kegiatan', '$output', $nilai, '$subtitle', '$output_subtitle', $nilai_subtitle, '$target_subtitle', '$satuan_subtitle')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();

                    if ($kode_lama != '' && $subtitle_lama != '') {
//                        $c = new Criteria();
//                        $c->add(KonversiSubtitle2Peer::UNIT_ID_LAMA, $unit_id);
//                        $c->add(KonversiSubtitle2Peer::KEGIATAN_CODE_LAMA, $kode_kegiatan);
//                        $c->add(KonversiSubtitle2Peer::SUBTITLE_LAMA, $subtitle);
//                        if (!KonversiSubtitle2Peer::doSelectOne($c)) {
                        $query = "insert into ebudget.konversi_subtitle2
                                        values('$unit_id','$kode_lama', '$nama_subtitle_lama', '$unit_id','$kode_kegiatan', '$subtitle', FALSE)";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();
//                        }
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kode kegiatan baru telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKegiatanBaru3() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = str_replace('#', '', utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue())));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $nama_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $output_old = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $target = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $nilai = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $keterangan_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $output_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                $target_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                $satuan_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
                $nilai_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                $keterangan_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));

                if ($unit_id != '' && $kode_kegiatan != '') {
                    $output = $output_old . '|' . $target . '|' . $satuan;

                    $query = "insert into ebudget.baru3
                        values('$unit_id','$kode_kegiatan', '$nama_kegiatan', '$output', $nilai, '$subtitle', '$output_subtitle', $nilai_subtitle, '$target_subtitle', '$satuan_subtitle', '$keterangan_kegiatan', '$keterangan_subtitle')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kode kegiatan baru telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeKonversiSubtitle() {
        $con = Propel::getConnection();
        $con->begin();
        $congis = Propel::getConnection('gis');
//        $query = "select max(sub_id) as id from ebudget.dinas_subtitle_indikator";
//        $stmt2 = $con->prepareStatement($query);
//        $rs2 = $stmt2->executeQuery();
//        $rs2->next();
//        $sub_id_baru = $rs2->getString('id');
        //try {
        $c = new Criteria();
        $c->add(KonversiSubtitlePeer::MASUK, TRUE, Criteria::NOT_EQUAL);
        $konversi = KonversiSubtitlePeer::doSelect($c);
        foreach ($konversi as $value) {
            $unit_id_lama = $value->getUnitIdLama();
            $kegiatan_code_lama = $value->getKegiatanCodeLama();
            $subtitle_lama = $value->getSubtitleLama();
            $unit_id_baru = $value->getUnitIdBaru();
            $kegiatan_code_baru = $value->getKegiatanCodeBaru();
            $subtitle_baru = $value->getSubtitleBaru();

            //cek dahulu
//            $c = new Criteria();
//            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
//            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
//            $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle_lama);
//            if (!DinasRincianDetailPeer::doSelectOne($c)) {
//                $con->rollback();
//                $this->setFlash('gagal', 'Ada yang tidak ketemu' . $unit_id_lama . ' - ' . $kegiatan_code_lama . ' - ' . $subtitle_lama);
//                return $this->redirect('admin/fiturTambahan');
//            }
            $query = " update ebudget.konversi_subtitle set masuk=true "
                    . " where unit_id_lama='$unit_id_lama' and kegiatan_code_lama='$kegiatan_code_lama' and subtitle_lama='$subtitle_lama' ";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
//            $value->setMasuk(TRUE);
//            $value->save();
//            $query = "select * from ebudget.baru
//                        where unit_id='$unit_id_baru' and kegiatan_code='$kegiatan_code_baru' and subtitle='$subtitle_lama'";
//            $stmt = $con->prepareStatement($query);
//            $rs = $stmt->executeQuery();
//            while ($rs->next()) {
//                //buat dan update kegiatan
//                if ($temp_keg != $rs->getString('kegiatan_code')) {
//                    $c = new Criteria();
//                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_baru);
//                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code_baru);
//                    if ($kegiatan = DinasMasterKegiatanPeer::doSelectOne($c)) {
//                        $kegiatan->setNamaKegiatan($rs->getString('nama_kegiatan'));
//                        $kegiatan->setOutput($rs->getString('output'));
//                        $kegiatan->setAlokasiDana($rs->getString('nilai_kegiatan'));
//                        $kegiatan->setTambahanPagu(0);
//                    } else {
//                        $kode_urusan = substr($rs->getString('kegiatan_code'), 0, 8);
//                        $kode_program2 = substr($rs->getString('kegiatan_code'), 0, 11);
//                        $kegiatan = new DinasMasterKegiatan();
//                        $kegiatan->setUnitId($rs->getString('unit_id'));
//                        $kegiatan->setKodeKegiatan($rs->getString('kegiatan_code'));
//                        $kegiatan->setNamaKegiatan($rs->getString('nama_kegiatan'));
//                        $kegiatan->setOutput($rs->getString('output'));
//                        $kegiatan->setAlokasiDana($rs->getString('nilai_kegiatan'));
//                        $kegiatan->setKodeUrusan($kode_urusan);
//                        $kegiatan->setKodeProgram2($kode_program2);
//                        $kegiatan->setTahun('2017');
//                        $kegiatan->setTahap('murni');
//                        $kegiatan->setTambahanPagu(0);
//                        $rincian = new DinasRincian();
//                        $rincian->setUnitId($rs->getString('unit_id'));
//                        $rincian->setKegiatanCode($rs->getString('kegiatan_code'));
//                        $rincian->setTipe('murni');
//                        $rincian->setTahun('2017');
//                        $rincian->save();
//                    }
//                    $kegiatan->save();
//                }
//                $temp_keg = $rs->getString('kegiatan_code');
//
//                //update nilai subtitle
//                $arrPecah = explode('|', $rs->getString('output_subtitle'));
//                $c = new Criteria();
//                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id_baru);
//                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code_baru);
//                $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle_baru);
//                if ($sub = DinasSubtitleIndikatorPeer::doSelectOne($c)) {
//                    $sub->setIndikator($arrPecah[0]);
//                    $sub->setNilai($arrPecah[1]);
//                    $sub->setSatuan($arrPecah[2]);
//                    $sub->setAlokasiDana($rs->getString('nilai_subtitle'));
//                } else {
//                    //$sub_id_baru++;
//                    $sub = new DinasSubtitleIndikator();
//                    //$sub->setSubId($sub_id_baru);
//                    $sub->setUnitId($rs->getString('unit_id'));
//                    $sub->setKegiatanCode($rs->getString('kegiatan_code'));
//                    $sub->setSubtitle($rs->getString('subtitle'));
//                    $sub->setIndikator($arrPecah[0]);
//                    $sub->setNilai($arrPecah[1]);
//                    $sub->setSatuan($arrPecah[2]);
//                    $sub->setAlokasiDana($rs->getString('nilai_subtitle'));
//                }
//                $sub->save();
//            }
            //cek kalau nabrak
//            $c = new Criteria();
//            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_baru);
//            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_baru);
//            if (DinasRincianDetailPeer::doSelectOne($c)) {
//                //buat detail_no baru
//                $sesuatu = new DinasRincianDetail;
//                $det_no = $sesuatu->getMaxDetailNo($unit_id_baru, $kegiatan_code_baru);
//                //update 1 per 1
//                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
//                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
//                $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle_lama);
//                $rd_lama = DinasRincianDetailPeer::doSelect($c);
//                foreach ($rd_lama as $value2) {
//                    $det_no_lama = $value2->getDetailNo();
//                    $query = " update ebudget.dinas_rincian_detail "
//                            . " set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', detail_no='$det_no', subtitle='$subtitle_baru' "
//                            . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
//                    $stmt = $con->prepareStatement($query);
//                    $stmt->executeQuery();
//                    //update rd
////                    $value2->setUnitId($unit_id_baru);
////                    $value2->setKegiatanCode($kegiatan_code_baru);
////                    $value2->setDetailNo($det_no);
////                    $value2->setSubtitle($subtitle_baru);
////                    $value2->save();
//                    $det_no++;
//
//                    //update gis
//                    $kode_rka = $unit_id_lama . '.' . $kegiatan_code_lama . '.' . $det_no_lama;
//                    $kode_rka_baru = $unit_id_baru . '.' . $kegiatan_code_baru . '.' . $det_no;
//                    $query = "update history_pekerjaan_v2
//                        set kode_rka='$kode_rka_baru'
//                        where kode_rka='$kode_rka' and tahun='2017'";
//                    $stmtgis = $congis->prepareStatement($query);
//                    $stmtgis->executeQuery();
//                }
//            } else {
//                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
//                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
//                $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle_lama);
//                $rd_lama = DinasRincianDetailPeer::doSelect($c);
//                foreach ($rd_lama as $value2) {
//                    $det_no_lama = $value2->getDetailNo();
//                    $query = " update ebudget.dinas_rincian_detail "
//                            . " set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru' "
//                            . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
//                    $stmt = $con->prepareStatement($query);
//                    $stmt->executeQuery();
//                    //update rd
////                    $value2->setUnitId($unit_id_baru);
////                    $value2->setKegiatanCode($kegiatan_code_baru);
////                    $value2->setSubtitle($subtitle_baru);
////                    $value2->save();
//                    //update gis
//                    $kode_rka = $unit_id_lama . '.' . $kegiatan_code_lama . '.' . $det_no_lama;
//                    $kode_rka_baru = $unit_id_baru . '.' . $kegiatan_code_baru . '.' . $det_no;
//                    $query = "update history_pekerjaan_v2
//                        set kode_rka='$kode_rka_baru'
//                        where kode_rka='$kode_rka' and tahun='2017'";
//                    $stmtgis = $congis->prepareStatement($query);
//                    $stmtgis->executeQuery();
//                }
//            }
            //buat detail_no baru
            $sesuatu = new DinasRincianDetail;
            $det_no = $sesuatu->getMaxDetailNo($unit_id_baru, $kegiatan_code_baru);
            //update 1 per 1
            $c = new Criteria();
            $c->add(RkuaRincianDetailPeer::UNIT_ID, $unit_id_lama);
            $c->add(RkuaRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
            $c->add(RkuaRincianDetailPeer::SUBTITLE, $subtitle_lama);
            $rd_lama = RkuaRincianDetailPeer::doSelect($c);
            foreach ($rd_lama as $value2) {
                //update rd
                $det_no_lama = $value2->getDetailNo();
                $query = " insert into ebudget.dinas_rincian_detail "
                        . " select '$kegiatan_code_baru',  tipe,  '$det_no',  rekening_code,  komponen_id,  detail_name,  volume,  keterangan_koefisien,  '$subtitle_baru',  komponen_harga,  komponen_harga_awal,  komponen_name,  satuan,  pajak,  '$unit_id_baru',  from_sub_kegiatan,  sub,  kode_sub,  last_update_user,  last_update_time,  last_update_ip,  tahap,  tahap_edit,  tahap_new,  status_lelang,  nomor_lelang,  koefisien_semula,  volume_semula,  harga_semula,  total_semula,  lock_subtitle,  status_hapus,  tahun,  kode_lokasi,  kecamatan,  rekening_code_asli,  nilai_anggaran,  note_skpd,  note_peneliti,  asal_kegiatan,  is_blud,  ob,  ob_from_id,  is_per_komponen,  kegiatan_code_asal,  th_ke_multiyears,  lokasi_kecamatan,  lokasi_kelurahan,  harga_sebelum_sisa_lelang,  is_musrenbang,  sub_id_asal,  subtitle_asal,  kode_sub_asal,  sub_asal,  last_edit_time,  is_potong_bpjs,  is_iuran_bpjs,  is_kapitasi_bpjs,  is_iuran_jkn,  is_iuran_jkk,  status_ob,  ob_parent,  ob_alokasi_baru,  is_hibah,  status_level,  status_sisipan,  status_level_tolak,  is_tapd_setuju,  is_bappeko_setuju,  akrual_code,  tipe2,  is_penyelia_setuju,  note_tapd,  note_bappeko "
                        . " from ebudget.rkua_rincian_detail "
                        . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                //update gis
                $kode_rka = $unit_id_lama . '.' . $kegiatan_code_lama . '.' . $det_no_lama;
                $kode_rka_baru = $unit_id_baru . '.' . $kegiatan_code_baru . '.' . $det_no;
                $query = "update history_pekerjaan_v2
                        set kode_rka='$kode_rka_baru'
                        where kode_rka='$kode_rka'";
                $stmtgis = $congis->prepareStatement($query);
                $stmtgis->executeQuery();

                //update gmap
                $query = "update gis_ebudget.geojsonlokasi_rev1
                        set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', detail_no='$det_no'
                        where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                //update waitinglist
                if ($unit_id_lama == '2600') {
                    $query = "update ebudget.waitinglist_pu 
                        set kode_rka='$kode_rka_baru'
                        where kode_rka='$kode_rka'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }

                $det_no++;
            }

            //upadte waitinglist_pu
            if ($unit_id_lama == '2600') {
                $query = "update ebudget.waitinglist_pu 
                        set unit_id='XXX$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru'
                        where unit_id='XXX$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and subtitle='$subtitle_lama'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update gis_ebudget.geojsonlokasi_waitinglist a
                        set kegiatan_code=b.kegiatan_code
                        from ebudget.waitinglist_pu b
                        where a.id_waiting=b.id_waiting and a.kegiatan_code<>b.kegiatan_code";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
//                $c = new Criteria();
//                $c->add(WaitingListPUPeer::UNIT_ID, 'XXX' . $unit_id_lama);
//                $c->add(WaitingListPUPeer::KEGIATAN_CODE, $kegiatan_code_lama);
//                $c->add(WaitingListPUPeer::SUBTITLE, $subtitle_lama);
//                $rd_lama = WaitingListPUPeer::doSelect($c);
//                foreach ($rd_lama as $value2) {
//                    
//                }
            }

            $query = " update ebudget.dinas_subtitle_indikator s1
    set prioritas=s.prioritas
    from ebudget.rkua_subtitle_indikator s, ebudget.konversi_subtitle k
    where s.subtitle=k.subtitle_lama and s.unit_id=k.unit_id_lama and s.kegiatan_code=k.kegiatan_code_lama and
    s1.subtitle=k.subtitle_baru and s1.unit_id=k.unit_id_baru and s1.kegiatan_code=k.kegiatan_code_baru and s.prioritas<>s1.prioritas";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            //update dinas_subtitle_indikator
//            $c = new Criteria();
//            $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id_baru);
//            $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code_baru);
//            $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle_lama);
//            if (!DinasSubtitleIndikatorPeer::doSelectOne($c)) {
//                $query = "update ebudget.dinas_subtitle_indikator 
//                        set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru'
//                        where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and subtitle='$subtitle_lama'";
//                $stmt = $con->prepareStatement($query);
//                $stmt->executeQuery();
//            }
        }
        $con->commit();
        $this->setFlash('berhasil', 'Sudah terkonversi');
        return $this->redirect('admin/fiturTambahan');
//        } catch (Exception $ex) {
//            $con->rollback();
//            $this->setFlash('gagal', 'Terjadi kesalahan');
//            return $this->redirect('admin/fiturTambahan');
//        }
    }

    public function executeKonversiSubtitleBaru() {
        $con = Propel::getConnection();
        $con->begin();
        $congis = Propel::getConnection('gis');
        $c = new Criteria();
        $c->add(KonversiSubtitle2Peer::MASUK, TRUE, Criteria::NOT_EQUAL);
        $konversi = KonversiSubtitle2Peer::doSelect($c);
        foreach ($konversi as $value) {
            $unit_id_lama = $value->getUnitIdLama();
            $kegiatan_code_lama = $value->getKegiatanCodeLama();
            $subtitle_lama = $value->getSubtitleLama();
            $unit_id_baru = $value->getUnitIdBaru();
            $kegiatan_code_baru = $value->getKegiatanCodeBaru();
            $subtitle_baru = $value->getSubtitleBaru();

            //cek dahulu
            $query = " update ebudget.konversi_subtitle2 set masuk=true "
                    . " where unit_id_lama='$unit_id_lama' and kegiatan_code_lama='$kegiatan_code_lama' and subtitle_lama='$subtitle_lama' ";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            //buat detail_no baru
            $sesuatu = new DinasRincianDetail;
            $det_no = $sesuatu->getMaxDetailNo($unit_id_baru, $kegiatan_code_baru);
            //update 1 per 1
            $c = new Criteria();
            $c->add(Rkua2RincianDetailPeer::UNIT_ID, $unit_id_lama);
            $c->add(Rkua2RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
            $c->add(Rkua2RincianDetailPeer::SUBTITLE, $subtitle_lama);
            $rd_lama = Rkua2RincianDetailPeer::doSelect($c);
            foreach ($rd_lama as $value2) {
                //update rd
                $det_no_lama = $value2->getDetailNo();
                $query = " insert into ebudget.dinas_rincian_detail "
                        . " select '$kegiatan_code_baru',  tipe,  '$det_no',  rekening_code,  komponen_id,  detail_name,  volume,  keterangan_koefisien,  '$subtitle_baru',  komponen_harga,  komponen_harga_awal,  komponen_name,  satuan,  pajak,  '$unit_id_baru',  from_sub_kegiatan,  sub,  kode_sub,  last_update_user,  last_update_time,  last_update_ip,  tahap,  tahap_edit,  tahap_new,  status_lelang,  nomor_lelang,  koefisien_semula,  volume_semula,  harga_semula,  total_semula,  lock_subtitle,  status_hapus,  tahun,  kode_lokasi,  kecamatan,  rekening_code_asli,  nilai_anggaran,  note_skpd,  note_peneliti,  asal_kegiatan,  is_blud,  ob,  ob_from_id,  is_per_komponen,  kegiatan_code_asal,  th_ke_multiyears,  lokasi_kecamatan,  lokasi_kelurahan,  harga_sebelum_sisa_lelang,  is_musrenbang,  sub_id_asal,  subtitle_asal,  kode_sub_asal,  sub_asal,  last_edit_time,  is_potong_bpjs,  is_iuran_bpjs,  is_kapitasi_bpjs,  is_iuran_jkn,  is_iuran_jkk,  status_ob,  ob_parent,  ob_alokasi_baru,  is_hibah,  status_level,  status_sisipan,  status_level_tolak,  is_tapd_setuju,  is_bappeko_setuju,  akrual_code,  tipe2,  is_penyelia_setuju,  note_tapd,  note_bappeko "
                        . " from ebudget.rkua2_rincian_detail "
                        . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                //update gis
                $kode_rka = $unit_id_lama . '.' . $kegiatan_code_lama . '.' . $det_no_lama;
                $kode_rka_baru = $unit_id_baru . '.' . $kegiatan_code_baru . '.' . $det_no;
                $query = "update history_pekerjaan_v2
                        set kode_rka='$kode_rka_baru'
                        where kode_rka='$kode_rka' and tahun='2017'";
                $stmtgis = $congis->prepareStatement($query);
                $stmtgis->executeQuery();

                //update gmap
                $query = "update gis_ebudget.geojsonlokasi_rev1
                        set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', detail_no='$det_no_lama'
                        where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                //update waitinglist
                if ($unit_id_lama == '2600') {
                    $query = "update ebudget.waitinglist_pu 
                        set kode_rka='$kode_rka_baru'
                        where kode_rka='$kode_rka'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }

                $det_no++;
            }

            //upadte waitinglist_pu
            if ($unit_id_lama == '2600') {
                $query = "update ebudget.waitinglist_pu 
                        set unit_id='XXX$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru'
                        where unit_id='XXX$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and subtitle='$subtitle_lama'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();

                $query = "update gis_ebudget.geojsonlokasi_waitinglist a
                        set kegiatan_code=b.kegiatan_code
                        from ebudget.waitinglist_pu b
                        where a.id_waiting=b.id_waiting and a.kegiatan_code<>b.kegiatan_code and a.tahun='2017'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }

            $query = " update ebudget.dinas_subtitle_indikator s1
                set prioritas=s.prioritas
                from ebudget.rkua2_subtitle_indikator s, ebudget.konversi_subtitle2 k
                where s.subtitle=k.subtitle_lama and s.unit_id=k.unit_id_lama and s.kegiatan_code=k.kegiatan_code_lama and
                s1.subtitle=k.subtitle_baru and s1.unit_id=k.unit_id_baru and s1.kegiatan_code=k.kegiatan_code_baru and s.prioritas<>s1.prioritas";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
        }
        $con->commit();
        $this->setFlash('berhasil', 'Sudah terkonversi');
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeKonversiSubtitle2() {
        $con = Propel::getConnection();
        $con->begin();
        $congis = Propel::getConnection('gis');
//        $query = "select max(sub_id) as id from ebudget.dinas_subtitle_indikator";
//        $stmt2 = $con->prepareStatement($query);
//        $rs2 = $stmt2->executeQuery();
//        $rs2->next();
//        $sub_id_baru = $rs2->getString('id');
        //try {
        $c = new Criteria();
        $c->add(KonversiSubtitlePeer::MASUK, TRUE, Criteria::NOT_EQUAL);
        $konversi = KonversiSubtitlePeer::doSelect($c);
        foreach ($konversi as $value) {
            $unit_id_lama = $value->getUnitIdLama();
            $kegiatan_code_lama = $value->getKegiatanCodeLama();
            $subtitle_lama = $value->getSubtitleLama();
            $unit_id_baru = $value->getUnitIdBaru();
            $kegiatan_code_baru = $value->getKegiatanCodeBaru();
            $subtitle_baru = $value->getSubtitleBaru();

            //cek dahulu
//            $c = new Criteria();
//            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
//            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
//            $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle_lama);
//            if (!DinasRincianDetailPeer::doSelectOne($c)) {
//                $con->rollback();
//                $this->setFlash('gagal', 'Ada yang tidak ketemu' . $unit_id_lama . ' - ' . $kegiatan_code_lama . ' - ' . $subtitle_lama);
//                return $this->redirect('admin/fiturTambahan');
//            }
            $query = " update ebudget.konversi_subtitle set masuk=true "
                    . " where unit_id_lama='$unit_id_lama' and kegiatan_code_lama='$kegiatan_code_lama' and subtitle_lama='$subtitle_lama' ";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
//            $value->setMasuk(TRUE);
//            $value->save();

            $query = "select * from ebudget.baru
                        where unit_id='$unit_id_baru' and kegiatan_code='$kegiatan_code_baru' and subtitle='$subtitle_lama'";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                //buat dan update kegiatan
                if ($temp_keg != $rs->getString('kegiatan_code')) {
                    $c = new Criteria();
                    $c->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_baru);
                    $c->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code_baru);
                    if ($kegiatan = DinasMasterKegiatanPeer::doSelectOne($c)) {
                        $kegiatan->setNamaKegiatan($rs->getString('nama_kegiatan'));
                        $kegiatan->setOutput($rs->getString('output'));
                        $kegiatan->setAlokasiDana($rs->getString('nilai_kegiatan'));
                        $kegiatan->setTambahanPagu(0);
                    } else {
                        $kode_urusan = substr($rs->getString('kegiatan_code'), 0, 8);
                        $kode_program2 = substr($rs->getString('kegiatan_code'), 0, 11);
                        $kegiatan = new DinasMasterKegiatan();
                        $kegiatan->setUnitId($rs->getString('unit_id'));
                        $kegiatan->setKodeKegiatan($rs->getString('kegiatan_code'));
                        $kegiatan->setNamaKegiatan($rs->getString('nama_kegiatan'));
                        $kegiatan->setOutput($rs->getString('output'));
                        $kegiatan->setAlokasiDana($rs->getString('nilai_kegiatan'));
                        $kegiatan->setKodeUrusan($kode_urusan);
                        $kegiatan->setKodeProgram2($kode_program2);
                        $kegiatan->setTahun('2017');
                        $kegiatan->setTahap('murni');
                        $kegiatan->setTambahanPagu(0);
                        $rincian = new DinasRincian();
                        $rincian->setUnitId($rs->getString('unit_id'));
                        $rincian->setKegiatanCode($rs->getString('kegiatan_code'));
                        $rincian->setTipe('murni');
                        $rincian->setTahun('2017');
                        $rincian->save();
                    }
                    $kegiatan->save();
                }
                $temp_keg = $rs->getString('kegiatan_code');

                //update nilai subtitle
                $arrPecah = explode('|', $rs->getString('output_subtitle'));
                $c = new Criteria();
                $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id_baru);
                $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code_baru);
                $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle_baru);
                if ($sub = DinasSubtitleIndikatorPeer::doSelectOne($c)) {
                    $sub->setIndikator($arrPecah[0]);
                    $sub->setNilai($arrPecah[1]);
                    $sub->setSatuan($arrPecah[2]);
                    $sub->setAlokasiDana($rs->getString('nilai_subtitle'));
                } else {
                    //$sub_id_baru++;
                    $sub = new DinasSubtitleIndikator();
                    //$sub->setSubId($sub_id_baru);
                    $sub->setUnitId($rs->getString('unit_id'));
                    $sub->setKegiatanCode($rs->getString('kegiatan_code'));
                    $sub->setSubtitle($rs->getString('subtitle'));
                    $sub->setIndikator($arrPecah[0]);
                    $sub->setNilai($arrPecah[1]);
                    $sub->setSatuan($arrPecah[2]);
                    $sub->setAlokasiDana($rs->getString('nilai_subtitle'));
                }
                $sub->save();
            }
            //cek kalau nabrak
            $c = new Criteria();
            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_baru);
            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_baru);
            if (DinasRincianDetailPeer::doSelectOne($c)) {
                //buat detail_no baru
                $sesuatu = new DinasRincianDetail;
                $det_no = $sesuatu->getMaxDetailNo($unit_id_baru, $kegiatan_code_baru);
                //update 1 per 1
                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
                $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle_lama);
                $rd_lama = DinasRincianDetailPeer::doSelect($c);
                foreach ($rd_lama as $value2) {
                    $det_no_lama = $value2->getDetailNo();
                    $query = " update ebudget.dinas_rincian_detail "
                            . " set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', detail_no='$det_no', subtitle='$subtitle_baru' "
                            . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    //update rd
//                    $value2->setUnitId($unit_id_baru);
//                    $value2->setKegiatanCode($kegiatan_code_baru);
//                    $value2->setDetailNo($det_no);
//                    $value2->setSubtitle($subtitle_baru);
//                    $value2->save();
                    $det_no++;

                    //update gis
                    $kode_rka = $unit_id_lama . '.' . $kegiatan_code_lama . '.' . $det_no_lama;
                    $kode_rka_baru = $unit_id_baru . '.' . $kegiatan_code_baru . '.' . $det_no;
                    $query = "update history_pekerjaan_v2
                        set kode_rka='$kode_rka_baru'
                        where kode_rka='$kode_rka' and tahun='2017'";
                    $stmtgis = $congis->prepareStatement($query);
                    $stmtgis->executeQuery();
                }
            } else {
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
                $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle_lama);
                $rd_lama = DinasRincianDetailPeer::doSelect($c);
                foreach ($rd_lama as $value2) {
                    $det_no_lama = $value2->getDetailNo();
                    $query = " update ebudget.dinas_rincian_detail "
                            . " set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru' "
                            . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    //update rd
//                    $value2->setUnitId($unit_id_baru);
//                    $value2->setKegiatanCode($kegiatan_code_baru);
//                    $value2->setSubtitle($subtitle_baru);
//                    $value2->save();
                    //update gis
                    $kode_rka = $unit_id_lama . '.' . $kegiatan_code_lama . '.' . $det_no_lama;
                    $kode_rka_baru = $unit_id_baru . '.' . $kegiatan_code_baru . '.' . $det_no;
                    $query = "update history_pekerjaan_v2
                        set kode_rka='$kode_rka_baru'
                        where kode_rka='$kode_rka' and tahun='2017'";
                    $stmtgis = $congis->prepareStatement($query);
                    $stmtgis->executeQuery();
                }
            }

            //upadte waitinglist_pu
            if ($unit_id_lama == '2600') {
                $query = "update ebudget.waitinglist_pu 
                        set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru'
                        where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and subtitle='$subtitle_lama'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }

            //update dinas_subtitle_indikator
            $c = new Criteria();
            $c->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id_baru);
            $c->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code_baru);
            $c->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle_lama);
            if (!DinasSubtitleIndikatorPeer::doSelectOne($c)) {
                $query = "update ebudget.dinas_subtitle_indikator 
                        set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', subtitle='$subtitle_baru'
                        where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and subtitle='$subtitle_lama'";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }
        }
        $con->commit();
        $this->setFlash('berhasil', 'Sudah terkonversi');
        return $this->redirect('admin/fiturTambahan');
//        } catch (Exception $ex) {
//            $con->rollback();
//            $this->setFlash('gagal', 'Terjadi kesalahan');
//            return $this->redirect('admin/fiturTambahan');
//        }
    }

    public function executeGantiKegiatanSubtitle() {
        $con = Propel::getConnection();
        $con->begin();

        $unit_id_lama = '2700';
        $kegiatan_code_lama = '1.1.2.10.01.0009';
        $subtitle = 'Pengadaan Koneksi Internet dan Bandwidth';
        $kegiatan_code_baru = '1.1.2.10.01.0008';

        //buat detail_no baru
        $sesuatu = new DinasRincianDetail;
        $det_no = $sesuatu->getMaxDetailNo($unit_id_lama, $kegiatan_code_baru);
        //update 1 per 1
        $c = new Criteria();
        $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
        $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
        $c->add(DinasRincianDetailPeer::SUBTITLE, $subtitle);
        $rd_lama = DinasRincianDetailPeer::doSelect($c);
        foreach ($rd_lama as $value2) {
            $det_no_lama = $value2->getDetailNo();
            $query = " update ebudget.dinas_rincian_detail "
                    . " set kegiatan_code='$kegiatan_code_baru', detail_no='$det_no' "
                    . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$det_no_lama' ";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $det_no++;
        }

        //update dinas_subtitle_indikator
//        $query = "update ebudget.dinas_subtitle_indikator 
//                        set kegiatan_code='$kegiatan_code_baru'
//                        where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and subtitle='$subtitle'";
//        $stmt = $con->prepareStatement($query);
//        $stmt->executeQuery();

        $con->commit();
        $this->setFlash('berhasil', 'Sudah terkonversi');
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadPerkap() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            $unit_id_baru = '0309';
            $kegiatan_code_baru = '2.2.2.02.02.0022';
            $subtitle_baru = 'Pengadaan Sarana Perkantoran';
            //buat detail_no baru
            $sesuatu = new DinasRincianDetail;
            $det_no = $sesuatu->getMaxDetailNo($unit_id_baru, $kegiatan_code_baru);

            for ($row = 2; $row <= $highR; $row++) {
                $unit_id_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kegiatan_code_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $detail_no_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));

                $c = new Criteria();
                $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id_lama);
                $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code_lama);
                $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no_lama);
                if ($rd_lama = DinasRincianDetailPeer::doSelectOne($c)) {
//                    $rd_lama->setUnitId($unit_id_baru);
//                    $rd_lama->setKegiatanCode($kegiatan_code_baru);
//                    $rd_lama->setSubtitle($subtitle_baru);
//                    $rd_lama->setDetailNo($det_no);
//                    $rd_lama->save();
                    $query = " update ebudget.dinas_rincian_detail "
                            . " set unit_id='$unit_id_baru', kegiatan_code='$kegiatan_code_baru', detail_no='$det_no', subtitle='$subtitle_baru' "
                            . " where unit_id='$unit_id_lama' and kegiatan_code='$kegiatan_code_lama' and detail_no='$detail_no_lama' ";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                } else {
                    unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                    //echo $ex; exit;
                    $con->rollback();
                    $this->setFlash('gagal', 'Tidak ditemukan');
                }
                $det_no++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Data telah dipindah ke perlengkapan');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUbahInflasi() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $inflasi = $this->getRequestParameter('inflasi');
            if ($inflasi == 0) {
                $query = "update " . sfConfig::get('app_default_schema') . ".shsd set shsd_harga=shsd_harga_dasar";
            } else {
                $faktor_inflasi = 1 + ($inflasi / 100.0);
                $query = "update " . sfConfig::get('app_default_schema') . ".shsd set shsd_harga=shsd_harga_dasar*$faktor_inflasi where is_inflasi=true";
            }
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

//            $query = "update " . sfConfig::get('app_default_schema') . ".asb_fisik set komponen_harga=komponen_harga_dasar*$faktor_inflasi where is_inflasi=true";
//            $stmt = $con->prepareStatement($query);
//            $stmt->executeQuery();
//            
//            $query = "update " . sfConfig::get('app_default_schema') . ".komponen set komponen_harga=komponen_harga_dasar*$faktor_inflasi where is_inflasi=true";
//            $stmt = $con->prepareStatement($query);
//            $stmt->executeQuery();

            $con->commit();
            $this->setFlash('berhasil', 'Harga komponen telah berubah');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'GAGAL karena ' . $ex);
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadKodeKegiatanPak() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));

                $arr_error = array();
                if ($kode_kegiatan != '') {
                    $kode_lama = '';
                    $query = "select kode_kegiatan from ebudget.master_kegiatan
                        where unit_id='$unit_id' and nama_kegiatan ilike '$nama'";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $kode_lama = $rs->getString('kode_kegiatan');
                    }

                    if ($kode_lama <> '') {
                        $query = "insert into konversi_kegiatan_2016
                        values('$unit_id','$kode_lama', '$kode_kegiatan', '$nama')";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();
                    } else {
                        $arr_error[] = "- tidak dapat $unit_id, $nama -";
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($arr_error) {
                $temp = '';
                foreach ($arr_error as $error) {
                    $temp .= $error;
                }
                $con->rollback();
                $this->setFlash('gagal', $temp);
            } else {
                $con->commit();
                $this->setFlash('berhasil', 'Kode kegiatan baru telah terupload');
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadPaguPak() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'pagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 1; $row <= $highR; $row++) {
                $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $pagu = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));

                if ($kode_kegiatan != '') {
                    $c_cek = new Criteria();
                    $c_cek->add(PaguPakPeer::UNIT_ID, $unit_id);
                    $c_cek->add(PaguPakPeer::KEGIATAN_CODE, $kode_kegiatan);
                    if ($rs_pagu = PaguPakPeer::doSelectOne($c_cek)) {
                        $rs_pagu->setPagu($pagu);
                        $rs_pagu->save();
                    } else {
                        $pagu_baru = new PaguPak();
                        $pagu_baru->setUnitId($unit_id);
                        $pagu_baru->setKegiatanCode($kode_kegiatan);
                        $pagu_baru->setPagu($pagu);
                        $pagu_baru->save();
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Pagu kegiatan PAK telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadMisiKegiatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'misi_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $kode_misi = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $nama_misi = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));

                $arr_error = array();
                if ($kode_misi != '') {
                    if ($kode_misi < 10) {
                        $kode_misi_baru = '0' . $kode_misi;
                    } else {
                        $kode_misi_baru = $kode_misi;
                    }
                    $c_cek_misi = new Criteria();
                    $c_cek_misi->add(MasterMisiPeer::KODE_MISI, $kode_misi_baru);
                    if (!$dapat_misi = MasterMisiPeer::doSelectOne($c_cek_misi)) {
                        $misi = new MasterMisi();
                        $misi->setKodeMisi($kode_misi_baru);
                        $misi->setNamaMisi($nama_misi);
                        $misi->save();
                    }

                    $c_cek_kegiatan = new Criteria();
                    $c_cek_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $c_cek_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    if ($dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                        $dapat_kegiatan->setKodeMisi($kode_misi_baru);
                        $dapat_kegiatan->save();

                        $c_cek_kegiatan = new Criteria();
                        $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                        $c_cek_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan);
                        $dapat_kegiatan->setKodeMisi($kode_misi_baru);
                        $dapat_kegiatan->save();
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Misi kegiatan telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeBuatUangKinerja() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query = "select unit_id, kode_keg, poin, nilai from ebudget.uk";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $unit_id = trim($rs->getString('unit_id'));
                $kode_kegiatan = trim($rs->getString('kode_keg'));
                $volume = $rs->getString('poin');

                $query = "select sub_id from ebudget.dinas_subtitle_indikator
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='Penunjang Kinerja'";
                //echo $query; exit;
                $stmt2 = $con->prepareStatement($query);
                $rs2 = $stmt2->executeQuery();
                if ($rs2->next()) {
                    $func = new DinasRincianDetail();
                    $detail_no = $func->getMaxDetailNo($unit_id, $kode_kegiatan);
                    $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                        komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                        lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,status_sisipan,akrual_code,tipe2)
                        values
                        ('" . $kode_kegiatan . "', 'SSH', " . $detail_no . ", '5.2.1.04.01', '23.01.01.04.16', '', " . $volume . ", '" . $volume . " poin', 'Penunjang Kinerja',
                        4000 , 4000 ,'Uang Kinerja PNS', 'Poin', 0,'" . $unit_id . "','', '',
                        '', '', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','false','false','false','','','untuk penyesuaian kebutuhan skpd','false','false','murni','false','false', 'false', '', '')";
                    echo $query3;
                    $con->rollback();
                    exit;
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();
                }
//                else {
//                    echo $query;
//                    exit;
//                    $con->rollback();
//                    $this->setFlash('gagal', 'Gagal karena gak wenaaaaaaaak');
//                    return $this->redirect('admin/fiturTambahan');
//                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'WENAKNOOOOO');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeBuatUangKinerja2() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query = "select unit_id, kode_keg, poin, nilai from ebudget.uk2";
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $unit_id = trim($rs->getString('unit_id'));
                $kode_kegiatan = trim($rs->getString('kode_keg'));
                $volume = $rs->getString('poin');

                $query = "select sub_id from ebudget.dinas_subtitle_indikator
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and subtitle='Penunjang Kinerja'";
                //echo $query; exit;
                $stmt2 = $con->prepareStatement($query);
                $rs2 = $stmt2->executeQuery();
                if ($rs2->next()) {
                    $func = new DinasRincianDetail();
                    $detail_no = $func->getMaxDetailNo($unit_id, $kode_kegiatan);
                    $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                        komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                        lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,status_sisipan,akrual_code,tipe2)
                        values
                        ('" . $kode_kegiatan . "', 'SSH', " . $detail_no . ", '5.2.1.04.01', '23.01.01.04.16', '', " . $volume . ", '" . $volume . " poin', 'Penunjang Kinerja',
                        4000 , 4000 ,'Uang Kinerja PNS', 'Poin', 0,'" . $unit_id . "','', '',
                        '', '', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','false','false','false','','','untuk penyesuaian kebutuhan skpd','false','false','murni','false','false', 'false', '', '')";
                    echo $query3;
                    $con->rollback();
                    exit;
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();
                } else {
                    echo $query;
                    exit;
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal karena gak wenaaaaaaaak');
                    return $this->redirect('admin/fiturTambahan');
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'WENAKNOOOOO');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadPaguKegiatanMurni() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'kegiatan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = str_replace('#', '', str_replace('#', '', utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()))));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $nilai_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));

                if ($kode_kegiatan != '' && $unit_id != '') {
                    //echo $kode_kegiatan. ' - '. $unit_id. ' - '.$nilai_kegiatan;
                    $c_cek_kegiatan = new Criteria();
                    $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $c_cek_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                    if ($dapat_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                        $dapat_kegiatan->setAlokasiDana($nilai_kegiatan);
                        $dapat_kegiatan->save();
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kegiatan telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadUkKegiatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'kegiatan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = str_replace('#', '', str_replace('#', '', utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()))));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $poin = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $nilai = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));

                if ($kode_kegiatan != '' && $unit_id != '') {
                    $query3 = "INSERT INTO ebudget.uk2 VALUES('$unit_id', '$kode_kegiatan', $poin, $nilai)";
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kegiatan telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKegiatanMurni2() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'kegiatan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                //$skpd = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $kode_urusan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $urusan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $kode_program2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $program = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $nilai_skpd = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $nama_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                $output_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                $target_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
                $satuan_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                $nilai_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
                $id_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
                $subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));
                $output_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
                $target_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()));
                $satuan_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()));
                $nilai_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(19, $row)->getValue()));

                $arr_error = array();
                if ($kode_urusan != '') {
//                    echo $kode_urusan . ' ' . $urusan . ' ' . $kode_program2 . ' ' . $program . ' ' . $unit_id . ' ' . $nilai_skpd . ' ' . $kode_kegiatan . ' ' . $nama_kegiatan . ' ' . $output_kegiatan . ' ' . $target_kegiatan . ' ' . $satuan_kegiatan . ' ' . $nilai_kegiatan . ' ' . $id_sub . ' ' . $subtitle . ' ' . $output_sub . ' ' . $target_sub . ' ' . $satuan_sub . ' ' . $nilai_sub . '<br>';
//                    $c_cek_urusan = new Criteria();
//                    $c_cek_urusan->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan);
//                    if (!MasterUrusanPeer::doSelectOne($c_cek_urusan)) {
//                        $urusan_new = new MasterUrusan();
//                        $urusan_new->setKodeUrusan($kode_urusan);
//                        $urusan_new->setNamaUrusan($urusan);
//                        $urusan_new->save();
//                    }
//                    $c_cek_program = new Criteria();
//                    $c_cek_program->add(MasterProgram2Peer::KODE_PROGRAM2, $kode_program2);
//                    if (!MasterProgram2Peer::doSelectOne($c_cek_program)) {
//                        $program_new = new MasterProgram2();
//                        $program_new->setKodeProgram('99');
//                        $program_new->setKodeProgram2($kode_program2);
//                        $program_new->setNamaProgram2($program);
//                        $program_new->save();
//                    }
                    //echo "$tahun | ($unit_id)$skpd | $kode_detail_kegiatan | $komponen | $detail_name | $volume | $satuan | $realisasi<br>";
//                    $c_cek_skpd = new Criteria();
//                    $c_cek_skpd->add(UnitKerjaPeer::UNIT_ID, $unit_id);
//                    if ($dapat_skpd = UnitKerjaPeer::doSelectOne($c_cek_skpd)) {
//                        $query = "update unit_kerja set pagu=$nilai_skpd ,pagu_murni=$nilai_skpd where unit_id='$unit_id'";
//                        $stmt = $con->prepareStatement($query);
//                        $stmt->executeQuery();

                    $c_cek_kegiatan = new Criteria();
                    $c_cek_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $c_cek_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    if (!$dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                        $output = $output_kegiatan . '|' . $target_kegiatan . '|' . $satuan_kegiatan;
                        $kegiatan = new MasterKegiatan();
                        $kegiatan->setUnitId($unit_id);
                        $kegiatan->setKodeKegiatan($kode_kegiatan);
                        $kegiatan->setKodeUrusan($kode_urusan);
                        $kegiatan->setKodeProgram2($kode_program2);
                        $kegiatan->setNamaKegiatan($nama_kegiatan);
                        $kegiatan->setOutput($output);
                        $kegiatan->setAlokasiDana($nilai_kegiatan);
                        $kegiatan->setTahap('murni');
                        $kegiatan->setTipe('murni');
                        $kegiatan->setTahun(2017);
                        $kegiatan->save();

                        $rincian = new Rincian();
                        $rincian->setUnitId($unit_id);
                        $rincian->setKegiatanCode($kode_kegiatan);
                        $rincian->setTipe('murni');
                        $rincian->setTahun(2017);
                        $rincian->save();
                    }

                    $c_cek_subtitle = new Criteria();
                    $c_cek_subtitle->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_cek_subtitle->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c_cek_subtitle->add(SubtitleIndikatorPeer::SUBTITLE, $subtitle, Criteria::ILIKE);
                    if (!$dapat_subtitle = SubtitleIndikatorPeer::doSelectOne($c_cek_subtitle)) {
                        $new_subtitle = new SubtitleIndikator();
                        $new_subtitle->setSubId($id_sub);
                        $new_subtitle->setUnitId($unit_id);
                        $new_subtitle->setKegiatanCode($kode_kegiatan);
                        $new_subtitle->setSubtitle($subtitle);
                        $new_subtitle->setIndikator($output_sub);
                        $new_subtitle->setNilai($target_sub);
                        $new_subtitle->setSatuan($satuan_sub);
                        $new_subtitle->setAlokasiDana($nilai_sub);
                        $new_subtitle->setTahun(2017);
                        $new_subtitle->save();
                    }
//                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kegiatan telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKegiatanMurni() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'kegiatan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 2; $row <= $highR; $row++) {
                    //$skpd = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $kode_urusan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $urusan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $kode_program2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $program = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $nilai_skpd = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                    $nama_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    $output_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                    $target_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
                    $satuan_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                    $nilai_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
                    $id_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
                    $subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));
                    $output_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
                    $target_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()));
                    $satuan_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()));
                    $nilai_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(19, $row)->getValue()));

                    $arr_error = array();

                    if ($id_sub != '') {
                        echo $kode_urusan . ' ' . $urusan . ' ' . $kode_program2 . ' ' . $program . ' ' . $unit_id . ' ' . $nilai_skpd . ' ' . $kode_kegiatan . ' ' . $nama_kegiatan . ' ' . $output_kegiatan . ' ' . $target_kegiatan . ' ' . $satuan_kegiatan . ' ' . $nilai_kegiatan . ' ' . $id_sub . ' ' . $subtitle . ' ' . $output_sub . ' ' . $target_sub . ' ' . $satuan_sub . ' ' . $nilai_sub . '<br>';
                        $c_cek_urusan = new Criteria();
                        $c_cek_urusan->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan);
                        if (!MasterUrusanPeer::doSelectOne($c_cek_urusan)) {
                            $urusan_new = new MasterUrusan();
                            $urusan_new->setKodeUrusan($kode_urusan);
                            $urusan_new->setNamaUrusan($urusan);
                            $urusan_new->save();
                        }
                        $c_cek_program = new Criteria();
                        $c_cek_program->add(MasterProgram2Peer::KODE_PROGRAM2, $kode_program2);
                        if (!MasterProgram2Peer::doSelectOne($c_cek_program)) {
                            $program_new = new MasterProgram2();
                            $program_new->setKodeProgram('99');
                            $program_new->setKodeProgram2($kode_program2);
                            $program_new->setNamaProgram2($program);
                            $program_new->save();
                        }

                        //echo "$tahun | ($unit_id)$skpd | $kode_detail_kegiatan | $komponen | $detail_name | $volume | $satuan | $realisasi<br>";
                        $c_cek_skpd = new Criteria();
                        $c_cek_skpd->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                        if ($dapat_skpd = UnitKerjaPeer::doSelectOne($c_cek_skpd)) {
                            $query = "update unit_kerja set pagu=$nilai_skpd, pagu_murni=$nilai_skpd where unit_id='$unit_id'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();

                            $c_cek_kegiatan = new Criteria();
                            $c_cek_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                            $c_cek_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                            if (!$dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                                $output = $output_kegiatan . '|' . $target_kegiatan . '|' . $satuan_kegiatan;
                                $kegiatan = new MasterKegiatan();
                                $kegiatan->setUnitId($unit_id);
                                $kegiatan->setKodeKegiatan($kode_kegiatan);
                                $kegiatan->setKodeUrusan($kode_urusan);
                                $kegiatan->setKodeProgram2($kode_program2);
                                $kegiatan->setNamaKegiatan($nama_kegiatan);
                                $kegiatan->setOutput($output);
                                $kegiatan->setAlokasiDana($nilai_kegiatan);
                                $kegiatan->setTahap('murni');
                                $kegiatan->setTipe('murni');
                                $kegiatan->setTahun(2017);
                                $kegiatan->save();

                                $rincian = new Rincian();
                                $rincian->setUnitId($unit_id);
                                $rincian->setKegiatanCode($kode_kegiatan);
                                $rincian->setTipe('murni');
                                $rincian->setTahun(2017);
                                $rincian->save();
                            }

                            $c_cek_subtitle = new Criteria();
                            $c_cek_subtitle->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_cek_subtitle->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                            $c_cek_subtitle->add(SubtitleIndikatorPeer::SUBTITLE, $subtitle, Criteria::ILIKE);
                            if (!$dapat_subtitle = SubtitleIndikatorPeer::doSelectOne($c_cek_subtitle)) {
                                $new_subtitle = new SubtitleIndikator();
                                $new_subtitle->setSubId($id_sub);
                                $new_subtitle->setUnitId($unit_id);
                                $new_subtitle->setKegiatanCode($kode_kegiatan);
                                $new_subtitle->setSubtitle($subtitle);
                                $new_subtitle->setIndikator($output_sub);
                                $new_subtitle->setNilai($target_sub);
                                $new_subtitle->setSatuan($satuan_sub);
                                $new_subtitle->setAlokasiDana($nilai_sub);
                                $new_subtitle->setTahun(2017);
                                $new_subtitle->save();
                            }

                            $c_cek_subtitle = new Criteria();
                            $c_cek_subtitle->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_cek_subtitle->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                            $c_cek_subtitle->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitle, Criteria::ILIKE);
                            if (!$dapat_subtitle = DinasSubtitleIndikatorPeer::doSelectOne($c_cek_subtitle)) {
                                $new_subtitle = new DinasSubtitleIndikator();
                                $new_subtitle->setSubId($id_sub);
                                $new_subtitle->setUnitId($unit_id);
                                $new_subtitle->setKegiatanCode($kode_kegiatan);
                                $new_subtitle->setSubtitle($subtitle);
                                $new_subtitle->setIndikator($output_sub);
                                $new_subtitle->setNilai($target_sub);
                                $new_subtitle->setSatuan($satuan_sub);
                                $new_subtitle->setAlokasiDana($nilai_sub);
                                $new_subtitle->setTahun(2017);
                                $new_subtitle->save();
                            }
                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            exit;
            $con->commit();
            $this->setFlash('berhasil', 'Kegiatan telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadWaiting() {
        $sekarang = date('Y-m-d H:i:s');
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'waiting_khusus_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 1; $row <= $highR; $row++) {
                $id_waiting = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $data_subtitle = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $data_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));

                if ($id_waiting != '') {
                    $arr = explode('<=>', $data_subtitle);
                    $kode_kegiatan = $arr[0];
                    $subtitle = $arr[1];
                    $arr2 = explode('<=>', $data_komponen);
                    $komponen_id = $arr2[0];
                    $komponen_name = $arr2[1];
//                    echo $id_waiting . ' - ' . $kode_kegiatan . ' - ' . $subtitle . ' - ' . $komponen_id . ' - ' . $komponen_name . '<br>';
                    $c_cek_waiting = new Criteria();
                    $c_cek_waiting->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                    if (WaitingListPUPeer::doSelectOne($c_cek_waiting)) {
                        $waiting = WaitingListPUPeer::doSelectOne($c_cek_waiting);
//                        echo $waiting->getIdWaiting() . ' ' . $waiting->getKegiatanCode() . ' ' . $waiting->getSubtitle() . ' ' . $waiting->getKomponenId() . ' ' . $waiting->getKomponenName() . ' ' . $waiting->getUpdatedAt() . ' ' . $waiting->getStatusHapus() . '<br>';
                        $waiting->setSubtitle($subtitle);
                        $waiting->setKomponenId($komponen_id);
                        $waiting->setKomponenName($komponen_name);
                        $waiting->setStatusHapus(false);
                        $waiting->setUpdatedAt($sekarang);
                        $waiting->save();
                    }
                }
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
//            exit;
            $con->commit();
            $this->setFlash('berhasil', 'Waitinglist telah terupload');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadSshMurni() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'sshmurni_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 6; $row <= $highR; $row++) {
                    $kode = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $merk = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $spec = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    $hidden = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                    $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                    $status_masuk = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
                    $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
                    $is_survey_bp = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(15, $row)->getValue()));

                    if ($satuan != '') {
                        //echo "$kode | $nama | $merk | $spec | $hidden | $satuan | $harga | $pajak<br>";
                        $c_cek_shsd = new Criteria();
                        $c_cek_shsd->add(ShsdPeer::SHSD_ID, $kode);
                        if (ShsdPeer::doSelectOne($c_cek_shsd)) {
                            $ada_error[] = "Komponen $kode - $nama sudah ada di tabel SHSD";
                        } else {
                            if ($pajak == "10") {
                                $pajak_new = FALSE;
                            } else {
                                $pajak_new = TRUE;
                            }
                            if ($is_survey_bp == "survei") {
                                $is_survey_bp_new = TRUE;
                            } else {
                                $is_survey_bp_new = FALSE;
                            }
                            $shsd = new Shsd();
                            $shsd->setShsdId($kode);
                            $shsd->setShsdName($nama . ' ' . $spec);
                            $shsd->setSatuan($satuan);
                            $shsd->setShsdHarga($harga);
                            $shsd->setRekeningCode($rekening);
                            $shsd->setShsdMerk($merk);
                            $shsd->setNamaDasar($nama);
                            $shsd->setSpec($spec);
                            $shsd->setHiddenSpec($hidden);
                            $shsd->setShsdLocked(TRUE);
                            $shsd->setNonPajak($pajak_new);
                            $shsd->setIsSurveyBp($is_survey_bp_new);
                            $shsd->save();

                            $c_cek_komponen = new Criteria();
                            $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $kode);
                            if (KomponenPeer::doSelectOne($c_cek_komponen)) {
                                $ada_error[] = "Komponen $kode - $nama sudah ada di tabel Komponen";
                            } else {
                                $komponen = new Komponen();
                                $komponen->setKomponenId($kode);
                                $komponen->setKomponenName($nama . ' ' . $spec);
                                $komponen->setSatuan($satuan);
                                $komponen->setKomponenHarga($harga);
                                $komponen->setRekening($rekening);
                                $komponen->setShsdId($kode);
                                $komponen->setKomponenTipe('SHSD');
                                $komponen->setKomponenNonPajak($pajak_new);
                                $komponen->setStatusMasuk($status_masuk);
                                $komponen->setIsSurveyBp($is_survey_bp_new);
                                $komponen->save();
                            }

                            $arr_rekening = explode('/', $rekening);
                            foreach ($arr_rekening as $item) {
                                $c_cek_komponen_rekening = new Criteria();
                                $c_cek_komponen_rekening->add(KomponenRekeningPeer::KOMPONEN_ID, $kode);
                                $c_cek_komponen_rekening->add(KomponenRekeningPeer::REKENING_CODE, $item);
                                if (KomponenRekeningPeer::doSelectOne($c_cek_komponen_rekening)) {
                                    $ada_error[] = "Komponen $kode - $nama sudah ada di tabel Komponen Rekening";
                                } else {
                                    $komponen_rekening = new KomponenRekening();
                                    $komponen_rekening->setKomponenId($kode);
                                    $komponen_rekening->setRekeningCode($item);
                                    $komponen_rekening->save();
                                }
                            }
                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'SSH telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadHistoryPekerjaan() {
        $con = Propel::getConnection('gis');
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'akrual_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                $tahun = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, 1)->getValue()));
                for ($row = 4; $row <= $highR; $row++) {
                    //$skpd = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $volume = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $realisasi = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $kode_detail_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    $detail_name = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));

                    if ($komponen != '') {
                        //echo "$tahun | ($unit_id)$skpd | $kode_detail_kegiatan | $komponen | $detail_name | $volume | $satuan | $realisasi<br>";
                        $c_cek_history = new Criteria();
                        $c_cek_history->add(HistoryPekerjaan3Peer::KODE_DETAIL_KEGIATAN, $kode_detail_kegiatan);
                        $c_cek_history->add(HistoryPekerjaan3Peer::UNIT_ID, $unit_id);
                        $c_cek_history->add(HistoryPekerjaan3Peer::TAHUN, $tahun);
                        if ($dapat_komponen = HistoryPekerjaan3Peer::doSelectOne($c_cek_history)) {
//                            $dapat_komponen = HistoryPekerjaan3Peer::doSelectOne($c_cek_history);
//                            $dapat_komponen->setKomponenName($komponen);
//                            $dapat_komponen->setVolume($volume);
//                            $dapat_komponen->setSatuan($satuan);
//                            $dapat_komponen->setRealisasi($realisasi);
//                            $dapat_komponen->setDetailName($detail_name);
//                            $dapat_komponen->save();
                            $query = "update history_pekerjaan3 set komponen_name='$komponen',volume='$volume',satuan='$satuan',realisasi='$realisasi' where kode_detail_kegiatan='$kode_detail_kegiatan' and unit_id='$unit_id' and tahun='$tahun'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();
                        } else {
                            $dapat_komponen = new HistoryPekerjaan3();
                            $dapat_komponen->setUnitId($unit_id);
                            $dapat_komponen->setKomponenName($komponen);
                            $dapat_komponen->setVolume($volume);
                            $dapat_komponen->setSatuan($satuan);
                            $dapat_komponen->setRealisasi($realisasi);
                            $dapat_komponen->setKodeDetailKegiatan($kode_detail_kegiatan);
                            $dapat_komponen->setDetailName($detail_name);
                            $dapat_komponen->setTahun($tahun);
                            $dapat_komponen->save();
                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            $con->commit();
            $this->setFlash('berhasil', 'History perkejaan telah disimpan');
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeGenerateAkrualKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $query = "update  " . sfConfig::get('app_default_schema') . ".komponen a
                set akrual_code=b.akrual_code||'|'||substring(shsd_id from char_length(shsd_id)-3 for 2)
                from " . sfConfig::get('app_default_schema') . ".kategori_shsd b
                where shsd_id ilike '%.__._'
                and komponen_tipe='SHSD'
                and substring(shsd_id from 1 for char_length(shsd_id)-5) = kategori_shsd_id
                and char_length(b.akrual_code)>0";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query = "update  " . sfConfig::get('app_default_schema') . ".komponen a
                set akrual_code=b.akrual_code||'|'||substring(shsd_id from char_length(shsd_id)-4 for 3)
                from " . sfConfig::get('app_default_schema') . ".kategori_shsd b
                where shsd_id ilike '%.___._'
                and komponen_tipe='SHSD'
                and substring(shsd_id from 1 for char_length(shsd_id)-6) = kategori_shsd_id
                and char_length(b.akrual_code)>0";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query = "update  " . sfConfig::get('app_default_schema') . ".komponen a
                set akrual_code=b.akrual_code||'|'||substring(shsd_id from char_length(shsd_id)-1)
                from " . sfConfig::get('app_default_schema') . ".kategori_shsd b
                where shsd_id ilike '%.__'
                and komponen_tipe='SHSD'
                and substring(shsd_id from 1 for char_length(shsd_id)-3) = kategori_shsd_id
                and char_length(b.akrual_code)>0";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query = "update  " . sfConfig::get('app_default_schema') . ".komponen a
                set akrual_code=b.akrual_code||'|'||substring(shsd_id from char_length(shsd_id)-2)
                from " . sfConfig::get('app_default_schema') . ".kategori_shsd b
                where shsd_id ilike '%.___'
                and komponen_tipe='SHSD'
                and substring(shsd_id from 1 for char_length(shsd_id)-4) = kategori_shsd_id
                and char_length(b.akrual_code)>0";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $query = "update  " . sfConfig::get('app_default_schema') . ".komponen a
                set akrual_code=b.akrual_code||'|'||substring(shsd_id from char_length(shsd_id)-4)
                from " . sfConfig::get('app_default_schema') . ".kategori_shsd b
                where shsd_id not in
                (select shsd_id from " . sfConfig::get('app_default_schema') . ".komponen where shsd_id ilike '%.__._' and substring(shsd_id from 1 for char_length(shsd_id)-5) in (select kategori_shsd_id from ebudget.kategori_shsd))
                and shsd_id not in
                (select shsd_id from " . sfConfig::get('app_default_schema') . ".komponen where shsd_id ilike '%.___._' and substring(shsd_id from 1 for char_length(shsd_id)-6) in (select kategori_shsd_id from ebudget.kategori_shsd))
                and shsd_id not in
                (select shsd_id from " . sfConfig::get('app_default_schema') . ".komponen where shsd_id ilike '%.__' and substring(shsd_id from 1 for char_length(shsd_id)-3) in (select kategori_shsd_id from ebudget.kategori_shsd))
                and shsd_id not in
                (select shsd_id from " . sfConfig::get('app_default_schema') . ".komponen where shsd_id ilike '%.___' and substring(shsd_id from 1 for char_length(shsd_id)-4) in (select kategori_shsd_id from ebudget.kategori_shsd))
                and komponen_tipe='SHSD'
                and substring(shsd_id from 1 for char_length(shsd_id)-6) = kategori_shsd_id
                and char_length(b.akrual_code)>0";
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            $con->commit();
            $this->setFlash('berhasil', 'Kode akrual telah tersimpan');
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadDataAkrual2() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'akrual_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 1; $row <= $highR; $row++) {
                $a = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $b = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $c = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $d = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $e = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $f = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $g = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $h = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $kode_akrual = $a;
                if ($b <> '')
                    $kode_akrual .= '.' . $b;
                if ($c <> '')
                    $kode_akrual .= '.' . $c;
                if ($d <> '')
                    $kode_akrual .= '.' . $d;
                if ($e <> '')
                    $kode_akrual .= '.' . $e;
                if ($f <> '')
                    $kode_akrual .= '.' . $f;
                if ($g <> '')
                    $kode_akrual .= '.' . $g;
                if ($h <> '')
                    $kode_akrual .= '.' . $h;

                if ($kode_akrual != '') {
                    $c_cek_shsd = new Criteria();
                    $c_cek_shsd->add(AkrualPeer::AKRUAL_CODE, $kode_akrual);
                    if ($dapat_komponen = AkrualPeer::doSelectOne($c_cek_shsd)) {
                        $query = "update " . sfConfig::get('app_default_schema') . ".akrual set nama='$nama' where akrual_code='$kode_akrual'";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();
                    } else {
                        $dapat_komponen = new Akrual();
                        $dapat_komponen->setAkrualCode($kode_akrual);
                        $dapat_komponen->setNama($nama);
                        $dapat_komponen->save();
                    }
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Kode akrual telah tersimpan, kecuali ' . $error);
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadDataAkrual() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'akrual_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 1; $row <= $highR; $row++) {
                $a = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $b = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $c = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $d = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                $e = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                $f = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                $g = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                $h = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                $nama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                $kode_akrual = $a;
                if ($b <> '')
                    $kode_akrual .= '.' . $b;
                if ($c <> '')
                    $kode_akrual .= '.' . $c;
                if ($d <> '')
                    $kode_akrual .= '.' . $d;
                if ($e <> '')
                    $kode_akrual .= '.' . $e;
                if ($f <> '')
                    $kode_akrual .= '.' . $f;
                if ($g <> '')
                    $kode_akrual .= '.' . $g;
                if ($h <> '')
                    $kode_akrual .= '.' . $h;

                $j = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                $k = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                $l = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));
                $m = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(12, $row)->getValue()));
                $n = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(13, $row)->getValue()));
                $o = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(14, $row)->getValue()));
                $nama2 = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()));
                $kode_shsd = $j;
                if ($k <> '')
                    if (strlen($k) < 2)
                        $kode_shsd .= '.0' . $k;
                    else
                        $kode_shsd .= '.' . $k;
                if ($l <> '')
                    if (strlen($l) < 2)
                        $kode_shsd .= '.0' . $l;
                    else
                        $kode_shsd .= '.' . $l;
                if ($m <> '')
                    if (strlen($m) < 2)
                        $kode_shsd .= '.0' . $m;
                    else
                        $kode_shsd .= '.' . $m;
                if ($n <> '')
                    if (strlen($n) < 2)
                        $kode_shsd .= '.0' . $n;
                    else
                        $kode_shsd .= '.' . $n;
                if ($o <> '')
                    if (strlen($o) < 2)
                        $kode_shsd .= '.0' . $o;
                    else
                        $kode_shsd .= '.' . $o;

                if ($kode_akrual != '' && $kode_shsd != '') {
                    $c_cek_shsd = new Criteria();
                    $c_cek_shsd->add(KategoriShsdPeer::KATEGORI_SHSD_ID, $kode_shsd);
                    if ($dapat_komponen = KategoriShsdPeer::doSelectOne($c_cek_shsd)) {
//                        echo 'dapat kategori shsd => ';
//                        $dapat_komponen->setAkrualCode($kode_akrual);
//                        $dapat_komponen->save();
                        $query = "update " . sfConfig::get('app_default_schema') . ".kategori_shsd set akrual_code='$kode_akrual' where kategori_shsd_id='$kode_shsd'";
                        $stmt = $con->prepareStatement($query);
                        $stmt->executeQuery();
                    } else {
                        $c_cek_komponen = new Criteria();
                        $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $kode_shsd);
                        if ($dapat_komponen = KomponenPeer::doSelectOne($c_cek_komponen)) {
//                            echo 'dapat komponen => ';
                            $dapat_komponen->setAkrualCode($kode_akrual);
                            $dapat_komponen->save();
                        } else {
                            $kode_shsd = $j;
                            if ($k <> '')
                                $kode_shsd .= '.' . $k;
                            if ($l <> '')
                                $kode_shsd .= '.' . $l;
                            if ($m <> '')
                                $kode_shsd .= '.' . $m;
                            if ($n <> '')
                                $kode_shsd .= '.' . $n;
                            if ($o <> '')
                                $kode_shsd .= '.' . $o;
                            $c_cek_rekening = new Criteria();
                            $c_cek_rekening->add(RekeningPeer::REKENING_CODE, $kode_shsd);
                            if ($dapat_komponen = RekeningPeer::doSelectOne($c_cek_rekening)) {
//                                echo 'dapat rekening => ';
                                $dapat_komponen->setAkrualCode($kode_akrual);
                                $dapat_komponen->save();
                            } else {
                                $error .= $kode_shsd . ' tidak ada ';
                            }
                        }
                    }
//                    echo $kode_shsd . ' - ' . $kode_akrual . '<br>';
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Kode akrual telah tersimpan, kecuali ' . $error);
        } catch (Exception $ex) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeTarikRKA() {
        try {
            $rs_unit_kerja = UnitKerjaPeer::doSelect(new Criteria());
            foreach ($rs_unit_kerja as $unit_kerja) {
                $unit_id = $unit_kerja->getUnitId();
                $c = new Criteria();
                $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                $rs_kegiatan = MasterKegiatanPeer::doSelect($c);
                foreach ($rs_kegiatan as $kegiatan) {
                    $kode_kegiatan = $kegiatan->getKodeKegiatan();
                    $con = Propel::getConnection();
                    $con->begin();
//tanpa centang pilihan
                    $query = "select detail_no
                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd
                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and 
                rd.status_hapus=false and rd.status_level=7 and 
                (detail_no in (select detail_no as nilai 
                    from " . sfConfig::get('app_default_schema') . ".rincian_detail drd 
                    where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and 
                    drd.status_hapus=false and (drd.nilai_anggaran <> rd.nilai_anggaran or char_length(rd.note_skpd) >= 10 or char_length(rd.note_peneliti) >= 10)) 
                    or 
                    detail_no not in (select detail_no as nilai 
                    from " . sfConfig::get('app_default_schema') . ".rincian_detail drd 
                    where drd.unit_id='$unit_id' and drd.kegiatan_code='$kode_kegiatan' and 
                    drd.status_hapus=false))";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    $pilihan = array();
                    while ($rs->next()) {
                        array_push($pilihan, $rs->getInt('detail_no'));
                    }
                    if ($pilihan != null) {
//tanpa centang pilihan
                        $rd = new DinasRincianDetail();
                        if ($rd->cekPerBelanja($unit_id, $kode_kegiatan, 7, $pilihan)) {
                            $con->rollback();
                            $this->setFlash('gagal', 'Masih ada belanja yang belum balance');
                            echo $unit_id . ' ' . $kode_kegiatan . 'Masih ada belanja yang belum balance';
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                        }
                        foreach ($pilihan as $detail_no) {
                            $c = new Criteria();
                            $c->add(DinasRincianDetailPeer::UNIT_ID, $unit_id);
                            $c->add(DinasRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c->add(DinasRincianDetailPeer::DETAIL_NO, $detail_no);
                            $rs_drd = DinasRincianDetailPeer::doSelectOne($c);

//cek yang dicentang, levelnya lebih dari 7 (sampai tim anggaran)
                            if ($rs_drd->getStatusLevel() < 7) {
                                $this->setFlash('gagal', 'Komponen ' . $rs_rd->getKomponenName() . ' belum berada di posisi Penyelia II');
                                $con->rollback();
                                echo $unit_id . ' ' . $kode_kegiatan;
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan");
                            }

                            //$nilaiBaru = round($rs_drd->getKomponenHargaAwal() * $rs_drd->getVolume() * (100 + $rs_drd->getPajak()) / 100);
                            $nilaiBaru = $rs_drd->getNilaiAnggaran();
                            $totNilaiSwakelola = 0;
                            $totNilaiKontrak = 0;
                            $totNilaiAlokasi = 0;
                            $totNilaiHps = 0;
                            $ceklelangselesaitidakaturanpembayaran = 0;
                            $lelang = 0;

                            if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                                $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                    $lelang = $rd->getCekLelang($unit_id, $kode_kegiatan, $detail_no, $rs_drd->getNilaiAnggaran());
                                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                        $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                                        $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);

                                        $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                                        $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                                    }
                                }
                            }
                            if (($nilaiBaru < $totNilaiKontrak) || ($nilaiBaru < $totNilaiSwakelola)) {
                                if ($totNilaiKontrak == 0) {
                                    $this->setFlash('gagal', 'Mohon maaf , untuk  komponen ' . $rs_drd->getKomponenName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                                } else if ($totNilaiSwakelola == 0) {
                                    $this->setFlash('gagal', 'Mohon maaf , untuk  komponen ' . $rs_drd->getKomponenName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                                } else {
                                    $this->setFlash('gagal', 'Mohon maaf , untuk  komponen ' . $rs_drd->getKomponenName() . ' sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                                }
                                $con->rollback();
                                echo $unit_id . ' ' . $kode_kegiatan;
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                            } else if ($nilaiBaru < $totNilaiHps) {
                                $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                                $con->rollback();
                                echo $unit_id . ' ' . $kode_kegiatan;
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                            } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                                $this->setFlash('gagal', 'Proses Lelang untuk  komponen ' . $rs_drd->getKomponenName() . ' (' . $rs_drd->getDetailName() . ') telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                                $con->rollback();
                                echo $unit_id . ' ' . $kode_kegiatan;
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                            } else if ($lelang > 0) {
                                $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk  komponen ' . $rs_drd->getKomponenName() . ' (' . $rs_drd->getDetailName() . ')');
                                $con->rollback();
                                echo $unit_id . ' ' . $kode_kegiatan;
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                            } else {
                                if ($nilaiBaru < $totNilaiAlokasi) {
                                    $I = new Criteria();
                                    $I->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
                                    $I->add(RincianDetailBpPeer::KEGIATAN_CODE, $kode_kegiatan);
                                    $I->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
                                    $rd_bp = RincianDetailBpPeer::doSelectOne($I);
                                    if ($rd_bp) {
                                        try {
                                            $rd_bp->setKeteranganKoefisien($rs_drd->getKeteranganKoefisien());
                                            $rd_bp->setVolume($rs_drd->getVolume());
                                            $rd_bp->setDetailName($rs_drd->getDetailName());
                                            $rd_bp->setSubtitle($rs_drd->getSubtitle());
                                            $rd_bp->setSub($rs_drd->getSub());
                                            $rd_bp->setNoteSkpd($rs_drd->getNoteskpd());
                                            $rd_bp->setKodeSub($rs_drd->getKodeSub());
                                            $rd_bp->setKecamatan($rs_drd->getKodeJasmas());
                                            $rd_bp->setIsPerKomponen('true');
                                            $rd_bp->setTahap($rs_drd->getTahap());
                                            $rd_bp->save();

                                            budgetLogger::log('Mengupdate volume eProject untuk komponen menjadi ' . $rs_drd->getVolume() . ' dari unit id :' . $unit_id . ' dengan kode :' . $kode_kegiatan . '; detail_no :' . $detail_no . '; komponen_id:' . $rs_drd->getKomponenId() . '; komponen_name:' . $rs_drd->getKomponenName());
                                        } catch (Exception $ex) {
                                            $this->setFlash('gagal', 'Gagal karena' . $ex);
                                            $con->rollback();
                                            echo $unit_id . ' ' . $kode_kegiatan;
//exit;
//return $this->redirect("report/tampillaporanasli?unit_id=$unit_id&kode_kegiatan=$kode_kegiatan&status=1");
                                        }
                                    }
                                }
                                $c = new Criteria();
                                $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                                $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                                if ($rs_rd = RincianDetailPeer::doSelectOne($c)) {
                                    $rs_rd->setTipe($rs_drd->getTipe());
                                    $rs_rd->setRekeningCode($rs_drd->getRekeningCode());
                                    $rs_rd->setKomponenId($rs_drd->getKomponenId());
                                    $rs_rd->setDetailName($rs_drd->getDetailName());
                                    $rs_rd->setVolume($rs_drd->getVolume());
                                    $rs_rd->setKeteranganKoefisien($rs_drd->getKeteranganKoefisien());
                                    $rs_rd->setSubtitle($rs_drd->getSubtitle());
                                    $rs_rd->setKomponenHarga($rs_drd->getKomponenHarga());
                                    $rs_rd->setKomponenHargaAwal($rs_drd->getKomponenHargaAwal());
                                    $rs_rd->setKomponenName($rs_drd->getKomponenName());
                                    $rs_rd->setSatuan($rs_drd->getSatuan());
                                    $rs_rd->setPajak($rs_drd->getPajak());
                                    $rs_rd->setFromSubKegiatan($rs_drd->getFromSubKegiatan());
                                    $rs_rd->setSub($rs_drd->getSub());
                                    $rs_rd->setKodeSub($rs_drd->getKodeSub());
                                    $rs_rd->setLastUpdateUser($rs_drd->getLastUpdateUser());
                                    $rs_rd->setLastUpdateTime($rs_drd->getLastUpdateTime());
                                    $rs_rd->setLastUpdateIp($rs_drd->getLastUpdateIp());
                                    $rs_rd->setTahap($rs_drd->getTahap());
                                    $rs_rd->setTahapEdit($rs_drd->getTahapEdit());
                                    $rs_rd->setTahapNew($rs_drd->getTahapNew());
                                    $rs_rd->setStatusLelang($rs_drd->getStatusLelang());
                                    $rs_rd->setNomorLelang($rs_drd->getNomorLelang());
                                    $rs_rd->setKoefisienSemula($rs_drd->getKoefisienSemula());
                                    $rs_rd->setVolumeSemula($rs_drd->getVolumeSemula());
                                    $rs_rd->setHargaSemula($rs_drd->getHargaSemula());
                                    $rs_rd->setTotalSemula($rs_drd->getTotalSemula());
                                    $rs_rd->setLockSubtitle($rs_drd->getLockSubtitle());
                                    $rs_rd->setStatusHapus($rs_drd->getStatusHapus());
                                    $rs_rd->setTahun($rs_drd->getTahun());
                                    $rs_rd->setKodeLokasi($rs_drd->getKodeLokasi());
                                    $rs_rd->setKecamatan($rs_drd->getKecamatan());
                                    $rs_rd->setRekeningCodeAsli($rs_drd->getRekeningCodeAsli());
                                    $rs_rd->setNilaiAnggaran($rs_drd->getNilaiAnggaran());
                                    $rs_rd->setNoteSkpd($rs_drd->getNoteSkpd());
                                    $rs_rd->setNotePeneliti($rs_drd->getNotePeneliti());
                                    $rs_rd->setIsBlud($rs_drd->getIsBlud());
                                    $rs_rd->setOb($rs_drd->getOb());
                                    $rs_rd->setObFromId($rs_drd->getObFromId());
                                    $rs_rd->setIsPerKomponen($rs_drd->getIsPerKomponen());
                                    $rs_rd->setKegiatanCodeAsal($rs_drd->getKegiatanCodeAsal());
                                    $rs_rd->setThKeMultiyears($rs_drd->getThKeMultiyears());
                                    $rs_rd->setLokasiKecamatan($rs_drd->getLokasiKecamatan());
                                    $rs_rd->setLokasiKelurahan($rs_drd->getLokasiKelurahan());
                                    $rs_rd->setHargaSebelumSisaLelang($rs_drd->getHargaSebelumSisaLelang());
                                    $rs_rd->setIsMusrenbang($rs_drd->getIsMusrenbang());
                                    $rs_rd->setSubIdAsal($rs_drd->getSubIdAsal());
                                    $rs_rd->setSubtitleAsal($rs_drd->getSubtitleAsal());
                                    $rs_rd->setKodeSubAsal($rs_drd->getKodeSubAsal());
                                    $rs_rd->setSubAsal($rs_drd->getSubAsal());
                                    $rs_rd->setLastEditTime($rs_drd->getLastEditTime());
                                    $rs_rd->setIsPotongBpjs($rs_drd->getIsPotongBpjs());
                                    $rs_rd->setIsIuranBpjs($rs_drd->getIsIuranBpjs());
                                    $rs_rd->setStatusOb($rs_drd->getStatusOb());
                                    $rs_rd->setObParent($rs_drd->getObParent());
                                    $rs_rd->setObAlokasiBaru($rs_drd->getObAlokasiBaru());
                                    $rs_rd->setIsHibah($rs_drd->getIsHibah());
                                    $rs_rd->save();
                                } else {
                                    $con = Propel::getConnection();
                                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                            . " select kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, "
                                            . " keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, komponen_name, satuan, "
                                            . " pajak, unit_id, from_sub_kegiatan, sub, kode_sub, last_update_user, last_update_time, last_update_ip, "
                                            . " tahap, tahap_edit, tahap_new, status_lelang, nomor_lelang, koefisien_semula, volume_semula, harga_semula, "
                                            . " total_semula, lock_subtitle, status_hapus, tahun, kode_lokasi, kecamatan, rekening_code_asli, "
                                            . " nilai_anggaran, note_skpd, note_peneliti, asal_kegiatan, is_blud, ob, ob_from_id, "
                                            . " is_per_komponen, kegiatan_code_asal, th_ke_multiyears, lokasi_kecamatan, lokasi_kelurahan, "
                                            . " harga_sebelum_sisa_lelang, is_musrenbang, sub_id_asal, subtitle_asal, kode_sub_asal, sub_asal, "
                                            . " last_edit_time, is_potong_bpjs, is_iuran_bpjs, is_kapitasi_bpjs, is_iuran_jkn, is_iuran_jkk, "
                                            . " status_ob, ob_parent, ob_alokasi_baru, is_hibah "
                                            . " from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                            . " where status_hapus = false "
                                            . " and unit_id = '" . $unit_id . "' "
                                            . " and kegiatan_code = '" . $kode_kegiatan . "' "
                                            . " and detail_no = '" . $detail_no . "' ";
                                    $stmt = $con->prepareStatement($query);
                                    $stmt->executeQuery();
                                }

                                if ($unit_id != '9999') {
                                    $c = new Criteria();
                                    $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                                    $c->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                                    $c->addAnd(RincianDetailPeer::DETAIL_NO, $detail_no);
                                    if ($rs_rka = RincianDetailPeer::doSelectOne($c)) {
                                        $semula = $rs_rka->getNilaiAnggaran();
                                    } else {
                                        $semula = 0;
                                    }
                                    $menjadi = $rs_rd->getNilaiAnggaran();
                                    if ($semula <> $menjadi) {
                                        $tahap = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
                                        $c_log = new Criteria();
                                        $c_log->add(LogPerubahanRevisiPeer::UNIT_ID, $unit_id);
                                        $c_log->addAnd(LogPerubahanRevisiPeer::KEGIATAN_CODE, $kode_kegiatan);
                                        $c_log->addAnd(LogPerubahanRevisiPeer::DETAIL_NO, $detail_no);
                                        $c_log->addAnd(LogPerubahanRevisiPeer::TAHAP, $tahap);

                                        if ($log = LogPerubahanRevisiPeer::doSelectOne($c_log)) {
                                            $log->setNilaiAnggaranSemula($semula);
                                            $log->setNilaiAnggaranMenjadi($menjadi);
                                            $log->setStatus(1);
                                            $log->save();
                                        } else {
                                            $log = new LogPerubahanRevisi();
                                            $log->setUnitId($unit_id);
                                            $log->setKegiatanCode($kode_kegiatan);
                                            $log->setDetailNo($detail_no);
                                            $log->setTahap($tahap);
                                            $log->setNilaiAnggaranSemula($semula);
                                            $log->setNilaiAnggaranMenjadi($menjadi);
                                            $log->setStatus(1);
                                            $log->save();
                                        }
                                    }
                                }
                                $rs_drd->setStatusLevel(8);
                                $rs_drd->save();
                                echo 'Telah berhasil memproses masuk ke dalam RKA' . $unit_id . ' - ' . $kode_kegiatan . '<br>';
                            }
                        }
                    }
                }
            }
            echo 'Telah berhasil memproses masuk ke dalam RKA';
            $con->commit();
            exit;
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit;
        }
    }

    /**
     * Executes index action
     *
     */
    public function executeBukaRincianDewan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        try {
            $query_seting = "update ebudget.seting_aplikasi "
                    . "set status_rincian_dewan = 1 "
                    . "where id = 1";
            $stmt_seting = $con->prepareStatement($query_seting);
            $stmt_seting->executeQuery();
            $this->setFlash('berhasil', 'Login Dewan DAPAT melihat rincian komponen');
            return $this->redirect('kegiatan/list');
        } catch (Exception $exc) {
            $this->setFlash('gagal', 'Login Dewan TIDAK DAPAT melihat rincian komponen');
            return $this->redirect('kegiatan/list');
        }
    }

    public function executeUrutkanPrioritasWaiting() {
        $con = Propel::getConnection();
        try {
            $query_jml_waiting = "select kegiatan_code, count(*) as jumlah
                                from ebudget.waitinglist_pu
                                where status_hapus=false and status_waiting=0
                                group by kegiatan_code";
            $stmt_jml_waiting = $con->prepareStatement($query_jml_waiting);
            $rs_jml_waiting = $stmt_jml_waiting->executeQuery();
            while ($rs_jml_waiting->next()) {
                $kode_kegiatan = trim($rs_jml_waiting->getString('kegiatan_code'));
                $jumlah = $rs_jml_waiting->getInt('jumlah');
                $query = " select id_waiting, kegiatan_code, prioritas "
                        . " from ebudget.waitinglist_pu "
                        . " where status_hapus=false and status_waiting=0 and kegiatan_code='" . $kode_kegiatan . "' "
                        . " order by kegiatan_code, prioritas, id_waiting ";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                $i = 1;
                while ($rs->next()) {
                    $id_waiting = $rs->getInt('id_waiting');
                    $query = " update ebudget.waitinglist_pu "
                            . " set prioritas = " . $i++
                            . " where id_waiting = " . $id_waiting;
                    $stmt2 = $con->prepareStatement($query);
                    $rs2 = $stmt2->executeQuery();
                }
            }
            $this->setFlash('berhasil', 'Prioritas sudah urut kembali');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $this->setFlash('gagal', 'Prioritas gagal diurutkan');
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeBetulDeskripsiResume() {
        $con = Propel::getConnection();
        try {

            $query_skpd="select unit_id from unit_kerja";
            $skpd=$con->prepareStatement($query_skpd);
            $rs_skpd =  $skpd->executeQuery();
             while ($rs_skpd->next()) {
                $unit_id=$rs_skpd->getString('unit_id');
                $query="delete from ebudget.deskripsi_resume_rapat_rekening
                        where kegiatan_code not in (select kode_kegiatan from ebudget.dinas_master_kegiatan where unit_id='". $unit_id ."') 
                        and id_deskripsi_resume_rapat=(select distinct max(id) from ebudget.deskripsi_resume_rapat
                        where unit_id='" . $unit_id . "'
                        group by unit_id)";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
             }
            $this->setFlash('berhasil', 'Deskripsi Resume Rapat sudah disesuaikan mohon cek kembali');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $this->setFlash('gagal', 'Deskripsi Resume Rapat gagal diupdate');
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executePerbaruiLink() {
        $con = Propel::getConnection();
        try {
            $c = new Criteria();
            $rs_spjm = UsulanSPJMPeer::doSelect($c);
            foreach ($rs_spjm as $value) {
                $filepath = $value->getFilepath();
                $filepath_baru = str_replace("/budgeting.surabaya", "/ebudgeting.surabaya", $filepath);
                $spjm_update = UsulanSPJMPeer::retrieveByPK($value->getIdSpjm());
                $spjm_update->setFilepath($filepath_baru);
                $spjm_update->save();
            }
            $c = new Criteria();
            $rs_dinas = UsulanDinasPeer::doSelect($c);
            foreach ($rs_dinas as $value) {
                $filepath = $value->getFilepath();
                $filepathrar = $value->getFilepathRar();
                $filepath_baru = str_replace("/budgeting.surabaya", "/ebudgeting.surabaya", $filepath);
                $filepathrar_baru = str_replace("/budgeting.surabaya", "/ebudgeting.surabaya", $filepathrar);
                $dinas_update = UsulanDinasPeer::retrieveByPK($value->getIdUsulan());
                $dinas_update->setFilepath($filepath_baru);
                $dinas_update->setFilepathRar($filepathrar_baru);
                $dinas_update->save();
            }
            $this->setFlash('berhasil', 'Filepath telah diperbarui');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $this->setFlash('gagal', 'Filepath gagal diperbarui');
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTutupRincianDewan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        try {
            $query_seting = "update ebudget.seting_aplikasi "
                    . "set status_rincian_dewan = 0 "
                    . "where id = 1";
            $stmt_seting = $con->prepareStatement($query_seting);
            $stmt_seting->executeQuery();
            $this->setFlash('berhasil', 'Login Dewan TIDAK DAPAT melihat rincian komponen');
            return $this->redirect('kegiatan/list');
        } catch (Exception $exc) {
            $this->setFlash('gagal', 'Login Dewan DAPAT melihat rincian komponen');
            return $this->redirect('kegiatan/list');
        }
    }

    public function executeUbahTambahanPagu() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_list_kegiatan = "select * "
                    . "from ebudget.master_kegiatan "
                    . "where unit_id <> '9999' ";
            $stmt_list_kegiatan = $con->prepareStatement($query_list_kegiatan);
            $rs_list_kegiatan = $stmt_list_kegiatan->executeQuery();
            while ($rs_list_kegiatan->next()) {
                $unit_id = trim($rs_list_kegiatan->getString('unit_id'));
                $kode_kegiatan = trim($rs_list_kegiatan->getString('kode_kegiatan'));
                $alokasi_dana = trim($rs_list_kegiatan->getString('alokasi_dana'));
                $tambahan_pagu = trim($rs_list_kegiatan->getString('tambahan_pagu'));

                $total_mk = $alokasi_dana + $tambahan_pagu;

                $query_total_skpd = "select sum(nilai_anggaran) as total "
                        . "from ebudget.rincian_detail "
                        . "where unit_id = '$unit_id' and status_hapus = false "
                        . "and kegiatan_code = '$kode_kegiatan' ";
                $stmt_total_skpd = $con->prepareStatement($query_total_skpd);
                $rs_total = $stmt_total_skpd->executeQuery();
                while ($rs_total->next()) {
                    $total_dinas_sekarang = $rs_total->getString('total');
                }

                if ($total_dinas_sekarang <> $total_mk) {
                    $tambahan_pagu_baru = $total_dinas_sekarang - $alokasi_dana;

                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                    $dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_kegiatan);
                    if ($dapat_kegiatan) {
                        $dapat_kegiatan->setTambahanPagu($tambahan_pagu_baru);
                        $dapat_kegiatan->save();
                    }
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Pengubahan Tambahan Pagu berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Pengubahan Tambahan Pagu gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTambahPaguKUAPPAS() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_list_skpd = "select * "
                    . "from ebudget.maks_uk "
                    . "order by unit_id";
            $stmt_list_skpd = $con->prepareStatement($query_list_skpd);
            $rs_list_skpd = $stmt_list_skpd->executeQuery();
            while ($rs_list_skpd->next()) {

                $unit_id = trim($rs_list_skpd->getString('unit_id'));
                $nilai_maks = trim($rs_list_skpd->getString('nilai_maks'));

                $kode_UK = '23.01.01.04.04';
                $rekening_UK = '5.2.1.04.01';

                $query_total_skpd_uk = "select sum(nilai_anggaran) as total "
                        . "from ebudget.rincian_detail "
                        . "where unit_id = '$unit_id' and status_hapus = false "
                        . "and rekening_code = '$rekening_UK' ";
                $stmt_total_skpd_uk = $con->prepareStatement($query_total_skpd_uk);
                $rs_total = $stmt_total_skpd_uk->executeQuery();
                while ($rs_total->next()) {
                    $total_uk_dinas_sekarang = $rs_total->getString('total');
                }

                $over_uk = $total_uk_dinas_sekarang - $nilai_maks;

                if ($over_uk > 0) {
                    $c_ambil_kegiatan_01 = new Criteria();
                    $c_ambil_kegiatan_01->add(MasterKegiatanPeer::KODE_KEGIATAN, '%.01.____', Criteria::ILIKE);
                    $c_ambil_kegiatan_01->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    $dapat_ambil_kegiatan_01 = MasterKegiatanPeer::doSelectOne($c_ambil_kegiatan_01);

                    if ($dapat_ambil_kegiatan_01) {
                        $kode_kegiatan = $dapat_ambil_kegiatan_01->getKodeKegiatan();

                        $c_ambil_rd = new Criteria();
                        $c_ambil_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_ambil_rd->add(RincianDetailPeer::UNIT_ID, $unit_id);
                        $c_ambil_rd->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
                        $c_ambil_rd->add(RincianDetailPeer::KOMPONEN_ID, $kode_UK);
                        $dapat_ambil_komponen_uk_01 = RincianDetailPeer::doSelectOne($c_ambil_rd);

                        if ($dapat_ambil_komponen_uk_01) {
                            $unit_rd = $dapat_ambil_komponen_uk_01->getUnitId();
                            $kegiatan_rd = $dapat_ambil_komponen_uk_01->getKegiatanCode();
                            $detailno_rd = $dapat_ambil_komponen_uk_01->getDetailNo();
                            $harga_rd = $dapat_ambil_komponen_uk_01->getKomponenHargaAwal();

                            $kode_rka = $unit_rd . '.' . $kegiatan_rd . '.' . $detailno_rd;

                            $satuan_rd = $dapat_ambil_komponen_uk_01->getSatuan();
                            $nilaianggaran_rd = $dapat_ambil_komponen_uk_01->getNilaiAnggaran();

                            if ($nilaianggaran_rd >= $over_uk) {
                                $nilai_baru = $nilaianggaran_rd - $over_uk;

                                $volume_baru = ($nilai_baru / $harga_rd);
                                $koefisien_baru = $volume_baru . ' ' . $satuan_rd;

                                $dapat_ambil_komponen_uk_01->setVolume($volume_baru);
                                $dapat_ambil_komponen_uk_01->setKeteranganKoefisien($koefisien_baru);
                                $dapat_ambil_komponen_uk_01->setNoteSkpd('Pengurangan untuk penyesuaian nilai UK');
                                $dapat_ambil_komponen_uk_01->save();
                            }
                        }
                    }
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Pengurangan UK berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Pengurangan UK gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeKurangiBarjasUK() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_list_skpd = "select * "
                    . "from ebudget.maks_uk "
                    . "order by unit_id";
            $stmt_list_skpd = $con->prepareStatement($query_list_skpd);
            $rs_list_skpd = $stmt_list_skpd->executeQuery();
            while ($rs_list_skpd->next()) {

                $unit_id = trim($rs_list_skpd->getString('unit_id'));
                $nilai_maks = trim($rs_list_skpd->getString('nilai_maks'));

                $kode_UK = '23.01.01.04.04';
                $rekening_UK = '5.2.1.04.01';

                $query_total_skpd_uk = "select sum(nilai_anggaran) as total "
                        . "from ebudget.rincian_detail "
                        . "where unit_id = '$unit_id' and status_hapus = false "
                        . "and rekening_code = '$rekening_UK' ";
                $stmt_total_skpd_uk = $con->prepareStatement($query_total_skpd_uk);
                $rs_total = $stmt_total_skpd_uk->executeQuery();
                while ($rs_total->next()) {
                    $total_uk_dinas_sekarang = $rs_total->getString('total');
                }

                $over_uk = $total_uk_dinas_sekarang - $nilai_maks;

                if ($over_uk > 0) {
                    $c_ambil_kegiatan_01 = new Criteria();
                    $c_ambil_kegiatan_01->add(MasterKegiatanPeer::KODE_KEGIATAN, '%.01.____', Criteria::ILIKE);
                    $c_ambil_kegiatan_01->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    $dapat_ambil_kegiatan_01 = MasterKegiatanPeer::doSelectOne($c_ambil_kegiatan_01);

                    if ($dapat_ambil_kegiatan_01) {
                        $kode_kegiatan = $dapat_ambil_kegiatan_01->getKodeKegiatan();

                        $c_ambil_rd = new Criteria();
                        $c_ambil_rd->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_ambil_rd->add(RincianDetailPeer::UNIT_ID, $unit_id);
                        $c_ambil_rd->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
                        $c_ambil_rd->add(RincianDetailPeer::KOMPONEN_ID, $kode_UK);
                        $dapat_ambil_komponen_uk_01 = RincianDetailPeer::doSelectOne($c_ambil_rd);

                        if ($dapat_ambil_komponen_uk_01) {
                            $unit_rd = $dapat_ambil_komponen_uk_01->getUnitId();
                            $kegiatan_rd = $dapat_ambil_komponen_uk_01->getKegiatanCode();
                            $detailno_rd = $dapat_ambil_komponen_uk_01->getDetailNo();
                            $harga_rd = $dapat_ambil_komponen_uk_01->getKomponenHargaAwal();

                            $kode_rka = $unit_rd . '.' . $kegiatan_rd . '.' . $detailno_rd;

                            $satuan_rd = $dapat_ambil_komponen_uk_01->getSatuan();
                            $nilaianggaran_rd = $dapat_ambil_komponen_uk_01->getNilaiAnggaran();

                            if ($nilaianggaran_rd >= $over_uk) {
                                $nilai_baru = $nilaianggaran_rd - $over_uk;

                                $volume_baru = ($nilai_baru / $harga_rd);
                                $koefisien_baru = $volume_baru . ' ' . $satuan_rd;

                                $dapat_ambil_komponen_uk_01->setVolume($volume_baru);
                                $dapat_ambil_komponen_uk_01->setKeteranganKoefisien($koefisien_baru);
                                $dapat_ambil_komponen_uk_01->setNoteSkpd('Pengurangan untuk penyesuaian nilai UK');
                                $dapat_ambil_komponen_uk_01->save();
                            }
                        }
                    }
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Pengurangan UK berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Pengurangan UK gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeKasihKodeKegiatan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_list_skpd = "select * "
                    . "from unit_kerja "
                    . "order by unit_id";
            $stmt_list_skpd = $con->prepareStatement($query_list_skpd);
            $rs_list_skpd = $stmt_list_skpd->executeQuery();
            while ($rs_list_skpd->next()) {

                $kode = 1;

                $unit_id = trim($rs_list_skpd->getString('unit_id'));

                $query_list_kegiatan = "select dk.* "
                        . "from devplan_kegiatan_30sept dk, unit_kerja uk "
                        . "where dk.kode_skpd = uk.unit_id and uk.unit_id = '$unit_id' "
                        . "order by kode_skpd, kode_kegiatan";
                $stmt_list_kegiatan = $con->prepareStatement($query_list_kegiatan);
                $rs_list_kegiatan = $stmt_list_kegiatan->executeQuery();
                while ($rs_list_kegiatan->next()) {
                    $serial_kegiatan = trim($rs_list_kegiatan->getString('id'));
                    $queryUpdateKegiatan = "update devplan_kegiatan_30sept "
                            . "set kode_kegiatan = '$kode' "
                            . "where id = '$serial_kegiatan' ";
                    $stmtUpdateKegiatan = $con->prepareStatement($queryUpdateKegiatan);
                    $stmtUpdateKegiatan->executeQuery();

                    $kode++;
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Pemberian Kode Kegiatan berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Pemberian Kode Kegiatan gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTarikDevplanKegiatan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $sekarang = date('Y-m-d H:i:s');

            $query_list_kegiatan = "select dk.* "
                    . "from devplan_kegiatan_30sept dk, unit_kerja uk "
                    . "where dk.kode_skpd = uk.unit_id and dk.kode_program is not null and dk.kode_program <> '' "
                    . "order by kode_skpd, kode_kegiatan";
            $stmt_list_kegiatan = $con->prepareStatement($query_list_kegiatan);
            $rs_list_kegiatan = $stmt_list_kegiatan->executeQuery();
            while ($rs_list_kegiatan->next()) {
                $serial_kegiatan = trim($rs_list_kegiatan->getString('id'));

                $kode_skpd = trim($rs_list_kegiatan->getString('kode_skpd'));
                $kode_kegiatan = trim($rs_list_kegiatan->getString('kode_kegiatan'));

                $kode_urusan = trim($rs_list_kegiatan->getString('kode_urusan'));
                $kode_program = trim($rs_list_kegiatan->getString('kode_program'));
                $kode_indikator = trim($rs_list_kegiatan->getString('kode_indikator'));

                $kode = $kode_program . '.' . $kode_kegiatan;

                $kode_keuangan = str_replace('.', ' ', $kode);

                $nama_kegiatan = trim($rs_list_kegiatan->getString('nama_kegiatan'));

                $lokasi = trim($rs_list_kegiatan->getString('lokasi'));
                $target_capaian = trim($rs_list_kegiatan->getString('target_capaian'));

                $output_narasi = trim($rs_list_kegiatan->getString('output_narasi'));
                $output_volume = trim($rs_list_kegiatan->getString('output_volume'));
                $output_satuan = trim($rs_list_kegiatan->getString('output_satuan'));

                $output = $output_narasi . '|' . $output_volume . '|' . $output_satuan;

                $alokasi = 0;
//                cek subtitle juga
//                $query_count_komponen = "select count(*) as jumlah "
//                        . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk, devplan_output_subtitle_30sept dos, unit_kerja uk "
//                        . "where ddk.id_kegiatan = dk.id and ddk.subtitle = dos.subtitle "
//                        . "and dos.id_kegiatan = dk.id and dk.kode_skpd = uk.unit_id "
//                        . "and dk.id = $serial_kegiatan ";
//                cek subtitle juga
//                cek kegiatan doang
                $query_count_komponen = "select count(*) as jumlah "
                        . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_26okt ddk, unit_kerja uk "
                        . "where ddk.id_kegiatan = dk.id and dk.kode_skpd = uk.unit_id "
                        . "and dk.id = $serial_kegiatan "
                        . "and ddk.volume > 0";
//                cek kegiatan doang

                $stmt_count_komponen = $con->prepareStatement($query_count_komponen);
                $rs_count_komponen = $stmt_count_komponen->executeQuery();
                while ($rs_count_komponen->next()) {
                    $jumlah_komponen = $rs_count_komponen->getFloat('jumlah');
                }

                if ($jumlah_komponen > 0) {
//                cek subtitle juga                    
//                    $query_list_komponen = "select sum(round(harga*volume)) as total "
//                            . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk, devplan_output_subtitle_30sept dos, unit_kerja uk "
//                            . "where ddk.id_kegiatan = dk.id and ddk.subtitle = dos.subtitle "
//                            . "and dos.id_kegiatan = dk.id and dk.kode_skpd = uk.unit_id "
//                            . "and dk.id = $serial_kegiatan ";
//                cek subtitle juga
//                cek kegiatan doang
                    $query_list_komponen = "select round(sum(harga*volume)) as total "
                            . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_26okt ddk, unit_kerja uk "
                            . "where ddk.id_kegiatan = dk.id and dk.kode_skpd = uk.unit_id  "
                            . "and dk.id = $serial_kegiatan ";
//                cek kegiatan doang
                    $stmt_list_komponen = $con->prepareStatement($query_list_komponen);
                    $rs_list_komponen = $stmt_list_komponen->executeQuery();
                    while ($rs_list_komponen->next()) {
                        $alokasi = $rs_list_komponen->getFloat('total');
                    }

//                    cek kode_kegiatan & unit_id
                    $query_cek_kegiatan = "select count(*) as total_ada "
                            . "from " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                            . "where unit_id = '$kode_skpd' and kode_kegiatan = '$kode' ";
                    $stmt_cek_kegiatan = $con->prepareStatement($query_cek_kegiatan);
                    $rs_cek_komponen = $stmt_cek_kegiatan->executeQuery();
                    while ($rs_cek_komponen->next()) {
                        $total_ada = $rs_cek_komponen->getFloat('total_ada');
                    }
//                    cek kode_kegiatan & unit_id                    

                    if ($total_ada == 0) {
//                        kalo belum ada, nambah kegiatan baru
                        $queryInsertKegiatan = "insert into " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                                . "(unit_id, kode_kegiatan, nama_kegiatan, tahun, output, kode_program2, tipe, "
                                . "lokasi, kode_indikator, kode_urusan, tahap, kode_keg_keuangan, alokasi_dana, last_update_time) "
                                . "VALUES ('$kode_skpd','$kode','$nama_kegiatan', '" . sfConfig::get('app_tahun_default') . "', '$output', '$kode_program', 'murni', "
                                . "'$lokasi', '$kode_indikator', '$kode_urusan', 'murni', '$kode_keuangan', $alokasi, '$sekarang') ";
                        $stmtInsertKegiatan = $con->prepareStatement($queryInsertKegiatan);
                        $stmtInsertKegiatan->executeQuery();

                        $queryInsertKegiatanNilai = "insert into " . sfConfig::get('app_default_schema') . ".kegiatan_nilai "
                                . "(unit_id,kegiatan_code,nilai_1,nilai_2,nilai_3,total,keterangan,prioritas) "
                                . "VALUES ('$kode_skpd','$kode',2,2,2,200,'LOLOS','Utama')";
                        $stmtInsertKegiatanNilai = $con->prepareStatement($queryInsertKegiatanNilai);
                        $stmtInsertKegiatanNilai->executeQuery();

                        $queryInsertRincian = "insert into " . sfConfig::get('app_default_schema') . ".rincian "
                                . "(unit_id, kegiatan_code, tipe, rincian_confirmed, rincian_changed, rincian_selesai, "
                                . "waktu_access, rincian_level, lock, last_update_time, tahun, lock_bappeko) "
                                . "VALUES ('$kode_skpd', '$kode', 'murni', FALSE, FALSE, FALSE, "
                                . "'$sekarang', 3, TRUE, '$sekarang', '" . sfConfig::get('app_tahun_default') . "', TRUE) ";
                        $stmtInsertRincian = $con->prepareStatement($queryInsertRincian);
                        $stmtInsertRincian->executeQuery();
//                        kalo belum ada, nambah kegiatan baru                        
                    } else {
//                        kalo ada, update alokasi di kegiatan tersebut 
                        $queryUpdateAnggaran = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                                . "set alokasi_dana = $alokasi, output = '$output' "
                                . "where unit_id = '$kode_skpd' and kode_kegiatan = '$kode' ";
                        $stmtUpdateAnggaran = $con->prepareStatement($queryUpdateAnggaran);
                        $stmtUpdateAnggaran->executeQuery();
//                        kalo ada, update alokasi di kegiatan tersebut                         
                    }
                }
            }

            $con->commit();
            $this->setFlash('berhasil', 'Tarik kegiatan berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Tarik kegiatan gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTarikDevplanSubtitle() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $sekarang = date('Y-m-d H:i:s');
            $query_list_kegiatan = "select dk.* "
                    . "from devplan_kegiatan_30sept dk, unit_kerja uk "
                    . "where dk.kode_skpd = uk.unit_id and dk.kode_program is not null and dk.kode_program <> '' "
                    . "order by kode_skpd, kode_kegiatan";
            $stmt_list_kegiatan = $con->prepareStatement($query_list_kegiatan);
            $rs_list_kegiatan = $stmt_list_kegiatan->executeQuery();
            while ($rs_list_kegiatan->next()) {
                $serial_kegiatan = trim($rs_list_kegiatan->getString('id'));

                $kode_skpd = trim($rs_list_kegiatan->getString('kode_skpd'));
                $kode_kegiatan = trim($rs_list_kegiatan->getString('kode_kegiatan'));
                $kode_program = trim($rs_list_kegiatan->getString('kode_program'));

                $kode = $kode_program . '.' . $kode_kegiatan;

                $query_count_komponen = "select count(*) as jumlah "
                        . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk  "
                        . "where ddk.id_kegiatan = dk.id "
                        . "and dk.id = $serial_kegiatan and ddk.volume > 0 ";
                $stmt_count_komponen = $con->prepareStatement($query_count_komponen);
                $rs_count_komponen = $stmt_count_komponen->executeQuery();
                while ($rs_count_komponen->next()) {
                    $jumlah_komponen = $rs_count_komponen->getFloat('jumlah');
                }
                if ($jumlah_komponen > 0) {
                    $query_distinct_subtitle = "select distinct trim(subtitle) as subtitle, dk.id "
                            . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk  "
                            . "where ddk.id_kegiatan = dk.id  "
                            . "and dk.id = $serial_kegiatan  and ddk.volume > 0";
                    $stmt_distinct_subtitle = $con->prepareStatement($query_distinct_subtitle);
                    $rs_distinct_subtitle = $stmt_distinct_subtitle->executeQuery();
                    while ($rs_distinct_subtitle->next()) {
                        $subtitle = trim($rs_distinct_subtitle->getString('subtitle'));
                        $serial_kegiatan = $rs_distinct_subtitle->getString('id');

                        $query_ada_output_subtitle = "select count(*) as jumlah "
                                . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk  "
                                . "where ddk.id_kegiatan = dk.id "
                                . "and dk.id = $serial_kegiatan  and trim(ddk.subtitle) ilike '$subtitle' ";
                        $stmt_ada_output_subtitle = $con->prepareStatement($query_ada_output_subtitle);
                        $rs_ada_output_subtitle = $stmt_ada_output_subtitle->executeQuery();
                        while ($rs_ada_output_subtitle->next()) {
                            $jumlah_output_subtitle = $rs_ada_output_subtitle->getFloat('jumlah');
                        }

                        if ($jumlah_output_subtitle > 0) {
                            $query_output_subtitle = "select * from devplan_output_subtitle_30sept "
                                    . "where id_kegiatan = '$serial_kegiatan' and trim(subtitle) ilike '$subtitle' ";
                            $stmt_output_subtitle = $con->prepareStatement($query_output_subtitle);
                            $rs_output_subtitle = $stmt_output_subtitle->executeQuery();
                            while ($rs_output_subtitle->next()) {
                                $output_narasi = trim($rs_output_subtitle->getString('output_narasi'));
                                $output_volume = trim($rs_output_subtitle->getString('output_volume'));
                                $output_satuan = trim($rs_output_subtitle->getString('output_satuan'));
                            }
                        } else {
                            $output_narasi = '';
                            $output_volume = '';
                            $output_satuan = '';
                        }

                        $query_jumlah_subtitle = "select round(sum(harga*volume)) as total "
                                . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk  "
                                . "where ddk.id_kegiatan = dk.id "
                                . "and dk.id = $serial_kegiatan  and trim(ddk.subtitle) ilike '$subtitle' ";
                        $stmt_jumlah_subtitle = $con->prepareStatement($query_jumlah_subtitle);
                        $rs_jumlah_subtitle = $stmt_jumlah_subtitle->executeQuery();
                        while ($rs_jumlah_subtitle->next()) {
                            $alokasi = $rs_jumlah_subtitle->getFloat('total');
                        }

//                    cek subtitle, kode_kegiatan & unit_id
                        $query_cek_subtitle = "select count(*) as total_ada "
                                . "from " . sfConfig::get('app_default_schema') . ".subtitle_indikator "
                                . "where unit_id = '$kode_skpd' and kegiatan_code = '$kode' and subtitle = '$subtitle' ";
                        $stmt_cek_subtitle = $con->prepareStatement($query_cek_subtitle);
                        $rs_cek_subtitle = $stmt_cek_subtitle->executeQuery();
                        while ($rs_cek_subtitle->next()) {
                            $total_ada = $rs_cek_subtitle->getFloat('total_ada');
                        }
//                    cek subtitle, kode_kegiatan & unit_id

                        if ($total_ada == 0) {
                            $queryInsert = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_indikator "
                                    . "(unit_id, kegiatan_code, subtitle, indikator, nilai, satuan, last_update_time, tahun, lock_subtitle, prioritas, alokasi_dana) "
                                    . "VALUES ('$kode_skpd','$kode','$subtitle', '$output_narasi', '$output_volume','$output_satuan', '$sekarang','" . sfConfig::get('app_tahun_default') . "',FALSE,0, $alokasi) ";
                            $stmtInsert = $con->prepareStatement($queryInsert);
                            $stmtInsert->executeQuery();

                            $queryInsertSubNilai = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_nilai "
                                    . "(unit_id, kegiatan_code, subtitle, nilai_1, nilai_2, nilai_3, total, keterangan) "
                                    . "VALUES ('$kode_skpd','$kode','$subtitle', 2,2,2,200, 'LOLOS') ";
                            $stmtInsertSubNilai = $con->prepareStatement($queryInsertSubNilai);
                            $stmtInsertSubNilai->executeQuery();
                        }
                    }
                }
            }

            $con->commit();
            $this->setFlash('berhasil', 'Tarik Subtitle berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Tarik Subtitle gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeCekNamaJalan() {
        set_time_limit(0);

        $con = Propel::getConnection('gis');
        $con->begin();

        $con_budget = Propel::getConnection();
        $con_budget->begin();

        try {

            $query_list_jalan = "select distinct nama_jalan "
                    . "from master_jalan "
                    . "where nama_jalan not ilike '% RT %' 
                        and nama_jalan not ilike '% RW %' 
                        and nama_jalan not ilike '% gg %' 
                        and nama_jalan not ilike '% gang %' 
                        and nama_jalan not ilike '% RT.%' 
                        and nama_jalan not ilike '% RW.%' 
                        and nama_jalan not ilike '% gg.%' 
                        and nama_jalan not ilike '%I%' 
                        and nama_jalan not ilike '%V%' 
                        and nama_jalan not ilike '%X%' 
                        and nama_jalan ilike 'jl.%' 
                        and nama_jalan not ilike '%1%' 
                        and nama_jalan not ilike '%2%' 
                        and nama_jalan not ilike '%3%' 
                        and nama_jalan not ilike '%4%' 
                        and nama_jalan not ilike '%5%' 
                        and nama_jalan not ilike '%6%' 
                        and nama_jalan not ilike '%7%' 
                        and nama_jalan not ilike '%8%' 
                        and nama_jalan not ilike '%9%' 
                        and nama_jalan not ilike '%blok%' 
                        and nama_jalan not ilike '%+%' 
                        and nama_jalan not ilike '%-%' 
                        and nama_jalan not ilike '%kel.%' 
                        and nama_jalan not ilike '%kec.%' 
                        and nama_jalan not ilike '%(%' 
                        and nama_jalan not ilike '%)%' 
                        and nama_jalan not ilike '%s/d%' 
                        and nama_jalan not ilike '%jalan %' 
                        and nama_jalan not ilike '%paket %'  
                        and nama_jalan not ilike '%akses %' 
                        order by nama_jalan";
            $stmt_list_jalan = $con->prepareStatement($query_list_jalan);
            $rs_list_jalan = $stmt_list_jalan->executeQuery();

            $buang = array(" ", ".");

            while ($rs_list_jalan->next()) {
                $nama_jalan = trim($rs_list_jalan->getString('nama_jalan'));

                $array_baru = explode('Jl.', trim($nama_jalan));

                if (trim($array_baru[1]) != "") {
                    $jalan_baru = 'Jl. ' . ucwords(strtolower(trim(str_replace($buang, "", $array_baru[1]))));

//                    cek jalan
                    $query_cek_jalan = "select count(*) as total "
                            . "from master_lokasi_gmap "
                            . "where nama_lokasi ilike '$jalan_baru' ";
                    $stmt_cek_jalan = $con_budget->prepareStatement($query_cek_jalan);
                    $rs_cek_jalan = $stmt_cek_jalan->executeQuery();
                    while ($rs_cek_jalan->next()) {
                        $total_ada = trim($rs_cek_jalan->getString('total'));
                    }
//                    cek jalan
//                    cari max id jalan                    
                    $query_ambil_id = "select max(id) as max_id "
                            . "from master_lokasi_gmap ";
                    $stmt_ambil_id = $con_budget->prepareStatement($query_ambil_id);
                    $rs_ambil_id = $stmt_ambil_id->executeQuery();
                    while ($rs_ambil_id->next()) {
                        $max_id = trim($rs_ambil_id->getString('max_id'));
                    }
                    $max_id++;
//                    cari max id jalan            

                    if ($total_ada == 0) {
                        $query_insert_jalan = "insert into "
                                . "master_lokasi_gmap "
                                . "values($max_id,'$jalan_baru')";
                        $stmt_insert_jalan = $con_budget->prepareStatement($query_insert_jalan);
                        $stmt_insert_jalan->executeQuery();
                    }
                }
            }

            $con_budget->commit();
            $con->commit();
            $this->setFlash('berhasil', 'Tarik kegiatan berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con_budget->rollback();
            $con->rollback();
            $this->setFlash('gagal', 'Tarik kegiatan gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
        exit();
    }

    public function executeAmbilrincianbappeko() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_komponen = "select * "
                    . "from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk  "
                    . "where ddk.id_kegiatan = dk.id "
                    . "and ddk.volume > 0 ";
            $stmt_komponen = $con->prepareStatement($query_komponen);
            $rs_komponen = $stmt_komponen->executeQuery();
            while ($rs_komponen->next()) {
                $kegiatan_code = $rs_komponen->getString('kode_program') . '.' . $rs_komponen->getString('kode_kegiatan');
                $unit_id = $rs_komponen->getString('unit_id');
                $volume = $rs_komponen->getFloat('volume');
                $detail_name = $rs_komponen->getString('keterangan');
                $komponen_id = $rs_komponen->getString('kode');
                $komponen_name = $rs_komponen->getString('nama');
                $komponen_harga_awal = $rs_komponen->getFloat('harga');
                $satuan = $rs_komponen->getString('satuan');
                $pajak = 0;
                $subtitle = $rs_komponen->getString('subtitle');

                $koefisien = $volume . ' ' . $satuan;

                $queryInsert = "insert into " . sfConfig::get('app_default_schema') . ".bappeko_rincian_detail "
                        . "(unit_id, kegiatan_code, detail_no, tipe, subtitle, komponen_id, komponen_name,detail_name,komponen_harga_awal,pajak, volume, keterangan_koefisien, satuan, tahun, status_hapus ) "
                        . "VALUES ('$unit_id','$kegiatan_code','9999', 'MURNI', '$subtitle', '$komponen_id', '$komponen_name','$detail_name',$komponen_harga_awal,$pajak,$volume,'$koefisien','$satuan','" . sfConfig::get('app_tahun_default') . "',FALSE) ";
                $stmtInsert = $con->prepareStatement($queryInsert);
                $stmtInsert->executeQuery();
            }

            $con->commit();
            $this->setFlash('berhasil', 'Pengambilan Rincian ke Bappeko Rincian Detail berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Pemberian Pengambilan Rincian ke Bappeko Rincian Detail karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadDataPagu() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/waiting_list_pu/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/waiting_list_pu/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 1; $row <= $highR; $row++) {
                $kode_kegiatan = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $nama_kegiatan = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $output = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $target_output = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $pagu = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                $nama_skpd = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
                $unit_id = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());

                $query = "insert into " . sfConfig::get('app_default_schema') . ".pagu_baruru  
                (unit_id, kegiatan_code, kegiatan_name, pagu, output, target_output )  
                values 
                ('$unit_id','$kode_kegiatan','$nama_kegiatan',$pagu,'$output','$target_output')";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil dimasukkan');
            $this->redirect('waitinglist_pu/waitingupload');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            $this->redirect('waitinglist_pu/waitingupload');
        }
    }

    public function executeUploadDataKonektorMusrenbang() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {

                $kode_devplan = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $nama_devplan = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $satuan_devplan = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $harga_devplan = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $komponen_id = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $komponen_rekening = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());

                if ($kode_devplan == '' || $nama_devplan == '' || $satuan_devplan == '' || $harga_devplan == '' || $komponen_id == '' || $komponen_rekening == '') {
                    unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);
                    $this->setFlash('gagal', 'Data tidak terisi semua');
                    return $this->redirect('admin/fiturTambahan');
                }

                $c_cek_musrenbang = new Criteria();
                $c_cek_musrenbang->add(KonektorMusrenbangPeer::KODE_DEVPLAN, $kode_devplan);
                $c_cek_musrenbang->add(KonektorMusrenbangPeer::KOMPONEN_ID, $komponen_id);
                $cek_musrenbang = KonektorMusrenbangPeer::doCount($c_cek_musrenbang);

                if ($cek_musrenbang == 0) {
                    $c_insert_musrenbang = new KonektorMusrenbang();
                    $c_insert_musrenbang->setKodeDevplan($kode_devplan);
                    $c_insert_musrenbang->setNamaDevplan($nama_devplan);
                    $c_insert_musrenbang->setSatuanDevplan($satuan_devplan);
                    $c_insert_musrenbang->setHargaDevplan($harga_devplan);
                    $c_insert_musrenbang->setKomponenId($komponen_id);
                    $c_insert_musrenbang->setKomponenRekening($komponen_rekening);
                    $c_insert_musrenbang->save();
                } else {
                    $c_update_musrenbang = new Criteria();
                    $c_update_musrenbang->add(KonektorMusrenbangPeer::KODE_DEVPLAN, $kode_devplan);
                    $c_update_musrenbang->add(KonektorMusrenbangPeer::KOMPONEN_ID, $komponen_id);
                    $data_konektor_musrenbang = KonektorMusrenbangPeer::doSelectOne($c_update_musrenbang);
                    $data_konektor_musrenbang->setNamaDevplan($nama_devplan);
                    $data_konektor_musrenbang->setSatuanDevplan($satuan_devplan);
                    $data_konektor_musrenbang->setHargaDevplan($harga_devplan);
                    $data_konektor_musrenbang->setKomponenRekening($komponen_rekening);
                    $data_konektor_musrenbang->save();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil dimasukkan');
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeAmbilmusrenbang() {
        set_time_limit(0);

        $con = Propel::getConnection();

        $total_kurang = 0;
        $string_kurang = '';

        $sekarang = date('Y-m-d H:i:s');

//            cek semua kode komponen musrenbang sudah ada di konektor musrenbang
        $query_cek_konektor = ""
                . "(
select distinct kode, nama, harga, satuan
from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk 
where ddk.id_kegiatan = dk.id and ddk.tipe = 'MUSREN' 
and ddk.volume > 0 
and ddk.kode = 'MUSREN_215'
) 
order by nama"
                . "";
        $stmt_cek_konektor = $con->prepareStatement($query_cek_konektor);
        $rs_cek_konektor = $stmt_cek_konektor->executeQuery();
        while ($rs_cek_konektor->next()) {
            $kode_musrenbang = $rs_cek_konektor->getString('kode');
            $nama_musrenbang = $rs_cek_konektor->getString('nama');
            $harga_musrenbang = $rs_cek_konektor->getString('harga');
            $satuan_musrenbang = $rs_cek_konektor->getString('satuan');

            $c_cek_konektor = new Criteria();
            $c_cek_konektor->add(KonektorMusrenbangPeer::KODE_DEVPLAN, $kode_musrenbang);
            $c_cek_konektor->add(KonektorMusrenbangPeer::NAMA_DEVPLAN, $nama_musrenbang);
            $c_cek_konektor->add(KonektorMusrenbangPeer::SATUAN_DEVPLAN, $satuan_musrenbang);
            $c_cek_konektor->add(KonektorMusrenbangPeer::HARGA_DEVPLAN, $harga_musrenbang);
            $c_cek_konektor->addJoin(KonektorMusrenbangPeer::KOMPONEN_ID, KomponenPeer::KOMPONEN_ID);
            $ada_konektor = KonektorMusrenbangPeer::doCount($c_cek_konektor);

            if ($ada_konektor == 0) {
                $total_kurang++;
                $string_kurang = $string_kurang . '|' . $kode_musrenbang . '-' . $nama_musrenbang;
            }
        }
//            cek semua kode komponen musrenbang sudah ada di konektor musrenbang                    
        if ($total_kurang > 0) {
            $this->setFlash('gagal', 'Ditemukan komponen tidak terhubung di konektor_musrenbang ' . $string_kurang);
            return $this->redirect('admin/fiturTambahan');
        } else {
            $con->begin();
            try {
                $query_komponen = ""
                        . "( 
                        select 
                        kode_program, kode_kegiatan, unit_id, subtitle, 
                        harga, satuan, nama, 
                        volume, kode, jalan, nomor, kecamatan, kelurahan, id_musrenbang, spasial, keterangan   
                        from devplan_kegiatan_30sept dk, devplan_detail_kegiatan_30sept ddk 
                        where ddk.id_kegiatan = dk.id and ddk.tipe = 'MUSREN' 
                        and ddk.volume > 0 
                        and ddk.kode = 'MUSREN_215'
                        )
                        order by unit_id, kode_program, kode_kegiatan, subtitle, kode, nama"
                        . "";
                $stmt_komponen = $con->prepareStatement($query_komponen);
                $rs_komponen = $stmt_komponen->executeQuery();
                while ($rs_komponen->next()) {
                    $kegiatan_code = $rs_komponen->getString('kode_program') . '.' . $rs_komponen->getString('kode_kegiatan');
                    $unit_id = $rs_komponen->getString('unit_id');
                    $subtitle = trim($rs_komponen->getString('subtitle'));

//                    cek subtitle ada atau tidak
                    $query_subtitle = "
                        select count(*) as ada_subtitle 
                        from " . sfConfig::get('app_default_schema') . ".subtitle_indikator 
                        where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and trim(subtitle) ilike '$subtitle' ";
                    $stmt_subtitle = $con->prepareStatement($query_subtitle);
                    $rs_subtitle = $stmt_subtitle->executeQuery();
                    while ($rs_subtitle->next()) {
                        $ada_subtitle = ($rs_subtitle->getFloat('ada_subtitle'));
                    }
//                    cek subtitle ada atau tidak

                    if ($ada_subtitle == 0) {
                        $this->setFlash('gagal', 'Tidak ditemukan subtitle : ' . $subtitle . ' pada kegiatan ' . $kegiatan_code . ' dinas ' . $unit_id);
                        return $this->redirect('admin/fiturTambahan');
                    } else {
                        $volume_musrenbang = $rs_komponen->getFloat('volume');
                        $harga_musrenbang = $rs_komponen->getFloat('harga');

                        $lokasi_jalan_musrenbang = $rs_komponen->getString('jalan');
                        $lokasi_no_musrenbang = $rs_komponen->getString('nomor');

                        $lokasi = $lokasi_jalan_musrenbang . ' no. ' . $lokasi_no_musrenbang;

                        $keterangan = $rs_komponen->getString('keterangan');

                        $spasial = $rs_komponen->getString('spasial');

                        $panjang_spasial = strlen($spasial);

                        $posisibukakurung = strpos($spasial, '(');
                        $posisitutupkurung = strpos($spasial, ')');

                        $panjang_jenis = $posisibukakurung;
                        $jenis_spasial = substr($spasial, 0, $panjang_jenis);

                        $panjang_koordinat = ($posisitutupkurung - 1) - $posisibukakurung;
                        $koordinat_spasial = str_replace(' ', '|', substr($spasial, $posisibukakurung + 1, $panjang_koordinat));

//                    "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"geometry\":{\"type\":\"LineString\",\"coordinates\":[[-7.326147634849104,112.71778419616567],[-7.326248726708986,112.71925404670583]]},\"properties\":{\"jarak\":162.67694253300422}}],\"keterangan\":\"\",\"keteranganalamat\":\"Jl. Kebonsari LVK I\"}"
//                    "{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[-7.257752156000136,112.74737186731272],[-7.257709584497372,112.74721629918986],[-7.257544619886153,112.7472779899972],[-7.257571227085607,112.74740673602992]]]},\"properties\":{\"luas\":322.7530615646142}}],\"keterangan\":\"\",\"keteranganalamat\":\"Gedung Jimerto\"}"
//                    "{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Point","coordinates":[-7.2642396,112.74557879999998]}}],"keterangan":"","keteranganalamat":"II"}"

                        $id = $rs_komponen->getString('id_musrenbang');
                        $musrenbang_name = $rs_komponen->getString('nama');
                        $musrenbang_code = $rs_komponen->getString('kode');
                        $musrenbang_satuan = $rs_komponen->getString('satuan');
                        $musrenbang_harga = $rs_komponen->getFloat('harga');

                        $total_anggaran = round($volume_musrenbang * $harga_musrenbang);



                        $c_cek_musrenbang = new Criteria();
                        $c_cek_musrenbang->add(MusrenbangRKAPeer::ID_MUSRENBANG, $id);
                        $c_cek_musrenbang->add(MusrenbangRKAPeer::STATUS_HAPUS, FALSE);
                        $ada_musrenbang = KomponenPeer::doCount($c_cek_musrenbang);
                        if ($ada_musrenbang == 0) {
                            $c_cek_konektor = new Criteria();
                            $c_cek_konektor->add(KonektorMusrenbangPeer::KODE_DEVPLAN, $musrenbang_code);
                            $c_cek_konektor->add(KonektorMusrenbangPeer::NAMA_DEVPLAN, $musrenbang_name);
                            $c_cek_konektor->add(KonektorMusrenbangPeer::SATUAN_DEVPLAN, $musrenbang_satuan);
                            $c_cek_konektor->add(KonektorMusrenbangPeer::HARGA_DEVPLAN, $musrenbang_harga);
                            $c_cek_konektor->addJoin(KonektorMusrenbangPeer::KOMPONEN_ID, KomponenPeer::KOMPONEN_ID);
                            $data_komponen = KomponenPeer::doSelectOne($c_cek_konektor);

                            $c_ambil_konektor = new Criteria();
                            $c_ambil_konektor->add(KonektorMusrenbangPeer::KODE_DEVPLAN, $musrenbang_code);
                            $c_ambil_konektor->add(KonektorMusrenbangPeer::NAMA_DEVPLAN, $musrenbang_name);
                            $c_ambil_konektor->add(KonektorMusrenbangPeer::SATUAN_DEVPLAN, $musrenbang_satuan);
                            $c_ambil_konektor->add(KonektorMusrenbangPeer::HARGA_DEVPLAN, $musrenbang_harga);
                            $data_konektor = KonektorMusrenbangPeer::doSelectOne($c_ambil_konektor);

                            $komponen_id = $data_komponen->getKomponenId();
                            $komponen_name = $data_komponen->getKomponenName();
                            $komponen_harga = $data_komponen->getKomponenHarga();
                            $komponen_satuan = $data_komponen->getSatuan();
                            $komponen_tipe = $data_komponen->getKomponenTipe();
                            $komponen_non_pajak = $data_komponen->getKomponenNonPajak();

                            $komponen_rekening = $data_konektor->getKompoenRekening();

                            $komponen_pajak = 0;
                            if ($komponen_non_pajak == FALSE) {
                                $komponen_pajak = 10;
                                $komponen_itung_pajak = 1.1;
                            } else {
                                $komponen_pajak = 0;
                                $komponen_itung_pajak = 1;
                            }

                            $komponen_volume = $total_anggaran / ($komponen_harga * $komponen_itung_pajak);

                            $c_new_musrenbang = new MusrenbangRKA();
                            $c_new_musrenbang->setUnitId($unit_id);
                            $c_new_musrenbang->setKegiatanCode($kegiatan_code);
                            $c_new_musrenbang->setSubtitle($subtitle);

                            $c_new_musrenbang->setCreatedAt($sekarang);
                            $c_new_musrenbang->setUpdatedAt($sekarang);
                            $c_new_musrenbang->setCreatedBy('admin');
                            $c_new_musrenbang->setUpdatedBy('admin');

                            $c_new_musrenbang->setStatusHapus(FALSE);
                            $c_new_musrenbang->setIdMusrenbang($id);

                            $c_new_musrenbang->setKomponenName($komponen_name);

                            if ($komponen_tipe == 'SHSD' || $komponen_tipe == 'EST') {
                                $c_new_musrenbang->setDetailName($keterangan);
                            } else {
                                $c_new_musrenbang->setDetailName($lokasi);
                                $c_new_musrenbang->setKoordinatMusrenbang($koordinat_spasial);
                            }

                            $c_new_musrenbang->setKomponenRekening($komponen_rekening);

                            $c_new_musrenbang->setKomponenTipe($komponen_tipe);
                            $c_new_musrenbang->setKomponenId($komponen_id);
                            $c_new_musrenbang->setSatuan($komponen_satuan);
                            $c_new_musrenbang->setTahunMusrenbang(sfConfig::get('app_tahun_default'));

                            $c_new_musrenbang->setKomponenHarga($komponen_harga);
                            $c_new_musrenbang->setKomponenPajak($komponen_pajak);
                            $c_new_musrenbang->setVolume($komponen_volume);
                            $c_new_musrenbang->setAnggaran($total_anggaran);

                            $c_new_musrenbang->save();
                        }
                    }
                }

//                ambil yang tipenya gak FISIK (SHSD buat digabung jadi 1)
                $query_group_by_ssh_musrenbang = " select unit_id, kegiatan_code, subtitle, komponen_id, komponen_name, komponen_tipe, detail_name, komponen_rekening, "
                        . "satuan, komponen_harga, komponen_pajak, sum(anggaran) as total_anggaran "
                        . "from " . sfConfig::get('app_default_schema') . ".musrenbang_rka  "
                        . "where status_hapus = false and komponen_tipe <> 'FISIK' "
                        . "group by unit_id, kegiatan_code, subtitle, komponen_id, komponen_name, komponen_tipe, detail_name, komponen_rekening, satuan, komponen_harga, komponen_pajak "
                        . "order by unit_id, kegiatan_code, subtitle, komponen_id, komponen_name, komponen_tipe, detail_name, komponen_rekening, satuan, komponen_harga, komponen_pajak ";
                $stmt_gabung_per_ssh = $con->prepareStatement($query_group_by_ssh_musrenbang);
                $rs_gabung_per_ssh = $stmt_gabung_per_ssh->executeQuery();
                while ($rs_gabung_per_ssh->next()) {
                    $unit_id_gabung = $rs_gabung_per_ssh->getString('unit_id');
                    $kegiatan_code_gabung = $rs_gabung_per_ssh->getString('kegiatan_code');
                    $subtitle_gabung = $rs_gabung_per_ssh->getString('subtitle');
                    $komponen_id_gabung = $rs_gabung_per_ssh->getString('komponen_id');
                    $komponen_name_gabung = $rs_gabung_per_ssh->getString('komponen_name');
                    $komponen_tipe_gabung = $rs_gabung_per_ssh->getString('komponen_tipe');
                    $detail_name_gabung = $rs_gabung_per_ssh->getString('detail_name');
                    $komponen_rekening_gabung = $rs_gabung_per_ssh->getString('komponen_rekening');
                    $satuan_gabung = $rs_gabung_per_ssh->getString('satuan');
                    $komponen_harga_gabung = $rs_gabung_per_ssh->getString('komponen_harga');
                    $komponen_pajak_gabung = $rs_gabung_per_ssh->getString('komponen_pajak');
                    if ($komponen_pajak_gabung == 10) {
                        $komponen_itung_pajak_gabung = 1.1;
                    } else {
                        $komponen_itung_pajak_gabung = 1;
                    }
                    $total_anggaran_gabung = $rs_gabung_per_ssh->getFloat('total_anggaran');

                    $komponen_volume_gabung = $total_anggaran_gabung / ($komponen_harga_gabung * $komponen_itung_pajak_gabung);

                    $keterangan_koefisien_gabung = $komponen_volume_gabung . ' ' . $satuan_gabung;

//                    cari detail_no terakhir rincian_detail
                    $detail_no = 0;
                    $queryDetailNo = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where unit_id='$unit_id_gabung' and kegiatan_code='$kegiatan_code_gabung'";
                    $stmt = $con->prepareStatement($queryDetailNo);
                    $rs_max = $stmt->executeQuery();
                    while ($rs_max->next()) {
                        $detail_no = $rs_max->getString('nilai');
                    }
                    $detail_no+=1;
//                    cari detail_no terakhir rincian_detail
//                    cari kode_sub terakhir rka_member                    
                    $sql_rka_member = "select max(kode_sub) as kode_sub "
                            . "from " . sfConfig::get('app_default_schema') . ".rka_member "
                            . "where kode_sub ilike 'RKAM%'";
                    $stmt = $con->prepareStatement($sql_rka_member);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $kodesub = $rs->getString('kode_sub');
                    }
                    $kode = substr($kodesub, 4, 5);
                    $kode+=1;
                    if ($kode < 10) {
                        $kodesub = 'RKAM0000' . $kode;
                    } elseif ($kode < 100) {
                        $kodesub = 'RKAM000' . $kode;
                    } elseif ($kode < 1000) {
                        $kodesub = 'RKAM00' . $kode;
                    } elseif ($kode < 10000) {
                        $kodesub = 'RKAM0' . $kode;
                    } elseif ($kode < 100000) {
                        $kodesub = 'RKAM' . $kode;
                    }
//                    cari kode_sub terakhir rka_member                                                            
                    $subsubtitle = $komponen_name_gabung . ' ' . $detail_name_gabung . ' (Musrenbang)';
//                    insert rincian_detail                    
                    $query_insert_rd = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, last_update_user, last_update_time, tahap_edit,tahun, is_musrenbang)
                                values
                                ('" . $kegiatan_code_gabung . "', '" . $komponen_tipe_gabung . "', " . $detail_no . ", '" . $komponen_rekening_gabung . "', '" . $komponen_id_gabung . "', '" . $detail_name_gabung . "', " . $komponen_volume_gabung . ", '" . $keterangan_koefisien_gabung . "', '" . $subtitle . "',
                                    " . $komponen_harga_gabung . ", " . $komponen_harga_gabung . ",'" . $komponen_name_gabung . "', '" . $satuan_gabung . "', " . $komponen_pajak_gabung . ",'" . $unit_id_gabung . "','" . $kodesub . "', '" . $subsubtitle . "',
                                        'admin', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "',TRUE)";
                    $stmt_insert_rd = $con->prepareStatement($query_insert_rd);
                    $stmt_insert_rd->executeQuery();
//                    insert rincian_detail
//                    insert rka_member                    
                    $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".rka_member 
                        (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun ) 
                        values ('$kodesub','$unit_id_gabung','$kegiatan_code_gabung',$detail_no,'$komponen_id_gabung','$komponen_name_gabung','$detail_name_gabung','$komponen_rekening_gabung','" . sfConfig::get('app_tahun_default') . "')";
                    $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                    $stmt2->executeQuery();
//                    insert rka_member                    
//                    update musrenbang_rka isi detail_no                    
                    $queryUpdateMusrenbang = " update " . sfConfig::get('app_default_schema') . ".musrenbang_rka "
                            . "set detail_no = $detail_no "
                            . "where unit_id = '$unit_id_gabung' and kegiatan_code = '$kegiatan_code_gabung' and subtitle = '$subtitle_gabung' "
                            . "and komponen_id = '$komponen_id_gabung' and komponen_name = '$komponen_name_gabung' and komponen_tipe = '$komponen_tipe_gabung' "
                            . "and detail_name = '$detail_name_gabung' and komponen_rekening = '$komponen_rekening_gabung' "
                            . "and satuan = '$satuan_gabung' and komponen_harga = $komponen_harga_gabung and komponen_pajak = $komponen_pajak_gabung ";
                    $stmt2 = $con->prepareStatement($queryUpdateMusrenbang);
                    $stmt2->executeQuery();
//                    update musrenbang_rka isi detail_no                    
                }
//                ambil yang tipenya gak FISIK (SHSD buat digabung jadi 1)
//                
//                -----------------------------------
//                
//                ambil yang tipenya FISIK (FISIK dipisah jadi satu-satu)    
                $query_group_by_fisik_musrenbang = " select unit_id, kegiatan_code, subtitle, komponen_id, komponen_name, komponen_tipe, detail_name, komponen_rekening, "
                        . "satuan, komponen_harga, komponen_pajak, anggaran as total_anggaran, id "
                        . "from " . sfConfig::get('app_default_schema') . ".musrenbang_rka  "
                        . "where status_hapus = false and komponen_tipe = 'FISIK' "
                        . "order by unit_id, kegiatan_code, subtitle, komponen_id, komponen_name, komponen_tipe, detail_name, komponen_rekening, satuan, komponen_harga, komponen_pajak ";
                $stmt_gabung_per_fisik = $con->prepareStatement($query_group_by_fisik_musrenbang);
                $rs_gabung_per_fisik = $stmt_gabung_per_fisik->executeQuery();
                while ($rs_gabung_per_fisik->next()) {
                    $unit_id_fisik = $rs_gabung_per_fisik->getString('unit_id');
                    $kegiatan_code_fisik = $rs_gabung_per_fisik->getString('kegiatan_code');
                    $subtitle_fisik = $rs_gabung_per_fisik->getString('subtitle');
                    $komponen_id_fisik = $rs_gabung_per_fisik->getString('komponen_id');
                    $komponen_name_fisik = $rs_gabung_per_fisik->getString('komponen_name');
                    $komponen_tipe_fisik = $rs_gabung_per_fisik->getString('komponen_tipe');
                    $detail_name_fisik = $rs_gabung_per_fisik->getString('detail_name');
                    $komponen_rekening_fisik = $rs_gabung_per_fisik->getString('komponen_rekening');
                    $satuan_fisik = $rs_gabung_per_fisik->getString('satuan');
                    $komponen_harga_fisik = $rs_gabung_per_fisik->getString('komponen_harga');
                    $komponen_pajak_fisik = $rs_gabung_per_fisik->getString('komponen_pajak');

                    $id_musrenbang_rka = $rs_gabung_per_fisik->getString('id');

                    if ($komponen_pajak_fisik == 10) {
                        $komponen_itung_pajak_fisik = 1.1;
                    } else {
                        $komponen_itung_pajak_fisik = 1;
                    }
                    $total_anggaran_fisik = $rs_gabung_per_fisik->getFloat('total_anggaran');

                    $komponen_volume_fisik = $total_anggaran_fisik / ($komponen_harga_fisik * $komponen_itung_pajak_fisik);

                    $keterangan_koefisien_fisik = $komponen_volume_fisik . ' ' . $satuan_fisik;

//                    cari detail_no terakhir rincian_detail
                    $detail_no = 0;
                    $queryDetailNo = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where unit_id='$unit_id_fisik' and kegiatan_code='$kegiatan_code_fisik'";
                    $stmt = $con->prepareStatement($queryDetailNo);
                    $rs_max = $stmt->executeQuery();
                    while ($rs_max->next()) {
                        $detail_no = $rs_max->getString('nilai');
                    }
                    $detail_no+=1;
//                    cari detail_no terakhir rincian_detail
//                    cari kode_sub terakhir rka_member                    
                    $sql_rka_member = "select max(kode_sub) as kode_sub "
                            . "from " . sfConfig::get('app_default_schema') . ".rka_member "
                            . "where kode_sub ilike 'RKAM%'";
                    $stmt = $con->prepareStatement($sql_rka_member);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $kodesub = $rs->getString('kode_sub');
                    }
                    $kode = substr($kodesub, 4, 5);
                    $kode+=1;
                    if ($kode < 10) {
                        $kodesub = 'RKAM0000' . $kode;
                    } elseif ($kode < 100) {
                        $kodesub = 'RKAM000' . $kode;
                    } elseif ($kode < 1000) {
                        $kodesub = 'RKAM00' . $kode;
                    } elseif ($kode < 10000) {
                        $kodesub = 'RKAM0' . $kode;
                    } elseif ($kode < 100000) {
                        $kodesub = 'RKAM' . $kode;
                    }
//                    cari kode_sub terakhir rka_member                                                            
                    $subsubtitle = $komponen_name_fisik . ' (' . $detail_name_fisik . ') (Musrenbang)';

//                    insert rincian_detail                    
                    $query_insert_rd = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, last_update_user, last_update_time, tahap_edit,tahun, is_musrenbang)
                                values
                                ('" . $kegiatan_code_fisik . "', '" . $komponen_tipe_fisik . "', " . $detail_no . ", '" . $komponen_rekening_fisik . "', '" . $komponen_id_fisik . "', '" . $detail_name_fisik . "', " . $komponen_volume_fisik . ", '" . $keterangan_koefisien_fisik . "', '" . $subtitle . "',
                                    " . $komponen_harga_fisik . ", " . $komponen_harga_fisik . ",'" . $komponen_name_fisik . "', '" . $satuan_fisik . "', " . $komponen_pajak_fisik . ",'" . $unit_id_fisik . "','" . $kodesub . "', '" . $subsubtitle . "',
                                        'admin', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "',TRUE)";
                    $stmt_insert_rd = $con->prepareStatement($query_insert_rd);
                    $stmt_insert_rd->executeQuery();
//                    insert rincian_detail
//                    insert rka_member                    
                    $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".rka_member 
                        (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun ) 
                        values ('$kodesub','$unit_id_fisik','$kegiatan_code_fisik',$detail_no,'$komponen_id_fisik','$komponen_name_fisik','$detail_name_fisik','$komponen_rekening_fisik','" . sfConfig::get('app_tahun_default') . "')";
                    $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                    $stmt2->executeQuery();
//                    insert rka_member                    
//                    update musrenbang_rka isi detail_no                    
                    $queryUpdateMusrenbang = " update " . sfConfig::get('app_default_schema') . ".musrenbang_rka "
                            . "set detail_no = $detail_no "
                            . "where id = $id_musrenbang_rka ";
                    $stmt2 = $con->prepareStatement($queryUpdateMusrenbang);
                    $stmt2->executeQuery();
//                    update musrenbang_rka isi detail_no      
                }
//                ambil yang tipenya FISIK (FISIK dipisah jadi satu-satu)                                


                $con->commit();
                $this->setFlash('berhasil', 'Pengambilan Musrenbang ke Rincian Detail');
                return $this->redirect('admin/fiturTambahan');
            } catch (Exception $exc) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
                return $this->redirect('admin/fiturTambahan');
            }
        }
    }

    public function executeUploadDataSubtitle() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {
                $unit_id = str_replace('*', '', trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                $kode_program = str_replace('*', '', trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                $kode_kegiatan = str_replace('*', '', trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                $nama_kegiatan = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $output_narasi_kegiatan = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $output_volume_kegiatan = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $output_satuan_kegiatan = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                $subtitle = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
                $output_narasi_subtitle = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                $output_volume_subtitle = trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
                $output_satuan_subtitle = trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());

                $kode_kegiatan_fix = $kode_program . '.' . $kode_kegiatan;
                $output_kegiatan_fix = $output_narasi_kegiatan . '|' . $output_volume_kegiatan . '|' . $output_satuan_kegiatan;

                $query = "insert into " . sfConfig::get('app_default_schema') . ".subtitle_30sept  
                (unit_id, kode_kegiatan, nama_kegiatan, output_kegiatan,subtitle,indikator_subtitle,nilai_subtitle, satuan_subtitle)  
                values 
                ('$unit_id','$kode_kegiatan_fix','$nama_kegiatan','$output_kegiatan_fix','$subtitle','$output_narasi_subtitle','$output_volume_subtitle','$output_satuan_subtitle')";
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil dimasukkan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    function executeGantiTertentuByKomponen() {

        $id_komponen = $this->getRequestParameter('id');
        $id_komponen_baru = $this->getRequestParameter('komponen_id');
        $harga_komponen = $this->getRequestParameter('harga');
        //$satuan_baru = $this->getRequestParameter('satuan');
        $pajak_komponen = $this->getRequestParameter('pajak');

         $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $id_komponen_baru);
                $rs_komponen = KomponenPeer::doSelectOne($c);
                if ($rs_komponen) {
                    $komponen_name_baru = $rs_komponen->getKomponenName();
                    $harga_komponen = $rs_komponen->getKomponenHargaBulat();
                    $satuan_baru = $rs_komponen->getSatuan();

                }


        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "select * from ebudget.dinas_rincian_detail 
                where status_hapus = false and komponen_id = '$id_komponen' ";
               // and (komponen_harga_awal <> '$harga_komponen' or pajak <> $pajak_komponen) ";
           

             // $query1 = "select * from ebudget.dinas_rincian_detail 
             //    where status_hapus = false and komponen_id = '$id_komponen' and  unit_id ilike '12%' and kegiatan_code in 
             //    (select kode_kegiatan from ebudget.dinas_master_kegiatan where unit_id ilike '12%' and nama_kegiatan ilike '%Kelurahan%') 
             //     ";

            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');                
                $komponen_id = $rs1->getString('komponen_id');
                $komponen_name = $rs1->getString('komponen_name');
                $satuan = $rs1->getString('satuan');
                $volume = $rs1->getString('volume');
                $pajak = $rs1->getString('pajak');
                $komponen_harga_awal = $rs1->getString('komponen_harga_awal');
                $keterangan_koefisien = $rs1->getString('keterangan_koefisien');
                $nilai_anggaran = $rs1->getString('nilai_anggaran');

                $harga_baru = $harga_komponen;

                if ($pajak_komponen == 0) {
                    $itung_pajak_baru = 1;
                } else {
                    $itung_pajak_baru = 1.1;
                }

                $komponen_volume_baru = $nilai_anggaran / ($harga_baru * $itung_pajak_baru);
                //$komponen_volume_baru = 1;
                $keterangan_koefisien_baru = $komponen_volume_baru . ' ' . $satuan;

                // $keterangan_koefisien_baru = $volume . ' ' . $satuan_baru;

                // $query3 = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                //         . "set komponen_id='$id_komponen_baru',volume=$komponen_volume_baru, komponen_harga=$harga_baru, satuan='$satuan_baru',komponen_harga_awal=$harga_baru, keterangan_koefisien = '$keterangan_koefisien_baru', pajak = $pajak_komponen "
                //         . "where status_hapus = false "
                //         . "and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";


                // $query3 = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                //         . "set komponen_id='$id_komponen_baru',volume=$komponen_volume_baru, komponen_harga=$harga_baru, keterangan_koefisien='$keterangan_koefisien_baru',satuan='$satuan_baru',komponen_harga_awal=$harga_baru, pajak = $pajak_komponen "
                //         . "where status_hapus = false "
                //         . "and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";


                $query3 = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set komponen_id='$id_komponen_baru', komponen_harga=$harga_baru,volume=$komponen_volume_baru, keterangan_koefisien='$keterangan_koefisien_baru', komponen_harga_awal=$harga_baru, pajak = $pajak_komponen"
                        . "where status_hapus = false "
                        . "and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt3 = $con->prepareStatement($query3);
                $stmt3->executeQuery();

                // $query4 = "update " . sfConfig::get('app_default_schema') . ".murni_bukubiru_rincian_detail "
                //         . "set komponen_id='$id_komponen_baru', komponen_harga=$harga_baru,volume=$komponen_volume_baru, keterangan_koefisien='$keterangan_koefisien_baru', komponen_harga_awal=$harga_baru, pajak = $pajak_komponen"
                //         . "where status_hapus = false "
                //         . "and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                // //echo $query3; $con->rollback(); exit;
                // $stmt4 = $con->prepareStatement($query4);
                // $stmt4->executeQuery();

                
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk komponen ' . $komponen_name . ' menjadi harga ' . $harga_komponen);
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    

    function executeUploadGantiTertentuByKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'komponen_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 2; $row <= $highR; $row++) {
                $id_komponen = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $pajak_komponen = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());        
                $harga_komponen = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());

                //update SHSD dan komponen
                if ($pajak_komponen == 0) {
                    $non_pajak = 'TRUE';
                    $itung_pajak_baru = 1;
                } else {
                    $non_pajak = 'FALSE';
                    $itung_pajak_baru = 1.1;
                }
                $c = new Criteria();
                // $c->add(ShsdPeer::SHSD_ID, $id_komponen);
                // if ($shsd = ShsdPeer::doSelectOne($c)) {
                //     $query = " update " . sfConfig::get('app_default_schema') . ".shsd "
                //             . " set shsd_harga=$harga_komponen, non_pajak=$non_pajak "
                //             . " where shsd_id='$id_komponen'";
                //     $stmt = $con->prepareStatement($query);
                //     $stmt->executeQuery();
                // } else {
                //     $con->rollback();
                //     $this->setFlash('gagal', 'Gagal karena ' . $id_komponen . ' tidak ditemukan di tabel SHSD');
                //     return $this->redirect('admin/fiturTambahan');
                // }
                // $c = new Criteria();
                // $c->add(KomponenPeer::KOMPONEN_ID, $id_komponen);
                // if ($shsd = KomponenPeer::doSelectOne($c)) {
                //     $query = " update " . sfConfig::get('app_default_schema') . ".komponen "
                //             . " set komponen_harga=$harga_komponen, komponen_non_pajak=$non_pajak "
                //             . " where shsd_id='$id_komponen'";
                //     $stmt = $con->prepareStatement($query);
                //     $stmt->executeQuery();
                // } else {
                //     $con->rollback();
                //     $this->setFlash('gagal', 'Gagal karena ' . $id_komponen . ' tidak ditemukan di tabel Komponen');
                //     return $this->redirect('admin/fiturTambahan');
                // }

                //update ke RKA
                $harga_baru = $harga_komponen;
                $query1 = "select * from ebudget.dinas_rincian_detail 
                        where status_hapus=false and komponen_id='$id_komponen' 
                        and (komponen_harga_awal<>$harga_komponen or pajak<>$pajak_komponen) ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $unit_id = $rs1->getString('unit_id');
                    $kegiatan_code = $rs1->getString('kegiatan_code');
                    $detail_no = $rs1->getString('detail_no');
                    $satuan = $rs1->getString('satuan');
                    $nilai_anggaran = $rs1->getString('nilai_anggaran');

                    $komponen_volume_baru = $nilai_anggaran / ($harga_baru * $itung_pajak_baru);

                    $keterangan_koefisien_baru = $komponen_volume_baru . ' ' . $satuan;

                    $query3 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . " set volume=$komponen_volume_baru, komponen_harga=$harga_baru, komponen_harga_awal=$harga_baru, keterangan_koefisien = '$keterangan_koefisien_baru', pajak = '$pajak_komponen' "
                            . " where status_hapus = false "
                            . " and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengubah harga komponen ');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    function executeUploadGantiNamaKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'komponen_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 0; $row <= $highR; $row++) {
                $id_komponen_lama = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $id_komponen_baru = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());        
                $nama_komponen_baru = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
    
                //update ke RKA                
                $query1 = "select * from ebudget.dinas_rincian_detail 
                        where status_hapus=false and komponen_id='$id_komponen_lama' 
                        ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $unit_id = $rs1->getString('unit_id');
                    $kegiatan_code = $rs1->getString('kegiatan_code');
                    $detail_no = $rs1->getString('detail_no');
                    
                    $query3 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . " set komponen_id='$id_komponen_baru', komponen_name='$nama_komponen_baru' "
                            . " where status_hapus = false "
                            . " and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengubah nama komponen ');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    function executeUploadGantiTipe2Komponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'komponen_tipe2' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) 
            {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 0; $row <= $highR; $row++) {
                $id_komponen = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $komponen_tipe2 = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());        
                
                //update ke RKA                  
                $query1 = "select * from ebudget.komponen
                        where komponen_id='$id_komponen' ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $komponen_id = $rs1->getString('komponen_id');
                    
                    $query2 = " update " . sfConfig::get('app_default_schema') . ".komponen "
                            . " set komponen_tipe2='$komponen_tipe2' "
                            . " where komponen_id='$komponen_id' ";                            
                    $stmt2 = $con->prepareStatement($query2);
                    $stmt2->executeQuery();

                    $query3 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . " set tipe2= ebudget.komponen.komponen_tipe2 
                                from ebudget.komponen
                                where ebudget.dinas_rincian_detail.komponen_id=ebudget.komponen.komponen_id 
                                and ebudget.dinas_rincian_detail.tipe2 <> ebudget.komponen.komponen_tipe2
                                and ebudget.dinas_rincian_detail.status_hapus=false                               
                                and ebudget.dinas_rincian_detail.komponen_id='$komponen_id'";
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();

                    $query4 = " update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . " set tipe2= ebudget.komponen.komponen_tipe2 
                                from ebudget.komponen
                                where ebudget.rincian_detail.komponen_id=ebudget.komponen.komponen_id 
                                and ebudget.rincian_detail.tipe2 <> ebudget.komponen.komponen_tipe2
                                and ebudget.rincian_detail.status_hapus=false                                 
                                and ebudget.rincian_detail.komponen_id='$komponen_id'";
                    $stmt4 = $con->prepareStatement($query4);
                    $stmt4->executeQuery();

                    $query5 = " update " . sfConfig::get('app_default_schema') . ".rincian_detail_bp "
                            . " set tipe2= ebudget.komponen.komponen_tipe2 
                                from ebudget.komponen
                                where ebudget.rincian_detail_bp.komponen_id=ebudget.komponen.komponen_id 
                                and ebudget.rincian_detail_bp.tipe2 <> ebudget.komponen.komponen_tipe2
                                and ebudget.rincian_detail_bp.status_hapus=false
                                and ebudget.rincian_detail_bp.komponen_id='$komponen_id'";
                    $stmt5 = $con->prepareStatement($query5);
                    $stmt5->executeQuery();

                    // $query6 = " update " . sfConfig::get('app_default_schema') . ".murni_rincian_detail "
                    //         . " set tipe2= ebudget.komponen.komponen_tipe2 
                    //             from ebudget.komponen
                    //             where ebudget.murni_rincian_detail.komponen_id=ebudget.komponen.komponen_id 
                    //             and ebudget.murni_rincian_detail.tipe2 <> ebudget.komponen.komponen_tipe2
                    //             and ebudget.murni_rincian_detail.status_hapus=false                              
                    //             and ebudget.murni_rincian_detail.komponen_id='$komponen_id'";
                    // $stmt6 = $con->prepareStatement($query6);
                    // $stmt6->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengubah komponen tipe 2 dan tipe 2 di tabel rincian detail ');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    function executeUploadGantiCatatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'catatan_pembahasan' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) 
            {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 0; $row <= $highR; $row++) {
                $id_sub_kegiatan = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $catatan_pembahasan = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()); 
                $detail_kegiatan = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()); 

                //update nama user
                // $query1 = "select * from master_user_v2
                //         where user_id='$id_sub_kegiatan' ";
                // $stmt1 = $con->prepareStatement($query1);
                // $rs1 = $stmt1->executeQuery();
                // while ($rs1->next()) {

                //      $query2 = " update master_user_v2 "
                //             . " set user_name='$catatan_pembahasan' "
                //             . " where user_id='$id_sub_kegiatan' ";                            
                //     $stmt2 = $con->prepareStatement($query2);
                //     $stmt2->executeQuery();

                // }
                // if (!$rs1) {
                //  $this->setFlash('gagal', 'Gagal karena tidak ditemukan User Id ' .  $id_sub_kegiatan . ' pada tabel user');
                //     return $this->redirect('admin/fiturTambahan');
                // }

                //update kode rekening
                // $query1 = "select * from ebudget.rekening
                //         where rekening_name='$id_sub_kegiatan' ";
                // $stmt1 = $con->prepareStatement($query1);
                // $rs1 = $stmt1->executeQuery();
                // while ($rs1->next()) {

                //      $query2 = " update ebudget.rekening "
                //             . " set rekening_code='$catatan_pembahasan' "
                //             . " where rekening_name='$id_sub_kegiatan' ";                            
                //     $stmt2 = $con->prepareStatement($query2);
                //     $stmt2->executeQuery();

                // }
                // if (!$rs1) {
                //  $this->setFlash('gagal', 'Gagal karena tidak ditemukan nama rekening ' .  $id_sub_kegiatan . ' pada tabel rekening');
                //     return $this->redirect('admin/fiturTambahan');
                // }

                //update nama pd
                // $query1 = "select * from unit_kerja
                //         where unit_id='$id_sub_kegiatan' ";
                // $stmt1 = $con->prepareStatement($query1);
                // $rs1 = $stmt1->executeQuery();
                // while ($rs1->next()) {

                //      $query2 = " update unit_kerja "
                //             . " set kode_permen='$catatan_pembahasan' "
                //             . " where unit_id='$id_sub_kegiatan' ";                            
                //     $stmt2 = $con->prepareStatement($query2);
                //     $stmt2->executeQuery();

                // }
                // if (!$rs1) {
                //  $this->setFlash('gagal', 'Gagal karena tidak ditemukan PD Id ' .  $id_sub_kegiatan . ' pada tabel unit kerja');
                //     return $this->redirect('admin/fiturTambahan');
                // }


                //update komponen tipe2 dan tipe
                // $query1 = "select * from ebudget.komponen
                //         where komponen_id='$id_sub_kegiatan' ";
                // $stmt1 = $con->prepareStatement($query1);
                // $rs1 = $stmt1->executeQuery();
                // while ($rs1->next()) {

                //      $query2 = " update ebudget.komponen "
                //             . " set komponen_tipe2='$catatan_pembahasan' "
                //             . " where komponen_id='$id_sub_kegiatan'";                            
                //     $stmt2 = $con->prepareStatement($query2);
                //     $stmt2->executeQuery();


                //     $query21 = " update ebudget.dinas_rincian_detail "
                //             . " set tipe2='$catatan_pembahasan' "
                //             . " where komponen_id='$id_sub_kegiatan'";                            
                //     $stmt21 = $con->prepareStatement($query21);
                //     $stmt21->executeQuery();


                // }
                // if (!$rs1) {
                //  $this->setFlash('gagal', 'Gagal karena tidak ditemukan Komponen Id ' .  $id_sub_kegiatan . ' pada tabel Komponen');
                //     return $this->redirect('admin/fiturTambahan');
                // }





    
                // //update ke RKA                  
                $query1 = "select * from ebudget.dinas_master_kegiatan
                        where kode_kegiatan='$id_sub_kegiatan' ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $id_sub_kegiatan = $rs1->getString('kode_kegiatan');

                    //asli
                    $query2 = " update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
                            . " set user_id='$catatan_pembahasan' "
                            . " where kode_kegiatan='$id_sub_kegiatan' ";                            
                    $stmt2 = $con->prepareStatement($query2);
                    $stmt2->executeQuery();

                    // update cdetail_name
                    // $query2 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail"
                    //         . " set detail_name='$catatan_pembahasan' "
                    //         . " where detail_kegiatan='$detail_kegiatan' ";                            
                    // $stmt2 = $con->prepareStatement($query2);
                    // $stmt2->executeQuery();


                    // update anggaran_rasionalisasi
                    // $query2 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail"
                    //         . " set anggaran_rasionalisasi=$catatan_pembahasan "
                    //         . " where detail_kegiatan='$detail_kegiatan' ";                            
                    // $stmt2 = $con->prepareStatement($query2);
                    // $stmt2->executeQuery();

                    // $query21 = " update " . sfConfig::get('app_default_schema') . ".rincian_detail"
                    //         . " set anggaran_rasionalisasi=$catatan_pembahasan "
                    //         . " where detail_kegiatan='$detail_kegiatan' ";                            
                    // $stmt21 = $con->prepareStatement($query21);
                    // $stmt21->executeQuery();

                    // $query22 = " update " . sfConfig::get('app_default_schema') . ".rincian_detail_bp"
                    //         . " set anggaran_rasionalisasi=$catatan_pembahasan "
                    //         . " where detail_kegiatan='$detail_kegiatan' ";                            
                    // $stmt22 = $con->prepareStatement($query22);
                    // $stmt22->executeQuery();


                    

                    // $query3 = " update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                    //         . " set catatan_pembahasan='$catatan_pembahasan' "
                    //         . " where kode_kegiatan='$id_sub_kegiatan' ";   
                    // $stmt3 = $con->prepareStatement($query3);
                    // $stmt3->executeQuery();

                    
                    // $query5 = " update " . sfConfig::get('app_default_schema') . ".master_kegiatan_bp "
                    //         . " set catatan_pembahasan='$catatan_pembahasan' "
                    //         . " where kode_kegiatan='$id_sub_kegiatan' "; 
                    // $stmt5 = $con->prepareStatement($query5);
                    // $stmt5->executeQuery();

                    
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengupadate catatan_pembahasan di tabel dinas_master_kegiatan');
                       return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    function executeUploadSaveBa() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'catatan_ba' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) 
            {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 0; $row <= $highR; $row++) {               
                
            $unit_id = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
            $kode_kegiatan = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());  
            $query = "select is_pernah_rka, tahap from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            $rs_tahap = $stmt->executeQuery();
            if ($rs_tahap->next()) {
                $tahap = $rs_tahap->getString('tahap');
                $pernahrka = $rs_tahap->getBoolean('is_pernah_rka');
            } else {
                $tahap = 'murni';
                $pernahrka = false;
            }

            $tabel_semula = 'rincian_detail';
            if ($pernahrka) {
                $tabel_semula = 'prev_rincian_detail';
            }

            $query = "select max(id) as id from " . sfConfig::get('app_default_schema') . ".berita_acara";
            $stmt = $con->prepareStatement($query);
            $rs_id = $stmt->executeQuery();
            if ($rs_id->next()) {
                $id_baru = $rs_id->getInt('id') + 1;
            } else {
                $id_baru = 1;
            }
            $token = md5(rand());

            $berita_acara = new BeritaAcara();
            $berita_acara->setId($id_baru);
            $berita_acara->setUnitId($unit_id);
            $berita_acara->setKegiatanCode($kode_kegiatan);
            $berita_acara->setToken($token);
            $berita_acara->setWaktu(date('Y-m-d H:i:s'));
            $berita_acara->setTahap($tahap);
            $berita_acara->save();

            $where_anggaran = " and detail2.nilai_anggaran>0 ";
            $where_anggaran2 = " ";
            $c_kegiatan_anggaran = new Criteria();
            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $c_kegiatan_anggaran->add(DinasMasterKegiatanPeer::IS_PERNAH_RKA, TRUE);
            if (DinasMasterKegiatanPeer::doSelectOne($c_kegiatan_anggaran)) {
                $where_anggaran = "";
                $where_anggaran2 = "or detail2.nilai_anggaran<>(select nilai_anggaran from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and detail_no=detail2.detail_no) ";
            }
            $query = "(
            SELECT
                    detail.rekening_code,detail2.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                    detail2.komponen_harga_awal as detail_harga2,
                    detail2.pajak as pajak2,
                    detail2.komponen_id as komponen_id2,
                    detail2.detail_name as detail_name2,
                    detail2.komponen_name as komponen_name2,
                    detail2.volume as volume2,
                    detail2.subtitle as subtitle_name2,
                    detail2.satuan as detail_satuan2,
                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                    detail2.detail_no as detail_no2,
                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                    (detail2.nilai_anggaran) as hasil_kali2,

                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                    detail.komponen_harga_awal as detail_harga,
                    detail.pajak as pajak,
                    detail.komponen_id as komponen_id,
                    detail.detail_name as detail_name,
                    detail.komponen_name as komponen_name,
                    detail.volume as volume,
                    detail.subtitle as subtitle_name,
                    detail.satuan as detail_satuan,
                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                    detail.detail_no as detail_no,
                    detail.volume * detail.komponen_harga_awal as hasil,
                    (detail.nilai_anggaran) as hasil_kali,
                    detail2.detail_name as detail_name_rd,
                    detail2.sub as sub2,
                    detail.sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    detail2.is_bagian_hukum_setuju,
                    detail2.is_inspektorat_setuju,
                    detail2.is_badan_kepegawaian_setuju,
                    detail2.is_lppa_setuju,
                    detail2.is_bagian_organisasi_setuju,
                    detail.sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    detail.is_covid,
                    detail2.is_covid as is_covid2

            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'

                    and detail2.kegiatan_code=detail.kegiatan_code
                    and detail2.unit_id=detail.unit_id
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.detail_no=detail.detail_no
                    and detail.status_hapus<>true
                    and detail2.status_hapus<>true
                    and detail2.status_level >= 4
                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                    or detail2.rekening_code <> detail.rekening_code 
                    or detail2.subtitle<>detail.subtitle
                    or detail2.komponen_name <> detail.komponen_name
                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                    or detail2.detail_name <> detail.detail_name
                    $where_anggaran2)


            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name
            )
            union
            (
            SELECT
                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                    detail2.komponen_harga_awal as detail_harga2,
                    detail2.pajak as pajak2,
                    detail2.komponen_id as komponen_id2,
                    detail2.detail_name as detail_name2,
                    detail2.komponen_name as komponen_name2,
                    detail2.volume as volume2,
                    detail2.subtitle as subtitle_name2,
                    detail2.satuan as detail_satuan2,
                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                    detail2.detail_no as detail_no2,
                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                    (detail2.nilai_anggaran) as hasil_kali2,

                    '' as detail_name,
                    0 as detail_harga,
                    0 as pajak,
                    '' as komponen_id,
                    '' as detail_name,
                    '' as komponen_name,
                    0 as volume,
                    '' as subtitle_name,
                    '' as detail_satuan,
                    '' as keterangan_koefisien,
                    0 as detail_no,
                    0 as hasil,
                    0 as hasil_kali,
                    detail2.detail_name as detail_name_rd,
                    detail2.sub as sub2,
                    '' as sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    detail2.is_bagian_hukum_setuju,
                    detail2.is_inspektorat_setuju,
                    detail2.is_badan_kepegawaian_setuju,
                    detail2.is_lppa_setuju,
                    detail2.is_bagian_organisasi_setuju, 
                    11 as sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    false as is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'
                    and detail2.kegiatan_code=detail.kegiatan_code
                    and detail2.unit_id=detail.unit_id
                    and detail2.rekening_code=detail.rekening_code
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.detail_no=detail.detail_no
                    and detail.status_hapus=true
                    and detail2.status_hapus<>true
                    and detail2.status_level >= 4
                    and detail2.tahap='$tahap'
                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                    or detail2.rekening_code <> detail.rekening_code 
                    or detail2.subtitle<>detail.subtitle
                    or detail2.komponen_name <> detail.komponen_name
                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                    or detail2.detail_name <> detail.detail_name
                    $where_anggaran2)


            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name
            )
            union
            (
            SELECT

                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    '' as detail_name22,
                    0 as detail_harga2,
                    0 as pajak2,
                    '' as komponen_id2,
                    '' as detail_name2,
                    '' as komponen_name2,
                    0 as volume2,
                    '' as subtitle_name2,
                    '' as detail_satuan2,
                    '' as keterangan_koefisien2,
                    detail.detail_no as detail_no2,
                    0 as hasil2,
                    0 as hasil_kali2,

                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                    detail.komponen_harga_awal as detail_harga,
                    detail.pajak as pajak,
                    detail.komponen_id as komponen_id,
                    detail.detail_name as detail_name,
                    detail.komponen_name as komponen_name,
                    detail.volume as volume,
                    detail.subtitle as subtitle_name,
                    detail.satuan as detail_satuan,
                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                    detail.detail_no as detail_no,
                    detail.volume * detail.komponen_harga_awal as hasil,
                    (detail.nilai_anggaran) as hasil_kali,
                    detail.detail_name as detail_name_rd,
                    '' as sub2,
                    detail.sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    detail2.is_bagian_hukum_setuju,
                    detail2.is_inspektorat_setuju,
                    detail2.is_badan_kepegawaian_setuju,
                    detail2.is_lppa_setuju,
                    detail2.is_bagian_organisasi_setuju,
                    11 as sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    false as is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening, " . sfConfig::get('app_default_schema') . ".$tabel_semula detail

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'

                    and detail2.kegiatan_code=detail.kegiatan_code
                    and detail2.unit_id=detail.unit_id
                    and detail2.rekening_code=detail.rekening_code
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.detail_no=detail.detail_no
                    and detail.status_hapus<>true
                    and detail2.status_hapus=true
                    and detail2.status_level >= 4
                    and detail2.tahap='$tahap'
                    and (detail2.nilai_anggaran <> detail.nilai_anggaran
                    or detail2.rekening_code <> detail.rekening_code 
                    or detail2.subtitle<>detail.subtitle
                    or detail2.komponen_name <> detail.komponen_name
                    or detail2.keterangan_koefisien <> detail.keterangan_koefisien
                    or detail2.detail_name <> detail.detail_name
                    $where_anggaran2)


            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name
            )
            union
            (
            SELECT

                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    detail2.komponen_name || ' ' || detail2.detail_name as detail_name22,
                    detail2.komponen_harga_awal as detail_harga2,
                    detail2.pajak as pajak2,
                    detail2.komponen_id as komponen_id2,
                    detail2.detail_name as detail_name2,
                    detail2.komponen_name as komponen_name2,
                    detail2.volume as volume2,
                    detail2.subtitle as subtitle_name2,
                    detail2.satuan as detail_satuan2,
                    replace(detail2.keterangan_koefisien,'<*>','X') as keterangan_koefisien2,
                    detail2.detail_no as detail_no2,
                    detail2.volume * detail2.komponen_harga_awal as hasil2,
                    (detail2.nilai_anggaran) as hasil_kali2,

                    '' as detail_name,
                    0 as detail_harga,
                    0 as pajak,
                    '' as komponen_id,
                    '' as detail_name,
                    '' as komponen_name,
                    0 as volume,
                    '' as subtitle_name,
                    '' as detail_satuan,
                    '' as keterangan_koefisien,
                    0 as detail_no,
                    0 as hasil,
                    0 as hasil_kali,
                    detail2.detail_name as detail_name_rd,
                    detail2.sub as sub2,
                    '' as sub,
                    detail2.note_skpd as note_skpd,
                    detail2.note_peneliti as note_peneliti,
                    detail2.note_tapd as note_tapd,
                    detail2.note_bappeko as note_bappeko,
                    detail2.status_level as status_level,
                    detail2.status_level_tolak,
                    detail2.status_sisipan,
                    detail2.status_lelang,
                    detail2.is_tapd_setuju,
                    detail2.is_bappeko_setuju,
                    detail2.is_penyelia_setuju,
                    detail2.is_bagian_hukum_setuju,
                    detail2.is_inspektorat_setuju,
                    detail2.is_badan_kepegawaian_setuju,
                    detail2.is_lppa_setuju,
                    detail2.is_bagian_organisasi_setuju,
                    11 as sumber_dana_id,
                    detail2.sumber_dana_id as sumber_dana_id2,
                    false as is_covid,
                    detail2.is_covid as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2, " . sfConfig::get('app_default_schema') . ".rekening rekening

            WHERE
                    detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id'

                    and detail2.status_hapus=false
                    and rekening.rekening_code=detail2.rekening_code
                    and detail2.status_level >= 4

                    and (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) not in
                    (
                    select (detail.unit_id||detail.kegiatan_code||detail.detail_no) from " . sfConfig::get('app_default_schema') . ".$tabel_semula detail where detail.kegiatan_code = '$kode_kegiatan' and
                    detail.unit_id='$unit_id'
                    )

            ORDER BY

                    rekening.rekening_code,
                    detail2.subtitle ,
                    detail2.komponen_name)
            union
            (
            SELECT
                    rekening.rekening_code,rekening.rekening_code as rekening_code2,
                    rekening.rekening_name as nama_rekening2,
                    '' as detail_name22,
                    0 as detail_harga2,
                    0 as pajak2,
                    '' as komponen_id2,
                    '' as detail_name2,
                    '' as komponen_name2,
                    0 as volume2,
                    '' as subtitle_name2,
                    '' as detail_satuan2,
                    '' as keterangan_koefisien2,
                    detail.detail_no as detail_no2,
                    0 as hasil2,
                    0 as hasil_kali2,

                    detail.komponen_name || ' ' || detail.detail_name as detail_name2,
                    detail.komponen_harga_awal as detail_harga,
                    detail.pajak as pajak,
                    detail.komponen_id as komponen_id,
                    detail.detail_name as detail_name,
                    detail.komponen_name as komponen_name,
                    detail.volume as volume,
                    detail.subtitle as subtitle_name,
                    detail.satuan as detail_satuan,
                    replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
                    detail.detail_no as detail_no,
                    detail.volume * detail.komponen_harga_awal as hasil,
                    (detail.nilai_anggaran) as hasil_kali,
                    '' as detail_name_rd,
                    '' as sub2,
                    detail.sub,
                    '' as note_skpd,
                    '' as note_peneliti,
                    '' as note_tapd,
                    '' as note_bappeko,
                    null as status_level,
                    null,
                    false,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,                                                   
                    11 as sumber_dana_id,
                    11 as sumber_dana_id2,
                    false as is_covid,                    
                    false as is_covid2


            FROM
                    " . sfConfig::get('app_default_schema') . ".$tabel_semula detail, " . sfConfig::get('app_default_schema') . ".rekening rekening

            WHERE
                    detail.kegiatan_code = '$kode_kegiatan' and
                    detail.unit_id='$unit_id'

                    and detail.status_hapus=false
                    and rekening.rekening_code=detail.rekening_code
                    and (detail.unit_id||detail.kegiatan_code||detail.detail_no) not in
                    (
                    select (detail2.unit_id||detail2.kegiatan_code||detail2.detail_no) from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail detail2 where detail2.kegiatan_code = '$kode_kegiatan' and
                    detail2.unit_id='$unit_id' and detail2.status_hapus<>true
                    )

            ORDER BY

                    rekening.rekening_code,
                    detail.subtitle ,
                    detail.komponen_name)
            ORDER BY sub2 ASC, sub ASC, detail_name22 ASC";

            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {

                $c = new Criteria();
                $c->add(MasterSumberDanaPeer::ID, $rs->getString('sumber_dana_id'));
                $c->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                $v = MasterSumberDanaPeer::doSelectOne($c);
                if($v->getSumberDana() == APBD)
                {
                    $sumber_dana_id_semula= '';
                }
                else
                {
                    $sumber_dana_id_semula= '('.$v->getSumberDana().')';
                }

                $c1 = new Criteria();
                $c1->add(MasterSumberDanaPeer::ID, $rs->getString('sumber_dana_id2'));
                $c1->addAscendingOrderByColumn(MasterSumberDanaPeer::SUMBER_DANA);
                $v1 = MasterSumberDanaPeer::doSelectOne($c1);
                if($v1->getSumberDana() == APBD)
                {
                    $sumber_dana_id_menjadi= '';
                }
                else
                {
                    $sumber_dana_id_menjadi= '('.$v1->getSumberDana().')';
                }

                if($rs->getString('is_covid') == t)
                {
                    $is_covid_semula='(COVID)';
                }
                else
                {
                    $is_covid_semula='';
                }

                 if($rs->getString('is_covid2') == t)
                {
                    $is_covid_menjadi='(COVID)';
                }
                else
                {
                    $is_covid_menjadi='';
                }
                
                // die($rs->getString('detail_no2').' '.$rs->getString('is_covid').' '.$rs->getString('is_covid2').' '.$sumber_dana_id_semula.' '.$sumber_dana_id_menjadi.' '.$is_covid_semula.' '.$is_covid_menjadi); 

                $detail_ba = new BeritaAcaraDetail();
                $detail_ba->setIdBeritaAcara($id_baru);
                $detail_ba->setSubtitle($rs->getString('subtitle_name2'));
                $detail_ba->setDetailNo($rs->getString('detail_no2'));
                $detail_ba->setRekeningCode($rs->getString('rekening_code2'));
                $detail_ba->setRekeningCodeSemula($rs->getString('rekening_code'));
                if ($rs->getString('sub') && $rs->getString('komponen_name')) {
                    $subnya = '[' . $rs->getString('sub') . '] ';
                }
                if ($rs->getString('sub2') && $rs->getString('komponen_name2')) {
                    $subnya2 = '[' . $rs->getString('sub2') . '] ';
                }
                // $detail_ba->setSemulaKomponen($subnya . $rs->getString('komponen_name') . ' ' . $rs->getString('detail_name').' '.$sumber_dana_id_semula.''.$is_covid_semula);
                $detail_ba->setSemulaKomponen($rs->getString('komponen_name') . ' ' . $rs->getString('detail_name').' '.$sumber_dana_id_semula.''.$is_covid_semula);
                // $detail_ba->setMenjadiKomponen($subnya2 . $rs->getString('komponen_name2') . ' ' . $rs->getString('detail_name_rd').' '.$sumber_dana_id_menjadi.''.$is_covid_menjadi);
                $detail_ba->setMenjadiKomponen($rs->getString('komponen_name2') . ' ' . $rs->getString('detail_name_rd').' '.$sumber_dana_id_menjadi.''.$is_covid_menjadi);
                $detail_ba->setSemulaSatuan($rs->getString('detail_satuan'));
                $detail_ba->setMenjadiSatuan($rs->getString('detail_satuan2'));
                $detail_ba->setSemulaKoefisien($rs->getString('keterangan_koefisien'));
                $detail_ba->setMenjadiKoefisien($rs->getString('keterangan_koefisien2'));

                $detail_ba->setSemulaHarga($rs->getString('detail_harga'));
                $detail_ba->setMenjadiHarga($rs->getString('detail_harga2'));

                $hasil1 = $rs->getString('detail_harga') * $rs->getString('volume');
                $hasil2 = $rs->getString('detail_harga2') * $rs->getString('volume2');
                $detail_ba->setSemulaHasil($hasil1);
                $detail_ba->setMenjadiHasil($hasil2);

                $detail_ba->setSemulaPajak($rs->getString('pajak'));
                $detail_ba->setMenjadiPajak($rs->getString('pajak2'));

//                $total1 = $hasil1 + (($rs->getString('pajak') / 100) * $hasil1);
//                $total2 = $hasil2 + (($rs->getString('pajak2') / 100) * $hasil2);
                $detail_ba->setSemulaTotal($rs->getString('hasil_kali'));
                $detail_ba->setMenjadiTotal($rs->getString('hasil_kali2'));
                $detail_ba->setCatatan($rs->getString('note_skpd'));
                $detail_ba->setBelanja(substr($rs->getString('rekening_code'), 0, 6));
                $detail_ba->save();
            }

            //update catatan pembahasan BA resume
            $tahaps = DinasMasterKegiatanPeer::getTahapKegiatan($unit_id, $kode_kegiatan);
            $catatan = DinasMasterKegiatanPeer::getCatatanPembahasanKegiatan($unit_id, $kode_kegiatan);
            $querys = "update " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan set catatan_pembahasan='$catatan' where id_deskripsi_resume_rapat=
                (select max(id) from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat
                where tahap=$tahaps and unit_id='$unit_id') and kegiatan_code='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($querys);
            $stmt->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengupadate BA resume rapat di tabel dinas_master_kegiatan');

            
                       return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }


    function executeUploadGantiVol() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'komponen_vol_orang' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 0; $row <= $highR; $row++) 
            {
                $detail_kegiatan = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $volume_orang = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());                

                $query1 = "select * from ebudget.dinas_rincian_detail
                        where detail_kegiatan='$detail_kegiatan' ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $komponen_id = $rs1->getString('komponen_id');
                    $detail= $rs1->getString('detail_kegiatan');

                    $vol_sebelum="SELECT volume_orang  from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_kegiatan='".$detail."'";
                    $st = $con->prepareStatement($vol_sebelum);
                    $rs2=$st->executeQuery();
                    while($rs2->next())
                    {
                        $cek = "SELECT count(*) as hasil from  " . sfConfig::get('app_default_schema') . ".log_volume_orang where 
                        detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";                   
                        $stmt3=  $con->prepareStatement($cek);
                        $rs=$stmt3->executeQuery();
                        while($rs->next())
                        { 
                            $nilai=$rs->getString('hasil');
                            if($nilai==0)
                            {
                               $insertvol ="INSERT INTO " . sfConfig::get('app_default_schema') . ".log_volume_orang(
                                detail_kegiatan, volume_semula, volume_menjadi, tahap, waktu_edit)
                                VALUES ('" . $detail. "',".$rs1->getString('volume_orang').", ".$volume_orang.",'".sfConfig::get('app_tahap_detail')."','".date('Y-m-d h:i:sa')."')";
                                $stmt2 = $con->prepareStatement($insertvol);
                                $stmt2->executeQuery();
                            }
                            else
                            {
                                $updatevol =
                                "UPDATE " . sfConfig::get('app_default_schema') . ".log_volume_orang
                                SET volume_menjadi=".$volume_orang.",tahap='".sfConfig::get('app_tahap_detail')."',waktu_edit='".date('Y-m-d h:i:sa')."' where detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";
                                $stmt2 = $con->prepareStatement($updatevol);
                                $stmt2->executeQuery();
                            }

                        } 
                    }
                                       
                    $query = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . " set volume_orang='$volume_orang' "
                            . " where detail_kegiatan='$detail' ";                           
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();

                    $queryrka = " update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . " set volume_orang='$volume_orang' "
                            . " where detail_kegiatan='$detail' ";  
                    $stmt1 = $con->prepareStatement($queryrka);
                    $stmt1->executeQuery();

                    $queryrbp =" update " . sfConfig::get('app_default_schema') . ".rincian_detail_bp "
                            . " set volume_orang='$volume_orang' "
                            . " where detail_kegiatan='$detail' ";  
                    $stmt2 = $con->prepareStatement($queryrbp);
                    $stmt2->executeQuery();                    
                   
                }                   
                    
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengubah Volume orang sesuai edelivery di tabel rincian detail ');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadDataMaksUK() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {
                $unit_name = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $nilai_maks_uk = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());

                $c = new Criteria();
                $c->add(UnitKerjaPeer::UNIT_NAME, $unit_name);
                $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                if ($rs_unitkerja) {
                    $unit_id = $rs_unitkerja->getUnitId();
                }

                $query1 = "select count(*) as total from ebudget.maks_uk  
                where unit_id = '$unit_id' ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $total_ada = $rs1->getString('total');
                }
                if ($total_ada == 0) {
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".maks_uk   
                (unit_id, unit_name, nilai_maks)  
                values 
                ('$unit_id','$unit_name','$nilai_maks_uk')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                } else {
                    $query = "update " . sfConfig::get('app_default_schema') . ".maks_uk   
                        set nilai_maks = '$nilai_maks_uk' 
                            where unit_id = '$unit_id'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil dimasukkan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadDataMutasi() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {
                $user_id = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $unit_id = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $level = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $jabatan = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                //echo $user_id; echo $unit_id; exit;
                $c = new Criteria();
                $c->add(UserHandleV2Peer::USER_ID, $user_id);
                $rs_userid = UserHandleV2Peer::doSelectOne($c);
                if ($rs_userid) {
                    $user_id = $rs_userid->getUserId();
                    $query = " update user_handle_v2"
                            . " set unit_id=$unit_id "
                            . " where user_id='$user_id'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    $query2 = " update schema_akses_v2"
                            . " set level_id=$level"
                            . " where user_id='$user_id'";
                    $stmt1 = $con->prepareStatement($query2);
                    $stmt1->executeQuery();
                    $query3 = " update master_user_v2"
                            . " set jabatan='$jabatan'"
                            . " where user_id='$user_id'";
                    $stmt2 = $con->prepareStatement($query3);
                    $stmt2->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil dimasukkan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadPAGUKUA() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {
                $kode_kegiatan = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $unit_name = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $nama_kegiatan = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $output_kegiatan = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $pagu_kegiatan = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());

                $pengurangan = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
                $jasmas = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
                $tambahan = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
                $habitat = trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
                $transport = trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());

                $tambahan_pagu_plus = trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue());

                $c = new Criteria();
                $c->add(UnitKerjaPeer::UNIT_NAME, $unit_name);
                $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                if ($rs_unitkerja) {
                    $unit_id = $rs_unitkerja->getUnitId();
                }

                $query1 = "select count(*) as total from ebudget.pagu_kua_murni  
                where unit_id = '$unit_id' and kode_kegiatan = '$kode_kegiatan' ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {
                    $total_ada = $rs1->getString('total');
                }
                if ($total_ada == 0) {
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".pagu_kua_murni   
                (unit_id, kode_kegiatan, nama_kegiatan, output_kegiatan, pagu_kegiatan, tambahan_pagu)  
                values 
                ('$unit_id','$kode_kegiatan','$nama_kegiatan','$output_kegiatan','$pagu_kegiatan','$tambahan_pagu_plus')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                } else {
                    $query = "update " . sfConfig::get('app_default_schema') . ".pagu_kua_murni   
                        set pagu_kegiatan = '$pagu_kegiatan', tambahan_pagu = '$tambahan_pagu_plus'  
                            where unit_id = '$unit_id' and kode_kegiatan = '$kode_kegiatan'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Pagu KUA Berhasil dimasukkan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadTambahPaguUK() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {
                $unit_name = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $tambahan_pagu_plus = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());

                $c = new Criteria();
                $c->add(UnitKerjaPeer::UNIT_NAME, $unit_name);
                $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                if ($rs_unitkerja) {
                    $unit_id = $rs_unitkerja->getUnitId();
                }


                $c_ambil_kegiatan_01 = new Criteria();
                $c_ambil_kegiatan_01->add(MasterKegiatanPeer::KODE_KEGIATAN, '%.01.____', Criteria::ILIKE);
                $c_ambil_kegiatan_01->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                $dapat_ambil_kegiatan_01 = MasterKegiatanPeer::doSelectOne($c_ambil_kegiatan_01);

                if ($dapat_ambil_kegiatan_01) {
                    $kode_kegiatan = $dapat_ambil_kegiatan_01->getKodeKegiatan();
                    $pagu_tambahan_awal = $dapat_ambil_kegiatan_01->getTambahanPagu();

                    $pagu_tambahan_baru = $pagu_tambahan_awal + $tambahan_pagu_plus;

                    $dapat_ambil_kegiatan_01->setTambahanPagu($pagu_tambahan_baru);
                    $dapat_ambil_kegiatan_01->save();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Tambahan Pagu UK Berhasil dimasukkan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadUpgradePU() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {
//                kode  SKPD    kegiatan    komponen_id komponen    detail  volume      Anggaran
                $kode_rka = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $volume_excel = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());

                $array_kode = explode('.', $kode_rka);
//                XXXX.X.XX.XX.XXXX.YYY
                $unit_id = $array_kode[0];
                $kode_kegiatan = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $detail_no = $array_kode[5];

                if ($unit_id <> '2600') {
                    $this->setFlash('gagal', 'Gagal karena bukan PU');
                    return $this->redirect('admin/fiturTambahan');
                }

                $c = new Criteria();
                $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
                $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                $dapat_rd = RincianDetailPeer::doSelectOne($c);
                if ($dapat_rd) {
                    $volume = $dapat_rd->getVolume();
                    if ($volume == $volume_excel) {
                        $rekening_code = $dapat_rd->getRekeningCode();
                        $kode_belanja = substr($rekening_code, 0, 5);
                        if ($kode_belanja == '5.2.3') {

                            $satuan = $dapat_rd->getSatuan();
                            $volume_baru = ceil($volume);

                            $koefisien_baru = $volume_baru . ' ' . $satuan;

                            $dapat_rd->setVolume($volume_baru);
                            $dapat_rd->setKeteranganKoefisien($koefisien_baru);
                            $dapat_rd->save();
                        }
                    }
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Sikat PU Berhasil dimasukkan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    function executeGantiPindahKomponen() {
        $id_komponen_semula = $this->getRequestParameter('id_semula');
        $id_komponen_menjadi = $this->getRequestParameter('id_menjadi');

        $c_komponen = new Criteria();
        $c_komponen->add(KomponenPeer::KOMPONEN_ID, $id_komponen_menjadi);
        $dapat_komponen = KomponenPeer::doSelectOne($c_komponen);

        if (!$dapat_komponen) {
            $this->setFlash('gagal', 'Gagal karena tidak ditemukan Id Komponen ' . $id_komponen_menjadi . ' pada tabel Komponen');
            return $this->redirect('admin/fiturTambahan');
        } else {
            $komponen_id_baru = $dapat_komponen->getKomponenId();
            $komponen_name_baru = $dapat_komponen->getKomponenName();
            $komponen_harga_baru = $dapat_komponen->getKomponenHarga();
            $komponen_non_pajak_baru = $dapat_komponen->getKomponenNonPajak();
            $komponen_satuan_baru = $dapat_komponen->getSatuan();
            if ($komponen_non_pajak_baru == true) {
                $pajak_baru = 0;
                $itung_pajak_baru = 1;
            } else {
                $pajak_baru = 10;
                $itung_pajak_baru = 1.1;
            }
        }

        $con = Propel::getConnection();
        $con->begin();
        // $c_rka = new Criteria();
        // $c_rka->add(DinasRincianDetailPeer::KOMPONEN_ID, $id_komponen_semula);
        // $c_rka->add(DinasRincianDetailPeer::STATUS_HAPUS, FALSE);
        // $dapat_rka = DinasRincianDetailPeer::doSelectOne($c_rka);
        $query1 = "select * from ebudget.dinas_rincian_detail 
                where status_hapus = false and komponen_id = '$id_komponen_semula' ";

        $stmt1 = $con->prepareStatement($query1);
        $rs1 = $stmt1->executeQuery();

        if (!$rs1) {
            $this->setFlash('gagal', 'Gagal karena tidak ditemukan Id Komponen ' . $id_komponen_semula . ' pada tabel RKA');
            return $this->redirect('admin/fiturTambahan');
        }

        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "select * from ebudget.dinas_rincian_detail 
                where status_hapus = false and komponen_id = '$id_komponen_semula' ";

            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $nilai_anggaran = $rs1->getString('nilai_anggaran');

                $komponen_volume_baru = $nilai_anggaran / ($komponen_harga_baru * $itung_pajak_baru);

                $keterangan_koefisien_baru = $komponen_volume_baru . ' ' . $komponen_satuan_baru;

                $query3 = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set volume=$komponen_volume_baru, komponen_harga=$komponen_harga_baru, komponen_harga_awal=$komponen_harga_baru, pajak=$pajak_baru, "
                        . "keterangan_koefisien = '$keterangan_koefisien_baru', satuan = '$komponen_satuan_baru', "
                        . "komponen_id = '$komponen_id_baru', komponen_name = '$komponen_name_baru' "
                        . "where status_hapus = false "
                        . "and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt3 = $con->prepareStatement($query3);
                $stmt3->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk komponen ' . $id_komponen_semula . ' menjadi harga ' . $id_komponen_menjadi);
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUbah12BulanJadi11Bulan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "select rd.* 
from ebudget.rincian_detail rd, unit_kerja uk 
where status_hapus = false and rd.unit_id = uk.unit_id 
and rd.unit_id <> '9999' and rekening_code = '5.2.2.24.01' and satuan ilike 'orang bulan'  
and keterangan_koefisien ilike '%x%' 
and (keterangan_koefisien ilike '%orang%bulan%' or keterangan_koefisien ilike '%bulan%orang%')
and keterangan_koefisien not ilike '%x%x%' 
and nilai_anggaran > 0 and komponen_harga_awal >= 3008100 and (keterangan_koefisien ilike '12 bulan%' or keterangan_koefisien ilike '%12 bulan%' )
order by rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no";

            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $volume = $rs1->getString('volume');

                $volume_baru = ($volume / 12) * 11;

                $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "set volume= $volume_baru "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt3 = $con->prepareStatement($query3);
                $stmt3->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk menjadikan 11 bulan ');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUbahTenagaKerjaPerkalianEnakPerKomponen() {
        set_time_limit(0);

        $komponen_id = $this->getRequestParameter('komponen_id');
        $harga_baru = $this->getRequestParameter('harga_baru');

        $c_komponen = new Criteria();
        $c_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
        $dapat_komponen = KomponenPeer::doSelectOne($c_komponen);
        if ($dapat_komponen) {
            $satuan_komponen = $dapat_komponen->getSatuan();
        } else {
            $this->setFlash('gagal', 'Komponen Id Salah');
            return $this->redirect('admin/fiturTambahan');
        }

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "select * 
                from ebudget.rincian_detail rd, unit_kerja uk 
                where rd.unit_id = uk.unit_id and rd.status_hapus = false 
                and rd.komponen_id = '$komponen_id' 
                    and rd.unit_id <> '9999' 
                    order by rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no";
            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();

            while ($rs1->next()) {
                $nilai_bulan = 0;
                $nilai_orang = 0;
                $nilai_dapat = 0;
                $nilai_koefisien = 1;
                $ada_bulan = 0;
                $ada_orang = 0;

                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $nilai_anggaran = $rs1->getString('nilai_anggaran');
                $volume = $rs1->getString('volume');
                $koefisien = $rs1->getString('keterangan_koefisien');

                $satuan = $rs1->getString('satuan');

                if ($nilai_anggaran == 0) {
                    $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "set komponen_harga_awal = $harga_baru, komponen_harga = $harga_baru "
                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();
                } else {
                    $array_koefisien = explode('X', $koefisien);
                    $total_koefisien = count($array_koefisien);
                    if ($total_koefisien == 1) {
//                        kalo gelondongan
                        $volume_baru = $nilai_anggaran / ($harga_baru);
                        if ($satuan_komponen == 'Orang Bulan') {
                            $koefisien_baru = $volume_baru . ' Orang Bulan';
                        } else if ($satuan_komponen == 'Orang Hari') {
                            $koefisien_baru = $volume_baru . ' Orang Hari';
                        } else {
                            $koefisien_baru = $volume_baru . ' ' . $satuan;
                        }
                        $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                . "set keterangan_koefisien = '$koefisien_baru', komponen_harga_awal = $harga_baru, komponen_harga = $harga_baru, volume = $volume_baru "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                    } else if ($total_koefisien == 2) {
//                        kalo perkalian
                        $volume_baru = $nilai_anggaran / ($harga_baru);

                        if ($satuan_komponen == 'Orang Bulan') {
                            foreach ($array_koefisien as $value_koefisien) {
                                $array_nilai_koefisien = explode(' ', trim($value_koefisien));
                                if ($array_nilai_koefisien[1] == 'Orang') {
                                    $nilai_orang = $array_nilai_koefisien[0];
                                } else if ($array_nilai_koefisien[1] == 'Bulan') {
                                    $nilai_bulan = $array_nilai_koefisien[0];
                                }
                            }
                            $bulan_baru = $volume_baru / $nilai_orang;
                            if ($nilai_orang <> 0 && $bulan_baru <> '') {
                                $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                                $array_kode_rka = array('3000.1.08.25.0033.2', '3000.1.08.25.0033.9', '3000.1.08.25.0037.1', '3000.1.08.25.0037.2', '3000.1.08.25.0037.3', '3000.1.08.25.0037.7');
                                if (in_array($kode_rka, $array_kode_rka)) {
                                    $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan';
                                } else {
//                                selain tenaga honorer
                                    if ($bulan_baru > 11) {
                                        $bulan_baru = 11;
                                        $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan';
                                        $volume_baru = $bulan_baru * $nilai_orang;
                                    } else if ($bulan_baru >= 8 && $bulan_baru < 11) {
                                        $bulan_baru = 11;
                                        $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan';
                                        $volume_baru = $bulan_baru * $nilai_orang;
                                    } else {
                                        $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan';
                                    }
//                                selain tenaga honorer                                        
                                }
                            } else {
                                $koefisien_baru = $volume_baru . ' Orang Bulan';
                            }
                        } else if ($satuan_komponen == 'Orang Hari') {
                            foreach ($array_koefisien as $value_koefisien) {
                                $array_nilai_koefisien = explode(' ', trim($value_koefisien));
                                if ($array_nilai_koefisien[1] == 'Orang') {
                                    $nilai_orang = $array_nilai_koefisien[0];
                                } else if ($array_nilai_koefisien[1] == 'Hari') {
                                    $nilai_bulan = $array_nilai_koefisien[0];
                                }
                            }
                            $bulan_baru = $volume_baru / $nilai_orang;
                            if ($nilai_orang <> 0 && $bulan_baru <> '') {
                                $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Hari';
                            } else {
                                $koefisien_baru = $volume_baru . ' Orang Hari';
                            }
                        } else {
                            $koefisien_baru = $volume_baru . ' ' . $satuan;
                        }

                        $nilai_anggaran_baru = 0;

                        $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                . "set keterangan_koefisien = '$koefisien_baru', komponen_harga_awal = $harga_baru, komponen_harga = $harga_baru, volume = $volume_baru "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();

                        $query4 = "select * 
                            from ebudget.rincian_detail 
                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                        $stmt4 = $con->prepareStatement($query4);
                        $rs4 = $stmt4->executeQuery();
                        while ($rs4->next()) {
                            $nilai_anggaran_baru = $rs1->getString('nilai_anggaran');
                        }

                        $tambahan_anggaran = $nilai_anggaran_baru - $nilai_anggaran;

                        $query5 = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                                . "set tambahan_pagu=tambahan_pagu+$tambahan_anggaran "
                                . "where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                        $stmt5 = $con->prepareStatement($query5);
                        $stmt5->executeQuery();
                    } else if ($total_koefisien == 3) {
//                        kalo perkalian 3
                        $nilai_var_temp1 = 1;
                        $satuan_var_temp1 = '';

                        $volume_baru = $nilai_anggaran / ($harga_baru);

                        if ($satuan_komponen == 'Orang Bulan') {
                            foreach ($array_koefisien as $value_koefisien) {
                                $array_nilai_koefisien = explode(' ', trim($value_koefisien));
                                if ($array_nilai_koefisien[1] == 'Orang') {
                                    $nilai_orang = $array_nilai_koefisien[0];
                                } else if ($array_nilai_koefisien[1] == 'Bulan') {
                                    $nilai_bulan = $array_nilai_koefisien[0];
                                } else {
                                    $nilai_var_temp1 = $array_nilai_koefisien[0];
                                    $satuan_var_temp1 = $array_nilai_koefisien[1];
                                }
                            }
                            $bulan_baru = $volume_baru / ($nilai_orang * $nilai_var_temp1);
                            if ($nilai_orang <> 0 && $bulan_baru <> '') {
                                $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                                $array_kode_rka = array('3000.1.08.25.0033.2', '3000.1.08.25.0033.9', '3000.1.08.25.0037.1', '3000.1.08.25.0037.2', '3000.1.08.25.0037.3', '3000.1.08.25.0037.7');
                                if (in_array($kode_rka, $array_kode_rka)) {
                                    $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan X ' . $nilai_var_temp1 . ' ' . $satuan_var_temp1;
                                } else {
//                                selain tenaga honorer
                                    if ($bulan_baru > 11) {
                                        $bulan_baru = 11;
                                        $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan X ' . $nilai_var_temp1 . ' ' . $satuan_var_temp1;
                                        $volume_baru = $bulan_baru * $nilai_orang * $nilai_var_temp1;
                                    } else if ($bulan_baru >= 8 && $bulan_baru < 11) {
                                        $bulan_baru = 11;
                                        $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan X ' . $nilai_var_temp1 . ' ' . $satuan_var_temp1;
                                        $volume_baru = $bulan_baru * $nilai_orang * $nilai_var_temp1;
                                    } else {
                                        $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Bulan X ' . $nilai_var_temp1 . ' ' . $satuan_var_temp1;
                                    }
//                                selain tenaga honorer                                        
                                }
                            } else {
                                $koefisien_baru = $volume_baru . ' Orang Bulan';
                            }
                        } else if ($satuan_komponen == 'Orang Hari') {
                            foreach ($array_koefisien as $value_koefisien) {
                                $array_nilai_koefisien = explode(' ', trim($value_koefisien));
                                if ($array_nilai_koefisien[1] == 'Orang') {
                                    $nilai_orang = $array_nilai_koefisien[0];
                                } else if ($array_nilai_koefisien[1] == 'Hari') {
                                    $nilai_bulan = $array_nilai_koefisien[0];
                                } else {
                                    $nilai_var_temp1 = $array_nilai_koefisien[0];
                                    $satuan_var_temp1 = $array_nilai_koefisien[1];
                                }
                            }
                            $bulan_baru = $volume_baru / ($nilai_orang * $nilai_var_temp1);
                            if ($nilai_orang <> 0 && $bulan_baru <> '') {
                                $koefisien_baru = $nilai_orang . ' Orang X ' . $bulan_baru . ' Hari X ' . $nilai_var_temp1 . ' ' . $satuan_var_temp1;
                            } else {
                                $koefisien_baru = $volume_baru . ' Orang Hari';
                            }
                        } else {
                            $koefisien_baru = $volume_baru . ' ' . $satuan;
                        }

                        $nilai_anggaran_baru = 0;

                        $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                . "set keterangan_koefisien = '$koefisien_baru', komponen_harga_awal = $harga_baru, komponen_harga = $harga_baru, volume = $volume_baru "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();

                        $query4 = "select * 
                            from ebudget.rincian_detail 
                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                        $stmt4 = $con->prepareStatement($query4);
                        $rs4 = $stmt4->executeQuery();
                        while ($rs4->next()) {
                            $nilai_anggaran_baru = $rs1->getString('nilai_anggaran');
                        }

                        $tambahan_anggaran = $nilai_anggaran_baru - $nilai_anggaran;

                        $query5 = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                                . "set tambahan_pagu=tambahan_pagu+$tambahan_anggaran "
                                . "where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
                        $stmt5 = $con->prepareStatement($query5);
                        $stmt5->executeQuery();
                    } else {
//                        kalo gelondongan
                        $volume_baru = $nilai_anggaran / ($harga_baru);
                        if ($satuan_komponen == 'Orang Bulan') {
                            $koefisien_baru = $volume_baru . ' Orang Bulan';
                        } else if ($satuan_komponen == 'Orang Hari') {
                            $koefisien_baru = $volume_baru . ' Orang Hari';
                        } else {
                            $koefisien_baru = $volume_baru . ' ' . $satuan;
                        }

                        $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                . "set keterangan_koefisien = '$koefisien_baru', komponen_harga_awal = $harga_baru, komponen_harga = $harga_baru, volume = $volume_baru "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                    }
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil mengganti komponen ' . $komponen_id);
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTarikPerlengkapan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
//                ambil data dari query             
            $query1 = "select * 
                from ebudget.rincian_detail rd, unit_kerja uk 
                where rd.unit_id = uk.unit_id and rd.status_hapus = false 
                and (rd.unit_id||'.'||kegiatan_code||'.'||detail_no) in 
                (
'0306.1.18.02.0001.41',
'1100.1.19.01.0001.52',
'1209.1.20.29.0155.96',
'1211.1.20.02.0020.23',
'1214.1.20.02.0023.12',
'1300.2.01.21.0002.124',
'1300.2.01.22.0002.11',
'1300.2.01.22.0002.71',
'1600.1.14.02.0001.99',
'1900.1.26.01.0001.199',
'1900.1.26.01.0001.200',
'2100.1.19.02.0002.21',
'2200.1.13.02.0001.53',
'2200.1.13.02.0001.54',
'2400.1.09.02.0001.19',
'2500.1.04.02.0001.90',
'2500.1.04.02.0001.96',
'2600.1.03.28.0003.79',
'2800.1.07.02.0001.35',
'2800.1.07.02.0001.36',
'2800.1.07.21.0002.235',
'2800.1.07.21.0007.368',
'3000.1.08.25.0031.9',
'3100.1.22.20.0010.103' 
                ) 
                order by rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no";
            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
//                ambil data
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $nilai_anggaran = $rs1->getString('nilai_anggaran');
//                ambil data
//                pindah komponen ke tabel tarik_perlengkapan                
                $query_pindah_tabel = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_tarik_perlengkapan5 "
                        . "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt_pindah_tabel = $con->prepareStatement($query_pindah_tabel);
                $stmt_pindah_tabel->executeQuery();
//                pindah komponen ke tabel tarik_perlengkapan
//                update status_hapus untuk tabel rincian_detail                
                $query_hapus_exist = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "set status_hapus = true, note_skpd = 'TARIK PERLENGKAPAN' "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt_hapus_exist = $con->prepareStatement($query_hapus_exist);
                $stmt_hapus_exist->executeQuery();
//                update status_hapus untuk tabel rincian_detail
//                update kurangin pagu di tabel master_kegiatan
                $query_kurangi_pagu = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                        . "set tambahan_pagu=tambahan_pagu-$nilai_anggaran "
                        . "where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code' ";
                $stmt_kurangi_pagu = $con->prepareStatement($query_kurangi_pagu);
                $stmt_kurangi_pagu->executeQuery();
//                update kurangin pagu di tabel master_kegiatan                
            }
//                ambil data dari query
//                variabel tujuan
            $unit_id_tujuan = '0309';
            $kegiatan_code_tujuan = '1.20.30.0004';
            $subtitle_tujuan = 'Pengadaan Peralatan Kantor dan Rumah Tangga';
//                variabel tujuan                
//                ambil data dari tabel_temporary            
            $query2 = "select komponen_id, rekening_code, sum(volume) as total_volume, sum(nilai_anggaran) as total_anggaran "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail_tarik_perlengkapan5 "
                    . "group by komponen_id, rekening_code "
                    . "order by komponen_id";
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            while ($rs2->next()) {
//                ambil data
                $komponen_id = $rs2->getString('komponen_id');
                $rekening_code = $rs2->getString('rekening_code');
                $total_volume = $rs2->getString('total_volume');
                $total_anggaran = $rs2->getString('total_anggaran');
//                ambil data                
//                ambil data komponen 
                $c_komponen = new Criteria();
                $c_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $dapat_komponen = KomponenPeer::doSelectOne($c_komponen);
                if (!$dapat_komponen) {
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal karena tidak ditemukan komponen ' . $komponen_id);
                    return $this->redirect('admin/fiturTambahan');
                } else {
                    $komponen_name = $dapat_komponen->getKomponenName();
                    $komponen_satuan = $dapat_komponen->getSatuan();
                    $komponen_harga = $dapat_komponen->getKomponenHarga();
                    $komponen_tipe = $dapat_komponen->getKomponenTipe();
                    if ($komponen_tipe == 'SHSD') {
                        $komponen_tipe = 'SSH';
                    }
                    $komponen_non_pajak = $dapat_komponen->getKomponenNonPajak();
                    if ($komponen_non_pajak == true) {
                        $komponen_pajak = 0;
                    } else {
                        $komponen_pajak = 10;
                    }

                    $keterangan_koefisien_baru = $total_volume . ' ' . $komponen_satuan;

//                    ambil detail_no
                    $detail_no = 0;
                    $queryDetailNo = "select max(detail_no) as nilai "
                            . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where unit_id='$unit_id_tujuan' and kegiatan_code='$kegiatan_code_tujuan'";
                    $stmtDetailNo = $con->prepareStatement($queryDetailNo);
                    $rs_max = $stmtDetailNo->executeQuery();
                    while ($rs_max->next()) {
                        $detail_no = $rs_max->getString('nilai');
                    }
                    $detail_no+=1;
//                    ambil detail_no
//                    ambil kode_sub                    
                    $sql = "select max(kode_sub) as kode_sub "
                            . "from " . sfConfig::get('app_default_schema') . ".rka_member "
                            . "where kode_sub ilike 'RKAM%'";
                    $con = Propel::getConnection();
                    $stmt_kodesub = $con->prepareStatement($sql);
                    $rs = $stmt_kodesub->executeQuery();
                    while ($rs->next()) {
                        $kodesub = $rs->getString('kode_sub');
                    }
                    $kode = substr($kodesub, 4, 5);
                    $kode+=1;
                    if ($kode < 10) {
                        $kodesub = 'RKAM0000' . $kode;
                    } elseif ($kode < 100) {
                        $kodesub = 'RKAM000' . $kode;
                    } elseif ($kode < 1000) {
                        $kodesub = 'RKAM00' . $kode;
                    } elseif ($kode < 10000) {
                        $kodesub = 'RKAM0' . $kode;
                    } elseif ($kode < 100000) {
                        $kodesub = 'RKAM' . $kode;
                    }
                    $subsubtitle = $komponen_name;
//                    ambil kode_sub                                        
//                input di rka_member
                    $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".rka_member (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun )
                                        values ('$kodesub','$unit_id_tujuan','$kegiatan_code_tujuan',$detail_no,'$komponen_id','$komponen_name','','$rekening_code','" . sfConfig::get('app_tahun_default') . "')";
                    $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                    $stmt2->executeQuery();
//                input di rka_member
//                input di rincian_detail
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, 
                                last_update_user, last_update_time, tahap_edit,tahun,
                                note_skpd,tahap)
                                values
                                ('" . $kegiatan_code_tujuan . "', '" . $komponen_tipe . "', " . $detail_no . ", '" . $rekening_code . "', '" . $komponen_id . "', '', " . $total_volume . ", '" . $keterangan_koefisien_baru . "', '" . $subtitle_tujuan . "',
                                    " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $komponen_satuan . "', " . $komponen_pajak . ",'" . $unit_id_tujuan . "','" . $kodesub . "', '" . $subsubtitle . "',
                                        'admin', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','PENGUMPULAN TARIKAN PERLENGKAPAN','murni')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
//                input di rincian_detail                    
//                update tambah pagu di tabel master_kegiatan
                    $query_tambah_pagu = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                            . "set tambahan_pagu=tambahan_pagu+$total_anggaran "
                            . "where unit_id='$unit_id_tujuan' and kode_kegiatan='$kegiatan_code_tujuan' ";
                    $stmt_tambah_pagu = $con->prepareStatement($query_tambah_pagu);
                    $stmt_tambah_pagu->executeQuery();
//                update tambah pagu di tabel master_kegiatan
                }
//                ambil data komponen                 
            }
//                ambil data dari tabel_temporary                        
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil ditarik ke perlengkapan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTarikDiskominfo() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
//                ambil data dari query             
            $query1 = "select * 
                from ebudget.dinas_rincian_detail rd, unit_kerja uk 
                where rd.unit_id = uk.unit_id and rd.status_hapus = false 
                and (rd.unit_id||'.'||kegiatan_code||'.'||detail_no) = '0800.1.20.02.0033.68' 
                order by rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no";
            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
//                ambil data
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $nilai_anggaran = $rs1->getString('nilai_anggaran');
//                ambil data
//                pindah komponen ke tabel tarik_diskominfo                
                $query_pindah_tabel = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_tarik_diskominfo "
                        . "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt_pindah_tabel = $con->prepareStatement($query_pindah_tabel);
                $stmt_pindah_tabel->executeQuery();
//                pindah komponen ke tabel tarik_diskominfo
//                update status_hapus untuk tabel rincian_detail                
                $query_hapus_exist = "update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                        . "set status_hapus = true, note_skpd = 'TARIK DISKOMINFO' "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt_hapus_exist = $con->prepareStatement($query_hapus_exist);
                $stmt_hapus_exist->executeQuery();
//                update status_hapus untuk tabel rincian_detail
//                update kurangin pagu di tabel master_kegiatan
                $query_kurangi_pagu = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
                        . "set tambahan_pagu=tambahan_pagu-$nilai_anggaran "
                        . "where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code' ";
                $stmt_kurangi_pagu = $con->prepareStatement($query_kurangi_pagu);
                $stmt_kurangi_pagu->executeQuery();
//                update kurangin pagu di tabel master_kegiatan                
            }
//                ambil data dari query
//                variabel tujuan
            $unit_id_tujuan = '2700';
            $kegiatan_code_tujuan = '1.25.19.0001';
            $subtitle_tujuan = 'Pemeliharaan jaringan dan Perangkat pendukungnya';
//                variabel tujuan                
//                ambil data dari tabel_temporary            
            $query2 = "select komponen_id, rekening_code, sum(volume) as total_volume, sum(nilai_anggaran) as total_anggaran "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail_tarik_diskominfo "
                    . "group by komponen_id, rekening_code "
                    . "order by komponen_id";
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            while ($rs2->next()) {
//                ambil data
                $komponen_id = $rs2->getString('komponen_id');
                $rekening_code = $rs2->getString('rekening_code');
                $total_volume = $rs2->getString('total_volume');
                $total_anggaran = $rs2->getString('total_anggaran');
//                ambil data                
//                ambil data komponen 
                $c_komponen = new Criteria();
                $c_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $dapat_komponen = KomponenPeer::doSelectOne($c_komponen);
                if (!$dapat_komponen) {
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal karena tidak ditemukan komponen ' . $komponen_id);
                    return $this->redirect('admin/fiturTambahan');
                } else {
                    $komponen_name = $dapat_komponen->getKomponenName();
                    $komponen_satuan = $dapat_komponen->getSatuan();
                    $komponen_harga = $dapat_komponen->getKomponenHarga();
                    $komponen_tipe = $dapat_komponen->getKomponenTipe();
                    if ($komponen_tipe == 'SHSD') {
                        $komponen_tipe = 'SSH';
                    }
                    $komponen_non_pajak = $dapat_komponen->getKomponenNonPajak();
                    if ($komponen_non_pajak == true) {
                        $komponen_pajak = 0;
                    } else {
                        $komponen_pajak = 10;
                    }

                    $keterangan_koefisien_baru = $total_volume . ' ' . $komponen_satuan;

//                    ambil detail_no
                    $detail_no = 0;
                    $queryDetailNo = "select max(detail_no) as nilai "
                            . "from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . "where unit_id='$unit_id_tujuan' and kegiatan_code='$kegiatan_code_tujuan'";
                    $stmtDetailNo = $con->prepareStatement($queryDetailNo);
                    $rs_max = $stmtDetailNo->executeQuery();
                    while ($rs_max->next()) {
                        $detail_no = $rs_max->getString('nilai');
                    }
                    $detail_no+=1;
//                    ambil detail_no
//                    ambil kode_sub                    
                    $sql = "select max(kode_sub) as kode_sub "
                            . "from " . sfConfig::get('app_default_schema') . ".dinas_rka_member "
                            . "where kode_sub ilike 'RKAM%'";
                    $con = Propel::getConnection();
                    $stmt_kodesub = $con->prepareStatement($sql);
                    $rs = $stmt_kodesub->executeQuery();
                    while ($rs->next()) {
                        $kodesub = $rs->getString('kode_sub');
                    }
                    $kode = substr($kodesub, 4, 5);
                    $kode+=1;
                    if ($kode < 10) {
                        $kodesub = 'RKAM0000' . $kode;
                    } elseif ($kode < 100) {
                        $kodesub = 'RKAM000' . $kode;
                    } elseif ($kode < 1000) {
                        $kodesub = 'RKAM00' . $kode;
                    } elseif ($kode < 10000) {
                        $kodesub = 'RKAM0' . $kode;
                    } elseif ($kode < 100000) {
                        $kodesub = 'RKAM' . $kode;
                    }
                    $subsubtitle = $komponen_name;
//                    ambil kode_sub                                        
//                input di rka_member
                    $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".dinas_rka_member 
                        (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun ) 
                        values ('$kodesub','$unit_id_tujuan','$kegiatan_code_tujuan',$detail_no,'$komponen_id','$komponen_name','','$rekening_code','" . sfConfig::get('app_tahun_default') . "')";
                    $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                    $stmt2->executeQuery();
//                input di rka_member
//                input di rincian_detail
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, 
                                last_update_user, last_update_time, tahap_edit,tahun,
                                note_skpd,tahap)
                                values
                                ('" . $kegiatan_code_tujuan . "', '" . $komponen_tipe . "', " . $detail_no . ", '" . $rekening_code . "', '" . $komponen_id . "', '', " . $total_volume . ", '" . $keterangan_koefisien_baru . "', '" . $subtitle_tujuan . "',
                                    " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $komponen_satuan . "', " . $komponen_pajak . ",'" . $unit_id_tujuan . "','" . $kodesub . "', '" . $subsubtitle . "',
                                        'admin', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','PENGUMPULAN TARIKAN DISKOMINFO','murni')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
//                input di rincian_detail                    
//                update tambah pagu di tabel master_kegiatan
                    $query_tambah_pagu = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
                            . "set tambahan_pagu=tambahan_pagu+$total_anggaran "
                            . "where unit_id='$unit_id_tujuan' and kode_kegiatan='$kegiatan_code_tujuan' ";
                    $stmt_tambah_pagu = $con->prepareStatement($query_tambah_pagu);
                    $stmt_tambah_pagu->executeQuery();
//                update tambah pagu di tabel master_kegiatan
                }
//                ambil data komponen                 
            }
//                ambil data dari tabel_temporary                        
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil ditarik ke diskominfo');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUbahTenagaHonda() {
        set_time_limit(0);

        $komponen_id = $this->getRequestParameter('komponen_id');
        $harga_baru = $this->getRequestParameter('harga_baru');

        $con = Propel::getConnection();
        $query1 = "select * 
                from ebudget.rincian_detail rd, unit_kerja uk 
                where rd.unit_id = uk.unit_id and rd.status_hapus = false 
                and rd.komponen_id = '$komponen_id' 
                    and rd.unit_id <> '9999' 
                    order by rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no";
        $stmt1 = $con->prepareStatement($query1);
        $rs1 = $stmt1->executeQuery();

        while ($rs1->next()) {
            $nilai_baru = 0;

            $unit_id = $rs1->getString('unit_id');
            $kegiatan_code = $rs1->getString('kegiatan_code');
            $detail_no = $rs1->getString('detail_no');

            $nilai_lama = $rs1->getString('nilai_anggaran');

            $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                    . "set komponen_harga_awal = $harga_baru, komponen_harga = $harga_baru "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
            $stmt3 = $con->prepareStatement($query3);
            $stmt3->executeQuery();

            $query4 = "select * from ebudget.rincian_detail "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
            $stmt4 = $con->prepareStatement($query4);
            $rs4 = $stmt4->executeQuery();
            while ($rs4->next()) {
                $nilai_baru = $rs4->getString('nilai_anggaran');
            }

            $tambahan_kegiatan_baru = $nilai_baru - $nilai_lama;

            $query5 = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                    . "set tambahan_pagu = tambahan_pagu+$tambahan_kegiatan_baru "
                    . "where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code'";
            $stmt5 = $con->prepareStatement($query5);
            $stmt5->executeQuery();
        }
        $this->setFlash('berhasil', 'Berhasil merubah komponen ' . $komponen_id . ' dengan harga ' . $harga_baru);
        return $this->redirect('admin/fiturTambahan');
    }

    function executeGantiPindahKomponenAntarKegiatanSubtitle() {
        $unit_id = $this->getRequestParameter('unit_id');

        $kode_kegiatan_asal = $this->getRequestParameter('kode_kegiatan_asal');
        $subtitle_asal = $this->getRequestParameter('subtitle_asal');

        $kode_kegiatan_tujuan = $this->getRequestParameter('kode_kegiatan_tujuan');
        $subtitle_tujuan = $this->getRequestParameter('subtitle_tujuan');

        $c_rincian_detail = new Criteria();
        $c_rincian_detail->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
        $c_rincian_detail->addAnd(RincianDetailPeer::UNIT_ID, $unit_id);
        $c_rincian_detail->addAnd(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan_asal);
        $c_rincian_detail->addAnd(RincianDetailPeer::SUBTITLE, $subtitle_asal);
        $dapat_rincian_detail = RincianDetailPeer::doSelect($c_rincian_detail);

        if (!$dapat_rincian_detail) {
            $this->setFlash('gagal', 'Gagal karena tidak ditemukan komponen pada SKPD ' . $unit_id . ' Kegiatan ' . $kode_kegiatan_asal . ' Subtitle ' . $subtitle_asal);
            return $this->redirect('admin/fiturTambahan');
        } else {
            $komponen_id_baru = $dapat_komponen->getKomponenId();
            $komponen_name_baru = $dapat_komponen->getKomponenName();
            $komponen_harga_baru = $dapat_komponen->getKomponenHarga();
            $komponen_non_pajak_baru = $dapat_komponen->getKomponenNonPajak();
            $komponen_satuan_baru = $dapat_komponen->getSatuan();
            if ($komponen_non_pajak_baru == true) {
                $pajak_baru = 0;
                $itung_pajak_baru = 1;
            } else {
                $pajak_baru = 10;
                $itung_pajak_baru = 1.1;
            }
        }

        $c_rka = new Criteria();
        $c_rka->add(RincianDetailPeer::KOMPONEN_ID, $id_komponen_semula);
        $c_rka->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
        $dapat_rka = RincianDetailPeer::doSelectOne($c_rka);

        if (!$dapat_rka) {
            $this->setFlash('gagal', 'Gagal karena tidak ditemukan Id Komponen ' . $id_komponen_semula . ' pada tabel RKA');
            return $this->redirect('admin/fiturTambahan');
        }

        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query1 = "select * from ebudget.rincian_detail 
                where status_hapus = false and komponen_id = '$id_komponen_semula' ";

            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $nilai_anggaran = $rs1->getString('nilai_anggaran');

                $komponen_volume_baru = $nilai_anggaran / ($komponen_harga_baru * $itung_pajak_baru);

                $keterangan_koefisien_baru = $komponen_volume_baru . ' ' . $komponen_satuan_baru;

                $query3 = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "set volume=$komponen_volume_baru, komponen_harga=$komponen_harga_baru, komponen_harga_awal=$komponen_harga_baru, pajak=$pajak_baru, "
                        . "keterangan_koefisien = '$keterangan_koefisien_baru', satuan = '$komponen_satuan_baru', "
                        . "komponen_id = '$komponen_id_baru', komponen_name = '$komponen_name_baru' "
                        . "where status_hapus = false "
                        . "and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt3 = $con->prepareStatement($query3);
                $stmt3->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk komponen ' . $id_komponen_semula . ' menjadi harga ' . $id_komponen_menjadi);
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeTarikPendidikan() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
//                ambil data dari query             
            $query1 = "select * 
                from ebudget.rincian_detail rd, unit_kerja uk 
                where rd.unit_id = uk.unit_id and rd.status_hapus = false 
                and (rd.unit_id||'.'||kegiatan_code||'.'||detail_no) in 
                (
'2000.1.01.16.0004.1',
'2000.1.01.16.0004.2',
'2000.1.01.16.0004.3',
'2000.1.01.16.0004.4',
'2000.1.01.16.0004.5',
'2000.1.01.16.0004.6',
'2000.1.01.16.0004.7',
'2000.1.01.16.0004.8',
'2000.1.01.16.0004.9',
'2000.1.01.16.0004.10',
'2000.1.01.16.0004.11',
'2000.1.01.16.0004.12',
'2000.1.01.16.0004.13',
'2000.1.01.16.0004.14',
'2000.1.01.16.0004.15',
'2000.1.01.16.0004.16',
'2000.1.01.16.0004.17',
'2000.1.01.16.0004.18',
'2000.1.01.16.0004.19',
'2000.1.01.16.0004.20',
'2000.1.01.16.0004.21',
'2000.1.01.16.0004.22',
'2000.1.01.16.0004.23',
'2000.1.01.16.0004.24',
'2000.1.01.16.0004.25',
'2000.1.01.16.0004.26',
'2000.1.01.16.0004.27',
'2000.1.01.16.0004.28',
'2000.1.01.16.0004.29',
'2000.1.01.16.0004.30',
'2000.1.01.16.0004.31',
'2000.1.01.16.0004.32',
'2000.1.01.16.0004.33',
'2000.1.01.16.0004.34',
'2000.1.01.16.0004.35',
'2000.1.01.16.0004.36',
'2000.1.01.16.0004.37',
'2000.1.01.16.0004.44'
                )
                order by rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no";
            $stmt1 = $con->prepareStatement($query1);
            $rs1 = $stmt1->executeQuery();
            while ($rs1->next()) {
//                ambil data
                $unit_id = $rs1->getString('unit_id');
                $kegiatan_code = $rs1->getString('kegiatan_code');
                $detail_no = $rs1->getString('detail_no');

                $nilai_anggaran = $rs1->getString('nilai_anggaran');
//                ambil data
//                pindah komponen ke tabel tarik_diskominfo                
                $query_pindah_tabel = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_tarik_pendidikan "
                        . "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt_pindah_tabel = $con->prepareStatement($query_pindah_tabel);
                $stmt_pindah_tabel->executeQuery();
//                pindah komponen ke tabel tarik_diskominfo
//                update status_hapus untuk tabel rincian_detail                
                $query_hapus_exist = "update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "set status_hapus = true, note_skpd = 'TARIK PENDIDIKAN' "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                $stmt_hapus_exist = $con->prepareStatement($query_hapus_exist);
                $stmt_hapus_exist->executeQuery();
//                update status_hapus untuk tabel rincian_detail
//                update kurangin pagu di tabel master_kegiatan
                $query_kurangi_pagu = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                        . "set tambahan_pagu=tambahan_pagu-$nilai_anggaran "
                        . "where unit_id='$unit_id' and kode_kegiatan='$kegiatan_code' ";
                $stmt_kurangi_pagu = $con->prepareStatement($query_kurangi_pagu);
                $stmt_kurangi_pagu->executeQuery();
//                update kurangin pagu di tabel master_kegiatan                
            }
//                ambil data dari query
//                variabel tujuan
            $unit_id_tujuan = '2000';
            $kegiatan_code_tujuan = '1.01.16.0003';
            $subtitle_tujuan = 'Bintek BOPDA dan Hibah BOPDA';
//                variabel tujuan                
//                ambil data dari tabel_temporary            
            $query2 = "select komponen_id, rekening_code, sum(volume) as total_volume, sum(nilai_anggaran) as total_anggaran "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail_tarik_pendidikan "
                    . "group by komponen_id, rekening_code "
                    . "order by komponen_id";
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();
            while ($rs2->next()) {
//                ambil data
                $komponen_id = $rs2->getString('komponen_id');
                $rekening_code = $rs2->getString('rekening_code');
                $total_volume = $rs2->getString('total_volume');
                $total_anggaran = $rs2->getString('total_anggaran');
//                ambil data                
//                ambil data komponen 
                $c_komponen = new Criteria();
                $c_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $dapat_komponen = KomponenPeer::doSelectOne($c_komponen);
                if (!$dapat_komponen) {
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal karena tidak ditemukan komponen ' . $komponen_id);
                    return $this->redirect('admin/fiturTambahan');
                } else {
                    $komponen_name = $dapat_komponen->getKomponenName();
                    $komponen_satuan = $dapat_komponen->getSatuan();
                    $komponen_harga = $dapat_komponen->getKomponenHarga();
                    $komponen_tipe = $dapat_komponen->getKomponenTipe();
                    if ($komponen_tipe == 'SHSD') {
                        $komponen_tipe = 'SSH';
                    }
                    $komponen_non_pajak = $dapat_komponen->getKomponenNonPajak();
                    if ($komponen_non_pajak == true) {
                        $komponen_pajak = 0;
                    } else {
                        $komponen_pajak = 10;
                    }

                    $keterangan_koefisien_baru = $total_volume . ' ' . $komponen_satuan;

//                    ambil detail_no
                    $detail_no = 0;
                    $queryDetailNo = "select max(detail_no) as nilai "
                            . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . "where unit_id='$unit_id_tujuan' and kegiatan_code='$kegiatan_code_tujuan'";
                    $stmtDetailNo = $con->prepareStatement($queryDetailNo);
                    $rs_max = $stmtDetailNo->executeQuery();
                    while ($rs_max->next()) {
                        $detail_no = $rs_max->getString('nilai');
                    }
                    $detail_no+=1;
//                    ambil detail_no
//                    ambil kode_sub                    
                    $sql = "select max(kode_sub) as kode_sub "
                            . "from " . sfConfig::get('app_default_schema') . ".rka_member "
                            . "where kode_sub ilike 'RKAM%'";
                    $con = Propel::getConnection();
                    $stmt_kodesub = $con->prepareStatement($sql);
                    $rs = $stmt_kodesub->executeQuery();
                    while ($rs->next()) {
                        $kodesub = $rs->getString('kode_sub');
                    }
                    $kode = substr($kodesub, 4, 5);
                    $kode+=1;
                    if ($kode < 10) {
                        $kodesub = 'RKAM0000' . $kode;
                    } elseif ($kode < 100) {
                        $kodesub = 'RKAM000' . $kode;
                    } elseif ($kode < 1000) {
                        $kodesub = 'RKAM00' . $kode;
                    } elseif ($kode < 10000) {
                        $kodesub = 'RKAM0' . $kode;
                    } elseif ($kode < 100000) {
                        $kodesub = 'RKAM' . $kode;
                    }
                    $subsubtitle = $komponen_name;
//                    ambil kode_sub                                        
//                input di rka_member
                    $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".rka_member 
                        (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun ) 
                        values ('$kodesub','$unit_id_tujuan','$kegiatan_code_tujuan',$detail_no,'$komponen_id','$komponen_name','','$rekening_code','" . sfConfig::get('app_tahun_default') . "')";
                    $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                    $stmt2->executeQuery();
//                input di rka_member
//                input di rincian_detail
                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, 
                                last_update_user, last_update_time, tahap_edit,tahun,
                                note_skpd,tahap)
                                values
                                ('" . $kegiatan_code_tujuan . "', '" . $komponen_tipe . "', " . $detail_no . ", '" . $rekening_code . "', '" . $komponen_id . "', '', " . $total_volume . ", '" . $keterangan_koefisien_baru . "', '" . $subtitle_tujuan . "',
                                    " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $komponen_satuan . "', " . $komponen_pajak . ",'" . $unit_id_tujuan . "','" . $kodesub . "', '" . $subsubtitle . "',
                                        'admin', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','PENGUMPULAN TARIKAN DISKOMINFO','murni')";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
//                input di rincian_detail                    
//                update tambah pagu di tabel master_kegiatan
                    $query_tambah_pagu = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                            . "set tambahan_pagu=tambahan_pagu+$total_anggaran "
                            . "where unit_id='$unit_id_tujuan' and kode_kegiatan='$kegiatan_code_tujuan' ";
                    $stmt_tambah_pagu = $con->prepareStatement($query_tambah_pagu);
                    $stmt_tambah_pagu->executeQuery();
//                update tambah pagu di tabel master_kegiatan
                }
//                ambil data komponen                 
            }
//                ambil data dari tabel_temporary                        
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil ditarik ke pendidikan');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUbahAlokasiDana() {
        set_time_limit(0);

        $con = Propel::getConnection();
        $con->begin();
        try {
            $query_list_kegiatan = "select * "
                    . "from ebudget.master_kegiatan "
                    . "where unit_id <> '9999' ";
            $stmt_list_kegiatan = $con->prepareStatement($query_list_kegiatan);
            $rs_list_kegiatan = $stmt_list_kegiatan->executeQuery();
            while ($rs_list_kegiatan->next()) {
                $unit_id = trim($rs_list_kegiatan->getString('unit_id'));
                $kode_kegiatan = trim($rs_list_kegiatan->getString('kode_kegiatan'));
                $query_total_skpd = "select sum(nilai_anggaran) as total "
                        . "from ebudget.rincian_detail "
                        . "where unit_id = '$unit_id' and status_hapus = false "
                        . "and kegiatan_code = '$kode_kegiatan' ";
                $stmt_total_skpd = $con->prepareStatement($query_total_skpd);
                $rs_total = $stmt_total_skpd->executeQuery();
                while ($rs_total->next()) {
                    $total_dinas_sekarang = $rs_total->getString('total');
                }

                $c_kegiatan = new Criteria();
                $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                $c_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                $dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_kegiatan);
                if ($dapat_kegiatan) {
                    $dapat_kegiatan->setAlokasiDana($total_dinas_sekarang);
                    $dapat_kegiatan->setTambahanPagu(0);
                    $dapat_kegiatan->save();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Pengubahan Pagu Kegiatan berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Pengubahan Pagu Kegiatan gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadJkn() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $kode = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $volume = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $volume_orang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    //$keterangan = $volume.' '.$satuan;
                    if ($kode!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode);                          
                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {
                            $query = "update ebudget.dinas_rincian_detail set volume = $volume, keterangan_koefisien = '$keterangan', volume_orang = $volume_orang where detail_kegiatan = '$kode'";
                            //$query = "update ebudget.dinas_master_kegiatan set tambahan_pagu=$komponen_baru where kode_kegiatan='$kode_rka'";                       
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();
                        } else {
                            $ada_error[] = "kode kegiatan $kode tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tenaga Ops Dan Jkn telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadUpdateKeterangan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $kode_rka = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    //$volume_total = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $keterangan_koefisien = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    //$kode_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                   // $komponen_harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                   // $volume_total_titik = str_replace(',', '.', $volume_total);
                    if ($kode_rka!='' &&  $keterangan_koefisien!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode_rka);                                      
                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {

                            $query1 = "update ebudget.dinas_rincian_detail 
                                       set keterangan_koefisien='$keterangan_koefisien'
                                       where detail_kegiatan='$kode_rka'";
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                            $query2 = "update ebudget.rincian_detail 
                                       set keterangan_koefisien='$keterangan_koefisien'
                                       where detail_kegiatan='$kode_rka'";
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();

                            $query3 = "update ebudget.rincian_detail_bp
                                       set keterangan_koefisien='$keterangan_koefisien'
                                       where detail_kegiatan='$kode_rka'";
                            $stmt3 = $con->prepareStatement($query3);
                            $stmt3->executeQuery();

                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tenaga telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    
    public function executeUploadTambahanPagu() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $tambahan_pagu = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));                   
                    if ($kode_kegiatan!='' &&  $tambahan_pagu!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            // $query = "update ebudget.dinas_master_kegiatan set tambahan_pagu=$tambahan_pagu where kode_kegiatan='$kode_kegiatan'";
                             
                            // $stmt = $con->prepareStatement($query);
                            // $stmt->executeQuery();

                            $query1 = "update ebudget.dinas_master_kegiatan set tambahan_pagu=$tambahan_pagu where kode_kegiatan='$kode_kegiatan'";
                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                           
                         //    $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode_kegiatan);                          
                         // if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {
                         //    // $query = "update ebudget.dinas_master_kegiatan set tambahan_pagu=$tambahan_pagu where kode_kegiatan='$kode_kegiatan'";
                         //     $query = "update ebudget.dinas_rincian_detail set volume='$tambahan_pagu', keterangan_koefisien='$tambahan_pagu'||' Poin' where detail_kegiatan='$kode_kegiatan'";
                         //    $stmt = $con->prepareStatement($query);
                         //    $stmt->executeQuery();

                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambahan pagu telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
public function executeUploadAlokasiDana() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $alokasi_dana = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));                   
                    if ($kode_kegiatan!='' &&  $alokasi_dana!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            
                            $query1 = "update ebudget.dinas_master_kegiatan set alokasi_dana=$alokasi_dana where kode_kegiatan='$kode_kegiatan'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                            $query2 = "update ebudget.pagu set alokasi=$alokasi_dana where kode_kegiatan='$kode_kegiatan'";
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();
                         
                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Alokasi Dana Kegiatan telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadPaguRasionalisasi() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

           // $fileName_xls = 'pagurasionalisasi_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];
           $fileName_xls = 'pagurasionalisasi_' . date('Y-m-d_H-i') . '.xls';

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 2; $row <= $highR; $row++) {
                    
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $alokasi_dana = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));                   
                    if ($kode_kegiatan!='' &&  $alokasi_dana!='') {                         
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(PaguRasionalisasiPeer::KEGIATAN_CODE, $kode_kegiatan);                          
                         if (PaguRasionalisasiPeer::doSelectOne($c_cek_kegiatan)) {
                            
                            $query1 = "update ebudget.pagu_rasionalisasi set pagu=$alokasi_dana where kegiatan_code='$kode_kegiatan'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();
                           
                         
                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Pagu Rasionalisasi telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }


    public function executeUploadHarga2021() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    
                    $komponen_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $harga_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));                   
                    if ($komponen_id!='' &&  $harga_baru!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::KOMPONEN_ID, $komponen_id);   

                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {
                            
                            $query1 = "update ebudget.dinas_rincian_detail set komponen_harga=$harga_baru , komponen_harga_awal=$harga_baru where komponen_id='$komponen_id'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();                          
                         
                        } else {
                            $ada_error[] = "Komponen tidak ditemukan.";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Harga Dinas Rincian Detail Sudah di rubah');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
     public function executeUploadVolumeUK2021() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    
                    $kegiatan_code = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $volume_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $keterangan_koefisien = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));                   
                    if ( $kegiatan_code!='' &&  $volume_baru!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kegiatan_code);   

                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {

                            $query1 = "update ebudget.dinas_rincian_detail set volume=$volume_baru , keterangan_koefisien='$keterangan_koefisien' where detail_kegiatan='$kegiatan_code'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery(); 
                            
                            // $query1 = "update ebudget.dinas_rincian_detail set volume=$volume_baru , keterangan_koefisien=concat($volume_baru,' Poin') where kegiatan_code='$kegiatan_code' and subtitle='Penunjang Kinerja' and status_hapus=false and komponen_name ilike '%Uang Kinerja%'";                             
                            // $stmt1 = $con->prepareStatement($query1);
                            // $stmt1->executeQuery(); 

                            // $query2 = "update ebudget.dinas_subtitle_indikator set nilai=$volume_baru,alokasi_dana=$volume_baru where kegiatan_code='$kegiatan_code'
                            //     and subtitle='Penunjang Kinerja'";                             
                            // $stmt2 = $con->prepareStatement($query2);
                            // $stmt2->executeQuery();                            
                         
                        } else {
                            $ada_error[] = "Komponen di kegiatan ".$kegiatan_code." tidak ditemukan.";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Volume UK di Rincian Detail Sudah di rubah');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

     public function executeUploadUbahKomponenRevisi() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'ubahkomponen_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {

                    
                    $kegiatan_code = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $komponen_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $komponen_name = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $volume_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    $keterangan_koefisien = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $volume_orang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));


                    if ( $kegiatan_code!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kegiatan_code);   

                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {

                            $query1 = "update ebudget.dinas_rincian_detail set komponen_id='$komponen_id', volume=$volume_baru , komponen_name='$komponen_name',satuan='$satuan',komponen_harga=$harga, komponen_harga_awal=$harga, pajak=$pajak, keterangan_koefisien='$keterangan_koefisien',volume_orang=$volume_orang where detail_kegiatan='$kegiatan_code'";                                                        
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery(); 

                            // $query1 = "update ebudget.dinas_rincian_detail set komponen_id='$komponen_id', volume=$volume_baru , komponen_name='$komponen_name',satuan='$satuan',pajak=$pajak, keterangan_koefisien='$keterangan_koefisien',volume_orang=$volume_orang where detail_kegiatan='$kegiatan_code'";                                                        
                            // $stmt1 = $con->prepareStatement($query1);
                            // $stmt1->executeQuery(); 
                            
                            // $query1 = "update ebudget.dinas_rincian_detail set volume=$volume_baru , keterangan_koefisien=concat($volume_baru,' Poin') where kegiatan_code='$kegiatan_code' and subtitle='Penunjang Kinerja' and status_hapus=false and komponen_name ilike '%Uang Kinerja%'";                             
                            // $stmt1 = $con->prepareStatement($query1);
                            // $stmt1->executeQuery(); 

                            // $query2 = "update ebudget.dinas_subtitle_indikator set nilai=$volume_baru,alokasi_dana=$volume_baru where kegiatan_code='$kegiatan_code'
                            //     and subtitle='Penunjang Kinerja'";                             
                            // $stmt2 = $con->prepareStatement($query2);
                            // $stmt2->executeQuery();                            
                         
                        } else {
                            $ada_error[] = "Komponen di kegiatan ".$kegiatan_code." tidak ditemukan.";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Komponen di Rincian Detail Sudah di rubah');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

     public function executeUbahRekeningKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    
                    $kode_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $rekening_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $rekening_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));                   
                    if ($kode_komponen!='' &&  $rekening_lama!='' && $rekening_baru != '') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(KomponenPeer::KOMPONEN_ID, $kode_komponen);

                         if (KomponenPeer::doSelectOne($c_cek_kegiatan)) {                           

                            $query1 = "update ebudget.komponen
                                       set rekening=replace(rekening,'$rekening_lama','$rekening_baru')
                                       where komponen_id='$kode_komponen'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                            $c_cek= new Criteria();                         
                            $c_cek->add(KomponenRekeningPeer::KOMPONEN_ID, $kode_komponen);
                            $c_cek->add(KomponenRekeningPeer::REKENING_CODE, $rekening_lama);
                            $c_cek->addOr(KomponenRekeningPeer::REKENING_CODE, $rekening_baru);
                            if (KomponenRekeningPeer::doSelectOne($c_cek)) {
                            $query21 = "delete from ebudget.komponen_rekening                                      
                                       where komponen_id='$kode_komponen' and rekening_code='$rekening_lama'";          
                            $stmt21 = $con->prepareStatement($query21);
                            $stmt21->executeQuery();  

                            }
                            else
                            {
                                $c_cek1= new Criteria();                         
                                $c_cek1->add(KomponenRekeningPeer::KOMPONEN_ID, $kode_komponen);
                                $c_cek1->add(KomponenRekeningPeer::REKENING_CODE, $rekening_lama);
                                if (KomponenRekeningPeer::doSelectOne($c_cek1)) { 

                                    $query2 = "update ebudget.komponen_rekening
                                               set rekening_code='$rekening_baru'
                                               where komponen_id='$kode_komponen' and rekening_code='$rekening_lama'";          
                                    $stmt2 = $con->prepareStatement($query2);
                                    $stmt2->executeQuery();
                                
                                }
                                else
                                {
                                     $query2 = "insert into ebudget.komponen_rekening (
                                    rekening_code, komponen_id)
                                 values ('$rekening_baru','$kode_komponen')
                                      ";          
                                $stmt2 = $con->prepareStatement($query2);
                                $stmt2->executeQuery();
                            }
                          }
                         
                        } else {
                            $ada_error[] = "kode komponen ".$kode_komponen." tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Ubah Rekening Komponen telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeTandaiKomponen() {
        $con = Propel::getConnection();
        $con->begin();        
        try {
            $c = new Criteria();
            $rs_usulan = KomponenPeer::doSelect($c);

            $ada_error = array(); 
            $komp_honorer = array('2.1.1.01.01.01.008','2.1.1.03.05.01.001', '2.1.1.01.01.02.099', '2.1.1.01.01.01.004', '2.1.1.01.01.01.005', '2.1.1.01.01.01.006');        
            
            foreach ($rs_usulan as $value) {
                if($value->getKomponenName()=='Iuran JKN Tenaga Operasional')
                {
                     $query1 = "update ebudget.komponen
                                       set is_iuran_jkn=true,is_iuran_bpjs=true
                                       where komponen_name ='Iuran JKN Tenaga Operasional'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();                           

                }
                elseif($value->getKomponenName()=='Iuran JKK Tenaga Operasional')
                {
                     $query1 = "update ebudget.komponen
                                       set is_iuran_jkk=true,is_iuran_bpjs=true
                                       where komponen_name ='Iuran JKK Tenaga Operasional'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();                           

                }
                elseif($value->getKomponenName()=='Iuran JK Tenaga Operasional')
                {
                     $query1 = "update ebudget.komponen
                                       set is_iuran_jk=true,is_iuran_bpjs=true
                                       where komponen_name ='Iuran JK Tenaga Operasional'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();                           

                }
                elseif(strpos($value->getKomponenName(), 'Narasumber') !== FALSE)
                {
                     $query1 = "update ebudget.komponen
                                       set is_narsum=true
                                       where komponen_name  ilike '%Narasumber%'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();                        
                }
                elseif(strpos($value->getKomponenName(), 'BBM') !== FALSE)
                {
                     $query1 = "update ebudget.komponen
                                       set is_bbm=true
                                       where komponen_name  ilike 'BBM%'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();                        
                }
                elseif(strpos(trim($value->getSatuan()), 'Orang Bulan') !== FALSE)
                {
                    //Cek komponen sesuai list komponen honorer
                    foreach ($komp_honorer as $key => $komp) {
                        if( strpos($value->getKomponenId(), $komp) !== FALSE)
                        {

                            $query1 = "update ebudget.komponen
                                       set is_potong_bpjs=true
                                       where satuan='Orang Bulan' and komponen_id ilike '".$komp."%'";                            
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();       

                        }

                    }
                                      
                }

            }

            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Penanda Komponen sudah telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
            //print_r($hasil);exit();          
        }
        catch (Exception $ex) {           
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
         return $this->redirect('admin/fiturTambahan');
        
    }

     public function executeUpdateRekening() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    
                    //$kode_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $rekening_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $rekening_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue())); 
                    $nama_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));                  
                    if ($rekening_lama!='' && $rekening_baru != '') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(RekeningPeer::REKENING_CODE, $rekening_lama);

                         if (RekeningPeer::doSelectOne($c_cek_kegiatan)) {                           

                            $query1 = "update ebudget.komponen
                                       set rekening=replace(rekening,'$rekening_lama','$rekening_baru')
                                       where rekening ilike '%$rekening_lama%'";                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                            $query2 = "update ebudget.komponen_rekening
                                       set rekening_code='$rekening_baru'
                                       where rekening_code='$rekening_lama'";          
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();

                            $query3 = "update ebudget.rekening
                                       set rekening_name='$nama_baru',rekening_code='$rekening_baru'
                                       where rekening_code ='$rekening_lama'";                             
                            $stmt3 = $con->prepareStatement($query3);
                            $stmt3->executeQuery();
                         
                        } else {
                            $ada_error[] = "kode rekening ". $rekening_lama." tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Ubah Rekening Komponen telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

     public function executeUpdateKomponenRekening() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    
                    $kode_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $rekening_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $rekening_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));                   
                    if ($rekening_lama!='' && $rekening_baru != '') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(KomponenRekeningPeer::REKENING_CODE, $rekening_lama);
                         $c_cek_kegiatan->add(KomponenRekeningPeer::KOMPONEN_ID, $kode_komponen);


                         if (RekeningPeer::doSelectOne($c_cek_kegiatan)) {                           
                            //cek kalau udh ada rekening baru
                            $c_cek = new Criteria();                         
                            $c_cek->add(KomponenRekeningPeer::REKENING_CODE, $rekening_baru);
                            $c_cek->add(KomponenRekeningPeer::KOMPONEN_ID, $kode_komponen);
                            if (RekeningPeer::doSelectOne($c_cek)) {
                            }
                            else
                            {
                            $query2 = "update ebudget.komponen_rekening
                                       set rekening_code='$rekening_baru'
                                       where komponen_id='$kode_komponen' and rekening_code='$rekening_lama'";
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();
                            }
                         
                        } 
                        // else {
                        //     $ada_error[] = "kode ".$kode_komponen." dengn kode rekening ". $rekening_baru." tidak ada di dalam database";

                        // }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Ubah Rekening Komponen telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

     public function executeTambahBukaKomponenMurni() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'bukakomponen_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                $kode_temp='';
                for ($row = 0; $row <= $highR; $row++) {                    
                    $kode_detail_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));   
                    //$rekening_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    if ($kode_detail_kegiatan != '') {                    
                                          
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode_detail_kegiatan);

                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {                                   

                             $query2 = "insert into ebudget.buka_komponen_murni (detail_kegiatan)
                             values('$kode_detail_kegiatan')";        
                             $stmt2 = $con->prepareStatement($query2);
                             $stmt2->executeQuery();                         
                        } else {
                            $ada_error[] = "kode detail_kegiatan ".  $kode_detail_kegiatan." tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambah Buka Komponen telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeTambahLepasValidasi() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'bukavalidasidetail_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                $kode_temp='';
                for ($row = 0; $row <= $highR; $row++) {                    
                    $kode_detail_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));   
                    //$rekening_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    if ($kode_detail_kegiatan != '') {                    
                                          
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode_detail_kegiatan);

                         if (DinasRincianDetailPeer::doSelectOne($c_cek_kegiatan)) {                                   

                             $query2 = "insert into ebudget.bypass_detail (detail_kegiatan)
                             values('$kode_detail_kegiatan')";        
                             $stmt2 = $con->prepareStatement($query2);
                             $stmt2->executeQuery();                         
                        } else {
                            $ada_error[] = "isian mulai dari kolom 0";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambah Buka Validasi by detail kegiatan telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeTambahRekeningKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                $kode_temp='';
                for ($row = 0; $row <= $highR; $row++) {                    
                    $kode_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));   
                    $rekening_baru = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    if ($rekening_baru != '') {  

                    if($kode_komponen!='')
                    {
                       $kode_temp=$kode_komponen;
                       $kode_komponen=$kode_komponen;
                    } 
                    else
                    {
                      $kode_komponen=$kode_temp;
                    }

                                          
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(KomponenPeer::KOMPONEN_ID, $kode_komponen);

                         if (KomponenPeer::doSelectOne($c_cek_kegiatan)) {                           

                             $query1 = "update ebudget.komponen
                                        set rekening=concat(rekening,'/','$rekening_baru')
                                        where komponen_id='$kode_komponen'";                             
                                $stmt1 = $con->prepareStatement($query1);
                                $stmt1->executeQuery();                          

                             $query2 = "insert into ebudget.komponen_rekening (rekening_code,komponen_id)
                             values('$rekening_baru','$kode_komponen')";        
                             $stmt2 = $con->prepareStatement($query2);
                             $stmt2->executeQuery();
                         
                        } else {
                            $ada_error[] = "kode komponen ". $kode_komponen." tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambah Rekening Komponen telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

public function executeHapusRekeningKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahanpagu_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                $kode_temp='';
                for ($row = 0; $row <= $highR; $row++) {                    
                    $kode_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));   
                    $rekening_lama = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    if ($rekening_lama != '') {  

                    if($kode_komponen!='')
                    {
                       $kode_temp=$kode_komponen;
                       $kode_komponen=$kode_komponen;
                    } 
                    else
                    {
                      $kode_komponen=$kode_temp;
                    }

                                          
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(KomponenPeer::KOMPONEN_ID, $kode_komponen);

                         if (KomponenPeer::doSelectOne($c_cek_kegiatan)) {                           

                             $query1 = "update ebudget.komponen
                                        set rekening=replace(rekening,'/".$rekening_lama."','')
                                        where komponen_id='$kode_komponen'";                             
                                $stmt1 = $con->prepareStatement($query1);
                                $stmt1->executeQuery();  

                             $query11 = "update ebudget.komponen
                                        set rekening=replace(rekening,'".$rekening_lama."/','')
                                        where komponen_id='$kode_komponen'";                             
                                $stmt11 = $con->prepareStatement($query11);
                                $stmt11->executeQuery();                         

                             $query2 = "delete from ebudget.komponen_rekening
                              where komponen_id='$kode_komponen' and rekening_code='$rekening_lama'";        
                             $stmt2 = $con->prepareStatement($query2);
                             $stmt2->executeQuery();
                         
                        } else {
                            $ada_error[] = "kode komponen ". $kode_komponen." tidak ada di dalam database";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Hapus Rekening Komponen telah selesai.');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKelurahanTambah() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahankelurahan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                   
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $komponen_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $nama_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $jumlah = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    $volume_orang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $keterangan_koefisien = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                    $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                   // $detail_name = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    //$accres = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));

                                        
                    if ($kode_kegiatan!='' & $jumlah!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            $query1 = "select * from ebudget.dinas_master_kegiatan 
                        where kode_kegiatan= '$kode_kegiatan' 
                       ";
                      //die($kode_kegiatan);

                    $stmt1 = $con->prepareStatement($query1);
                    $rs1 = $stmt1->executeQuery();
                    if($rs1->next()) {
                        $unit_id = $rs1->getString('unit_id');
                        $kegiatan_code = $rs1->getString('kode_kegiatan');
                        $kegiatan_name = $rs1->getString('nama_kegiatan');
                        $kegiatan_id  = $rs1->getString('kegiatan_id');
                       // die($kegiatan_code);
                        $querys = "select max(detail_no) as ok,max(status_level) as level from ebudget.dinas_rincian_detail
                        where kegiatan_code= '$kegiatan_code' ";
                        $stmts = $con->prepareStatement($querys);
                        $rss = $stmts->executeQuery();
                        if($rss->next())
                        {

                        $angka=$rss->getInt('ok');
                        // if($accres == '')
                        // {
                        //     $accres=0;
                        // }

                        //die($detail_name.'-'.$accres);
                        $detail_no = $angka+1;
                        // $detail2 = $rss->getInt('ok')+2;
                        // $detail3 = $rss->getInt('ok')+3;
                        // $detail4 = $rss->getInt('ok')+4;
                        //$status_level = $rss->getInt('level');
                        //die($rss->getInt('ok').'-'.$rss->getString('ok').'-'.$detail_no);
                        $status_level = 0;
                        $subtitle =$kegiatan_name;
                        $volume = $jumlah;
                        //$rekening = '5.1.02.02.01.0004';
                        //$vol_jk = $volume * 4200479.190;
                        //die($angka.'-'.$detail_no);
                        // die($unit_id.'-'.$detail_no.''.$kegiatan_code.'-'.$kegiatan_id.'-'.$subtitle.'-'.$rekening.'-'.$komponen_id.'-'.$volume.'-'.$keterangan_koefisien.'-'.$harga.'-'.$nama_komponen.'-'.$satuan.'-'.$status_level);
                                              
                        $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (unit_id, kegiatan_code,kegiatan_id, detail_no, tipe, rekening_code,
                        komponen_id,volume, keterangan_koefisien, subtitle,
                        komponen_harga, komponen_harga_awal, komponen_name, satuan, 
                        pajak, tahap, tahap_edit, tahun,is_potong_bpjs, status_level,
                        is_iuran_bpjs, is_iuran_jkn, is_iuran_jkk, is_persediaan, is_musrenbang,
                        is_rab, is_output, detail_kegiatan, prioritas_wali,kode_sub,status_hapus,accres)
                        VALUES
                        (
                            '$unit_id',$kegiatan_code,'$kegiatan_id',$detail_no,'SSH','$rekening',
                            '$komponen_id',$volume,'$keterangan_koefisien', '$subtitle',
                            $harga,$harga,'$nama_komponen','$satuan',
                            $pajak,'murni','murni','2022',false,$status_level, 
                            false,false,false,false,false,
                            false,false,'".$unit_id.".".$kegiatan_code.".".$detail_no."',false,'',false,0
                        )";
                     
                        //echo $query3; $con->rollback(); exit;
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                      
                    }  
                    
                    }        

                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }                    
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambahan komponen telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    public function executeUploadKelurahan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahankelurahan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                   
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $komponen_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $nama_komponen = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $satuan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $jumlah = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $harga = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                    $rekening = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    $volume_orang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()));
                    $keterangan_koefisien = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(8, $row)->getValue()));
                    $detail_name = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    $accres = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue()));
                    $pajak = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()));

                                        
                    if ($kode_kegiatan!='' & $jumlah!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            $query1 = "select * from ebudget.dinas_master_kegiatan 
                        where kode_kegiatan= '$kode_kegiatan' 
                       ";
                      // die($kode_kegiatan);

                    $stmt1 = $con->prepareStatement($query1);
                    $rs1 = $stmt1->executeQuery();
                    if($rs1->next()) {
                        $unit_id = $rs1->getString('unit_id');
                        $kegiatan_code = $rs1->getString('kode_kegiatan');
                        $kegiatan_name = $rs1->getString('nama_kegiatan');
                        $kegiatan_id  = $rs1->getString('kegiatan_id');
                      // die($kegiatan_code);
                        // $querys = "select max(detail_no) as ok,max(status_level) as level from ebudget.dinas_rincian_detail
                        // where kegiatan_code= '$kegiatan_code' ";
                        // $stmts = $con->prepareStatement($querys);
                        // $rss = $stmts->executeQuery();
                        // if($rss->next())
                        // {

                        $detail_no=$row+1;

                        //$detail_no = $rss->getInt('ok')+1;
                        // $detail2 = $rss->getInt('ok')+2;
                        // $detail3 = $rss->getInt('ok')+3;
                        // $detail4 = $rss->getInt('ok')+4;
                        //$status_level = $rss->getInt('level');
                        $status_level=0;
                        $subtitle =$kegiatan_name ;
                        $volume = $jumlah;
                        //die($pajak."-".$detail_no);
                        //$rekening = '5.1.02.02.01.0004';
                        //$vol_jk = $volume * 4200479.190;
                                              
                        $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (unit_id, kegiatan_code,kegiatan_id, detail_no, tipe, rekening_code,
                        komponen_id, volume_orang,volume, keterangan_koefisien, subtitle,
                        komponen_harga, komponen_harga_awal, komponen_name, satuan, 
                        pajak, tahap, tahap_edit, tahun,is_potong_bpjs, status_level,
                        is_iuran_bpjs, is_iuran_jkn, is_iuran_jkk, is_persediaan, is_musrenbang,
                        is_rab, is_output, detail_kegiatan, prioritas_wali,kode_sub,status_hapus,detail_name,accres)
                        VALUES
                        (
                            '$unit_id','$kegiatan_code','$kegiatan_id',$detail_no,'SSH','$rekening',
                            '$komponen_id',$volume_orang,$volume,'$keterangan_koefisien', '$subtitle',
                            $harga,$harga,'$nama_komponen','$satuan',
                            $pajak,'murni',  'murni','2022',false,$status_level, 
                            false,false,false,false,false,
                            false,false,'".$unit_id.".".$kegiatan_code.".".$detail_no."',false,'',false,'$detail_name',$accres
                        )";
                     
                        //echo $query3; $con->rollback(); exit;
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                      
                    // }  
                    
                    }        

                        } else {
                            $ada_error[] = "kode kegiatan ".$kode_kegiatan."tidak ada di dalam database";

                        }
                    }                    
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambahan komponen telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    
    public function executeUploadMakananAlamat() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahankelurahan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                   
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $jumlah = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                        
                    if ($kode_kegiatan!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            $query1 = "select * from ebudget.dinas_master_kegiatan 
                        where kode_kegiatan= '$kode_kegiatan' 
                       ";
                      // die($kode_kegiatan);

                    $stmt1 = $con->prepareStatement($query1);
                    $rs1 = $stmt1->executeQuery();
                    if($rs1->next()) {
                        $unit_id = $rs1->getString('unit_id');
                        $kegiatan_code = $rs1->getString('kode_kegiatan');
                        $kegiatan_name = $rs1->getString('nama_kegiatan');
                      // die($kegiatan_code);
                         $querys = "select max(detail_no) as ok from ebudget.dinas_rincian_detail
                        where status_hapus=false and kegiatan_code= '$kegiatan_code' 
                         ";
                        $stmts = $con->prepareStatement($querys);
                        $rss = $stmts->executeQuery();
                        if($rss->next())
                        {

                        $detail_no = $rss->getInt('ok')+1;
                        $subtitle =$kegiatan_name;
                        $volume = $jumlah;
                                              
                        $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (unit_id, kegiatan_code, detail_no, tipe, rekening_code,
                        komponen_id, volume,keterangan_koefisien, subtitle,
                        komponen_harga, komponen_harga_awal, komponen_name, satuan, 
                        pajak, tahap, tahap_edit, tahun,is_potong_bpjs, 
                        is_iuran_bpjs, is_iuran_jkn, is_iuran_jkk, is_persediaan, is_musrenbang,
                        is_rab, is_output, detail_kegiatan, prioritas_wali,kode_sub,status_hapus)
                        VALUES
                        (
                            '$unit_id','$kegiatan_code',$detail_no,'SSH','5.2.2.03.07',
                            '2.1.1.01.02.02.007.008',$volume,'$keterangan','$subtitle',
                            500,500,'Biaya Pengiriman Permakanan Lansia/ PACA/Yatim', 'Alamat Hari',0,
                            'murni','murni','2020',false,false,false,false,false,false,
                            false,false,'".$unit_id.".".$kegiatan_code.".".$detail_no."',false,'',false
                        )";
                     
                        //echo $query3; $con->rollback(); exit;
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                      
                        }  
                    
                    }        

                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }                    
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'SIkat Makanan telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadMakananOrang() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahankelurahan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                   
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $jumlah = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));

                    if ($kode_kegiatan!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            $query1 = "select * from ebudget.dinas_master_kegiatan 
                        where kode_kegiatan= '$kode_kegiatan' 
                       ";
                      // die($kode_kegiatan);

                    $stmt1 = $con->prepareStatement($query1);
                    $rs1 = $stmt1->executeQuery();
                    if($rs1->next()) {
                        $unit_id = $rs1->getString('unit_id');
                        $kegiatan_code = $rs1->getString('kode_kegiatan');
                        $kegiatan_name = $rs1->getString('nama_kegiatan');
                      // die($kegiatan_code);
                         $querys = "select max(detail_no) as ok from ebudget.dinas_rincian_detail
                        where status_hapus=false and kegiatan_code= '$kegiatan_code' 
                         ";
                        $stmts = $con->prepareStatement($querys);
                        $rss = $stmts->executeQuery();
                        if($rss->next())
                        {

                        $detail_no = $rss->getInt('ok')+1;
                        $subtitle =$kegiatan_name;
                        $volume = $jumlah;
                                              
                        $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (unit_id, kegiatan_code, detail_no, tipe, rekening_code,
                        komponen_id, volume,keterangan_koefisien, subtitle,
                        komponen_harga, komponen_harga_awal, komponen_name, satuan, 
                        pajak, tahap, tahap_edit, tahun,is_potong_bpjs, 
                        is_iuran_bpjs, is_iuran_jkn, is_iuran_jkk, is_persediaan, is_musrenbang,
                        is_rab, is_output, detail_kegiatan, prioritas_wali,kode_sub,status_hapus)
                        VALUES
                        (
                            '$unit_id','$kegiatan_code',$detail_no,'SSH','5.2.2.30.03',
                            '2.1.1.01.01.01.004.031',$volume,'$keterangan','$subtitle',
                            300000,300000,'Biaya Operasional Pelaksana Permakanan', 'Orang Bulan',0,
                            'murni','murni','2020',false,false,false,false,false,false,
                            false,false,'".$unit_id.".".$kegiatan_code.".".$detail_no."',false,'',false
                        )";
                     
                        //echo $query3; $con->rollback(); exit;
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                      
                        }  
                    
                    }        

                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }                    
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'SIkat Makanan telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadMakananOrangHari() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'tambahankelurahan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                   
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $jumlah = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $keterangan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));

                    if ($kode_kegiatan!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);                          
                         if (DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            $query1 = "select * from ebudget.dinas_master_kegiatan 
                        where kode_kegiatan= '$kode_kegiatan' 
                       ";
                      // die($kode_kegiatan);

                    $stmt1 = $con->prepareStatement($query1);
                    $rs1 = $stmt1->executeQuery();
                    if($rs1->next()) {
                        $unit_id = $rs1->getString('unit_id');
                        $kegiatan_code = $rs1->getString('kode_kegiatan');
                        $kegiatan_name = $rs1->getString('nama_kegiatan');
                      // die($kegiatan_code);
                         $querys = "select max(detail_no) as ok from ebudget.dinas_rincian_detail
                        where status_hapus=false and kegiatan_code= '$kegiatan_code' 
                         ";
                        $stmts = $con->prepareStatement($querys);
                        $rss = $stmts->executeQuery();
                        if($rss->next())
                        {

                        $detail_no = $rss->getInt('ok')+1;
                        $subtitle =$kegiatan_name;
                        $volume = $jumlah;
                                              
                        $query3 = "insert into " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                        (unit_id, kegiatan_code, detail_no, tipe, rekening_code,
                        komponen_id, volume,keterangan_koefisien, subtitle,
                        komponen_harga, komponen_harga_awal, komponen_name, satuan, 
                        pajak, tahap, tahap_edit, tahun,is_potong_bpjs, 
                        is_iuran_bpjs, is_iuran_jkn, is_iuran_jkk, is_persediaan, is_musrenbang,
                        is_rab, is_output, detail_kegiatan, prioritas_wali,kode_sub,status_hapus)
                        VALUES
                        (
                            '$unit_id','$kegiatan_code',$detail_no,'SSH','5.2.2.12.05',
                            '1.1.7.01.07.05.001.024',$volume,'$keterangan','$subtitle',
                            11000,11000,'Permakanan Lansia/PACA/Yatim', 'Orang Hari',10,
                            'murni','murni','2020',false,false,false,false,false,false,
                            false,false,'".$unit_id.".".$kegiatan_code.".".$detail_no."',false,'',false
                        )";
                     
                        //echo $query3; $con->rollback(); exit;
                        $stmt3 = $con->prepareStatement($query3);
                        $stmt3->executeQuery();
                      
                        }  
                    
                    }        

                        } else {
                            $ada_error[] = "kode kegiatan tidak ada di dalam database";

                        }
                    }                    
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'SIkat Makanan telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadVolumeOrang() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'volumeorang_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    $kode = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $volume = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    
                    if ($kode != '') {                                           
                         $c_cek_volume = new Criteria();
                         $c_cek_volume->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode);                         
                     if (DinasRincianDetailPeer::doSelectOne($c_cek_volume)) {
                        $detail=$kode;
                        $vol_sebelum="SELECT volume_orang  from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_kegiatan='".$detail."'";
                        $st = $con->prepareStatement($vol_sebelum);
                        $rs1=$st->executeQuery();
                        while($rs1->next())
                        {
                            $cek = "SELECT count(*) as hasil from  " . sfConfig::get('app_default_schema') . ".log_volume_orang where 
                            detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";                   
                            $stmt3=  $con->prepareStatement($cek);
                            $rs=$stmt3->executeQuery();
                            while($rs->next())
                            { 
                                $nilai=$rs->getString('hasil');
                                if($nilai==0)
                                {
                                   $insertvol ="INSERT INTO " . sfConfig::get('app_default_schema') . ".log_volume_orang(
                                    detail_kegiatan, volume_semula, volume_menjadi, tahap, waktu_edit)
                                    VALUES ('" . $detail. "',".$rs1->getString('volume_orang').", $volume,'".sfConfig::get('app_tahap_detail')."','".date('Y-m-d h:i:sa')."')";
                                    $stmt2 = $con->prepareStatement($insertvol);
                                    $stmt2->executeQuery();
                                }
                                else
                                {
                                    if( $vol_sebelum ==  $volume)
                                    {

                                    }
                                    else
                                    {
                                    $updatevol =
                                    "UPDATE " . sfConfig::get('app_default_schema') . ".log_volume_orang
                                    SET volume_menjadi=$volume,tahap='".sfConfig::get('app_tahap_detail')."',waktu_edit='".date('Y-m-d h:i:sa')."' where detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";
                                    $stmt2 = $con->prepareStatement($updatevol);
                                    $stmt2->executeQuery();
                                     }
                                }

                            } 
                        }
                   

                            $query =
                            "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            set volume_orang = $volume
                            WHERE detail_kegiatan='$kode'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();

                            $queryrka =
                            "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                            set volume_orang = $volume
                            WHERE detail_kegiatan='$kode'";
                            $stmt1 = $con->prepareStatement($queryrka);
                            $stmt1->executeQuery();

                            $queryrbp =
                            "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                            set volume_orang = $volume
                            WHERE detail_kegiatan='$kode'";
                            $stmt2 = $con->prepareStatement($queryrbp);
                            $stmt2->executeQuery();
                                              
                        } else {
                            $ada_error[] = "Detail kegiatan $kode tidak ada di dalam RKA";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Volume telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
public function executeUploadVolumeOrangAnggaran() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'volumeoranganggaran_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;
            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();

                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    $kode = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $volume = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    
                    if ($kode != '') {                                           
                         $c_cek_volume = new Criteria();
                         $c_cek_volume->add(DinasRincianDetailPeer::DETAIL_KEGIATAN, $kode);                         
                     if (DinasRincianDetailPeer::doSelectOne($c_cek_volume)) {
                        $detail=$kode;
                        // $vol_sebelum="SELECT volume_orang  from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_kegiatan='".$detail."'";
                        // $st = $con->prepareStatement($vol_sebelum);
                        // $rs1=$st->executeQuery();
                        // while($rs1->next())
                        // {
                        //     $cek = "SELECT count(*) as hasil from  " . sfConfig::get('app_default_schema') . ".log_volume_orang where 
                        //     detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";                   
                        //     $stmt3=  $con->prepareStatement($cek);
                        //     $rs=$stmt3->executeQuery();
                        //     while($rs->next())
                        //     { 
                        //         $nilai=$rs->getString('hasil');
                        //         if($nilai==0)
                        //         {
                        //            $insertvol ="INSERT INTO " . sfConfig::get('app_default_schema') . ".log_volume_orang(
                        //             detail_kegiatan, volume_semula, volume_menjadi, tahap, waktu_edit)
                        //             VALUES ('" . $detail. "',".$rs1->getString('volume_orang').", $volume,'".sfConfig::get('app_tahap_detail')."','".date('Y-m-d h:i:sa')."')";
                        //             $stmt2 = $con->prepareStatement($insertvol);
                        //             $stmt2->executeQuery();
                        //         }
                        //         else
                        //         {
                        //             if( $vol_sebelum ==  $volume)
                        //             {

                        //             }
                        //             else
                        //             {
                        //             $updatevol =
                        //             "UPDATE " . sfConfig::get('app_default_schema') . ".log_volume_orang
                        //             SET volume_menjadi=$volume,tahap='".sfConfig::get('app_tahap_detail')."',waktu_edit='".date('Y-m-d h:i:sa')."' where detail_kegiatan='".$detail."' and tahap='".sfConfig::get('app_tahap_detail')."'";
                        //             $stmt2 = $con->prepareStatement($updatevol);
                        //             $stmt2->executeQuery();
                        //              }
                        //         }

                        //     } 
                        // }
                   

                            $query =
                            "UPDATE " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                            set volume_orang_anggaran = $volume
                            WHERE detail_kegiatan='$kode'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();

                            $queryrka =
                            "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail
                            set volume_orang_anggaran = $volume
                            WHERE detail_kegiatan='$kode'";
                            $stmt1 = $con->prepareStatement($queryrka);
                            $stmt1->executeQuery();

                            $queryrbp =
                            "UPDATE " . sfConfig::get('app_default_schema') . ".rincian_detail_bp
                            set volume_orang_anggaran = $volume
                            WHERE detail_kegiatan='$kode'";
                            $stmt2 = $con->prepareStatement($queryrbp);
                            $stmt2->executeQuery();
                                              
                        } else {
                            $ada_error[] = "Detail kegiatan $kode tidak ada di dalam RKA";

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Volume Orang Anggaran telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    public function executeUploadPrioritas() {
    $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'prioritas' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) 
            {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);

            $objPHPExcel->getActiveSheetIndex(0);
            $objWorksheet = $objPHPExcel->getActiveSheet();

            $highR = $objWorksheet->getHighestRow();
            for ($row = 0; $row <= $highR; $row++) {
                $detail = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $prioritas_id = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());   

                // $query2 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                //             . " set prioritas_id=$prioritas_id "
                //             . " where detail_kegiatan='$detail' ";                            
                //     $stmt2 = $con->prepareStatement($query2);
                //     $stmt2->executeQuery();
     
                
                //update ke RKA                  
                $query1 = "select * from ebudget.dinas_rincian_detail
                        where detail_kegiatan='$detail' ";
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                while ($rs1->next()) {                    
                    
                    $query2 = " update " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                            . " set prioritas_id=$prioritas_id "
                            . " where detail_kegiatan='$detail' ";                            
                    $stmt2 = $con->prepareStatement($query2);
                    $stmt2->executeQuery();

                    $query3 = " update " . sfConfig::get('app_default_schema') . ".rincian_detail "
                            . " set prioritas_id=$prioritas_id "
                            . " where detail_kegiatan='$detail' ";                            
                    $stmt3 = $con->prepareStatement($query3);
                    $stmt3->executeQuery();

                    
                    $query4 = " update " . sfConfig::get('app_default_schema') . ".rincian_detail_bp "
                            . " set prioritas_id=$prioritas_id "
                            . " where detail_kegiatan='$detail' ";                            
                    $stmt4 = $con->prepareStatement($query4);
                    $stmt4->executeQuery();

                    
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Berhasil untuk mengupadate id prioritas');
                       return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }


    public function executeUbahIdKomponen() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = $highR; $row >= 2; $row--) {
//              A=kosong B=id_semula C=id_selanjutnya 
                $id_lama = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $id_baru = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());

                $c_cek_komponen = new Criteria();
                $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $id_lama);
                $dapat_komponen = KomponenPeer::doSelectOne($c_cek_komponen);
                if ($dapat_komponen) {
                    $query_update_id = "update ebudget.komponen "
                            . "set komponen_id = '$id_baru' "
                            . "where komponen_id = '$id_lama' ";
                    $stmt_update_id = $con->prepareStatement($query_update_id);
                    $stmt_update_id->executeQuery();
                } else {
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal karena tidak ditemukan id untuk ' . $id_lama);
                    return $this->redirect('admin/fiturTambahan');
                }

                $c_cek_komponen_rekening = new Criteria();
                $c_cek_komponen_rekening->add(KomponenRekeningPeer::KOMPONEN_ID, $id_lama);
                $dapat_komponen_rekening = KomponenRekeningPeer::doSelect($c_cek_komponen_rekening);
                if ($dapat_komponen_rekening) {
                    $query_update_id = "update ebudget.komponen_rekening "
                            . "set komponen_id = '$id_baru' "
                            . "where komponen_id = '$id_lama' ";
                    $stmt_update_id = $con->prepareStatement($query_update_id);
                    $stmt_update_id->executeQuery();
                }

                $c = new Criteria();
                $c->add(RincianDetailPeer::KOMPONEN_ID, $id_lama);
                $c->addAnd(RincianDetailPeer::STATUS_HAPUS, FALSE);
                $dapat_rd = RincianDetailPeer::doSelect($c);
                if ($dapat_rd) {
                    $query_update_id = "update ebudget.rincian_detail "
                            . "set komponen_id = '$id_baru' "
                            . "where komponen_id = '$id_lama' ";
                    $stmt_update_id = $con->prepareStatement($query_update_id);
                    $stmt_update_id->executeQuery();
                }
            }
            $con->commit();
            $this->setFlash('berhasil', 'Sikat Ganti Komponen ID Berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeSwitchrekeningrame() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 0; $row <= $highR; $row++) {

                $kode_rka = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $rekening_menjadi = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());

                $query_update_rekening = "update ebudget.dinas_rincian_detail "
                        . "set rekening_code = '$rekening_menjadi'  "
                        . "where detail_kegiatan = '$kode_rka'  ";
                $stmt_update_rekening = $con->prepareStatement($query_update_rekening);
                $stmt_update_rekening->executeQuery();

                // $query_update_rka = "update ebudget.rincian_detail "
                //         . "set rekening_code = '$rekening_menjadi'  "
                //         . "where detail_kegiatan = '$kode_rka'  ";
                // $stmt_update_rka = $con->prepareStatement($query_update_rka);
                // $stmt_update_rka->executeQuery();

                // $query_update_bp = "update ebudget.rincian_detail_bp "
                //         . "set rekening_code = '$rekening_menjadi'  "
                //         . "where detail_kegiatan = '$kode_rka'  ";
                // $stmt_update_bp = $con->prepareStatement($query_update_bp);
                // $stmt_update_bp->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Sikat Ganti Kode Rekening berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeIsikodeDPA() {
        set_time_limit(0);

        $fileName = $this->getRequest()->getFileName('file');
        $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName);

        $file_path = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName;
        if (!file_exists($file_path)) {
            exit("CEK FILE.\n");
        }
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load($file_path);

        $objPHPExcel->getActiveSheetIndex(0);
        $objWorksheet = $objPHPExcel->getActiveSheet();

        $highR = $objWorksheet->getHighestRow();
        $highC = $objWorksheet->getHighestColumn();

        $con = Propel::getConnection();
        $con->begin();
        try {
            for ($row = 2; $row <= $highR; $row++) {

                $unit_id_keuangan = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $kode_kegiatan_keuangan = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $nomor_dpa_keuangan = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());

                $kode_kegiatan_budget = str_replace(' ', '.', $kode_kegiatan_keuangan);

                $unit_id_budget = '';

                $query_ambil_unit_id_budget = "select unit_id "
                        . "from ebudget.unit_kerja_keuangan "
                        . "where unit_keuangan_id = '$unit_id_keuangan' ";
                $stmt_ambil_unit_id_budget = $con->prepareStatement($query_ambil_unit_id_budget);
                $rs_ambil_unit_id_budget = $stmt_ambil_unit_id_budget->executeQuery();
                while ($rs_ambil_unit_id_budget->next()) {
                    $unit_id_budget = $rs_ambil_unit_id_budget->getString('unit_id');
                }


                $query_update_master_kegiatan_bp = "update ebudget.dinas_master_kegiatan "
                        . "set kode_dpa = '$nomor_dpa_keuangan' "
                        . "where unit_id = '$unit_id_budget' and kode_kegiatan = '$kode_kegiatan_budget' ";
                $stmt_update_master_kegiatan_bp = $con->prepareStatement($query_update_master_kegiatan_bp);
                $stmt_update_master_kegiatan_bp->executeQuery();

                // $query_update_master_kegiatan = "update ebudget.master_kegiatan "
                //         . "set kode_dpa = '$nomor_dpa_keuangan' "
                //         . "where unit_id = '$unit_id_budget' and kode_kegiatan = '$kode_kegiatan_budget' ";
                // $stmt_update_master_kegiatan = $con->prepareStatement($query_update_master_kegiatan);
                // $stmt_update_master_kegiatan->executeQuery();

                // $query_update_master_kegiatan_bp = "update ebudget.master_kegiatan_bp "
                //         . "set kode_dpa = '$nomor_dpa_keuangan' "
                //         . "where unit_id = '$unit_id_budget' and kode_kegiatan = '$kode_kegiatan_budget' ";
                // $stmt_update_master_kegiatan_bp = $con->prepareStatement($query_update_master_kegiatan_bp);
                // $stmt_update_master_kegiatan_bp->executeQuery();
            }
            $con->commit();
            $this->setFlash('berhasil', 'Sikat Isi Kode DPA berhasil');
            return $this->redirect('admin/fiturTambahan');
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $exc->getMessage());
            return $this->redirect('admin/fiturTambahan');
        }
    }

    public function executeUploadUbahSubKegiatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'updateStatusLevel_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));
                    $kode_urusan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));
                    $kode_bidang = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));
                    $kode_program = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));
                    $kode_keg = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));      
                    $kode_sub_keg = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue()));
                    if ($kode_kegiatan!='' &&  $unit_id!='') {                        
                         $c_cek_kegiatan = new Criteria();                         
                         $c_cek_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan); 
                         if ($kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_cek_kegiatan)) {
                            $query = "update ebudget.dinas_master_kegiatan set kode_urusan='$kode_urusan', kode_bidang='$kode_bidang', kode_program2='$kode_program', kode_head_kegiatan = '$kode_keg', kegiatan_id = '$kode_sub_keg' where kode_kegiatan='$kode_kegiatan' and unit_id = '$unit_id'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();

                            $query1 = "update ebudget.dinas_rincian set kegiatan_id = '$kode_sub_keg' where kegiatan_code='$kode_kegiatan' and unit_id = '$unit_id'";
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                            $query2 = "update ebudget.dinas_subtitle_indikator set kegiatan_id = '$kode_sub_keg' where kegiatan_code='$kode_kegiatan' and unit_id = '$unit_id'";
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();

                            $query3 = "update ebudget.dinas_rincian_detail set kegiatan_id = '$kode_sub_keg' where kegiatan_code='$kode_kegiatan' and unit_id = '$unit_id'";
                            $stmt3 = $con->prepareStatement($query3);
                            $stmt3->executeQuery();
                        } 
                        // else {
                        //     $ada_error[] = "kode kegiatan tidak ada di dalam database".$kode_kegiatan;

                        // }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Kode Sub telah terupdate');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    public function executeDashboard() {

        $query = "select k.id, k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2 
        from ebudget.dinas_rincian_detail detail2,ebudget.rekening r, ebudget.kelompok_belanja k 
        where r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id and detail2.status_hapus=false and k.id = 9
        group by k.id, k.belanja_name";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $this->rs_rekening = $stmt->executeQuery();

        $query = "select k.id, k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2 
        from ebudget.dinas_rincian_detail detail2,ebudget.rekening r, ebudget.kelompok_belanja k 
        where r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id and detail2.status_hapus=false and k.id = 10
        group by k.id, k.belanja_name";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $this->rs_rekening_bj = $stmt->executeQuery();

        $query = "select k.id, k.belanja_name, sum(detail2.nilai_anggaran) as hasil_kali2 
        from ebudget.dinas_rincian_detail detail2,ebudget.rekening r, ebudget.kelompok_belanja k 
        where r.rekening_code=detail2.rekening_code and k.belanja_id=r.belanja_id and detail2.status_hapus=false and k.id = 11
        group by k.id, k.belanja_name";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $this->rs_rekening_md = $stmt->executeQuery();

        $query = "select sum(detail2.nilai_anggaran) as hasil_kali2 
        from ebudget.dinas_rincian_detail detail2,ebudget.rekening r
        where r.rekening_code=detail2.rekening_code and detail2.status_hapus=false";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $this->rs_total = $stmt->executeQuery();

        $query ="
        select keg.unit_name, peg.bag_peg, bar.bag_bar, mod.bag_mod FROM
        (
        select r.unit_id,r.unit_name, sum(detail2.nilai_anggaran) as hasil_kali2 
                from ebudget.dinas_rincian_detail detail2,unit_kerja r
                where r.unit_id=detail2.unit_id and detail2.status_hapus=false
                GROUP BY r.unit_id
                Order BY hasil_kali2 DESC
                LIMIT 10
        ) keg
        inner join
        (
        select r.unit_id,r.unit_name, sum(detail2.nilai_anggaran) as bag_peg
                from ebudget.dinas_rincian_detail detail2,unit_kerja r, ebudget.rekening re, ebudget.kelompok_belanja k 
                where r.unit_id=detail2.unit_id and detail2.status_hapus=false and re.rekening_code=detail2.rekening_code and k.belanja_id=re.belanja_id and k.id = 9
                GROUP BY r.unit_id
                Order BY r.unit_id
        ) peg on keg.unit_id = peg.unit_id
        inner join
        (
        select r.unit_id,r.unit_name, sum(detail2.nilai_anggaran) as bag_bar
                from ebudget.dinas_rincian_detail detail2,unit_kerja r, ebudget.rekening re, ebudget.kelompok_belanja k 
                where r.unit_id=detail2.unit_id and detail2.status_hapus=false and re.rekening_code=detail2.rekening_code and k.belanja_id=re.belanja_id and k.id = 10
                GROUP BY r.unit_id
                Order BY r.unit_id
        ) bar on keg.unit_id = bar.unit_id
        inner join
        (
        select r.unit_id,r.unit_name, sum(detail2.nilai_anggaran) as bag_mod
                from ebudget.dinas_rincian_detail detail2,unit_kerja r, ebudget.rekening re, ebudget.kelompok_belanja k 
                where r.unit_id=detail2.unit_id and detail2.status_hapus=false and re.rekening_code=detail2.rekening_code and k.belanja_id=re.belanja_id and k.id = 11
                GROUP BY r.unit_id
                Order BY r.unit_id
        ) mod on keg.unit_id = mod.unit_id";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $i = 0;
        while($rs->next()) {
            $jumlah[] = $rs->getString('unit_name');
            $peg[] = $rs->getString('bag_peg');
            $bar[] = $rs->getString('bag_bar');
            $mod[] = $rs->getString('bag_mod');
            $i++;
        }
        $pegint = intval($peg);
        $this->rs_unit = json_encode($jumlah, true);
        $this->rs_peg = json_encode($peg, true);
        $this->rs_bar = json_encode($bar, true);
        $this->rs_mod = json_encode($mod, true);

        $query = "select k.kode_program2 as kode_bidang, p.nama_program2 as nama_bidang, sum(rd.nilai_anggaran) as nilai_draft3
                    from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".master_program2 p
                    where k.unit_id<>'9999' and k.unit_id=rd.unit_id and k.kode_kegiatan=rd.kegiatan_code and k.kode_program2=p.kode_program2
                    and rd.status_hapus=false
                    group by k.kode_program2, p.nama_program2
                    order by k.kode_program2";

        $query2 = "select sum(nilai_anggaran) as sum_draft
        from " . sfConfig::get('app_default_schema') . ".dinas_rincian r," . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k," . sfConfig::get('app_default_schema') . ".dinas_rincian_detail d 
        where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.status_hapus=FALSE and
        d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_draft2 = 0;
        $total_draft2 = 0;

        $program = array();
        while ($rs->next()) {
            $program[$i]['name'] = $rs->getString('nama_bidang');
            $persen_draft3_arr[$i] = $rs->getString('nilai_draft3') * 100 / $sum_draft;
            $program[$i]['y'] = (double)$persen_draft3_arr[$i];

            $i++;
        }

        $this->setprogram = json_encode($program, true);

        $this->setLayout('layoutdashboard');
    }

    public function executeUploadSubKegiatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'subkegiatan_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $nama_unit = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));                   
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));                   
                    $nama_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()));                   
                    $id_sub = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));                   
                    $kode_subkegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));                   
                    $nama_subkegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue())); 

                    $tahunSekarang = '2022';

                    if ($unit_id!='' &&  $id_sub!='' && $kode_subkegiatan!='' && $nama_subkegiatan!='') {    
                        
                        // UPDATE or INSERT di ebudget.dinas_master_kegiatan
                        $cek_master_kegiatan = new Criteria();
                        $cek_master_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
                        $cek_master_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $id_sub);
                                                
                        if (DinasMasterKegiatanPeer::doSelectOne($cek_master_kegiatan)) {
                            //Keadaan jika di db sudah ada unit_id dan kode_kegiatan
                            $query1 = "update ebudget.dinas_master_kegiatan set 
                            kegiatan_id='$kode_subkegiatan', nama_kegiatan='$nama_subkegiatan', is_btl=true 
                            where kode_kegiatan='$id_sub' and unit_id='$unit_id'";
                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                        } else {
                            //Keadaan jika di db belum ada unit_id dan kode_kegiatan
                            $query1 = "insert into ebudget.dinas_master_kegiatan (unit_id,kegiatan_id,kode_head_kegiatan,kode_kegiatan,kode_bidang,alokasi_dana,nama_kegiatan,is_btl) values('$unit_id','$kode_subkegiatan','','$id_sub','',0,'$nama_subkegiatan',true)";
                             
                            $stmt1 = $con->prepareStatement($query1);
                            $stmt1->executeQuery();

                        }
                        
                        // UPDATE or INSERT di ebudget.dinas_subtitle_indikator
                        $cek_subtitle_indikator = new Criteria();
                        $cek_subtitle_indikator->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                        $cek_subtitle_indikator->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $id_sub);
                                                
                        if (DinasSubtitleIndikatorPeer::doSelectOne($cek_subtitle_indikator)) {
                            //Keadaan jika di db sudah ada unit_id dan kode_kegiatan
                            $query2 = "update ebudget.dinas_subtitle_indikator set 
                            kegiatan_id='$kode_subkegiatan', subtitle='$nama_subkegiatan', tahun='2022' 
                            where kegiatan_code='$id_sub' and unit_id='$unit_id'";
                             
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();

                        } else {
                            //Keadaan jika di db belum ada unit_id dan kode_kegiatan
                            $query2 = "insert into ebudget.dinas_subtitle_indikator (unit_id,kegiatan_code,kegiatan_id,subtitle,tahun,prioritas,alokasi_dana) values('$unit_id','$id_sub','$kode_subkegiatan','$nama_subkegiatan','2022',1,0)";
                             
                            $stmt2 = $con->prepareStatement($query2);
                            $stmt2->executeQuery();

                        }
                        
                        // UPDATE or INSERT di ebudget.dinas_rincian
                        $cek_rincian = new Criteria();
                        $cek_rincian->add(DinasRincianPeer::UNIT_ID, $unit_id);
                        $cek_rincian->add(DinasRincianPeer::KEGIATAN_CODE, $id_sub);
                                                
                        if (DinasRincianPeer::doSelectOne($cek_rincian)) {
                            //Keadaan jika di db sudah ada unit_id dan kode_kegiatan
                            $query3 = "update ebudget.dinas_rincian set 
                            kegiatan_id='$kode_subkegiatan', rincian_level='3', lock=true, tahun='2022' 
                            where kegiatan_code='$id_sub' and unit_id='$unit_id'";
                             
                            $stmt3 = $con->prepareStatement($query3);
                            $stmt3->executeQuery();

                        } else {
                            //Keadaan jika di db belum ada unit_id dan kode_kegiatan
                            $query3 = "insert into ebudget.dinas_rincian (kegiatan_id,kegiatan_code,tipe,unit_id,rincian_level,lock,tahun,rincian_revisi_level) values('$kode_subkegiatan','$id_sub','murni','$unit_id',3,true,'2022',1)";
                             
                            $stmt3 = $con->prepareStatement($query3);
                            $stmt3->executeQuery();

                        }
                        
                        
                        // UPDATE or INSERT di ebudget.pagu
                        $cek_pagu = new Criteria();
                        $cek_pagu->add(PaguPeer::UNIT_ID, $unit_id);
                        $cek_pagu->add(PaguPeer::KODE_KEGIATAN, $id_sub);
                                                
                        if (PaguPeer::doSelectOne($cek_pagu)) {
                            //Keadaan jika di db sudah ada unit_id dan kode_kegiatan
                            $query4 = "update ebudget.pagu set 
                            is_btl=true 
                            where kode_kegiatan='$id_sub' and unit_id='$unit_id'";
                             
                            $stmt4 = $con->prepareStatement($query4);
                            $stmt4->executeQuery();

                        } else {
                            //Keadaan jika di db belum ada unit_id dan kode_kegiatan
                            $query4 = "insert into ebudget.pagu values('$unit_id','$id_sub',null,0,0,true)";
                             
                            $stmt4 = $con->prepareStatement($query4);
                            $stmt4->executeQuery();

                        }
                    }
                }
                $i++;
            }
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Tambahan pagu telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    
    public function executeTambahUserBaru() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'newuser_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $level_user = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $user_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));                   
                    $username = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));                   
                    $password = md5(utf8_encode(trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue())));     
                    $skpd = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()));     

                    $tahunSekarang = '2022';
                    $pass_default = md5('password');

                    if ($level_user!='' &&  $user_id!='' && $username!='' && $password!='' && $skpd != '') {    
                        
                        $berhasil_user = 0;
                        $berhasil_akses = 0;

                        $users_cek = new Criteria();
                        $users_cek->add(MasterUserV2Peer::USER_ID, $user_id, Criteria::EQUAL);
                        $ada_user = MasterUserV2Peer::doCount($users_cek);

                        if ($ada_user == 0) {
                            $query_insert_user = "insert into master_user_v2 "
                                    . "(user_id, user_name, user_default_password, user_password, user_enable) "
                                    . "values('$user_id','$username','$pass_default','$password',TRUE)";
                            $stmt_insert_user = $con->prepareStatement($query_insert_user);
                            $stmt_insert_user->executeQuery();

                            $berhasil_user = 1;
                        }

                        if ($berhasil_user == 1) {
                            
                            $users_cek2 = new Criteria();
                            $users_cek2->add(SchemaAksesV2Peer::USER_ID, $user_id, Criteria::EQUAL);
                            $users_cek2->add(SchemaAksesV2Peer::SCHEMA_ID, 2, Criteria::EQUAL);
                            $users_cek2->add(SchemaAksesV2Peer::LEVEL_ID, $level_user, Criteria::EQUAL);
                            $ada_akses = MasterUserV2Peer::doCount($users_cek2);

                            if ($ada_akses == 0) {
                                $query_insert_akses = "insert into schema_akses_v2 "
                                        . "(user_id, schema_id, level_id) "
                                        . "values('$user_id',2,$level_user)";
                                $stmt_insert_akses = $con->prepareStatement($query_insert_akses);
                                $stmt_insert_akses->executeQuery();
                                $berhasil_akses = 1;
                            }

                            if ($level_user == 1) {
                                $unit_id = substr($user_id, (strlen($user_id) - 4), 4);
                                $c = new Criteria();
                                $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                                if (UnitKerjaPeer::doSelectOne($c)) {
                                    $query_insert_akses = "insert into user_handle_v2 "
                                            . "(user_id, schema_id, unit_id) "
                                            . "values('$user_id',2,'$unit_id')";
                                    $stmt_insert_akses = $con->prepareStatement($query_insert_akses);
                                    $stmt_insert_akses->executeQuery();
                                } else {
                                    $ada_error[] = "User $user_id tidak sesuai dengan format dinas BIDANG_UNITID";
                                    // $con->rollback();
                                    // $this->setFlash('gagal', 'User ' . $user_id . ' tidak sesuai dengan format dinas BIDANG_UNITID');
                                }
                            } elseif($level_user == 13 || $level_user == 14) {
                                $query_insert_akses = "insert into user_handle_v2 "
                                            . "(user_id, schema_id, unit_id) "
                                            . "values('$user_id',2,'$skpd')";
                                $stmt_insert_akses = $con->prepareStatement($query_insert_akses);
                                $stmt_insert_akses->executeQuery();
                            } else{
                                $query_insert_akses = "insert into user_handle_v2 "
                                            . "(user_id, schema_id, unit_id) "
                                            . "values('$user_id',2,'$skpd')";
                                $stmt_insert_akses = $con->prepareStatement($query_insert_akses);
                                $stmt_insert_akses->executeQuery();
                            }



                            // $this->setFlash('berhasil', 'User ' . $user_id . ' telah berhasil dibuat');
                        }

                        // $con->commit();
                    }
                }
                $i++;
            }
            // $con->commit();
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Username telah terupload');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    
    public function executeUpdateUserIdByKodeKegiatan() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'updateuser_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $user_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));       

                    if ($kode_kegiatan!='' &&  $user_id!='') {    

                        $query_update_user = "update ebudget.dinas_master_kegiatan set user_id = '$user_id' where kode_kegiatan = '$kode_kegiatan'";
                        $stmt_update = $con->prepareStatement($query_update_user);
                        $stmt_update->executeQuery();
                    }
                }
                $i++;
            }
            // $con->commit();
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'User ID telah terupdate');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    
    public function executeUpdateLockFalse() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'updatelock_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $kode_kegiatan = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));   
                    $lock = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));  
                    $rincian_level = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()));  
                    
                    if($lock == 1){
                        $str = "lock = true";
                    }elseif ($lock == 0) {
                        $str = "lock = false";
                    }

                    if ($kode_kegiatan!='' && $lock != '' && $rincian_level != '') {    

                        $query_update_user = "update ebudget.dinas_rincian set $str, rincian_level=$rincian_level where kegiatan_code = '$kode_kegiatan'";
                        $stmt_update = $con->prepareStatement($query_update_user);
                        $stmt_update->executeQuery();
                    }
                }
                $i++;
            }
            // $con->commit();
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Lock telah terupdate');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }
    
    
    public function executeUpdateUserLevel() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'updateuserlevel_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $user_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));   
                    $level_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()));  

                    if ($user_id!='' && $level_id != '') {    

                        $c_user_level = new Criteria();
                        $c_user_level->add(SchemaAksesV2Peer::USER_ID, $user_id);
                        $dapat_user_level = SchemaAksesV2Peer::doSelectOne($c_user_level);
                        if ($dapat_user_level) {
                            $dapat_user_level->setLevelId($level_id);
                            if(!$dapat_user_level->save()){
                                $ada_error[] = "Gagal update user level pada baris ke-".$row;
                            }
                        }
                    }
                }
                $i++;
            }
            // $con->commit();
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'User Level telah terupdate');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUploadKunciKomponen() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $array_file_xls = explode('.', $this->getRequest()->getFileName('file'));

            $fileName_xls = 'uploadkuncikomponen_' . date('Y-m-d_H-i') . '.' . $array_file_xls[1];

            $this->getRequest()->moveFile('file', sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);

            $file_path_xls = sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls;

            if (!file_exists($file_path_xls)) {
                unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
                $this->setFlash('gagal', 'CEK FILE ' . $fileName_xls);
                return $this->redirect('admin/fiturTambahan');
            }

            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load($file_path_xls);
            $i = 0;

            $ada_error = array();
            while ($i < $objPHPExcel->getSheetCount()) {
                $objPHPExcel->setActiveSheetIndex($i);
                $objWorksheet = $objPHPExcel->getActiveSheet();
                $highR = $objWorksheet->getHighestRow();
                for ($row = 0; $row <= $highR; $row++) {
                    //$unit_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));
                    $komponen_id = utf8_encode(trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue()));

                    if ($komponen_id!='') {    

                        $query_delete_komponen = "DELETE FROM ebudget.komponen where komponen_id='".$komponen_id."'";
                        $stmt_update = $con->prepareStatement($query_delete_komponen);
                        $stmt_update->executeQuery();
                        
                        
                        $query_update_komponen_all = "UPDATE ebudget.komponen_all SET komponen_locked=false where komponen_id='".$komponen_id."'";
                        $stmt_update = $con->prepareStatement($query_update_komponen_all);
                        $stmt_update->executeQuery();

                    }
                }
                $i++;
            }
            // $con->commit();
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //exit;
            if ($ada_error == null) {
                $con->commit();
                $this->setFlash('berhasil', 'Lock telah terupdate');
            } else {
                $con->rollback();
                $error = implode(' - ', $ada_error);
                $this->setFlash('gagal', 'Gagal karena ' . $error);
            }
        } catch (Exception $ex) {
            unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }
        unlink(sfConfig::get('sf_root_dir') . '/web/uploads/sampah/' . $fileName_xls);
        return $this->redirect('admin/fiturTambahan');
    }

    public function executeUbahUserid() {
        $con = Propel::getConnection();
        $con->begin();
        try {
            $user_id_lama = trim($this->getRequestParameter('user_id_lama'));
            $user_id_baru = trim($this->getRequestParameter('user_id_baru'));

            if($user_id_lama != '' && $user_id_baru != ''){
                //ubah master_user_v2
                $query_ubah_master_user = "UPDATE master_user_v2 SET user_id='$user_id_baru' WHERE user_id='$user_id_lama'";
                $stmt_update_master = $con->prepareStatement($query_ubah_master_user);
                $stmt_update_master->executeQuery();
                
                //ubah schema_akses_v2
                $query_ubah_schema_akses = "UPDATE schema_akses_v2 SET user_id='$user_id_baru' WHERE user_id='$user_id_lama'";
                $stmt_update_schema = $con->prepareStatement($query_ubah_schema_akses);
                $stmt_update_schema->executeQuery();
                
                //ubah user_handle_v2
                $query_ubah_user_handle = "UPDATE user_handle_v2 SET user_id='$user_id_baru' WHERE user_id='$user_id_lama'";
                $stmt_update_user_handle = $con->prepareStatement($query_ubah_user_handle);
                $stmt_update_user_handle->executeQuery();
                
                //ubah ebudget.dinas_master_kegiatan
                $query_ubah_dinas_master_kegiatan = "UPDATE ebudget.dinas_master_kegiatan SET user_id='$user_id_baru' WHERE user_id='$user_id_lama'";
                $stmt_update_dinas_master_kegiatan = $con->prepareStatement($query_ubah_dinas_master_kegiatan);
                $stmt_update_dinas_master_kegiatan->executeQuery();
                
                //ubah ebudget.master_kegiatan
                $query_ubah_master_kegiatan = "UPDATE ebudget.master_kegiatan SET user_id='$user_id_baru' WHERE user_id='$user_id_lama'";
                $stmt_update_master_kegiatan = $con->prepareStatement($query_ubah_master_kegiatan);
                $stmt_update_master_kegiatan->executeQuery();

                $con->commit();
                $this->setFlash('berhasil', 'User ID telah terupdate');
            }else{
                $con->rollback();
                $this->setFlash('gagal', 'Gagal karena user_id_lama atau user_id_baru kosong');
            }
        } catch (Exception $ex) {
            //echo $ex; exit;
            $con->rollback();
            $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
        }

        return $this->redirect('admin/fiturTambahan');
    }
}
