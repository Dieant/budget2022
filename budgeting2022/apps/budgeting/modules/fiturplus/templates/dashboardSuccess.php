<!-- Main content -->
<section class="content">
<!-- Small boxes (Stat box) -->
<div class="row">
  <?php
  foreach ($rs_rekening as $value) {
  ?>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua font-number">
      <div class="inner">
        <h3>Rp <?php echo number_format($value['hasil_kali2'], 0, ',', '.'); ?></h3>

        <p><?php echo $value['belanja_name'] ?></p>
      </div>
    </div>
  </div>
  <?php
  }

  foreach ($rs_rekening_bj as $value) {
  ?>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-black font-number">
      <div class="inner">
        <h3>Rp <?php echo number_format($value['hasil_kali2'], 0, ',', '.'); ?></h3>

        <p><?php echo $value['belanja_name'] ?></p>
      </div>
    </div>
  </div>
  <?php
  }
  foreach ($rs_rekening_md as $value) {
  ?>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green font-number">
      <div class="inner">
        <h3>Rp <?php echo number_format($value['hasil_kali2'], 0, ',', '.'); ?></h3>

        <p><?php echo $value['belanja_name'] ?></p>
      </div>
    </div>
  </div>
  <?php
  }
  foreach ($rs_total as $value) {
  ?>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-orange font-number">
      <div class="inner">
        <h3>Rp <?php echo number_format($value['hasil_kali2'], 0, ',', '.'); ?></h3>

        <p>TOTAL ANGGARAN</p>
      </div>
    </div>
  </div>
  <?php
  }
  ?>

</div>
<!-- /.row -->
<!-- Main row -->
<div class="row">
<!-- Left col -->
  <section class="col-lg-7 connectedSortable">
  <!-- Custom tabs (Charts with tabs)-->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Rincian Rekening Per PD</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <figure class="highcharts-figure">
          <div id="container_bar" style="height: 650px;"></div>
      </figure>
      <div id="success"></div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>

  <section class="col-lg-5 connectedSortable">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Persentasi Program</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body chart-responsive">
        <div id="container_chart" style="height: 650px;"></div>
      </div>
      <!-- /.box-body -->
    </div>
  </section>
    

</div>
  <!-- /.nav-tabs-custom -->
  
</section>

<script type="text/javascript">

  setInterval("my_function();",100000); 
 
  function my_function(){
      window.location = location.href;
  }

  var data_chart = <?php echo $setprogram; ?>;
  Highcharts.setOptions({
      colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
          return {
              radialGradient: {
                  cx: 0.5,
                  cy: 0.3,
                  r: 0.7
              },
              stops: [
                  [0, color],
                  [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
              ]
          };
      })
  });

// Build the chart
  Highcharts.chart('container_chart', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  connectorColor: 'silver'
              }
          }
      },
      series: [{
          name: 'Share',
          data: 
              data_chart
          
      }]
  });

  var kategori = <?php echo $rs_unit; ?>;

  var peg = '<?php echo $rs_peg; ?>';
  var peg1 = peg.replace(/"/gi,'');
  // var barjas = '<?php echo $rs_bar; ?>';
  // var barjas1 = barjas.replace(/"/gi,'');
  // var modal = '<?php echo $rs_mod; ?>';
  // var modal1 = modal.replace(/"/gi,'');
  Highcharts.chart('container_bar', {
              chart: {
                  type: 'bar'
              },
              title: {
                  text: ''
              },
              xAxis: {
                  categories: kategori,
                  title: {
                      text: null
                  }
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Nilai Anggaran (millions)',
                      align: 'high'
                  },
                  labels: {
                      overflow: 'justify'
                  }
              },
              tooltip: {
                  valueSuffix: ' millions'
              },
              plotOptions: {
                  bar: {
                      dataLabels: {
                          enabled: true
                      }
                  }
              },
              legend: {
                  layout: 'vertical',
                  align: 'right',
                  verticalAlign: 'top',
                  x: -40,
                  y: 20,
                  floating: true,
                  borderWidth: 1,
                  backgroundColor:
                      Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                  shadow: true
              },
              credits: {
                  enabled: false
              },
              series: [{
                  name: 'Belanja Pegawai',
                  data: [634248199,3063616284,54785439875,4594651904,147566598374,7577580938,4337019991,13596987059,17548339324,16731609712]
              }, {
                  name: 'Belanja Barang Jasa',
                  data: [111166520126,143425730042,522500227861,189101594046,668229551908,135860292108,67426035405,216889823100,164840285005,705666471443]
              }, {
                  name: 'Belanja Modal',
                  data: [33293923705,6152725479,32955450944,184452972046,41521438573,769499337763,300905396430,1012774955587,77441634275,163970581233]
              }]
  });

</script>