<?php

/**
 * asb actions.
 *
 * @package    budgeting
 * @subpackage asb
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class asbActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->executeAsblist();
    }

    //    ticket 22 - urgent list asb
    public function executeAsblist() {
        $this->processFiltersasblist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/asblist/filters');

        $pagers = new sfPropelPager('Komponen', 25);

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK', Criteria::EQUAL);
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $c->setDistinct();
        $this->addFiltersCriteriaasblist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltersasblist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/asblist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/asblist/filters');
        }
    }

    protected function addFiltersCriteriaasblist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //    ticket 22 - urgent list asb
    //    ticket 22 - urgent rincian asb
    public function executeRincianAsb() {
        $id_asb = $this->getRequestParameter('id');

        $c_data_asb = new Criteria();
        $c_data_asb->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK', Criteria::EQUAL);
        $c_data_asb->addAnd(KomponenPeer::KOMPONEN_ID, $id_asb, Criteria::EQUAL);
        $this->data_asb = $data_asb = KomponenPeer::doSelectOne($c_data_asb);

        $c_data_member_asb = new Criteria();
        $c_data_member_asb->add(KomponenMemberPeer::KOMPONEN_ID, $id_asb, Criteria::EQUAL);
        $c_data_member_asb->addDescendingOrderByColumn(KomponenMemberPeer::MEMBER_NO);
        $this->data_member_asb = $data_member_asb = KomponenMemberPeer::doSelect($c_data_member_asb);
    }

    //    ticket 22 - urgent rincian asb
}
