<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <thead class="head_peach">
            <tr>
                <th>Kode Komponen</th>
                <th>Nama Komponen</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Rekening</th>      
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $asblist): $odd = fmod(++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <td><?php echo $asblist->getKomponenId() ?></td>
                    <td><?php echo $asblist->getKomponenName() ?></td>
                    <td style="text-align: center"><?php echo $asblist->getSatuan() ?></td>
                    <td style="text-align: right">
                        <?php
                        if (sfConfig::get('app_tahun_default') != '2016') {
                            echo number_format($asblist->getKomponenHargaBulat(), 0, ',', '.');
                        } else {
                            echo number_format($asblist->getKomponenHarga(), 2, ',', '.');
                        }
                        ?>
                    </td>
                    <td style="text-align: center">
                        <?php
                        if ($asblist->getRekening() == '') {
                            echo '';
                        } else {
                            $array_rekening = explode('/', $asblist->getRekening());
                            if (count($array_rekening) > 1) {
                                foreach ($array_rekening as $value) {
                                    if ($value <> '.' && $value <> '') {
                                        echo $value . ' (' . RekeningPeer::getStringRekeningName($value) . ')<br/>';
                                    }
                                }
                            } else {
                                echo $asblist->getRekening() . ' (' . RekeningPeer::getStringRekeningName($asblist->getRekening()) . ')';
                            }
                        }
                        ?>
                    </td> 
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'asb/asblist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "asb/asblist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'asb/asblist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>