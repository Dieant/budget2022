<div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <thead class="head_peach">
            <tr>
                <th>Kode Komponen</th>
                <th>Nama Komponen</th>
                <th>Satuan</th>
                <th>Harga</th>
                 <th>Pajak</th>
                <th>Rekening</th>      
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $estlist): $odd = fmod( ++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <td><?php echo $estlist->getKomponenId() ?></td>
                    <td><?php echo $estlist->getKomponenName() ?></td>
                    <td style="text-align: center"><?php echo $estlist->getSatuan() ?></td>
                    <td style="text-align: right">
                        <?php
                        echo number_format($estlist->getKomponenHarga(), 2, ',', '.');
                        ?>
                    </td>
                    <td style="text-align: center">
                        <?php
                        if ($estlist->getKomponenNonPajak() == 'true') {
                            echo '0 %';
                        } else {
                            echo '10 %';
                        }
                        ?>
                    </td>
                    <td style="text-align: center">
                        <?php
                        $array_rekening = explode('/', $estlist->getRekening());
                        if (count($array_rekening) > 1) {
                            foreach ($array_rekening as $value) {
                                if ($value <> '.' && $value <> '') {
                                    echo $value . ' (' . RekeningPeer::getStringRekeningName($value) . ')<br/>';
                                }
                            }
                        } else {
                            echo $estlist->getRekening() . ' (' . RekeningPeer::getStringRekeningName($estlist->getRekening()) . ')';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'estimasi/estlist?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "estimasi/estlist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'estimasi/estlist?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>