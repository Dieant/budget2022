<?php

/**
 * estimasi actions.
 *
 * @package    budgeting
 * @subpackage estimasi
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class estimasiActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

    //    ticket 23 - urgent list est
    public function executeEstlist() {
        $this->processFiltersestlist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/estlist/filters');

        $pagers = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'EST', Criteria::EQUAL);
        $c->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::NOT_EQUAL);
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $c->setDistinct();
        $this->addFiltersCriteriaestlist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeBtllist() {
        $this->processFiltersbtllist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/btllist/filters');

        $pagers = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'BTL', Criteria::EQUAL);
        $c->add(KomponenRekeningPeer::REKENING_CODE, '', Criteria::NOT_EQUAL);
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $c->setDistinct();
        $this->addFiltersCriteriabtllist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }


    protected function processFiltersestlist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/estlist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/estlist/filters');
        }
    }

    protected function processFiltersbtllist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/btllist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/btllist/filters');
        }
    }

    protected function addFiltersCriteriaestlist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            }  elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

        protected function addFiltersCriteriabtllist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            }  elseif ($this->filters['select'] == 3) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //    ticket 23 - urgent list est
}
