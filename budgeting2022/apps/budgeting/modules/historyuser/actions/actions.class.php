<?php

/**
 * historyuser actions.
 *
 * @package    budgeting
 * @subpackage historyuser
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class historyuserActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

//    history list
    public function executeHistorylist() {
        
        $this->unit_id = $unit_id = $this->getRequestParameter('unit_id');
        $this->kegiatan_code = $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        
        $this->processFiltershistorylist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/historylist/filters');

        $pagers = new sfPropelPager('HistoryUser', 50);
        $c = new Criteria();
        $c->add(HistoryUserPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
        $c->add(HistoryUserPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
        $c->addDescendingOrderByColumn(HistoryUserPeer::TIME_ACT);
        $c->setDistinct();
        $this->addFiltersCriteriahistorylist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltershistorylist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/historylist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/historylist/filters');
        }
    }

    protected function addFiltersCriteriahistorylist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['description_is_empty'])) {
                    $criterion = $c->getNewCriterion(HistoryUserPeer::DESCRIPTION, '');
                    $criterion->addOr($c->getNewCriterion(HistoryUserPeer::DESCRIPTION, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['description']) && $this->filters['description'] !== '') {
                    $kata = '%' . $this->filters['description'] . '%';
                    $c->add(HistoryUserPeer::DESCRIPTION, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['description_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(HistoryUserPeer::DESCRIPTION, '');
                    $criterion->addOr($c->getNewCriterion(HistoryUserPeer::DESCRIPTION, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }
    
//    history list
}
