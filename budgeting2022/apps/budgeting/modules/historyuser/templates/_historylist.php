<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th class="text-center text-bold">Waktu</th>
                <th class="text-center text-bold">IP</th>
                <th class="text-center text-bold">User</th>
                <th class="text-center text-bold">Tipe</th>
                <th class="text-center text-bold">Deskripsi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pager->getResults() as $historylist): ?>
                <tr>
                    <td class="text-center"><?php echo date("d-m-Y H:i", strtotime($historylist->getTimeAct())); ?></td>
                    <td class="text-center"><?php echo $historylist->getIp() ?></td>
                    <td class="text-center"><?php echo $historylist->getUsername() ?></td>
                    <td class="text-center text-bold"><?php echo $historylist->getAksi() ?></td>
                    <td><?php echo $historylist->getDescription() ?></td>
                </tr>
            <?php endforeach; ?>        
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults());
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'historyuser/historylist?unit_id=' . $unit_id . '&kegiatan_code=' . $kegiatan_code . '&page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "historyuser/historylist?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'historyuser/historylist?unit_id=' . $unit_id . '&kegiatan_code=' . $kegiatan_code . '&page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>