<?php

/**
 * hspk actions.
 *
 * @package    budgeting
 * @subpackage hspk
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class hspkActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->executeHspklist();
    }
    
    //    ticket 21 - urgent list hspk
    public function executeHspklist() {
        $this->processFiltershspklist();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/hspklist/filters');

        $pagers = new sfPropelPager('Komponen', 50);

        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK', Criteria::EQUAL);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addFiltersCriteriahspklist($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFiltershspklist() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/hspklist/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/hspklist/filters');
        }
    }

    protected function addFiltersCriteriahspklist($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            }elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['komponen_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    //    ticket 21 - urgent list hspk
    
    //    ticket 21 - urgent rincian hspk
    public function executeRincianHspk() {
        $id_hspk = $this->getRequestParameter('id');
        
        $c_data_hspk = new Criteria();
        $c_data_hspk->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK', Criteria::EQUAL);
        $c_data_hspk->addAnd(KomponenPeer::KOMPONEN_ID, $id_hspk, Criteria::EQUAL);
        $this->data_hspk = $data_hspk = KomponenPeer::doSelectOne($c_data_hspk);
        
        $c_data_member_hspk = new Criteria();
        $c_data_member_hspk->add(KomponenMemberPeer::KOMPONEN_ID, $id_hspk, Criteria::EQUAL);
        $c_data_member_hspk->addDescendingOrderByColumn(KomponenMemberPeer::MEMBER_NO);
        $this->data_member_hspk = $data_member_hspk = KomponenMemberPeer::doSelect($c_data_member_hspk);
    }
    //    ticket 21 - urgent rincian hspk

}
