<?php if ($gagal): ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p>Berita acara tidak ditemukan</p>
    </div>
<?php else: ?>
    <?php if ($ada_lebih_baru): ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <p>Terdapat Resume Rapat yang lebih baru <?php echo link_to('Klik di sini', 'cek_qr/cekResumeRapat?' . $param_link_baru) ?></p>
        </div>
    <?php endif; ?>
    <section class="content-header">
        <h1>Resume Rapat <?php echo $unit_kerja; ?></h1>
    </section>
    <style>
        table{
            border-collapse: collapse;
        }
        .garis {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Resume Rapat
            </div>
            <div class="box-body">
                <div id="sf_admin_container" class="table-responsive">
                    <table>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > Hari/Tanggal</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" >:</h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $tanggal ?></h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > Pukul</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" >:</h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $jam . '.' . $menit ?> WIB</h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > Tempat</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" >:</h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $tempat ?></h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > Acara</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" >:</h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $acara ?></h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > &nbsp;</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" ></h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $acara2 ?></h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > Pimpinan</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" >:</h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $pimpinan ?></h5>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" > Nama SKPD</h5>
                            </td>
                            <td style="padding:0px"><h5 width='100%' align="left" style="margin:1px" >:</h5></td>
                            <td style="padding:0px">
                                <h5 width='100%' align="left" style="margin:1px" ><?php echo $unit_kerja ?></h5>
                            </td>
                        </tr>
                    </table>
                    <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" width="100%">
                        <td valign='top'>
                            <table width='100%' class="garis">
                                <thead>
                                <th class="garis" bgcolor="#d0d0e1">No</th>
                                <th class="garis" colspan='1' bgcolor="#d0d0e1">Kode Kegiatan</th>
                                <th class="garis" colspan='1' bgcolor="#d0d0e1">Nama Kegiatan</th>
                                <!--<th class="garis" colspan='1' bgcolor="#d0d0e1">Kode Rekening</th>-->
                                <th class="garis" colspan='1' bgcolor="#d0d0e1">Uraian Kode Rekening</th>
                                <th class="garis" colspan='1' bgcolor="#d0d0e1">Semula</th>
                                <th class="garis" colspan='1' bgcolor="#d0d0e1">Menjadi</th>
                                <th class="garis" colspan='1' bgcolor="#d0d0e1">Selisih</th>
                                <th class="garis" colspan='1' bgcolor="#d0d0e1" width="50%">Alasan/Keterangan</th>
                                </thead>
                                <?php
                                foreach ($detail as $item) {
                                    ?>
                                    <tr>
                                        <?php
                                        if ($item->getKegiatanCode() != $kode_kegiatan) {
                                            $query = "select count(*) as jml, kegiatan_name, catatan_pembahasan
                                                        from " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_kegiatan a, " . sfConfig::get('app_default_schema') . ".deskripsi_resume_rapat_rekening b
                                                        where a.id_deskripsi_resume_rapat=" . $item->getIdDeskripsiResumeRapat() . " and a.kegiatan_code='" . $item->getKegiatanCode() . "' and
                                                        a.id_deskripsi_resume_rapat=b.id_deskripsi_resume_rapat and a.kegiatan_code=b.kegiatan_code
                                                        group by kegiatan_name, catatan_pembahasan";
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query);
                                            $rs_kegiatan = $stmt->executeQuery();
                                            $rs_kegiatan->next();
                                            ?>
                                            <td class="garis" rowspan="<?php echo $rs_kegiatan->getString('jml'); ?>" ><?php echo ++$no ?></td>
                                            <td class="garis" rowspan="<?php echo $rs_kegiatan->getString('jml'); ?>" class="text-bold text-left"><?php echo $item->getKegiatanCode(); ?></td>
                                            <td class="garis" rowspan="<?php echo $rs_kegiatan->getString('jml'); ?>" class="text-bold text-left"><?php echo $rs_kegiatan->getString('kegiatan_name'); ?></td>
                                            <?php
                                        }
                                        ?>
                                        <td class="garis" class="text-bold text-left"><?php echo $item->getRekeningName(); ?></td>
                                        <td class="garis" class="text-right" style="text-align:right"><?php echo number_format($item->getSemula(), 0, '.', ',') ?></td>
                                        <td class="garis" class="text-right" style="text-align:right"><?php echo number_format($item->getMenjadi(), 0, '.', ',') ?></td>
                                        <td class="garis" class="text-right" style="text-align:right"><?php echo number_format(($item->getSemula() - $item->getMenjadi()), 0, '.', ',') ?></td>
                                        <?php
                                        if ($item->getKegiatanCode() != $kode_kegiatan) {
                                            ?>
                                            <td class="garis" rowspan="<?php echo $rs_kegiatan->getString('jml'); ?>" class="text-left" ><?php echo $rs_kegiatan->getString('catatan_pembahasan') ?></td>
                                            <?php
                                        }
                                        $kode_kegiatan = $item->getKegiatanCode();
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        <tr>
                            <td colspan="20"><p><?php echo $catatan; ?></p></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>