<?php if ($gagal): ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p>Print RKA tidak ditemukan</p>
    </div>
<?php else: ?>
    <?php if ($ada_lebih_baru): ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <p>Terdapat Print RKA yang lebih baru <?php echo link_to('Klik di sini', 'cek_qr/cekPrintRka?' . $param_link_baru) ?></p>
        </div>
    <?php endif; ?>
    <section class="content-header">
        <h1>Print RKA <?php echo $unit_kerja . ' - ' . $kode_kegiatan; ?></h1>
    </section>
    <style>
        table{
            border-collapse: collapse;
        }
        .garis {
            border: 1px solid black;
            padding: 5px;
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Print RKA
            </div>
            <div class="box-body">
                <div id="sf_admin_container" class="table-responsive">
                    <table style="empty-cells: show;" bgcolor="#ffffff" border="1" cellpadding="3" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" style="padding: 5px;">&nbsp;</td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Komponen</strong></span></td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Satuan</strong></span></td>
                            <td align="center" width="20%" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Koefisien</strong></span></td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Harga</strong></span></td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Hasil</strong></span></td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>PPN</strong></span></td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Total</strong></span></td>
                            <td align="center" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong>Belanja</strong></span></td>
                        </tr>
                        <?php
                        $total = 0;
                        $totalrek = 0;
                        $totalsub = 0;
                        $pertama = TRUE;

                        foreach ($items as $item) {
                            //subtitle
                            if ($subtitle != $item->getSubtitle()) {
                                if (!$pertama) {
                                    //total rekening sebelum
                                    echo '<tr bgcolor="#ffffff">';
                                    echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                    echo 'Total ' . $rekening_name . ' :</td>';
                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                    echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                    echo '</tr>';
                                    $totalrek = 0;
                                    //total subtitle sebelum
                                    echo '<tr bgcolor="#ffffff">';
                                    echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                    echo '<b>Total ' . $subtitle . '  :</td></b>';
                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;"><b>' . number_format(round($totalsub, 0), 0, ',', '.') . '</b></td>';
                                    echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                    echo '</tr>';
                                    $totalsub = 0;
                                }
                                ?>
                                <tr align="left" bgcolor="white"><td colspan="9" style="padding: 5px;"><span class="style3" style="font-size: smaller"><strong><?php echo ':: ' . $item->getSubtitle() ?></strong></span></td></tr>
                                                <?php
                                                $con = Propel::getConnection();
                                                $query = "select sum(hasil*((pajak+100)/100)) as jml
                                                                        from " . sfConfig::get('app_default_schema') . ".print_rka_pak_detail
                                                                        where subtitle ilike '" . $item->getSubtitle() . "' and subsubtitle='" . $item->getSubsubtitle() . "' and id_print_rka_pak=$id_print_rka";
                                                $stmt = $con->prepareStatement($query);
                                                $rs = $stmt->executeQuery();
                                                $rs->next();
                                                $total_subsubtitle = $rs->getString('jml');

                                                echo '<tr bgcolor="#ffffff">';
                                                echo '<td colspan="7" class="Font8v style3" align="left" style="padding: 5px; font-size: smaller;">';
                                                echo '<b><i>.:. ' . $item->getSubsubtitle();
                                                '</td></i></b>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;"><b>' . number_format(round($total_subsubtitle, 0), 0, ',', '.') . '</b></td>';
                                                echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                                echo '</tr>';

                                                $c = new Criteria();
                                                $c->add(RekeningPeer::REKENING_CODE, $item->getRekeningCode());
                                                $rs = RekeningPeer::doSelectOne($c);
                                                $rekening_name = $rs->getRekeningName();

                                                $c = new Criteria();
                                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, substr($item->getRekeningCode(), 0, 5));
                                                $belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                $nama_belanja = $belanja->getBelanjaName();
                                                echo '<tr bgcolor="white"><td style="padding: 5px;"> :</td><td colspan="8" style="padding: 5px; font-size: smaller;">' . $item->getRekeningCode() . ' ' . $rekening_name . '</td></tr>';
                                                $subsubtitle = $item->getSubsubtitle();
                                                $rekening_code = $item->getRekeningCode();
                                            }
                                            $subtitle = $item->getSubtitle();

                                            //subsubtitle
                                            if ($subsubtitle != $item->getSubsubtitle()) {
                                                //total rekening sebelum
                                                if (!$pertama) {
                                                    echo '<tr bgcolor="#ffffff">';
                                                    echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                                    echo 'Total ' . $rekening_name . ' :</td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                                    echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                                    echo '</tr>';
                                                    $totalrek = 0;
                                                }
                                                $query = "select sum(hasil*((pajak+100)/100)) as jml
                                                                        from " . sfConfig::get('app_default_schema') . ".print_rka_pak_detail
                                                                        where subtitle ilike '$subtitle' and subsubtitle='" . $item->getSubsubtitle() . "' and id_print_rka_pak=$id_print_rka";
                                                $stmt = $con->prepareStatement($query);
                                                $rs = $stmt->executeQuery();
                                                $total_subsubtitle = $rs->getInt('jml');

                                                echo '<tr bgcolor="#ffffff">';
                                                echo '<td colspan="7" class="Font8v style3" align="left" style="padding: 5px; font-size: smaller;">';
                                                echo '<b><i>.:. ' . $item->getSubsubtitle();
                                                '</td></i></b>';
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;"><b>' . number_format(round($total_subsubtitle, 0), 0, ',', '.') . '</b></td>';
                                                echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                                echo '</tr>';
                                                $c = new Criteria();
                                                $c->add(RekeningPeer::REKENING_CODE, $item->getRekeningCode());
                                                $rs = RekeningPeer::doSelectOne($c);
                                                $rekening_name = $rs->getRekeningName();

                                                $c = new Criteria();
                                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, substr($item->getRekeningCode(), 0, 5));
                                                $belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                $nama_belanja = $belanja->getBelanjaName();
                                                echo '<tr bgcolor="white"><td style="padding: 5px;"> :</td><td colspan="8" style="padding: 5px; font-size: smaller;">' . $item->getRekeningCode() . ' ' . $rekening_name . '</td></tr>';
                                                $rekening_code = $item->getRekeningCode();
                                            }
                                            $subsubtitle = $item->getSubsubtitle();

                                            //rekening
                                            if ($rekening_code != $item->getRekeningCode()) {
                                                //total rekening sebelum
                                                if (!$pertama) {
                                                    echo '<tr bgcolor="#ffffff">';
                                                    echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                                    echo 'Total ' . $rekening_name . ' :</td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                                    echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                                    echo '</tr>';
                                                    $totalrek = 0;
                                                }
                                                $c = new Criteria();
                                                $c->add(RekeningPeer::REKENING_CODE, $item->getRekeningCode());
                                                $rs = RekeningPeer::doSelectOne($c);
                                                $rekening_name = $rs->getRekeningName();

                                                $c = new Criteria();
                                                $c->add(KelompokBelanjaPeer::BELANJA_CODE, substr($item->getRekeningCode(), 0, 5));
                                                $belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                $nama_belanja = $belanja->getBelanjaName();
                                                echo '<tr bgcolor="white"><td style="padding: 5px;"> :</td><td colspan="8" style="padding: 5px; font-size: smaller;">' . $item->getRekeningCode() . ' ' . $rekening_name . '</td></tr>';
                                            }
                                            $rekening_code = $item->getRekeningCode();

                                            //komponen
                                            echo '<tr bgcolor="#ffffff" valign="top">';
                                            echo '<td class="Font8v" align="left" style="padding: 5px;">&nbsp;</td>';

                                            echo '<td class="Font8v" align="left" style="padding: 5px;"><span class="style3" style="font-size: smaller">' . $item->getNamaKomponen() . '</span></td>';
                                            echo '<td class="Font8v style3" align="center" nowrap="nowrap" style="padding: 5px;"><span class="Font8v" style="font-size: smaller">' . $item->getSatuan() . '</span></td>';
                                            echo '<td class="Font8v style3" align="center" style="padding: 5px;"><span class="Font8v" style="font-size: smaller">' . $item->getKoefisien() . '</span></td>';
                                            if (number_format($item->getHarga(), 0, ',', '.') == '0') {
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format($item->getHarga(), 4, ',', '.') . '</td>';
                                            } else {
                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format($item->getHarga(), 0, ',', '.') . '</td>';
                                            }
                                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format($item->getHasil(), 0, ',', '.') . '</td>';
                                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . $item->getPajak() . '%</td>';
                                            $hasil = $item->getHasil();
                                            $total1 = $hasil + (($item->getPajak() / 100) * $hasil);
                                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format(round($total1, 0), 0, ',', '.') . '</td>';

                                            $l = strlen($nama_belanja);
                                            echo '<td class="Font8v style3" align="center" style="padding: 5px; font-size: smaller;">' . substr(ucwords(strtolower($nama_belanja)), 8, $l) . '</td>';
                                            echo '</tr>';
                                            $total = $total + $total1;
                                            $totalsub = $totalsub + $total1;
                                            $totalrek = $totalrek + $total1;
                                            $pertama = FALSE;
                                        }
                                        //total rekening sebelum
                                        echo '<tr bgcolor="#ffffff">';
                                        echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                        echo 'Total ' . $rekening_name . ' :</td>';
                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;">' . number_format(round($totalrek, 0), 0, ',', '.') . '</td>';
                                        echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                        echo '</tr>';
                                        //total subtitle sebelum
                                        echo '<tr bgcolor="#ffffff">';
                                        echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                        echo '<b>Total ' . $subtitle . '  :</td></b>';
                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;"><b>' . number_format(round($totalsub, 0), 0, ',', '.') . '</b></td>';
                                        echo '<td class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">&nbsp;</td>';
                                        echo '</tr>';

                                        echo '<tr bgcolor="#ffffff">';
                                        echo '<td colspan="7" class="Font8v style3" align="right" style="padding: 5px; font-size: smaller;">';
                                        echo '<b>Grand Total  :</b></td>';
                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="padding: 5px; font-size: smaller;"><b>' . number_format(round($total, 0), 0, ',', '.') . '</b></td>';
                                        echo '<td class="Font8v style3" align="right">&nbsp;</td>';
                                        echo '</tr>';
                                        ?>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>