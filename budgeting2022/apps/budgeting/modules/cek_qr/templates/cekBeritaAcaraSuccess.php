<?php if ($gagal): ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        <p>Berita acara tidak ditemukan</p>
    </div>
<?php else: ?>
    <?php if ($ada_lebih_baru): ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <p>Terdapat usulan yang lebih baru <?php echo link_to('Klik di sini', 'cek_qr/cekBeritaAcara?' . $param_link_baru) ?></p>
        </div>
    <?php endif; ?>
    <section class="content-header">
        <h1>Perubahan Detail Rincian Kegiatan <?php echo $kode_kegiatan; ?></h1>
        <h2><?php echo $unit_kerja; ?></h2>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                Perbandingan Semula & Menjadi Per Komponen
            </div>
            <div class="box-body">
                <div id="sf_admin_container" class="table-responsive">
                    <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" class="sf_admin_list">
                        <tr>
                            <td colspan='3'>
                                <table width='100%'>
                                    <tr>
                                        <td valign='top'>
                                            <table width='100%'>
                                                <tr class="sf_admin_row_1" align='center'>
                                                <thead>
                                                <th colspan='7'><strong>SEMULA</strong></th>
                                                <th><strong>|</strong></th>
                                                <th colspan='7'><strong>MENJADI</strong></th>
                                                <th >&nbsp;</th>
                                                </thead>

                                                <thead>
                                                <th>Komponen</th>
                                                <th>Satuan</th>
                                                <th>Koefisien</th>
                                                <th>Harga</th>
                                                <th>Hasil</th>
                                                <th>PPN</th>
                                                <th>Total</th>
                                                <th align='center'>|</th>
                                                <th>Komponen</th>
                                                <th>Satuan</th>
                                                <th>Koefisien</th>
                                                <th>Harga</th>
                                                <th>Hasil</th>
                                                <th>PPN</th>
                                                <th>Total</th>
                                                <th>Catatan</th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $total_belanja_semula = 0;
                                                    $total_belanja_menjadi = 0;
                                                    $total_rekening_semula = 0;
                                                    $total_rekening_menjadi = 0;
                                                    $total_semula = 0;
                                                    $total_menjadi = 0;
                                                    foreach ($detail as $value) {
                                                        if ($prev_belanja != $value->getBelanja()) {
                                                            //total rekening
                                                            if ($total_rekening_semula > 0 || $total_rekening_menjadi > 0) {
                                                                echo '<tr bgcolor="#ffffff">';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_rekening . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_semula, 0, ',', '.') . '</td>';
                                                                echo '<td> </td>';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_rekening . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_menjadi, 0, ',', '.') . '</td>';
                                                                echo '<td colspan="2"></td>';
                                                                echo '</tr>';
                                                            }
                                                            $total_belanja_semula += $total_rekening_semula;
                                                            $total_belanja_menjadi += $total_rekening_menjadi;
                                                            $total_rekening_semula = 0;
                                                            $total_rekening_menjadi = 0;

                                                            //total belanja
                                                            if ($total_belanja_semula > 0 || $total_rekening_semula > 0) {
                                                                echo '<tr bgcolor="#ffffff">';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_belanja . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_belanja_semula, 0, ',', '.') . '</td>';
                                                                echo '<td> </td>';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_belanja . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_belanja_menjadi, 0, ',', '.') . '</td>';
                                                                echo '<td colspan="2"></td>';
                                                                echo '</tr>';
                                                            }
                                                            $total_semula += $total_belanja_semula;
                                                            $total_menjadi += $total_belanja_menjadi;
                                                            $total_belanja_semula = 0;
                                                            $total_belanja_menjadi = 0;

                                                            //tampil belanja
                                                            $c = new Criteria();
                                                            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $value->getBelanja());
                                                            $rs_belanja = KelompokBelanjaPeer::doSelectOne($c);
                                                            $temp_belanja = $rs_belanja->getBelanjaName();
                                                            ?>
                                                            <tr align="left" bgcolor="#ffffff">
                                                                <td colspan="7"><span class="style3"><strong><?php echo ':: ' . $value->getBelanja() . ' ' . $rs_belanja->getBelanjaName() ?></strong></span></td>
                                                                <td>&nbsp;</td>
                                                                <td colspan="9"><span class="style3"><strong><?php echo ':: ' . $value->getBelanja() . ' ' . $rs_belanja->getBelanjaName() ?></strong></span></td>
                                                            </tr>
                                                            <?php
                                                            //tampil subtitle
                                                            echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong> .:. ' . $value->getSubtitle() . '</strong></span></td>';
                                                            echo '<td><strong><span>&nbsp;</span></strong></td>';
                                                            echo '<td colspan="9"><span class="style3"><strong> .:. ' . $value->getSubtitle() . '</strong></span></td></tr>';

                                                            //tampil rekening
                                                            $c = new Criteria();
                                                            $c->add(RekeningPeer::REKENING_CODE, $value->getRekeningCode());
                                                            $rs = RekeningPeer::doSelectOne($c);
                                                            $temp_rekening = $value->getRekeningCode() . ' ' . $rs->getRekeningName();
                                                            echo '<tr bgcolor="#ffffff"><td colspan="7" class="Font8v style3" align="left"><span class="style3"><strong>' . $value->getRekeningCode() . ' ' . $rs->getRekeningName() . '</strong></span></td>';
                                                            echo '<td>&nbsp;</td>';
                                                            echo '<td colspan="9" class="Font8v style3" align="left"><span class="style3"><strong>' . $value->getRekeningCode() . ' ' . $rs->getRekeningName() . '</strong></span></td></tr>';
                                                        } elseif ($prev_subtitle != $value->getSubtitle()) {
                                                            //total per rekening
                                                            if ($total_rekening_semula > 0 || $total_rekening_menjadi > 0) {
                                                                echo '<tr bgcolor="#ffffff">';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_rekening . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_semula, 0, ',', '.') . '</td>';
                                                                echo '<td> </td>';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_rekening . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_menjadi, 0, ',', '.') . '</td>';
                                                                echo '<td colspan="2"></td>';
                                                                echo '</tr>';
                                                            }
                                                            $total_belanja_semula += $total_rekening_semula;
                                                            $total_belanja_menjadi += $total_rekening_menjadi;
                                                            $total_rekening_semula = 0;
                                                            $total_rekening_menjadi = 0;

                                                            //tampil subtitle baru
                                                            echo '<tr align="left" bgcolor="#ffffff"><td colspan="7"><span class="style3"><strong> .:. ' . $value->getSubtitle() . '</strong></span></td>';
                                                            echo '<td><strong><span>&nbsp;</span></strong></td>';
                                                            echo '<td colspan="9"><span class="style3"><strong> .:. ' . $value->getSubtitle() . '</strong></span></td></tr>';

                                                            //tampil rekening baru
                                                            $c = new Criteria();
                                                            $c->add(RekeningPeer::REKENING_CODE, $value->getRekeningCode());
                                                            $rs = RekeningPeer::doSelectOne($c);
                                                            $temp_rekening = $value->getRekeningCode() . ' ' . $rs->getRekeningName();
                                                            echo '<tr bgcolor="#ffffff"><td colspan="7" class="Font8v style3" align="left"><span class="style3"><strong>' . $value->getRekeningCode() . ' ' . $rs->getRekeningName() . '</strong></span></td>';
                                                            echo '<td>&nbsp;</td>';
                                                            echo '<td colspan="9" class="Font8v style3" align="left"><span class="style3"><strong>' . $value->getRekeningCode() . ' ' . $rs->getRekeningName() . '</strong></span></td></tr>';
                                                        } elseif ($prev_rekening != $value->getRekeningCode()) {
                                                            //total rekening
                                                            if ($total_rekening_semula > 0 || $total_rekening_menjadi > 0) {
                                                                echo '<tr bgcolor="#ffffff">';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_rekening . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_semula, 0, ',', '.') . '</td>';
                                                                echo '<td> </td>';
                                                                echo '<td colspan="6" class="Font8v style3" align="right">';
                                                                echo 'Total ' . $temp_rekening . ' :</td>';
                                                                echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_menjadi, 0, ',', '.') . '</td>';
                                                                echo '<td colspan="2"></td>';
                                                                echo '</tr>';
                                                            }
                                                            $total_belanja_semula += $total_rekening_semula;
                                                            $total_belanja_menjadi += $total_rekening_menjadi;
                                                            $total_rekening_semula = 0;
                                                            $total_rekening_menjadi = 0;

                                                            //tampil rekening baru
                                                            $c = new Criteria();
                                                            $c->add(RekeningPeer::REKENING_CODE, $value->getRekeningCode());
                                                            $rs = RekeningPeer::doSelectOne($c);
                                                            $temp_rekening = $value->getRekeningCode() . ' ' . $rs->getRekeningName();
                                                            echo '<tr bgcolor="#ffffff"><td colspan="7" class="Font8v style3" align="left"><span class="style3"><strong>' . $value->getRekeningCode() . ' ' . $rs->getRekeningName() . '</strong></span></td>';
                                                            echo '<td>&nbsp;</td>';
                                                            echo '<td colspan="9" class="Font8v style3" align="left"><span class="style3"><strong>' . $value->getRekeningCode() . ' ' . $rs->getRekeningName() . '</strong></span></td></tr>';
                                                        }
                                                        //kiri
                                                        echo '<tr bgcolor="#ffffff" valign="top">';
                                                        echo '<td class="Font8v" align="left"><span class="style3">' . $value->getSemulaKomponen() . '</span></td>';
                                                        echo '<td class="Font8v style3" align="center" nowrap="nowrap"><span class="Font8v">' . $value->getSemulaSatuan() . '</span></td>';
                                                        echo '<td class="Font8v style3" align="center"><span class="Font8v">' . $value->getSemulaKoefisien() . '</span></td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($value->getSemulaHarga(), 0, ',', '.') . '</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($value->getSemulaHasil(), 0, ',', '.') . '</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . $value->getSemulaPajak() . '%</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($value->getSemulaTotal(), 0, ',', '.') . '</td>';

                                                        //tengah
                                                        echo '<td><strong><span style="color:red;">!</span></strong></td>';

                                                        //kanan
                                                        echo '<td class="Font8v" align="left"><span class="style3">' . $value->getMenjadiKomponen() . '</span></td>';
                                                        echo '<td class="Font8v style3" align="center" nowrap="nowrap"><span class="Font8v">' . $value->getMenjadiSatuan() . '</span></td>';
                                                        echo '<td class="Font8v style3" align="center"><span class="Font8v">' . $value->getMenjadiKoefisien() . '</span></td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($value->getMenjadiHarga(), 0, ',', '.') . '</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($value->getMenjadiHasil(), 0, ',', '.') . '</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . $value->getMenjadiPajak() . '%</td>';
                                                        echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($value->getMenjadiTotal(), 0, ',', '.') . '</td>';
                                                        echo '<td align="left">' . trim($value->getCatatan()) . '</td>';
                                                        echo '</tr>';
                                                        $total_rekening_semula += $value->getSemulaTotal();
                                                        $total_rekening_menjadi += $value->getMenjadiTotal();

                                                        $prev_rekening = $value->getRekeningCode();
                                                        $prev_subtitle = $value->getSubtitle();
                                                        $prev_belanja = $value->getBelanja();
                                                    }
                                                    //total rekening
                                                    echo '<tr bgcolor="#ffffff">';
                                                    echo '<td colspan="6" class="Font8v style3" align="right">';
                                                    echo 'Total ' . $temp_rekening . ' :</td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_semula, 0, ',', '.') . '</td>';
                                                    echo '<td> </td>';
                                                    echo '<td colspan="6" class="Font8v style3" align="right">';
                                                    echo 'Total ' . $temp_rekening . ' :</td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_rekening_menjadi, 0, ',', '.') . '</td>';
                                                    echo '<td colspan="2"></td>';
                                                    echo '</tr>';
                                                    $total_belanja_semula += $total_rekening_semula;
                                                    $total_belanja_menjadi += $total_rekening_menjadi;

                                                    //total belanja
                                                    echo '<tr bgcolor="#ffffff">';
                                                    echo '<td colspan="6" class="Font8v style3" align="right">';
                                                    echo 'Total ' . $temp_belanja . ' :</td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_belanja_semula, 0, ',', '.') . '</td>';
                                                    echo '<td> </td>';
                                                    echo '<td colspan="6" class="Font8v style3" align="right">';
                                                    echo 'Total ' . $temp_belanja . ' :</td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap">' . number_format($total_belanja_menjadi, 0, ',', '.') . '</td>';
                                                    echo '<td colspan="2"></td>';
                                                    echo '</tr>';

                                                     $total_semula += $total_belanja_semula;
                                                     $total_menjadi += $total_belanja_menjadi;

                                                    //grand total
                                                    echo '<tr bgcolor="#ffffff">';
                                                    echo '<td colspan="6" class="Font8v style3" align="right"><strong>';
                                                    echo 'Grand Total :</strong></td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($total_semula, 0, ',', '.') . '</strong></td>';
                                                    echo '<td> </td>';
                                                    echo '<td colspan="6" class="Font8v style3" align="right"><strong>';
                                                    echo 'Grand Total :</strong></td>';
                                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap"><strong>' . number_format($total_menjadi, 0, ',', '.') . '</strong></td>';
                                                    echo '<td colspan="2"></td>';
                                                    echo '</tr>';
                                                    ?>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>