<?php

/**
 * cek_qr actions.
 *
 * @package    budgeting
 * @subpackage cek_qr
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class cek_qrActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeCekBeritaAcara() {
        $id = $this->getRequestParameter('id');
        $token = $this->getRequestParameter('token');

        $c = new Criteria();
        $c->add(BeritaAcaraPeer::ID, $id);
        $c->add(BeritaAcaraPeer::TOKEN, $token);
        if ($ba = BeritaAcaraPeer::doSelectOne($c)) {
            $unit_id = $ba->getUnitId();

            $c = new Criteria();
            $c->addDescendingOrderByColumn(BeritaAcaraPeer::ID);
            $c->add(BeritaAcaraPeer::UNIT_ID, $unit_id);
            $c->add(BeritaAcaraPeer::KEGIATAN_CODE, $ba->getKegiatanCode());
            $ba_maks = BeritaAcaraPeer::doSelectOne($c);
            if ($ba_maks->getId() > $ba->getId()) {
                //$ba = $ba_maks;
                $id_baru = $ba_maks->getId();
                $token_baru = $ba_maks->getToken();
                $this->ada_lebih_baru = TRUE;
                $this->param_link_baru = "id=$id_baru&token=$token_baru";
            }

            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $rs_unit = UnitKerjaPeer::doSelectOne($c);
            $this->unit_kerja = $rs_unit->getUnitName();
            $this->kode_kegiatan = $ba->getKegiatanCode();
            $c_detail = new Criteria();
            $c_detail->add(BeritaAcaraDetailPeer::ID_BERITA_ACARA, $id);
            $c_detail->addAscendingOrderByColumn(BeritaAcaraDetailPeer::BELANJA);
            $c_detail->addAscendingOrderByColumn(BeritaAcaraDetailPeer::SUBTITLE);
            $c_detail->addAscendingOrderByColumn(BeritaAcaraDetailPeer::REKENING_CODE);
            $this->detail = BeritaAcaraDetailPeer::doSelect($c_detail);
        } else {
            $this->gagal = TRUE;
        }
    }

    public function executeCekResumeRapat() {
        $id = $this->getRequestParameter('id');
        $token = $this->getRequestParameter('token');

        $c = new Criteria();
        $c->add(DeskripsiResumeRapatPeer::ID, $id);
        $c->add(DeskripsiResumeRapatPeer::TOKEN, $token);
        if ($ba = DeskripsiResumeRapatPeer::doSelectOne($c)) {
            $this->unit_id = $unit_id = $ba->getUnitId();

            $c = new Criteria();
            $c->addDescendingOrderByColumn(DeskripsiResumeRapatPeer::ID);
            $c->add(DeskripsiResumeRapatPeer::UNIT_ID, $unit_id);
            $ba_maks = DeskripsiResumeRapatPeer::doSelectOne($c);
            if ($ba_maks->getId() > $ba->getId()) {
                //$ba = $ba_maks;
                $id_baru = $ba_maks->getId();
                $token_baru = $ba_maks->getToken();
                $this->ada_lebih_baru = TRUE;
                $this->param_link_baru = "id=$id_baru&token=$token_baru";
            }
            $tanggal = $ba->getTanggal();
            $date = date_create($tanggal);
            $day = date('D', strtotime($tanggal));
            $dayList = array('Sun' => 'Minggu', 'Mon' => 'Senin', 'Tue' => 'Selasa', 'Wed' => 'Rabu', 'Thu' => 'Kamis', 'Fri' => 'Jumat', 'Sat' => 'Sabtu');
            $hari = $dayList[$day];
            $month = date('n', strtotime($tanggal));
            $monthList = array(1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
            $bulan = $monthList[$month];
            $this->tanggal = $hari . ', ' . date_format($date, 'd ') . $bulan . date_format($date, ' Y');
            $jam = $ba->getJam();
            if ($jam < 10)
                $jam = '0' . $jam;
            $this->jam = $jam;

            $menit = $ba->getMenit();
            if ($menit < 10)
                $menit = '0' . $menit;
            $this->menit = $menit;

            $this->tempat = $ba->getTempat();
            $this->acara = $ba->getAcara();
            $this->acara2 = $ba->getAcara2();
            $this->pimpinan = $ba->getPimpinan();
            $this->catatan = $ba->getCatatan();
            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $rs_unit = UnitKerjaPeer::doSelectOne($c);
            $this->unit_kerja = $rs_unit->getUnitName();

            $c_detail = new Criteria();
            $c_detail->add(DeskripsiResumeRapatRekeningPeer::ID_DESKRIPSI_RESUME_RAPAT, $id);
            $c_detail->addAscendingOrderByColumn(DeskripsiResumeRapatRekeningPeer::KEGIATAN_CODE);
            $c_detail->addAscendingOrderByColumn(DeskripsiResumeRapatRekeningPeer::REKENING_CODE);
            $this->detail = DeskripsiResumeRapatRekeningPeer::doSelect($c_detail);
        } else {
            $this->gagal = TRUE;
        }
    }

    public function executeCekPrintRka() {
        $this->id_print_rka = $id_print_rka = $this->getRequestParameter('id');
        $this->token = $token = $this->getRequestParameter('token');

        $c = new Criteria();
        $c->add(PrintRkaPakPeer::ID, $id_print_rka);
        $c->add(PrintRkaPakPeer::TOKEN, $token);
        if ($rka = PrintRkaPakPeer::doSelectOne($c)) {
            $c = new Criteria();
            $c->addDescendingOrderByColumn(PrintRkaPakPeer::ID);
            $c->add(PrintRkaPakPeer::UNIT_ID, $rka->getUnitId());
            $c->add(PrintRkaPakPeer::KEGIATAN_CODE, $rka->getKegiatanCode());
            $rka_maks = PrintRkaPakPeer::doSelectOne($c);
            if ($rka_maks->getId() > $rka->getId()) {
                $id_baru = $rka_maks->getId();
                $token_baru = $rka_maks->getToken();
                $this->ada_lebih_baru = TRUE;
                $this->param_link_baru = "id=$id_baru&token=$token_baru";
            }

            $c = new Criteria();
            $c->add(PrintRkaPakDetailPeer::ID_PRINT_RKA_PAK, $id_print_rka);
            $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::SUBTITLE);
            $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::SUBSUBTITLE);
            $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::REKENING_CODE);
            $c->addAscendingOrderByColumn(PrintRkaPakDetailPeer::NAMA_KOMPONEN);
            $this->items = PrintRkaPakDetailPeer::doSelect($c);
            
            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $rka->getUnitId());
            $rs_unit = UnitKerjaPeer::doSelectOne($c);
            $this->unit_kerja = $rs_unit->getUnitName();
            $this->kode_kegiatan = $rka->getKegiatanCode();
        } else {
            $this->gagal = TRUE;
        }
    }

}
