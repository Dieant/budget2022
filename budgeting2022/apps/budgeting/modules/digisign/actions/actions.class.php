<?php

/**
 * digisign actions.
 *
 * @package    budgeting
 * @subpackage digisign
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class digisignActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex()
  {
     // print_r('hello');exit;
  
  }
  public function executeGetPekerjaans()
  {
      $this->unit_id=$unit_id= $this->getRequestParameter('unit_id');
      $this->kode_kegiatan=$kegiatan_code=$this->getRequestParameter('kode_kegiatan');
      $this->nama_subtitle=$nama_subtitle=$this->getRequestParameter('nama_subtitle');
      $id=$this->getRequestParameter('id');
      //print_r('hallo bui');
        if($id)
        {
                $sub_id = $id;
                $c = new Criteria();
                $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
                $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                if($rs_subtitle)
                {
                        $unit_id = $rs_subtitle->getUnitId();
                        $kegiatan_code = $rs_subtitle->getKegiatanCode();
                        $subtitle = $rs_subtitle->getSubtitle();
                        $nama_subtitle = trim($subtitle);
                }

                $c_rincianDetail= new Criteria();
                $c_rincianDetail->add(RincianDetailPeer::UNIT_ID,$unit_id);
                $c_rincianDetail->add(RincianDetailPeer::KEGIATAN_CODE,$kegiatan_code);
                $c_rincianDetail->add(RincianDetailPeer::SUBTITLE,$nama_subtitle,Criteria::ILIKE);
                $c_rincianDetail->add(RincianDetailPeer::STATUS_HAPUS,false);
                $c_rincianDetail->add(RincianDetailPeer::TAHUN,sfConfig::get('app_tahun_default'));
                //$c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KODE_SUB);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::REKENING_CODE);
                $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KOMPONEN_NAME);
                $rs_rd=RincianDetailPeer::doSelect($c_rincianDetail);
                $this->rs_rd=$rs_rd;
                $this->id = $sub_id;
                $this->rinciandetail = 'ada';
                
                $this->setLayout('layout');

    
        }
}
  public function executeDigisign() {
   
        //code to generate pdf
        //print_r($this->getUser()->getAttribute('user_login','',$this->getUser()->getCredentialMember()));exit;
        $this->sub_id=$id=$this->getRequestParameter('id');
        $this->unit_id=$unit_id=$this->getRequestParameter('unit_id');
        $this->kode_kegiatan=$kode_kegiatan=$this->getRequestParameter('kode_kegiatan');
        $this->nama_subtitle= base64_decode($this->getRequestParameter('nama_subtitle'));

        //$html = myTools::getHtmlContent('dinas/getPekerjaans?unit_id='.$unit_id.'&kode_kegiatan='.$kode_kegiatan.'&id='.$id.'&nama_subtitle='.$nama_subtitle, 'layoutdinas');
        $html = $this->getController()->getPresentationFor($this->getModuleName(), 'getPekerjaans');


        $mpdf = new mPDF('en-GB','A4');
        $mpdf->useOnlyCoreFonts = true;

        // LOAD a stylesheet
        $stylesheet = file_get_contents(sfConfig::get('sf_web_dir').'/css/main.css');
        $stylesheet = file_get_contents(sfConfig::get('sf_web_dir').'/css/2c-hd-flex-presentation.css');
        $stylesheet = file_get_contents(sfConfig::get('sf_web_dir').'/css/2c-hd-flex-layout.css');
        $this->namafile=$namafile=$unit_id.'-'.$kode_kegiatan.'-'.$id.'-'.date("d-m-Y_H-i-s");
        try
        {
                $mpdf->WriteHTML($stylesheet, 1);       // The parameter 1 tells that this is css/style only and no body/html/text
                $mpdf->WriteHTML($html);
                $mpdf->Output(sfConfig::get('sf_web_dir').'/pdf/'.$namafile.'.pdf','F');
               // return sfView::NONE;
        }
        catch (Exception $e)
        {
               // throw $e;
                $this->setFlash('gagal', "Gagal Membuat file PDF karena ".$e);
                return $this->redirect('dinas/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kode_kegiatan);
        }
        
  }
  public function validateTandatangan()
  {
        
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if (!$this->getRequest()->hasFile('private_key')) {
                $this->getRequest()->setError('private_key', 'Anda harus memasukkan dokumen kunci privat');
                return false;
            }
            
            $this->private_key = file_get_contents($this->getRequest()->getFilePath('private_key'));
            $email=$this->getUser()->getAttribute('email',null,'otorisasi');
            if (!$email) {
                $this->getRequest()->setError('email', 'Email anda tidak valid');
                return false;
            }
            $this->passPhrase=$this->getRequestParameter('passphrase');
            if (!$this->passPhrase) {
                $this->getRequest()->setError('passphrase', 'Pass phrase tidak boleh kosong');
                return false;
            }
            if (!Otorisasi::validatePrivateKey($this->private_key, $this->passPhrase)) {
                $this->getRequest()->setError('private_key', 'Dokumen kunci privat atau passphrase tidak sesuai dengan kunci publik yang terdaftar pada IKP Pemerintah Kota SURABAYA');
                return false;
            }
        }
        return true;
  }
  public function handleErrorTandatangan()
  {
      //jika terjadi error, maka session variabel untuk private key harus direset
      Otorisasi::reset();
      if($this->getRequest()->getError('email'))
      {$this->setFlash('gagal', $this->getRequest()->getError('email'));}
      else if($this->getRequest()->getError('private_key'))
      {$this->setFlash('gagal', $this->getRequest()->getError('private_key')); }
      else if($this->getRequest()->getError('passphrase'))
      {$this->setFlash('gagal', $this->getRequest()->getError('passphrase'));}
      
      return $this->redirect('dinas/edit?unit_id='.$this->getRequestParameter('unit_id').'&kode_kegiatan='.$this->getRequestParameter('kode_kegiatan'));
  }
  public function executeTandatangan() {
   if ($this->getRequest()->getMethod() == sfRequest::POST) {
        //sampai tahap ini, sudah dilakukan validasi terhadap email, passphrase dan pasangan publik/private key
        //dari server IKP
        //jadi session variabel private_key,jika sudah ada isinya, maka dianggap otorisasi sudah beres
        $this->getContext()->getUser()->setAttribute('private_key',$this->private_key,'otorisasi');
        $this->getContext()->getUser()->setAttribute('passphrase',$this->passPhrase,'otorisasi');
        $this->pathFile=$this->getRequestParameter('namafile');
        //print_r('bisma'.$this->private_key);

        
        $filename = sfConfig::get('sf_web_dir').'/pdf/'.$this->pathFile.'.pdf';
           
        $sertifikat=Otorisasi::downloadPublicKey();
        $openssl = new OpenSSL();
        $openssl->setPrivateKey($this->private_key, FALSE, $this->passPhrase);
        $isi= $openssl->readf($filename);
        $openssl->setPlainText($isi);
        $openssl->sign();
        $signature = $openssl->getSignature();
        $encodeSignature=base64_encode($signature);
        
        try
        {
            $tandatangan = new PdfFile();
            $tandatangan->setUnitId($this->getRequestParameter('unit_id'));
            $tandatangan->setKegiatanCode($this->getRequestParameter('kode_kegiatan'));
            $tandatangan->setSubtitle($this->getRequestParameter('nama_subtitle'));
            $tandatangan->setSubId($this->getRequestParameter('sub_id'));
            $tandatangan->setNamaAsli($this->pathFile.'.pdf');
            $tandatangan->setHash($encodeSignature);
            $tandatangan->setUserId($this->getUser()->getAttribute('user_login','',$this->getUser()->getCredentialMember()));
            $tandatangan->save();
            file_put_contents(sfConfig::get('sf_web_dir').'/pdf/'.$this->pathFile.'.ttd',$encodeSignature); //tanda tangan disimpan di file yang sama dengan pdf, tapi ditambahi ttd
            
            //kunci subtitle
            $subtitle = str_replace('=','/',$this->getRequestParameter('nama_subtitle'));
            $unit_id=$this->getRequestParameter('unit_id');
            $kegiatan_code=$this->getRequestParameter('kode_kegiatan');
            $sub_id=$this->getRequestParameter('sub_id');
            $sql = "update ". sfConfig::get('app_default_schema') .".rincian_detail set lock_subtitle='LOCK' where subtitle='$subtitle' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
            $sql2= "update ". sfConfig::get('app_default_schema') .".subtitle_indikator set lock_subtitle=true where subtitle='$subtitle' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub_id='$sub_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();
            $stmt2 = $con->prepareStatement($sql2);
            $stmt2->executeQuery();
            
            //end kunci subtitle 
            $this->setFlash('berhasil', "File sudah berhasil ditanda tangani silahkan download file dibawah ini untuk keperluan validasi");
            //return $this->redirect('dinas/tandatangan');
        }
        catch (Exception $e)
        {
               // throw $e;
                Otorisasi::reset();
                $this->setFlash('gagal', "Gagal Menandatangani File akibat ".$e);
                return $this->redirect('dinas/list');
        }
        //$decodeTandaTangan = base64_decode($tandaTangan); // kalau mau validasi harus di decode dulu
        
        //print_r('bisma phass '.$openssl->getPrivateKeyPassphrase().' private key '.$openssl->getPrivateKey().' cek '.$openssl->getSignature().' information sertifikat '.$sertifikat);
        //print_r('validasi '.$openssl->verify($isi, $decodeTandaTangan, $sertifikat).' tandatangan '.$tandaTangan.' decode ttd '.$decodeTandaTangan);
        //exit;
        //return $this->redirect('digisign/tandatangan'); // jangan dikembalikan ke digisign biar tidak membuat file baru.
        
    }   
  }
  public function executeValidasi() {
   if ($this->getRequest()->getMethod() == sfRequest::POST) {
       $validasi = new OpenSSL();
       $file = $this->file;
       $signature= $this->signature;
       $decodeTandaTangan = base64_decode($signature);
       $publicKey= $this->public_key;
       $this->returnValidasi=$validasi->verify($file, $decodeTandaTangan, $publicKey);
       if($this->returnValidasi == 1)
       { $this->setFlash('berhasil', 'File dan Tanda Tangan Digital <font color=red><b> VALID </b></font>');}
       else if($this->returnValidasi == 0)
       { $this->setFlash('gagal', 'File dan Tanda Tangan Digital TIDAK VALID ');}
       else
       { $this->setFlash('gagal', 'Terjadi kesalahan dalam memasukkan data. silahkan Ulangi!');}
       
       //print_r('tanda tangan '.$signature.' decode '.$decodeTandaTangan);
       //echo '<br>';
       //print_r('validasi '.$validasi->verify($file, $decodeTandaTangan, $publicKey).' tandatangan '.$this->signature.' decode ttd '.$this->public_key);
       //exit;
   }
 }
 public function validateValidasi()
  {
        
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if (!$this->getRequest()->hasFile('public_key')) {
                $this->getRequest()->setError('public_key', 'Anda harus memasukkan dokumen kunci public yang anda dapatkan sewaktu mendownload file');
                return false;
            }
            $this->public_key = file_get_contents($this->getRequest()->getFilePath('public_key'));
            if (!$this->getRequest()->hasFile('signature')) {
                $this->getRequest()->setError('signature', 'Anda harus memasukkan dokumen tanda tangan yang anda dapatkan sewaktu mendownload file');
                return false;
            }
            $this->signature = file_get_contents($this->getRequest()->getFilePath('signature'));
            if (!$this->getRequest()->hasFile('file')) {
                $this->getRequest()->setError('file', 'Anda harus memasukkan dokumen yang anda dapatkan sewaktu mendownload file');
                return false;
            }
            $this->file = file_get_contents($this->getRequest()->getFilePath('file'));
            
        }
        return true;
  }
  public function handleErrorValidasi()
  {
      //jika terjadi error, maka session variabel untuk private key harus direset
      if($this->getRequest()->getError('public_key'))
      {$this->setFlash('gagal', $this->getRequest()->getError('public_key'));}
      else if($this->getRequest()->getError('signature'))
      {$this->setFlash('gagal', $this->getRequest()->getError('signature')); }
      else if($this->getRequest()->getError('file'))
      {$this->setFlash('gagal', $this->getRequest()->getError('file'));}
      
      return $this->redirect('digisign/validasi');
  }
 
}
