<?php use_helper('I18N', 'Date') ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo $namafile;
?>
<div id="sf_admin_container">
    
<h1>Validasi Tanda Tangan Digital</h1>
<?php include_partial('peneliti/list_messages') ?>
<?php echo form_tag('digisign/validasi',array('multipart'=>true,'method'=>'POST')); ?>
<table cellspacing="0" class="sf_admin_list" width="20%">
<thead>
       
</thead>
<tbody>
        <tr class="sf_admin_row_1" >
            <td width="15%">File</td>
            <td width="50%"><?php echo  input_file_tag('file') ?> <font color="red"> tipe file : .pdf</font>  </td>
        </tr>
        <tr class="sf_admin_row_0" >
            <td width="15%">Tanda Tangan</td>
            <td width="50%"><?php echo input_file_tag('signature')?> <font color="red"> tipe file : .ttd</font></td>
        </tr>
            <tr class="sf_admin_row_1" >
            <td width="15%">Kunci Public</td>
            <td width="50%"><?php echo input_file_tag('public_key') ?> <font color="red"> tipe file : .crt</font></td>
        </tr>
        
</tbody>
<tfoot>
    <tr>
        <th colspan="2" align="right"><?php echo submit_tag('Validasi','class=inpButton') ?></th>
    </tr>
</tfoot>
        
    </table>
</form>

</div>