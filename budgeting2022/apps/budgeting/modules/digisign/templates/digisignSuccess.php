<?php use_helper('I18N', 'Date') ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo $namafile;
?>
<div id="sf_admin_container">
    
<h1>Tanda Tangan Digital</h1>
<?php

if (!Otorisasi::isValid()) {
    echo '<h3 style="color:red">Maaf anda belum upload private key</h3>';
   
?>
<?php echo form_tag('digisign/tandatangan',array('multipart'=>true,'method'=>'POST')); ?>
<table cellspacing="0" class="sf_admin_list" width="20%">
<thead>
       
</thead>
<tbody>
        <tr class="sf_admin_row_1" >
            <td width="15%">File</td>
            <td width="50%"><?php echo  link_to($namafile.'.pdf', sfConfig::get('app_path_pdf').$namafile.'.pdf') ?> </td>
        </tr>
        <tr class="sf_admin_row_0" >
            <td width="15%">Phassphrase</td>
            <td width="50%"><?php echo input_password_tag('passphrase') ?></td>
        </tr>
        <tr class="sf_admin_row_1">
            <td>Private Key<font color="red"></font></td>
            <td><?php echo input_file_tag('private_key')?></td>
            <?php echo input_hidden_tag('unit_id', $unit_id); ?>
            <?php echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);?>
            <?php echo input_hidden_tag('sub_id',$sub_id); ?>
            <?php echo input_hidden_tag('nama_subtitle', $nama_subtitle);?>
            <?php echo input_hidden_tag('namafile',$namafile); ?>
        </tr>
</tbody>
<tfoot>
    <tr>
        <th colspan="2" align="right"><?php echo submit_tag('Tanda Tangan','class=inpButton') ?></th>
    </tr>
</tfoot>
        
    </table>
</form>
<?php   
} else {
    echo "<h3>Kunci Private sudah divalidasi!</h3><br/><pre>$privateKey</pre><br/><hr/>";
}
?>

</div>