<?php use_helper('I18N', 'Date') ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//echo $namafile;
?>
<div id="sf_admin_container">
    
<h1><?php echo __('Tanda Tangan Digital', 
array()) ?></h1>
<?php include_partial('peneliti/list_messages') ?>
<?php
 //print_r('bisma '.$pathFile.' tanda tangan '.$signature);

if (!Otorisasi::isValid()) {
    echo '<h3 style="color:red">Maaf anda belum upload private key, silahkan ulangi proses tanda tangan</h3>';
   
} else {
   // print_r('bisma'.$pathFile);
    Otorisasi::reset();
?>
<table cellspacing="0" class="sf_admin_list" width="20%">
<thead>
       
</thead>
<tbody>
        <tr class="sf_admin_row_1" >
            <td width="15%">File</td>
            <td width="50%"><?php echo  link_to($pathFile.'.pdf', sfConfig::get('app_path_pdf').$pathFile.'.pdf') ?> </td>
        </tr>
        <tr class="sf_admin_row_0" >
            <td width="15%">Tanda Tangan</td>
            <td width="50%"><?php echo  link_to($pathFile.'.ttd', sfConfig::get('app_path_pdf').$pathFile.'.ttd',array('popup' => array('Tanda Tangan Digital', 'width=500,height=100,left=150,top=150'))) ?>
                <font color="red"> *Akan terbuka layar baru dan tekan Ctrl + S untuk menyimpan*</font> </td>
        </tr>
        <tr class="sf_admin_row_1">
            <td>Kunci Public<font color="red"></font></td>
            <td><?php echo link_to('Kunci Public', 'https://www.ikp-surabaya.com/index.php/repository/getCertByEmail.html?id='.$sf_user->getAttribute('email','','otorisasi')); ?></td>
            
        </tr>
</tbody>
<tfoot>
    
</tfoot>
        
    </table>
<?php    
}
?>

</div>