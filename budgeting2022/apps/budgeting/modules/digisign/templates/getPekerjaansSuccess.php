<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_dinas');

$i = 0;
$kode_sub = '';
$temp_rekening = '';
// print_r($rs_rinciandetail.'<br><br>'.$rs_rd);
?>

<table cellspacing="0" class="sf_admin_list" style="font-size: 10">
    <thead>
        <tr>
            <th><b>Subtitle</b></th>
            <th><b>Rekening | Komponen</b></th>
            <th><b>Satuan</b></th>
            <th><b>Koefisien</b></th>
            <th><b>Harga</b></th>
            <th><b>Hasil</b></th>
            <th><b>PPN</b></th>
            <th><b>Total</b></th>
            <th><b>Belanja</b></th>
        </tr>
    <tbody>
        <tr >
            <td colspan="7"><b><i>
                        <?php echo $nama_subtitle ?></i></b>   
            </td>

            <td align="right">
                <?php
                $query2 = "select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
			from " . sfConfig::get('app_default_schema') . ".rincian_detail
			where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query2);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                }
                ?></td>
            <td>&nbsp;</td>
        </tr>
        <?php
        foreach ($rs_rd as $rd):


            $odd = fmod($i++, 2);
            $unit_id = $rd->getUnitId();
            $kegiatan_code = $rd->getKegiatanCode();


            if ($kode_sub != $rd->getKodeSub()) {
                $kode_sub = $rd->getKodeSub();
                $sub = $rd->getSub();
                $cekKodeSub = substr($kode_sub, 0, 4);

                if ($cekKodeSub == 'RKAM') {//RKA Member
                    $C_RKA = new Criteria();
                    $C_RKA->add(RkaMemberPeer::KODE_SUB, $kode_sub);
                    $C_RKA->addAscendingOrderByColumn(RkaMemberPeer::KODE_SUB);
                    $rs_rkam = RkaMemberPeer::doSelectOne($C_RKA);
                    //print_r($rs_rkam);exit;
                    if ($rs_rkam) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                            <td colspan="7">
                                <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                            </td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                                ?></td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    } else { //else RKAM
                    }
                } else { //else form SUB
                    $c = new Criteria();
                    $c->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                    $rs_subparameter = RincianSubParameterPeer::doSelectOne($c);
                    if ($rs_subparameter) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                            <td colspan="7">
                                <b> :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b></td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                                /*
                                  $query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
                                  from ". sfConfig::get('app_default_schema') .".rincian_detail
                                  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
                                  $con = Propel::getConnection();
                                  $stmt = $con->prepareStatement($query2);
                                  $t = $stmt->executeQuery();
                                  while($t->next())
                                  {
                                  echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                  } */
                                ?></td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    } else {
                        $ada = 'tidak';
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                        //print_r($query);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                    <td colspan="7">
                                        <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                                    <td align="right">
                                        <?php
                                        echo number_format($rd->getTotalSub($sub), 0, ',', '.');

                                        /* $query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
                                          from ". sfConfig::get('app_default_schema') .".rincian_detail
                                          where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%'";
                                          $con = Propel::getConnection();
                                          $stmt = $con->prepareStatement($query2);
                                          $t = $stmt->executeQuery();
                                          while($t->next())
                                          {
                                          echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                          } */
                                        $ada = 'ada';
                                        ?> </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <?php
                            }
                        }

                        if ($ada == 'tidak') {
                            if ($kode_sub != '') {
                                $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub = '$sub' and status_hapus=false";
                                //print_r($query);exit;
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    if ($t->getString('kode_sub')) {
                                        ?>
                                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                            <td colspan="7">
                                                <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b>
                                            </td>

                                            <td align="right">
                                                <?php
                                                echo number_format($rd->getTotalSub($sub), 0, ',', '.');

                                                /* $query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
                                                  from ". sfConfig::get('app_default_schema') .".rincian_detail
                                                  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%'";
                                                  $con = Propel::getConnection();
                                                  $stmt = $con->prepareStatement($query2);
                                                  $t = $stmt->executeQuery();
                                                  while($t->next())
                                                  {
                                                  echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                  } */
                                                $ada = 'ada';
                                            }
                                            ?>
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }

            $rekening_code = $rd->getRekeningCode();
            //echo $rd->getRekeningCode();
            if ($temp_rekening != $rekening_code) {
                $temp_rekening = $rekening_code;
                $c = new Criteria();
                $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
                $rs_rekening = RekeningPeer::doSelectOne($c);
                if ($rs_rekening) {
                    $rekening_name = $rs_rekening->getRekeningName();
                    ?>
                    <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                        <td colspan="7"><i><?php echo $rekening_code . ' ' . $rekening_name ?></i> </td>

                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr class="pekerjaans_<?php echo $id ?>">
                <td>
                    <?php
                    $kegiatan = $rd->getKegiatanCode();
                    $unit = $rd->getUnitId();
                    $no = $rd->getDetailNo();
                    $sub = $rd->getSubtitle();

                    //djieb : subtitle lock
                    if ($rd->getLockSubtitle() <> 'LOCK') {

                        if ($rd->getFromSubKegiatan() == '') {
                            if ($status == 'OPEN') {
                                //if($unit_id=='3000' and $kegiatan_code=='0018'){
                                if (($rd->getTipe() != 'SUB') || ( $rd->getFromSubKegiatan() == 'PJU001 ' || $rd->getFromSubKegiatan() == 'PJU002')) {

                                    //djiebrats : join with e-delivery we give words "Nilai Swakelola pada e-delivery = Rp. XXXXX"
                                    //	       and give words "Nilai kontraks"
                                    //eof djiebrats
                                    //cek apakah sudah ada di eproject, kalo sudah ada tidak bisa dihapus
                                    /*
                                      $nilaiTerpakai=0;
                                      $jumlahRows=0;
                                      $query_eproject="SELECT
                                      s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting, sum(dp.ALOKASI) as jml
                                      from
                                      eproject.detail_pekerjaan dp, eproject.pekerjaan p,
                                      eproject.detail_kegiatan dk,
                                      eproject.kegiatan k, eproject.skpd s
                                      where
                                      (p.STATE<>0) and (p.STATE IS NOT NULL) and
                                      dp.pekerjaan_id=p.id  and p.kegiatan_id=k.id and s.kode='$unit_id' and k.kode='$kegiatan' and dk.id_budgeting=$no
                                      and k.skpd_id=s.id and dp.detail_kegiatan_id=dk.id
                                      group by s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting";

                                      $con = Propel::getConnection();
                                      $statement=$con->prepareStatement($query_eproject);
                                      $rs_eprojects=$statement->executeQuery();
                                      while($rs_eprojects->next())
                                      {
                                      $jumlahRows = $rs_eprojects->getRow();
                                      $nilaiTerpakai=$rs_eprojects->getString('jml');
                                      }
                                      //  print_r('jum '.$jumlahRows.' count :'. count($jumlahRows));
                                     */
                                    //EOF Cek
                                    //untuk edit ada di lib/model/". sfConfig::get('app_default_schema') ."/rincianDetail.php
                                    //bisma : cek ke Edeliveri pakai fungsi.

                                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                        $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan, $no);
                                        $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan, $no);
                                        $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kegiatan, $no);
                                        $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kegiatan, $no);
                                        //print_r('test'.$totNilaiSwakelola.' '.$totNilaiKontrak);

                                        if ($totNilaiSwakelola > 0) {
                                            echo '<br><font color=red> Nilai Total swakelola pada <br>e-delivery = Rp ' . number_format($totNilaiSwakelola, 0, ',', '.') . '</font><br><br>';
                                        } elseif ($totNilaiKontrak > 0) {
                                            echo '<font color=red> Nilai Total Kontrak pada <br>e-delivery = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</font><br><br>';
                                        } elseif ($totNilaiRealisasi > 0) {
                                            echo '<font color=red> Nilai Total Realisasi pada <br>e-delivery = Rp ' . number_format($totNilaiRealisasi, 0, ',', '.') . '</font><br><br>';
                                        } elseif ($totVolumeRealisasi > 0) {
                                            echo '<font color=red> Volume Realisasi pada <br>e-delivery = Rp ' . number_format($totVolumeRealisasi, 0, ',', '.') . '</font><br><br>';
                                        }

                                        //print_r($totNilaiSwakelola);
                                        if ($totNilaiKontrak == 0 and $totNilaiSwakelola == 0) {
                                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusKegiatan(' . $id . ',"' . $kegiatan . '","' . $unit . '",' . $no . ')');
                                        }
                                    }
                                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'tutup') {
                                        //echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'",'.$no.')');    
                                    }
                                    //echo link_to(image_tag('/sf/sf_admin/images/edit.png'), 'dinas/editKegiatan?id='.$rd->getDetailNo().'&unit='.$rd->getUnitId().'&kegiatan='.$rd->getKegiatanCode().'&edit='.md5('ubah'));
                                    //}
                                }
                            }
                        } elseif ($rd->getFromSubKegiatan() != '') {
                            if ($status == 'OPEN') {
                                //if($unit_id=='3000' and $kegiatan_code=='0018'){

                                if (($rd->getTipe() == 'SUB') || ( $rd->getFromSubKegiatan() == 'PJU001 ' || $rd->getFromSubKegiatan() == 'PJU002')) {

                                    if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                        $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan, $no);
                                        $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan, $no);
                                        $totNilaiRealisasi = $rd->getCekRealisasi($unit_id, $kegiatan, $no);
                                        $totVolumeRealisasi = $rd->getCekVolumeRealisasi($unit_id, $kegiatan, $no);

                                        if ($totNilaiSwakelola > 0) {
                                            echo '<br><font color=red> Nilai Total swakelola pada <br>e-delivery = Rp ' . number_format($totNilaiSwakelola, 0, ',', '.') . '</font><br><br>';
                                        } elseif ($totNilaiKontrak > 0) {
                                            echo '<font color=red> Nilai Total Kontrak pada <br>e-delivery = Rp ' . number_format($totNilaiKontrak, 0, ',', '.') . '</font><br><br>';
                                        } elseif ($totNilaiRealisasi > 0) {
                                            echo '<font color=red> Nilai Total Realisasi pada <br>e-delivery = Rp ' . number_format($totNilaiRealisasi, 0, ',', '.') . '</font><br><br>';
                                        } elseif ($totVolumeRealisasi > 0) {
                                            echo '<font color=red> Volume Realisasi pada <br>e-delivery = Rp ' . number_format($totVolumeRealisasi, 0, ',', '.') . '</font><br><br>';
                                        }
                                        //print_r($jumlahRows);
                                        if ($totNilaiKontrak == 0 and $totNilaiSwakelola == 0) {
                                            //echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'",'.$no.')');
                                        }
                                    }

                                    //echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'",'.$no.')');
                                    //echo link_to(image_tag('/sf/sf_admin/images/edit.png'), 'dinas/editKegiatan?id='.$rd->getDetailNo().'&unit='.$rd->getUnitId().'&kegiatan='.$rd->getKegiatanCode().'&edit='.md5('ubah'));
                                    //}
                                }
                            }
                        }
                    }
                    ?>
                </td>

                <td><?php
                    $c = new Criteria();
                    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
                    $rs_est_fisik = KomponenPeer::doSelectOne($c);
                    if ($rd->getTipe() == 'FISIK' || $rs_est_fisik->getIsEstFisik()) {
                        echo $rd->getKomponenName();
                    } else {
                        echo $rd->getKomponenName() . ' ';
                        if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                            echo $rd->getDetailName() . ' ';
                        }
                    }

                    if ($rd->getLockSubtitle() == 'LOCK')
//djiebrats : give image "new" when this kompone have status_masuk is new
                        $query2 = "select status_masuk from " . sfConfig::get('app_default_schema') . ".komponen where komponen_id='" . $rd->getKomponenId() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $t = $stmt->executeQuery();
                    while ($t->next()) {
                        
                    }
//DJIEBRATS:
                    /*
                      if($nilaiTerpakai!=0){
                      echo '<br><font color=red>sudah dipaketkan di e-project sebesar Rp
                      '.number_format($nilaiTerpakai, 0, ',', '.').'</font><br>';
                      }
                     */
                    //djiebrats : join with e-delivery we give words "Nilai Swakelola pada e-delivery = Rp. XXXXX"
                    //and give words "Nilai kontraks"
                    /*
                      if(sfConfig::get('app_fasilitas_cekeDelivery')=='buka')
                      {
                      $totNilaiSwakelola=$rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan, $no);
                      $totNilaiKontrak=$rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan, $no);

                      if($totNilaiSwakelola>0){
                      echo '<br><font color=red> Nilai Total swakelola pada <br>e-delivery = Rp '.number_format($totNilaiSwakelola, 0, ',', '.').'</font><br><br>';
                      }
                      if($totNilaiKontrak>0){
                      echo '<font color=red> Nilai Total Kontrak pada <br>e-delivery = Rp '.number_format($totNilaiKontrak, 0, ',', '.').'</font><br><br>';
                      }
                      } */
//eof djiebrats			
                    ?>


                </td>
                <td align="center"><?php echo $rd->getSatuan() ?></td>
                <td align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
                <td align="right"><?php
                    if ($rd->getSatuan() == '%') {
                        echo $rd->getKomponenHargaAwal();
                    } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                        echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }

                    if ($rd->getStatusLelang() == 'lock') {
                        echo image_tag('/images/bahaya2.gif');
                    }

                    if ($rd->getStatusLelang() == 'unlock') {
                        echo form_tag("dinas/ubahhargadasar?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                        echo input_tag("komponen_harga_awal", '', array('style' => 'width:50px', 'value' => $rd->getKomponenHargaAwal()));
                        echo "<br>";
                        echo submit_tag(' OK ');
                        echo "</form>";
                    }
                    ?></td>
                <td align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td align="right"><?php echo $rd->getPajak() . '%'; ?></td>
                <td align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $pajak = $rd->getPajak();
                    $total = $volume * $harga * (100 + $pajak) / 100;
                    echo number_format($total, 0, ',', '.');
                    ?></td>
                <td align="center">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>
            </tr>
            <?php
        endforeach;
        ?>
    </tbody>

</table>

<br/>
<br/>
<table cellspacing="0" class="sf_admin_list" style="font-size: 12" width="100%" >
    <tr >
        <td width="33%">&nbsp;</td>
        <td width="33%">&nbsp;</td>

        <td align="center" width="33%">
            Surabaya, <?php echo date('d-M-Y'); ?>
            <br/><br/>
            <br/><br/>
            <br/><br/>
            <?php echo $sf_user->getNamaLengkap() ?>
        </td>
    </tr>
</table>
