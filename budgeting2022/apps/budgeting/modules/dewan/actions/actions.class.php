<?php

/**
 * dewan actions.
 *
 * @package    budgeting
 * @subpackage dewan
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class dewanActions extends sfActions {

    /**
     * Executes index action
     *
     */

    public function executeBandingaslipakbp() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        if ($this->getRequestParameter('status'))
            $status = $this->status = $this->getRequestParameter('status');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = PakBukuPutihMasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->tabel_semula = $tabel_semula = 'rincian_detail';
            // if ($master_kegiatan->getIsPernahRka()) {
            //     $this->tabel_semula = $tabel_semula = 'prev_rincian_detail';
            // }
            $this->catatan = $master_kegiatan->getCatatan();
            $this->catatan_pembahasan = $master_kegiatan->getCatatanPembahasan();
            // // $this->catatan_bpkpd = $master_kegiatan->getCatatanBpkpd();
            // // $this->catatan_penyelia = $master_kegiatan->getCatatanPenyelia();
            // // $this->catatan_bappeko = $master_kegiatan->getCatatanBappeko();
            // $this->tahap = $master_kegiatan->getTahap();
            // $this->level_kegiatan = $master_kegiatan->getStatusLevel();
            $this->kegiatan_is_tapd_setuju = $master_kegiatan->getIsTapdSetuju();
            $this->kegiatan_is_bappeko_setuju = $master_kegiatan->getIsBappekoSetuju();
            $this->kegiatan_is_penyelia_setuju = $master_kegiatan->getIsPenyeliaSetuju();
            $this->user_id_pptk = $master_kegiatan->getUserIdPptk();
            $this->user_id_kpa = $master_kegiatan->getUserIdKpa();
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 9, 2);
            $this->ubah_f1_dinas = $master_kegiatan->getUbahF1Dinas();
            $this->ubah_f1_peneliti = $master_kegiatan->getUbahF1Peneliti();
            $this->sisa_lelang_dinas = $master_kegiatan->getSisaLelangDinas();
            $this->sisa_lelang_peneliti = $master_kegiatan->getSisaLelangPeneliti();
            $this->catatan_sisa_lelang_dinas = $master_kegiatan->getCatatanSisaLelangDinas();
            $this->catatan_sisa_lelang_peneliti = $master_kegiatan->getCatatanSisaLelangPeneliti();

            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 8);
            }
//print_r($kode_program22);
//exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
                from " . sfConfig::get('app_default_schema') . ".master_program kp
                where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
                from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
                where kp2.kode_program2 ilike '" . $master_kegiatan->getKodeProgram2() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
                from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
                where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

//              $query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail_bp rd
//                      where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            $query = "select sum(nilai_anggaran) as nilai from " .
                    sfConfig::get('app_default_schema') . ".$tabel_semula rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukuputih_rincian_detail rd
                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }

            $status_level = -1;
            if ($this->getUser()->hasCredential('dinas')) {
                $status_level = 0;
            } elseif ($this->getUser()->hasCredential('pptk')) {
                $status_level = 1;
            } elseif ($this->getUser()->hasCredential('kpa')) {
                $status_level = 2;
            } elseif ($this->getUser()->hasCredential('pa')) {
                $status_level = 3;
            } elseif ($this->getUser()->getNamaUser() == 'anggaran' || $this->getUser()->hasCredential('bappeko')) {
                $status_level = 4;
            } elseif ($this->getUser()->hasCredential('peneliti') || $this->getUser()->getNamaLogin() == 'admin' || $this->getUser()->getNamaLogin() == 'superadmin_ebudgeting') {
                $status_level = 0;
            } else {
                $status_level = 8;
            }
            $this->status_level = $status_level;

            $temp = FALSE;
            $c = new Criteria();
            $c->add(PakBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(PakBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(PakBukuPutihRincianDetailPeer::STATUS_LEVEL, 6);
            if (PakBukuPutihRincianDetailPeer::doSelectOne($c))
                $temp = TRUE;
            $this->akan_masuk_rka = $temp;
            $this->tahap = 'pak';

        }
    }

    public function executeUbahPassLogin() {
        $username = $this->getUser()->getNamaLogin();
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            if ($this->getRequestParameter('keluar')) {

                // if ($this->getUser()->hasCredential('dinas'))
                    // return $this->redirect('login/logoutDinas');
                // else 
                if ($this->getUser()->hasCredential('dewan'))
                    return $this->redirect('login/logoutDewan');
                // else if ($this->getUser()->hasCredential('viewer'))
                //     return $this->redirect('login/logoutViewer');
                // else if ($this->getUser()->hasCredential('peneliti'))
                //     return $this->redirect('login/logoutPeneliti');
                // else if ($this->getUser()->hasCredential('data'))
                //     return $this->redirect('login/logoutData');
                else
                    return $this->redirect('login/logout');
            }
            if ($this->getRequestParameter('simpan')) {
                $pass_lama = $this->getRequestParameter('pass_lama');
                $pass_baru = $this->getRequestParameter('pass_baru');
                $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
                $md5_pass_lama = md5($pass_lama);
                $md5_pass_baru = md5($pass_baru);

                $c_user = new Criteria();
                $c_user->add(MasterUserV2Peer::USER_ID, $username);
                $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
                $rs_user = MasterUserV2Peer::doSelectOne($c_user);
                if ($rs_user) {
                    if ($pass_baru == $ulang_pass_baru) {
                        $con = Propel::getConnection();
                        $con->begin();
                        try {
                            $newSetting = MasterUserV2Peer::retrieveByPK($username);
                            //$newSetting->setUserName($namaUser);
                            //$newSetting->setNip($nip);
                            $newSetting->setUserPassword($md5_pass_baru);
                            $newSetting->save($con);
                            budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                            $this->setFlash('berhasil', 'Password sudah berhasil dirubah, silahkan keluar dan login kembali');
                            // $con->commit();

                            $c = new Criteria();
                            $c->add(SchemaAksesV2Peer::USER_ID, $username);
                            $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                            $cs = SchemaAksesV2Peer::doSelectOne($c);
                            $cs->setIsUbahPass(true);
                            $cs->save($con);
                            $con->commit();
                            // return $this->redirect('entri/eula?unit_id=' . $unit_id);
                        } catch (Exception $e) {
                            $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                            $con->rollback();
                        }
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
        $this->setLayout('layouteula');
    }

    public function executeIndex() {
        $this->redirect('dewan/listRevisi');
    }

    public function executeReport() {
        $this->kirim = 3;
        return sfView::SUCCESS;
    }

    public function executeTampilReport() {
        $jenis = $this->getRequestParameter('b');
        switch ($jenis) {
            case 'rd':
                $this->executeReportDinas();
                $this->setTemplate('reportDinas');
                break;
            case 'pd':
                $this->executePilihDinas();
                $this->setTemplate('pilihDinas');
                break;
            case 'rdkb':
                $this->executeReportDinasKegiatanBelanja($this->getRequestParameter('dinas'));
                $this->setTemplate('reportDinasKegiatanBelanja');
                break;
            case 'rdk':
                $this->executeReportDinasKegiatan();
                $this->setTemplate('reportDinasKegiatan');
                break;
            case 'rb':
                $this->executeReportBelanja();
                $this->setTemplate('reportBelanja');
                break;
            case 'rbr':
                $this->executeReportBelanjaRekening();
                $this->setTemplate('reportBelanjaRekening');
                break;
            case 'rpd':
                $this->executeReportProgramDinas();
                $this->setTemplate('reportProgramDinas');
                break;
            case 'rp13':
                $this->executeReportProgramP13();
                $this->setTemplate('reportProgramP13');
                break;
            case 'rp':
                $this->executeReportProgram();
                $this->setTemplate('reportProgram');
                break;
            case 'rpk':
                $this->executeReportProgramKegiatan();
                $this->setTemplate('reportProgramKegiatan');
                break;
            case 'rdks':
                $this->executeReportDinasKegiatanSubtitle();
                $this->setTemplate('reportDinasKegiatanSubtitle');
                break;
            case 'rpks':
                $this->executeReportProgramKegiatanSubtitle();
                $this->setTemplate('reportProgramKegiatanSubtitle');
                break;
            case 'rrk':
                $this->executeReportRekeningKomponen();
                $this->setTemplate('reportRekeningKomponen');
                break;
            case 'pr':
                $this->executePilihRekening();
                $this->setTemplate('pilihRekening');
                break;
            case 'rrkk':
                $this->executeReportRekeningKomponenKegiatan();
                $this->setTemplate('reportRekeningKomponenKegiatan');
                break;
            default:
                break;
        }
    }

    public function executePrintrekening() {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan;
        $this->unit_id = $unit_id;
        $this->setLayout('kosong');
    }
    
    public function executePrintsubtitlerekening() {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan;
        $this->unit_id = $unit_id;
        $this->setLayout('kosong');
    }
    
    public function executePrintsubtitlerekeningRevisi() {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan;
        $this->unit_id = $unit_id;
        $this->setLayout('kosong');
    }
    
    public function executePrintrekeningkomponen() {
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $kode_kegiatan;
        $this->unit_id = $unit_id;
        $this->setLayout('kosong');
    }

    public function executeReportDinas() {
        $this->setFlash('RD', 'selected');
        $query = "select unit_id, unit_name , sum(nilai_draft) as pagu,sum((jumlah_draft)) as total_draft,sum((jumlah_setuju)) as total_locked,
			 					sum((nilai_setuju)) as nilai_draft, sum((nilai_setuju*jumlah_setuju)) as nilai_locked
						FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian6_dewan 
						WHERE unit_id<>'9999'
						GROUP BY unit_id, unit_name
						ORDER BY unit_id,unit_name";

        $query2 = "select nilai_anggaran as sum_draft
			 from " . sfConfig::get('app_default_schema') . ".dewan_rincian r," . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan k," . sfConfig::get('app_default_schema') . ".dewan_rincian_detail d 
			where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id  and d.status_hapus=FALSE and d.volume>0 and 
			d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";

        $con = Propel::getConnection();
        $stmt2 = $con->prepareStatement($query2);
        $rs2 = $stmt2->executeQuery();
        while ($rs2->next()) {
            $sum_draft = $rs2->getString('sum_draft');
        }

        $query2 = "select nilai_anggaran as sum_locked
			 from " . sfConfig::get('app_default_schema') . ".dewan_rincian r, " . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail d 
			where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.status_hapus=FALSE and
			d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";

        $stmt3 = $con->prepareStatement($query2);
        $rs3 = $stmt3->executeQuery();
        while ($rs3->next()) {
            $sum_locked = $rs3->getString('sum_locked');
        }

        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if (!$sum_draft)
            $sum_draft = 9999999999;
        if (!$sum_locked)
            $sum_locked = 9999999999;

        $i = 0;
        $this->total_pagu = 0;
        $this->total_selisih = 0;

//foreach($rs as $s)
        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $pagu_arr = array();
        $this->pagu = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $selisih_arr = array();
        $this->selisih = array();

        $nilai_usulan_arr = array();
        $this->nilai_usulan = array();

        while ($rs->next()) {
            $persen_draft_arr[$i] = $rs->getString('nilai_draft') * 100 / $sum_draft;
            $persen_draft_arr[$i] = number_format($persen_draft_arr[$i], 2, ",", ".");
            $persen_locked_arr[$i] = $rs->getString('nilai_locked') * 100 / $sum_locked;
            $persen_locked_arr[$i] = number_format($persen_locked_arr[$i], 2, ",", ".");

            $pagu_arr[$i] = $rs->getString('pagu');

            $query3 = "select sum(nilai_draft) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4_dewan rd where rd.unit_id='" . $rs->getString('unit_id') . "'";
            $stmt5 = $con->prepareStatement($query3);
            $rs5 = $stmt5->executeQuery();

            while ($rs5->next()) {
                $nilai_usulan_arr[$i] = number_format($rs5->getString('nilai_usulan'), 0, ",", ".");
                $this->nilai_draft2+=$rs5->getString('nilai_usulan');
            }

            $this->total_pagu+=$pagu_arr[$i];
            $selisih_arr[$i] = $pagu_arr[$i] - $rs->getString('nilai_draft');
            $this->total_selisih+=$selisih_arr[$i];
            $this->total_draft2+=$rs->getString('total_draft');
            $this->nilai_locked2+=$rs->getString('nilai_locked');
            $this->total_locked2+=$rs->getString('total_locked');
            $nilai_draft_arr[$i] = number_format($rs->getString('nilai_draft'), 0, ",", ".");
            $nilai_locked_arr[$i] = number_format($rs->getString('nilai_locked'), 0, ",", ".");
            $pagu_arr[$i] = number_format($pagu_arr[$i], 0, ",", ".");
            $selisih_arr[$i] = number_format($selisih_arr[$i], 0, ",", ".");
            $i+=1;
        }

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->total_pagu = number_format($this->total_pagu, 0, ",", ".");
        $this->total_selisih = number_format($this->total_selisih, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->pagu = $pagu_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->selisih = $selisih_arr;

        $this->nilai_usulan = $nilai_usulan_arr;
    }

    public function executeReportRekeningKomponenKegiatan() {
        $active_rek = $this->getRequestParameter('rek');
        $query = "select rekening_code as old_rekening_code,rekening_name as old_rekening_name,komponen_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
		FROM " . sfConfig::get('app_default_schema') . ".v_rekening_komponen
		WHERE unit_id<>'9999' and unit_id<>'4444' and rekening_code='$active_rek'
		GROUP BY rekening_code,rekening_name,komponen_name
		ORDER BY rekening_code,rekening_name,komponen_name";
        $add1 = " and substring(k.kode_kegiatan,1,1)='0' ";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r, " . sfConfig::get('app_default_schema') . ".master_kegiatan k, " . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'  and r.unit_id<>'4444' $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.unit_id<>'4444'  and r.rincian_level=3 $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }


        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($rs) {
            if ($sum_locked == 0 or ! $sum_locked)
                $sum_locked = 99999999999;
            if ($sum_draft == 0 or ! $sum_draft)
                $sum_draft = 99999999999;
            $i = 0;
            $j = 0;
            $rekening_code = '';
            $nilai_draft = 0;
            $nilai_draft2 = 0;
            $nilai_locked = 0;
            $nilai_locked2 = 0;
            $persen_draft = 0;
            $persen_locked = 0;

            $persen_draft_arr = array();
            $this->persen_draft = array();

            $persen_locked_arr = array();
            $this->persen_locked = array();

            $nilai_draft_arr = array();
            $this->nilai_draft = array();

            $nilai_locked_arr = array();
            $this->nilai_locked = array();

            $dinas_ok_arr = array();
            $this->dinas_ok = array();

            $rekening_code_arr = array();
            $this->rekening_code = array();

            $rekening_name_arr = array();
            $this->rekening_name = array();

            $rekening_name = '';
            $persen_draft3 = 0;

            $persen_draft3_arr = array();
            $this->persen_draft3 = array();
            $persen_locked3 = 0;

            $persen_locked3_arr = array();
            $this->persen_locked3 = array();

            $nilai_draft3_arr = array();
            $this->nilai_draft3 = array();

            $units_arr = array();
            $this->units = array();

            $nilai_locked3_arr = array();
            $this->nilai_locked3 = array();

            $this->komponen_name = array();
            $komponen_name = array();
            //foreach($rs as $s)
            while ($rs->next()) {
                $komponen_name[$i] = $rs->getString('komponen_name');
                if ($rekening_code != $rs->getString('old_rekening_code')) {
                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;
                    $persen_draft = $nilai_draft * 100 / $sum_draft;
                    //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
                    $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                    $persen_locked = $nilai_locked * 100 / $sum_locked;
                    //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
                    $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                    //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
                    $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
                    $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                    //$rs[$i-$j]['dinas_ok']='t';
                    $dinas_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['rekening_code']=$rekening_code;
                    $rekening_code_arr[$i - $j] = $rekening_code;
                    //$rs[$i-$j]['rekening_name']=$rekening_name;
                    $rekening_name_arr[$i - $j] = $rekening_name;
                    $j = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                }
                //$rekening_code=$rs[$i]['old_rekening_code'];
                $rekening_code = $rs->getString('old_rekening_code');
                //$rekening_name=$rs[$i]['old_rekening_name'];
                $rekening_name = $rs->getString('old_rekening_name');

                //$nilai_draft+=$rs[$i]['nilai_draft3'];
                $nilai_draft+=$rs->getString('nilai_draft3');
                //$nilai_locked+=$rs[$i]['nilai_locked3'];
                $nilai_locked+=$rs->getString('nilai_locked3');
                //$persen_draft3=$rs[$i]['nilai_draft3'] * 100 / $sum_draft;
                $persen_draft3 = $rs->getString('nilai_draft3') * 100 / $sum_draft;
                //$rs[$i]['persen_draft3']=number_format($persen_draft3,2,",",".");
                $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
                //$persen_locked3=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
                $persen_locked3 = $rs->getString('nilai_locked3') * 100 / $sum_locked;
                //$rs[$i]['persen_locked3']=number_format($persen_locked3,2,",",".");
                $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");


                //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
                $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
                //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
                $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");

                $q10 = "SELECT distinct unit_name,nama_kegiatan from " . sfConfig::get('app_default_schema') . ".rincian_detail r," . sfConfig::get('app_default_schema') . ".master_kegiatan k,unit_kerja u where k.unit_id=r.unit_id and k.kode_kegiatan=r.kegiatan_code and r.unit_id=u.unit_id and r.komponen_name='" . $rs->getString('komponen_name') . "' and r.unit_id<>'9999' and r.unit_id<>'4444'";
                //echo $q10;exit;
                //$r10 = @pg_query($conn_id,$q10	);
                $stmt = $con->prepareStatement($q10);
                //$rs10=pg_fetch_all($r10);
                $rs10 = $stmt->executeQuery();
                //print_r ($rs10);exit;
                $t10 = '<table border=1>';
                //foreach($rs10 as $s10)
                while ($rs10->next()) {
                    $t10.="<tr><td>" . $rs10->getString('unit_name') . "</td><td>" . $rs10->getString('nama_kegiatan') . '</td></tr>';
                }
                //$rs[$i]['units']= $t10.'</table>';
                $units_arr[$i] = $t10 . '</table>';


                $i+=1;
                $j+=1;
            }
            $nilai_draft2+=$nilai_draft;
            $nilai_locked2+=$nilai_locked;
            $persen_draft = $nilai_draft * 100 / $sum_draft;
            //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
            $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
            $persen_locked = $nilai_locked * 100 / $sum_locked;
            //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
            $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");
            //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
            $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
            //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
            $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
            //$rs[$i-$j]['dinas_ok']='t';
            $dinas_ok_arr[$i - $j] = 't';
            //$rs[$i-$j]['rekening_code']=$rekening_code;
            $rekening_code_arr[$i - $j] = $rekening_code;
            //$rs[$i-$j]['rekening_name']=$rekening_name;
            $rekening_name_arr[$i - $j] = $rekening_name;

            $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
            $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

            $this->komponen_name = $komponen_name;
            $this->persen_draft = $persen_draft_arr;
            $this->persen_locked = $persen_locked_arr;
            $this->nilai_draft = $nilai_draft_arr;
            $this->nilai_locked = $nilai_locked_arr;
            $this->dinas_ok = $dinas_ok_arr;
            $this->rekening_code = $rekening_code_arr;
            $this->rekening_name = $rekening_name_arr;
            $this->persen_draft3 = $persen_draft3_arr;
            $this->persen_locked3 = $persen_locked3_arr;
            $this->nilai_draft3 = $nilai_draft3_arr;
            $this->units = $units_arr;
            $this->nilai_locked3 = $nilai_locked3_arr;
        }
    }

    public function executePilihRekening() {
        $query = "select rekening_code || ' ' || rekening_name as rekening_combo,rekening_code from " . sfConfig::get('app_default_schema') . ".rekening order by rekening_code";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rek = array();
        $rek = array();
        while ($rs->next()) {
            $rek[$rs->getString('rekening_code')] = $rs->getString("rekening_combo");
        }
        $this->rek = $rek;
    }

    public function executeReportRekeningKomponen() {
        $query = "select rekening_code as old_rekening_code,rekening_name as old_rekening_name,komponen_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
			FROM " . sfConfig::get('app_default_schema') . ".v_rekening_komponen
			WHERE unit_id<>'9999'
			GROUP BY rekening_code,rekening_name,komponen_name
			ORDER BY rekening_code,rekening_name,komponen_name";
        $add1 = " and substring(k.kode_kegiatan,1,1)='0' ";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3 $add1";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        $rekening_code = '';
        //foreach($rs as $s)
        $nilai_draft2 = 0;
        $nilai_draft = 0;
        $nilai_locked2 = 0;
        $nilai_locked = 0;
        $persen_draft = 0;
        $persen_locked = 0;

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $rekening_code_arr = array();
        $this->rekening_code = array();


        $rekening_code = '';

        $rekening_name_arr = array();
        $this->rekening_name = array();

        $rekening_name = '';
        $persen_draft3 = 0;

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $persen_locked3 = 0;

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $komponen_name_arr = array();
        $this->komponen_name = array();

        while ($rs->next()) {
            $komponen_name_arr[$i] = $rs->getString('komponen_name');
            //if ($rekening_code!=$rs[$i]['old_rekening_code'])
            if ($rekening_code != $rs->getString('old_rekening_code')) {

                $nilai_draft2+=$nilai_draft;
                $nilai_locked2+=$nilai_locked;
                $persen_draft = $nilai_draft * 100 / $sum_draft;
                //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
                $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                $persen_locked = $nilai_locked * 100 / $sum_locked;
                //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
                $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                //$rs[$i-$j]['dinas_ok']='t';
                $dinas_ok_arr[$i - $j] = 't';
                //$rs[$i-$j]['rekening_code']=$rekening_code;
                $rekening_code_arr[$i - $j] = $rekening_code;
                //$rs[$i-$j]['rekening_name']=$rekening_name;
                $rekening_name_arr[$i - $j] = $rekening_name;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            //$rekening_code=$rs[$i]['old_rekening_code'];
            $rekening_code = $rs->getString('old_rekening_code');
            //$rekening_name=$rs[$i]['old_rekening_name'];
            $rekening_name = $rs->getString('old_rekening_name');

            //$nilai_draft+=$rs[$i]['nilai_draft3'];
            $nilai_draft+=$rs->getString('nilai_draft3');
            //$nilai_locked+=$rs[$i]['nilai_locked3'];
            $nilai_locked+=$rs->getString('nilai_locked3');
            //$persen_draft3=$rs[$i]['nilai_draft3'] * 100 / $sum_draft;
            $persen_draft3 = $rs->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($persen_draft3,2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
            //$persen_locked3=$rs[$i]['nilai_locked3'] * 100 / $sum_locked;
            $persen_locked3 = $rs->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($persen_locked3,2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");

            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");

            $i+=1;
            $j+=1;
        }
        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;
        $persen_draft = $nilai_draft * 100 / $sum_draft;
        //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
        $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
        $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

        //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$j]['dinas_ok']='t';
        $dinas_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['rekening_code']=$rekening_code;
        $rekening_code_arr[$i - $j] = $rekening_code;
        //$rs[$i-$j]['rekening_name']=$rekening_name;
        $rekening_name_arr[$i - $j] = $rekening_name;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->rekening_code = $rekening_code_arr;

        $this->rekening_name = $rekening_name_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->komponen_name = $komponen_name_arr;
    }

    public function executeReportProgramKegiatanSubtitle() {
        $query = "select s.subtitle,s.indikator,s.nilai,s.satuan,k.kode_kegiatan,k.unit_id,
		(select nilai_draft from " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan r where r.unit_id=k.unit_id and r.kode_kegiatan=k.kode_kegiatan) as total_nilai,(select sum(r2.komponen_harga_awal*r2.volume*(r2.pajak+100)/100) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail r2 where r2.unit_id=k.unit_id and r2.kegiatan_code=k.kode_kegiatan and r2.subtitle=s.subtitle) as nilai_sub,k.nama_kegiatan, s.nilai,u.unit_name,u.unit_id,p.nama_program,p.kode_program
        from " . sfConfig::get('app_default_schema') . ".subtitle_indikator s," . sfConfig::get('app_default_schema') . ".master_kegiatan k, unit_kerja u," . sfConfig::get('app_default_schema') . ".master_program p
		where s.unit_id<>'9999'and s.unit_id=k.unit_id and s.kegiatan_code=k.kode_kegiatan and u.unit_id=k.unit_id and p.kode_program=k.kode_program order by k.kode_program,k.unit_id,k.kode_kegiatan,s.subtitle";

        //$result = @pg_query($conn_id,$query);
        //$rs=pg_fetch_all($result);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;
        //$countarr=count($rs);
        //for($i=0;$i<$countarr;$i++)
        $kode_program = '';
        $lompat = 0;
        $unit_id = '';
        $kode_kegiatan = '';
        $this->program_ok = array();
        $i = 0;
        $this->total_program = array();
        $total_program = 0;
        $this->kegiatan_ok = array();
        $total_alokasi_dana = 0;
        $this->total_nilai = array();
        $this->nilai_sub = array();
        $this->unit_name = array();
        $this->total_alokasi_dana = 0;
        while ($rs->next()) {

            //if($rs[$i]['kode_program']<>$rs[$i-1]['kode_program']) 
            if ($rs->getString('kode_program') <> $kode_program) {
                $kode_program = $rs->getString('kode_program');

                //$rs[$i]['program_ok']=1;
                $this->program_ok[$i] = 1;
                //$rs[$i-$lompat+1]['total_program']=number_format($total_program,0,',','.');
                $this->total_program[$i - $lompat + 1] = number_format($total_program, 0, ',', '.');
                //$rs[$i-$lompat-1]['total_program']=number_format($total_program,0,',','.');
                $this->total_program[$i - $lompat - 1] = number_format($total_program, 0, ',', '.');
                //$rs[$i-$lompat]['total_program']=number_format($total_program,0,',','.');
                $this->total_program[$i - $lompat] = number_format($total_program, 0, ',', '.');
                $lompat = 0;
                $total_program = 0;
            } else {
                $lompat++;
            }
            //if($rs[$i]['unit_id']==$rs[$i-1]['unit_id']) $rs[$i]['unit_name']="";
            if ($rs->getString('unit_id') == $unit_id) {
                $unit_id = $rs->getString('unit_id');
                //$rs[$i]['unit_name']="";
                $this->unit_name[$i] = "";
            }
            //if(($rs[$i]['kode_kegiatan']<>$rs[$i-1]['kode_kegiatan'] and $rs[$i]['unit_id']==$rs[$i-1]['unit_id']) or $rs[$i]['unit_id']<>$rs[$i-1]['unit_id']) 
            if (($rs->getString('kode_kegiatan') <> $kode_kegiatan and $rs->getString('unit_id') == $unit_id) or $rs->getString('unit_id') <> $unit_id) {
                $kode_kegiatan = $rs->getString('kode_kegiatan');
                $unit_id = $rs->getString('unit_id');
                //$rs[$i]['kegiatan_ok']=1;
                $this->kegiatan_ok[$i] = 1;
                //$total_alokasi_dana+=$rs[$i]['total_nilai'];
                $total_alokasi_dana+=$rs->getString('total_nilai');
                //$total_program+=$rs[$i]['total_nilai'];
                $total_program+=$rs->getString('total_nilai');
            }
            //$rs[$i]['total_nilai']=number_format($rs[$i]['total_nilai'],0,',','.');
            $this->total_nilai[$i] = number_format($rs->getString('total_nilai'), 0, ',', '.');
            //$rs[$i]['nilai_sub']=number_format($rs[$i]['nilai_sub'],0,',','.');
            $this->nilai_sub[$i] = number_format($rs->getString('nilai_sub'), 0, ',', '.');
        }
        $this->total_alokasi_dana = number_format($total_alokasi_dana, 0, ',', '.');
        $i++;
    }

    public function executeReportDinasKegiatanSubtitle() {
        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan as old_kode_kegiatan, nama_kegiatan as old_nama_kegiatan,subtitle ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft4,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked4,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft4, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked4 
		FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
		WHERE unit_id<>'9999'
		GROUP BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,subtitle
		ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,subtitle";

        $query2 = "Select unit_id,kode_kegiatan from " . sfConfig::get('app_default_schema') . ".master_kegiatan order by unit_id,kode_kegiatan limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $kode_kegiatan = $rs->getString('kode_kegiatan');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        $i = 0;
        $j = 0;
        $k = 0;
        //foreach($rs as $s)
        $nilai_draft3 = 0;
        $nilai_locked3 = 0;
        $nilai_draft = 0;
        $nilai_locked = 0;

        $subtitle_arr = array();
        $this->subtitle = array();

        $unit_id_arr = array();
        $this->unit_id = array();

        $unit_name_arr = array();
        $this->unit_name = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $kegiatan_ok_arr = array();
        $this->kegiatan_ok = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $kode_kegiatan_arr = array();
        $this->kode_kegiatan = array();

        $nama_kegiatan_arr = array();
        $this->nama_kegiatan = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $nilai_draft4_arr = array();
        $this->nilai_draft4 = array();

        $nilai_locked4_arr = array();
        $this->nilai_locked4 = array();
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $unit_name = '';
        $nama_kegiatan = '';
        while ($rs->next()) {
            $subtitle_arr[$i] = $rs->getString('subtitle');
            if ($kode_kegiatan != $rs->getString('old_kode_kegiatan')) {
                $udah = 'f';
                if ($unit_id != $rs->getString('old_unit_id')) {
                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;
                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;

                    //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
                    $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
                    $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
                    //$rs[$i-$k]['dinas_ok']='t';
                    $dinas_ok_arr[$i - $k] = 't';
                    //$rs[$i-$k]['unit_id']=$unit_id;
                    $unit_id_arr[$i - $k] = $unit_id;
                    //$rs[$i-$k]['unit_name']=$unit_name;
                    $unit_name_arr[$i - $k] = $unit_name;
                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                    $udah = 't';
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['kode_kegiatan']=$kode_kegiatan;
                    $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
                    //$rs[$i-$j]['nama_kegiatan']=$nama_kegiatan;
                    $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                    $j = 0;
                }
                $unit_id = $rs->getString('old_unit_id');
                $unit_name = $rs->getString('old_unit_name');
                //$rs[$i]['unit_id']=$unit_id;
                $unit_id_arr[$i] = $unit_id;

                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                if ($udah != 't') {
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['kode_kegiatan']=$kode_kegiatan;
                    $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
                    //$rs[$i-$j]['nama_kegiatan']=$nama_kegiatan;
                    $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
            }
            //$kode_kegiatan=$rs[$i]['old_kode_kegiatan'];
            $kode_kegiatan = $rs->getString('old_kode_kegiatan');
            //$nama_kegiatan=$rs[$i]['old_nama_kegiatan'];
            $nama_kegiatan = $rs->getString('old_nama_kegiatan');

            //$nilai_draft3+=$rs[$i]['nilai_draft4'];
            $nilai_draft3+=$rs->getString('nilai_draft4');
            //$nilai_locked3+=$rs[$i]['nilai_locked4'];
            $nilai_locked3+=$rs->getString('nilai_locked4');

            //$rs[$i]['nilai_draft4']=number_format($rs[$i]['nilai_draft4'],0,",",".");
            $nilai_draft4_arr[$i] = number_format($rs->getString('nilai_draft4'), 0, ",", ".");
            //$rs[$i]['nilai_locked4']=number_format($rs[$i]['nilai_locked4'],0,",",".");
            $nilai_locked4_arr[$i] = number_format($rs->getString('nilai_locked4'), 0, ",", ".");
            $i+=1;
            $j+=1;
            $k+=1;
        }

        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
        $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
        $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
        //$rs[$i-$j]['kegiatan_ok']='t';
        $kegiatan_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['kode_kegiatan']=$kode_kegiatan;
        $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
        //$rs[$i-$j]['nama_kegiatan']=$nama_kegiatan;
        $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$k]['dinas_ok']='t';
        $dinas_ok_arr[$i - $k] = 't';
        //$rs[$i-$k]['unit_id']=$unit_id;
        $unit_id_arr[$i - $k] = $unit_id;
        //$rs[$i-$k]['unit_name']=$unit_name;
        $unit_name_arr[$i - $k] = $unit_name;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->unit_id = $unit_id_arr;

        $this->unit_name = $unit_name_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->kegiatan_ok = $kegiatan_ok_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->kode_kegiatan = $kode_kegiatan_arr;

        $this->nama_kegiatan = $nama_kegiatan_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->nilai_draft4 = $nilai_draft4_arr;

        $this->nilai_locked4 = $nilai_locked4_arr;

        $this->subtitle = $subtitle_arr;
    }

    public function executeReportProgramKegiatan() {
        $query = "select kode_program as old_kode_program,nama_program as old_nama_program, unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan,nama_kegiatan,((jumlah_draft*jumlah_non_lanjutan)) as total_draft4,((jumlah_setuju*jumlah_non_lanjutan)) as total_locked4,((nilai_draft*jumlah_non_lanjutan)) as nilai_draft4, ((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked4
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4
			WHERE unit_id<>'9999'
			ORDER BY kode_program,nama_program ,unit_id,unit_name,kode_kegiatan,nama_kegiatan ";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }

        $query2 = "Select kode_program,nama_program from " . sfConfig::get('app_default_schema') . ".master_program order by kode_program limit 1";
        //$result = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        while ($rs->next()) {
            $kode_program = $rs->getString('kode_program');
            $nama_program = $rs->getString('nama_program');
        }

        $query2 = "SELECT unit_id ,unit_name FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan WHERE kode_program='$kode_program' order by 	kode_program,unit_id LIMIT 1";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $unit_name = $rs->getString('unit_name');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);
        $this->rs = $rs;

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        $k = 0;


        //foreach($rs as $s)
        $nilai_draft3 = 0;
        $nilai_locked3 = 0;
        $nilai_draft = 0;
        $nilai_locked = 0;

        $kode_program_arr = array();
        $this->kode_program = array();

        $nama_program_arr = array();
        $this->nama_program = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $unit_id_arr = array();
        $this->unit_id = array();

        $unit_name_arr = array();
        $this->unit_name = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft4_arr = array();
        $this->nilai_draft4 = array();

        $persen_draft4_arr = array();
        $this->persen_draft4 = array();

        $nilai_locked4 = array();
        $this->nilai_locked4 = array();

        $persen_locked4_arr = array();
        $this->persen_locked4 = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $kegiatan_ok_arr = array();
        $this->kegiatan_ok = array();
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $persen_draft3 = 0;
        while ($rs->next()) {
            if ($unit_id != $rs->getString('old_unit_id')) {
                $udah = 'f';
                if ($kode_program != $rs->getString('old_kode_program')) {
                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;

                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;

                    $persen_draft = $nilai_draft * 100 / $sum_draft;
                    //$rs[$i-$k]['persen_draft']=number_format($persen_draft,2,",",".");
                    $persen_draft_arr[$i - $k] = number_format($persen_draft, 2, ",", ".");
                    $persen_locked = $nilai_locked * 100 / $sum_locked;
                    //$rs[$i-$k]['persen_locked']=number_format($persen_locked,2,",",".");
                    $persen_locked_arr[$i - $k] = number_format($persen_locked, 2, ",", ".");
                    //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
                    $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
                    $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
                    //$rs[$i-$k]['dinas_ok']='t';
                    $dinas_ok_arr[$i - $k] = 't';
                    //$rs[$i-$k]['kode_program']=$kode_program;
                    $kode_program_arr[$i - $k] = $kode_program;
                    //$rs[$i-$k]['nama_program']=$nama_program;
                    $nama_program_arr[$i - $k] = $nama_program;
                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                    $udah = 't';
                    $persen_draft3 = $nilai_draft3 * 100 / $sum_draft;
                    $persen_draft3_arr[$i - $j] = number_format($persen_draft3, 2, ",", ".");
                    $persen_locked3 = $nilai_locked3 * 100 / $sum_locked;
                    $persen_locked3_arr[$i - $j] = number_format($persen_locked3, 2, ",", ".");
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['unit_id']=$unit_id;
                    $unit_id_arr[$i - $j] = $unit_id;
                    //$rs[$i-$j]['unit_name']=$unit_name;
                    $unit_name_arr[$i - $j] = $unit_name;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
                //$kode_program=$rs[$i]['old_kode_program'];
                $kode_program = $rs->getString('old_kode_program');
                //$nama_program=$rs[$i]['old_nama_program'];
                $nama_program = $rs->getString('old_nama_program');
                //$rs[$i]['kode_program']=$kode_program;
                $kode_program_arr[$i] = $kode_program;
                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                if ($udah != 't') {
                    $persen_draft3 = $nilai_draft3 * 100 / $sum_draft;
                    //$rs[$i-$j]['persen_draft3']=number_format($persen_draft3,2,",",".");
                    $persen_draft3_arr[$i - $j] = number_format($persen_draft3, 2, ",", ".");
                    $persen_locked3 = $nilai_locked3 * 100 / $sum_locked;
                    //$rs[$i-$j]['persen_locked3']=number_format($persen_locked3,2,",",".");
                    $persen_locked3_arr[$i - $j] = number_format($persen_locked3, 2, ",", ".");
                    //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    //$rs[$i-$j]['kegiatan_ok']='t';
                    $kegiatan_ok_arr[$i - $j] = 't';
                    //$rs[$i-$j]['unit_id']=$unit_id;
                    $unit_id_arr[$i - $j] = $unit_id;
                    //$rs[$i-$j]['unit_name']=$unit_name;
                    $unit_name_arr[$i - $j] = $unit_name;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
            }
            $unit_id = $rs->getString('old_unit_id');
            $unit_name = $rs->getString('old_unit_name');

            $nilai_draft3+=$rs->getString('nilai_draft4');
            $nilai_locked3+=$rs->getString('nilai_locked4');
            $persen_draft4 = $rs->getString('nilai_draft4') * 100 / $sum_draft;
            //$rs[$i]['persen_draft4']=number_format($persen_draft4,2,",",".");
            $persen_draft4_arr[$i] = number_format($persen_draft4, 2, ",", ".");
            $persen_locked4 = $rs->getString('nilai_locked4') * 100 / $sum_locked;
            //$rs[$i]['persen_locked4']=number_format($persen_locked4,2,",",".");
            $persen_locked4_arr[$i] = number_format($persen_locked4, 2, ",", ".");
            //$rs[$i]['nilai_draft4']=number_format($rs[$i]['nilai_draft4'],0,",",".");
            $nilai_draft4_arr[$i] = number_format($rs->getString('nilai_draft4'), 0, ",", ".");
            //$rs[$i]['nilai_locked4']=number_format($rs[$i]['nilai_locked4'],0,",",".");
            $nilai_locked4_arr[$i] = number_format($rs->getString('nilai_locked4'), 0, ",", ".");
            $i+=1;
            $j+=1;
            $k+=1;
        }

        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        $persen_draft3 = $nilai_draft3 * 100 / $sum_draft;
        //$rs[$i-$j]['persen_draft3']=number_format($persen_draft3,2,",",".");
        $persen_draft3_arr[$i - $j] = number_format($persen_draft3, 2, ",", ".");
        $persen_locked3 = $nilai_locked3 * 100 / $sum_locked;
        //$rs[$i-$j]['persen_locked3']=number_format($persen_locked3,2,",",".");
        $persen_locked3_arr[$i - $j] = number_format($persen_locked3, 2, ",", ".");
        //$rs[$i-$j]['nilai_draft3']=number_format($nilai_draft3,0,",",".");
        $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked3']=number_format($nilai_locked3,0,",",".");
        $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
        //$rs[$i-$j]['kegiatan_ok']='t';
        $kegiatan_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['unit_id']=$unit_id;
        $unit_id_arr[$i - $j] = $unit_id;
        //$rs[$i-$j]['unit_name']=$unit_name;
        $unit_name_arr[$i - $j] = $unit_name;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        $persen_draft = $nilai_draft * 100 / $sum_draft;
        //$rs[$i-$k]['persen_draft']=number_format($persen_draft,2,",",".");
        $persen_draft_arr[$i - $k] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        //$rs[$i-$k]['persen_locked']=number_format($persen_locked,2,",",".");
        $persen_locked_arr[$i - $k] = number_format($persen_locked, 2, ",", ".");
        //$rs[$i-$k]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$k]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$k]['dinas_ok']='t';
        $dinas_ok_arr[$i - $k] = 't';
        //$rs[$i-$k]['kode_program']=$kode_program;
        $kode_program_arr[$i - $k] = $kode_program;
        //$rs[$i-$k]['nama_program']=$nama_program;
        $nama_program_arr[$i - $k] = $nama_program;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");


        $this->kode_program = $kode_program_arr;

        $this->nama_program = $nama_program_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->persen_draft = $persen_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->unit_id = $unit_id_arr;

        $this->unit_name = $unit_name_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft4 = $nilai_draft4_arr;

        $this->persen_draft4 = $persen_draft4_arr;

        $this->nilai_locked4 = $nilai_locked4_arr;

        $this->persen_locked4 = $persen_locked4_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->kegiatan_ok = $kegiatan_ok_arr;
    }

    public function executeReportProgram() {
        $query = "select kode_bidang, nama_bidang ,sum((jumlah_draft)) as total_draft3,sum((jumlah_setuju)) as total_locked3, sum((nilai_draft)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju)) as nilai_locked3
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian_semula_menjadi_dewan 
			WHERE unit_id<>'9999'
			GROUP BY kode_bidang, nama_bidang
			ORDER BY kode_bidang, nama_bidang";

        $query2 = "select sum(nilai_anggaran) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".dewan_rincian r," . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan k," . sfConfig::get('app_default_schema') . ".dewan_rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.status_hapus=FALSE and
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
//$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
//while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(nilai_anggaran) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".dewan_rincian r," . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan k," . sfConfig::get('app_default_schema') . ".dewan_rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.status_hapus=FALSE and
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
//$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
//while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }

//$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
//$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;

//foreach($rs as $s)
        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $total_draft2 = 0;
        $total_locked2 = 0;
        while ($rs->next()) {
//$rs[$i]['persen_draft3']=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
            $persen_draft3_arr[$i] = $rs->getString('nilai_draft3') * 100 / $sum_draft;
//$rs[$i]['persen_draft3']=number_format($rs[$i]['persen_draft3'],2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3_arr[$i], 2, ",", ".");
//$rs[$i]['persen_locked3']=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
            $persen_locked3_arr[$i] = $rs->getString('nilai_locked3') * 100 / $sum_locked;
//$rs[$i]['persen_locked3']=number_format($rs[$i]['persen_locked3'],2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3_arr[$i], 2, ",", ".");

//$nilai_draft2+=$rs[$i]['nilai_draft3'];
            $nilai_draft2+=$rs->getString('nilai_draft3');
//$total_draft2+=$rs[$i]['total_draft3'];
            $total_draft2+=$rs->getString('total_draft3');
//$nilai_locked2+=$rs[$i]['nilai_locked3'];
            $nilai_locked2+=$rs->getString('nilai_locked3');
//$total_locked2+=$rs[$i]['total_locked3'];
            $total_locked2+=$rs->getString('total_locked3');
//$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
//$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");
            $i+=1;
        }

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_locked3 = $persen_locked3_arr;
    }

//    public function executeReportProgram() {
//        $query = "select kode_program, nama_program ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3, sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3
//			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4
//			WHERE unit_id<>'9999'
//			GROUP BY kode_program, nama_program
//			ORDER BY kode_program, nama_program";
//
//        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
//	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
//		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
//		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
//        //$result2 = @pg_query($conn_id,$query2);
//        $con = Propel::getConnection();
//        $stmt = $con->prepareStatement($query2);
//        $rs = $stmt->executeQuery();
//        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
//        while ($rs->next()) {
//            $sum_draft = $rs->getString('sum_draft');
//        }
//
//        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
//	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
//		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
//		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
//        //$result2 = @pg_query($conn_id,$query2);
//        $stmt = $con->prepareStatement($query2);
//        $rs = $stmt->executeQuery();
//        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
//        while ($rs->next()) {
//            $sum_locked = $rs->getString('sum_locked');
//        }
//
//        //$result = @pg_query($conn_id,$query);
//        $stmt = $con->prepareStatement($query);
//        //$rs=pg_fetch_all($result);
//        $rs = $stmt->executeQuery();
//        $this->rs = $rs;
//
//        if ($sum_locked == 0 or !$sum_locked)
//            $sum_locked = 99999999999;
//        if ($sum_draft == 0 or !$sum_draft)
//            $sum_draft = 99999999999;
//        $i = 0;
//
//        //foreach($rs as $s)
//        $nilai_draft3_arr = array();
//        $this->nilai_draft3 = array();
//
//        $persen_draft3_arr = array();
//        $this->persen_draft3 = array();
//
//        $nilai_locked3_arr = array();
//        $this->nilai_locked3 = array();
//
//        $persen_locked3_arr = array();
//        $this->persen_locked3 = array();
//        $nilai_draft2 = 0;
//        $nilai_locked2 = 0;
//        $total_draft2 = 0;
//        $total_locked2 = 0;
//        while ($rs->next()) {
//            //$rs[$i]['persen_draft3']=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
//            $persen_draft3_arr[$i] = $rs->getString('nilai_draft3') * 100 / $sum_draft;
//            //$rs[$i]['persen_draft3']=number_format($rs[$i]['persen_draft3'],2,",",".");
//            $persen_draft3_arr[$i] = number_format($persen_draft3_arr[$i], 2, ",", ".");
//            //$rs[$i]['persen_locked3']=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
//            $persen_locked3_arr[$i] = $rs->getString('nilai_locked3') * 100 / $sum_locked;
//            //$rs[$i]['persen_locked3']=number_format($rs[$i]['persen_locked3'],2,",",".");
//            $persen_locked3_arr[$i] = number_format($persen_locked3_arr[$i], 2, ",", ".");
//
//            //$nilai_draft2+=$rs[$i]['nilai_draft3'];
//            $nilai_draft2+=$rs->getString('nilai_draft3');
//            //$total_draft2+=$rs[$i]['total_draft3'];
//            $total_draft2+=$rs->getString('total_draft3');
//            //$nilai_locked2+=$rs[$i]['nilai_locked3'];
//            $nilai_locked2+=$rs->getString('nilai_locked3');
//            //$total_locked2+=$rs[$i]['total_locked3'];
//            $total_locked2+=$rs->getString('total_locked3');
//            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
//            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
//            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
//            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");
//            $i+=1;
//        }
//
//        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
//        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");
//
//        $this->nilai_draft3 = $nilai_draft3_arr;
//
//        $this->persen_draft3 = $persen_draft3_arr;
//
//        $this->nilai_locked3 = $nilai_locked3_arr;
//
//        $this->persen_locked3 = $persen_locked3_arr;
//    }

    public function executeReportProgramP13() {
        $query = "select kode_bidang, nama_bidang ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3, sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4
			WHERE unit_id<>'9999'
			GROUP BY kode_bidang, nama_bidang
			ORDER BY kode_bidang, nama_bidang";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
		from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_draft = $rs->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $sum_locked = $rs->getString('sum_locked');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        //$rs=pg_fetch_all($result);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;

        //foreach($rs as $s)
        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();
        $nilai_draft2 = 0;
        $total_draft2 = 0;
        $nilai_locked2 = 0;
        $total_locked2 = 0;
        while ($rs->next()) {
            //$rs[$i]['persen_draft3']=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
            $persen_draft3_arr[$i] = $rs->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($rs[$i]['persen_draft3'],2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3_arr[$i], 2, ",", ".");
            //$rs[$i]['persen_locked3']=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
            $persen_locked3_arr[$i] = $rs->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($rs[$i]['persen_locked3'],2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3_arr[$i], 2, ",", ".");

            //$nilai_draft2+=$rs[$i]['nilai_draft3'];
            $nilai_draft2+=$rs->getString('nilai_draft3');
            //$total_draft2+=$rs[$i]['total_draft3'];
            $total_draft2+=$rs->getString('total_draft3');
            //$nilai_locked2+=$rs[$i]['nilai_locked3'];
            $nilai_locked2+=$rs->getString('nilai_locked3');
            //$total_locked2+=$rs[$i]['total_locked3'];
            $total_locked2+=$rs->getString('total_locked3');
            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs->getString('nilai_locked3'), 0, ",", ".");
            $i+=1;
        }

        //$nilai_draft2=number_format($nilai_draft2,0,",",".");
        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_locked3 = $persen_locked3_arr;
    }

    public function executeReportDinasKegiatanBelanja($dinas) {
        $bek = '';
        if ($dinas != 'semua')
            $bek = " and unit_id=" . $dinas;

        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan as old_kode_kegiatan, nama_kegiatan as old_nama_kegiatan,belanja_name,sum ((jumlah_draft)) as total_draft4,sum((jumlah_setuju)) as total_locked4,
 		sum((nilai_draft)) as nilai_draft4, sum((nilai_draft*jumlah_setuju)) as nilai_locked4 
		FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
		WHERE unit_id<>'9999' $bek
		GROUP BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,belanja_name
		ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,belanja_name";

        $query2 = "Select unit_id,kode_kegiatan from " . sfConfig::get('app_default_schema') . ".master_kegiatan order by unit_id,kode_kegiatan limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        $unit_name = '';
        $nama_kegiatan = '';
        $nilai_draft = 0;
        $this->nilai_draft3 = 0;
        $nilai_locked = 0;
        $this->nilai_locked3 = 0;
        $this->nilai_draft2 = 0;
        $this->nilai_locked2 = 0;
        $this->nilai_draft = array();
        $this->nilai_locked = array();
        $this->dinas_ok = array();
        $this->unit_id = array();
        $this->unit_name = array();
        $this->nilai_draft3 = array();
        $this->nilai_locked3 = array();
        $this->kegiatan_ok = array();
        $this->kode_kegiatan = array();
        $this->nama_kegiatan = array();
        $this->nilai_draft4 = array();
        $this->nilai_locked4 = array();
        $nilai_draft3 = 0;
        $nilai_locked3 = 0;
        $nilai_draft2 = 0;
        $nilai_locked2 = 0;
        $this->belanja_name = array();
        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $kode_kegiatan = $rs->getString('kode_kegiatan');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs2 = $stmt->executeQuery();
        $this->rs = $rs2;

        //if (pg_num_rows($result)>0)
        //if($rs2->next())
        //{
        $i = 0;
        $j = 0;
        $k = 0;
        //foreach($rs as $s)
        while ($rs2->next()) {
            $this->belanja_name[$i] = $rs2->getString('belanja_name');
            if ($kode_kegiatan != $rs2->getString('old_kode_kegiatan')) {
                $udah = 'f';
                if ($unit_id != $rs2->getString('old_unit_id')) {

                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;
                    $this->nilai_draft2+=$nilai_draft;
                    $this->nilai_locked2+=$nilai_locked;

                    $this->nilai_draft[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    $this->nilai_locked[$i - $k] = number_format($nilai_locked, 0, ",", ".");
                    $this->dinas_ok[$i - $k] = 't';
                    $this->unit_id[$i - $k] = $unit_id;
                    $this->unit_name[$i - $k] = $unit_name;
                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;
                    $udah = 't';
                    $this->nilai_draft3[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    $this->nilai_locked3[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    $this->kegiatan_ok[$i - $j] = 't';
                    $this->kode_kegiatan[$i - $j] = $kode_kegiatan;
                    $this->nama_kegiatan[$i - $j] = $nama_kegiatan;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                    $j = 0;
                }
                $unit_id = $rs2->getString('old_unit_id');
                $unit_name = $rs2->getString('old_unit_name');
                $this->unit_id[$i] = $unit_id;

                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                if ($udah != 't') {

                    $this->nilai_draft3[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    $this->nilai_locked3[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    $this->kegiatan_ok[$i - $j] = 't';
                    $this->kode_kegiatan[$i - $j] = $kode_kegiatan;
                    $this->nama_kegiatan[$i - $j] = $nama_kegiatan;
                    $j = 0;
                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;
                }
            }
            $kode_kegiatan = $rs2->getString('old_kode_kegiatan');
            $nama_kegiatan = $rs2->getString('old_nama_kegiatan');

            $nilai_draft3+=$rs2->getString('nilai_draft4');
            $nilai_locked3+=$rs2->getString('nilai_locked4');

            $this->nilai_draft4[$i] = number_format($rs2->getString('nilai_draft4'), 0, ",", ".");
            $this->nilai_locked4[$i] = number_format($rs2->getString('nilai_locked4'), 0, ",", ".");
            $i+=1;
            $j+=1;
            $k+=1;
        }

        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        $this->nilai_draft3[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        $this->nilai_locked3[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
        $this->kegiatan_ok[$i - $j] = 't';
        $this->kode_kegiatan[$i - $j] = $kode_kegiatan;
        $this->nama_kegiatan[$i - $j] = $nama_kegiatan;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        $this->nilai_draft[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        $this->nilai_locked[$i - $k] = number_format($nilai_locked, 0, ",", ".");
        $this->dinas_ok[$i - $k] = 't';
        $this->unit_id[$i - $k] = $unit_id;
        $this->unit_name[$i - $k] = $unit_name;

        $this->nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");
        //}
    }

    public function executePilihDinas() {
        $query = "select unit_name,unit_id from unit_kerja order by unit_name";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->dinas = array();
        $this->dinas['semua'] = '***Tampilkan Semua***';
        while ($rs->next()) {
            $this->dinas[$rs->getString('unit_id')] = $rs->getString('unit_name');
        }
        //$this->dinas=UnitKerjaPeer::doSelect(new Criteria());
    }

    public function executeReportProgramDinas() {
        $query = "select kode_program as old_kode_program,nama_program as old_nama_program, unit_id, unit_name,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 		sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian4 WHERE unit_id<>'9999' GROUP BY kode_program,nama_program ,unit_id,unit_name	ORDER BY kode_program,nama_program ,unit_id,unit_name ";

        $query2 = "Select kode_program,nama_program from " . sfConfig::get('app_default_schema') . ".master_program order by kode_program limit 1";
        //$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows = @pg_fetch_assoc($result))extract($rows,EXTR_OVERWRITE);
        while ($rs->next()) {
            $kode_program = $rs->getString('kode_program');
            $nama_program = $rs->getString('nama_program');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs2 = $stmt->executeQuery();
        while ($rs2->next()) {
            $sum_draft = $rs2->getString('sum_draft');
        }

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
		where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
		d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs3 = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs3->next()) {
            $sum_locked = $rs3->getString('sum_locked');
        }

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs4 = $stmt->executeQuery();
        $this->rs = $rs4;
        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        //foreach($rs as $s)
        $nilai_draft = $nilai_locked = $this->nilai_draft2 = $this->nilai_locked2 = 0;

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $kode_program_arr = array();
        $this->kode_program = array();

        $nama_program_arr = array();
        $this->nama_program = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();
        $unit_id = '';
        while ($rs4->next()) {
            if ($kode_program != $rs4->getString('old_kode_program')) {

                $this->nilai_draft2+=$nilai_draft;
                $this->nilai_locked2+=$nilai_locked;
                $persen_draft = $nilai_draft * 100 / $sum_draft;
                $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                $persen_locked = $nilai_locked * 100 / $sum_locked;
                $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                $dinas_ok_arr[$i - $j] = 't';
                $kode_program_arr[$i - $j] = $kode_program;
                $nama_program_arr[$i - $j] = $nama_program;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            $kode_program = $rs4->getString('old_kode_program');
            $nama_program = $rs4->getString('old_nama_program');
            $kode_program_arr[$i] = $unit_id;
            $nilai_draft+=$rs4->getString('nilai_draft3');
            $nilai_locked+=$rs4->getString('nilai_locked3');

            $persen_draft3 = $rs4->getString('nilai_draft3') * 100 / $sum_draft;
            $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
            $persen_locked3 = $rs4->getString('nilai_locked3') * 100 / $sum_locked;
            $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");

            $nilai_draft3_arr[$i] = number_format($rs4->getString('nilai_draft3'), 0, ",", ".");
            $nilai_locked3_arr[$i] = number_format($rs4->getString('nilai_locked3'), 0, ",", ".");

            $i+=1;
            $j+=1;
        }
        $this->nilai_draft2+=$nilai_draft;
        $this->nilai_locked2+=$nilai_locked;
        $persen_draft = $nilai_draft * 100 / $sum_draft;
        $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        $dinas_ok_arr[$i - $j] = 't';
        $kode_program_arr[$i - $j] = $kode_program;
        $nama_program_arr[$i - $j] = $nama_program;

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->kode_program = $kode_program_arr;

        $this->nama_program = $nama_program_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;
    }

    public function executeReportBelanjaRekening() {
        $this->setFlash('RBK', 'selected');
        //$query="select belanja_id as old_belanja_id, belanja_name as old_belanja_name,rekening_code,rekening_name,sum ((jumlah_draft)) as total_draft3,sum((jumlah_setuju)) as total_locked3,sum((nilai_draft)) as nilai_draft3, sum((nilai_draft*jumlah_setuju)) as nilai_locked3 FROM ". sfConfig::get('app_default_schema') .".v_kegiatan_rekening WHERE unit_id<>'9999' GROUP BY belanja_id,belanja_name,rekening_code,rekening_name ORDER BY belanja_id,belanja_name,rekening_code,rekening_name";

        $query = "select belanja_id as old_belanja_id, belanja_name as old_belanja_name,rekening_code,rekening_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,
 					sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
			FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
			WHERE unit_id<>'9999'
			GROUP BY belanja_id,belanja_name,rekening_code,rekening_name
			ORDER BY belanja_id,belanja_name,rekening_code,rekening_name";

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d	where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
	d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
        //$result2 = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs->next())
            $sum_draft = $rs->getString('sum_draft');

        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
        //$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
        $rs2 = $stmt->executeQuery();
        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        while ($rs2->next())
            $sum_locked = $rs2->getString('sum_locked');

        //$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs3 = $stmt->executeQuery();
        $this->rs = $rs3;

        //if(!$result) echo @pg_last_error($conn_id);
        //$rs=pg_fetch_all($result);

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;
        $i = 0;
        $j = 0;
        //foreach($rs as $s)
        $belanja_id = '';
        $nilai_draft = 0;
        $nilai_locked = 0;
        $belanja_name = '';
        //$this->nilai_draft2=0;
        //$this->nilai_locked2=0;
        $unit_id = '';
        //$this->dinas_ok='';

        $persen_draft_arr = array();
        $this->persen_draft = array();

        //$persen_locked=0;
        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $belanja_id_arr = array();
        $this->belanja_id = array();

        $belanja_name_arr = array();
        $this->belanja_name = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $persen_draft3_arr = array();
        $this->persen_draft3 = array();

        $persen_locked3_arr = array();
        $this->persen_locked3 = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $rekening_code_arr = array();
        $this->rekening_code = array();

        $rekening_name_arr = array();
        $this->rekening_name = array();

        //print_r($query);exit;
        while ($rs3->next()) {

            $rekening_code_arr[$i] = $rs3->getString('rekening_code');
            $rekening_name_arr[$i] = $rs3->getString('rekening_name');

            if ($belanja_id != $rs3->getString('old_belanja_id')) {
                $this->nilai_draft2+=$nilai_draft;
                $this->nilai_locked2+=$nilai_locked;
                $persen_draft = $nilai_draft * 100 / $sum_draft;
                //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
                $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
                $persen_locked = $nilai_locked * 100 / $sum_locked;
                //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
                $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

                //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                //$rs[$i-$j]['dinas_ok']='t';
                $dinas_ok_arr[$i - $j] = 't';
                //$rs[$i-$j]['belanja_id']=$belanja_id;
                $belanja_id_arr[$i - $j] = $belanja_id;
                //$rs[$i-$j]['belanja_name']=$belanja_name;
                $belanja_name_arr[$i - $j] = $belanja_name;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            //$belanja_id=$rs[$i]['old_belanja_id'];
            $belanja_id = $rs3->getString('old_belanja_id');

            //$belanja_name=$rs[$i]['old_belanja_name'];
            $belanja_name = $rs3->getString('old_belanja_name');
            //$rs[$i]['belanja_id']=$unit_id;
            $belanja_id_arr[$i] = $unit_id;
            //$nilai_draft+=$rs[$i]['nilai_draft3'];
            $nilai_draft = $nilai_draft + $rs3->getString('nilai_draft3');
            //$nilai_locked+=$rs[$i]['nilai_locked3'];
            $nilai_locked+=$rs3->getString('nilai_locked3');
            //$persen_draft3=$rs[$i]['nilai_draft3'] * 100 / $sum_draft; 
            $persen_draft3 = $rs3->getString('nilai_draft3') * 100 / $sum_draft;
            //$rs[$i]['persen_draft3']=number_format($persen_draft3,2,",",".");
            $persen_draft3_arr[$i] = number_format($persen_draft3, 2, ",", ".");
            //$persen_locked3=$rs[$i]['nilai_locked3'] * 100 / $sum_locked; 
            $persen_locked3 = $rs3->getString('nilai_locked3') * 100 / $sum_locked;
            //$rs[$i]['persen_locked3']=number_format($persen_locked3,2,",",".");
            $persen_locked3_arr[$i] = number_format($persen_locked3, 2, ",", ".");

            //$rs[$i]['nilai_draft3']=number_format($rs[$i]['nilai_draft3'],0,",",".");
            $nilai_draft3_arr[$i] = number_format($rs3->getString('nilai_draft3'), 0, ",", ".");
            //$rs[$i]['nilai_locked3']=number_format($rs[$i]['nilai_locked3'],0,",",".");
            $nilai_locked3_arr[$i] = number_format($rs3->getString('nilai_locked3'), 0, ",", ".");
            //print_r($belanja_id);exit;
            $i+=1;
            $j+=1;
        }

        $this->nilai_draft2+=$nilai_draft;
        $this->nilai_locked2+=$nilai_locked;
        $persen_draft = $nilai_draft * 100 / $sum_draft;
        //$rs[$i-$j]['persen_draft']=number_format($persen_draft,2,",",".");
        $persen_draft_arr[$i - $j] = number_format($persen_draft, 2, ",", ".");
        $persen_locked = $nilai_locked * 100 / $sum_locked;
        //$rs[$i-$j]['persen_locked']=number_format($persen_locked,2,",",".");
        $persen_locked_arr[$i - $j] = number_format($persen_locked, 2, ",", ".");

        //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
        //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
        //$rs[$i-$j]['dinas_ok']='t';
        $dinas_ok_arr[$i - $j] = 't';
        //$rs[$i-$j]['belaja_id']=$belanja_id;
        $belanja_id_arr[$i - $j] = $belanja_id;
        //$rs[$i-$j]['belanja_name']=$belanja_name;
        $belanja_name_arr[$i - $j] = $belanja_name;

        //$nilai_draft2=number_format($nilai_draft2,0,",",".");
        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        //$nilai_locked2=number_format($nilai_locked2,0,",",".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->belanja_id = $belanja_id_arr;

        $this->belanja_name = $belanja_name_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->persen_draft3 = $persen_draft3_arr;

        $this->persen_locked3 = $persen_locked3_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->rekening_code = $rekening_code_arr;

        $this->rekening_name = $rekening_name_arr;
    }

    public function executeReportBelanja() {
        $this->setFlash('RB', 'selected');
        $query = "select  belanja_id, belanja_name,round(sum((jumlah_draft))) as total_draft, round(sum((jumlah_setuju))) as total_locked,
 					round(sum((nilai_draft))) as nilai_draft, round(sum((nilai_draft*jumlah_setuju))) as nilai_locked
			FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening_dewan  
			WHERE unit_id<>'9999'
			GROUP BY belanja_id, belanja_name
			ORDER BY belanja_id, belanja_name";

        $query2 = "select sum(nilai_anggaran) as sum_draft 
		from " . sfConfig::get('app_default_schema') . ".dewan_rincian r," . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan k," . sfConfig::get('app_default_schema') . ".dewan_rincian_detail d 
                    where d.status_hapus=FALSE and (k.kode_kegiatan=r.kegiatan_code) and (k.unit_id=r.unit_id) and (d.kegiatan_code=r.kegiatan_code) and (d.unit_id=r.unit_id) and (r.unit_id<>'9999') ";
        $con = Propel::getConnection();
//$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
//while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs = $stmt->executeQuery();
        while ($rs->next())
            $sum_draft = $rs->getString('sum_draft');

        $query2 = "select sum(nilai_anggaran) as sum_locked
	 	from " . sfConfig::get('app_default_schema') . ".dewan_rincian r," . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan k," . sfConfig::get('app_default_schema') . ".dewan_rincian_detail d 
		where (k.kode_kegiatan=r.kegiatan_code) and (k.unit_id=r.unit_id) and d.status_hapus=FALSE and
		(d.kegiatan_code=r.kegiatan_code) and (d.unit_id=r.unit_id) and (r.unit_id<>'9999') and (r.rincian_level=3) ";
//$result2 = @pg_query($conn_id,$query2);
        $stmt = $con->prepareStatement($query2);
//while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
        $rs2 = $stmt->executeQuery();
        while ($rs2->next())
            $sum_locked = $rs2->getString('sum_locked');


//$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
//if(!$result) echo @pg_last_error($conn_id);
//$rs=pg_fetch_all($result);
        $rs3 = $stmt->executeQuery();
        $this->rs = $rs3;

        if ($sum_locked == 0 or ! $sum_locked)
            $sum_locked = 99999999999;
        if ($sum_draft == 0 or ! $sum_draft)
            $sum_draft = 99999999999;

        $i = 0;

//foreach($rs as $s)

        $persen_draft_arr = array();
        $this->persen_draft = array();

        $persen_locked_arr = array();
        $this->persen_locked = array();

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $belanja_id_arr = array();
        $this->belanja_id = array();

        $belanja_name_arr = array();
        $this->belanja_name = array();

        while ($rs3->next()) {
            $belanja_name_arr[$i] = $rs3->getString('belanja_name');
            $belanja_id_arr[$i] = $rs3->getString('belanja_id');
//$rs[$i]['persen_draft']=$rs[$i]['nilai_draft'] * 100 / $sum_draft; 
            $persen_draft_arr[$i] = $rs3->getString('nilai_draft') * 100 / $sum_draft;
//$rs[$i]['persen_draft']=number_format($rs[$i]['persen_draft'],2,",",".");
            $persen_draft_arr[$i] = number_format($persen_draft_arr[$i], 2, ",", ".");
//$rs[$i]['persen_locked']=$rs[$i]['nilai_locked'] * 100 / $sum_locked; 
            $persen_locked_arr[$i] = $rs3->getString('nilai_locked') * 100 / $sum_locked;
//$rs[$i]['persen_locked']=number_format($rs[$i]['persen_locked'],2,",",".");
            $persen_locked_arr[$i] = number_format($persen_locked_arr[$i], 2, ",", ".");

//$nilai_draft2+=$rs[$i]['nilai_draft'];
            $this->nilai_draft2+=$rs3->getString('nilai_draft');
//$total_draft2+=$rs[$i]['total_draft'];
            $this->total_draft2+=$rs3->getString('total_draft');
//$nilai_locked2+=$rs[$i]['nilai_locked'];
            $this->nilai_locked2+=$rs3->getString('nilai_locked');
//$total_locked2+=$rs[$i]['total_locked'];
            $this->total_locked2+=$rs3->getString('total_locked');
//$rs[$i]['nilai_draft']=number_format($rs[$i]['nilai_draft'],0,",",".");
            $nilai_draft_arr[$i] = number_format($rs3->getString('nilai_draft'), 0, ",", ".");
//$rs[$i]['nilai_locked']=number_format($rs[$i]['nilai_locked'],0,",",".");
            $nilai_locked_arr[$i] = number_format($rs3->getString('nilai_locked'), 0, ",", ".");
            $i+=1;
        }

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");

        $this->persen_draft = $persen_draft_arr;

        $this->persen_locked = $persen_locked_arr;

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;
        $this->belanja_id = $belanja_id_arr;
        $this->belanja_name = $belanja_name_arr;
    }

//    public function executeReportBelanja() {
//        $this->setFlash('RB', 'selected');
//        $query = "select  belanja_id, belanja_name,sum((jumlah_draft)) as total_draft,sum((jumlah_setuju)) as total_locked,
// 					sum((nilai_draft)) as nilai_draft, sum((nilai_draft*jumlah_setuju)) as nilai_locked
//			FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening
//			WHERE unit_id<>'9999'
//			GROUP BY belanja_id, belanja_name
//			ORDER BY belanja_id, belanja_name";
//
//        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
//		from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d where (k.kode_kegiatan=r.kegiatan_code) and (k.unit_id=r.unit_id) and (d.kegiatan_code=r.kegiatan_code) and (d.unit_id=r.unit_id) and (r.unit_id<>'9999') ";
//        $con = Propel::getConnection();
//        //$result2 = @pg_query($conn_id,$query2);
//        $stmt = $con->prepareStatement($query2);
//        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
//        $rs = $stmt->executeQuery();
//        while ($rs->next())
//            $sum_draft = $rs->getString('sum_draft');
//
//        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
//	 	from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
//		where (k.kode_kegiatan=r.kegiatan_code) and (k.unit_id=r.unit_id) and 
//		(d.kegiatan_code=r.kegiatan_code) and (d.unit_id=r.unit_id) and (r.unit_id<>'9999') and (r.rincian_level=3) ";
//        //$result2 = @pg_query($conn_id,$query2);
//        $stmt = $con->prepareStatement($query2);
//        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
//        $rs2 = $stmt->executeQuery();
//        while ($rs2->next())
//            $sum_locked = $rs2->getString('sum_locked');
//
//
//        //$result = @pg_query($conn_id,$query);
//        $stmt = $con->prepareStatement($query);
//        //if(!$result) echo @pg_last_error($conn_id);
//        //$rs=pg_fetch_all($result);
//        $rs3 = $stmt->executeQuery();
//        $this->rs = $rs3;
//
//        if ($sum_locked == 0 or !$sum_locked)
//            $sum_locked = 99999999999;
//        if ($sum_draft == 0 or !$sum_draft)
//            $sum_draft = 99999999999;
//
//        $i = 0;
//
//        //foreach($rs as $s)
//
//        $persen_draft_arr = array();
//        $this->persen_draft = array();
//
//        $persen_locked_arr = array();
//        $this->persen_locked = array();
//
//        $nilai_draft_arr = array();
//        $this->nilai_draft = array();
//
//        $nilai_locked_arr = array();
//        $this->nilai_locked = array();
//
//        $belanja_id_arr = array();
//        $this->belanja_id = array();
//
//        $belanja_name_arr = array();
//        $this->belanja_name = array();
//
//        while ($rs3->next()) {
//            $belanja_name_arr[$i] = $rs3->getString('belanja_name');
//            $belanja_id_arr[$i] = $rs3->getString('belanja_id');
//            //$rs[$i]['persen_draft']=$rs[$i]['nilai_draft'] * 100 / $sum_draft; 
//            $persen_draft_arr[$i] = $rs3->getString('nilai_draft') * 100 / $sum_draft;
//            //$rs[$i]['persen_draft']=number_format($rs[$i]['persen_draft'],2,",",".");
//            $persen_draft_arr[$i] = number_format($persen_draft_arr[$i], 2, ",", ".");
//            //$rs[$i]['persen_locked']=$rs[$i]['nilai_locked'] * 100 / $sum_locked; 
//            $persen_locked_arr[$i] = $rs3->getString('nilai_locked') * 100 / $sum_locked;
//            //$rs[$i]['persen_locked']=number_format($rs[$i]['persen_locked'],2,",",".");
//            $persen_locked_arr[$i] = number_format($persen_locked_arr[$i], 2, ",", ".");
//
//            //$nilai_draft2+=$rs[$i]['nilai_draft'];
//            $this->nilai_draft2+=$rs3->getString('nilai_draft');
//            //$total_draft2+=$rs[$i]['total_draft'];
//            $this->total_draft2+=$rs3->getString('total_draft');
//            //$nilai_locked2+=$rs[$i]['nilai_locked'];
//            $this->nilai_locked2+=$rs3->getString('nilai_locked');
//            //$total_locked2+=$rs[$i]['total_locked'];
//            $this->total_locked2+=$rs3->getString('total_locked');
//            //$rs[$i]['nilai_draft']=number_format($rs[$i]['nilai_draft'],0,",",".");
//            $nilai_draft_arr[$i] = number_format($rs3->getString('nilai_draft'), 0, ",", ".");
//            //$rs[$i]['nilai_locked']=number_format($rs[$i]['nilai_locked'],0,",",".");
//            $nilai_locked_arr[$i] = number_format($rs3->getString('nilai_locked'), 0, ",", ".");
//            $i+=1;
//        }
//
//        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
//        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");
//
//        $this->persen_draft = $persen_draft_arr;
//
//        $this->persen_locked = $persen_locked_arr;
//
//        $this->nilai_draft = $nilai_draft_arr;
//
//        $this->nilai_locked = $nilai_locked_arr;
//        $this->belanja_id = $belanja_id_arr;
//        $this->belanja_name = $belanja_name_arr;
//    }
//    public function executeReportDinas() {
//        $this->setFlash('RD', 'selected');
//        /**
//         * if (($this->getRequestParameter('tipe_anggaran')==2) or (!$this->getRequestParameter('tipe_anggaran'))) 
//         * 		 {
//         * 			 $query="select unit_id, unit_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked,sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked FROM ". sfConfig::get('app_default_schema') .".v_dinas_kegiatan
//         * 			WHERE unit_id<>'9999'
//         * 			GROUP BY unit_id, unit_name
//         * 			ORDER BY unit_id,unit_name";
//         * 			  $this->a2="SELECTED";
//         * 		 }
//         * 		elseif ($tipe_anggaran==3)
//         * 		{
//         * 			$query="select unit_id, unit_name ,sum((jumlah_draft*jumlah_lanjutan)) as total_draft,sum((jumlah_setuju*jumlah_lanjutan)) as total_locked,sum((nilai_draft*jumlah_lanjutan)) as nilai_draft, sum((nilai_draft*jumlah_setuju*jumlah_lanjutan)) as nilai_locked 
//         * 						FROM ". sfConfig::get('app_default_schema') .".v_dinas_kegiatan
//         * 						WHERE unit_id<>'9999'
//         * 						GROUP BY unit_id, unit_name
//         * 						ORDER BY unit_id,unit_name";
//         * 			 $this->a3="SELECTED";
//         * 		}
//         * 		else
//         * 		{
//         */
//        $query = "select unit_id, unit_name ,sum((jumlah_draft)) as total_draft,sum((jumlah_setuju)) as total_locked,
//			 					sum((nilai_setuju)) as nilai_draft, sum((nilai_setuju*jumlah_setuju)) as nilai_locked
//						FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian3
//						WHERE unit_id<>'9999'
//						GROUP BY unit_id, unit_name
//						ORDER BY unit_id,unit_name";
//
//        //$this->a1="SELECTED";
//        //}
//
//        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_draft
//			 from " . sfConfig::get('app_default_schema') . ".rincian r," . sfConfig::get('app_default_schema') . ".master_kegiatan k," . sfConfig::get('app_default_schema') . ".rincian_detail d 
//			where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id  and 
//			d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999'";
//
//        $con = Propel::getConnection();
//        $stmt2 = $con->prepareStatement($query2);
//        $rs2 = $stmt2->executeQuery();
//        while ($rs2->next()) {
//            $sum_draft = $rs2->getString('sum_draft');
//        }
//        //$result2 = @pg_query($conn_id,$query2);
//        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
//
//        $query2 = "select sum(komponen_harga_awal*volume*(100+pajak)/100) as sum_locked
//			 from " . sfConfig::get('app_default_schema') . ".rincian r, " . sfConfig::get('app_default_schema') . ".master_kegiatan k, " . sfConfig::get('app_default_schema') . ".rincian_detail d 
//			where k.kode_kegiatan=r.kegiatan_code and k.unit_id=r.unit_id and 
//			d.kegiatan_code=r.kegiatan_code and d.unit_id=r.unit_id and r.unit_id<>'9999' and r.rincian_level=3";
//
//        $stmt3 = $con->prepareStatement($query2);
//        $rs3 = $stmt3->executeQuery();
//        while ($rs3->next()) {
//            $sum_locked = $rs3->getString('sum_locked');
//        }
//
//        //$result2 = @pg_query($conn_id,$query2);
//        //while($rows2 = @pg_fetch_assoc($result2))	extract($rows2,EXTR_OVERWRITE);
//
//        $stmt = $con->prepareStatement($query);
//        $rs = $stmt->executeQuery();
//        $this->rs = $rs;
//        //$result = @pg_query($conn_id,$query);
//        //$rs=pg_fetch_all($result);
//
//        if (!$sum_draft)
//            $sum_draft = 9999999999;
//        if (!$sum_locked)
//            $sum_locked = 9999999999;
//
//        $i = 0;
//        $this->total_pagu = 0;
//        $this->total_selisih = 0;
//
//        //foreach($rs as $s)
//        $persen_draft_arr = array();
//        $this->persen_draft = array();
//
//        $persen_locked_arr = array();
//        $this->persen_locked = array();
//
//        $pagu_arr = array();
//        $this->pagu = array();
//
//        $nilai_draft_arr = array();
//        $this->nilai_draft = array();
//
//        $nilai_locked_arr = array();
//        $this->nilai_locked = array();
//
//        $selisih_arr = array();
//        $this->selisih = array();
//
//        $nilai_usulan_arr = array();
//        $this->nilai_usulan = array();
//
//        while ($rs->next()) {
//            //$rs[$i]['persen_draft']=$rs[$i]['nilai_draft'] * 100 / $sum_draft; 
//            $persen_draft_arr[$i] = $rs->getString('nilai_draft') * 100 / $sum_draft;
//            //$rs[$i]['persen_draft']=number_format($rs[$i]['persen_draft'],2,",",".");
//            $persen_draft_arr[$i] = number_format($persen_draft_arr[$i], 2, ",", ".");
//            //$rs[$i]['persen_locked']=$rs[$i]['nilai_locked'] * 100 / $sum_locked; 
//            $persen_locked_arr[$i] = $rs->getString('nilai_locked') * 100 / $sum_locked;
//            //$rs[$i]['persen_locked']=number_format($rs[$i]['persen_locked'],2,",",".");
//            $persen_locked_arr[$i] = number_format($persen_locked_arr[$i], 2, ",", ".");
//
//            //$query2="select pagu from unit_kerja where unit_id='".$rs->getString('unit_id')."'";
//            $query2 = "select sum(alokasi_dana) as pagu  from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $rs->getString('unit_id') . "'";
//            //$result2 = @pg_query($conn_id,$query2);
//            $stmt4 = $con->prepareStatement($query2);
//            $rs4 = $stmt4->executeQuery();
//
//            //while($rows2 = $rs4->next())	extract($rows2,EXTR_OVERWRITE);
//            while ($rs4->next()) {
//                $pagu_arr[$i] = $rs4->getString('pagu');
//            }
//
//            $query3 = "select sum(rd.volume*rd.komponen_harga_awal*(100+rd.pajak)/100) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".rincian_detail rd where rd.unit_id='" . $rs->getString('unit_id') . "'";
//            $stmt5 = $con->prepareStatement($query3);
//            $rs5 = $stmt5->executeQuery();
//
//            //while($rows2 = $rs4->next())	extract($rows2,EXTR_OVERWRITE);
//            while ($rs5->next()) {
//                $nilai_usulan_arr[$i] = number_format($rs5->getString('nilai_usulan'), 0, ",", ".");
//                $this->nilai_draft2+=$rs5->getString('nilai_usulan');
//            }
//            //$nilai_draft2+=$rs[$i]['nilai_draft'];
//            //$this->nilai_draft2+=$rs->getString('nilai_draft');
//            $this->total_pagu+=$pagu_arr[$i];
//            $selisih_arr[$i] = $pagu_arr[$i] - $rs->getString('nilai_draft');
//            $this->total_selisih+=$selisih_arr[$i];
//            $this->total_draft2+=$rs->getString('total_draft');
//            $this->nilai_locked2+=$rs->getString('nilai_locked');
//            $this->total_locked2+=$rs->getString('total_locked');
//            $nilai_draft_arr[$i] = number_format($rs->getString('nilai_draft'), 0, ",", ".");
//            $nilai_locked_arr[$i] = number_format($rs->getString('nilai_locked'), 0, ",", ".");
//            $pagu_arr[$i] = number_format($pagu_arr[$i], 0, ",", ".");
//            $selisih_arr[$i] = number_format($selisih_arr[$i], 0, ",", ".");
//            $i+=1;
//        }
//
//        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
//        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");
//
//        $this->total_pagu = number_format($this->total_pagu, 0, ",", ".");
//        $this->total_selisih = number_format($this->total_selisih, 0, ",", ".");
//
//        $this->persen_draft = $persen_draft_arr;
//
//        $this->persen_locked = $persen_locked_arr;
//
//        $this->pagu = $pagu_arr;
//
//        $this->nilai_draft = $nilai_draft_arr;
//
//        $this->nilai_locked = $nilai_locked_arr;
//
//        $this->selisih = $selisih_arr;
//
//        $this->nilai_usulan = $nilai_usulan_arr;
//    }

    public function executeReportDinasKegiatan() {
        $this->setFlash('RDK', 'selected');
        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan,nama_kegiatan,kode_sasaran,catatan,((jumlah_draft*1)) as total_draft3,((jumlah_setuju*1)) as total_locked3,((nilai_draft*1)) as nilai_draft3, ((nilai_setuju*jumlah_setuju*1)) as nilai_locked3
			FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian5_dewan 
				WHERE unit_id<>'9999'
			ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,kode_sasaran ";
// print_r($query);exit;
//parse_str(decode($coded));
//if($error_message) $onload = "alert('$error_message');";

        $query2 = "Select unit_id,unit_name from unit_kerja order by unit_id limit 1";
//$result = @pg_query($conn_id,$query2);
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query2);
        $rs = $stmt->executeQuery();
//while($rs->next()) extract($rs,EXTR_OVERWRITE);

        while ($rs->next()) {
            $unit_id = $rs->getString('unit_id');
            $unit_name = $rs->getString('unit_name');
        }

//$result = @pg_query($conn_id,$query);
        $stmt = $con->prepareStatement($query);
        $rs2 = $stmt->executeQuery();
        $this->rs = $rs2;
//if(!$rs) echo @pg_last_error($conn_id);
//$rs=pg_fetch_all($result);

        $i = 0;
        $j = 0;
//foreach($rs as $s)
        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $unit_id_arr = array();
        $this->unit_id = array();

        $unit_name_arr = array();
        $this->unit_name = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();



        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $nilai_usulan_unit_arr = array();
        $this->nilai_usulan_unit = array();

        $nilai_usulan_kegiatan_arr = array();
        $this->nilai_usulan_kegiatan = array();
        $this->nilai_draft2 = 0;
        $this->nilai_draft4 = 0;
        $this->nilai_locked2 = 0;
        $nilai_draft = 0;
        $nilai_locked = 0;
//$this->nilai_draft2=$this->nilai_locked2=0;
        while ($rs2->next()) {
            if ($unit_id != $rs2->getString('old_unit_id')) {

                $this->nilai_draft2+=$nilai_draft;
                $this->nilai_locked2+=$nilai_locked;
                $query3 = "select sum(rd.nilai_anggaran) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail rd 
                                    where rd.unit_id='" . $unit_id . "' and rd.status_hapus=FALSE ";
                $stmt3 = $con->prepareStatement($query3);
                $rs3 = $stmt3->executeQuery();
                while ($rs3->next()) {
                    $nilai_usulan_unit_arr[$i - $j] = number_format($rs3->getString('nilai_usulan'), 0, ",", ".");
                }
                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
                $dinas_ok_arr[$i - $j] = 't';
                $unit_id_arr[$i - $j] = $unit_id;
                $unit_name_arr[$i - $j] = $unit_name;
                $j = 0;
                $nilai_draft = 0;
                $nilai_locked = 0;
            }
            $unit_id = $rs2->getString('old_unit_id');
            $unit_name = $rs2->getString('old_unit_name');
            $unit_id_arr[$i] = $unit_id;
            $nilai_draft+=$rs2->getString('nilai_draft3');
            $nilai_locked+=$rs2->getString('nilai_locked3');

            $query4 = "select sum(rd.nilai_anggaran) as nilai_usulan_kegiatan from " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail rd 
                            where rd.status_hapus=FALSE and rd.unit_id='" . $unit_id . "' and kegiatan_code='" . $rs2->getString('kode_kegiatan') . "'";
            $stmt4 = $con->prepareStatement($query4);
            $rs4 = $stmt4->executeQuery();
            while ($rs4->next()) {
                $nilai_usulan_kegiatan_arr[$i] = number_format($rs4->getString('nilai_usulan_kegiatan'), 0, ",", ".");
                $this->nilai_draft4+=$rs4->getString('nilai_usulan_kegiatan');
            }

            $nilai_draft3_arr[$i] = number_format($rs2->getString('nilai_draft3'), 0, ",", ".");
            $nilai_locked3_arr[$i] = number_format($rs2->getString('nilai_locked3'), 0, ",", ".");

            $i+=1;
            $j+=1;
        }
        $this->nilai_draft2+=$nilai_draft;
        $this->nilai_locked2+=$nilai_locked;

        $query3 = "select sum(rd.nilai_anggaran) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail rd 
                    where rd.status_hapus=FALSE and rd.unit_id='" . $unit_id . "'";
        $stmt3 = $con->prepareStatement($query3);
        $rs3 = $stmt3->executeQuery();
        while ($rs3->next()) {
            $nilai_usulan_unit_arr[$i - $j] = number_format($rs3->getString('nilai_usulan'), 0, ",", ".");
        }
//$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
//$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
//$rs[$i-$j]['dinas_ok']='t';
        $dinas_ok_arr[$i - $j] = 't';
//$rs[$i-$j]['unit_id']=$unit_id;
        $unit_id_arr[$i - $j] = $unit_id;
//$rs[$i-$j]['unit_name']=$unit_name;
        $unit_name_arr[$i - $j] = $unit_name;

        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");
        $this->nilai_draft4 = number_format($this->nilai_draft4, 0, ",", ".");

        $this->nilai_draft = $nilai_draft_arr;

        $this->nilai_locked = $nilai_locked_arr;

        $this->dinas_ok = $dinas_ok_arr;

        $this->unit_id = $unit_id_arr;

        $this->unit_name = $unit_name_arr;

        $this->nilai_draft3 = $nilai_draft3_arr;

        $this->nilai_locked3 = $nilai_locked3_arr;

        $this->nilai_usulan_unit = $nilai_usulan_unit_arr;

        $this->nilai_usulan_kegiatan = $nilai_usulan_kegiatan_arr;
    }

//    public function executeReportDinasKegiatan() {
//        $this->setFlash('RDK', 'selected');
//        $query = "select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan,nama_kegiatan ,kode_program,kode_bidang,kode_sasaran,((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3,((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, ((nilai_setuju*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 FROM " . sfConfig::get('app_default_schema') . ".v_dinas_kegiatan_rincian3 WHERE unit_id<>'9999'	ORDER BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,kode_program,kode_bidang,kode_sasaran ";
//
//        //parse_str(decode($coded));
//        //if($error_message) $onload = "alert('$error_message');";
//
//        $query2 = "Select unit_id,unit_name from unit_kerja order by unit_id limit 1";
//        //$result = @pg_query($conn_id,$query2);
//        $con = Propel::getConnection();
//        $stmt = $con->prepareStatement($query2);
//        $rs = $stmt->executeQuery();
//        //while($rs->next()) extract($rs,EXTR_OVERWRITE);
//
//        while ($rs->next()) {
//            $unit_id = $rs->getString('unit_id');
//            $unit_name = $rs->getString('unit_name');
//        }
//
//        //$result = @pg_query($conn_id,$query);
//        $stmt = $con->prepareStatement($query);
//        $rs2 = $stmt->executeQuery();
//        $this->rs = $rs2;
//        //if(!$rs) echo @pg_last_error($conn_id);
//        //$rs=pg_fetch_all($result);
//
//        $i = 0;
//        $j = 0;
//        //foreach($rs as $s)
//        $nilai_draft_arr = array();
//        $this->nilai_draft = array();
//
//        $nilai_locked_arr = array();
//        $this->nilai_locked = array();
//
//        $dinas_ok_arr = array();
//        $this->dinas_ok = array();
//
//        $unit_id_arr = array();
//        $this->unit_id = array();
//
//        $unit_name_arr = array();
//        $this->unit_name = array();
//
//        $nilai_draft3_arr = array();
//        $this->nilai_draft3 = array();
//
//
//
//        $nilai_locked3_arr = array();
//        $this->nilai_locked3 = array();
//
//        $nilai_usulan_unit_arr = array();
//        $this->nilai_usulan_unit = array();
//
//        $nilai_usulan_kegiatan_arr = array();
//        $this->nilai_usulan_kegiatan = array();
//        $this->nilai_draft2 = 0;
//        $this->nilai_draft4 = 0;
//        $this->nilai_locked2 = 0;
//        $nilai_draft = 0;
//        $nilai_locked = 0;
//        //$this->nilai_draft2=$this->nilai_locked2=0;
//        while ($rs2->next()) {
//            if ($unit_id != $rs2->getString('old_unit_id')) {
//
//                $this->nilai_draft2+=$nilai_draft;
//                $this->nilai_locked2+=$nilai_locked;
//                $query3 = "select sum(rd.volume*rd.komponen_harga_awal*(100+rd.pajak)/100) as nilai_usulan from " . sfConfig::get('app_default_schema') . ".rincian_detail rd where rd.unit_id='" . $unit_id . "'";
//                $stmt3 = $con->prepareStatement($query3);
//                $rs3 = $stmt3->executeQuery();
//                while ($rs3->next()) {
//                    $nilai_usulan_unit_arr[$i] = number_format($rs3->getString('nilai_usulan'), 0, ",", ".");
//                }
//                $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
//                $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
//                $dinas_ok_arr[$i - $j] = 't';
//                $unit_id_arr[$i - $j] = $unit_id;
//                $unit_name_arr[$i - $j] = $unit_name;
//                $j = 0;
//                $nilai_draft = 0;
//                $nilai_locked = 0;
//            }
//            $unit_id = $rs2->getString('old_unit_id');
//            $unit_name = $rs2->getString('old_unit_name');
//            $unit_id_arr[$i] = $unit_id;
//            $nilai_draft+=$rs2->getString('nilai_draft3');
//            $nilai_locked+=$rs2->getString('nilai_locked3');
//
//            $query4 = "select sum(rd.volume*rd.komponen_harga_awal*(100+rd.pajak)/100) as nilai_usulan_kegiatan from " . sfConfig::get('app_default_schema') . ".rincian_detail rd where rd.unit_id='" . $unit_id . "' and kegiatan_code='" . $rs2->getString('kode_kegiatan') . "'";
//            $stmt4 = $con->prepareStatement($query4);
//            $rs4 = $stmt4->executeQuery();
//            while ($rs4->next()) {
//                $nilai_usulan_kegiatan_arr[$i] = number_format($rs4->getString('nilai_usulan_kegiatan'), 0, ",", ".");
//                $this->nilai_draft4+=$rs4->getString('nilai_usulan_kegiatan');
//            }
//
//            $nilai_draft3_arr[$i] = number_format($rs2->getString('nilai_draft3'), 0, ",", ".");
//            $nilai_locked3_arr[$i] = number_format($rs2->getString('nilai_locked3'), 0, ",", ".");
//
//            $i+=1;
//            $j+=1;
//        }
//        $this->nilai_draft2+=$nilai_draft;
//        $this->nilai_locked2+=$nilai_locked;
//
//        //$rs[$i-$j]['nilai_draft']=number_format($nilai_draft,0,",",".");
//        $nilai_draft_arr[$i - $j] = number_format($nilai_draft, 0, ",", ".");
//        //$rs[$i-$j]['nilai_locked']=number_format($nilai_locked,0,",",".");
//        $nilai_locked_arr[$i - $j] = number_format($nilai_locked, 0, ",", ".");
//        //$rs[$i-$j]['dinas_ok']='t';
//        $dinas_ok_arr[$i - $j] = 't';
//        //$rs[$i-$j]['unit_id']=$unit_id;
//        $unit_id_arr[$i - $j] = $unit_id;
//        //$rs[$i-$j]['unit_name']=$unit_name;
//        $unit_name_arr[$i - $j] = $unit_name;
//
//        $this->nilai_draft2 = number_format($this->nilai_draft2, 0, ",", ".");
//        $this->nilai_locked2 = number_format($this->nilai_locked2, 0, ",", ".");
//        $this->nilai_draft4 = number_format($this->nilai_draft4, 0, ",", ".");
//
//        $this->nilai_draft = $nilai_draft_arr;
//
//        $this->nilai_locked = $nilai_locked_arr;
//
//        $this->dinas_ok = $dinas_ok_arr;
//
//        $this->unit_id = $unit_id_arr;
//
//        $this->unit_name = $unit_name_arr;
//
//        $this->nilai_draft3 = $nilai_draft3_arr;
//
//        $this->nilai_locked3 = $nilai_locked3_arr;
//
//        $this->nilai_usulan_unit = $nilai_usulan_unit_arr;
//
//        $this->nilai_usulan_kegiatan = $nilai_usulan_kegiatan_arr;
//    }

    public function executeUbahPass() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $member = MasterUserV2Peer::retrieveByPK($this->getUser()->getNamaUser());

            $member->setUserPassword(md5($this->getRequestParameter('pass_baru')));
            $member->save();
            $this->setFlash('berhasil', 'Password berhasil terganti.');

            $this->habis_ganti = true;
        }
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

    public function executeList() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $this->pager = new sfPropelPager('MasterUserV2', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        //irul 8 november - bukuputih
        $this->pager = new sfPropelPager('MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->add(MasterKegiatanPeer::UNIT_ID,'9999',  Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
        }
        $this->addFiltersCriteria($c);
        //irul 8 november - bukuputih

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    public function executeListRevisi() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $this->pager = new sfPropelPager('MasterUserV2', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        //irul 8 november - bukuputih
        $this->pager = new sfPropelPager('MurniBukuPutihMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID,'9999',  Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        }
        $this->addFiltersCriteriaRevisi($c);
        //irul 8 november - bukuputih

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    public function executeListRevisiMurniBukuPutih() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $this->pager = new sfPropelPager('MasterUserV2', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        //evin 12 september - murnibukuputih
        $this->pager = new sfPropelPager('MurniBukuPutihMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID,'9999',  Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->addAscendingOrderByColumn(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        }
        $this->addFiltersCriteriaRevisiMurniBukuPutih($c);
        //evin 12 september - murnibukuputih

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    public function executeListRevisiPakBukuPutih() {
        $this->processSort();
        $this->processFilters();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        $this->pager = new sfPropelPager('MasterUserV2', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        $this->addFiltersCriteria($c);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

        //aminudin 12 september - pakbukuputih
        $this->pager = new sfPropelPager('PakBukuPutihMasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID,'9999',  Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        $unit_id = $this->getRequestParameter('unit_id');
        if (isset($unit_id)) {
            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->addAscendingOrderByColumn(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN);
        }
        $this->addFiltersCriteriaRevisiPakBukuPutih($c);
        //aminudin 12 september - pakbukuputih


        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilters() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/master_kegiatan');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/master_kegiatan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/master_kegiatan/filters');
        }
    }

    protected function processSort() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/master_kegiatan/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/master_kegiatan/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/master_kegiatan/sort')) {
            
        }
    }

    protected function addSortCriteria($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/master_user/sort')) {
            $sort_column = MasterUserV2Peer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/master_user/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    protected function addFiltersCriteria($c) {
        //$c->setDistinct();
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    protected function addFiltersCriteriaRevisi($c) {
        //$c->setDistinct();
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    protected function addFiltersCriteriaRevisiMurniBukuPutih($c) {
        //$c->setDistinct();
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MurniBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(MurniBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    protected function addFiltersCriteriaRevisiPakBukuPutih($c) {
        //$c->setDistinct();
        if (isset($this->filters['kode_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['kode_kegiatan']) && $this->filters['kode_kegiatan'] !== '') {
            $kode_kegiatan = $this->filters['kode_kegiatan'];
            $arr = explode('.', $kode_kegiatan);
            $jum_array = count($arr);
            //if($jum_array<2)
            //{
            $cton1 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_URUSAN, $kode_kegiatan . '%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM2, $kode_kegiatan . '%', Criteria::LIKE);
            $cton1->addOr($cton2);
            //$c->add($cton1);
            //}
            if ($jum_array == 2) {
                //$c->add(MasterKegiatanPeer::KODE_PROGRAM,$arr[1],Criteria::LIKE);
                $cton3 = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::KODE_PROGRAM, $arr[1] . '%', Criteria::LIKE);
                $cton1->addAnd($cton3);
            }
            $c->add($cton1);
        }
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(PakBukuPutihMasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
        if (isset($this->filters['unit_id']) && $this->filters['unit_id'] !== '') {
            $unit = $this->filters['unit_id'];
            $c->add(PakBukuPutihMasterKegiatanPeer::UNIT_ID, $unit);
        }
    }

    //irul 13 maret 2014 - laporan per rekening
    public function executeLaporanRekening() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatans = $this->getRequestParameter('kode_kegiatan');
        $this->unit_id_header = $unit_id;

        $this->kode_kegiatan = $kode_kegiatans;
        $this->kode_kegiatan_header = $kode_kegiatans;
        $c = new Criteria();
        //awal
//        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
//        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatans);
//        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        //awal
        $c->add(BelurMasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(BelurMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatans);
        $master_kegiatan = BelurMasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
//print_r($kode_program22);
//exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }$this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan_utama = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program kp
				where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
				where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
				where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
//print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }
        }

//end Header

        /*
          $unit_name= new Criteria();
          $unit_name->add(UnitKerjaPeer::UNIT_ID,$unit_id);
          $rs_unit= UnitKerjaPeer::doSelectOne($unit_name);

          $this->nama_unit=$rs_unit->getUnitName();
          //$this->unit_unit=$rs_unit->getUnitId();
         */

        $query = "(select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan as old_kode_kegiatan, nama_kegiatan as old_nama_kegiatan,subtitle,rekening_code,rekening_name , 
            sum(nilai_draft) as nilai_locked4 , -666 as nilai_draft4 
            FROM " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening2_belur v WHERE unit_id='$unit_id' and kode_kegiatan='$kode_kegiatans'
            GROUP BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,subtitle,rekening_code,rekening_name ) union 
            (select unit_id as old_unit_id, unit_name as old_unit_name,kode_kegiatan as old_kode_kegiatan, nama_kegiatan as old_nama_kegiatan,subtitle,rekening_code,rekening_name , 0 as nilai_locked4, 
            sum(nilai_draft) as nilai_draft4 FROM " . sfConfig::get('app_default_schema') . ".prev_v_kegiatan_rekening_belur v 
            WHERE unit_id='$unit_id'  and kode_kegiatan='$kode_kegiatans' and kode_kegiatan || subtitle || rekening_code not in 
	    (select kode_kegiatan || subtitle || rekening_code from " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening2_belur v WHERE unit_id='$unit_id'  and kode_kegiatan='$kode_kegiatans') 
            GROUP BY unit_id,unit_name,kode_kegiatan,nama_kegiatan,subtitle,rekening_code,rekening_name )	 
            ORDER BY old_unit_id,old_unit_name,old_kode_kegiatan,old_nama_kegiatan,subtitle,rekening_code,rekening_name";

        $con = Propel::getConnection();
//print_r($query);exit;
        /* $query2="Select unit_id,kode_kegiatan from ". sfConfig::get('app_default_schema') .".master_kegiatan
          where unit_id='$unit_id'
          order by unit_id,kode_kegiatan limit 1";


          $stmt=$con->prepareStatement($query2);
          $rs2=$stmt->executeQuery();
          while($rs2->next())
          {
          $unit_id=$rs2->getString('unit_id');
          $kode_kegiatan=$rs2->getString('kode_kegiatan');
          } */



        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->rs = $rs;

//while($rs->next()){
//print_r($query);
//}

        $nilai_draft_arr = array();
        $this->nilai_draft = array();

        $nilai_locked_arr = array();
        $this->nilai_locked = array();

        $target_arr = array();
        $this->target = array();

        $dinas_ok_arr = array();
        $this->dinas_ok = array();

        $kegiatan_ok_arr = array();
        $this->kegiatan_ok = array();

        $subtitle_arr = array();
        $this->subtitle = array();

        $kode_kegiatan_arr = array();
        $this->kode_kegiatan = array();

        $nama_kegiatan_arr = array();
        $this->nama_kegiatan = array();

        $nilai_draft3_arr = array();
        $this->nilai_draft3 = array();

        $nilai_locked3_arr = array();
        $this->nilai_locked3 = array();

        $nilai_draft4_arr = array();
        $this->nilai_draft4 = array();
        $nilai_locked4_arr = array();
        $this->nilai_locked4 = array();
        $kode_rekening_arr = array();
        $this->kode_rekening = array();
        $nama_rekening_arr = array();
        $this->nama_rekening = array();

        $total_keg_semula_arr = array();
        $total_keg_menjadi_arr = array();
        $total_keg_ubah_arr = array();
        $total_unit_semula_arr = array();
        $total_unit_menjadi_arr = array();
        $total_unit_ubah_arr = array();
        $nilai_ubah_arr = array();
        $unit_id_arr = array();
        $unit_name_arr = array();
        $sub_ok_arr = array();
        $nilai_draft5_arr = array();
        $nilai_locked5_arr = array();
        $nilai_ubah3_arr = array();
        $nilai_ubah4_arr = array();
        $nilai_ubah5_arr = array();


        /* $subtitle='';
          $nilai_draft=0;
          $nilai_locked=0;
          $nilai_draft3=0;
          $nilai_locked3=0;
          $nilai_draft2=0;
          $nilai_locked2=0;
          //$nilai_draft4=0;
          //$nilai_locked4=0;
          $nama_kegiatan='';
          $kode_kegiatan=''; */
        $unit_id = '';

        $i = 0;
        $j = 0;
        $k = 0;
        $m = 0;
//$nilai_draft4_q8=0;


        while ($rs->next()) {
//var_dump($nilai_ubah4_arr);
//echo '<br>';
//bisma ngoding

            $nilai_draft4_q8 = $rs->getString('nilai_draft4');
            if ($rs->getString('nilai_draft4') == -666) {
                $query8 = "select sum(nilai_draft) as nilai_draft4 from " . sfConfig::get('app_default_schema') . ".prev_v_kegiatan_rekening_belur p where p.unit_id='" . $rs->getString('old_unit_id') . "' and p.kode_kegiatan='" .
                        $rs->getString('old_kode_kegiatan') . "' and p.rekening_code='" . $rs->getString('rekening_code') . "' and p.subtitle = '" . $rs->getString('subtitle') . "'";

// echo $query8;

                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query8);
                $rs8 = $stmt->executeQuery();
                while ($rs8->next()) {
                    $nilai_draft4_q8 = $rs8->getString('nilai_draft4');
                }
            }
            if ($nilai_draft4_q8 === 0) {
                $nilai_draft4_q8 = 0;
            }
            $nilai_draft4_arr[$i] = $nilai_draft4_q8;


            if ($kode_kegiatan != $rs->getString('old_kode_kegiatan')) {

                $udah = 'f';
                $oldsub = 'budgeting';

                $query8 = "select sum(nilai_draft) as total_keg_semula 
                    from " . sfConfig::get('app_default_schema') . ".prev_v_kegiatan_rekening_belur p where p.unit_id='" . $rs->getString('old_unit_id') . "' and p.kode_kegiatan='" . $rs->getString('old_kode_kegiatan') . "'";
//echo $query8;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query8);
                $rs8 = $stmt->executeQuery();
                while ($rs8->next()) {
                    $total_keg_semula_q8 = $rs8->getString('total_keg_semula');
                }
                $total_keg_semula_arr[$i] = number_format($total_keg_semula_q8, 0, ",", ".");

                $query8 = "select sum(nilai_draft) as total_keg_menjadi 
                    from " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening_belur p where p.unit_id='" . $rs->getString('old_unit_id') . "' and p.kode_kegiatan='" . $rs->getString('old_kode_kegiatan') . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query8);
                $rs8 = $stmt->executeQuery();
                while ($rs8->next()) {
                    $total_keg_menjadi_q8 = $rs8->getString('total_keg_menjadi');
                }
                $total_keg_menjadi_arr[$i] = number_format($total_keg_menjadi_q8, 0, ",", ".");
                $total_keg_ubah_arr[$i] = number_format($total_keg_menjadi_q8 - $total_keg_semula_q8, 0, ",", ".");

                if ($unit_id != $rs->getString('old_unit_id')) {
                    $query8 = "select sum(nilai_draft) as total_unit_semula 
                            from " . sfConfig::get('app_default_schema') . ".prev_v_kegiatan_rekening_belur p where p.unit_id='" . $rs->getString('old_unit_id') . "'and p.kode_kegiatan='" . $rs->getString('old_kode_kegiatan') . "'";

                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query8);
                    $rs8 = $stmt->executeQuery();
                    while ($rs8->next()) {
                        $total_unit_semula = $rs8->getString('total_unit_semula');
                    }

                    $total_unit_semula_arr[$i] = number_format($total_unit_semula, 0, ",", ".");

                    $query8 = "select sum(nilai_draft) as total_unit_menjadi 
                            from " . sfConfig::get('app_default_schema') . ".v_kegiatan_rekening_belur p where p.unit_id='" . $rs->getString('old_unit_id') . "'and p.kode_kegiatan='" . $rs->getString('old_kode_kegiatan') . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query8);
                    $rs8 = $stmt->executeQuery();
                    while ($rs8->next()) {
                        $total_unit_menjadi = $rs8->getString('total_unit_menjadi');
                    }
                    $total_unit_menjadi_arr[$i] = number_format($total_unit_menjadi, 0, ",", ".");
                    $total_unit_ubah_arr[$i] = number_format($total_unit_menjadi - $total_unit_semula, 0, ",", ".");

                    $nilai_draft+=$nilai_draft3;
                    $nilai_locked+=$nilai_locked3;

                    $nilai_draft2+=$nilai_draft;
                    $nilai_locked2+=$nilai_locked;

                    $oldsub = 'budgeting';

                    $nilai_ubah_arr[$i - $k] = number_format($nilai_locked - $nilai_draft, 0, ",", ".");
                    $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
                    $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");

                    $dinas_ok_arr[$i - $k] = 't';
                    $unit_id_arr[$i - $k] = $unit_id;
                    $unit_name_arr[$i - $k] = $unit_name;

                    $k = 0;
                    $nilai_draft = 0;
                    $nilai_locked = 0;

                    $udah = 't';

                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");
                    $nilai_ubah3_arr[$i - $j] = number_format(0 + $nilai_locked3 - $nilai_draft3, 0, ",", ".");

                    $kegiatan_ok_arr[$i - $j] = 't';
                    $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
                    $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;

                    $nilai_draft3 = 0;
                    $nilai_locked3 = 0;

                    $j = 0;
                    $m = 0;
                    $nilai_draft5 = 0;
                    $nilai_locked5 = 0;
                }
                $unit_id = $rs->getString('old_unit_id');
                $unit_name = $rs->getString('old_unit_name');
                $unit_id_arr[$i] = $unit_id;

                $nilai_draft+=$nilai_draft3;
                $nilai_locked+=$nilai_locked3;

                $sub_sudah = 'f';

                if ($udah != 't') {
                    $nilai_ubah3_arr[$i - $j] = number_format($nilai_locked3 - $nilai_draft3, 0, ",", ".");
                    $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
                    $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");

                    $kegiatan_ok_arr[$i - $j] = 't';
                    $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
                    $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;

                    $j = 0;
                }

                $nilai_draft3 = 0;
                $nilai_locked3 = 0;
            }
//else
            {
                if ($oldsub != $rs->getString('subtitle') and $m >= 1) {
                    $oldsub = $rs->getString('subtitle');
//$subtitle_arr[$i-$m]=$rs->getString('subtitle');
                    $sub_ok_arr[$i - $m] = 't';

                    $nilai_draft5_arr[$i - $m] = number_format(0 + $nilai_draft5, 0, ",", ".");
                    $nilai_locked5_arr[$i - $m] = number_format(0 + $nilai_locked5, 0, ",", ".");
                    $nilai_ubah5_arr[$i - $m] = number_format(0 + $nilai_locked5 - $nilai_draft5, 0, ",", ".");

                    $nilai_draft5 = 0;
                    $nilai_locked5 = 0;
                    $sub_sudah = 'f';
                    $m = 0;
                }
            }

            $nilai_ubah4_arr[$i] = number_format($rs->getString('nilai_locked4') - $nilai_draft4_q8, 0, ",", ".");

            $kode_kegiatan = $rs->getString('old_kode_kegiatan');
            $nama_kegiatan = $rs->getString('old_nama_kegiatan');

            $nilai_draft3+=$nilai_draft4_arr[$i];
//$nilai_locked3+=$nilai_locked4_arr[$i];
            $nilai_locked3+=$rs->getString('nilai_locked4');

            $nilai_draft5+=($nilai_draft4_arr[$i] + 0);
//$nilai_locked5+=($nilai_locked4_arr[$i]+0);
            $nilai_locked5+=($rs->getString('nilai_locked4') + 0);

            $nilai_draft4_arr[$i] = number_format($nilai_draft4_arr[$i], 0, ",", ".");

            $nilai_locked4_arr[$i] = number_format($rs->getString('nilai_locked4'), 0, ",", ".");

            $i+=1;
            $j+=1;
            $k+=1;
            $m+=1;
        }
        $nilai_draft+=$nilai_draft3;
        $nilai_locked+=$nilai_locked3;

        $nilai_ubah3_arr[$i - $j] = number_format($nilai_locked3 - $nilai_draft3, 0, ",", ".");
        $nilai_draft3_arr[$i - $j] = number_format($nilai_draft3, 0, ",", ".");
        $nilai_locked3_arr[$i - $j] = number_format($nilai_locked3, 0, ",", ".");

        $kegiatan_ok_arr[$i - $j] = 't';
        $kode_kegiatan_arr[$i - $j] = $kode_kegiatan;
        $nama_kegiatan_arr[$i - $j] = $nama_kegiatan;

        $nilai_draft2+=$nilai_draft;
        $nilai_locked2+=$nilai_locked;

        $nilai_draft_arr[$i - $k] = number_format($nilai_draft, 0, ",", ".");
        $nilai_locked_arr[$i - $k] = number_format($nilai_locked, 0, ",", ".");

        $dinas_ok_arr[$i - $k] = 't';
        $unit_id_arr[$i - $k] = $unit_id;
        $unit_name_arr[$i - $k] = $unit_name;

//$subtitle_arr[$i-$m]=$subtitle;
        $sub_ok_arr[$i - $m] = 't';

        $nilai_ubah5_arr[$i - $m] = number_format(0 + $nilai_locked5 - $nilai_draft5, 0, ",", ".");
        $nilai_draft5_arr[$i - $m] = number_format(0 + $nilai_draft5, 0, ",", ".");
        $nilai_locked5_arr[$i - $m] = number_format(0 + $nilai_locked5, 0, ",", ".");

        $nilai_draft2 = number_format($nilai_draft2, 0, ",", ".");
        $nilai_locked2 = number_format($nilai_locked2, 0, ",", ".");
//mumet
//var_dump($kode_rekening_arr);

        $this->nilai_draft2 = $nilai_draft2;
        $this->nilai_locked2 = $nilai_locked2;
        $this->nilai_draft = $nilai_draft_arr;
        $this->nilai_locked = $nilai_locked_arr;
        $this->target = $target_arr;
        $this->dinas_ok = $dinas_ok_arr;
        $this->kegiatan_ok = $kegiatan_ok_arr;
        $this->subtitle = $subtitle_arr;
        $this->kode_kegiatan = $kode_kegiatan_arr;
        $this->nama_kegiatan = $nama_kegiatan_arr;
        $this->nilai_draft3 = $nilai_draft3_arr;
        $this->nilai_locked3 = $nilai_locked3_arr;
        $this->nilai_draft4 = $nilai_draft4_arr;
        $this->nilai_locked4 = $nilai_locked4_arr;
        $this->kode_rekening = $kode_rekening_arr;
        $this->nama_rekening = $nama_rekening_arr;

        $this->total_keg_semula = $total_keg_semula_arr;
        $this->total_keg_menjadi = $total_keg_menjadi_arr;
        $this->total_keg_ubah = $total_keg_ubah_arr;
        $this->total_unit_semula = $total_unit_semula_arr;
        $this->total_unit_menjadi = $total_unit_menjadi_arr;
        $this->total_unit_ubah = $total_unit_ubah_arr;
        $this->nilai_ubah_arr = $nilai_ubah_arr;
        $this->unit_id = $unit_id_arr;
        $this->unit_name = $unit_name_arr;
        $this->sub_ok = $sub_ok_arr;
        $this->nilai_ubah3 = $nilai_ubah3_arr;
        $this->nilai_ubah4 = $nilai_ubah4_arr;
        $this->nilai_draft5 = $nilai_draft5_arr;
        $this->nilai_locked5 = $nilai_locked5_arr;
        $this->nilai_ubah5 = $nilai_ubah5_arr;
    }

    //irul 13 maret 2014 - laporan per rekening

    public function executeEdit() {
        if ($this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(MurniBukuPutihSubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(MurniBukuPutihSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->addAscendingOrderByColumn(MurniBukuPutihSubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelect($c);

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(MurniBukuPutihSubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = MurniBukuPutihSubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }

            $query = "select *
                from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus = false order by sub, rekening_code, komponen_name";

            $con = Propel::getConnection(MurniBukuPutihRincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;

            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(MurniBukuPutihRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
            $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KECAMATAN);
            $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::LOKASI_KELURAHAN);
            $c_rincianDetail->addAscendingOrderByColumn(MurniBukuPutihRincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = MurniBukuPutihRincianDetailPeer::doSelect($c_rincianDetail);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }

        if ($this->getRequestParameter('act') == 'editHeader') {
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');

            $c = new Criteria();
            //            $c->addSelectColumn('note_skpd');
            $c->add(MurniBukuPutihRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(MurniBukuPutihRincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $c->add(MurniBukuPutihRincianDetailPeer::DETAIL_NO, $this->id);
            $notes = MurniBukuPutihRincianDetailPeer::doSelectOne($c);
            $note = $notes->getNotePeneliti();
            //            var_dump($note);
            $this->note_peneliti = $note;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $catatan = $this->getRequestParameter('catatan');
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');
//                 echo $this->unit_id.'.'.$catatan;
            $c = new Criteria();
            $c->add(MurniBukuPutihRincianDetailPeer::UNIT_ID, $this->unit_id);
            $c->add(MurniBukuPutihRincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(MurniBukuPutihRincianDetailPeer::DETAIL_NO, $this->id);
            $rincian_detail = MurniBukuPutihRincianDetailPeer::doSelectOne($c);

            if ($rincian_detail) {
                $rincian_detail->setNotePeneliti($catatan);
                $rincian_detail->save();
//                   return $this->redirect('peneliti/edit?unit_id='.$rincian_detail->getUnitId().'&kode_kegiatan='.$rincian_detail->getKegiatanCode());
            }
        }
    }

    public function executeGetPekerjaansMasalah() {
        if ($this->getRequestParameter('id') == '0') {
            $sub_id = $this->getRequestParameter('id');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');

            $query = "select *
                from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah
                where unit_id = '$unit_id' and kegiatan_code = '$kegiatan_code' and tahun = '" . sfConfig::get('app_tahun_default') . "' order by sub, kode_sub, rekening_code, komponen_name";
            //print($query);exit;
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();

            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    //irul 19september-2014 banding buku putih

    public function executeTampillaporanBukuPutih() {
        if ($this->getRequestParameter('unit_id')) {
            $this->executeBandingBukuPutih();
            $this->setTemplate('bandingBukuPutih');
        }
    }

    public function executeBandingBukuPutih() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'X.XX') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }

            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }

            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program kp
				where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
				where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
				from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
				where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

            $query = "select sum(nilai_anggaran) as nilai from " .
                    sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
						where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }
        }
    }

    //irul 19september-2014 banding buku putih
//    ticket # - urgent list ssh
    public function executeSshlocked() {
        $this->processFilterssshlocked();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sshlocked/filters');

        $pagers = new sfPropelPager('Komponen', 50);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD', Criteria::EQUAL);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addFiltersCriteriasshlocked($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterssshlocked() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sshlocked/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sshlocked/filters');
        }
    }

    protected function addFiltersCriteriasshlocked($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['komponen_name_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['rekening_code_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::REKENING, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::REKENING, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::REKENING, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    public function executeTemplateRKA() {
//$this->setTemplate('templateRKA');
        $this->setLayout('kosong');
        $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
    }
}
