<td>
    <ul class="sf_admin_td_actions">
        <?php echo '<li>' . link_to(image_tag('/sf/sf_admin/images/list.png', array('alt' => __('Melihat Hasil Rincian'), 'title' => __('Melihat Hasil Rincian'))), 'dewan/printsubtitlerekening?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId(), array('class' => 'btn btn-default btn-flat')) . '</li>'; ?>
        <?php
        $query = "select * from " . sfConfig::get('app_default_schema') . ".seting_aplikasi "
                . "where id = 1";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            if ($t->getString('status_rincian_dewan') == 1) {
                echo '<li>' . link_to(image_tag('/sf/sf_admin/images/edit_icon.png', array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'))), 'dewan/edit?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId(), array('class' => 'btn btn-default btn-flat')) . '</li>';
            }
        }
        ?>

<!--echo link_to('<i class="fa fa-search"></i> Cek', 'usulan_ssh/usulansshcek?id=' . $usulan->getIdUsulan() . '&key=' . md5('cek_ssh'), array('class' => 'btn btn-default btn-flat'));-->
    </ul>
</td>