<div class="box box-primary box-solid">        
    <div class="box-header with-border">
        Laporan Per Belanja
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode Belanja</th>
                <th class="text-center">Nama Belanja</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            $rs->first();
            do {
                ?>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-center text-bold"><?php echo isset($belanja_id[$i]) ? $belanja_id[$i] : '' ?></td>
                    <td class="text-left text-bold"> <?php echo isset($belanja_name[$i]) ? $belanja_name[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($nilai_draft[$i]) ? $nilai_draft[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($persen_draft[$i]) ? $persen_draft[$i] : '' ?>%</td>
                    <td class="text-right"><?php echo isset($nilai_locked[$i]) ? $nilai_locked[$i] : '' ?></td>
                    <td class="text-right"><?php echo isset($persen_locked[$i]) ? $persen_locked[$i] : '' ?>%</td>
                </tr>
                <?php
                $i++;
            } while ($rs->next())
            ?>      
            <tr>
                <td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="text-center text-bold">Total</td>
                <td class="text-right text-bold"><?php echo $nilai_draft2; ?></td>
                <td class="text-right text-bold">100%</td>
                <td class="text-right text-bold"><?php echo $nilai_locked2; ?></td>
                <td class="text-right text-bold">100%</td>
            </tr>
        </table>       
    </div>
</div>



