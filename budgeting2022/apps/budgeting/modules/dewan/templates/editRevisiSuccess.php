    <?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$sub_id = 1;
$cetak = FALSE;
$kegiatan_code = $sf_params->get('kode_kegiatan');

$status = 'OPEN';
$sf_user->removeCredential('status_dinas');
$sf_user->addCredential('status_dinas');
$sf_user->setAttribute('status', $status, 'status_dinas');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Subtitle Kegiatan SKPD <?php echo $kode_kegiatan; ?></h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('dewan/list_messages'); ?>

    <?php
    if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
        echo "<div class='box box-info'>";
        echo "<div class='box-body' style='color:#FF0000'>";

        $query_warning_per_rekening = "( 
                        select semula.rekening_code, semula.total as prev_total,  rd.total as exist_total  
                        from 
                        ( 
                        select rekening_code, sum(nilai_anggaran) as total 
                        from " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) semula, 
                                ( 
                                select rekening_code, sum(nilai_anggaran) as total 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) rd  
                                where semula.rekening_code = rd.rekening_code and semula.total <> rd.total 
                                order by semula.rekening_code 
                                ) union all ( 
                                select semula.rekening_code, semula.total as prev_total,  0 as exist_total 
                                from 
                                ( 
                                select rekening_code, sum(nilai_anggaran) as total 
                                from " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) semula 
                                where semula.rekening_code not in 
                                ( select rekening_code  
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code ) 
                                ) union all ( 
                                select rd.rekening_code, 0 as prev_total,  rd.total as exist_total 
                                from 
                                ( 
                                select rekening_code, sum(nilai_anggaran) as total 
                                from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code 
                                ) rd where rd.rekening_code not in 
                                ( select rekening_code  
                                from " . sfConfig::get('app_default_schema') . ".rincian_detail  "
                . "where status_hapus = false and unit_id = '$unit_id' and kegiatan_code ilike '$kode_kegiatan' 
                                group by rekening_code 
                                order by rekening_code ) 
                                )";
        $munculNotif = 0;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query_warning_per_rekening);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            $rek_lama = $t->getString('rekening_code');
            $tot_lama = number_format($t->getString('prev_total'), 0, ',', '.');
            $tot_baru = number_format($t->getString('exist_total'), 0, ',', '.');
            $beda = $t->getString('prev_total') - $t->getString('exist_total');
            $selisih = number_format($beda, 0, ',', '.');
            if ($selisih <> 0) {
                if ($munculNotif == 0) {
                    echo "Terdapat perubahan total nilai per rekening dari Previous : <br>";
                }
                $munculNotif = 1;
            }
            echo "Kode Rekening : $rek_lama   Selisih=$selisih [ Semula=$tot_lama, Menjadi=$tot_baru ] <br>";
        }
        echo "</div>";
        echo "</div>";

        $con = Propel::getConnection();
        $query = "SELECT kb.belanja_name, sum(d2.nilai_anggaran) AS nilai_draft, 
                    (SELECT sum(rd.nilai_anggaran) FROM " . sfConfig::get('app_default_schema') . ".master_kegiatan k1, " . sfConfig::get('app_default_schema') . ".rincian r1, unit_kerja u1, " . sfConfig::get('app_default_schema') . ".rekening rek1, " . sfConfig::get('app_default_schema') . ".rincian_detail rd, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb1 
                        WHERE k1.kode_kegiatan = r1.kegiatan_code AND k1.unit_id = u1.unit_id AND k1.unit_id = r1.unit_id AND rd.unit_id = r1.unit_id AND rd.kegiatan_code = r1.kegiatan_code AND rd.rekening_code = rek1.rekening_code AND rek1.belanja_id = kb1.belanja_id AND rd.status_hapus = false AND rd.unit_id = k.unit_id AND rd.kegiatan_code = k.kode_kegiatan AND kb1.belanja_id = kb.belanja_id GROUP BY k1.unit_id, u1.unit_name, u1.kelompok_id, k1.kode_kegiatan, k1.tahap, k1.nama_kegiatan, kb1.belanja_id, kb1.belanja_name, kb1.belanja_urutan
                    ) AS nilai_prev
            FROM " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan k, " . sfConfig::get('app_default_schema') . ".dinas_rincian r, unit_kerja u, " . sfConfig::get('app_default_schema') . ".rekening rek, " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail d2, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
            WHERE k.unit_id = '" . $unit_id . "' AND k.kode_kegiatan = '" . $kode_kegiatan . "' AND k.kode_kegiatan = r.kegiatan_code AND k.unit_id = u.unit_id AND k.unit_id = r.unit_id AND d2.unit_id = r.unit_id AND d2.kegiatan_code = r.kegiatan_code AND d2.rekening_code = rek.rekening_code AND rek.belanja_id = kb.belanja_id AND d2.status_hapus = false
            GROUP BY k.unit_id, u.unit_name, u.kelompok_id, k.kode_kegiatan, k.tahap, k.nama_kegiatan, kb.belanja_id, kb.belanja_name, kb.belanja_urutan";
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $hasil = '';
        while ($rs->next()) {
            $selisih_belanja = $rs->getInt('nilai_prev') - $rs->getInt('nilai_draft');
            if ($selisih_belanja != 0)
                $hasil .= $rs->getString('belanja_name') . " => Selisih=" . number_format($selisih_belanja, 0, ',', '.') . " [Semula=" . number_format($rs->getInt('nilai_prev'), 0, ',', '.') . ", Menjadi=" . number_format($rs->getInt('nilai_draft'), 0, ',', '.') . "]<br>";
        }
        if ($hasil != '') {
            ?>
            <div class='alert alert-danger alert-dismissable'>
                Terdapat perubahan total nilai per belanja dari Previous : <br>
                <?php echo $hasil; ?>
            </div>
            <?php
        }
    }
    ?>
    <div class="box box-body text-center box-solid box-primary" id="headerrka_<?php echo $sub_id ?>">
        <div class="btn-group btn-group-justified" role="group">
            <?php
            echo link_to_function('<span>' . image_tag('down.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)) . '</span> Melihat Header Kegiatan', 'inKeterangan(' . $sub_id . ',"' . $kode_kegiatan . '","' . $unit_id . '")', array('class' => 'btn btn-flat btn-default text-bold col-xs-3'));
            if ($tahap != '') {
                echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Subtitle', 'report/TemplateRKA?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id . '&tahap=' . $tahap, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Rekening', 'report/printrekening?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id. '&tahap=' . $tahap, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                echo link_to(image_tag('b_print.png') . ' Format RKA Tanpa Komponen', 'report/printrekeningtanpakomponen?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id. '&tahap=' . $tahap, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
            } else {
                echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Subtitle', 'report/TemplateRKApptk?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                echo link_to(image_tag('b_print.png') . ' Format RKA Berdasarkan Rekening', 'report/printrekeningpptk?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
                echo link_to(image_tag('b_print.png') . ' Format RKA Tanpa Komponen', 'report/printrekeningtanpakomponen?kode_kegiatan=' . $kode_kegiatan . '&unit_id=' . $unit_id, array('class' => 'btn btn-flat btn-default text-bold col-xs-4', 'target' => '_blank'));
            }
            ?>
        </div>
        <div class="clearfix"></div>
    </div><!-- /.box-body -->
    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container">
                <div id="sf_admin_content" class="table-responsive">
                    <?php if (!$rs_subtitle): ?>
                        <?php echo __('Tidak Ada Subtitle untuk SKPD dengan Kegiatan ini') ?>
                    <?php else: ?>
                        <table cellspacing="0" class="sf_admin_list">
                            <thead>  
                                <tr>
                                    <th><b>Subtitle | Rekening</b></th>
                                    <th><b>Kode Akrual</b></th>
                                    <th><b>Komponen</b></th>
                                    <th><b>Satuan</b></th>
                                    <th><b>Koefisien</b></th>
                                    <th><b>Harga</b></th>
                                    <th><b>Hasil</b></th>
                                    <th><b>PPN</b></th>
                                    <th><b>Total</b></th>
                                    <th><b>Belanja</b></th>
                                    <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
                                    <th><b>Catatan Tim Anggaran</b></th>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($rs_subtitle as $subtitle_indikator):
                                    ?>
                                    <?php
                                    $unit_id = $subtitle_indikator->getUnitId();
                                    $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                    $subtitle = $subtitle_indikator->getSubtitle();
                                    $sub_id = $subtitle_indikator->getSubId();
                                    $nama_subtitle = trim($subtitle);
                                    $odd = fmod($i++, 2);

                                    //////////////////////////////////////////////////////////////////////////////////
                                    //* untuk memberi warning jika komponen ada catatan penyelia dan SKPD
                                    $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail 
                                        where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' and subtitle ilike '%$nama_subtitle%' and status_hapus = false ";
                                    //print_r($query);exit;
                                    //diambil nilai terpakai
                                    $con = Propel::getConnection();
                                    $statement = $con->prepareStatement($query);
                                    $rs = $statement->executeQuery();
                                    $jml = $rs->getRecordCount();

//                                    $query2 = "select a.unit_id, a.kegiatan_code, a.note_skpd from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail a, " . sfConfig::get('app_default_schema') . ".rincian_detail b
//                                        where a.unit_id=b.unit_id and a.kegiatan_code=b.kegiatan_code and a.detail_no=b.detail_no and
//                                        a.nilai_anggaran<>b.nilai_anggaran and a.unit_id = '$unit_id' and a.kegiatan_code = '$kode_kegiatan' and a.subtitle ilike '%" . str_replace('\'', '%', $nama_subtitle) . "' and a.status_hapus = false
//                                        limit 1";
//                                    $statement = $con->prepareStatement($query2);
//                                    $rs = $statement->executeQuery();
//                                    if ($jml == 0)
//                                        $jml = $rs->getRecordCount();
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    if ($jml > 0) {
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>" id="subtitle_<?php print $sub_id ?>">
                                            <td style="background: pink" colspan="8">
                                                <b>
                                                    <i>
                                                        <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                    </i>
                                                </b>&nbsp;


                                                <div id="<?php echo 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId() ?>">
                                                    <?php
                                                    //ARG
                                                    if ($subtitle_indikator->getLakilaki() != 0 || $subtitle_indikator->getPerempuan() != 0 || $subtitle_indikator->getDewasa() != 0 || $subtitle_indikator->getAnak() != 0 || $subtitle_indikator->getLansia() != 0 || $subtitle_indikator->getInklusi() != 0) {
                                                        if ($subtitle_indikator->getCatatan() != '')
                                                            echo 'Catatan ARG :' . $subtitle_indikator->getCatatan();
                                                        echo '<br>';

                                                        echo link_to_remote(submit_tag('Catatan ARG'), array('alt' => 'Mengubah Catatan gender (ARG) ',
                                                            'title' => 'Mengubah Catatan gender (ARG)', 'update' => 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId(),
                                                            'url' => 'dewan/catatanGender?act=edit&sub_id=' . $subtitle_indikator->getSubId() . '&catatanSubtitle=' . $subtitle_indikator->getCatatan() . ' &unit_id=' . $subtitle_indikator->getUnitId() . '&kode_kegiatan=' . $subtitle_indikator->getKegiatanCode(),
                                                            'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')"));
                                                    }
                                                    ?>
                                                </div>
                                            </td>

                                            <td style="background: pink" align="right">
                                                <?php
                                                $query2 = "select sum(nilai_anggaran) as hasil_kali
                                            from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                            where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query2);
                                                $t = $stmt->executeQuery();
                                                while ($t->next()) {
                                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                }
                                                ?></td>
                                            <td style="background: pink" colspan="3">&nbsp;</td>
                                        </tr>

                                    <?php } else {
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>" id="subtitle_<?php print $sub_id ?>">
                                            <td colspan="8"><b><i>

                                                        <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                    </i></b>&nbsp


                                                <div id="<?php echo 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId() ?>">
                                                    <?php
                                                    //ARG
                                                    if ($subtitle_indikator->getLakilaki() != 0 || $subtitle_indikator->getPerempuan() != 0 || $subtitle_indikator->getDewasa() != 0 || $subtitle_indikator->getAnak() != 0 || $subtitle_indikator->getLansia() != 0 || $subtitle_indikator->getInklusi() != 0) {
                                                        if ($subtitle_indikator->getCatatan() != '')
                                                            echo 'Catatan ARG :' . $subtitle_indikator->getCatatan();
                                                        echo '<br>';

                                                        echo link_to_remote(submit_tag('Catatan ARG'), array('alt' => 'Mengubah Catatan gender (ARG) ',
                                                            'title' => 'Mengubah Catatan gender (ARG)', 'update' => 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId(),
                                                            'url' => 'dewan/catatanGender?act=edit&sub_id=' . $subtitle_indikator->getSubId() . '&catatanSubtitle=' . $subtitle_indikator->getCatatan() . ' &unit_id=' . $subtitle_indikator->getUnitId() . '&kode_kegiatan=' . $subtitle_indikator->getKegiatanCode(),
                                                            'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')"));
                                                    }
                                                    ?>
                                                </div>
                                            </td>

                                            <td align="right">
                                                <?php
                                                $query2 = "select sum(nilai_anggaran) as hasil_kali
                                        from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
                                        where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query2);
                                                $t = $stmt->executeQuery();
                                                while ($t->next()) {
                                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                }
                                                ?></td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr> 
                                    <?php } ?>
                                    <tr id="indicator_<?php echo $sub_id ?>" style="display:none;" align="center"><td colspan="11">
                                <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                                </td></tr>
                            <?php endforeach; ?>
                            <?php $sub_id = 0 ?>
                            <tr class="sf_admin_row_7" id="subtitle_<?php print $sub_id ?>">
                                <td colspan="8"><b><i>

                                            <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatanMasalah(' . $sub_id . ',"' . $kode_kegiatan . '","' . $unit_id . '")') . ' Komponen - komponen yang tidak masuk RKA'; ?></i></b></td>

                                <td align="right">
                                    <?php
                                    //query asli bisma
                                    //$query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
                                    //widhie:untuk pembulatan komponen
                                    $query2 = "select sum(nilai_anggaran) as hasil_kali
			from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah
			where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id'";
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($query2);
                                    $t = $stmt->executeQuery();
                                    while ($t->next()) {
                                        echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                    }
                                    ?></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr id="indicator_<?php echo $sub_id ?>" style="display:none;" align="center"><td colspan="11">
                            <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                            </td></tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8" align="right">Total Keseluruhan:</td>
                                    <td align="right">
                                        <?php
                                        $unit_id = $subtitle_indikator->getUnitId();
                                        $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                        $subtitle = $subtitle_indikator->getSubtitle();
                                        $nama_subtitle = trim($subtitle);

                                        //query aslii bisma
                                        //$query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
                                        //widhie : untuk pembulatan komponen
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
				from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail
				where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                        }
                                        ?></td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            </tfoot>
                        </table>
                    <?php endif; ?>

                </div>

                <div id="sf_admin_footer">
                </div>

            </div>
        </div>
    </div>

</section><!-- /.content -->

<script>
    image1 = new Image();
    image1.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';

    image2 = new Image();
    image2.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';

    function showHideKegiatan(id) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }

        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getPekerjaansRevisi/id/" + id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator_' + id).remove();
                    $('#subtitle_' + id).after(msg);
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function showHideKegiatanMasalah(id, kegiatan, unit) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getPekerjaansMasalah/id/" + id + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator_' + id).remove();
                    $('#subtitle_0').after(msg);
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function hapusHeaderKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus Header Sub Subtitle kegiatan ini??');
        if (a === true)
        {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/hapusHeaderPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html"
            }).done();

        }
    }

    function editHeaderKegiatan(id, kegiatan, unit, kodesub) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/ajaxEditHeader/act/editHeader/kodeSub/" + kodesub + "/unitId/" + unit + "/kegiatanCode/" + kegiatan + ".html",
            context: document.body
        }).done(function (msg) {
            $('#header_' + kodesub).before(msg);
            $('#header_' + kodesub).remove();
        });
    }

    function hapusKegiatan(id, kegiatan, unit, no) {
        var a = confirm('Apakah anda yakin akan menghapus kegiatan ini??');
        if (a === true) {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/hapusPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + no + ".html"
            }).done();
        }
    }

    function editSubtitle(id) {
        var loading = $('#indicator_' + id);
        loading.show();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getPekerjaans/id/" + id + ".html",
            context: document.body
        }).done(function (msg) {
            loading.remove();
        });

    }

    function inKeterangan(id, kegiatan, unit) {
        var row = $('headerrka_' + id);
        var img = $('#img_' + id);
        var pekerjaans = $('#depan_' + id);
        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/down.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'headerrka_' + id;
            var pekerjaans = $('#depan_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getHeader/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + ".html",
                    context: document.body
                }).done(function (msg) {
                    $('#indicator').remove();
                    $("#headerrka_" + id).after(msg);
                });
            }
        } else {
            $('#depan_' + id).remove();
        }
    }

    function hapusSubKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus SUB kegiatan ini??');
        if (a === true) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            for (var i = 0; i < pekerjaans.length; i++) {
                var pekerjaan = pekerjaans[i];
                pekerjaan.style.display = 'none';
            }
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/hapusSubPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html",
                context: document.body
            }).done();
        }
    }
</script>