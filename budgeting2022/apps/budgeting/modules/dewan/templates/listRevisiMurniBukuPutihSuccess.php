<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php use_helper('I18N', 'Date', 'Validation', 'Object', 'Javascript') ?>

<section class="content-header">
    <?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        $nama_sistem = 'Pra-RKA';
    } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
        $nama_sistem = 'PAK';
    } else {
        $nama_sistem = 'Revisi';
    }
    ?>
    <h1><?php echo $nama_sistem ?> Daftar Kegiatan SKPD</h1>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border" style="background-color: #e4ed6d; color:black;">
            <h3 class="box-title">Filters</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_tag('dewan/listRevisiMurniBukuPutih', array('method' => 'get', 'class' => 'form-horizontal')) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Satuan Kerja</label>
                <div class="col-sm-10">
                    <?php
                    $e = new Criteria();
                    $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
                    $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                    $v = UnitKerjaPeer::doSelect($e);

                    echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Dinas------')), array('class' => 'form-control'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nama Kegiatan</label>
                <div class="col-sm-10">
                    <?php
                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control', 'placeholder' => 'Nama Kegiatan')
                    );
                    ?>
                </div>
            </div>
            <div id="sf_admin_container">
                <ul class="sf_admin_actions">
                    <li><?php echo button_to(__('reset'), 'dewan/listRevisiMurniBukuPutih?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
                    <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>
                </ul>
            </div>
<?php echo '</form>'; ?>
        </div>
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container">
                <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                <?php else: ?>
                    <?php include_partial('dewan/list_revisi_murni_bukuputih', array('pager' => $pager)) ?>
                <?php endif; ?>
            </div>
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->