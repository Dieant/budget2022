<?php use_helper('Javascript', 'Number') ?>
<?php ?>
<div style="font-family: Verdana">
    <?php
    $kode_kegiatan = $sf_params->get('kode_kegiatan');
    $unit_id = $sf_params->get('unit_id');

    $query = "select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program, 
	mk.kode_program2, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan, 
	mk.outcome, mk.target_outcome, mk.catatan, mk.ranking,uk.unit_name, mk.kode_urusan, 
	uk.kode_permen, uk.unit_address, mk.masukan, mk.output, mk.kelompok_sasaran
	from " . sfConfig::get('app_default_schema') . ".dewan_master_kegiatan mk,unit_kerja uk
	where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "'
	and uk.unit_id=mk.unit_id";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    $ranking = '';
    while ($rs->next()) {
        $kegiatan_kode = $rs->getString('kode_kegiatan');
        $nama_kegiatan = $rs->getString('nama_kegiatan');
        $alokasi = $rs->getString('alokasi_dana');
        $organisasi = $rs->getString('unit_name');
        $benefit = $rs->getString('benefit');
        $impact = $rs->getString('impact');
        $kode_program = $rs->getString('kode_program');
        $program_p_13 = $rs->getString('kode_program2');
        $kode_misi = $rs->getString('kode_misi');
        $kode_tujuan = $rs->getString('kode_tujuan');
        $outcome = $rs->getString('outcome');
        $masukan = $rs->getString('masukan');
        $output = $rs->getString('output');
        $target_outcome = $rs->getString('target_outcome');
        $catatan = $rs->getString('catatan');
        $ranking = $rs->getInt('ranking');
        $kode_organisasi = $rs->getString('kode_urusan');
        $kode_program2 = $rs->getString('kode_program2');
        $kode_permen = $rs->getString('kode_permen');
        $lokasi = $rs->getString('unit_address');
        $kelompok_sasaran = $rs->getString('kelompok_sasaran');
        $kode_per = explode(".", $kode_organisasi);

        $kode_program_dpa = '';

        if (substr($kode_program2, 0, 4) == 'x.xx') {
            $kode_program_dpa = substr($rs->getString('kode_urusan'), 0, 4);
        } else {
            $kode_program_dpa = $kode_program2;
        }

        $cp = new Criteria();
        $cp->add(MasterProgram2Peer::KODE_PROGRAM2, $kode_program2);
        $program2 = MasterProgram2Peer::doSelectOne($cp);

        $query = "select * from " . sfConfig::get('app_default_schema') . ".master_program2 ms "
                . "where ms.kode_program2='" . $kode_program2 . "'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs1 = $stmt->executeQuery();
        while ($rs1->next()) {
            $nama_program2 = $rs1->getString('nama_program2');
        }

        $kode_program22 = substr($kode_program2, 5, 2);
        if (substr($kode_program2, 0, 4) == 'x.xx') {
            $kode_urusan = substr($kode_per[0] . '.' . $kode_per[1], 0, 4);
        } else {
            $kode_urusan = substr($kode_program2, 0, 4);
        }
    }

    $total = 0;
    $query2 = "select sum(nilai_anggaran) as hasil
	from " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail rd
	where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";
    $stmt5 = $con->prepareStatement($query2);
    $total_rekening = $stmt5->executeQuery();
    while ($total_rekening->next()) {
        $total = $total_rekening->getString('hasil');
    }

    $cp->clear();
    $cp->add(UnitKerjaPeer::UNIT_ID, $unit_id);
    $pengguna_anggaran = UnitKerjaPeer::doSelectOne($cp);

    $temp_kode1 = explode('.', $kode_program2);
    $no_dppa_skpd = substr($kegiatan_kode, 0, 4) . '.' . $kode_permen . '.' . substr($kegiatan_kode, 5, 2) . '.' . substr($kegiatan_kode, 8);
    ?>
    <body>
        <table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" width="100%">
            <tbody>
                <tr><td colspan="5" align="center">
                        <?php echo image_tag('logosby.png', array('width' => '100')) ?><h5 style="margin-top: 10px">PEMERINTAH KOTA SURABAYA</h5>
                        <h4 style="margin: 25px 0px 20px 0px">RENCANA KERJA DAN ANGGARAN</h4>
                        <h4 style="margin: 0px 0px 20px 0px">SATUAN KERJA PERANGKAT DAERAH ( RKA - SKPD )</h4>
                        <h4 style="margin: 0px 0px 40px 0px">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></h4>
                        <h2 style="margin: 0px 0px 15px 0px">BELANJA LANGSUNG</h2>
                        <h5 style="margin: 0px 0px 25px 0px">NO RKA SKPD : <?php echo $no_dppa_skpd ?></h5>
                    </td></tr>
                <tr>
                    <td align="center" colspan="5">
                        <table align="center" style="font-size: smaller; font-weight: bold;" cellspacing="10px">
                            <tr>
                                <?php
                                $temp_ambil_urusan = explode(".", $kode_permen);
                                $kode_ambil_urusan = $temp_ambil_urusan[0] . '.' . $temp_ambil_urusan[1];
                                $c = new Criteria();
                                //$c->add(MasterUrusanPeer::KODE_URUSAN,$kode_per[0].'.'.$kode_per[1]);
                                $c->add(MasterUrusanPeer::KODE_URUSAN, $kode_ambil_urusan);
                                $urusan = MasterUrusanPeer::doSelectOne($c);
                                if ($kode_ambil_urusan == '1.20')
                                    $urusan_pasti = 'Pemerintahan Umum';
                                ?>
                                <td>URUSAN PEMERINTAHAN </td><td>:</td><td> <?php
                                    echo '( ' . $kode_ambil_urusan . ' ) ';
                                    //echo ($kode_ambil_urusan == '1.20') ? $urusan_pasti : $urusan->getNamaUrusan()
                                    ?> </td>
                            </tr>
                            <tr>
                                <td>ORGANISASI </td><td>:</td><td> <?php echo '( ' . $kode_permen . ' ) ' . $organisasi ?></td>
                            </tr>
                            <tr>
                                <td>PROGRAM </td><td>:</td><td> <?php echo '( ' . $kode_program_dpa . ' ) ' . $program2->getNamaProgram2() ?></td>
                            </tr>

                            <tr>
                                <td>KEGIATAN </td><td>:</td><td> <?php echo '( ' . substr($kegiatan_kode, 0, 4) . '.' . $kode_permen . '.' . substr($kegiatan_kode, 5, 2) . '.' . substr($kegiatan_kode, 8) . ') ' . $nama_kegiatan ?></td>
                            </tr>
    <!--			<tr>
                            <td>LOKASI KEGIATAN </td><td>: <?php echo $lokasi ?></td>
                            </tr>-->
                            <!--<tr>
                            <td>SUMBER DANA </td><td>: <?php echo '' ?></td>
                            </tr>-->
                            <tr>
                                <td>JUMLAH ANGGARAN </td><td>:</td><td> Rp. <?php echo number_format($total, 0, ',', '.') ?></td>
                            </tr>
                            <tr>
                                <td>TERBILANG </td><td>:</td><td> ( <?php echo ucwords(myTools::terbilang(round($total))) ?> Rupiah )</td>
                            </tr>
                            <tr>
                                <td>PENGGUNA ANGGARAN/<br>KUASA PENGGUNA ANGGARAN </td><td></td><td> </td>
                            </tr>
                            <tr>
                                <td>NAMA </td><td>:</td><td> <?php echo $pengguna_anggaran->getKepalaNama() ?></td>
                            </tr>
                            <tr>
                                <td>NIP </td><td>:</td><td> <?php echo $pengguna_anggaran->getKepalaNip() ?></td>
                            </tr>
                            <tr>
                                <td>JABATAN </td><td>:</td><td> <?php echo $pengguna_anggaran->getKepalaPangkat() ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>

                    <td colspan="3" style="border-top: 1px solid; border-left: 1px solid;"><div align="center"><span class="style1 font12"><strong><span class="style3">RENCANA KERJA DAN ANGGARAN</span><br>
                                </strong></span>        
                        </div>        <div align="center"><span class="font12 style1 style2"><span class="font12 style3 style1"><strong>SATUAN KERJA PERANGKAT DAERAH </strong></span></span></div></td>
                    <td style="border-top: 1px solid; border-left: 1px solid; border-right: 1px solid"><div class="style3" align="center"><strong>NOMOR RKA SKPD<br>
                                <?php echo $no_dppa_skpd ?> </strong></div></td>
                </tr>
                <tr>

                    <td colspan="5" style="border-top: 1px solid; font-weight: bold; border-left: 1px solid; border-right: 1px solid"><div align="center"><span class="style3">PEMERINTAH KOTA SURABAYA <br>
                            </span></div>        
                        <div align="center"><span class="style3">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></span></div></td>
                </tr>
                <tr>
                    <td colspan="4" style="border: 1px solid rgb(0, 0, 0);">
                        <table border="0" width="100%">
                            <tbody style="font-size: smaller;"><tr align="left" valign="top">
                                    <td nowrap="nowrap" width="194"><span class="style3">Urusan Pemerintahan </span></td>
                                    <td width="10"><span class="style3">:</span></td>
                                    <td width="990"><span class="style3"><?php
                                            echo '( ' . $kode_ambil_urusan . ' ) ';
                                            //echo ($kode_ambil_urusan == '1.20') ? $urusan_pasti : $urusan->getNamaUrusan()
                                            ?></span></td>
                                </tr>
                                <tr align="left" valign="top">
                                    <td><span class="style3">Organisasi</span></td>
                                    <td><span class="style3">:</span></td>
                                    <td><span class="style3">( <?php echo /* $kode_program2 */$kode_permen . ' ) ' . $organisasi ?> </span></td>

                                </tr>

                                <tr align="left" valign="top">
                                    <td><span class="style3">Program</span></td>
                                    <td><span class="style3">:</span></td>
                                    <td><span class="style3"><?php echo '( ' . $kode_program_dpa . ' ) ' . $program2->getNamaProgram2() ?></span></td>

                                </tr>

                                <tr align="left" valign="top">
                                    <td><span class="style3">Kegiatan</span></td>
                                    <td><span class="style3">:</span></td>
                                    <td><span class="Font8v style3">( <?php echo $kode_urusan . '.' . $kode_program22 . '.' . $kegiatan_kode . ' ' ?> )</span><span class="style3"> <?php echo $nama_kegiatan ?></span></td>
                                </tr>

                                <tr align="left" valign="top">
                                    <td><span class="style3">Lokasi Kegiatan</span></td>
                                    <td><span class="style3">:</span></td>

                                    <td><span class="style3"> <?php echo $lokasi; ?>  </span></td>
                                </tr>

                                <tr>
                                    <td>Latar belakang perubahan/dianggarkan dalam perubahan APBD</td>
                                    <td>:</td><td><span class="style3"> &nbsp;  </span></td>
                                </tr>
                            </tbody>
                        </table>
                        <table bgcolor="#333333" border="0" cellpadding="3" cellspacing="1" width="100%" style="font-size: smaller">
                            <tr align='center' valign='top'>
                                <td colspan="3" bgcolor="#FFFFFF"><b>Indikator & Tolak Ukur Kinerja Belanja Langsung</b></td>
                            </tr>
                            <tr align='center' valign='top'>
                                <td bgcolor="#FFFFFF" width="10%"><b>Indikator</b></td>
                                <td bgcolor="#FFFFFF" width="45%"><b>Tolak Ukur Kinerja</b></td>
                                <td bgcolor="#FFFFFF" width="45%"><b>Target Kinerja</b></td>
                            </tr>
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Capaian Program</td>
                                <td bgcolor="#FFFFFF"><?php echo $benefit ?></td>
                                <td bgcolor="#FFFFFF"><?php echo $impact ?></td>
                            </tr> 
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Masukan / Input</td>
                                <td bgcolor="#FFFFFF"><?php echo $masukan ?></td>
                                <td bgcolor="#FFFFFF">Rp. <?php echo number_format($total, 0, ',', '.') ?></td>
                            </tr> 
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Keluaran / Output</td>
                                <?php $tmp_outcome = explode('|', $outcome) ?>
                                <td bgcolor="#FFFFFF"><?php echo $tmp_outcome[0] ?></td>
                                <td bgcolor="#FFFFFF"><?php echo $tmp_outcome[1] . $tmp_outcome[2] ?></td>
                            </tr> 
                            <tr valign='top'>
                                <td align='left' bgcolor="#FFFFFF">Hasil / Outcome</td>
                                <?php
                                $tmp1 = explode('#', $output);
                                $tmp_target_output = array();
                                $tmp_tolok_output = array();
                                for ($i = 0; $i < count($tmp1); $i++) {
                                    $tmp2 = explode('|', $tmp1[$i]);
                                    $tmp_target_output[] = $tmp2[1] . ' ' . $tmp2[2];
                                    $tmp_tolok_output[] = $tmp2[0];
                                }
                                ?>
                                <td bgcolor="#FFFFFF"><?php
                                    foreach ($tmp_tolok_output as $ttl)
                                        echo "- " . $ttl . "<br>";
                                    ?></td>
                                <td bgcolor="#FFFFFF"><?php
                                    foreach ($tmp_target_output as $tto)
                                        echo "- " . $tto . "<br>";
                                    ?></td>
                            </tr> 
                        </table>
                    </td>

                </tr>
                <tr>
                    <td style="font-weight: bold; font-size: smaller; border-left: 1px solid; border-right: 1px solid" colspan="5"><span class="style3">Kelompok Sasaran Kegiatan :  <?php echo $kelompok_sasaran ?></span></td>
                </tr>
                <!--    &&nbsp;-->

                <tr>
                    <td colspan="4" style="border-right: 1px solid; border-left: 1px solid; border-top: 1px solid; font-weight: bold; font-size: smaller"><div align="center"><span class="style3">RINCIAN DOKUMEN PELAKSANAAN ANGGARAN</span></div>
                        <div align="center"><span class="style3">BELANJA LANGSUNG SATUAN KERJA PERANGKAT DAERAH </span></div></td>
                </tr>
            </tbody>
        </table>
        <table bgcolor="#333333" border="0" cellpadding="3" cellspacing="1" width="100%" style="font-size: smaller">        
            <tr >
                <td align="center" bgcolor="#FFFFFF" width="5%" >&nbsp;</td>
                <td align="center" bgcolor="#FFFFFF" width="75%"><span class="style3"><strong>Kode Rekening</strong></span></td>
                <td align="center" bgcolor="#FFFFFF" width="20%"><span class="style3"><strong>Jumlah</strong></span></td>
            </tr>
            <?php
            $query = "select si.subtitle, si.indikator, si.nilai, si.satuan, sum(nilai_anggaran) as total "
                    . "from " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail rd, " . sfConfig::get('app_default_schema') . ".dewan_subtitle_indikator si "
                    . "where rd.unit_id = si.unit_id and rd.kegiatan_code = si.kegiatan_code and rd.subtitle = si.subtitle "
                    . "and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false 
                        group by si.subtitle, si.indikator, si.nilai, si.satuan  
                        order by si.subtitle";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs_subtitle = $stmt->executeQuery();
            while ($rs_subtitle->next()) {
                $subtitle = $rs_subtitle->getString('subtitle');
                ?>
                <tr align='center' bgcolor="white">
                    <td  align='left' colspan="2">
                        <strong>:: <?php echo $rs_subtitle->getString('subtitle') ?></strong><br/>
                        <?php 
//"Penunjang kegiatan"
//"penunjang Kegiatan"
//"penunjang kegiatan"
//"Penunjang Kegiatan"
                            if($subtitle<>'Penunjang kegiatan'&&$subtitle<>'penunjang Kegiatan'&&$subtitle<>'penunjang kegiatan'&&$subtitle<>'Penunjang Kegiatan'){
                            echo 'Target: '.$rs_subtitle->getString('indikator').' '.$rs_subtitle->getString('nilai').' '.$rs_subtitle->getString('satuan');
                        }
                        ?>
                    </td>
                    <td align='right'><strong><?php echo number_format($rs_subtitle->getString('total'), 0, ',', '.') ?></strong></td>
                </tr>
                <?php
                $query = "select rekening.rekening_code, rekening.rekening_name, sum(detail2.nilai_anggaran) as total "
                        . "from "
                        . "(" . sfConfig::get('app_default_schema') . ".dewan_rincian_detail detail2 inner join " . sfConfig::get('app_default_schema') . ".rekening rekening on rekening.rekening_code = detail2.rekening_code) "
                        . "where detail2.kegiatan_code = '" . $kode_kegiatan . "' and detail2.unit_id = '" . $unit_id . "' and detail2.status_hapus=false and detail2.subtitle = '$subtitle' "
                        . "group by rekening.rekening_code, rekening.rekening_name "
                        . "order by rekening.rekening_code ";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $rs_rekening = $stmt->executeQuery();
                while ($rs_rekening->next()) {
                    ?>
                    <tr align='center' bgcolor="white">
                        <td>&nbsp;</td>
                        <td  align='left'><?php echo $rs_rekening->getString('rekening_code') . ' ' . $rs_rekening->getString('rekening_name') ?></td>
                        <td align='right'><?php echo number_format($rs_rekening->getString('total'), 0, ',', '.') ?></td>
                    </tr>
                    <?php
                }
            }

            $query2 = "select sum(nilai_anggaran) as hasil "
                    . "from " . sfConfig::get('app_default_schema') . ".dewan_rincian_detail rd "
                    . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";

            $con = Propel::getConnection();
            $stmt5 = $con->prepareStatement($query2);
            $total_rekening = $stmt5->executeQuery();
            while ($total_rekening->next()) {
                echo '<tr bgcolor="#ffffff">';
                echo '<td colspan="2" class="Font8v style3" align="right"><b>Total  :</b></td>';
                echo '<td class="Font8v style3" align="right" nowrap="nowrap"><b>' . number_format($total_rekening->getString('hasil'), 0, ',', '.') . '</b></td>';
                echo '</tr>';
            }
            ?>


        </table>
        <table border="0" width="100%">
            <tbody><tr>
                    <?php
                    $c = new Criteria();
                    $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
                    $cs = UnitKerjaPeer::doSelectOne($c);
                    if ($cs) {
                        $kepala = $cs->getKepalaPangkat();
                        $nama = $cs->getKepalaNama();
                        $nip = $cs->getKepalaNip();
                        $unit_name = $cs->getUnitName();
                    }
                    ?>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="60%">&nbsp;</td>
                    <td class="style4" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" align="left" width="30%">Surabaya, 
                        <p align="center" style="font-weight: bold">
                            <?php
                            if ($unit_id == '0700') {
                                echo strtoupper('INSPEKTUR');
                            } else {
                                ?>
                                KEPALA <?php
                                echo strtoupper($unit_name);
                            }
                            ?> </p> 
                        <br>
                        <br>

                        <br>
                        <br>
            <center><?php echo $nama ?><br>
                <?php echo $kepala ?><br>
                NIP : <?php echo $nip ?></center>  </td>
            </tr>

            <tr>
                <td colspan="2">
                    <table border="0"  cellspacing="0" width="100%">
                        <tbody><tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">Keterangan : </td>
                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">Tanggal Pembahasan : </td>


                            </tr>

                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">Catatan Hasil Pembahasan : </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">1. </td>

                            </tr>
                            <tr>

                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">2. </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">3. </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">4. </td>


                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">5. </td>

                            </tr>
                            <tr>
                                <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);"><div class="style3" align="center">
                                        <div align="center">Tim Anggaran Pemerintah Daerah </div>
                                    </div></td>


                            </tr>
                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="3%"><div align="center"><span class="style3">No</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="51%"><div align="center"><span class="style3">Nama</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="11%"><div align="center"><span class="style3">NIP</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="21%"><div align="center"><span class="style3">Jabatan</span></div></td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="14%"><div align="center"><span class="style3">Tanda Tangan </span></div></td>

                            </tr>
                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">1</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>

                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">2</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>
                            <tr>

                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">3</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" height="50px">4</td>

                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                                <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>

                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody></table></td>
            </tr>
            </tbody></table>
    </body>
</div>