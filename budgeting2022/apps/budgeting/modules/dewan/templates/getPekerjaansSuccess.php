<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_peneliti');
$i = 0;
$kode_sub = '';
$temp_rekening = '';
?>

<?php
foreach ($rs_rd as $rd):
    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();

    if ($kode_sub != $rd->getKodeSub()) {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(MurniBukuPutihRkaMemberPeer::KODE_SUB, $kode_sub);
            $rs_rkam = MurniBukuPutihRkaMemberPeer::doSelectOne($C_RKA);
            //print_r($rs_rkam);exit;
            if ($rs_rkam) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="7">
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            } else {
                
            }
        } else {

            $c = new Criteria();
            $c->add(MurniBukuPutihRincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = MurniBukuPutihRincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="7"> 
                        <b>:. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                //print_r($query);exit;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                            <td colspan="7"><?php
                                if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK')) {
//                                    echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $t->getString('kode_sub') . '")');
                                }
                                ?> <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                            <td align="right">
                                <?php
                                $query2 = "select sum(nilai_anggaran) as hasil_kali
								from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
								where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";

                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query2);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                }
                                $ada = 'ada';
                                ?> </td>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                        <?php
                    }
                }

                if ($ada == 'tidak') {
                    if ($kode_sub != '') {
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                                                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                        //print_r($query);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                    <td colspan="7"><?php
                                        if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK')) {
//                                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $t->getString('kode_sub') . '")');
                                        }
                                        ?> <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b></td>

                                    <td align="right">
                                        <?php
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
										from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
										where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                        }
                                        $ada = 'ada';
                                    }
                                    ?>
                                </td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <?php
                        }
                    }
                }
            }
        }
    } elseif (!$rd->getKodeSub()) {
        $kode_sub = '';
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        //$rs_rekening = RekeningPeer::retrieveByPk($rekening_code);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            $subtitle_name = $rd->getSubtitle();
            $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub='$kode_sub' and status_hapus=false";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_rekening);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                $jumlah_rekening = number_format($t->getString('jumlah_rekening'), 0, ',', '.');
                if ($t->getString('jumlah_rekening') == 0) {
                    $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub isnull and status_hapus=false";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query_rekening);
                    $ts = $stmt->executeQuery();
                    while ($ts->next()) {
                        $jumlah_rekening = number_format($ts->getString('jumlah_rekening'), 0, ',', '.');
                    }
                }
            }
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                <td colspan="7"><i><?php echo $rekening_code . ' ' . $rekening_name; ?></i> </td>
                <td align="right"><?php echo $jumlah_rekening ?></td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <?php
        }
    }

    if ($sf_user->getNamaUser() != 'parlemen2') {
        ?>
        <tr class="pekerjaans_<?php echo $id ?>">
            <td>
                <?php
                $kegiatan = $rd->getKegiatanCode();
                $unit = $rd->getUnitId();
                $no = $rd->getDetailNo();
                $sub = $rd->getSubtitle();
                $komponen_id = $rd->getKomponenId();
                ?>
            </td>
            <!--<td><?php echo $rd->getKomponenName() . ' ' . $rd->getDetailName(); ?>-->
            <!-- ///////////////////////////////////////warnain rows ///////////////////////////////////////////////////////////////////////////////// -->
            <?php
            //awal-comment
            //if ($rd->getNotePeneliti()!='' and $rd->getNotePeneliti() != NULL and $rd->getStatusHapus()==false ) {
            //awal-comment
            //irul 29 jan 2014 - note peneliti + note skpd
            if ((($rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL) or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL)) and $rd->getStatusHapus() == false) {
                //irul 29 jan 2014 - note peneliti + note skpd
                ?>
                <td style="background: pink"><?php
                    echo $rd->getKomponenName() . ' ';
                    if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                        echo ' ' . $rd->getDetailName() . ' ' . $rd->getLokasiKecamatan() . ' ' . $rd->getLokasiKelurahan();
                    }
                    ?>
                </td>
                <td style="background: pink" align="center"><?php echo $rd->getSatuan(); ?></td>
                <td style="background: pink" align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>

                <td style="background: pink" align="right">
                    <?php
                    // khusus mas rizki
                    //djieb  begin
                    $komponen_id = $rd->getKomponenId();
                    if ($rd->getSatuan() == '%') {
                        echo $rd->getKomponenHargaAwal();
                    } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                        echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?></td>

                <td style="background: pink" align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td style="background: pink" align="right"><?php echo $rd->getPajak() . '%'; ?></td>
                <td style="background: pink" align="right"><?php
                    $total = $rd->getNilaiAnggaran();
                    echo number_format($total, 0, '$total,', '.');
                    ?></td>
                <td style="background: pink" align="center">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>
                <?php
            } //////////////////////////////////// end of warna     ////////////////////////////////////////////////////////
            else {
                ?>
                <td><?php
                    echo $rd->getKomponenName() . ' ';
                    if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                        echo ' ' . $rd->getDetailName() . ' ' . $rd->getLokasiKecamatan() . ' ' . $rd->getLokasiKelurahan();
                    }
                    $rekno = $rd->getRekeningCode();
                    if ($rd->getLockSubtitle() == 'LOCK' and $rekno == '5.2.1.04.01') {
                        echo image_tag('/images/gembok.gif', array('width' => '25', 'height' => '25'));
                    }
                    ?>
                </td>
                <td align="center"><?php echo $rd->getSatuan(); ?></td>
                <td align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>

                <td align="right">
                    <?php
                    // khusus mas rizki
                    //djieb  begin
                    $komponen_id = $rd->getKomponenId();
                    if ($rd->getSatuan() == '%') {
                        echo $rd->getKomponenHargaAwal();
                    } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                        echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?></td>

                <td align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td align="right"><?php echo $rd->getPajak() . '%'; ?></td>
                <td align="right"><?php
                    $total = $rd->getNilaiAnggaran();
                    echo number_format($total, 0, ',', '.');
                    ?></td>
                <td align="center">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>           
            <?php } ?>
        </tr>
        <?php
    }
endforeach;
?>