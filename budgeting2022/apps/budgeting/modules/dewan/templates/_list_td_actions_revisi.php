<td class='tombol_actions'>
        <?php
        echo link_to('<i class="fa fa-th-list"></i>', 'dewan/TemplateRKA?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId(). '&tahap=murnibp', array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'), 'class' => 'btn btn-outline-info btn-sm'));
        ?>
        <?php
        $query = "select * from " . sfConfig::get('app_default_schema') . ".seting_aplikasi "
                . "where id = 1";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $t = $stmt->executeQuery();
        while ($t->next()) {
            if ($t->getString('status_rincian_dewan') == 1) {
                echo link_to('<i class="fa fa-th-edit"></i>', 'dewan/editRevisi?kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&unit_id=' . $master_kegiatan->getUnitId(), array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'), 'class' => 'btn btn-outline-primary btn-sm'));
            }
        }
        ?>
</td>