<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$sub_id = 1;
$cetak = FALSE;
$kegiatan_code = $sf_params->get('kode_kegiatan');

$status = 'LOCK';
$sf_user->removeCredential('status_dewan');
$sf_user->addCredential('status_dewan');
$sf_user->setAttribute('status', $status, 'status_dewan');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Subtitle Kegiatan SKPD <?php echo $kode_kegiatan; ?></h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('dinas/list_messages'); ?>
    <!-- Default box -->
    <div class="box box-info">        
        <div class="box-body"id="indicator" style="display:none;" align="center">
            <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
        </div>
    </div><!-- /.box -->

    <div class="box box-info">
        <div class="box-body">
            <div id="sf_admin_container">


                <div id="sf_admin_content" class="table-responsive">


                    <?php if (!$rs_subtitle): ?>
                        <?php echo __('Tidak Ada Subtitle untuk SKPD dengan Kegiatan ini') ?>
                    <?php else: ?>
                        <table cellspacing="0" class="sf_admin_list">
                            <thead>  
                                <tr>
                                    <th><b>Subtitle</b></th>
                                    <th><b>Rekening | Komponen</b></th>
                                    <th><b>Satuan</b></th>
                                    <th><b>Koefisien</b></th>
                                    <th><b>Harga</b></th>
                                    <th><b>Hasil</b></th>
                                    <th><b>PPN</b></th>
                                    <th><b>Total</b></th>
                                    <th><b>Belanja</b></th>
                            <!--        <th><b>Catatan SKPD</b></th>-->
                                    <!--<th><b>Catatan Tim Anggaran</b></th>-->
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $i = 1;
                                foreach ($rs_subtitle as $subtitle_indikator):
                                    ?>
                                    <?php
                                    $unit_id = $subtitle_indikator->getUnitId();
                                    $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                    $subtitle = $subtitle_indikator->getSubtitle();
                                    $sub_id = $subtitle_indikator->getSubId();
                                    $nama_subtitle = trim($subtitle);
                                    $odd = fmod($i++, 2);

                                    //////////////////////////////////////////////////////////////////////////////////
                                    //* untuk memberi warning jika komponen ada catatan penyelia dan SKPD
                                    $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail 
                                        where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit_id' and kegiatan_code = '$kode_kegiatan' and subtitle ilike '%$nama_subtitle%' and status_hapus = false ";
                                    //print_r($query);exit;
                                    //diambil nilai terpakai
                                    $con = Propel::getConnection();
                                    $statement = $con->prepareStatement($query);
                                    $rs = $statement->executeQuery();
                                    $jml = $rs->getRecordCount();
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    if ($jml > 0) {
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>" id="subtitle_<?php print $sub_id ?>">
                                            <td style="background: pink" colspan="7">
                                                <b>
                                                    <i>
                                                        <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                    </i>
                                                </b>&nbsp
                                                <div id="<?php echo 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId() ?>">
                                                    <?php
                                                    //ARG
                                                    if ($subtitle_indikator->getLakilaki() != 0 || $subtitle_indikator->getPerempuan() != 0 || $subtitle_indikator->getDewasa() != 0 || $subtitle_indikator->getAnak() != 0 || $subtitle_indikator->getLansia() != 0 || $subtitle_indikator->getInklusi() != 0) {
                                                        if ($subtitle_indikator->getCatatan() != '')
                                                            echo 'Catatan ARG :' . $subtitle_indikator->getCatatan();
                                                        echo '<br>';

                                                        echo link_to_remote(submit_tag('Catatan ARG'), array('alt' => 'Mengubah Catatan gender (ARG) ',
                                                            'title' => 'Mengubah Catatan gender (ARG)', 'update' => 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId(),
                                                            'url' => 'dinas/catatanGender?act=edit&sub_id=' . $subtitle_indikator->getSubId() . '&catatanSubtitle=' . $subtitle_indikator->getCatatan() . ' &unit_id=' . $subtitle_indikator->getUnitId() . '&kode_kegiatan=' . $subtitle_indikator->getKegiatanCode(),
                                                            'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')"));
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                                if ($subtitle_indikator->getLockSubtitle() == FALSE) {
                                                    $subtitleName = base64_encode($subtitle_indikator->getSubtitle());
                                                    //   echo ' '.link_to(image_tag('/images/digisign.png'), 'digisign/digisign?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code.'&id='.$sub_id.'&nama_subtitle='.$subtitleName,array('title'=>'Tanda Tangan Digital') );
                                                } else if ($subtitle_indikator->getLockSubtitle() == TRUE) {
                                                    echo '  ' . image_tag('../images/gembok.gif', array('title' => 'Telah Ditanda tangani', 'width' => '25', 'height' => '25'));
                                                }
                                                ?> <div id="<?php echo 'tambahPersonil_' . $sub_id ?>"></div>

                                            </td>

                                            <td style="background: pink" align="right">
                                                <?php
                                                $query2 = "select sum(nilai_anggaran) as hasil_kali
                                            from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                                            where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query2);
                                                $t = $stmt->executeQuery();
                                                while ($t->next()) {
                                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                }
                                                ?></td>
                                            <td style="background: pink" colspan="2">&nbsp;</td>
                                        </tr>

                                    <?php } else {
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>" id="subtitle_<?php print $sub_id ?>">
                                            <td colspan="7"><i><b>
                                                        <?php echo link_to_function(image_tag('b_plus.png', array('name' => 'img_' . $sub_id, 'id' => 'img_' . $sub_id, 'border' => 0)), 'showHideKegiatan(' . $sub_id . ')') . ' ' . $subtitle_indikator->getSubtitle(); ?>
                                                </i></b>&nbsp
                                                <div id="<?php echo 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId() ?>">
                                                    <?php
                                                    //ARG
                                                    if ($subtitle_indikator->getLakilaki() != 0 || $subtitle_indikator->getPerempuan() != 0 || $subtitle_indikator->getDewasa() != 0 || $subtitle_indikator->getAnak() != 0 || $subtitle_indikator->getLansia() != 0 || $subtitle_indikator->getInklusi() != 0) {
                                                        if ($subtitle_indikator->getCatatan() != '')
                                                            echo 'Catatan ARG :' . $subtitle_indikator->getCatatan();
                                                        echo '<br>';

                                                        echo link_to_remote(submit_tag('Catatan ARG'), array('alt' => 'Mengubah Catatan gender (ARG) ',
                                                            'title' => 'Mengubah Catatan gender (ARG)', 'update' => 'tempat_ajax_' . $subtitle_indikator->getUnitId() . '_' . $subtitle_indikator->getKegiatanCode() . '_' . $subtitle_indikator->getSubId(),
                                                            'url' => 'dinas/catatanGender?act=edit&sub_id=' . $subtitle_indikator->getSubId() . '&catatanSubtitle=' . $subtitle_indikator->getCatatan() . ' &unit_id=' . $subtitle_indikator->getUnitId() . '&kode_kegiatan=' . $subtitle_indikator->getKegiatanCode(),
                                                            'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')"));
                                                    }
                                                    ?>
                                                </div>
                                                <?php
                                                if ($subtitle_indikator->getLockSubtitle() == FALSE) {
                                                    $subtitleName = base64_encode($subtitle_indikator->getSubtitle());
                                                    //   echo ' '.link_to(image_tag('/images/digisign.png'), 'digisign/digisign?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code.'&id='.$sub_id.'&nama_subtitle='.$subtitleName,array('title'=>'Tanda Tangan Digital') );
                                                } else if ($subtitle_indikator->getLockSubtitle() == TRUE) {
                                                    echo '  ' . image_tag('../images/gembok.gif', array('title' => 'Telah Ditanda tangani', 'width' => '25', 'height' => '25'));
                                                }
                                                ?> <div id="<?php echo 'tambahPersonil_' . $sub_id ?>"></div>

                                            </td>

                                            <td align="right">
                                                <?php
                                                $query2 = "select sum(nilai_anggaran) as hasil_kali
                                        from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
                                        where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and subtitle ilike '$nama_subtitle' and status_hapus=FALSE";
                                                $con = Propel::getConnection();
                                                $stmt = $con->prepareStatement($query2);
                                                $t = $stmt->executeQuery();
                                                while ($t->next()) {
                                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                                }
                                                ?></td>
                                            <td colspan="3">&nbsp;</td>
                                        </tr> 
                                    <?php } ?>
                                    <tr id="indicator_<?php echo $sub_id ?>" style="display:none;" align="center"><td colspan="11">
                                <dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd>
                                </td></tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7" align="right">Total Keseluruhan:</td>
                                    <td align="right">
                                        <?php
                                        $unit_id = $subtitle_indikator->getUnitId();
                                        $kode_kegiatan = $subtitle_indikator->getKegiatanCode();
                                        $subtitle = $subtitle_indikator->getSubtitle();
                                        $nama_subtitle = trim($subtitle);
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
				from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail
				where kegiatan_code ='$kode_kegiatan' and unit_id='$unit_id' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                        }
                                        ?></td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                            </tfoot>
                        </table>
                    <?php endif; ?>

                </div>

                <div id="sf_admin_footer">
                </div>

            </div>
        </div>
    </div>

</section><!-- /.content -->

<script>
    image1 = new Image();
    image1.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';

    image2 = new Image();
    image2.src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';

    function showHideKegiatan(id) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }

        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getPekerjaans/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#subtitle_' + id).after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function showHideKegiatanMasalah(id, kegiatan, unit) {
        var row = $('#subtitle_' + id);
        var img = $('#img_' + id);

        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_plus.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/b_minus.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator_' + id).show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getPekerjaansMasalah/id/" + id + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#subtitle_0').after(msg);
                    $('#indicator_' + id).remove();
                });
            }
        } else {
            $('.pekerjaans_' + id).remove();
            minus = -1;
        }
    }

    function hapusHeaderKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus Header Sub Subtitle kegiatan ini??');
        if (a === true)
        {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/hapusHeaderPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html"
            }).done();

        }
    }

    function editHeaderKegiatan(id, kegiatan, unit, kodesub) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/ajaxEditHeader/act/editHeader/kodeSub/" + kodesub + "/unitId/" + unit + "/kegiatanCode/" + kegiatan + ".html",
            context: document.body
        }).done(function(msg) {
            $('#header_' + kodesub).before(msg);
            $('#header_' + kodesub).remove();
        });
    }

    function hapusKegiatan(id, kegiatan, unit, no) {
        var a = confirm('Apakah anda yakin akan menghapus kegiatan ini??');
        if (a === true) {
            $('.pekerjaans_' + id).remove();
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/hapusPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + no + ".html"
            }).done();
        }
    }

    function editSubtitle(id) {
        var loading = $('#indicator_' + id);
        loading.show();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getPekerjaans/id/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            loading.remove();
        });

    }

    function inKeterangan(id, kegiatan, unit) {
        var row = $('headerrka_' + id);
        var img = $('#img_' + id);
        var pekerjaans = $('#depan_' + id);
        if (img) {
            var src = document.getElementById('img_' + id).getAttribute('src');
            var minus = src.indexOf('/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png');
            if (minus !== -1) {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/down.png';
            } else {
                src = '/<?php echo sfConfig::get('app_default_coding'); ?>/images/up.png';
            }
            img.attr('src', src);
        }


        if (minus === -1) {
            var kegiatan_id = 'headerrka_' + id;
            var pekerjaans = $('#depan_' + id);
            var n = pekerjaans.length;

            if (n > 0) {
                for (var i = 0; i < pekerjaans.length; i++) {
                    var pekerjaan = pekerjaans[i];
                    pekerjaan.style.display = 'table-row';
                }
            } else {
                $('#indicator').show();
                $.ajax({
                    url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/getHeader/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + ".html",
                    context: document.body
                }).done(function(msg) {
                    $('#indicator').remove();
                    $("#headerrka_" + id).after(msg);
                });
            }
        } else {
            $('#depan_' + id).remove();
        }
    }

    function hapusSubKegiatan(id, kegiatan, unit, kodesub) {
        var a = confirm('Apakah anda yakin akan menghapus SUB kegiatan ini??');
        if (a === true) {
            var kegiatan_id = 'subtitle_' + id;
            var pekerjaans = $('.pekerjaans_' + id);
            for (var i = 0; i < pekerjaans.length; i++) {
                var pekerjaan = pekerjaans[i];
                pekerjaan.style.display = 'none';
            }
            $.ajax({
                url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dewan/hapusSubPekerjaans/id/" + id + "/kegiatan/" + kegiatan + "/unit/" + unit + "/no/" + kodesub + ".html",
                context: document.body
            }).done();
        }
    }
</script>