<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <?php include_partial('list_th_tabular_revisi') ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod(++$i, 2)
                ?>
            <tr>
                <?php include_partial('list_td_tabular_revisi', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd)) ?>
                <?php include_partial('list_td_actions_revisi', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd)) ?>
            </tr>
            <?php endforeach; ?>
            <tr class="sf_admin_row_3">
                <td colspan="2">&nbsp;</td>
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_bukuputih_rincian_detail "
                            . "where unit_id='$unit_id' and status_hapus=FALSE";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo number_format($nilai, 0, ',', '.');
                    ?>
                </td>
                <td colspan="1">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
        echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'dewan/listRevisi?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "dewan/listRevisi?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'dewan/listRevisi?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
        endif;
        ?>
    </ul>
</div>