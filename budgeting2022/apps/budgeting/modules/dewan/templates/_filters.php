<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php use_helper('Object', 'Javascript') ?>

<div class="sf_admin_filters">
    <?php echo form_tag('dewan/list', array('method' => 'get')) ?>

    <fieldset>
        <h2><?php echo __('Kolom Cari') ?></h2>
        <?php
        
        $e = new Criteria();
        $e->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $e->addJoin(UnitKerjaPeer::UNIT_ID, UserHandleV2Peer::UNIT_ID);
        $e->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaUser(), Criteria::EQUAL);
        $e->add(UserHandleV2Peer::SCHEMA_ID, '2', Criteria::EQUAL);                
        $e->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $v = UnitKerjaPeer::doSelect($e);
        ?>

        <div class="form-row">

            <label for="satuan_kerja"><?php echo __('Satuan Kerja:') ?></label>
            <div class="content" style="min-height: 25px">

                <?php echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '------Semua Dinas------')))//, Array('onChange'=>remote_function(Array('update'=>'isi', 'url'=>'peneliti/isix', 'with'=>"'b='+this.options[this.selectedIndex].value", 'loading'=>"Element.show('indicator');Element.hide('isi')", 'complete'=>"Element.hide('indicator');Element.show('isi')"))));//options_for_select($subtitle))  ?> 
            </div>
        </div>

        <div class="form-row">
            <?php //echo select_tag('subtitle', objects_for_select($v,'getSubtitle','getSubtitle',0));//options_for_select($subtitle))  ?> 
            <label for="kode_kegiatan"><?php echo __('Kode') ?></label>
            <div class="content" style="min-height: 25px">
                <?php
                echo input_tag('filters[kode_kegiatan]', isset($filters['kode_kegiatan']) ? $filters['kode_kegiatan'] : null, array(
                    'size' => 15,
                ))
                ?>
            </div>
        </div>

        <div class="form-row">
            <label for="nama_kegiatan"><?php echo __('Nama') ?></label>
            <div class="content" style="min-height: 25px">
                <?php
                echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array(
                    'size' => NULL,
                ))
                ?>
            </div>
        </div>

        <div class="form-row">
            <label for="tahap"><?php echo __('Tahap:') ?></label>
            <div class="content" style="min-height: 25px">
                <?php
                echo input_tag('filters[tahap]', isset($filters['tahap']) ? $filters['tahap'] : null, array(
                    'size' => 15,
                ))
                ?> 

            </div>
        </div>

    </fieldset>
    <ul class="sf_admin_actions">
        <li><?php echo button_to(__('reset'), 'dewan/list?filter=filter', 'class=sf_admin_action_reset_filter') ?></li>
        <li><?php echo submit_tag(__('cari'), 'name=filter class=sf_admin_action_filter') ?></li>

<!--	<li><?php //echo submit_tag(__('kunci'), 'name=kunci class=sf_admin_action_lock')   ?></li>
<li><?php //echo submit_tag(__('buka kunci'), 'name=buka class=sf_admin_action_unlock')   ?></li>
<li><?php //echo submit_tag(__('kunci semua'), 'name=kunciall class=sf_admin_action_lockall')   ?></li>
<li><?php //echo submit_tag(__('buka semua'), 'name=bukaall class=sf_admin_action_save_and_add')   ?></li>
<li><?php //echo submit_tag(__('tarik ke e-Project'), 'name=tarikrkaperdinas class=sf_admin_action_lock')   ?></li>-->
    </ul>
</form>
</div>
