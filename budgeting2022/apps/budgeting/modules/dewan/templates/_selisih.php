<?php

if (sfConfig::get('app_tahap_edit') == 'murni') {
//    $pagu = $master_kegiatan->getAlokasiDana();
    $pagu = $master_kegiatan->getAlokasiDana() + $master_kegiatan->getTambahanPagu();
} else {
    $query = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "'";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    while ($rs->next()) {
        $pagu = $rs->getFloat('nilai');
    }
}

$query = "select SUM(nilai_anggaran) as nilai
		from " . sfConfig::get('app_default_schema') . ".rincian_detail
		where kegiatan_code ='" . $master_kegiatan->getKodeKegiatan() . "' and unit_id ='" . $master_kegiatan->getUnitId() . "' and status_hapus=false and tahun='" . sfConfig::get('app_tahun_default') . "'";

$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
while ($rs->next()) {
    $nilai = $rs->getFloat('nilai');
}

$selisih = $pagu - $nilai;

$m_selisih = '';
if ($nilai > $pagu) {
    $m_selisih = "<br><font color=red style=text-decoration:blink>Melebihi Pagu!</font>";
}
echo number_format($selisih, 0, ',', '.') . $m_selisih;
?>
