<?php  use_helper('Url','Javascript','Form','Object', 'Validation'); ?>
<tr class="depan_1" bgcolor="#AFC4EF">
			<td colspan="9">
<h1 align="center"><?php echo 'RENCANA KERJA ANGGARAN' ?></h1>
<?php
$kode_kegiatan = $sf_params->get('kegiatan');
$unit_id = $sf_params->get('unit');


$kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
if (substr($master_kegiatan->getKodeProgram2(), 0, 4)=='X.XX')
{
	$kode_urusan = substr($master_kegiatan->getKodeUrusan(),0,4);
}
else
{
	$kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
}
//print_r($kode_program22);
//exit;
$e = new Criteria();
$e -> add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
$es = UnitKerjaPeer::doSelectOne($e);
if($es)
{
	$kode_permen = $es->getKodePermen();
}
		
		
$kode = $kode_urusan.'.'.$kode_permen.'.'.$kode_program22.'.'.$master_kegiatan->getKodeKegiatan();
$temp_kode = explode('.',$kode);
if(count($temp_kode)>=5)
	$kode_kegiatan_pakai = $temp_kode[0].'.'.$temp_kode[1].'.'.$temp_kode[3].'.'.$temp_kode[4];
else
	$kode_kegiatan_pakai = $kode; 
$query="select mk.kode_kegiatan, mk.nama_kegiatan, mk.alokasi_dana, mk.output, mk.kode_program, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan, mk.outcome, mk.target_outcome, mk.catatan, mk.ranking, uk.unit_name, uk.kode_permen, mk.kode_urusan, mk.masukan, mk.outcome
from ". sfConfig::get('app_default_schema') .".master_kegiatan mk,unit_kerja uk
where mk.kode_kegiatan = '".$kode_kegiatan."' and mk.unit_id = '".$unit_id."'
and uk.unit_id=mk.unit_id";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
//$c = new Criteria();
//$c->add(MasterKegiatanPeer::KODE_KEGIATAN and MasterKegiatanPeer::UNIT_ID, $kode_kegiatan and $unit_id);
//$kode = MasterKegiatanPeer::doSelect($c);
//foreach ($kode as $kegiatan)
$ranking = '';

while ($rs->next())
{
	$kegiatan_kode = $rs->getString('kode_kegiatan');
	$nama_kegiatan = $rs->getString('nama_kegiatan');
	$alokasi = $rs->getString('alokasi_dana');
	$organisasi = $rs->getString('unit_name');
	$benefit = $rs->getString('benefit');
	$impact = $rs->getString('impact');
	$kode_program =$rs->getString('kode_program');
	$program_p_13 = $rs->getString('kode_program2');
	$masukan = $rs->getString('masukan');
	//$urusan = $rs->getString('urusan');
	$kode_misi = $rs->getString('kode_misi');
	$kode_tujuan = $rs->getString('kode_tujuan');
	$outcome = $rs->getString('outcome');
	$target_outcome = $rs->getString('target_outcome');
	$catatan =$rs->getString('catatan');
	$ranking = $rs->getInt('ranking');
	$kode_permen = $rs->getString('kode_permen');
	$kode_urusan = $rs->getString('kode_urusan');
	$outcome = $rs->getString('outcome');
	$output = $rs->getString('output');

}

//$d = new Criteria();
//$d->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
//$kodes = RincianDetailPeer::doSelect($d);

$query="select distinct(rd.subtitle)
from ". sfConfig::get('app_default_schema') .".rincian_detail rd
where rd.kegiatan_code = '".$kode_kegiatan."' and rd.unit_id = '".$unit_id."'";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
//$stmt->setInt(1, $this->getId());
//$stmt->setLimit($max);
$rs = $stmt->executeQuery();
?>
<table cellspacing="0" class="sf_admin_list">
<tbody>
<tr class="sf_admin_row_1">
<td align='right'>Organisasi</td>
<td>:</td>
<td><?php echo $kode_permen.' '.$organisasi ?></td>
</tr>
<tr valign='top'>
<td align='right'>Kegiatan</td>
<td>:</td>
<td><?php echo $kode_urusan.'.'.$kode_program.'.'.$kode_program22.'.'.$kegiatan_kode.' '.$nama_kegiatan ?></td>
</tr>
<tr valign='top'>
<td align='right'>Nama Kegiatan</td>
<td>:</td>
<td><?php echo $nama_kegiatan ?></td>
</tr>
<tr valign='top'>
<?php
$misi_name='';
$query="select *
from ". sfConfig::get('app_default_schema') .".master_misi ms
where ms.kode_misi='".$kode_misi."'";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs1 = $stmt->executeQuery();
while ($rs1->next())
{
	$misi_name = $rs1->getString('nama_misi');
}
?>
<td align='right'>Misi</td>
<td>:</td>
<td><?php echo $misi_name ?></td>
</tr>
<tr valign='top'>
<?php
$tujuan='';
$query="select *
from " . sfConfig::get('app_default_schema') . ".master_tujuan mj
where mj.kode_misi='".$kode_misi."' and mj.kode_tujuan='".$kode_tujuan."'";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs1 = $stmt->executeQuery();
while ($rs1->next())
{
$tujuan = $rs1->getString('nama_tujuan');
}
?>
<td align='right'>Tujuan</td>
<td>:</td>
<td><?php echo $tujuan ?></td>
</tr>
<tr valign='top'>
<?php
$program='';
$string_tujuan ='%'.$kode_tujuan.'%';
$query="select *
from ". sfConfig::get('app_default_schema') .".master_program kp
where kp.kode_program='".$kode_program."' and kp.kode_tujuan ilike '".$string_tujuan."'";
//print_r($query);exit;
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs1 = $stmt->executeQuery();
while ($rs1->next())
{
	$program = $rs1->getString('kode_program').' '.$rs1->getString('nama_program');
}
?>
<td align='right'>Program RPJM</td>
<td>:</td>
<td><?php echo $program ?></td>
</tr>
<tr>
<td align='right'>Urusan</td>
<td>:</td>
<?php
	$u = new Criteria();
	$u -> add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan);
	$us = MasterUrusanPeer::doSelectOne($u);
	if($us)
	{
		$nama_urusan = $us ->getNamaUrusan();
	}
?>
<td><?php echo $kode_urusan.' '.$nama_urusan; //substr($program_p_13,0,4) ?></td>
</tr>
<tr valign='top'>
<?php
$program='';
//if($ranking)
//{
$query="select *
from ". sfConfig::get('app_default_schema') .".master_program2 kp
where kp.kode_program='".$kode_program."' and kp.kode_program2='".$program_p_13."'";//"' and kp.ranking=".$ranking."";

$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs1 = $stmt->executeQuery();
while ($rs1->next())
{
	$program = $rs1->getString('kode_program2').' '.$rs1->getString('nama_program2');
}
//}
?>
<td align='right'>Program P13</td>
<td>:</td>
<td><?php echo $program; ?></td>
</tr>
<tr valign="top">
<td align="right">Output Kegiatan</td>
<td>:</td>
<td><?php
  $output = str_replace("#", "|", $output);
  $arr_output = explode("|",$output); ?>
 <table cellspacing="0" class="sf_admin_list">
<thead>
<tr align='center'>
<th>Tolak Ukur Kinerja</th>
<th>Target</th>
<th>Satuan</th>
<th> </th>
</tr>
</thead>
<?php
for($i=0;$i<count($arr_output);$i=$i+3)
{
?>
<tbody>
<tr class="sf_admin_row_1" align='center'>
<td><?php echo input_tag('tolakukur', $arr_output[$i], 'size=100%') ?></td>
<td><?php echo input_tag('target', $arr_output[$i+1]) ?></td>
<td><?php echo input_tag('satuan', $arr_output[$i+2]) ?></td>
</tr>
</tbody>
<?php
	}
?>
</table>
</td>
</tr>
<tr class="sf_admin_row_1">
<td align='right'>Alokasi</td>
<td>:</td>
<td><?php echo input_tag('alokasi', number_format($alokasi, 0, ',', '.')) ?> </td>
<tr>
<td colspan="3">
<hr>
</td>
</tr>
<tr class="sf_admin_row_1">
<td align='right'>Input</td>
<td>:</td>
<td><?php echo textarea_tag('input', $masukan , 'size=100x3') ?></td>
</tr>
<tr class="sf_admin_row_1">
<td align='right' colspan='3'>

<?php 
 if($sf_user->getNamaUser()!='parlemen2') {
echo button_to_remote('edit data penunjang', array('update'=>'penunjang','url'=>'peneliti/penunjang?unit_id='.$unit_id.'&kode='.$kode_kegiatan, 'loading'=>"Element.show('indicator')", 
'complete'=>"Element.hide('indicator')")); }

?>

</td>

</tr>
<tr ><td colspan="3"><div id="penunjang"></div></td></tr>
<tr>
<td colspan="3">Subtitle - Output </td>
</tr>
<tr>
<td colspan="3" valign="center">
<table cellspacing="0" class="sf_admin_list">
<thead>
<tr align='center'>
<th>Subtitle</th>
<!--<th>Output</th>-->
<th>Target - Satuan</th>
<th>Penunjang</th>
</tr>
</thead>
<?php
//while ($rs->next())
//{
	//$subtitles = $rs->getString('subtitle');
	//if ($subtitles != "  Biaya Administrasi Umum")
	//{
		$d = new Criteria();
		//$d -> setOffset(10);
		$d -> add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
		$d -> add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
		//$d -> add(SubtitleIndikatorPeer::SUBTITLE, $subtitles);
		$d -> setDistinct(SubtitleIndikatorPeer::SUBTITLE);
		$d -> addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE); 
		$v = SubtitleIndikatorPeer::doSelect($d);
?>
		<tbody>
		<?php  foreach ($v as $vs)
		{
			$subtitle = $vs->getSubtitle();
			$output = $vs->getIndikator();
			$target = $vs->getNilai();
			$satuan = $vs->getSatuan();
			$penunjang = $vs->getTahap();
			$last = $vs->getLastUpdateIp();
		?>
		<tr class="sf_admin_row_1" align='center'><td><?php echo $subtitle ?> </td>
		<!--<td align='center'><?php echo textarea_tag('output', $output) ?> </td>-->
		<td align='center'><?php echo input_tag('target', $target).' '.input_tag('satuan', $satuan) ?> </td>
		<td align='center'><?php if($last=='buka'):echo $penunjang ; endif;  ?> </td></tr>
		<?php }
	//} 
?>
		</tbody>
</table>
</td>
</tr>
<tr>

<td colspan="3"><small>*PERHATIAN : Perubahan atau penambahan subtitle harap melalui BAPPEKO</small></td>
</tr>
<tr>
<td colspan="3"><hr></td>
</tr>
<?php
	$come = array();
	$temp_array_come = array();
	$come = explode('|', $outcome);
	for($i=0;$i<count($come);$i++)
	{
		$temp_array_code[]=$come[$i];
	}
?>
<tr class="sf_admin_row_1" valign="top">
<td align='right'>Outcome</td>
<td>:</td>
<td>
<table cellspacing="0" class="sf_admin_list">
<thead>
<tr align='center'>
<th>Tolak Ukur Kinerja</th>
<th>Target</th>
<th>Satuan</th>
<th> </th>
</tr>
</thead>
<?php
for($i=0;$i<count($temp_array_code);$i=$i+3)
{
?>
<tbody>
<tr class="sf_admin_row_1" align='center'>
<td><?php echo input_tag('tolakukur', $temp_array_code[$i]) ?></td>
<td><?php echo input_tag('target', $temp_array_code[$i+1]) ?></td>
<td><?php echo input_tag('satuan', $temp_array_code[$i+2]) ?></td>
<td>[hapus]</td>
</tr>
</tbody>
<?php
	}
?>
</table>
</td>
</tr>
<tr class="sf_admin_row_1" valign="top">
<td align='right'>Benefit</td>
<td>:</td>
<?php
	if (!$benefit)
		$benefit='';
?>
<td><?php echo textarea_tag('benefit', $benefit, 'size=100x3') ?></td>
</tr>
<tr>
<tr class="sf_admin_row_1" valign="top">
<td align='right'>Impact</td>
<td>:</td>
<?php
	if (!$impact)
		$impact='';
?>
<td><?php echo textarea_tag('impact', $impact, 'size=100x3') ?></td>
</tr>
<tr>
<tr class="sf_admin_row_1" valign="top">
<td align='right'>Catatan</td>
<td>:</td>
<td><?php echo textarea_tag('catatan', '', 'size=100x3') ?></td>
</tr>
<tr>
<td colspan='3' align='right'>
<?php
$query="select rd.komponen_harga, rd.volume, rd.pajak, rd.subtitle
from ". sfConfig::get('app_default_schema') .".rincian_detail rd
where rd.kegiatan_code = '".$kode_kegiatan."' and rd.unit_id = '".$unit_id."'
order by rd.subtitle";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
//$stmt->setInt(1, $this->getId());
//$stmt->setLimit($max);
$rs = $stmt->executeQuery();
//split("pembatas",string,);
$total=0;
$harga=0;
$harga_awal_kena_pajak=0;
$subtitle_array = array();
$subtitle_array_harga = array();
$nama_subtitle='';
$total_subtitle=0;
$total_subtitle_harga=0;
$volume = 0;
$tunai = 0;
	$harga_awal = 0;
	$total_subtitle = 0;
	$harga_awal_kena_pajak = 0;
	$total_subtitle = 0;
	$total = 0;
	$total_subtitle_harga = 0;



while ($rs->next())
{
	if ($nama_subtitle=='')
	{
		$nama_subtitle = $rs->getString('subtitle');
		$subtitle_array[] = $nama_subtitle;
	}

	else if (trim($nama_subtitle) != trim($rs->getString('subtitle')))
	{	
		$nama_subtitle = $rs->getString('subtitle');
		$subtitle_array[] = $nama_subtitle;
		$subtitle_array_harga[] = $total_subtitle_harga;
		$total_subtitle = 0;
		$total_subtitle_harga = 0;
	}
	$volume = $rs->getString('volume'); 
	$tunai = $rs->getString('komponen_harga');
	$harga_awal = ($rs->getString('volume') * $rs->getString('komponen_harga'));
	$total_subtitle = $harga_awal;
	$harga_awal_kena_pajak = $harga_awal + (($rs->getString('pajak')/100) * $harga_awal);
	$total_subtitle = $total_subtitle +(($rs->getString('pajak')/100) * $total_subtitle);
	$total = $total + $harga_awal_kena_pajak;
	$total_subtitle_harga = $total_subtitle_harga + $total_subtitle;
}
$subtitle_array_harga[] = $total_subtitle_harga;
$query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
from ". sfConfig::get('app_default_schema') .".rincian_detail
where kegiatan_code ='".$kode_kegiatan."' and unit_id='".$unit_id."'";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query2);
$rs = $stmt->executeQuery();
while ($rs->next())
{
	$total = $rs->getString('hasil_kali'); 
}
?>
Total nilai : <?php echo number_format($total, 0, ',', '.'); ?>
</td>
</tr>
</table>
</td>
</tr>