<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_dinas');

$i = 0;
$kode_sub = '';
$temp_rekening = '';
// print_r($rd.'<br><br>'.$rs_rd);
?>
<?php
foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();
    if ($kode_sub != $rd->getKodeSub()) {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);
        if ($cekKodeSub == 'RKAM') {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(DinasRkaMemberPeer::KODE_SUB, $kode_sub);
            $C_RKA->addAscendingOrderByColumn(DinasRkaMemberPeer::KODE_SUB);
            $rs_rkam = DinasRkaMemberPeer::doSelectOne($C_RKA);
            //print_r($rs_rkam);exit;
            if ($rs_rkam) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="8">
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <?php
            } else { //else RKAM
            }
        } else { //else form SUB
            $c = new Criteria();
            $c->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = DinasRincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="8">
                        <b> :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b></td>
                    <td align="right">
                        <?php echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.'); ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_sub_parameter "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                //print_r($query);exit;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                            <td colspan="8"><b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                $ada = 'ada';
                                ?> </td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    }
                }

                if ($ada == 'tidak') {
                    if ($kode_sub != '') {
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".dinas_rincian_detail "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub = '$sub' and status_hapus=false";
                        //print_r($query);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                    <td colspan="8">
                                        <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b>
                                    </td>

                                    <td align="right">
                                        <?php
                                        echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                        $ada = 'ada';
                                    }
                                    ?>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        }
                    }
                }
            }
        }
    }

    $rekening_code = $rd->getRekeningCode();
    //echo $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                <td colspan="8"><i><?php echo $rekening_code . ' ' . $rekening_name ?></i> </td>

                <td colspan="4">&nbsp;</td>
            </tr>
            <?php
        }
    }
    ?>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td>
            <?php
            $kegiatan = $rd->getKegiatanCode();
            $unit = $rd->getUnitId();
            $no = $rd->getDetailNo();
            $sub = $rd->getSubtitle();

            //irul 6oktober 2014 - cek musrenbang
            $benar_musrenbang = 0;
            if ($rd->getIsMusrenbang() == 'TRUE') {
                $benar_musrenbang = 1;
            }
            $benar_prioritas = 0;
            if ($rd->getPrioritasWali() == 'TRUE') {
                $benar_prioritas = 1;
            }
            $benar_output = 0;
            if ($rd->getIsOutput() == 'TRUE') {
                $benar_output = 1;
            }
            //irul 6oktober 2014 - cek musrenbang
            //irul 6oktober 2014 - cek multiyears
            $benar_multiyears = 0;
            if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                $benar_multiyears = 1;
            }
            //irul 6oktober 2014 - cek multiyears            

            $benar_hibah = 0;
            if ($rd->getIsHibah() == 'TRUE') {
                $benar_hibah = 1;
            }
            ?>
        </td>

        <?php
        //awal-comment
        $cek = FALSE;
//        $c_prev = new Criteria();
//        $c_prev->add(RincianDetailPeer::UNIT_ID, $rd->getUnitId());
//        $c_prev->add(RincianDetailPeer::KEGIATAN_CODE, $rd->getKegiatanCode());
//        $c_prev->add(RincianDetailPeer::DETAIL_NO, $rd->getDetailNo());
//        $c_prev->add(RincianDetailPeer::NILAI_ANGGARAN, $rd->getNilaiAnggaran());
//        if (!RincianDetailPeer::doSelectOne($c_prev)) {
//            $cek = TRUE;
//        }
        if ($cek or ( $rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL ) or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL ) and $rd->getStatusHapus() == false) {
            //awal-comment
            ?>
            <td style="background: pink" align="center">
                <?php
                echo KomponenPeer::buatKodeNamaAkrualLima($rd->getAkrualCode());
                echo '<br>' . KomponenPeer::buatKodeNamaAkrual($rd->getAkrualCode());

                //if (sfConfig::get('app_tahap_edit') == 'murni') {
                    $query =
                    "SELECT COUNT(*) AS jumlah
                    FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
                    WHERE detail_kegiatan = '" . $rd->getDetailKegiatan() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    if($rs->next()) $jml = $rs->getString('jumlah');

                    $query =
                    "SELECT COUNT(*) AS jumlah
                    FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni_mondalev
                    WHERE kegiatan_code = '" . $rd->getKegiatanCode() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    if($rs->next()) $jml_mondalev = $rs->getString('jumlah');

                    if($jml <= 0 && $jml_mondalev > 0 && ($rd->getIsOutput() || $rd->getIsMusrenbang() || $rd->getPrioritasWali()))
                        echo link_to_function('<i class="fa fa-unlock"></i> Buka Komponen', 'execBukaKomp("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat', 'disable' => true));
                        //echo link_to(image_tag('open.png') . ' BUKA KOMPONEN', 'peneliti/bukaKomponenMurni?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), array('class' => 'btn btn-default btn-flat btn-sm'));
                //}
                ?>
            </td>
            <td style="background: pink">
                <?php
                if ($benar_musrenbang == 1) {
                    echo '&nbsp;<span class="label label-success">Musrenbang</span>';
                }
                if ($benar_output == 1) {
                    echo '&nbsp;<span class="label label-info">Output</span>';
                }
                if ($benar_prioritas == 1) {
                    echo '&nbsp;<span class="label label-warning">Prioritas</span>';
                }
                if ($benar_hibah == 1) {
                    echo '&nbsp;<span class="label label-success">Hibah</span>';
                }
                if ($rd->getKecamatan() <> '') {
                    echo '&nbsp;<span class="label label-warning">Jasmas</span>';
                }
                if ($benar_multiyears == 1) {
                    echo '&nbsp;<span class="label label-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
                }
                if ($rd->getIsBlud() == 1) {
                    echo '&nbsp;<span class="label label-info">BLUD</span>';
                }
                echo '<br/>';
                echo $rd->getKomponenName();
                if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                    echo ' ' . $rd->getDetailName() . '<br/>';
                    if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                        if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                            echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                        }
                    }
                }
                $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
                        . "where komponen_id='" . $rd->getKomponenId() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query2);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                        echo image_tag('/images/newanima.gif');
                    }
                }
//                notifikasi
                if ($rd->getStatusLelang() == 'lock' || $rd->getStatusLelang() == 'unlock') {
                    echo '<br/><span class="label label-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                }
//                notifikasi
                ?>
                <br>
                <!-- <div class="btn-group">
                    <?php echo link_to_function('<i class="fa fa-edit"></i> Buat Catatan', 'execBuatNote("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat', 'disable' => true)); ?>
                </div> -->
            </td>
            <td style="background: pink" align="center"><?php echo $rd->getSatuan(); ?></td>
            <td style="background: pink" align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
            <td style="background: pink" align="right"><?php
                if ($rd->getSatuan() == '%') {
                    echo $rd->getKomponenHargaAwal();
                } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                    echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                } else {
                    echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                }
                ?></td>
            <td style="background: pink" align="right">
                <?php
                $volume = $rd->getVolume();
                $harga = $rd->getKomponenHargaAwal();
                $hasil = $volume * $harga;
                echo number_format($hasil, 0, ',', '.');
                ?>
            </td>
            <td style="background: pink" align="right">
                <?php echo $rd->getPajak() . '%'; ?>
            </td>
            <td style="background: pink" align="right">
                <?php
                $volume = $rd->getVolume();
                $harga = $rd->getKomponenHargaAwal();
                $pajak = $rd->getPajak();
                $total = $rd->getNilaiAnggaran();
                echo number_format($total, 0, ',', '.');
                ?>
            </td>
            <td style="background: pink" align="center">
                <?php
                $rekening = $rd->getRekeningCode();
                $rekening_code = substr($rekening, 0, 5);
                $c = new Criteria();
                $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                if ($rs_rekening) {
                    echo $rs_rekening->getBelanjaName();
                }
                ?>
            </td>
            <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
            <td style="background: pink">
                <?php
                echo 'Catatan SKPD:<br/>';
                $skpd_note = $rd->getNoteSkpd();
                echo textarea_tag('skpd', $skpd_note, 'readonly=readonly');
                if ($sf_user->hasCredential('bappeko')) {
                    $peneliti_note = $rd->getNoteBappeko();
                    echo '<br/>Catatan Bappeko:<br/>';
                } elseif ($sf_user->getNamaUser() == 'anggaran') {
                    $peneliti_note = $rd->getNoteTapd();
                    echo '<br/>Catatan BPKPD:<br/>';
                }
                echo textarea_tag('peneliti', $peneliti_note, 'readonly=readonly');
                ?>
            </td>
            <?php endif; ?>
        <?php } else {
            ?>
            <td align="center">
                <?php
                echo KomponenPeer::buatKodeNamaAkrualLima($rd->getAkrualCode());
                echo '<br>' . KomponenPeer::buatKodeNamaAkrual($rd->getAkrualCode());

                //if (sfConfig::get('app_tahap_edit') == 'murni') {
                    $query =
                    "SELECT COUNT(*) AS jumlah
                    FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni
                    WHERE detail_kegiatan = '" . $rd->getDetailKegiatan() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    if($rs->next()) $jml = $rs->getString('jumlah');

                    $query =
                    "SELECT COUNT(*) AS jumlah
                    FROM " . sfConfig::get('app_default_schema') . ".buka_komponen_murni_mondalev
                    WHERE kegiatan_code = '" . $rd->getKegiatanCode() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    if($rs->next()) $jml_mondalev = $rs->getString('jumlah');

                    if($jml <= 0 && $jml_mondalev > 0 && ($rd->getIsOutput() || $rd->getIsMusrenbang() || $rd->getPrioritasWali()))
                        echo link_to_function('<i class="fa fa-unlock"></i> Buka Komponen', 'execBukaKomp("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat', 'disable' => true));
                        //echo link_to(image_tag('/sf/sf_admin/images/open.png') . ' BUKA KOMPONEN', 'anggaran/bukaKomponenMurni?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), array('class' => 'btn btn-default btn-flat btn-sm'));
                //}
                ?>
            </td>
            <td>
                <?php
                if ($benar_musrenbang == 1) {
                    echo '&nbsp;<span class="label label-success">Musrenbang</span>';
                }
                if ($benar_output == 1) {
                    echo '&nbsp;<span class="label label-info">Output</span>';
                }
                if ($benar_prioritas == 1) {
                    echo '&nbsp;<span class="label label-warning">Prioritas</span>';
                }
                if ($benar_hibah == 1) {
                    echo '&nbsp;<span class="label label-success">Hibah</span>';
                }
                if ($rd->getKecamatan() <> '') {
                    echo '&nbsp;<span class="label label-warning">Jasmas</span>';
                }
                if ($benar_multiyears == 1) {
                    echo '&nbsp;<span class="label label-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
                }
                if ($rd->getIsBlud() == 1) {
                    echo '&nbsp;<span class="label label-info">BLUD</span>';
                }
                echo '<br/>';
                echo $rd->getKomponenName();
                if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                    echo ' ' . $rd->getDetailName() . '<br/>';
                    if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                        if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                            echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                        }
                    }
                }

                $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen where komponen_id='" . $rd->getKomponenId() . "'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query2);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                        echo image_tag('/images/newanima.gif');
                    }
                }
//                notifikasi
                if ($rd->getStatusLelang() == 'lock' || $rd->getStatusLelang() == 'unlock') {
                    echo '<br/><span class="label label-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                }
//                notifikasi
                ?>
                <br>
                <!-- <div class="btn-group">
                    <?php echo link_to_function('<i class="fa fa-edit"></i> Buat Catatan', 'execBuatNote("' . $rd->getDetailNo() . '","' . $unit_id . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat', 'disable' => true)); ?>
                </div> -->
            </td>
            <td align="center"><?php echo $rd->getSatuan() ?></td>
            <td align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
            <td align="right"><?php
                if ($rd->getSatuan() == '%') {
                    echo $rd->getKomponenHargaAwal();
                } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                    echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                } else {
                    echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                }
                ?></td>
            <td align="right"><?php
                $volume = $rd->getVolume();
                $harga = $rd->getKomponenHargaAwal();
                $hasil = $volume * $harga;
                echo number_format($hasil, 0, ',', '.');
                ?>
            </td>
            <td align="right">
                <?php echo $rd->getPajak() . '%'; ?>
            </td>
            <td align="right"><?php
                $volume = $rd->getVolume();
                $harga = $rd->getKomponenHargaAwal();
                $pajak = $rd->getPajak();
                $total = $rd->getNilaiAnggaran();
                echo number_format($total, 0, ',', '.');
                ?>
            </td>
            <td align="center">
                <?php
                $rekening = $rd->getRekeningCode();
                $rekening_code = substr($rekening, 0, 5);
                $c = new Criteria();
                $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                if ($rs_rekening) {
                    echo $rs_rekening->getBelanjaName();
                }
                ?>
            </td>
            <?php if(sfConfig::get('app_tahap_edit') != 'murni'): ?>
            <td>
                <?php
                echo 'Catatan SKPD:<br/>';
                $skpd_note = $rd->getNoteSkpd();
                echo textarea_tag('skpd', $skpd_note, 'readonly=readonly');
                if ($sf_user->hasCredential('bappeko')) {
                    $peneliti_note = $rd->getNoteBappeko();
                    echo '<br/>Catatan Bappeko:<br/>';
                } elseif ($sf_user->getNamaUser() == 'anggaran') {
                    $peneliti_note = $rd->getNoteTapd();
                    echo '<br/>Catatan BPKPD:<br/>';
                }
                echo textarea_tag('peneliti', $peneliti_note, 'readonly=readonly');
                ?>
            </td>
            <?php endif; ?>
        <?php } ?>
    </tr>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td colspan="10">
            <div class="col-md-offset-3 col-md-6" id="note_mondal_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() ?>"></div>
        </td>
    </tr>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td colspan="10">
            <div class="col-md-offset-3 col-md-6" id="tempat_note_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() ?>"></div>
        </td>
    </tr>
    <?php
endforeach;
?>

<script>
    function execBukaKomp(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/buatCatatanKomponen/unit/" + unitid + "/kegiatan/" + kodekegiatan + "/id/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#note_mondal_' + id).html(msg);
        });
    }

    function execBuatNote(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/anggaran/buatCatatan/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_note_' + id).html(msg);
        });
    }
    
    

</script>