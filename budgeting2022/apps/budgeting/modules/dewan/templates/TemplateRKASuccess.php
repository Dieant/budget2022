<?php use_helper('Javascript', 'Number') ?>
<?php
//echo "Print RKA melalui SABK ".link_to('(http://sabk.surabaya.go.id)','http://sabk.surabaya.go.id', array('target'=>'_blank')).". Tekan tombol BACK untuk kembali";exit;
echo use_stylesheet('/css/tampilan_print2.css');
?>
<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$tahap = $sf_params->get('tahap');
if ($tahap == 'pakbp') {
    $tabel_dpn = 'pak_bukuputih_';
} elseif ($tahap == 'pakbb') {
    $tabel_dpn = 'pak_bukubiru_';
} elseif ($tahap == 'murni') {
    $tabel_dpn = 'murni_';
} elseif ($tahap == 'murnibp') {
    $tabel_dpn = 'murni_bukuputih_';
} elseif ($tahap == 'murnibb') {
    $tabel_dpn = 'murni_bukubiru_';
} elseif ($tahap == 'revisi1') {
    $tabel_dpn = 'revisi1_';
} elseif ($tahap == 'revisi2') {
    $tabel_dpn = 'revisi2_';
} elseif ($tahap == 'revisi3') {
    $tabel_dpn = 'revisi3_';
} elseif ($tahap == 'revisi4') {
    $tabel_dpn = 'revisi4_';
} elseif ($tahap == 'revisi5') {
    $tabel_dpn = 'revisi5_';
} elseif ($tahap == 'revisi6') {
    $tabel_dpn = 'revisi6_';
} elseif ($tahap == 'revisi7') {
    $tabel_dpn = 'revisi7_';
} elseif ($tahap == 'revisi8') {
    $tabel_dpn = 'revisi8_';
} elseif ($tahap == 'revisi9') {
    $tabel_dpn = 'revisi9_';
} elseif ($tahap == 'revisi10') {
    $tabel_dpn = 'revisi10_';
} elseif ($tahap == 'rkua') {
    $tabel_dpn = 'rkua_';
} else {
    $tabel_dpn = 'dinas_';
}

$query = "select mk.kode_head_kegiatan, mh.nama_head_kegiatan, mk.kegiatan_id,mk.kode_kegiatan, mk.kode_kegiatan_baru,mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program,
    mk.kode_program2, mk.benefit, mk.impact, mk.kode_program2, mk.kode_misi, mk.kode_tujuan,
    mk.outcome, mk.target_outcome, mk.catatan, mk.ranking,uk.unit_name, mk.kode_urusan,
    uk.kode_permen,mk.output, mk.output_cm, mk.kelompok_sasaran, mk.lokasi, mk.kode_sasaran
    from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan mk, " . sfConfig::get('app_default_schema') . ".master_head_kegiatan mh, unit_kerja uk
    where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "'
    and mk.kode_head_kegiatan = mh.kode_head_kegiatan and uk.unit_id=mk.unit_id";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
$ranking = '';
while ($rs->next()) {
    $kode_head_kegiatan = $rs->getString('kode_head_kegiatan');
    $nama_head_kegiatan = $rs->getString('nama_head_kegiatan');
    $kegiatan_id = $rs->getString('kegiatan_id');
    $kegiatan_kode = $rs->getString('kode_kegiatan');
    $nama_kegiatan = $rs->getString('nama_kegiatan');
    $alokasi = $rs->getString('alokasi_dana');
    $organisasi = $rs->getString('unit_name');
    $benefit = $rs->getString('benefit');
    $impact = $rs->getString('impact');
    $kode_program = $rs->getString('kode_program');
    $program_p_13 = $rs->getString('kode_program2');
    //$urusan = $rs->getString('urusan');
    $kode_misi = $rs->getString('kode_misi');
    $kode_tujuan = '';
    if ($rs->getString('kode_tujuan')) {
        $kode_tujuan = $rs->getString('kode_tujuan');
    }

    $outcome = $rs->getString('outcome');
    $target_outcome = $rs->getString('target_outcome');
    $catatan = $rs->getString('catatan');
    $ranking = $rs->getInt('ranking');
    $kode_organisasi = $rs->getString('kode_urusan');
    $kode_program2 = $rs->getString('kode_program2');
    $kode_permen = $rs->getString('kode_permen');
    $output = $rs->getString('output');
    $output_cm = $rs->getString('output_cm');
    $kelompok_sasaran = $rs->getString('kelompok_sasaran');
    $lokasi = $rs->getString('lokasi');
    $kode_sasaran_kota = $rs->getString('kode_sasaran');
}
?>

<table style="empty-cells: show;" border="0" cellpadding="3" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td colspan="3" style="border: 1px solid rgb(0, 0, 0);">
                <div align="center"><span class="style1 font12"><strong><span class="style3" style="font-size: smaller">KERTAS KERJA</span><br></strong></span></div>
                <div align="center"><span class="font12 style1 style2"><span class="font12 style3 style1" style="font-size: smaller"><strong>ORGANISASI PERANGKAT DAERAH </strong></span></span></div>
            </td>
            <td rowspan="2" style="border: 1px solid rgb(0, 0, 0);"><div class="style3" align="center" style="font-size: smaller"><strong>Formulir<br>
                        OPD 2.2.1 </strong></div></td>
        </tr>
        <tr>

            <td colspan="3" style="border: 1px solid rgb(0, 0, 0);"><div align="center" style="font-size: smaller"><span class="style3">KOTA SURABAYA <br>
                    </span></div>
                <div align="center"><span class="style3" style="font-size: smaller">TAHUN ANGGARAN <?php echo sfConfig::get('app_tahun_default'); ?></span></div></td>
        </tr>
        <tr>
            <td colspan="4" style="border: 1px solid rgb(0, 0, 0);">
                <table border="0" width="100%">
                    <?php
                    $kode_per = explode(".", $kode_organisasi);
                    ?>
                    <tbody>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Organisasi</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller"><?php echo /* $kode_program2 */$unit_id . ' ' . $organisasi ?></span></td>

                        </tr>
                        <tr align="left" valign="top">
                            <td nowrap="nowrap" width="194">
                                <span class="style3" style="font-size: smaller">Urusan Pemerintahan </span>
                            </td>
                            <?php
                            $kode_parameter_urusan = $kode_organisasi;

                            $queryU = "select *
                            from " . sfConfig::get('app_default_schema') . ".master_urusan mu
                            where mu.kode_urusan='" . $kode_parameter_urusan . "'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($queryU);
                            $rsU = $stmt->executeQuery();
                            while ($rsU->next()) {
                                $nama_urusan = $rsU->getString('nama_urusan');
                            }
                            ?>
                            <td width="10"><span class="style3" style="font-size: smaller">:</span></td>
                            <td ><span class="style3" style="font-size: smaller"><?php echo $kode_organisasi . ' ' . $nama_urusan ?></span></td>
                        </tr>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Kegiatan</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="Font8v style3" style="font-size: smaller"><?php echo $kode_head_kegiatan . ' ' ?></span><span class="Font8v style3" style="font-size: smaller"><?php echo $nama_head_kegiatan ?></span></td>
                        </tr>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Sub Kegiatan</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="Font8v style3" style="font-size: smaller"><?php echo $kegiatan_id . ' ' ?></span><span class="Font8v style3" style="font-size: smaller"><?php echo $nama_kegiatan ?></span></td>
                        </tr>
                        <?php
                        $misi_name = '';
                        $c = new Criteria();
                        $c->add(VisiPdPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                        $rs_visi = VisiPdPeer::doSelectOne($c);
                        if(!is_null($rs_visi)) $visi_pd = $rs_visi->getVisi();
                        ?>
                       <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Visi RPJMD 2016-2022</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller"><?php echo $visi_pd; ?></span></td>
                        </tr>
                        <!-- get misi pd -->
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Misi RPJMD 2016-2022</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <?php
                                $di = new Criteria();
                                $di->add(MisiPdPeer::UNIT_ID, $unit_id);
                                $di->add(MisiPdPeer::KODE_KEGIATAN, $kode_kegiatan);
                                $vi = MisiPdPeer::doSelect($di);
                                foreach ($vi as $value):
                                    $misi_pd = $value->getMisi();
                                ?>
                                <span class="style3" style="font-size: smaller"><?php echo $misi_pd; ?></span><br/>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Tujuan Kota</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <table border="1"  align="left" width="100%" cellspacing="0">
                                    <tr align='center'>      
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Narasi Tujuan Kota</span></td>                       
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Indikator Tujuan Kota</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                    </tr>
                                    <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorTujuanKotaPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorTujuanKotaPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_itk = IndikatorTujuanKotaPeer::doSelect($c);
                                        $ind_tujuan_kota = array();
                                        $tar_tujuan_kota = array();
                                        foreach ($rs_itk as $key => $value) {
                                            array_push($ind_tujuan_kota, $value->getIndikatorTujuan());
                                            array_push($tar_tujuan_kota, $value->getNilai());
                                        }
                                        $idx = count($ind_tujuan_kota);
                                    ?>
                                        <tr>
                                            <td style="border: 1px solid">
                                                <?php
                                                // get tujuan pd
                                                $di = new Criteria();
                                                $di->add(TujuanKotaPeer::UNIT_ID, $unit_id);
                                                $di->add(TujuanKotaPeer::KODE_KEGIATAN, $kode_kegiatan);
                                                $vi = TujuanKotaPeer::doSelect($di);
                                                foreach ($vi as $value):
                                                        $tujuan_kota = $value->getTujuanKota();
                                                ?>
                                                <span class="Font8v" style="font-size: smaller"><?php echo $tujuan_kota; ?></span><br/>
                                                <?php endforeach; ?>
                                            </td>
                                            <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br('-'.$ind_tujuan_kota[$i]."\n");
                                                } 
                                                ?></span>
                                            </td>
                                            <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br($tar_tujuan_kota[$i]."\n");
                                                }
                                                ?></span>
                                            </td>
                                        </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Tujuan Perangkat Daerah</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <table border="1"  align="left" width="100%" cellspacing="0">
                                    <tr align='center'>      
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Narasi Perangkat Daerah</span></td>                       
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Indikator Perangkat Daerah</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                    </tr>
                                    <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorTujuanPdPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorTujuanPdPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_itp = IndikatorTujuanPdPeer::doSelect($c);
                                        $ind_tujuan_pd = array();
                                        $tar_tujuan_pd = array();
                                        foreach ($rs_itp as $key => $value) {
                                            array_push($ind_tujuan_pd, $value->getIndikatorTujuan());
                                            array_push($tar_tujuan_pd, $value->getNilai());
                                        }
                                        $idx = count($ind_tujuan_pd);
                                    ?>
                                        <tr>
                                            <td style="border: 1px solid">
                                                <?php
                                                // get tujuan pd
                                                $di = new Criteria();
                                                $di->add(TujuanPdPeer::UNIT_ID, $unit_id);
                                                $di->add(TujuanPdPeer::KODE_KEGIATAN, $kode_kegiatan);
                                                $vi = TujuanPdPeer::doSelect($di);
                                                foreach ($vi as $value):
                                                        $tujuan_pd = $value->getTujuanPd();
                                                ?>
                                                <span class="Font8v" style="font-size: smaller"><?php echo $tujuan_pd; ?></span><br/>
                                                <?php endforeach; ?>
                                            </td>
                                            <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br('-'.$ind_tujuan_pd[$i]."\n");
                                                } 
                                                ?></span>
                                            </td>
                                            <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br($tar_tujuan_pd[$i]."\n");
                                                }
                                                ?></span>
                                            </td>
                                        </tr>
                                </table>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td><span class="style3" style="font-size: smaller">Sasaran Kota</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <table border="1"  align="left" width="100%" cellspacing="0">
                                <div>
                                    <tr align="center">
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Narasi Sasaran Kota</span></td>
                                        <?php 
                                            $c = new Criteria();
                                            $c->add(IndikatorSasaranKotaPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                            $c->addAnd(IndikatorSasaranKotaPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                            $rs_isk = IndikatorSasaranKotaPeer::doSelect($c);
                                            $c_isk = 0;
                                            $ind_sasaran = array();
                                            $tar_sasaran = array();
                                            foreach ($rs_isk as $key => $value) {
                                                $indikator_sasaran = $value->getIndikatorSasaran();
                                                array_push($ind_sasaran, $indikator_sasaran);
                                                $target_sasaran = $value->getNilai();
                                                array_push($tar_sasaran, $target_sasaran);

                                            }
                                            $idx = count($ind_sasaran);
                                        ?>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Indikator Sasaran Kota</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                    </tr>
                                </div>
                                    <tr>
                                        <td style="border: 1px solid">
                                            <?php
                                            $di = new Criteria();
                                            $di->add(SasaranKotaPeer::UNIT_ID, $unit_id);
                                            $di->add(SasaranKotaPeer::KODE_KEGIATAN, $kode_kegiatan);
                                            $vi = SasaranKotaPeer::doSelect($di);
                                            foreach ($vi as $value):
                                                    $nama_sasaran = $value->getSasaranKota();
                                            ?>
                                            <span class="style5" style="font-size: smaller"><?php echo $nama_sasaran ?></span><br/>
                                            <?php endforeach; ?>
                                        </td>
                                        <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                            for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br('-'.$ind_sasaran[$i]."\n");
                                            } 
                                            ?></span>
                                        </td>
                                        <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                            for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br($tar_sasaran[$i]."\n");
                                            }
                                            ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- Get Sasaran Pd -->
                        <tr valign="top">
                            <td><span class="style3" style="font-size: smaller">Sasaran Perangkat Daerah</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <table border="1"  align="left" width="100%" cellspacing="0">
                                <div>
                                    <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorSasaranPdPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorSasaranPdPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_isp = IndikatorSasaranPdPeer::doSelect($c);
                                        $ind_sasaran_pd = array();
                                        $tar_sasaran_pd = array();
                                        foreach ($rs_isp as $key => $value) {
                                            // $indikator_sasaran = $value->getIndikatorSasaran();
                                            array_push($ind_sasaran_pd, $value->getIndikatorSasaran());
                                            // $target_sasaran = $value->getNilai();
                                            array_push($tar_sasaran_pd, $value->getNilai());

                                        }
                                        $idx = count($ind_sasaran_pd);
                                    ?>
                                    <tr align="center">
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Narasi Sasaran Perangkat Daerah</span></td>
                                            
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Indikator Sasaran Perangkat Daerah</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                    </tr>
                                </div>
                                    <tr>
                                        <td style="border: 1px solid">
                                            <?php
                                            $di = new Criteria();
                                            $di->add(SasaranPdPeer::UNIT_ID, $unit_id);
                                            $di->add(SasaranPdPeer::KODE_KEGIATAN, $kode_kegiatan);
                                            $vi = SasaranPdPeer::doSelect($di);
                                            foreach ($vi as $value):
                                                    $nama_sasaran_pd = $value->getSasaranPd();
                                            ?>
                                            <span class="style5" style="font-size: smaller"><?php echo $nama_sasaran_pd ?></span><br/>
                                            <?php endforeach; ?>
                                        </td>
                                        <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                            for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br('-'.$ind_sasaran_pd[$i]."\n");
                                            } 
                                            ?></span>
                                        </td>
                                        <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                            for($i=0; $i < $idx; $i++) {                                                
                                                echo nl2br($tar_sasaran_pd[$i]."\n");
                                            }
                                            ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- get program pd -->
                        <?php
                        $query = "select nama_program2 from " . sfConfig::get('app_default_schema') . ".master_program2 where kode_program2 = '$kode_program2'";
                        $stmt = $con->prepareStatement($query);
                        $rs1 = $stmt->executeQuery();
                        while ($rs1->next()) {
                            $program = $rs1->getString('nama_program2');
                        }
                        ?>
                        <tr align="left" valign="top">
                            <td><span class="style3" style="font-size: smaller">Program RPJMD 2016-2022</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <table border="1"  align="left" width="70%" cellspacing="0">
                                    <tr align='center'>      
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Narasi Program</span></td>                       
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Indikator Program</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                    </tr>
                                    <?php 
                                        $c = new Criteria();
                                        $c->add(IndikatorProgramKotaPeer::UNIT_ID,$unit_id, Criteria::EQUAL);
                                        $c->addAnd(IndikatorProgramKotaPeer::KODE_KEGIATAN,$kode_kegiatan, Criteria::EQUAL);
                                        $rs_ip = IndikatorProgramKotaPeer::doSelect($c);
                                        $ind_program_pd = array();
                                        $tar_program_pd = array();
                                        foreach ($rs_ip as $key => $value) {
                                            array_push($ind_program_pd, $value->getIndikatorProgram());
                                            array_push($tar_program_pd, $value->getNilai());
                                        }
                                        $idx = count($ind_program_pd);
                                    ?>
                                        <tr>
                                            <td style="border: 1px solid">
                                                <span class="Font8v" style="font-size: smaller"> <?php echo $program ?>  </span></td>
                                            <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br('-'.$ind_program_pd[$i]."\n");
                                                } 
                                                ?></span>
                                            </td>
                                            <td style="border: 1px solid"><span class="Font8v" style="font-size: smaller"><?php 
                                                for($i=0; $i < $idx; $i++) {                                                
                                                    echo nl2br($tar_program_pd[$i]."\n");
                                                }
                                                ?></span>
                                            </td>
                                        </tr>
                                </table>
                            </td>
                        </tr>
                        <?php
                        $query = "select rd.komponen_harga, rd.volume, rd.pajak, rd.subtitle from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false order by rd.subtitle";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        $total = 0;
                        $harga = 0;
                        $harga_awal_kena_pajak = 0;
                        $subtitle_array = array();
                        $subtitle_array_harga = array();
                        $nama_subtitle = '';
                        $total_subtitle = 0;
                        $total_subtitle_harga = 0;
                        while ($rs->next()) {
                            if ($nama_subtitle == '') {
                                $nama_subtitle = $rs->getString('subtitle');
                                $subtitle_array[] = $nama_subtitle;
                            } else if ($nama_subtitle != $rs->getString('subtitle')) {
                                $nama_subtitle = $rs->getString('subtitle');
                                $subtitle_array[] = $nama_subtitle;
                                $subtitle_array_harga[] = $total_subtitle_harga;
                                $total_subtitle = 0;
                                $total_subtitle_harga = 0;
                            }
                            $volume = $rs->getString('volume');
                            $tunai = $rs->getString('komponen_harga');
                            $harga_awal = ($rs->getString('volume') * $rs->getString('komponen_harga'));
                            $total_subtitle = $harga_awal;
                            $harga_awal_kena_pajak = $harga_awal - (($rs->getString('pajak') / 100) * $harga_awal);
                            $total_subtitle = $total_subtitle - (($rs->getString('pajak') / 100) * $total_subtitle);
                            $total = $total + $harga_awal_kena_pajak;
                            $total_subtitle_harga = $total_subtitle_harga + $total_subtitle;
                        }
                        $subtitle_array_harga[] = $total_subtitle_harga;
                        ?>
                        <tr align="left" valign="top">
                            <?php
                            $query2 = "select sum(nilai_anggaran) as hasil from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";
                            $con = Propel::getConnection();
                            $stmt5 = $con->prepareStatement($query2);
                            $total_rekening = $stmt5->executeQuery();
                            while ($total_rekening->next()) {
                                $total = $total_rekening->getString('hasil');
                            }
                            ?>
                            <td><span class="style3" style="font-size: smaller">Total Nilai</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td><span class="style3" style="font-size: smaller">Rp.<?php echo number_format($total, 0, ',', '.') ?>,00</span></td>
                        </tr>
                        <tr valign="top">
                            <td><span class="style3" style="font-size: smaller">Output Kegiatan</span></td>
                            <td><span class="style3" style="font-size: smaller">:</span></td>
                            <td>
                                <table border="1"  align="left" width="70%" cellspacing="0">
                                    <tr align='center'>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Tolok Ukur Kinerja</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Target</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Satuan</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Kelompok Sasaran</span></td>
                                        <td style="border: 1px solid"><span class="style5" style="font-size: smaller">Lokasi</span></td>
                                    </tr>
                                    <?php
                                        $di = new Criteria();
                                        $di->add(OutputSubtitlePeer::UNIT_ID, $unit_id);
                                        $di->add(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan);
                                        $vi = OutputSubtitlePeer::doSelect($di);
                                        foreach ($vi as $value):
                                            $kinerja = $value->getOutput();
                                            $kinerja_output = explode("|", $kinerja);
                                            $kelompok = $value->getKelompokSasaran();
                                            $lokasi = $value->getLokasi();
                                            ?>
                                            <tr class="sf_admin_row_1" align='center'>
                                                <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $kinerja_output[0] ?></span></td>
                                                <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $kinerja_output[1] ?></span></td>
                                                <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $kinerja_output[2] ?></span></td>
                                                <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $kelompok ?></span></td>
                                                <td style="border: 1px solid"><span class="style5" style="font-size: smaller"><?php echo $lokasi ?></span></td>
                                            </tr>
                                        <?php endforeach; ?>  
                                </table>
                            </td>
                        </tr>
                        <?php
                        $d = new Criteria();
                        $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                        $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $d->add(DinasSubtitleIndikatorPeer::NILAI_CM, NULL, Criteria::ISNOTNULL);
                        if (DinasSubtitleIndikatorPeer::doSelect($d)):
                            ?>
                            <tr valign="top">
                                <td><span class="style3" style="font-size: smaller">Output Countermeasure</span></td>
                                <td><span class="style3" style="font-size: smaller">:</span></td>
                                <td>

                                    <table border="1" align="left" width="95%" cellspacing="0">
                                        <tbody>
                                            <tr bgcolor="#ffffff" style="border: 1px solid">
                                                <td class="style3" width="223" style="border: 1px solid;"><div class="style5" align="center" style="font-size: smaller">Subtitle</div></td>
                                            <!--    <td width="218" style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller">Output</div></td>-->
                                                <td width="97" style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller">Target</div></td>
                                                <td width="102" style="border: 1px solid"><div class="style5" align="center" style="font-size: smaller">Satuan</div></td>
                                            </tr>
                                            <?php
                                            $query = "select distinct(rd.subtitle)
                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "subtitle_indikator rd
                                where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "'";

                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query);
                                            $rs = $stmt->executeQuery();
                                            while ($rs->next()) {
                                                $subtitles = $rs->getString('subtitle');
                                                $d = new Criteria();
                                                //$d -> setOffset(10);
                                                $d->add(DinasSubtitleIndikatorPeer::UNIT_ID, $unit_id);
                                                $d->add(DinasSubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
                                                $d->add(DinasSubtitleIndikatorPeer::SUBTITLE, $subtitles);
                                                $d->setDistinct(DinasSubtitleIndikatorPeer::SUBTITLE);
                                                $d->addAscendingOrderByColumn(DinasSubtitleIndikatorPeer::SUBTITLE);
                                                $v = DinasSubtitleIndikatorPeer::doSelect($d);
                                                $subtitle = '';
                                                foreach ($v as $vs) {

                                                    $subtitle = $vs->getSubtitle();
//                                                                        $output = $vs->getIndikator();
                                                    $target = $vs->getNilaiCm();
                                                    $satuan = $vs->getSatuanCm();
                                                    ?>
                                                    <tr>
                                                        <td style="border: 1px solid">
                                                            <span class="style5" style="font-size: smaller">
                                                                <?php echo $subtitle ?>
                                                            </span>
                                                        </td>
                                                        <!--<td style="border: 1px solid"><span class="Font8v style5" style="font-size: smaller"><?php //echo $output    ?></span></td>-->
                                                        <td style="border: 1px solid">
                                                            <span class="Font8v" style="font-size: smaller">
                                                                <?php echo $target ?>
                                                            </span>
                                                        </td>
                                                        <td style="border: 1px solid">
                                                            <span class="Font8v" style="font-size: smaller">
                                                                <?php echo $satuan ?>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        <?php endif; ?>

                    </tbody>
                </table>
            </td>

        </tr>
        <!--                                                    <tr>
            <td><span class="style3" style="font-size: smaller">Kelompok Sasaran Kegiatan </span></td>
            <td><span class="style3" style="font-size: smaller">:</span></td>
            <td><span class="style3" style="font-size: smaller"><?php echo $kelompok_sasaran ?></span></td>
            <td colspan="4" style="border: 1px solid rgb(0, 0, 0);" width="30%">
                <span class="style3" style="font-size: smaller; width: 194px">Kelompok Sasaran Kegiatan</span>
                <span class="style3" style="font-size: smaller; width: 10px">:</span>
                <span class="style3" style="font-size: smaller;"><?php echo $kelompok_sasaran ?></span>
            </td>
        </tr>-->

        <tr>
            <td colspan="4" style="border: 1px solid rgb(0, 0, 0);"><div align="center"><span class="style3" style="font-size: smaller">Rincian Anggaran Belanja Langsung</span></div>
                <div align="center"><span class="style3" style="font-size: smaller">Menurut Program dan Per Kegiatan Organisasi Perangkat Daerah </span></div></td>
        </tr>
    </tbody>
</table>
<table bgcolor="#ffffff" border="0" cellpadding="3" cellspacing="0" width="100%">
    <tr>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);">&nbsp;</td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Komponen</strong></span></td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Satuan</strong></span></td>
        <td align="center" width="20%" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Koefisien</strong></span></td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Harga</strong></span></td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Hasil</strong></span></td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>PPN</strong></span></td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Total</strong></span></td>
        <td align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>Belanja</strong></span></td>
    </tr>
    <?php
    $query = "select distinct(rd.subtitle)
                                                                from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd
                                                                where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false order by rd.subtitle";
    $con = Propel::getConnection();
    $stmt = $con->prepareStatement($query);
    $rs = $stmt->executeQuery();
    $subtitle = '';
    $pertama = 0;
    while ($rs->next()) {
        if (trim($subtitle) != trim($rs->getString('subtitle'))) {
            if ($pertama != 0) {
                $query2 = "select sum(nilai_anggaran) as hasil
from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd
where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' and rd.status_hapus=false";
                //print_r($query2);
                //$con = Propel::getConnection();
                $stmt5 = $con->prepareStatement($query2);
                $total_rekening = $stmt5->executeQuery();
                while ($total_rekening->next()) {
                    echo '<tr bgcolor="#ffffff">';
                    echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                    echo '<b>Total ' . $subtitle . '  :</td></b>';
                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total_rekening->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
                    echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                    echo '</tr>';
                }
            }
            $subtitle = $rs->getString('subtitle');
            $pertama+=1;
            ?>
            <tr align="left" bgcolor="white"><td colspan="9" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller"><strong>
                            <?php echo ':: ' . $subtitle ?>
                        </strong></span></td></tr>
            <?php
        }
        if (trim($subtitle) == trim($rs->getString('subtitle'))) {
            $j = 0;
            $subtitle = $rs->getString('subtitle');

            $query2 = "select distinct(rd.rekening_code), r.rekening_name, rd.kode_sub
from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r
where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "'
and rd.rekening_code=r.rekening_code order by rd.kode_sub,rd.rekening_code";
            //print_r($query2);
            //$con = Propel::getConnection();
            $stmt2 = $con->prepareStatement($query2);
            $rs2 = $stmt2->executeQuery();

            $total_semua = 0;
            $sub = '';
//bisma
            while ($rs2->next()) {
                if ($rs2->getString('rekening_code')) {
                    if ($rs2->getString('kode_sub')) {
                        if ($kode_sub != $rs2->getString('kode_sub')) {
                            $kode_sub = $rs2->getString('kode_sub');
                            $cekKodeSub = substr($kode_sub, 0, 4);

                            if ($cekKodeSub == 'RKAM') {
                                $query2 = "select rkam.komponen_name,rkam.detail_name, sum(rd.nilai_anggaran) as hasil
from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd, " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rka_member rkam
where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.kode_sub = '" . $kode_sub . "'
and rd.kode_sub=rkam.kode_sub and rd.status_hapus=false
group by rkam.komponen_name,rkam.detail_name,rd.kode_sub
order by rd.kode_sub";
                                // print_r($query2);
                                $con = Propel::getConnection();
                                $stmt5 = $con->prepareStatement($query2);
                                $total_rkam = $stmt5->executeQuery();
                                while ($total_rkam->next()) {
                                    echo '<tr bgcolor="#ffffff">';
                                    echo '<td colspan="7" class="Font8v style3" align="left" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                                    //total subtitle
                                    echo '<b><i>.:. ' . $total_rkam->getString('komponen_name') . ' ' . $total_rkam->getString('detail_name');
                                    '</td></i></b>';
                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total_rkam->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
                                    echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                                    echo '</tr>';
                                }
                            } elseif ($cekKodeSub == 'RSUB') {
                                //if($kode_sub!=$rs2->getString('kode_sub'))
                                {
                                    //echo $kode_sub.'aaa';
                                    $kode_sub = $rs2->getString('kode_sub');
                                    //$subId = $rs2->getString('from_sub_kegiatan');
                                    $s = new Criteria();
                                    //$s->add(DinasRincianSubParameterPeer::FROM_SUB_KEGIATAN, $subId);
                                    $s->add(DinasRincianSubParameterPeer::KEGIATAN_CODE, $kode_kegiatan);
                                    $s->add(DinasRincianSubParameterPeer::KODE_SUB, $kode_sub);
                                    $s->add(DinasRincianSubParameterPeer::UNIT_ID, $unit_id);
                                    //$s->add(DinasRincianSubParameterPeer::NEW_SUBTITLE, $sub);
                                    //$s->setDistinct();
                                    $sub_pilih = DinasRincianSubParameterPeer::doSelectOne($s);
                                    if ($sub_pilih) {
                                        $sub_name = $sub_pilih->getSubKegiatanName();
                                        $keterangan = $sub_pilih->getKeterangan();
                                        $detail_name = $sub_pilih->getDetailName();
                                    }


                                    //if($cekKodeSub=='RSUB')
                                    {
                                        ?>

                                        <tr align="left" bgcolor="white"><td colspan="9" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>
                                                    &nbsp;<i><?php echo $sub_name . ' ' . $detail_name . '<table width=100%>{' . $keterangan . '}' ?></i>
                                                </b></td></tr>
                                        <?php
                                    }
                                }
                            }
                        }


                        $query3 = "select distinct(rd.rekening_code), rd.komponen_id, rd.komponen_name, rd.satuan, rd.keterangan_koefisien, rd.komponen_harga_awal, rd.komponen_harga, rd.volume, rd.pajak, kb.belanja_name, rd.detail_no, rd.detail_name, rd.sub,rd.kode_sub,rd.tipe,rd.nilai_anggaran, rd.is_musrenbang, rd.prioritas_wali, rd.is_output, rd.note_koefisien
from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' and rd.rekening_code='" . $rs2->getString('rekening_code') . "'
and rd.rekening_code=r.rekening_code and r.belanja_id = kb.belanja_id and  rd.kode_sub = '" . $kode_sub . "'
order by rd.kode_sub,rd.rekening_code";

                        // print_r($query3);
                    } else {
                        /* $query3="select distinct(rd.rekening_code),rd.komponen_name, rd.satuan, rd.keterangan_koefisien, rd.komponen_harga_awal, rd.komponen_harga, rd.volume, rd.pajak, kb.belanja_name, rd.detail_no, rd.detail_name, rd.sub, rd.tipe
                          from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail rd, ". sfConfig::get('app_default_schema') .".rekening r, ". sfConfig::get('app_default_schema') .".kelompok_belanja kb
                          where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '".$kode_kegiatan."' and rd.unit_id = '".$unit_id."' and rd.subtitle = '".$subtitle."'
                          and rd.rekening_code='".$rs2->getString('rekening_code')."' and rd.rekening_code=r.rekening_code and r.belanja_id = kb.belanja_id and rd.sub = ''
                          order by rd.sub,rd.rekening_code"; */
                        $query3 = "select distinct(rd.rekening_code), rd.komponen_id, rd.komponen_name, rd.satuan, rd.keterangan_koefisien, rd.komponen_harga_awal, rd.komponen_harga, rd.volume, rd.pajak, kb.belanja_name, rd.detail_no, rd.detail_name, rd.sub,rd.kode_sub,rd.tipe,rd.nilai_anggaran, rd.is_musrenbang, rd.prioritas_wali, rd.is_output, rd.note_koefisien,case when rd.sumber_dana_id = 1 then '(DAK)'
                            when rd.sumber_dana_id = 2 then '(DBHCHT)' when rd.sumber_dana_id = 3 then '(DID)' 
                            when rd.sumber_dana_id = 4 then '(DBH)' when rd.sumber_dana_id = 5 then '(Pajak Rokok)'
                            when rd.sumber_dana_id = 6 then '(BLUD)' when rd.sumber_dana_id = 7 then '(Bantuan Keuangan Provinsi)'
                            when rd.sumber_dana_id = 8 then '(Kapitasi JKN)' when rd.sumber_dana_id = 9 then 'BOS' 
                            when rd.sumber_dana_id = 10 then '(DAU)' when rd.is_covid=true then '(Covid-19)' end as sumber_dana
from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd, " . sfConfig::get('app_default_schema') . ".rekening r, " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
where rd.volume>0 and rd.status_hapus=false and rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.subtitle = '" . $subtitle . "' and rd.rekening_code='" . $rs2->getString('rekening_code') . "'
and rd.rekening_code=r.rekening_code and r.belanja_id = kb.belanja_id and rd.kode_sub = ''
order by rd.kode_sub,rd.rekening_code";
                    }


                    $rekening = $rs2->getString('rekening_code');
                    echo '<tr bgcolor="white"><td style="border: 1px solid rgb(0, 0, 0);"> :</td><td colspan="8" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . $rekening . ' ' . $rs2->getString('rekening_name') . '</td></tr>';

                    //$con = Propel::getConnection();
                    $stmt3 = $con->prepareStatement($query3);
                    $rs3 = $stmt3->executeQuery();
                    $total = 0;
                    while ($rs3->next()) {
                        echo '<tr bgcolor="#ffffff" valign="top">';
                        //$ubah_subtitle = str_ireplace("/", "-", $subtitle);
                        echo '<td class="Font8v" align="left" style="border: 1px solid rgb(0, 0, 0);">&nbsp;</td>';

                        if (!$rs3->getString('sub')) {
                            $tipe = '{' . $rs3->getString('tipe') . '}';
                        } else {
                            $tipe = '';
                        }
                        $benar_musrenbang = 0;
                        if ($rs3->getString('is_musrenbang') == 't') {
                            $benar_musrenbang = 1;
                        }
                        $benar_prioritas = 0;
                        if($rs3->getString('prioritas_wali') == 't') {
                            $benar_prioritas = 1;
                        }
                        $benar_output = 0;
                        if($rs3->getString('is_output') == 't') {
                            $benar_output = 1;
                        }
                        //echo $rs3->getString('is_output');
                        echo '<td class="Font8v" align="left" style="border: 1px solid rgb(0, 0, 0);"><span class="style3" style="font-size: smaller">' . $rs3->getString('komponen_name') . ' ' . $rs3->getString('detail_name') . ' '.$rs3->getString('sumber_dana').' ';
                        echo '</span></td>';
                        echo '<td class="Font8v style3" align="center" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0);"><span class="Font8v" style="font-size: smaller">' . $rs3->getString('satuan') . '</span></td>';
                        echo '<td class="Font8v style3" align="center" style="border: 1px solid rgb(0, 0, 0);"><span class="Font8v" style="font-size: smaller">' . $rs3->getString('keterangan_koefisien') . '</span></td>';
                        if (number_format($rs3->getString('komponen_harga_awal'), 0, ',', '.') == '0') {
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($rs3->getString('komponen_harga_awal'), 4, ',', '.') . '</td>';
                        } 
                        else 
                        {
                        
                        $komp_id = $rs3->getString('komponen_id');
                        $sub_id = substr($komp_id, 0,18);
                        $komp_name = $rs3->getString('komponen_name');
                        $arr_komp = array('23.02.02.03.03.B','2.1.1.01.01.02.099.005');
                        $arr_name = array('');
                        if ( ($rs3->getString('komponen_harga_awal') != floor($rs3->getString('komponen_harga_awal'))) || $sub_id == '2.1.1.01.01.01.008' ) 
                        {
                        //yogie
                        // tambahan amin all komponen honorer 
                            if (    in_array($komp_name, $arr_name) ||
                                    in_array($komp_id,$arr_komp) ||
                                    // all komponen honorer
                                     $sub_id == '2.1.1.01.01.01.008' 
                                ) 
                            {
                                $c = new Criteria();
                                $c->add(KomponenPeer::KOMPONEN_ID, $komp_id);
                                $cs = KomponenPeer::doSelectOne($c);
                                if ($cs) {
                                    $a = $cs->getKomponenHarga();
                                }
                                else{
                                    $a = $rs3->getString('komponen_harga_awal');
                                }
                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($a, 2, ',', '.') . '</td>';
                            }
                            else 
                            {
                                $a = $rs3->getString('komponen_harga_awal');
                                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($a, 0, ',', '.') . '</td>';
                            }

                        } 
                        else 
                            {
                                    echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($rs3->getString('komponen_harga_awal'), 0, ',', '.') . '</td>';
                            }
                        }

                        $hasil = $rs3->getString('komponen_harga_awal') * $rs3->getString('volume');
                        if ($hasil < 1) {
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($hasil, 4, ',', '.') . '</td>';
                        }
                        else {

                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($hasil, 0, ',', '.') . '</td>';
                        }
                        
                        
                        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . $rs3->getString('pajak') . '%</td>';
                        // $total1 = $hasil + (($rs3->getString('pajak') / 100) * $hasil);
                        $total1 = $rs3->getString('nilai_anggaran');
                        
                        // if ($total1 < 1) {

                        //     $nilai_total_kurang_dr_satu = $hasil + (($rs3->getString('pajak') / 100) * $hasil);
                        //     // echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($total1,0), 0, ',', '.') . '</td>';
                        //     echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' .number_format($nilai_total_kurang_dr_satu, 4, ',', '.'). '</td>';
                        // }
                        // else {
                            echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format($total1, 0, ',', '.') . '</td>';
                        // }
                        
                        $l = strlen($rs3->getString('belanja_name'));
                        echo '<td class="Font8v style3" align="center" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . substr(ucwords(strtolower($rs3->getString('belanja_name'))), 8, $l) . '</td>';
                        echo '</tr>';
                        $total = $total + $total1;
                    }
                }
                $total_semua = $total_semua + $total;
                echo '<tr bgcolor="#ffffff">';
                echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
                //total rekening
                echo 'Total ' . $rs2->getString('rekening_name') . ' :</td>';
                //$query2="select sum(volume * komponen_harga_awal * (100+pajak)/100) as hasil_kali
                //from ". sfConfig::get('app_default_schema') .".dinas_rincian_detail rd
                //where rd.kegiatan_code = '".$kode_kegiatan."' and rd.unit_id = '".$unit_id."' and rd.subtitle='".$subtitle."' and rd.sub=r.rekening_code";
                //$con = Propel::getConnection();
                //$stmt5 = $con->prepareStatement($query2);
                //$total_rekening = $stmt5->executeQuery();
                echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">' . number_format(round($total,0), 0, ',', '.') . '</td>';
                echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
                echo '</tr>';
            }
        }
    }
    $query2 = "select sum(nilai_anggaran) as hasil
                            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail
                            where kegiatan_code = '" . $kode_kegiatan . "' and unit_id = '" . $unit_id . "' and subtitle = '" . $subtitle . "' and status_hapus=false";
    //print_r($query2);
    $con = Propel::getConnection();
    $stmt5 = $con->prepareStatement($query2);
    $total_rekening = $stmt5->executeQuery();
    while ($total_rekening->next()) {
        echo '<tr bgcolor="#ffffff">';
        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
        //total subtitle
        echo '<b>Total ' . $subtitle . '  :</td></b>';
        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total_rekening->getString('hasil',0)), 0, ',', '.') . '</b></td>';
        echo '<td class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>';
        echo '</tr>';
    }
    $query2 = "select sum(nilai_anggaran) as hasil
                            from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail rd
                            where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' and rd.status_hapus=false";

    $con = Propel::getConnection();
    $stmt5 = $con->prepareStatement($query2);
    $total_rekening = $stmt5->executeQuery();
    while ($total_rekening->next()) {
        echo '<tr bgcolor="#ffffff">';
        echo '<td colspan="7" class="Font8v style3" align="right" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;">';
        echo '<b>Grand Total  :</b></td>';
        echo '<td class="Font8v style3" align="right" nowrap="nowrap" style="border: 1px solid rgb(0, 0, 0); font-size: smaller;"><b>' . number_format(round($total_rekening->getString('hasil'), 0), 0, ',', '.') . '</b></td>';
        echo '<td class="Font8v style3" align="right">&nbsp;</td>';
        echo '</tr>';
    }
    ?>
</table>
<table border="0" width="100%">
    <tbody><tr>
            <?php
            
            // $query = " select max(status_level) as status_level "
            //         . " from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "rincian_detail "
            //         . " where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan'";
            // $con = Propel::getConnection();
            // $stmt = $con->prepareStatement($query);
            // $rs = $stmt->executeQuery();
            // if ($rs->next()) {
            //     $max = $rs->getInt('status_level');
            //         $query2 = " select status_level "
            //                     . " from " . sfConfig::get('app_default_schema') . "." . $tabel_dpn . "master_kegiatan "
            //                     . " where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            //         $stmt2 = $con->prepareStatement($query2);
            //         $rs2 = $stmt2->executeQuery();
            //             if ($rs2->next()) {
            //                 if ($rs2->getInt('status_level') > $max) {
            //                     $max = $rs2->getInt('status_level');
            //                 }
            //             }
            //     }

            $c = new Criteria();
            $c->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $cs = UnitKerjaPeer::doSelectOne($c);
            if ($cs) {
                $kepala = $cs->getKepalaPangkat();
                $nama = $cs->getKepalaNama();
                $nip = $cs->getKepalaNip();
                $unit_name = $cs->getUnitName();
               
            }

            $user='pa_'.$unit_id;
            $c1 = new Criteria();
            $c1->add(MasterUserV2Peer::USER_ID, $user);
            $cs1 = MasterUserV2Peer::doSelectOne($c1);
            if ($cs1) {
                $jabatan = $cs1->getJabatan();                
            }
            ?>
            <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;" width="60%">&nbsp;</td>
            <td class="style4" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;" align="left" width="30%">Surabaya,
                <p align="center">
                    <?php                                   
                         if (strpos($nip, 'A') !== false)
                          {
                              echo 'Plt. '.strtoupper($jabatan);
                          } 
                         else if (strpos($nip, 'B') !== false)
                          {
                              echo 'Plh. '.strtoupper($jabatan);
                          }  
                         else
                            echo strtoupper($jabatan);                      
                    ?> </p>
               <!--  <?php if( $max>=6) {?>
                <p align="center">
                <?php  echo "<img src='".sfConfig::get('app_path_uploads')."ttd/".$nip.".png' align='center' height='50%'>"; ?>   

                </p><?php } else {?>
                <br ><br>
                <p></p>
                <br><br>
                <?php }?>              -->    
                <br ><br>
               
                <br><br>        

              
                
    <center><?php echo $nama ?><br>
        <?php echo $kepala ?><br>
        NIP : <?php echo $nip ?></center>  </td>
</tr>

<tr>
    <td colspan="2">
        <table border="0" cellspacing="0" width="100%" >
            <tbody><tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">Keterangan : </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">Tanggal Pembahasan : </td>


                </tr>

                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">Catatan Hasil Pembahasan : </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">1. </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">2. </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">3. </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">4. </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">5. </td>
                </tr>
                <tr>
                    <td colspan="5" style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);"><div class="style3" align="center" style="font-size: smaller;">
                            <div align="center" style="font-size: smaller;">Tim Anggaran Pemerintah Daerah </div>
                        </div></td>

                </tr>
                <tr>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="3%"><div align="center"><span class="style3" style="font-size: smaller;">No</span></div></td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="51%"><div align="center"><span class="style3" style="font-size: smaller;">Nama</span></div></td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="11%"><div align="center"><span class="style3" style="font-size: smaller;">NIP</span></div></td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="21%"><div align="center"><span class="style3" style="font-size: smaller;">Jabatan</span></div></td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0);" width="14%"><div align="center"><span class="style3" style="font-size: smaller;">Tanda Tangan </span></div></td>

                </tr>
                <tr>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">1</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                </tr>

                <tr>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">2</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                </tr>
                <tr>

                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">3</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                </tr>
                <tr>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">4</td>

                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                    <td style="border: 1px solid rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: smaller;">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>

                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table></td>
</tr>
</tbody></table>
