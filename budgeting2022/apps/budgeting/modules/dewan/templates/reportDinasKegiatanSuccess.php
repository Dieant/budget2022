<div class="box box-primary box-solid">        
    <div class="box-header with-border">
        Laporan Per Dinas Per Kegiatan
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode Kegiatan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Pagu</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
            </tr>
            <?php
            $i = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    ?>

                    <?php if (isset($dinas_ok[$i])): ?>
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left"><strong><?php echo $unit_id[$i] ?> &nbsp; <?php echo $unit_name[$i] ?></strong></td>
                            <td align="right"><strong><?php echo $nilai_draft[$i] ?></strong></td>
                            <td align="right"><strong><?php echo $nilai_usulan_unit[$i] ?></strong></td>
                            <td align="right"><strong><?php echo $nilai_locked[$i] ?></strong></td>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <td  align="left"><?php echo $rs->getString('kode_kegiatan') ?></td>
                        <td align="left"><?php echo $rs->getString('nama_kegiatan') ?></td>
                        <td align="right"><?php echo $nilai_draft3[$i] ?></td>
                        <td align="right"><?php echo $nilai_usulan_kegiatan[$i] ?></td>
                        <td align="right"><?php echo $nilai_locked3[$i] ?></td>
                    </tr>
                    <?php $i++; ?>
                <?php
                }while ($rs->next());
            }
            ?>
            <tr>
                <td colspan="2"><strong>Total</strong></td>
                <td align="right"><strong><?php echo $nilai_draft2 ?></strong></td>
                <td align="right"><strong><?php echo $nilai_draft4 ?></strong></td>
                <td align="right"><strong><?php echo $nilai_locked2 ?></strong></td>
            </tr>
        </table>       
    </div>
</div>