<div class="box box-primary box-solid">        
    <div class="box-header with-border">
        Laporan Per Program
    </div>
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th class="text-center">Kode Program</th>
                <th class="text-center">Nama Program</th>
                <th class="text-center">Nilai Usulan</th>
                <th class="text-center">% Usulan</th>
                <th class="text-center">Nilai Disetujui</th>
                <th class="text-center">% Disetujui</th>
            </tr>
            <?php
            $i = 0;
            if ($rs->first() <> '') {
                $rs->first();
                do {
                    ?>
                    <tr>
                        <td  align="center" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('kode_bidang') ?></td>
                        <td align="left" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('nama_bidang') ?></td>
                        <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $nilai_draft3[$i] ?></td>
                        <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $persen_draft3[$i] ?>%</td>
                        <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $nilai_locked3[$i] ?></td>
                        <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $persen_locked3[$i] ?>%</td>
                    </tr>
                    <?php
                    $i++;
                } while ($rs->next());
            }
            ?>
            <tr>
                <td colspan="2" bgcolor="#FFFFFF" class="Font8v"><strong>Total</strong></td>
                <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $nilai_draft2 ?></strong></td>
                <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>100%</strong></td>
                <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $nilai_locked2 ?></strong></td>
                <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>100%</strong></td>
            </tr>
        </table>       
    </div>
</div>