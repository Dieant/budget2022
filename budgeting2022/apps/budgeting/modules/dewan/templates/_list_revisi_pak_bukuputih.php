<div class="table-responsive">
    <table cellspacing="0" class="sf_admin_list table">
        <thead>
            <tr><th style="background: #e4ed6d" colspan="7">
        <div class="float-right">
            <?php if ($pager->haveToPaginate()): ?>
                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/first.png', array('align' => 'absmiddle', 'alt' => __('First'), 'title' => __('First'))), 'dewan/listRevisiPakBukuPutih?page=1') ?>
                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/previous.png', array('align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))), 'dewan/listRevisiPakBukuPutih?page=' . $pager->getPreviousPage()) ?>

                <?php foreach ($pager->getLinks() as $page): ?>
                    <?php echo link_to_unless($page == $pager->getPage(), $page, 'dewan/listRevisiPakBukuPutih?page=' . $page) ?>
                <?php endforeach; ?>

                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/next.png', array('align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))), 'dewan/listRevisiPakBukuPutih?page=' . $pager->getNextPage()) ?>
                <?php echo link_to(image_tag(sfConfig::get('sf_admin_web_dir') . '/images/last.png', array('align' => 'absmiddle', 'alt' => __('Last'), 'title' => __('Last'))), 'dewan/listRevisiPakBukuPutih?page=' . $pager->getLastPage()) ?>
            <?php endif; ?>
        </div>
        <?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) ?>
        </th></tr>
        <tr>
            <?php include_partial('list_th_tabular_revisi') ?>
        </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod(++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <?php include_partial('list_td_tabular_revisi_pak_bukuputih', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd)) ?>
                    <?php include_partial('list_td_actions_revisi_pak_bukuputih', array('master_kegiatan' => $master_kegiatan, 'odd' => $odd)) ?>
                </tr>
            <?php endforeach; ?>
            <tr class="sf_admin_row_3">
                <td colspan="2">&nbsp;</td>
<!--                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(alokasi_dana+tambahan_pagu) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukuputih_master_kegiatan "
                            . "where unit_id='$unit_id'";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_awal = $rs_nilai->getString('nilai');
                    }
                    echo number_format($nilai_awal, 0, ',', '.');
                    ?>
                </td>-->
                <td align="right">
                    <?php
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".pak_bukuputih_rincian_detail "
                            . "where unit_id='$unit_id' and status_hapus=FALSE";
                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo number_format($nilai, 0, ',', '.');
                    ?>
                </td>
<!--                <td align="right">
                    <?php
                    $selisih = $nilai_awal - $nilai;
                    echo number_format($selisih, 0, ',', '.');
                    ?>
                </td>-->
                <td colspan="1">&nbsp;</td>
            </tr>
        </tbody>        
    </table>
</div>