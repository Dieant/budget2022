<?php use_helper('Object','Javascript'); ?>

<?php 
 
    if($tahap=='kegiatan')
    {
        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isiSubtitle', 'url'=>'admin/ajax?unit_id='.$unit_id.'&tahap=subtitle',
                        'with'=>"'b=b=kegiatan_tujuan&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }
    if($tahap=='subtitle')
    {
        echo select_tag('subtitle', objects_for_select($subtitleTujuan,'getSubtitle', 'getSubtitle',
                        '',array('include_custom'=>'Pilih Subtitle')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'admin/ajax?unit_id='.$unit_id.'&kegiatan_code='.$kegiatan_code.'&tahap='.$tahap,
                        'with'=>"'b=subtitle_tujuan&subtitle='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
    }