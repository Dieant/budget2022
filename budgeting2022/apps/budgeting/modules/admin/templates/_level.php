<?php

echo select_tag('filters[level]', options_for_select(array(
    '' => '',
    '1' => 'Dinas',
    '2' => 'Peneliti',
    '3' => 'Tim Data',
    '4' => 'Tim Rekening',
    '7' => 'Dewan',
    '8' => 'Bappeko',
    '9' => 'Administrator',
    '10' => 'Bappeko-PRK',
    '11' => 'Bappeko-Mitra'
                ), isset($filters['level']) ? $filters['level'] : ''),array('class' => 'form-control'));
?>