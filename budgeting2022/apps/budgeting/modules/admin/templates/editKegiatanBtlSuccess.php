<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<?php ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Kegiatan Belanja Tidak Langsung</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('admin/list_messages'); ?>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Form Kegiatan Belanja Tidak Langsung</h3>
        </div>
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">
                <?php echo form_tag('admin/editKegiatanBtl') ?>
                <table cellspacing="0" class="sf_admin_list">
                    <thead>  
                        <tr>
                            <th style="width: 19%"><b>Nama</b></th>
                            <th style="width: 1%">&nbsp;</th>
                            <th style="width: 80%"><b>Isian</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Kode Kegiatan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <input type="text" name="kode_kegiatan" class="form-control" placeholder="Kode Kegiatan" value="<?php echo $kode_kegiatan ?>">
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right'>
                            <td>Nama Kegiatan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <input type="text" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan" value="<?php echo $nama_kegiatan ?>">
                            </td>
                        </tr>
                        <tr class="sf_admin_row_0" align='right'>
                            <td>Catatan Kegiatan</td>
                            <td align="center">:</td>
                            <td align="left">
                                <input type="text" name="catatan" class="form-control" placeholder="Alasan" value="<?php echo $catatan ?>">
                            </td>
                        </tr>
                        <tr class="sf_admin_row_1" align='right' valign="top">
                            <td>Rekening</td>
                            <td align="center">:</td>
                            <td align="left">
                                <div class="row col-xs-12">
                                    <?php foreach ($rs_rekening as $value) { ?>
                                        <div class="div-induk-lokasi row">
                                            <div>
                                                <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Kode Rekening</font><br/>
                                                        <input type="text" name="kode_rekening[]" class="form-control" placeholder="Kode Rekening" value="<?php echo $value->getRekeningCode() ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-4 col-sm-4 col-md-4">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Nama Rekening</font><br/>
                                                        <input type="text" name="nama_rekening[]" class="form-control" placeholder="Nama Rekening" value="<?php echo $value->getRekeningName() ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Semula</font><br/>
                                                        <input type="text" name="semula[]" class="form-control" placeholder="Nilai Semula" value="<?php echo $value->getSemula() ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-options col-xs-4 col-sm-4 col-md-4">
                                                    <div class="input-group input-group-option col-xs-12">
                                                        <font class="text-bold">Menjadi</font><br/>
                                                        <input type="text" name="menjadi[]" class="form-control" placeholder="Nilai Menjadi" value="<?php echo $value->getMenjadi() ?>">
                                                    
                                                        <span class="input-group-addon input-group-addon-add">
                                                            <span class="glyphicon glyphicon-plus"></span>
                                                        </span>
                                                        <span class="input-group-addon input-group-addon-remove">
                                                            <span class="glyphicon glyphicon-remove"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="div-induk-lokasi row">
                                        <div>
                                            <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                <div class="input-group input-group-option col-xs-12">
                                                    <font class="text-bold">Kode Rekening</font><br/>
                                                    <input type="text" name="kode_rekening[]" class="form-control" placeholder="Kode Rekening" value="">
                                                </div>
                                            </div>
                                            <div class="form-group form-group-options col-xs-4 col-sm-4 col-md-4">
                                                <div class="input-group input-group-option col-xs-12">
                                                    <font class="text-bold">Nama Rekening</font><br/>
                                                    <input type="text" name="nama_rekening[]" class="form-control" placeholder="Nama Rekening" value="">
                                                </div>
                                            </div>
                                            <div class="form-group form-group-options col-xs-2 col-sm-2 col-md-2">
                                                <div class="input-group input-group-option col-xs-12">
                                                    <font class="text-bold">Semula</font><br/>
                                                    <input type="text" name="semula[]" class="form-control" placeholder="Nilai Semula" value="">
                                                </div>
                                            </div>
                                            <div class="form-group form-group-options col-xs-4 col-sm-4 col-md-4">
                                                <div class="input-group input-group-option col-xs-12">
                                                    <font class="text-bold">Menjadi</font><br/>
                                                    <input type="text" name="menjadi[]" class="form-control" placeholder="Nilai Menjadi" value="">
                                                
                                                    <span class="input-group-addon input-group-addon-add">
                                                        <span class="glyphicon glyphicon-plus"></span>
                                                    </span>
                                                    <span class="input-group-addon input-group-addon-remove">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                  
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr class="sf_admin_row_0" align='right' valign="top">
                            <td>&nbsp; </td>
                            <td>
                                <?php
                                echo input_hidden_tag('unit_id', $sf_params->get('unit_id'));
                                echo input_hidden_tag('kode_kegiatan_lama', $sf_params->get('kode_kegiatan'));
                                echo input_hidden_tag('kode_kegiatan_asal', $sf_params->get('kode_kegiatan_asal'));
                                echo input_hidden_tag('tahap', $sf_params->get('tahap'));
                                ?>
                            </td>
                            <td>
                                <?php
                                echo submit_tag('simpan', 'name=simpan') . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()"));
                                ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <?php echo '</form>'; ?>
            </div>
        </div>
    </div>
</section>
<style>
    .select2-container-multi .select2-choices .select2-search-choice {
        padding: 3px 5px 3px 18px;
        margin: 3px 0 3px 5px;
        line-height: 20px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".js-example-basic-single").select2();
        $(".js-example-basic-multiple").select2();
    });

    $(function () {
        $(document).on('click', 'div.form-group-options .input-group-addon-add', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            var sDivIluminatiHtml = divIluminati.html();
            var sInputGroupClasses = divIluminati.attr('class');
            //Gambiarra pra nao ficar criando mil inputs
            if (divIluminati.next().length >= 1)
                return;
            divIluminati.parent().append('<div class="' + sInputGroupClasses + '">' + sDivIluminatiHtml + '</div>');
        });
        $(document).on('click', 'div.form-group-options .input-group-addon-remove', function () {
            var divIluminati = $(this).parents('.div-induk-lokasi');
            divIluminati.remove();
        });
    });
</script>
