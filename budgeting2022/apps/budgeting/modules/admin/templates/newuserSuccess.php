<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah User</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Master</a></li>
                    <li class="breadcrumb-item active">Tambah User</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <?php include_partial('admin/list_messages') ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-foursquare"></i> Form
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('admin/newuser', array('class' => 'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Level user</label>
                                    <select name="level" class="form-control select2" id="combo_pilih">
                                        <?php foreach ($users_list as $value) { ?>
                                        <option value="<?php echo $value->getLevelId(); ?>">
                                            <?php echo $value->getLevelName(); ?></option>
                                        <?php
                                }
                                ?>
                                    </select>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6" id="skpd" style="display:none;">
                                <div class="form-group">
                                    <label>Perangkat Daerah</label><br/>
                                    <?php 
                                    $c = new Criteria();
                                    $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                    $v = UnitKerjaPeer::doSelect($c);
                                    echo select_tag('skpd', objects_for_select($v, 'getUnitId', 'getUnitName', array('include_custom' => '--Pilih Perangkat Daerah--')), array('class' => 'form-control select2','style' => 'width:100%')); 
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>User ID</label>
                                    <?php echo input_tag('userid', '', array('class' => 'form-control', 'placeholder' => 'User ID', 'required' => 'required')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>User Name</label>
                                    <?php echo input_tag('username', '', array('class' => 'form-control', 'placeholder' => 'User Name', 'required' => 'required')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <?php echo input_tag('userpass', '', array('type' => 'password','class' => 'form-control', 'value' => 'Password2022', 'required' => 'required')) ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br />
                                    <button type="submit" name="commit" class="btn btn-outline-primary btn-sm">Simpan <i class="far fa-save"></i></button>
                                    <button type="reset" name="reset" class="btn btn-outline-danger btn-sm">Reset <i class="fa fa-backspace"></i></button>
                                    <?php
                                echo input_hidden_tag('referer', $sf_request->getAttribute('referer'));
                            ?>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?>
                    </div>
                </div>
            </div>
        </div>
</section>

<script>
    $("#combo_pilih").change(function () {
        var id = $(this).val();
        if (id == '13' || id == '14' || id == '1')
            $('#skpd').show();
        else
            $('#skpd').hide();
    });
</script>