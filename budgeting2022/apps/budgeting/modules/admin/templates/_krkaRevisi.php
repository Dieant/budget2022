<div class="card-body table-responsive p-0">
  <table class="table table-hover">
    <thead class="head_peach">
            <tr>
                <th id="sf_admin_list_th_kompoen_id">
                    Perangkat Daerah
                </th>
                <th id="sf_admin_list_th_komponen_name">
                    Sub Kegiatan
                </th>
                <th id="sf_admin_list_th_kompoen_id">
                    Detail Kegiatan
                </th>
                <th id="sf_admin_list_th_kompoen_id">
                    Kode Komponen
                </th>
                <th id="sf_admin_list_th_komponen_name">
                    Nama Komponen
                </th>
                <th id="sf_admin_list_th_pajak">
                    Volume
                </th>
                <th id="sf_admin_list_th_pajak">
                    Koefisien
                </th>
                <th id="sf_admin_list_th_komponen_harga">
                    Harga
                </th>
                <th id="sf_admin_list_th_rekening">
                    Rekening
                </th>  
                <th id="sf_admin_list_th_pajak">
                    Pajak
                </th>  
                <th id="sf_admin_list_th_pajak">
                    Anggaran
                </th>
                <th id="sf_admin_list_th_pajak">
                    Tahap
                </th>
                <th id="sf_admin_list_th_pajak">
                    Realisasi
                </th>
            </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($pager->getResults() as $komponen): $odd = fmod(++$i, 2)
            ?>
            <tr>
                <td><?php echo UnitKerjaPeer::getStringUnitKerja($komponen->getUnitId()) ?></td>
                <td><?php echo $komponen->getKegiatanId() ?></td>
                <td><?php echo $komponen->getDetailKegiatan() ?></td>
                <td><?php echo $komponen->getKomponenId() ?></td>
                <td><?php echo $komponen->getKomponenName() ?></td>
                <td style="text-align: center"><?php echo $komponen->getVolume() ?></td>
                <td style="text-align: center"><?php echo $komponen->getKeteranganKoefisien() ?></td>
                <td style="text-align: right">
                    <?php 
                    $komp_id = $komponen->getKomponenId();
                    $sub_id = substr($komp_id, 0,11);
                    $komp_name = $komponen->getKomponenName();
                    $arr_komp = array('2.1.1.01.02.02.003.029');
                    $arr_name = array('Tenaga Operasional','Tenaga Administrasi 3','Tenaga Programmer 3','Tenaga Administrasi 3','Petugas Keamanan','Tenaga Operator 3','Tenaga Satgas','Pembantu Paramedis');
                    if ($komponen->getSatuan() == '%') {
                        // echo $komponen->getKomponenHargaAwal();
                        echo number_format($komponen->getKomponenHargaAwal(), 4, ',', '.');
                        // if ($komponen->getKomponenHargaAwal() < 1){
                        //     echo $komponen->getKomponenHargaAwal();
                        //     // echo "echo";
                        // }
                    }
                    elseif (
                        in_array($komp_name, $arr_name) ||
                        in_array($komp_id,$arr_komp) || 
                        $sub_id =='2.1.1.01.01.01.008' 
                    ) {
                        $c = new Criteria();
                        $c->add(KomponenPeer::KOMPONEN_ID, $komp_id);
                        $cs = KomponenPeer::doSelectOne($c);
                        if ($cs) {
                            $a = $cs->getKomponenHarga();
                        }
                        else{
                            $a = $komponen->getKomponenHargaAwal();
                        }
                        echo number_format($a, 2, ',', '.');
                    }  
                    elseif($komponen->getKomponenHargaAwal() != floor($komponen->getKomponenHargaAwal())){
                        $a = round($komponen->getKomponenHargaAwal());
                        echo number_format($a, 0, ',', '.');
                    } else {
                        echo number_format($komponen->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?>
                </td>
                <td style="text-align: center"><?php echo $komponen->getRekeningCode() ?></td>
                <td style="text-align: center"><?php echo $komponen->getPajak() ?></td>
                <td style="text-align: right"><?php echo number_format($komponen->getNilaiAnggaran(), 0, ',', '.') ?></td>
                <td style="text-align: center"><?php echo $komponen->getTahap() ?></td>
                
                <?php
                if( $komponen->getTahap() =='murnis')
                {
                    $nilairealisasi = 0;
                }
                else
                {
                // itung realisasi
                $unit_id = $komponen->getUnitId();
                $kode_kegiatan = $komponen->getKegiatanCode();
                $detail_no = $komponen->getDetailNo();
                
                $totNilaiAlokasi = 0;
                $totNilaiRealisasi = 0;
                $totNilaiKontrak = 0;
                $totNilaiSwakelola = 0;
                $nilairealisasi = 0;
                $rs_rinciandetail = new DinasRincianDetail;
                $totNilaiAlokasi = $rs_rinciandetail->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiRealisasi = $rs_rinciandetail->getCekRealisasi($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiKontrak = $rs_rinciandetail->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiSwakelola = $rs_rinciandetail->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                if ($totNilaiRealisasi > 0) {
                    $nilairealisasi = $totNilaiRealisasi;
                } else if ($totNilaiKontrak > 0) {
                    $nilairealisasi = $totNilaiKontrak;
                } else if ($totNilaiSwakelola > 0) {
                    $nilairealisasi = $totNilaiSwakelola;
                } else if ($totNilaiSwakelola > 0 && $totNilaiKontrak > 0) {
                    $nilairealisasi = $totNilaiSwakelola;
                }
                }
                ?>
                <td style="text-align: right"><?php echo number_format($nilairealisasi, 0, ',', '.') ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    </table>    
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'admin/krkaRevisi?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "admin/krkaRevisi?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'admin/krkaRevisi?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>