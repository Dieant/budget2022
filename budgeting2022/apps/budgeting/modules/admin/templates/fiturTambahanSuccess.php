<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pengaturan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
          <li class="breadcrumb-item active">Fitur Tambahan</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<?php include_partial('admin/menufitur') ?>
<section class="content">
    <?php include_partial('admin/list_messages') ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Fitur</h3>
              </div>
              <div class="card-body">
                    <!-- <div class="row">
                        <div class="col-sm">
                            <fieldset>
                                  <legend>Sinkron Data</legend>
                                  <table class="table table-striped">
                                      <tbody>
                                          <tr>
                                              <td><font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Kegiatan Murni dengan Devplan</font></td>                    
                                              <td><p><?php //echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKegiatanMurni'); ?></p></td>
                                          </tr>
                                      </tbody>
                                  </table>
                            </fieldset>
                        </div>
                    </div> -->
                <table class="table table-striped">
                    <tbody>
<!--                <tr class="sf_admin_row_0" >
                    <td style="width: 80%; text-align: justify">
                        <font style="font-weight: bold; font-size: 16px">Masukin Data Musrenbang</font><br/>
                        <p>Fitur ini untuk mengeksekusi data dari tabel <b>public.usulan_kecamatan_test</b> yang didapat dari <b>Mas Amir Bappeko (0896-7756-3100)</b>. Untuk saat ini terhubung dengan tabel pada database <b>gis_budgeting</b> yang digunakan pada aplikasi <b>gis</b>. 
                        <b>HANYA UNTUK AWAL TAHUN !!</b>.</p>
                    </td>
                    <td style="text-align: center"><?php echo link_to('<b>Eksekusi Musrenbang</b>', 'admin/masukinMusrenbang') ?></td>
                </tr>
                <tr class="sf_admin_row_1" >
                    <td style="width: 80%; text-align: justify">
                        <font style="font-weight: bold; font-size: 16px">Masukin Data Rekening</font><br/>
                        <p>Fitur ini untuk mengeksekusi data rekening baru <b>(pada /apps/budgeting/modules/admin/actions/rekening_2015_fix.xls)</b> untuk penyusunan aplikasi tahun depan. <b>HANYA UNTUK AWAL TAHUN DAN BILA TERJADI PERGANTIAN REKENING !!</b>.</p>
                    </td>
                    <td style="text-align: center"><?php echo link_to('<b>Eksekusi Data Rekening</b>', 'admin/masukinDataRekening') ?></td>
                </tr>                -->
<!--                <tr class="sf_admin_row_1" >
                    <td style="width: 80%; text-align: justify">
                        
                    </td>
                    <td style="text-align: center">
                <?php echo link_to('<b>Eksekusi Estimasi</b>', 'admin/masukinEstimasi') ?><br/>
                <?php echo link_to('<b>Eksekusi Koordinat</b>', 'admin/masukinKoordinat') ?><br/>
                <?php echo link_to('<b>Eksekusi Koordinat</b>', 'admin/masukinKomponen') ?><br/>
                <?php echo link_to('<b>Eksekusi Kode Program</b>', 'admin/cekKodeProgram') ?>
                    </td>
                </tr>                
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Perbaikan Rekening </font>
                        <p>Fitur ini untuk menggantikan kode rekening dalam RKA sesuai template yang telah ditentukan.</p>
                    </td>                    
                    <td><?php echo form_tag('admin/perbaikiRekening', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form></td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Perbaikan Komponen RKA </font>
                        <p>Fitur ini untuk menggantikan komponen dalam RKA sesuai template yang telah ditentukan.</p>
                    </td>                    
                    <td><?php echo form_tag('admin/perbaikiKomponen', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form></td>
                </tr>-->
<!--                <tr>
                    <td>Eksekusi Masukin ke gis_2015 dari data musrenbang Pak Daniel</td>
                    <td><?php echo link_to('<b>Eksekusi Masuk GMAP</b>', 'admin/masukGMAP') ?><br/></td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Masukin Musrenbang PU </font>                        
                    </td>                    
                    <td><?php echo form_tag('admin/musrenbangPU', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form></td>
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Perbaiki Kode Rekening Pada RKA (sesuai format Mbak Dian)</font>                        
                    </td>                    
                    <td><?php echo form_tag('admin/changeRekeningRKA', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form>
                    </td>
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Perbaiki Komponen ID (EST->SSH) Pada RKA (sesuai format Mbak Dian)</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('admin/changeKomponenIdRKA', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form>                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Ganti Volume lalla</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('admin/changeKomponenAll', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form>
                        <br/>
                <?php // echo link_to('<b>Pindah kebersihan</b>', 'admin/pindahKebersihan') ?>
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Pagu Tambahan Perlengkapan</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('admin/eksekusibalikkebersihan', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>
                <?php echo link_to('<b>Eksekusi Pindah 6 Nov</b>', 'admin/pindah6nov') ?><br/>
                <?php echo link_to('<b>Eksekusi Gabung Pindah 6 Nov</b>', 'admin/gabungPindahatk6nov') ?>                                                
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Ganti Pagu</font>                        
                    </td>                    
                    <td>
                <?php echo link_to('<b>Ganti Pagu</b>', 'admin/gantiPaguKegiatan') ?>
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">EksekusipaguPerlengkapan</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('admin/eksekusipaguPerlengkapan', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
           </tr>-->     
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Ganti Tenaga Kontrak</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('admin/eksekusiTenagaKontrak', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Ganti BBM</font>                        
                    </td>                    
                    <td>
                <?php echo link_to('<b>Ganti BBM</b>', 'admin/gantiBBM') ?>
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Ganti Tenaga Kontrak</font>                        
                    </td>                    
                    <td>
                <?php echo link_to('<b>Ganti Tenaga Kontrak</b>', 'admin/gantiTenagaKontrakNovember') ?><br/>
                <?php echo form_tag('admin/eksekusiBalikTenagaKontrak', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                                                    
                <?php echo link_to('<b>Ganti Tenaga Kontrak II</b>', 'admin/gantiTenagaKontrakNovemberII'); ?><br/>
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Eksekusi Ganti Volume 2</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('admin/changeKomponenAllPart3', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form>
                        <br/>
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        Ganti ASB
                    </td>
                    <td><?php echo form_tag('admin/gantiASB', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form>
                    </td>
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">HapusKomponenRekeningByNamaHarga</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('admin/eksekusiHapusKomponenRekeningByNamaHarga', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        Ubah Mata Bor
                    </td>
                    <td>
                <?php echo link_to('<b>MATA Bor BETON</b>', 'admin/gantiMataBor'); ?><br/>
                    </td>
                </tr>-->
<!--                <tr>
                    <td>
                        Tertentu Gelugu
                    </td>
                    <td>
                <?php echo link_to('<b>Tertentu</b>', 'admin/gantiTertentu'); ?><br/>
                    </td>
                </tr>-->
<!--                <tr>
                <?php echo form_tag('admin/eksekusiGantiSemulaMenjadiInput', 'multipart=true') ?>
                    <td>
                        Sebelum<br/>
                        Id <input name="id_awal" type="text"/><br/>
                        *cek untuk komponen apa saja yang akan diubah
                    </td>
                    <td>
                        Setelah<br/>
                        Id <input name="id_akhir" type="text"/><br/>
                        Nama <input name="nama_akhir" type="text"/><br/>
                        Harga <input name="harga_akhir" type="text"/><br/>
                <?php echo submit_tag('Send') ?>
                    </td>
                    </form>
                </tr>-->
<!--                <tr>
                    <td>
                        Ambil Data SABK
                    </td>
                    <td>
                <?php echo form_tag('admin/ambilNilaiSABK') ?>
                        <select name="tahap" required >
                            <option value="rev2">Rev2</option>
                            <option value="rev1">Rev1</option>                            
                            <option value="apbd">Murni</option>
                        </select>
                        <select name="dinas" required >
                <?php
                foreach ($unit_kerja as $value) {
                    echo '<option value="' . $value->getUnitId() . '">' . $value->getUnitName() . '</option>';
                }
                ?>
                        </select>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cek Data SABK
                    </td>
                    <td>
                <?php echo form_tag('admin/cekNilaiSABK') ?>
                        <select name="tahap" required >
                            <option value="rev2">Rev2</option>
                            <option value="rev1">Rev1</option>                            
                            <option value="apbd">Murni</option>
                        </select>
                        <select name="dinas" required >
                <?php
                foreach ($unit_kerja as $value) {
                    echo '<option value="' . $value->getUnitId() . '">' . $value->getUnitName() . '</option>';
                }
                ?>
                        </select>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>-->
<!--                <tr>
                    <td>Ambil Data Devplan2016</td>
                    <td>
                        <p><?php echo link_to('<b>Tarik Kegiatan</b>', 'fiturplus/tarikDevplanKegiatan'); ?></p>
                        <p><?php echo link_to('<b>Tarik Subtitle</b>', 'fiturplus/tarikDevplanSubtitle'); ?></p>
                        <hr/>
                        <p><?php echo link_to('<b>Tarik Rincian Komponen</b>', 'fiturplus/ambilrincianbappeko'); ?></p>                        
                        <hr/>
                        <p><?php echo link_to('<b>Tarik Musrenbang</b>', 'fiturplus/ambilmusrenbang'); ?></p>
                    </td>
                </tr>-->
<!--                <tr>
                    <td>
                        CEK
                    </td>
                    <td>
                        <p><?php echo link_to('<b>tes nama jalan</b>', 'fiturplus/cekNamaJalan'); ?></p>
                    </td>
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Ambil data Pagu</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('fiturplus/uploadDataPagu', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload data Konektor Musrenbang</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('fiturplus/uploadDataKonektorMusrenbang', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload data Subtitle 23 Okt</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('fiturplus/uploadDataSubtitle', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Cek SSH dengan Buku </font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadCekSsh', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!-- <tr>
                    <td>Ubah RKA untuk update harga & pajak (biasanya untuk murni)</td>
                    <td> -->
                        <?php //echo form_tag('fiturplus/gantiTertentuByKomponen') ?>
                        <!-- Kode Komponen : <input type="text" name="id"/><br/>
                        Kode Komponen Baru: <input type="text" name="komponen_id"/><br/> -->
                       <!--  Satuan Baru : <input type="text" name="satuan"/><br/> -->
                        <!-- Harga Komponen : <input type="text" name="harga"/><br/> 
                        Pajak Komponen : <input type="number" name="pajak"/><br/> -->
                        <?php //echo submit_tag('Send') ?>
                        <?php //echo '</form>'; ?>
                    <!-- </td>
                </tr> -->
                <!-- <tr>
                    <td>
                        Upload Update Harga & Pajak Khusus SSH (biasanya untuk murni)                      
                    </td>
                    <td>
                        <?php //echo form_tag('fiturplus/uploadGantiTertentuByKomponen', 'multipart=true') ?>
                        <?php //echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 2</p>
                        <p>*urutan kolom "KomponenID-Pajak-Harga"</p>
                        <?php //echo submit_tag('Send') ?>
                        <?php //echo '</form>'; ?>
                    </td>
                </tr> -->
                <tr>
                    <td>
                        Upload Update Komponen ID - Komponen Name                     
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadGantiNamaKomponen', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "KomponenIDLama-KomponenIDBaru-KomponenNameBaru"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Upload Update Komponen ID - Komponen Tipe2                     
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadGantiTipe2Komponen', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "KomponenIDBaru-KomponenTipe2"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>                
                <tr>
                    <td>
                        Upload Update Catatan Pembahasan Revisi                     
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadGantiCatatan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "KodeSubKegiatan-Catatan"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Upload Save BA (Untuk Perbandingan g keliatan)                    
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadSaveBa', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "UnitID-KodeSubKegiatan"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload data Nilai Maks UK</font>                        
                    </td>
                    <td>                        
                        <?php echo form_tag('fiturplus/uploadDataMaksUK', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                       
                        <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload data PA mutasi 2017</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/uploadDataMutasi', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                         <p>*data komponen mulai kolom 2</p>
                        <p>*urutan kolom "UserID-UnitID-LevelID-Jabatan"</p>
                        <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
                <!--untuk eksekusi sikat UK pak dadik                
                                <tr>
                                    <td>Sikat UK</td>
                                    <td>
                                        <p><?php echo link_to('<b>Sikat UK</b>', 'fiturplus/kurangiBarjasUK'); ?></p>
                                    </td>
                                </tr>
                untuk eksekusi sikat UK pak dadik-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload PAGU KUA PPAS MURNI</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('fiturplus/uploadPAGUKUA', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
<!--                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Tambahan PAGU UK</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('fiturplus/uploadTambahPaguUK', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
<!--                <tr>
untuk eksekusi pembulatan keatas PU dengan belanja modal
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Penyesuaian Volume PU</font>                        
                    </td>                    
                    <td>                        
                <?php echo form_tag('fiturplus/uploadUpgradePU', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>
untuk eksekusi pembulatan keatas PU dengan belanja modal
                -->
                <tr>
                    <td>Eksekusi Pindah Komponen</td>
                    <td>
                        <?php echo form_tag('fiturplus/gantiPindahKomponen'); ?>
                        Kode Komponen Semula  : <input type="text" name="id_semula"/><br/>
                        Kode Komponen Menjadi : <input type="text" name="id_menjadi"/><br/>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
<!--                <tr>
                    <td>Sikat Tambahan Pagu</td>
                    <td>
                        <p><?php echo link_to('<b>Sikat Tambahan Pagu</b>', 'fiturplus/ubahTambahanPagu'); ?></p>
                    </td>
                </tr>                -->
                <!-- <tr>
                    <td>Sikat Alokasi Dana (untuk PAK atau Murni)</td>
                    <td>
                        <p><?php //echo link_to('<b>Sikat Pagu Kegiatan</b>', 'fiturplus/ubahAlokasiDana'); ?></p>
                    </td>
                </tr>                 -->
<!--                <tr>
                    <td>Sikat 12 Bulan jadi 11 bulan</td>
                    <td>
                        <p><?php echo link_to('<b>Sikat jadi 12 bulan</b>', 'fiturplus/ubah12BulanJadi11Bulan'); ?></p>
                    </td>
                </tr>        -->
<!--                <tr>
                    <td>Sikat Per Komponen Tenaga Kerja</td>
                    <td>
                <?php echo form_tag('fiturplus/ubahTenagaKerjaPerkalianEnakPerKomponen') ?>
                        Kode Komponen  : <input type="text" name="komponen_id"/><br/>
                        Harga Menjadi : <input type="text" name="harga_baru"/><br/>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>   -->
<!--                <tr>
                    <td>Sikat Per Komponen Tenaga Honda</td>
                    <td>
                <?php echo form_tag('fiturplus/ubahTenagaHonda') ?>
                        Kode Komponen  : <input type="text" name="komponen_id"/><br/>
                        Harga Menjadi : <input type="text" name="harga_baru"/><br/>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>   --> 
<!--                <tr>
                    <td>Tarik Perlengkapan</td>
                    <td>
                        <p><?php echo link_to('<b>Tarik Perlengkapan</b>', 'fiturplus/tarikPerlengkapan'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>Tarik Pendidikan</td>
                    <td>
                        <p><?php echo link_to('<b>Tarik Pendidikan</b>', 'fiturplus/tarikPendidikan'); ?></p>
                    </td>
                </tr>-->
                <!--
                -->                                
                <!--<tr>
                                    <td>Tarik Diskominfo</td>
                                    <td>
                                        <p><?php echo link_to('<b>Tarik Diskominfo</b>', 'fiturplus/tarikDiskominfo'); ?></p>
                                    </td>
                                </tr>-->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Kode Sub Kegiatan</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/uploadUbahSubKegiatan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Rekening Komponen</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/ubahRekeningKomponen', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "KomponenID-Kode Rekening Lama- Kode Rekening Baru"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Komponen Rekening</font>                        
                    </td>                    
                    <td>                        
                        <?php //echo form_tag('fiturplus/updateKomponenRekening', 'multipart=true') ?>
                        <?php //echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Kode Komponen - Kode Rekening Lama- Kode Rekening Baru"</p>
                        <?php //echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr> -->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Rekening Gradak</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/updateRekening', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Kode Rekening Lama- Kode Rekening Baru - Nama Rekening Baru"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Tambah Rekening Komponen</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/tambahRekeningKomponen', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "KomponenID - Kode Rekening Baru"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Buka Komponen by Detail Kegiatan</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/tambahBukaKomponenMurni', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Kode Detail Kegiatan"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload By Pass Validasi by Detail Kegiatan</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/tambahLepasValidasi', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Kode Detail Kegiatan"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Hapus Rekening Komponen</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/hapusRekeningKomponen', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "KomponenID - Kode Rekening Lama"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Isi Kode DPA</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/isikodeDPA', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        </form><br/>                                            
                    </td>                    
                </tr>-->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Betulkan Deskripsi Resume Rapat Masalah</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Betulkan Deskripsi</b>', 'fiturplus/betulDeskripsiResume'); ?></p>
                    </td>                    
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Urutkan Prioritas Waiting List</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Urutkan Semua</b>', 'fiturplus/urutkanPrioritasWaiting'); ?></p>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Lihat SHSD yang mirip</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Lihat SHSD Mirip</b>', 'shsd/shsdMirip'); ?></p>
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tandai Tabel Komponen (JKN,JK,JKK,Narsum,BPJS,BBM)</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Tandai Tabel Komponen</b>', 'fiturplus/tandaiKomponen'); ?></p>
                    </td>                    
                </tr>
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Ganti Link Filepath pada Usulan SPJM dan Dinas</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Ganti Link Usulan SPJM dan Dinas</b>', 'fiturplus/perbaruiLink'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kode Akrual (Kategori SHSD)</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadDataAkrual', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kode Akrual (Tabel Akrual)</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadDataAkrual2', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Masukkan ke komponen dari kategori SHSD</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Generate Kode Akrual ke Komponen (SSH)</b>', 'fiturplus/generateAkrualKomponen'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload History Pekerjaan</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadHistoryPekerjaan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload SSH Murni</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadSshMurni', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>-->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Tambahan Pagu</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadTambahanPagu', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Harga Dinas Rincian Detail 2022</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadHarga2021', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Komponen Revisi</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadUbahKomponenRevisi', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Detail kegiatan - Komp id - Komp name - Satuan - Harga - Pajak - Volume - Keterangan Koef - Volume Orang"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Ubah Volume UK 2022</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadVolumeUK2021', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>                        
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Alokasi Dana (Pagu Kegiatan)</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadAlokasiDana', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Pagu Rasionalisasi</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadPaguRasionalisasi', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data sesuai format mbak Lucky dalam format .xls*</p>
                        <p>*urutan kolom "Nama SKPD-ID Keg-Nama Kegiatan-Alokasi-Total Rasionalisasi(Menjadi)*</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload HSPK Murni</font>                        
                    </td>
                    <td>
                <?php echo form_tag('fiturplus/uploadHspkMurni', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload ASB Murni</font>                        
                    </td>   
                    <td>
                <?php echo form_tag('fiturplus/uploadAsbMurni', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>-->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Switch Rekening</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/switchrekeningrame', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Update Keterangan Koefisien</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadUpdateKeterangan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload JKN JK JKK</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadJkn', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Komponen 2.1.1.01.02.02.007.008</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadMakananAlamat', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Komponen 2.1.1.01.01.01.004.031</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadMakananOrang', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Komponen 1.1.7.01.07.05.001.024</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadMakananOrangHari', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr> -->

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tambah Komponen Revisi </font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/UploadKelurahanTambah', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>* dimulai kolom 0</p>
                        <p>IdSub - komponenid - namakomponen - satuan = volume - harga - rekening - volume orang - keterangan_koefisien - pajak</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>                                         
                    </td>                    
                </tr> 

                  <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px; color: red;">Tambah Komponen Revisi  Khusus Kosong</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/UploadKelurahan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>* dimulai kolom 0</p>
                        <p>IdSub - komponenid - namakomponen - satuan = volume - harga - rekening - volume orang - keterangan_koefisien - detail name - accres - pajak</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>                                         
                    </td>                    
                </tr> 
                
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Volume Orang</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadVolumeOrang', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Detail kegiatan - Volume Orang"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Volume Orang Anggaran</font>                        
                    </td>                    
                    <td>
                        <?php //echo form_tag('fiturplus/uploadVolumeOrangAnggaran', 'multipart=true') ?>
                        <?php //echo input_file_tag('file') ?>
                        <?php //echo submit_tag('Send') ?>
                        <?php //echo '</form>'; ?>
                    </td>
                </tr> -->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Prioritas</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadPrioritas', 'multipart=true') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "Detail kegiatan - Kode Prioritas"</p>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Ubah Inflasi Komponen (untuk Murni setelah inflasi berubah)</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/ubahInflasi') ?>
                        <?php echo input_tag('inflasi') . ' %' ?>
                        <?php echo submit_tag('Ubah') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Output Kegiatan</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadOutputKegiatan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kegiatan Murni</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('fiturplus/uploadKegiatanMurni', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Waitinglist Khusus 28 Juli 2016</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('fiturplus/uploadWaiting', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Misi Kegiatan</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('fiturplus/uploadMisiKegiatan', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Pagu PAK 2016</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('fiturplus/uploadPaguPak', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kode Kegiatan PAK 2016</font>                        
                    </td>                    
                    <td>
                <?php echo form_tag('fiturplus/uploadKodeKegiatanPak', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Jalankan Konversi Subtitle 2017</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>JOSSS KONVERSI SUBTITLE</b>', 'fiturplus/konversiSubtitle'); ?></p>
                    </td>
                </tr>-->
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Jalankan Konversi Subtitle 2017 (KHUSUS 16 NOVEMBER)</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>KONVERSI SUBTITLE 16 November 2016</b>', 'fiturplus/konversiSubtitleBaru'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kegiatan Baru 2017</font>                        
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadKegiatanBaru', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kegiatan Baru 2017 (20 November 2016)</font>                        
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadKegiatanBaru3', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Uang Kinerja Kegiatan</font>                        
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadUkKegiatan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>-->
                <!--<tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Program Baru 2017</font>                        
                    </td>
                    <td>
                <?php echo form_tag('fiturplus/uploadProgramBaru', 'multipart=true') ?>
                <?php echo input_file_tag('file') ?>
                <?php echo submit_tag('Send') ?>
                <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Pagu Kegiatan</font>                        
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadPaguKegiatanMurni', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Urusan-Program Lengkap 2017</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadUrusanProgram', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Master Sasaran-Tujuan-Misi</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadMasterSasaranTujuanMisi', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Sasaran-Tujuan-Misi Kegiatan</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadSasaranTujuanMisi', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Pindah ke Perlengkapan</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadPerkap', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Output Subtitle</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/uploadOutputSubtitle', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>-->
                
                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Jalankan Penggabungan Revisi dengan Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Persiapan PRA PAK dari Devplan</b>', 'fiturplus/praPak'); ?></p>
                    </td>
                </tr>

                

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Master Header Indikator RKA dari Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b> Mulai Sinkronisasi </b>', 'admin/HeaderRka'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Subtitle dari Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/GetSubtitle'); ?></p>
                    </td>
                </tr> -->

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Kegiatan Murni dengan Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKegiatanMurni'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Kegiatan PAK dengan Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKegiatanPak'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Kegiatan DAK dengan Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKegiatanRevisi'); ?></p>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Devplan belum ada di SSH</font>
                    </td> 
                     <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKomponenKosong'); ?></p>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Murni dari API devplan Kloter 1</font>
                    </td> 
                     <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikApiDevplan'); ?></p>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Murni dari Devplan Kloter 1</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKomponenMurni1'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Murni dari Devplan Kloter 2</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKomponenMurni2'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Murni dari Devplan Kloter 3</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKomponenMurni3'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Generate RKA Member</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/GenerateRkaMember'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Murni dari Musrenbang</font>
                    </td>
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikKomponenMusrenbang'); ?></p>
                    </td>
                </tr>

                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Komponen Uang Kinerja dari Devplan</font>
                    </td>
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikUangKinerja'); ?></p>
                    </td>
                </tr> -->

                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Output dari Devplan</font>
                    </td>
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikOutput'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Is Output Kloter 1</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikIsOutput1'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Is Output Kloter 2</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikIsOutput2'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Is Output Kloter 3</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikIsOutput3'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Is Output Kloter 4</font>
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikIsOutput4'); ?></p>
                    </td>
                </tr> -->

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Master Header RKA dari Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/MasterHeader'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Master Header DAK dari Devplan</font>                        
                    </td>                    
                    <td>
                        <p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/MasterHeaderDak'); ?></p>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Indikator dari Devplan</font>
                    </td>
                    <td>
                        <p><p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikIndikator'); ?></p>
                    </td>
                </tr>

                 <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Sinkronisasi Data Indikator DAK dari Devplan</font>
                    </td>
                    <td>
                        <p><p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikIndikatorDAK'); ?></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Pagu PAK dari Devplan</font>
                    </td>
                    <td>
                        <p><p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikPaguPak'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Tarik dan Simpan Harga Lelang LPSE</font>
                    </td>
                    <td>
                        <p><p><?php echo link_to('<b>Mulai Sinkronisasi </b>', 'admin/TarikLPSE'); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Simpan Berita Acara</font>
                    </td>
                    <td>
                        <?php echo form_tag('admin/Saveba'); ?>
                        Kode Unit  : <input type="text" name="unit_id"/><br/>
                        Kode Kegiatan : <input type="text" name="kegiatan_code"/><br/>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Update Penanda Komponen By Detail Kegiatan</font>
                    </td>
                    <td>
                        <?php echo form_tag('admin/updateJKN'); ?>
                        <table id="tbl_detailkegiatan">
                            <tbody>
                                <tr>
                                    <td colspan="3"><b><i>(jika lebih dari 1 maka pisahkan dengan tanda ',' tanpa spasi)</i></b>
                                        <p></p>
                                        <p>contoh : </p>
                                        <p>detail kegiatan : 2300.1286.13,2300.1286.14</p>
                                        <p>kolom yang diupdate : is_iuran_jkn=false,is_iuran_jkk=true,is_koma=true,is_pengecualian=true</p></td>
                                </tr>
                                <tr>
                                    <td>Detail Kegiatan</td>
                                    <td>:</td>
                                    <td><input type="text" name="detail_kegiatan"/></td>
                                </tr>
                                <tr>
                                    <td>Kolom yang diupdate</td>
                                    <td>:</td>
                                    <td><textarea name="kolom"></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo submit_tag('Send') ?></td>
                                </tr>

                            </tbody>
                        </table>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px; color: red;">Update Penanda Komponen By Upload Detail Kegiatan</font>
                    </td>
                    <td>
                        <?php echo form_tag('admin/updateJKNUpload', 'multipart=true'); ?>
                        <table id="tbl_detailkegiatan">
                            <tbody>
                                <tr>
                                    <td colspan="3"><b><i>(jika lebih dari 1 maka pisahkan dengan tanda ',' tanpa spasi)</i></b>
                                        <p></p>
                                        <p>contoh : </p>
                                        <p>*data detail_kegiatan mulai kolom A dan baris 1</p>
                                        <p>*urutan kolom "detail_kegiatan"</p>
                                        <p>kolom yang diupdate : is_iuran_jkn=false,is_iuran_jkk=true,is_koma=true,is_pengecualian=true</p></td>
                                </tr>
                                <tr>
                                    <td>File detail kegiatan</td>
                                    <td>:</td>
                                    <td><?php echo input_file_tag('file') ?></td>
                                </tr>
                                <tr>
                                    <td>Kolom yang diupdate</td>
                                    <td>:</td>
                                    <td><textarea name="kolom"></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo submit_tag('Send') ?></td>
                                </tr>

                            </tbody>
                        </table>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <!-- <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Generate Query ke Excel</font>
                    </td>
                    <td>
                        <?php //echo form_tag('admin/generateExcelquery'); ?>
                        <table id="tbl_generatequery">
                            <tbody>
                                <tr>
                                    <td>Nama File</td>
                                    <td>:</td>
                                    <td><input type="text" name="nama_file"/></td>
                                </tr>
                                <tr>
                                    <td>Judul File</td>
                                    <td>:</td>
                                    <td><input type="text" name="judul_file"/></td>
                                </tr>
                                <tr>
                                    <td>Masukkan Query</td>
                                    <td>:</td>
                                    <td><textarea name="query"></textarea></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php //echo submit_tag('Generate Excel') ?></td>
                                </tr>

                            </tbody>
                        </table>
                        <?php //echo '</form>'; ?>
                    </td>
                </tr> -->
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Sub Kegiatan (Pendapatan)</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadSubKegiatan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload User Baru</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/tambahUserBaru', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data user mulai kolom 0</p>
                        <p>*urutan kolom "Level User - User ID - Username - Password - SKPD"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Update User ID By Kode Kegiatan</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/updateUserIdByKodeKegiatan', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data user mulai kolom 0</p>
                        <p>*urutan kolom "Kode Kegiatan - User ID"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Update Lock By Kode Kegiatan</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/updateLockFalse', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data kode kegiatan mulai kolom 0</p>
                        <p>*urutan kolom "Kode Kegiatan - Lock(1/0) - Rincian Level (2->Dinas, 3->Peneliti)"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Update Level ID by User ID</font>                        
                    </td>                    
                    <td>                        
                        <?php echo form_tag('fiturplus/updateUserLevel', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data mulai kolom 0</p>
                        <p>*urutan kolom "User ID - Level ID"</p>
                        <?php echo submit_tag('Send') ?>
                        </form>                                           
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Upload Kunci Komponen</font>                        
                    </td>                    
                    <td>
                        <?php echo form_tag('fiturplus/uploadKunciKomponen', 'multipart=true') ?>
                        <?php echo input_file_tag('file') ?>
                        <p>*data komponen mulai kolom 0</p>
                        <p>*urutan kolom "komponen_id"</p>
                        <?php echo submit_tag('Send') ?>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font style="font-weight: bold; font-size: 16px">Ubah User ID</font>
                    </td>
                    <td>
                        <?php echo form_tag('fiturplus/ubahUserid'); ?>
                        <table id="tbl_userid">
                            <tbody>
                                <tr>
                                    <td>Nama User ID Lama</td>
                                    <td>:</td>
                                    <td><input type="text" name="user_id_lama"/></td>
                                </tr>
                                <tr>
                                    <td>Nama User ID Baru</td>
                                    <td>:</td>
                                    <td><input type="text" name="user_id_baru"/></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><?php echo submit_tag('Ubah') ?></td>
                                </tr>

                            </tbody>
                        </table>
                        <?php echo '</form>'; ?>
                    </td>
                </tr>
        </table>
        <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
    </div>
</div>
</div>
</div>
</div>
</section>
