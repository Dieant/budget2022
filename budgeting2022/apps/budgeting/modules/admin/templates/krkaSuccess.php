<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Komponen Kertas Kerja</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Komponen Kertas Kerja</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('admin/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('admin/krka', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Perangkat Daerah</label>
                            <?php
                                $c = new Criteria();
                                $c->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
                                $v = UnitKerjaPeer::doSelect($c);
                                echo select_tag('filters[unit_id]', objects_for_select($v, 'getUnitId', 'getUnitName', isset($filters['unit_id']) ? $filters['unit_id'] : null, array('include_custom' => '--Pilih Perangkat Daerah--')), array('class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Pilihan</label>
                            <?php
                                $arrOptions = array(1 => 'Nama Komponen', 2 => 'Kode Komponen', 3 => 'Rekening');
                                echo select_tag('filters[select]', options_for_select($arrOptions, @$filters['select'] ? $filters['select'] : '', array('include_custom' => '--Pilihan--')), array('class' => 'form-control select2'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>Action</label>
                            <?php
                                echo input_tag('filters[komponen]', isset($filters['komponen']) ? $filters['komponen'] : null, array('class' => 'form-control'));
                            ?>
                        </div>
                      </div>
                      <!-- /.col -->
                      <div class="col-md-3">
                        <div class="form-group">
                            <label class="tombol_filter">Tombol Filter</label><br/>
                            <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                            <?php
                                echo link_to('Reset <i class="fas fa-backspace"></i>', 'admin/krka?filter=filter', array('class' => 'btn btn-outline-danger btn-sm')).'&nbsp';
                                echo link_to('Export to Excel <i class="fas fa-file-excel"></i>', 'admin/krkaExcel', array('class' => 'btn btn-outline-info btn-sm', 'target' => '_blank'));
                            ?>
                        </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">                 
                    <?php if (!$pager->getNbResults()): ?>
                    <?php echo __('no result') ?>
                    <?php else: ?>
                        <?php include_partial("admin/krka", array('pager' => $pager)) ?>
                    <?php endif; ?>            
                </div>
            </div>
        </div>
    </div>
</section>
