<?php if ($sf_request->getError('delete')): ?>
    <script type="text/javascript">
        $(document).ready(function () {
          toastr.error('Alert! <br/><?php echo __($sf_flash->get('delete')); ?>')
        });
    </script>
<?php elseif ($sf_flash->has('berhasil')): ?>
    <script type="text/javascript">
        $(document).ready(function () {
          toastr.success('Success! <br/><?php echo __($sf_flash->get('berhasil')); ?>')
        });
    </script>
<?php elseif ($sf_flash->has('gagal')): ?>
    <script type="text/javascript">
        $(document).ready(function () {
          toastr.error('Alert! <br/><?php echo __($sf_flash->get('gagal')); ?>')
        });
    </script>
<?php elseif ($sf_flash->has('warning')): ?>
    <script type="text/javascript">
        $(document).ready(function () {
          toastr.warning('Alert! <br/><?php echo __($sf_flash->get('warning')); ?>')
        });
    </script>
<?php endif; ?>
