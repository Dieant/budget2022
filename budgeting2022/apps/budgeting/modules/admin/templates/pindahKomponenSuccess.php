<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php use_helper('I18N', 'Date') ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>

<div id="sf_admin_container">

<h1><?php echo __('Pindah Komponen',
array()) ?>
<?php echo form_tag('admin/pindahKomponen', array('method' => 'get')) ?>
</h1>
    <fieldset>
    <h2><?php echo __('Tujuan pindah') ?></h2>
    <div class="sf_admin_filters">
        <div class="form-row">
            <label for="satuan_kerja"><?php echo __('Satuan Kerja :') ?></label>
            <div class="content" style="min-height: 25px">
                <?php 
                echo select_tag('criteria', objects_for_select($unit_kerja,'getUnitId', 'getUnitName',
                        $unit_idDinas,array('include_custom'=>'Pilih SKPD')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isiKeg', 'url'=>'admin/ajax?tahap=kegiatan',
                        'with'=>"'b=unit_id_tujuan&unit_id='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
                ?> 
            </div>
        </div>
    </div>
    <div class="sf_admin_filters">
        <div class="form-row">
            <label for="satuan_kerja"><?php echo __('Kegiatan :') ?></label>
            <div class="content" style="min-height: 25px">
                <div id="isiKeg">
                    <?php
                    //print_r($unit_idDinas.' '.$nama_unit);
                        echo select_tag('kegiatan', objects_for_select($kegiatan,'getKodeKegiatan', 'getKodeNamaKegiatan',
                        '',array('include_custom'=>'Pilih Kegiatan')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isiSubtitle', 'url'=>'admin/ajax?unit_id='.$unit_id.'&tahap=subtitle',
                        'with'=>"'b=b=kegiatan_tujuan&kegCode='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="sf_admin_filters">
        <div class="form-row">
            <label for="satuan_kerja"><?php echo __('Subtitle :') ?></label>
            <div class="content" style="min-height: 25px">
                <div id="isiSubtitle">
                    <?php
                    echo select_tag('subtitle', objects_for_select($subtitleTujuan,'getSubtitle', 'getSubtitle',
                        '',array('include_custom'=>'Pilih Subtitle')),
                         Array('id'=>'combo_pilih','onChange'=>remote_function(Array('update'=>'isi', 'url'=>'admin/ajax?unit_id='.$unit_id.'&kegiatan_code='.$kegiatan_code.'&tahap='.$tahap,
                        'with'=>"'b=subtitle_tujuan&subtitle='+this.options[this.selectedIndex].value",
                        'loading'=>"Element.show('indicator');Element.hide('isi');Element.hide('isi_dalam');",
                        'complete'=>"Element.hide('indicator');Element.show('isi');".visual_effect('highlight', 'isi')))));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <h2><?php echo __(' ') ?></h2>
    <div class="sf_admin_filters">
        <div class="form-row">
            <div id="isi2">
            <table  border="1" cellpadding="0" cellspacing="0" cellspacing="0" class="sf_admin_list">
              <tr>
                <td><div align="center"><strong></strong></div></td>
                <td width="10%"><div align="center"><strong><h2>Komponen Id</h2></strong></div></td>
                <td width="15%"><div align="center"><strong><h2>Komponen Name</h2></strong></div></td>
                <td width="15%"><div align="center"><strong><h2>Satuan</h2></strong></div></td>
                <td width="15%"><div align="center"><strong><h2>keterangan Koefisien</h2></strong></div></td>
                <td width="15%"><div align="center"><strong><h2>Harga</h2></strong></div></td>
                <td width="15%"><div align="center"><strong><h2>pajak</h2></strong></div></td>
                <td width="15%"><div align="center"><strong><h2>Total</h2></strong></div></td>
              </tr>
              <?php
              while($r_rincianDetail->next())
              {
              ?>
                  <tr>
                    <td width="3%"><div align="center"><?php echo checkbox_tag('komponen[]',$r_rincianDetail->get(1))?></div></td>  
                    <td width="10%"><div align="center"><?php echo $r_rincianDetail->get(2);?></div></td>
                    <td width="15%"><div align="center"><?php echo $r_rincianDetail->get(3);?></div></td>
                    <td width="15%"><div align="center"><?php echo $r_rincianDetail->get(4);?></div></td>
                    <td width="15%"><div align="center"><?php echo $r_rincianDetail->get(5);?></div></td>
                    <td width="15%"><div align="center"><?php echo $r_rincianDetail->get(6);?></div></td>
                    <td width="15%"><div align="center"><?php echo $r_rincianDetail->get(7);?></div></td>
                    <td width="15%"><div align="center"><strong><?php $rd = new RincianDetail();
                                                                    echo number_format($rd->getTotalNilaiKomponenBdsrDetailNo($unit_id_asal, $kegiatan_code_asal, $r_rincianDetail->get(1)),0,",",".");?>
                            </strong></div></td>
                  </tr>
              <?php
              }
              ?>
            </table>
            </div>
        </div>
    </div>
    <h2><?php echo __(' ') ?></h2>
    <div class="sf_admin_filters">
        <div class="form-row">
            <?php echo button_to('Ekekusi','admin/pindahKomponen?act=eksekusiPindah')?>
        </div>
    </div>
    <h2><?php echo __(' ') ?></h2>
    </fieldset>
    
    
</div>