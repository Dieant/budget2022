<?php use_helper('Javascript', 'Number') ?>
<?
	if($sf_params->get('print'))
{
	echo use_stylesheet('/css/tampilan_print2.css');
	echo javascript_tag("window.print();");
}
?>
        <table cellspacing="0" class="sf_admin_list" width="100%">
           <tr>
		  <td  align="center" bgcolor="#9999FF"  width="5"><strong></strong></td>
            <td width="97"  align="center" bgcolor="#9999FF"><strong>Kode Unit </strong></td>
            <td width="527" align="center" bgcolor="#9999FF"><strong>Nama Unit </strong></td>
            <td width="293" align="center" bgcolor="#9999FF"><strong>Nilai Usulan </strong></td>
            <td width="293" align="center" bgcolor="#9999FF"><strong>% Usulan </strong></td>
            <td width="271" align="center" bgcolor="#9999FF"><strong>Nilai Disetujui </strong></td>
            <td width="293" align="center" bgcolor="#9999FF"><strong>% Disetujui </strong></td>

          </tr>
		  
		   <?php $i=0; $rs->first();$unit=''; do{ ?>
		  
          <?php if(isset($dinas_ok[$i])=='t'){ ?>
		  <tr><td colspan="7"  bgcolor="#FFFFFF"  >&nbsp;</td></tr>
           <tr>
            <td colspan="3" align="left" bgcolor="#FFFFFF" ><strong><?php echo $rs->getString('old_belanja_id').'&nbsp;'.$rs->getString('old_belanja_name'); ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $nilai_draft[$i]; ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $persen_draft[$i]; ?>%</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $nilai_locked[$i]; ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $persen_locked[$i]; ?>%</strong></td>
          </tr>
          <?php }
		if($unit!=$rs->getString('unit_id'))
		{
			//print_r($rs->getString('unit_id'));exit;
			$unit=$rs->getString('unit_id');
			$belanja_old=$rs->getString('old_belanja_id');
			$sql = "select belanja_id as old_belanja_id, belanja_name as old_belanja_name,unit_id,unit_name ,sum((jumlah_draft*jumlah_non_lanjutan)) as total_draft3,sum((jumlah_setuju*jumlah_non_lanjutan)) as total_locked3, sum((nilai_draft*jumlah_non_lanjutan)) as nilai_draft3, sum((nilai_draft*jumlah_setuju*jumlah_non_lanjutan)) as nilai_locked3 
				FROM ". sfConfig::get('app_default_schema') .".v_kegiatan_rekening WHERE unit_id<>'9999' and unit_id='$unit' and belanja_id='$belanja_old'
				GROUP BY belanja_id,belanja_name,unit_id,unit_name ORDER BY belanja_id,belanja_name,unit_id,unit_name";
			//print_r($sql);exit;
				$con=Propel::getConnection();
				$stmt=$con->prepareStatement($sql);
				$jr=$stmt->executeQuery();
				while($jr->next())
			{
				$nilai_draft14 = $jr->getString('nilai_draft3');
				$persen_draft14 = $nilai_draft14 * 100 / $sum_draft;
				$nilai_locked14 = $jr->getString('nilai_locked3');
				$persen_locked14 = $nilai_locked14 * 100 / $sum_locked;

			}
 ?>
         <tr>
            <td  align="center" bgcolor="#FFFFFF"  width="5"><strong></strong></td>
          <td  align="center" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('unit_id') ?> </td>
            <td align="left" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('unit_name') ?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($nilai_draft14,0,",","."); ?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($persen_draft14,2,",","."); ?> %</td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($nilai_locked14,0,",",".");?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($persen_locked14,2,",",".");; ?> %</td>
          </tr>
		 <tr>
            <td  align="center" bgcolor="#FFFFFF"  width="5"><strong></strong></td>
          <td  align="center" bgcolor="#FFFFFF" class="Font8v"><strong></strong> </td>
            <td align="left" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('kode_kegiatan').' '.$rs->getString('nama_kegiatan') ?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs->getString('nilai_draft3'),0,",","."); ?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $persen_draft3[$i]; ?> %</td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs->getString('nilai_locked3'),0,",",".");?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $persen_locked3[$i]; ?> %</td>
          </tr>
          <?php }
		else
		{?>
         <tr>
            <td  align="center" bgcolor="#FFFFFF"  width="5"><strong></strong></td>
          <td  align="center" bgcolor="#FFFFFF" class="Font8v"><strong></strong> </td>
            <td align="left" bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('kode_kegiatan').' '.$rs->getString('nama_kegiatan') ?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs->getString('nilai_draft3'),0,",","."); ?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $persen_draft3[$i]; ?> %</td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo number_format($rs->getString('nilai_locked3'),0,",",".");?> </td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><?php echo $persen_locked3[$i]; ?> %</td>
          </tr>
          <?php
		}$i++; }while($rs->next()); ?>
		  <tr>
            <td colspan="3" bgcolor="#FFFFFF" class="Font8v"><strong>Total</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $nilai_draft2 ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>100%</strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong><?php echo $nilai_locked2 ?></strong></td>
            <td align="right" bgcolor="#FFFFFF" class="Font8v"><strong>100%</strong></td>
          </tr>
        </table>