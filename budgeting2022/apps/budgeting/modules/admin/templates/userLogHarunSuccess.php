<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Filter Log</h1>
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('admin/list_messages') ?>

    <!-- Default box -->
    <div class="box box-solid box-primary">
        <div class="box-body">
            <div id="sf_admin_container" class="table-responsive">                
                <table cellspacing="0" class="sf_admin_list">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User Name</th>
                            <th>Nama Lengkap</th>
                            <th>Waktu</th>
                            <th>TIndakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($filters as $filter): $odd = fmod(++$i, 2)
                            ?>
                            <tr class="sf_admin_row_<?php echo $odd ?>">
                                <td><?php echo $filter->getId() ?></td>
                                <td><?php echo $filter->getNamaUser(); ?></td>
                                <td><?php echo $filter->getNamaLengkap() ?></td>
                                <td><?php echo $filter->getWaktu() ?></td>
                                <td><?php echo $filter->getDeskripsi(); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section><!-- /.content -->