<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<p align="right">
    <?php
    echo button_to('KOMPONEN', 'admin/krka') . ' | ';
    $nama = $sf_user->getNamaLogin();
    if (($nama == 'admin')) {
        echo button_to('SATUAN', 'admin/satuanlist') . ' | ';
    }
    ?>
</p>