<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = $sf_user->getAttribute('status', '', 'status_admin');
$i = 0;
$kode_sub = '';
$temp_rekening = '';
?>

<?php
foreach ($rs_rd as $rd):
    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();


    if ($kode_sub != $rd->getKodeSub()) {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') {//RKA Member
            $C_RKA = new Criteria();
            $C_RKA->add(RkaMemberPeer::KODE_SUB, $kode_sub);
            $rs_rkam = RkaMemberPeer::doSelectOne($C_RKA);
            //print_r($rs_rkam);exit;
            if ($rs_rkam) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="8"><?php
                        if (($status == 'OPEN') and ($rd->getLockSubtitle <> 'LOCK')) {
//                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                            echo link_to_remote(image_tag('/sf/sf_admin/images/edit.png'), array('alt' => 'Mengubah Nama', 'title' => 'Mengubah nama', 'update' => 'header_' . $rs_rkam->getKodeSub(), 'url' => 'kegiatan/ajaxEditHeader?act=editHeader&kodeSub=' . $rs_rkam->getKodeSub() . '&unitId=' . $unit_id . '&kegiatanCode=' . $kegiatan_code));

                            //echo link_to_remote(image_tag('/sf/sf_admin/images/delete.png'),
                            //      array('alt'=>'Menghapus Header','title' => 'menghapus Header','update'=>'header_'.$rs_rkam->getKodeSub(), 'url'=>'kegiatan/hapusHeaderKegiatan?act=HapusHeader&no='.$rs_rkam->getKodeSub().'&unit='.$unit_id.'&kegiatan='.$kegiatan_code));
                            echo link_to_function(image_tag('/sf/sf_admin/images/cancel.png'), 'hapusHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                        }
                        ?>
                        <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                    <td>&nbsp;</td>
                </tr>
                <?php
            } else {
                
            }
        } else {

            $c = new Criteria();
            $c->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = RincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                    <td colspan="8"><?php
                        if ($status == 'OPEN') {
                            if (($rs_subparameter->getFromSubKegiatan() == 'PJU001') or
                                    ($rs_subparameter->getFromSubKegiatan() == '29.99.01.01') or
                                    ($rs_subparameter->getFromSubKegiatan() == '29.99.01.02') or
                                    ($rs_subparameter->getFromSubKegiatan() == '29.99.01.03')) {
                                echo link_to_function(image_tag('/sf/sf_default/images/icons/folder16.png'), 'if (confirm("Apakah anda yakin untuk me-nol kan Subtitle ini?")){satukanSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")}');
                            }
//                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                            //untuk gabung
                            //   echo link_to(image_tag('/images/gabung.png'), "kegiatan/gabungKomponen?unit_id=$unit_id&kegiatan_code=$kegiatan_code&kode_sub=$kode_sub&edit=".md5('gabung'));
                        }
                        ?> <b>
                            :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b></td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?></td>
                    <td>&nbsp;</td>
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                //print_r($query);exit;
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) {
                    if ($t->getString('kode_sub')) {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                            <td colspan="8"><?php
                                if ($status == 'OPEN') {
//                                    echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $t->getString('kode_sub') . '")');
                                }
                                ?> <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b></td>
                            <td align="right">
                                <?php
                                $query2 = "select sum(nilai_anggaran) as hasil_kali
								from " . sfConfig::get('app_default_schema') . ".rincian_detail
								where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";

                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query2);
                                $t = $stmt->executeQuery();
                                while ($t->next()) {
                                    echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                }
                                $ada = 'ada';
                                ?> </td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    }
                }

                if ($ada == 'tidak') {
                    if ($kode_sub != '') {
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail
                                                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                        //print_r($query);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) {
                            if ($t->getString('kode_sub')) {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                                    <td colspan="8"><?php
                                        if ($status == 'OPEN') {
//                                            echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'), 'hapusSubKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $t->getString('kode_sub') . '")');
                                        }
                                        ?> <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b></td>

                                    <td align="right">
                                        <?php
                                        $query2 = "select sum(nilai_anggaran) as hasil_kali
										from " . sfConfig::get('app_default_schema') . ".rincian_detail
										where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub ilike '%$sub%' and status_hapus=false";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query2);
                                        $t = $stmt->executeQuery();
                                        while ($t->next()) {
                                            echo number_format($t->getString('hasil_kali'), 0, ',', '.');
                                        }
                                        $ada = 'ada';
                                    }
                                    ?>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
                        }
                    }
                }
            }
        }
    } elseif (!$rd->getKodeSub()) {
        $kode_sub = '';
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        //$rs_rekening = RekeningPeer::retrieveByPk($rekening_code);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            $subtitle_name = $rd->getSubtitle();
            $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . ".rincian_detail
                                where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub='$kode_sub' and status_hapus=false";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query_rekening);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                $jumlah_rekening = number_format($t->getString('jumlah_rekening'), 0, ',', '.');
                if ($t->getString('jumlah_rekening') == 0) {
                    $query_rekening = "select sum(nilai_anggaran) as jumlah_rekening from " . sfConfig::get('app_default_schema') . ".rincian_detail
                                            where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle_name' and rekening_code='$rekening_code' and kode_sub isnull and status_hapus=false";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query_rekening);
                    $ts = $stmt->executeQuery();
                    while ($ts->next()) {
                        $jumlah_rekening = number_format($ts->getString('jumlah_rekening'), 0, ',', '.');
                    }
                }
            }
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#AFC4EF">
                <td colspan="8"><i><?php echo $rekening_code . ' ' . $rekening_name; ?></i> </td>
                <td align="right"><?php echo $jumlah_rekening ?></td>
                <td>&nbsp;</td>
            </tr>
            <?php
        }
    }

    if ($sf_user->getNamaUser() != 'parlemen2') {
        ?>
        <tr class="pekerjaans_<?php echo $id ?>">
            <td>
                <?php
                $kegiatan = $rd->getKegiatanCode();
                $unit = $rd->getUnitId();
                $no = $rd->getDetailNo();
                $sub = $rd->getSubtitle();
                $komponen_id = $rd->getKomponenId();


                //irul 6oktober 2014 - cek musrenbang
                $benar_musrenbang = 0;

                $query_cek_musrenbang = "select count(*) as ada "
                        . "from " . sfConfig::get('app_default_schema') . ".musrenbang_rka "
                        . "where unit_id = '" . $rd->getUnitId() . "' and kegiatan_code = '" . $rd->getKegiatanCode() . "' and detail_no = " . $rd->getDetailNo() . " ";
                $con = Propel::getConnection();
                $stmt_cek_musrenbang = $con->prepareStatement($query_cek_musrenbang);
                $t_cek_musrenbang = $stmt_cek_musrenbang->executeQuery();
                while ($t_cek_musrenbang->next()) {
                    $ada_musrenbang = $t_cek_musrenbang->getString('ada');
                }

                $query_cek_musrenbang2 = "select is_musrenbang "
                        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "where unit_id = '" . $rd->getUnitId() . "' and kegiatan_code = '" . $rd->getKegiatanCode() . "' and detail_no = " . $rd->getDetailNo() . " ";
                $con = Propel::getConnection();
                $stmt_cek_musrenbang2 = $con->prepareStatement($query_cek_musrenbang2);
                $t_cek_musrenbang2 = $stmt_cek_musrenbang2->executeQuery();
                while ($t_cek_musrenbang2->next()) {
                    $is_musrenbang = $t_cek_musrenbang2->getString('is_musrenbang');
                }

                if ($is_musrenbang == true and $ada_musrenbang >= 1) {
                    $benar_musrenbang = 1;
                }
                //irul 6oktober 2014 - cek musrenbang
                //irul 6oktober 2014 - cek multiyears
                $benar_multiyears = 0;

                $query_cek_multiyears = "select th_ke_multiyears, kegiatan_code_asal "
                        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "where unit_id = '" . $rd->getUnitId() . "' and kegiatan_code = '" . $rd->getKegiatanCode() . "' and detail_no = " . $rd->getDetailNo() . " ";
                $con = Propel::getConnection();
                $stmt_cek_multiyears = $con->prepareStatement($query_cek_multiyears);
                $t_cek_multiyears = $stmt_cek_multiyears->executeQuery();
                while ($t_cek_multiyears->next()) {
                    $th_ke_multiyears = $t_cek_multiyears->getString('th_ke_multiyears');
                    $kegiatan_code_asal = $t_cek_multiyears->getString('kegiatan_code_asal');

                    if ($th_ke_multiyears > 0 && $kegiatan_code_asal != '' && $kegiatan_code_asal != NULL) {
                        $benar_multiyears = 1;
                    }
                }
                //irul 6oktober 2014 - cek multiyears

                if ($status == 'OPEN') {
                    $query = "select distinct rekening_code from " . sfConfig::get('app_default_schema') . ".komponen_rekening where komponen_id='$komponen_id' and rekening_code<>'$rekening_code' ";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $ts = $stmt->executeQuery();
                    $pilih = array();
                    $ada = false;
                    $select_str = "<select name='rek_$no'>";
                    while ($ts->next()) {
                        $ada = true;
                        $r = $ts->getString('rekening_code');
                        $select_str.="<option value='$r'>$r</option>";
                    }
                    $select_str.="</select>";
                    if ($ada) {
                        //echo form_tag("kegiatan/gantiRekening?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                        //echo	$select_str;
                        //echo submit_tag('ganti');
                        //echo "</form><br>";
                    }
                    //echo link_to('buka edit harga','kegiatan/bukaeditharga?id='.$rs_rinciandetail->getString('detail_no').'&unit='.$rs_rinciandetail->getString('unit_id').'&kegiatan='.$rs_rinciandetail->getString('kegiatan_code').'&edit='.md5('ubah'));
                }


                if ($rd->getFromSubKegiatan() == '') {
                    if ($status == 'OPEN') {
//                        echo link_to(image_tag('/sf/sf_admin/images/delete.png'), 'kegiatan/hapusPekerjaans?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), array('confirm' => 'Yakin untuk menghapus komponen ini?')) . '&nbsp;';
                        //echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'",'.$no.')');
                        echo link_to(image_tag('/sf/sf_admin/images/edit.png'), 'kegiatan/editKegiatan?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'));
                        //}
                        //irul 25agustus 2014 - fungsi GMAP
                        if ($rd->getTipe() == 'FISIK') {
                            $id_kelompok = 0;
                            $tot = 0;
                            $con = Propel::getConnection();
                            $query = "select count(*) as tot from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                            $stmt = $con->prepareStatement($query);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $tot = $rs->getString('tot');
                            }
                            if ($tot == 0) {
                                $con = Propel::getConnection();
                                $c2 = new Criteria();
                                $c2->add(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE);
                                $c2->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK', Criteria::EQUAL);
                                $rd2 = KomponenPeer::doSelectOne($c2);
                                if ($rd2) {
                                    $komponen_id = $rd2->getKomponenId();
                                } else {
                                    $komponen_id = '0';
                                }

                                if ($komponen_id == '0') {
                                    $query2 = "select * from master_kelompok_gmap where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                                } else {
                                    $query2 = "select * from master_kelompok_gmap where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                                }
                                $stmt2 = $con->prepareStatement($query2);
                                $rs2 = $stmt2->executeQuery();
                                while ($rs2->next()) {
                                    $id_kelompok = $rs2->getString('id_kelompok');
                                }

                                if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                                    $id_kelompok = 19;
                                }
                            } else {
                                $con = Propel::getConnection();
                                $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                while ($rs->next()) {
                                    $mlokasi = $rs->getString('mlokasi');
                                    $id_kelompok = $rs->getString('id_kelompok');
                                }
                            }


                            if ($tot == 0) {
                                $array_bersih_kurung = array('(', ')');
                                $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                                echo '  ' . link_to(image_tag('/sf/sf_admin/images/map.png'), sfConfig::get('app_path_gmap') . 'insertBaru.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser());
                            } else {
                                echo '  ' . link_to(image_tag('/sf/sf_admin/images/map.png'), sfConfig::get('app_path_gmap') . 'updateData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser());
                            }
                        }
                        //irul 25agustus 2014 - fungsi GMAP
                    }
                } elseif ($rd->getFromSubKegiatan() != '') {
                    if ($status == 'OPEN') {
//                        echo link_to(image_tag('/sf/sf_admin/images/delete.png'), 'kegiatan/hapusPekerjaans?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), array('confirm' => 'Yakin untuk menghapus komponen ini?')) . '&nbsp;';
                        //echo link_to_function(image_tag('/sf/sf_admin/images/delete.png'),'hapusKegiatan('.$id.',"'.$kegiatan.'","'.$unit.'",'.$no.')');
                        echo link_to(image_tag('/sf/sf_admin/images/edit.png'), 'kegiatan/editKegiatan?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'));
                        //}
                        //irul 25agustus 2014 - fungsi GMAP
                        if ($rd->getTipe() == 'FISIK') {
                            $id_kelompok = 0;
                            $tot = 0;
                            $con = Propel::getConnection();
                            $query = "select count(*) as tot from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                            $stmt = $con->prepareStatement($query);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $tot = $rs->getString('tot');
                            }
                            if ($tot == 0) {
                                $con = Propel::getConnection();
                                $c2 = new Criteria();
                                $c2->add(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE);
                                $c2->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK', Criteria::EQUAL);
                                $rd2 = KomponenPeer::doSelectOne($c2);
                                if ($rd2) {
                                    $komponen_id = $rd2->getKomponenId();
                                } else {
                                    $komponen_id = '0';
                                }

                                if ($komponen_id == '0') {
                                    $query2 = "select * from master_kelompok_gmap where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                                } else {
                                    $query2 = "select * from master_kelompok_gmap where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                                }
                                $stmt2 = $con->prepareStatement($query2);
                                $rs2 = $stmt2->executeQuery();
                                while ($rs2->next()) {
                                    $id_kelompok = $rs2->getString('id_kelompok');
                                }

                                if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                                    $id_kelompok = 19;
                                }
                            } else {
                                $con = Propel::getConnection();
                                $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                                    where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                $stmt = $con->prepareStatement($query);
                                $rs = $stmt->executeQuery();
                                while ($rs->next()) {
                                    $mlokasi = $rs->getString('mlokasi');
                                    $id_kelompok = $rs->getString('id_kelompok');
                                }
                            }


                            if ($tot == 0) {
                                $array_bersih_kurung = array('(', ')');
                                $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                                echo '  ' . link_to(image_tag('/sf/sf_admin/images/map.png'), sfConfig::get('app_path_gmap') . 'insertBaru.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser());
                            } else {
                                echo '  ' . link_to(image_tag('/sf/sf_admin/images/map.png'), sfConfig::get('app_path_gmap') . 'updateData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=9&nm_user=' . $sf_user->getNamaUser());
                            }
                        }
                        //irul 25agustus 2014 - fungsi GMAP
                    }
                }
                ?>

            </td>
            <?php
            //irul 24maret 2014 - note peneliti + note skpd
            if ((($rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL) or ($rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL)) and $rd->getStatusHapus() == false) {
                //irul 24maret 2014 - note peneliti + note skpd
                ?>
                <td style="background: pink">
                    <?php
                    if ($benar_musrenbang == 1) {
                        ?>
                        <div style="background: #A8CD1B; padding: 5px; text-align: center;border: 1px solid #A8CD1B">
                            <font style="font-size: 14px; font-weight: bold">MUSRENBANG <?php echo $tahun_musrenbang ?> :: id <?php echo $id_musrenbang ?></font>    
                        </div><br/>
                        <?php
                    }
                    if ($benar_multiyears == 1) {
                        ?>
                        <div style="background: #67BCDB; padding: 5px; text-align: center;border: 1px solid #67BCDB">
                            <font style="font-size: 14px; font-weight: bold">MULTIYEARS TAHUN KE <?php echo $th_ke_multiyears ?></font>    
                        </div><br/>
                        <?php
                    }
                    if ($rd->getIsBlud() == 1) {
                        ?>
                        <div style="background: #F2C249; padding: 5px; text-align: center;border: 1px solid #F2C249">
                            <font style="font-size: 14px; font-weight: bold">BLUD</font>    
                        </div><br/>
                        <?php
                    }
                    echo $rd->getKomponenName();
                    if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                        echo ' ' . $rd->getDetailName() . ' ' . $rd->getLokasiKecamatan() . ' ' . $rd->getLokasiKelurahan();
                    }

//djiebrats : give image "new" when this kompone have status_masuk is new
                    $query2 = "select status_masuk from " . sfConfig::get('app_default_schema') . ".komponen where komponen_id='" . $rd->getKomponenId() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $t = $stmt->executeQuery();
                    while ($t->next()) {
                        if ($t->getString('status_masuk') == 'baru') {
                            echo image_tag('/images/newanima.gif');
                        }
                    }
//EO djiebrats give image new
//sisa lelang untuk fisik
                    ?>

                    <div id="<?php echo 'tempat_ajax_' . $no ?>">
                        <?php
                        if ($unit_id == '9999') {
                            if ($rd->getLockSubtitle() == 'unlock') {
                                echo "<font color=red>{harga dasar ulocked}</font>";
                                echo "<br>";
                                if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                                    echo link_to_remote('[lock kembali harga dasar]', array('alt' => 'Mengubah Harga', 'title' => 'Mengubah Harga', 'update' => 'tempat_ajax_' . $no, 'url' => 'kegiatan/lockhargadasar?act=lock&id=' . $id . '&detail_no=' . $no . '&unit_id=' . $unit_id . '&kegiatan_code=' . $kegiatan, 'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')", '&ajax_id=' . $ajax_id));
                            }
                            else {
                                echo "";

                                echo "<br>";
                                if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                                    echo link_to_remote('[unlock harga dasar]', array('alt' => 'Mengubah Harga', 'title' => 'Mengubah Harga', 'update' => 'tempat_ajax_' . $no, 'url' => 'kegiatan/lockhargadasar?act=unlock&id=' . $id . '&detail_no=' . $no . '&unit_id=' . $unit_id . '&kegiatan_code=' . $kegiatan, 'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')", '&ajax_id=' . $ajax_id));
                            }
                        }
                        ?></div>
                    <?php
                    if ($rd->getLockSubtitle() == 'LOCK')
                        echo image_tag('/images/gembok.gif');

                    if ((substr($rd->getKomponenId(), 0, 3) == 'SUB') or (substr($rd->getKomponenId(), 0, 3) == 'RSU')) {
                        echo link_to(image_tag('/images/pisah.png'), 'kegiatan/undoGabung?komponen_id=' . $rd->getKomponenId() . '&unit_id=' . $rd->getUnitId() . '&kegiatan_code=' . $rd->getKegiatanCode() . '&kode_sub=' . $rd->getKodeSub() . '&edit=' . md5('bataltarik'));
                    }


                    if (substr($rd->getStatusLelang(), 0, 14) == 'lock edelivery') {
                        echo '<br>';
                        $sts = explode(" ", $rd->getStatusLelang());
                        $persen = $sts[2];
                        for ($i = 1; $i <= $persen; $i++) {
                            echo image_tag('/images/kotakmerah.png');
                        }
                        for ($i = 1; $i <= 10 - $persen; $i++) {
                            echo image_tag('/images/kotakhijau.png');
                        }

                        echo '<BR><font color=red>[edelivery ' . $persen * 10 . '%]</font>';
                        echo '<br><font color=green>[semula @' . number_format($rd->getHargaSemula(), 0, ',', '.') . ' x ' . $rd->getKoefisienSemula() . ' = ' . number_format($rd->getTotalSemula(), 0, ',', '.') . ']</font>	';
                        echo '<br>';
                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                            echo link_to(image_tag('/images/bataltarik_edelivery.png'), 'kegiatan/bataltarikdelivery?detail_no=' . $rd->getDetailNo() . '&unit_id=' . $rd->getUnitId() . '&kegiatan_code=' . $rd->getKegiatanKode() . '&edit=' . md5('bataltarik'));
                    }
                    if ($rd->getStatusLelang() == 'edelivery') {
                        echo '<br>';
                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                            echo link_to(image_tag('/images/tarik_edelivery.png'), 'kegiatan/tarikdelivery?detail_no=' . $rd->getDetailNo() . '&unit_id=' . $rd->getUnitId() . '&kegiatan_code=' . $rd->getKegiatanCode() . '&edit=' . md5('tarik'));
                    }

//EO => macam2 untuk sisa lelang

                    if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'parlemen2') {

//djiebrats: nambah data dari edelivery
                        /*
                          $query2="select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                          de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                          sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting,
                          sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
                          sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak

                          from ". sfConfig::get('app_default_edelivery') .".detail_komponen dk, ". sfConfig::get('app_default_eproject') .".detail_kegiatan de, ". sfConfig::get('app_default_schema') .".rincian_detail rd

                          where rd.unit_id='$unit_id' and rd.kegiatan_code='$kegiatan' and rd.detail_no='$no'
                          and rd.subtitle ilike '$sub' and rd.volume>0 and rd.status_hapus=false
                          and de.id=dk.detail_kegiatan_id and substring(de.kode_detail_kegiatan,1,4)=rd.unit_id
                          and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code
                          and (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no


                          group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                          de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                          having sum(dk.harga_realisasi*dk.volume_realisasi)>0 or sum(dk.harga_kontrak*dk.volume_kontrak)>0
                          order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
                          //print_r($query2);exit;

                          $con = Propel::getConnection();
                          $stmt = $con->prepareStatement($query2);
                          $t = $stmt->executeQuery();
                          //print_r($t);
                          while($t->next())
                          {

                          if($t->getString('nilai_realisasi')>0){
                          echo '<font color=red>-Nilai swakelola pada <br>e-delivery = Rp
                          '.number_format($t->getString('nilai_realisasi'), 0, ',', '.').'<br></font>'; }
                          //else if($t->getString('nilai_kontrak')){
                          //echo '<font color=red> Nilai Kontrak pada <br>e-delivery = Rp '.number_format($t->getString('nilai_kontrak'), 0, ',', '.').'</font>';
                          //}
                          } */
                    }
//eof djiebrats			
                    ?>
                </td>
                <td align="center" style="background: pink"><?php echo $rd->getTipe(); ?></td>
                <td align="center" style="background: pink"><?php echo $rd->getSatuan(); ?></td>
                <td align="center" style="background: pink"><?php echo $rd->getKeteranganKoefisien(); ?></td>

                <td align="right" style="background: pink">
                    <?php
                    // khusus mas rizki
                    //djieb  begin
                    $komponen_id = $rd->getKomponenId();
                    if (($sf_user->getNamaUser() == 'rizki') and (substr($komponen_id, 0, 5) == 'BOPDA')) {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                        ?>
                        <div id="<?php echo 'rizki_' . $rd->getDetailNo(); ?>"></div>
                        <?php
                        echo link_to_remote(image_tag('/sf/sf_admin/images/edit.png'), array('alt' => 'Mengubah Nilai Khusus mas Rizki', 'title' => 'Mengubah Nilai Khusus mas Rizki', 'update' => 'rizki_' . $rd->getDetailNo(), 'url' => 'kegiatan/special?act=masRizki&id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&harga=' . $rd->getKomponenHargaAwal()));
                    } else if ($rd->getSatuan() == '%') {
                        echo number_format($rd->getKomponenHargaAwal(), 4, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?></td>

                <td align="right" style="background: pink"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td align="right" style="background: pink"><?php echo $rd->getPajak() . '%'; ?></td>
                <td align="right" style="background: pink"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $pajak = $rd->getPajak();
                    $total = $rd->getNilaiAnggaran();
                    echo number_format($total, 0, ',', '.');
                    ?></td>
                <td align="center" style="background: pink">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>
                <?php
                //irul 24maret 2014 - note peneliti + note skpd    
            } else {
                //irul 24maret 2014 - note peneliti + note skpd
                ?>
                <td>
                    <?php
                    if ($benar_musrenbang == 1) {
                        ?>
                        <div style="background: #A8CD1B; padding: 5px; text-align: center;border: 1px solid #A8CD1B">
                            <font style="font-size: 14px; font-weight: bold">MUSRENBANG <?php echo $tahun_musrenbang ?> :: id <?php echo $id_musrenbang ?></font>    
                        </div><br/>
                        <?php
                    }
                    if ($benar_multiyears == 1) {
                        ?>
                        <div style="background: #67BCDB; padding: 5px; text-align: center;border: 1px solid #67BCDB">
                            <font style="font-size: 14px; font-weight: bold">MULTIYEARS TAHUN KE <?php echo $th_ke_multiyears ?></font>    
                        </div><br/>
                        <?php
                    }
                    if ($rd->getIsBlud() == 1) {
                        ?>
                        <div style="background: #F2C249; padding: 5px; text-align: center;border: 1px solid #F2C249">
                            <font style="font-size: 14px; font-weight: bold">BLUD</font>    
                        </div><br/>
                        <?php
                    }
                    echo $rd->getKomponenName();
                    if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                        echo ' ' . $rd->getDetailName() . ' ' . $rd->getLokasiKecamatan() . ' ' . $rd->getLokasiKelurahan();
                    }

//djiebrats : give image "new" when this kompone have status_masuk is new
                    $query2 = "select status_masuk from " . sfConfig::get('app_default_schema') . ".komponen where komponen_id='" . $rd->getKomponenId() . "'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    $t = $stmt->executeQuery();
                    while ($t->next()) {
                        if ($t->getString('status_masuk') == 'baru') {
                            echo image_tag('/images/newanima.gif');
                        }
                    }
//EO djiebrats give image new
//sisa lelang untuk fisik
                    ?>

                    <div id="<?php echo 'tempat_ajax_' . $no ?>">
                        <?php
                        //if((($unit_id=='3000') and (($subtitle_name=='Pemasangan Penerangan Jalan Umum') || ($kegiatan_code=='0027') || ($kegiatan_code=='0025') || ($kegiatan_code=='0002')))|| (($unit_id=='B002') and ($kegiatan_code=='0001')) || (($unit_id=='1700') and (($kegiatan_code=='0005')||($kegiatan_code=='0013')||($kegiatan_code=='0026')||($kegiatan_code=='0009'))) || (($unit_id=='B002') and (($subtitle_name=='Pemanfaatan Pekarangan untuk Pengembangan Pangan (Urban Farming)') || ($subtitle_name=='Pelatihan Hasil Pertanian'))) || (($unit_id=='2600') and (($kegiatan_code='0005'))) ||(($unit_id=='2300') and (($kegiatan_code=='0001') || ($kegiatan_code=='0012'))))
                        if ($unit_id == '9999') {
                            if ($rd->getLockSubtitle() == 'unlock') {
                                echo "<font color=red>{harga dasar ulocked}</font>";
                                echo "<br>";
                                if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                                    echo link_to_remote('[lock kembali harga dasar]', array('alt' => 'Mengubah Harga', 'title' => 'Mengubah Harga', 'update' => 'tempat_ajax_' . $no, 'url' => 'kegiatan/lockhargadasar?act=lock&id=' . $id . '&detail_no=' . $no . '&unit_id=' . $unit_id . '&kegiatan_code=' . $kegiatan, 'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')", '&ajax_id=' . $ajax_id));
                            }
                            else {
                                echo "";

                                echo "<br>";
                                if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                                    echo link_to_remote('[unlock harga dasar]', array('alt' => 'Mengubah Harga', 'title' => 'Mengubah Harga', 'update' => 'tempat_ajax_' . $no, 'url' => 'kegiatan/lockhargadasar?act=unlock&id=' . $id . '&detail_no=' . $no . '&unit_id=' . $unit_id . '&kegiatan_code=' . $kegiatan, 'loading' => "Element.show('indikator')", 'complete' => "Element.hide('indikator')", '&ajax_id=' . $ajax_id));
                            }
                        }
                        ?></div>
                    <?php
//
//djiebrats : tambah tarik edelivery dkk untuk sisa lelang (non fisik)
                    if ($rd->getLockSubtitle() == 'LOCK')
                        echo image_tag('/images/gembok.gif');

                    if ((substr($rd->getKomponenId(), 0, 3) == 'SUB') or (substr($rd->getKomponenId(), 0, 3) == 'RSU')) {
                        echo link_to(image_tag('/images/pisah.png'), 'kegiatan/undoGabung?komponen_id=' . $rd->getKomponenId() . '&unit_id=' . $rd->getUnitId() . '&kegiatan_code=' . $rd->getKegiatanCode() . '&kode_sub=' . $rd->getKodeSub() . '&edit=' . md5('bataltarik'));
                    }


                    if (substr($rd->getStatusLelang(), 0, 14) == 'lock edelivery') {
                        echo '<br>';
                        //echo image_tag('/images/'.$rd->getStatusLelang(). ".png");
                        $sts = explode(" ", $rd->getStatusLelang());
                        $persen = $sts[2];
                        for ($i = 1; $i <= $persen; $i++) {
                            echo image_tag('/images/kotakmerah.png');
                        }
                        for ($i = 1; $i <= 10 - $persen; $i++) {
                            echo image_tag('/images/kotakhijau.png');
                        }

                        echo '<BR><font color=red>[edelivery ' . $persen * 10 . '%]</font>';
                        echo '<br><font color=green>[semula @' . number_format($rd->getHargaSemula(), 0, ',', '.') . ' x ' . $rd->getKoefisienSemula() . ' = ' . number_format($rd->getTotalSemula(), 0, ',', '.') . ']</font>	';
                        echo '<br>';
                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                            echo link_to(image_tag('/images/bataltarik_edelivery.png'), 'kegiatan/bataltarikdelivery?detail_no=' . $rd->getDetailNo() . '&unit_id=' . $rd->getUnitId() . '&kegiatan_code=' . $rd->getKegiatanKode() . '&edit=' . md5('bataltarik'));
                    }
                    if ($rd->getStatusLelang() == 'edelivery') {
                        echo '<br>';
                        if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten')
                            echo link_to(image_tag('/images/tarik_edelivery.png'), 'kegiatan/tarikdelivery?detail_no=' . $rd->getDetailNo() . '&unit_id=' . $rd->getUnitId() . '&kegiatan_code=' . $rd->getKegiatanCode() . '&edit=' . md5('tarik'));
                    }

//EO => macam2 untuk sisa lelang

                    if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'parlemen2') {

//djiebrats: nambah data dari edelivery
                        /*
                          $query2="select rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                          de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id,
                          sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai_budgeting,
                          sum(dk.harga_realisasi*dk.volume_realisasi) as nilai_realisasi,
                          sum(dk.harga_kontrak*dk.volume_kontrak) as nilai_kontrak

                          from ". sfConfig::get('app_default_edelivery') .".detail_komponen dk, ". sfConfig::get('app_default_eproject') .".detail_kegiatan de, ". sfConfig::get('app_default_schema') .".rincian_detail rd

                          where rd.unit_id='$unit_id' and rd.kegiatan_code='$kegiatan' and rd.detail_no='$no'
                          and rd.subtitle ilike '$sub' and rd.volume>0 and rd.status_hapus=false
                          and de.id=dk.detail_kegiatan_id and substring(de.kode_detail_kegiatan,1,4)=rd.unit_id
                          and substring(de.kode_detail_kegiatan,6,4)=rd.kegiatan_code
                          and (substring(de.kode_detail_kegiatan,11))::integer=rd.detail_no


                          group by rd.unit_id,rd.kegiatan_code,rd.detail_no,rd.subtitle,rd.komponen_name,rd.kode_sub,
                          de.kode_detail_kegiatan,dk.detail_swakelola_id,dk.kontrak_id
                          having sum(dk.harga_realisasi*dk.volume_realisasi)>0 or sum(dk.harga_kontrak*dk.volume_kontrak)>0
                          order by rd.unit_id,rd.kegiatan_code,rd.detail_no";
                          //print_r($query2);exit;

                          $con = Propel::getConnection();
                          $stmt = $con->prepareStatement($query2);
                          $t = $stmt->executeQuery();
                          //print_r($t);
                          while($t->next())
                          {

                          if($t->getString('nilai_realisasi')>0){
                          echo '<font color=red>-Nilai swakelola pada <br>e-delivery = Rp
                          '.number_format($t->getString('nilai_realisasi'), 0, ',', '.').'<br></font>'; }
                          //else if($t->getString('nilai_kontrak')){
                          //echo '<font color=red> Nilai Kontrak pada <br>e-delivery = Rp '.number_format($t->getString('nilai_kontrak'), 0, ',', '.').'</font>';
                          //}
                          } */
                    }
//eof djiebrats			
                    ?>
                </td>
                <td align="center"><?php echo $rd->getTipe(); ?></td>
                <td align="center"><?php echo $rd->getSatuan(); ?></td>
                <td align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>

                <td align="right">
                    <?php
                    // khusus mas rizki
                    //djieb  begin
                    $komponen_id = $rd->getKomponenId();
                    if (($sf_user->getNamaUser() == 'rizki') and (substr($komponen_id, 0, 5) == 'BOPDA')) {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                        ?>
                        <div id="<?php echo 'rizki_' . $rd->getDetailNo(); ?>"></div>
                        <?php
                        echo link_to_remote(image_tag('/sf/sf_admin/images/edit.png'), array('alt' => 'Mengubah Nilai Khusus mas Rizki', 'title' => 'Mengubah Nilai Khusus mas Rizki', 'update' => 'rizki_' . $rd->getDetailNo(), 'url' => 'kegiatan/special?act=masRizki&id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&harga=' . $rd->getKomponenHargaAwal()));
                    } else if ($rd->getSatuan() == '%') {
                        echo number_format($rd->getKomponenHargaAwal(), 4, ',', '.');
                    } else {
                        echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                    }
                    ?></td>

                <td align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $hasil = $volume * $harga;
                    echo number_format($hasil, 0, ',', '.');
                    ?></td>
                <td align="right"><?php echo $rd->getPajak() . '%'; ?></td>
                <td align="right"><?php
                    $volume = $rd->getVolume();
                    $harga = $rd->getKomponenHargaAwal();
                    $pajak = $rd->getPajak();
                    $total = $rd->getNilaiAnggaran();
                    echo number_format($total, 0, ',', '.');
                    ?></td>
                <td align="center">
                    <?php
                    $rekening = $rd->getRekeningCode();
                    $rekening_code = substr($rekening, 0, 5);
                    $c = new Criteria();
                    $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
                    $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
                    if ($rs_rekening) {
                        echo $rs_rekening->getBelanjaName();
                    }
                    ?></td>
                <?php
                //irul 24maret 2014 - note peneliti + note skpd
            }
            ?>
        </tr>
        <?php
    }
endforeach;
?>
