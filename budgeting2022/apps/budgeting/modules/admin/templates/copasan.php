<?php

//fungsi user_log
$namaprofil = $this->getUser()->getNamaUser();
$username = $this->getUser()->getNamaLogin();
budgetLogger::log($username . ' menghapus Usulan SSH id :' . $id_usulan . ' dengan nama : ' . $rs_usulan->getNama() . ' ' . $rs_usulan->getSpec() . ' pada dinas :' . $rs_usulan->getUnitId());
//fungsi user_log
//propel count
$c = new Criteria();
$c->add(myTablePeer::ID_PAGE, 5);
$count = myTablePeer::doCount($c);
//propel count
//propel select limit 1
$c = new Criteria();
$c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
$rs_usulan = UsulanSSHPeer::doSelectOne($c);
//propel select limit 1
//propel select all
$c = new Criteria();
$c->add(UsulanSSHPeer::ID_USULAN, $id_usulan, Criteria::EQUAL);
$rs_usulan = UsulanSSHPeer::doSelect($c);
//propel select all
//propel insert
$komponen_rekening = new KomponenRekening();
$komponen_rekening->setKomponenId($kode_barang . '.' . $id_ssh);
$komponen_rekening->setRekeningCode($rekening);
$komponen_rekening->save();
//propel insert
//ambil fungsi dari peer model
UsulanSPJMPeer::getStringPenyelia($usulanverifikasi->getIdSpjm());
//ambil fungsi dari peer model
//fungsi echo tanggal
date("j F Y H:i:s", strtotime($updated_at));
date("d-m-Y H:i:s", strtotime($updated_at));
//fungsi echo tanggal
// Text field (input)
echo input_tag('name', 'default value');
//<input type="text" name="name" id="name" value="default value" />
// All form helpers accept an additional options parameter
// It allows you to add custom attributes to the generated tag
echo input_tag('name', 'default value', 'maxlength=20');
//<input type="text" name="name" id="name" value="default value" maxlength="20" />
// Long text field (text area)
echo textarea_tag('name', 'default content', 'size=10x20');
//<textarea name="name" id="name" cols="10" rows="20">
//      default content
//    </textarea>
// Check box
echo checkbox_tag('single', 1, true);
echo checkbox_tag('driverslicense', 'B', false);
//<input type="checkbox" name="single" id="single" value="1" checked="checked" />
//    <input type="checkbox" name="driverslicense" id="driverslicense" value="B" />
// Radio button
echo radiobutton_tag('status[]', 'value1', true);
echo radiobutton_tag('status[]', 'value2', false);
//<input type="radio" name="status[]" id="status_value1" value="value1" checked="checked" />
//    <input type="radio" name="status[]" id="status_value2" value="value2" />
// Dropdown list (select)
echo select_tag('payment', '<option selected="selected">Visa</option>
   <option>Eurocard</option>
   <option>Mastercard</option>');

//<select name="payment" id="payment">
//      <option selected="selected">Visa</option>
//      <option>Eurocard</option>
//      <option>Mastercard</option>
//    </select>
// List of options for a select tag
echo options_for_select(array('Visa', 'Eurocard', 'Mastercard'), 0);
//<option value="0" selected="selected">Visa</option>
//    <option value="1">Eurocard</option>
//    <option value="2">Mastercard</option>
// Dropdown helper combined with a list of options
echo select_tag('payment', options_for_select(array(
    'Visa',
    'Eurocard',
    'Mastercard'
                ), 0));
//<select name="payment" id="payment">
//      <option value="0" selected="selected">Visa</option>
//      <option value="1">Eurocard</option>
//      <option value="2">Mastercard</option>
//    </select>
// To specify option names, use an associative array
echo select_tag('name', options_for_select(array(
    'Steve' => 'Steve',
    'Bob' => 'Bob',
    'Albert' => 'Albert',
    'Ian' => 'Ian',
    'Buck' => 'Buck'
                ), 'Ian'));
//<select name="name" id="name">
//      <option value="Steve">Steve</option>
//      <option value="Bob">Bob</option>
//      <option value="Albert">Albert</option>
//      <option value="Ian" selected="selected">Ian</option>
//      <option value="Buck">Buck</option>
//    </select>
// Drop-down list with multiple selection (selected values can be an array)
echo select_tag('payment', options_for_select(
                array('Visa' => 'Visa', 'Eurocard' => 'Eurocard', 'Mastercard' => 'Mastercard'), array('Visa', 'Mastercard')
        ), 'multiple=multiple');
//<select name="payment[]" id="payment" multiple="multiple">
//      <option value="Visa" selected="selected">Visa</option>
//      <option value="Eurocard">Eurocard</option>
//      <option value="Mastercard" selected="selected">Mastercard</option>
//    </select>
// Upload file field
echo input_file_tag('name');
//<input type="file" name="name" id="name" value="" />
// Password field
echo input_password_tag('name', 'value');
//<input type="password" name="name" id="name" value="value" />
// Hidden field
echo input_hidden_tag('name', 'value');
//<input type="hidden" name="name" id="name" value="value" />
// Submit button (as text)
echo submit_tag('Save');
//<input type="submit" name="submit" value="Save" />
// Submit button (as image)
echo submit_image_tag('submit_img');
//<input type="image" name="submit" src="/legacy/images/submit_img.png" />
//fungsi user_log
$namaprofil = $this->getUser()->getNamaUser();
$username = $this->getUser()->getNamaLogin();
budgetLogger::log($username . ' merequest token dengan kode : ' . $token_dapet . ' untuk penyelia : ' . $data_penyelia->getUsername() . ' Dinas : ' . $data_skpd->getUnitName() . ' untuk SSH sebanyak ' . $this->getRequestParameter('jumlah'));
//fungsi user_log


$c = new Criteria();
$c->addDescendingOrderByColumn(UsulanVerifikasiPeer::CREATED_AT);
$c->addJoin(UsulanVerifikasiPeer::UNIT_ID, UnitKerjaPeer::UNIT_ID, Criteria::INNER_JOIN);
$c->addJoin(UsulanVerifikasiPeer::ID_USULAN, UsulanSSHPeer::ID_USULAN, Criteria::INNER_JOIN);
$c->addJoin(UsulanVerifikasiPeer::ID_SPJM, UsulanSPJMPeer::ID_SPJM, Criteria::INNER_JOIN);
