<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Status Aplikasi berdasar app.yml</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Pengaturan</a></li>
          <li class="breadcrumb-item active">App</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<?php include_partial('admin/menufitur') ?>
<!-- Main content -->
<section class="content">
    <?php include_partial('admin/list_messages') ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            
            <div class="card-body table-responsive p-0">
                <table class="table table-striped">
                    <thead class="head_peach">
                        <tr>
                            <th>Nama</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Schema Budgeting </td>
                            <td><?php echo sfConfig::get('app_default_schema') ?></td>
                        </tr>
                        <tr>
                            <td>Schema Delivery </td>
                            <td><?php echo sfConfig::get('app_default_edelivery') ?></td>
                        </tr>
                        <tr>
                            <td>Schema Eproject </td>
                            <td><?php echo sfConfig::get('app_default_eproject') ?></td>
                        </tr>
                        <tr>
                            <td>Schema Eperformance </td>
                            <td><?php echo sfConfig::get('app_default_eperformance') ?></td>
                        </tr>
                        <tr>
                            <td>Tahun Anggaran</td>
                            <td><?php echo sfConfig::get('app_tahun_default') ?> *berpengaruh pada tahun dan query*</td>
                        </tr>
                        <tr>
                            <td>Tahap</td>
                            <td><?php echo sfConfig::get('app_tahap_edit') ?></td>
                        </tr>
                        <tr>
                            <td>Tahap Detail</td>
                            <td><?php echo sfConfig::get('app_tahap_detail') ?></td>
                        </tr>
                        <tr>
                            <td>Fasilitas Catatan Pergeseran  </td>
                            <td><?php
                                if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka') {
                                    echo '<font color=green>' . sfConfig::get('app_fasilitas_bukaCatatanPergeseran') . '</font>';
                                } else {
                                    echo '<font color=red>' . sfConfig::get('app_fasilitas_bukaCatatanPergeseran') . '</font>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>Fasilitas Pengecekan Ke e-Delivery  </td>
                            <td>
                                <?php
                                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                    echo '<font color=green>' . sfConfig::get('app_fasilitas_cekeDelivery') . '</font>';
                                } else {
                                    echo '<font color=red>' . sfConfig::get('app_fasilitas_cekeDelivery') . '</font>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>Fasilitas Pengecekan Ke e-Project  </td>
                            <td><?php
                                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                                    echo '<font color=green>' . sfConfig::get('app_fasilitas_cekeProject') . '</font>';
                                } else {
                                    echo '<font color=red>' . sfConfig::get('app_fasilitas_cekeProject') . '</font>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>Fasilitas Keterangan Komponen</td>
                            <td>
                                <?php
                                if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                                    echo '<font color=green>' . sfConfig::get('app_fasilitas_keteranganKomponen') . '</font>';
                                } else {
                                    echo '<font color=red>' . sfConfig::get('app_fasilitas_keteranganKomponen') . '</font>';
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td>Fasilitas batas pagu (Pilih salah satu pagu kegiatan atau pagu dinas)</td>
                            <td>
                                <?php
                                if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                                    echo '<font color=green>' . sfConfig::get('app_fasilitas_batasPaguDinas') . '</font>';
                                } else {
                                    echo '<font color=red>' . sfConfig::get('app_fasilitas_batasPaguDinas') . '</font>';
                                }
                                ?></td>
                        </tr>

                        <tr>
                            <td>Fasilitas batas pagu pada dinas berdasarkan pagu kegiatan</td>
                    <td>
                        <?php
                        if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                            echo '<font color=green>' . sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') . '</font>';
                        } else {
                            echo '<font color=red>' . sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') . '</font>';
                        }
                        ?></td>
                    </tr>
                    <tr>
                        <td>Fasilitas batas pagu pada dinas berdasarkan pagu dinas</td>
                    <td>
                        <?php
                        if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                            echo '<font color=green>' . sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') . '</font>';
                        } else {
                            echo '<font color=red>' . sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') . '</font>';
                        }
                        ?></td>
                    </tr>
                    <tr>
                        <td>Fasilitas batas pagu pada peneliti</td>
                        <td>
                            <?php
                            if (sfConfig::get('app_fasilitas_batasPaguPeneliti') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_batasPaguPeneliti') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_batasPaguPeneliti') . '</font>';
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Fasilitas buka deliveri manual   </td>
                        <td>
                            <?php
                            if (sfConfig::get('app_fasilitas_bukaDeliveriManual') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_bukaDeliveriManual') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_bukaDeliveriManual') . '</font>';
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Fasilitas warning rekening</td>
                        <td><?php
                            if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_warningRekening') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_warningRekening') . '</font>';
                            }
                            ?> *di bagian atas di dinas dan peneliti warna merah*</td>
                    </tr>
                    <tr>
                        <td>Fasilitas Ganti Rekening untuk peneliti(mengelompokkan berdasarkan header)  </td>
                        <td><?php
                            if (sfConfig::get('app_fasilitas_gantiRekening4Peneliti') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_gantiRekening4Peneliti') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_gantiRekening4Peneliti') . '</font>';
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Fasilitas Ganti Subtitle</td>
                        <td><?php
                            if (sfConfig::get('app_fasilitas_gantiSubtitle4Peneliti') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_gantiSubtitle4Peneliti') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_gantiSubtitle4Peneliti') . '</font>';
                            }
                            ?> </td>
                    </tr>
                    <tr>
                        <td>Fasilitas pagu bayangan</td>
                        <td><?php
                            if (sfConfig::get('app_fasilitas_paguBayangan') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_paguBayangan') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_paguBayangan') . '</font>';
                            }
                            ?> </td>
                    </tr>
                    <tr>
                        <td>Fasilitas Ganti Subtitle</td>
                        <td><?php
                            if (sfConfig::get('app_fasilitas_fasilitasPindahKegiatanPU') == 'buka') {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_gantiSubtitle4Peneliti') . '</font>';
                            } else {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_gantiSubtitle4Peneliti') . '</font>';
                            }
                            ?> </td>
                    </tr>
                    <tr>
                        <td>Fasilitas Cek Rekening Revisi (untuk sisipan yang tidak boleh geser per rekening)</td>
                        <td><?php
                            if (sfConfig::get('app_fasilitas_cekRekeningRevisi') == 'buka') {
                                echo '<font color=red>' . sfConfig::get('app_fasilitas_cekRekeningRevisi') . '</font>';
                            } else {
                                echo '<font color=green>' . sfConfig::get('app_fasilitas_cekRekeningRevisi') . '</font>';
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td>Lokasi Path Pdf   </td>
                        <td><?php echo sfConfig::get('app_path_pdf') ?></td>
                    </tr>
                    </tbody>
                </table>
                <br/>
                <table class="table table-hover">
                    <thead class="head_peach">
                        <tr>
                            <th colspan="2">Aplikasi eBudgeting-eProject-eDelivery</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td style="width: 80%; text-align: justify">
                                <font style="font-weight: bold; font-size: 16px">Kunci Kegiatan eBudgeting</font><br/>
                                <p>Fitur ini untuk mengunci kegiatan.</p>
                            </td>
                            <td style="text-align: center"><?php echo link_to('<b>Kunci Semua Kegiatan eBudgeting</b>', 'admin/lockAllKegiatan') ?></td>
                        </tr> 
                        <tr >
                            <td style="width: 80%; text-align: justify">
                                <font style="font-weight: bold; font-size: 16px">Generate Kode Kegiatan Keuangan</font><br/>
                                <p>Fitur ini untuk menggenerate ulang kode kegiatan keuangan yang terhubung dengan eDelivery dengan keuangan. Mengubah pada kolom <b>kode_keg_keuangan</b> pada tabel <b>master_kegiatan</b>.</p>
                            </td>
                            <td style="text-align: center"><?php echo link_to('<b>Generate Kode Kegiatan Keuangan</b>', 'admin/generateKodeKeuangan') ?></td>
                        </tr>
                        <tr >
                            <td style="width: 80%; text-align: justify">
                                <font style="font-weight: bold; font-size: 16px">Generate Kode Kegiatan Keuangan BP</font><br/>
                                <p>Fitur ini untuk menggenerate ulang kode kegiatan keuangan yang terhubung dengan eDelivery dengan keuangan. Mengubah pada kolom <b>kode_keg_keuangan</b> pada tabel <b>master_kegiatan_bp</b>. 
                                    <b>GUNAKAN HANYA SAAT eProject MEMBUTUHKAN DARURAT !!</b>.</p>
                            </td>
                            <td style="text-align: center"><?php echo link_to('<b>Generate Kode Kegiatan Keuangan BP</b>', 'admin/generateKodeKeuanganBp') ?></td>
                        </tr>
                        <tr >
                            <td style="width: 80%; text-align: justify">
                                <font style="font-weight: bold; font-size: 16px">Generate Kode DPA</font><br/>
                                <p>Fitur ini untuk menggenerate ulang kode kegiatan keuangan yang terhubung dengan eDelivery dengan keuangan. Mengubah pada kolom <b>kode_keg_keuangan</b> pada tabel <b>dinas_master_kegiatan, master_kegiatan, master_kegiatan_bp</b></p>
                            </td>
                            <td style="text-align: center"><?php echo link_to('<b>Generate Kode DPA</b>', 'admin/generateKodeDpa') ?></td>
                        </tr>
                    </tbody>
                </table>
                <div id="indicator" style="display:none;" align="center"><dt>&nbsp;</dt><dd><b>Mohon Tunggu </b><?php echo image_tag('loading.gif', array('align' => 'absmiddle')) ?></dd></div>
            </div>
        </div>
       </div>
      </div>
    </div>
</section>
