<table width="760"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="1" background="<?php echo image_path('/images/grs_vert.gif') ?>"></td>
      <td height="20" align="center" background="<?php echo image_path('/images/menu_bg.gif') ?>" class="font12"><font color="#FFFFFF"><strong class="font12"><span class="Font10BoldWhite" style="color: #d8732f">Rekapitulasi</span></strong></font>&nbsp;&nbsp;
	  </strong></font></span></strong></font></td>
	  <td align="right" background="<?php echo image_path('/images/menu_bg.gif') ?>" class="Font10BoldWhite"><?php echo link_to(image_tag('/images/cross.gif',array('width'=>'18','height'=>'16','border'=>'0','align'=>'absmiddle')),'admin/report') ?></td>
      <td width="1" background="<?php echo image_path('/images/grs_vert.gif') ?>"></td>
    </tr>
    <tr>
      <td width="1" background="<?php echo image_path('/images/grs_vert.gif') ?>"></td>
      <td colspan="2" align="center">
        <table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#333333">
			<tr>
				<td align="center" bgcolor="#9999FF" rowspan="3"><strong>NAMA SKPD </strong></td>
				<td colspan="15" bgcolor="#9999FF" align="center"><strong>STATUS </strong></td>
			</tr>
			<tr>
				<td colspan="5" bgcolor="#9999FF" align="center"><strong>PNS </strong></td>
				<td colspan="5" bgcolor="#9999FF" align="center"><strong>HONDA </strong></td>
				<td colspan="5" bgcolor="#9999FF" align="center"><strong>NON PNS </strong></td>
			</tr>
			<tr>
				
				<td align="center" bgcolor="#9999FF"><strong>Jumlah Personil</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>REKENING</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>ANGGARAN </strong></td>
				<td align="center" bgcolor="#9999FF"><strong>TOTAL</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>RERATA TAKE HOME PAY per BULAN</strong></td>
				
				<td align="center" bgcolor="#9999FF"><strong>Jumlah Personil</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>REKENING</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>ANGGARAN </strong></td>
				<td align="center" bgcolor="#9999FF"><strong>TOTAL</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>RERATA TAKE HOME PAY per BULAN</strong></td>
				
				<td align="center" bgcolor="#9999FF"><strong>Jumlah Personil</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>REKENING</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>ANGGARAN </strong></td>
				<td align="center" bgcolor="#9999FF"><strong>TOTAL</strong></td>
				<td align="center" bgcolor="#9999FF"><strong>RERATA TAKE HOME PAY per BULAN</strong></td>
			</tr>
			<?php
			$i=0;
			while($rs->next())
			{
			
			?>
				<tr>
					<td bgcolor="#FFFFFF" class="Font8v"><?php echo $rs->getString('unit_name') ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="center"><?php echo $rs->getString('jumlah_pns') ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="center"><?php echo $rekening_pns_arr[$i] ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo $anggaran_pns_arr[$i] ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo number_format($total_pns_arr[$i],0,",",".") ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo number_format(($total_pns_arr[$i]/$rs->getString('jumlah_pns'))/12,0,",",".") ?></td>
					
					<td bgcolor="#FFFFFF" class="Font8v" align="center"><?php echo $rs->getString('jumlah_honda') ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="center"><?php echo $rekening_honda_arr[$i] ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo $anggaran_honda_arr[$i] ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo number_format($total_honda_arr[$i],0,",",".") ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo number_format(($total_honda_arr[$i]/$rs->getString('jumlah_honda'))/12,0,",",".") ?></td>
					
					<td bgcolor="#FFFFFF" class="Font8v" align="center"><?php echo $rs->getString('jumlah_nonpns') ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="center"><?php echo $rekening_nonpns_arr[$i] ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo $anggaran_nonpns_arr[$i] ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo number_format($total_nonpns_arr[$i],0,",",".") ?></td>
					<td bgcolor="#FFFFFF" class="Font8v" align="right"><?php echo number_format(($total_nonpns_arr[$i]/$rs->getString('jumlah_nonpns'))/12,0,",",".") ?></td>
				</tr>
			<?php
				$i+=1;
			}
			?>
        </table>
        <br>
      </td>
      <td width="1" background="<?php echo image_path('/images/grs_vert.gif') ?>"></td>
    </tr>
    <tr>
       <td width="1" background="<?php echo image_path('/images/grs_vert.gif') ?>"></td>
      <td height="1" colspan="2" background="<?php echo image_path('/images/grs_hors.gif') ?>"></td>
      <td width="1" background="<?php echo image_path('/images/grs_vert.gif') ?>"></td>
    </tr>
  </table>
