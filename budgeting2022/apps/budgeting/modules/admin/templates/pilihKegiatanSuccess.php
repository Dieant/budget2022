<?php use_helper('Object', 'Javascript', 'I18N', 'Date'); ?>

<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<?php

$options = array();

foreach ($master_kegiatan as $keg) {
    $options[$keg->getKodeKegiatan()] = $keg->getKodeKegiatan() . ' - '. $keg->getNamaKegiatan();
}

echo options_for_select($options, '', array('include_blank' => true));
?>