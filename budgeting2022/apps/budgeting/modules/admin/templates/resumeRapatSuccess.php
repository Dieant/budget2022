<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Daftar Isian Rincian Resume Rapat</h1>
</section>

<!-- Main content -->
<section class="content">
    <?php include_partial('admin/list_messages'); ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Form Keterangan Resume Rapat</h3>
        </div>
        <div class="box-body">
            <?php echo form_tag('admin/prosesResumeRapat', array('method' => 'post', 'class' => 'form-horizontal')) ?>
            <?php echo input_hidden_tag('unit_id', $unit_id) ?>
            <?php echo input_hidden_tag('kode_kegiatan', $kode_kegiatan) ?>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal</label>
                <div class="col-sm-10">
                    <?php echo input_date_tag('tanggal', now, array('rich' => true, 'readonly' => 'true', 'required' => 'true')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Pukul</label>
                <div class="col-sm-10">
                    <?php //echo input_tag('jam', date('H'), array('type' => 'number', 'min' => '0', 'max' => '23', 'style' => 'width:6%', 'required' => 'true')) . ' . ' . input_tag('menit', date('i'), array('type' => 'number', 'min' => '0', 'max' => '59', 'style' => 'width:6%', 'required' => 'true')); ?>
                    <?php echo input_tag('jam', 8, array('type' => 'number', 'min' => '0', 'max' => '23', 'style' => 'width:6%', 'required' => 'true')) . ' . ' . input_tag('menit', 0, array('type' => 'number', 'min' => '0', 'max' => '59', 'style' => 'width:6%', 'required' => 'true')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Tempat</label>
                <div class="col-sm-10">
                    <?php echo input_tag('tempat', 'Ruang Sidang Sekretaris Daerah Kota Surabaya', array('class' => 'form-control', 'placeholder' => 'Tempat rapat', 'required' => 'true')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Acara</label>
                <div class="col-sm-10">
                    <?php
                    $temp_tahap = '';
                    if ($tahap == 'murni') {
                        $temp_tahap = 'Murni';
                    } elseif ($tahap == 'revisi1') {
                        $temp_tahap = 'Revisi I';
                    } elseif ($tahap == 'revisi2') {
                        $temp_tahap = 'Revisi II';
                    } elseif ($tahap == 'revisi3') {
                        $temp_tahap = 'Revisi III';
                    } elseif ($tahap == 'revisi4') {
                        $temp_tahap = 'Revisi IV';
                    } elseif ($tahap == 'revisi5') {
                        $temp_tahap = 'Revisi V';
                    } else
                        $temp_tahap = 'PAK';
                    ?>
                    <?php echo input_tag('acara', 'Membahas rencana pergeseran anggaran ' . sfConfig::get('app_tahun_default') . ' (' . $temp_tahap . ')', array('class' => 'form-control', 'placeholder' => 'Nama acara', 'required' => 'true')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">&nbsp;</label>
                <div class="col-sm-10">
                    <?php echo input_tag('acara2', '(Berdasarkan surat no. )', array('class' => 'form-control', 'placeholder' => 'Nama acara')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Pimpinan</label>
                <div class="col-sm-10">
                    <?php echo input_tag('pimpinan', null, array('class' => 'form-control', 'placeholder' => 'Pemimpin rapat', 'required' => 'true')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Catatan Resume Rapat</label>
                <div class="col-sm-10">
                    <?php echo textarea_tag('catatan', $deskripsi, array('class' => 'form-control')); ?>
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="col-sm-2 control-label">Kegiatan Belanja Tidak Langsung</label>
                <div class="col-sm-10">
                    <?php echo link_to('Edit Kegiatan Belanja Tidak Langsung', 'admin/kegiatanBtl?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan, array('class' => 'btn btn-link')); ?>
                </div>
            </div> -->
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <?php echo submit_tag('Print Resume Rapat', 'name=print class=btn btn-flat btn-success'); ?>
                    <?php echo submit_tag('Save Excel', 'name=excel class=btn btn-flat btn-success'); ?>
                    <?php echo reset_tag('Reset', 'class=btn btn-flat btn-warning'); ?>
                </div>
            </div>
            <?php echo '</form>'; ?>
        </div>
    </div>
</section>