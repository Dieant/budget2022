<?php

/**
 * gmap actions.
 *
 * @package    budgeting
 * @subpackage gmap
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class gmapActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
//                echo 'test';
    }

    public function executeGmap() {
        $nama = $this->getRequestParameter('user_id');
        $detno = $this->getRequestParameter('detail_no');
//        $l_id = $this->getRequestParameter('level');
        $keg = $this->getRequestParameter('kode_kegiatan');
//        $ket = $this->getRequestParameter('ket');
        $id = $this->getRequestParameter('id');
        $unit_dinas = $this->getRequestParameter('unit');
        //    $this->forward('default', 'module');
//        $this->forward('dinas','edit');
//      echo 'test';
//        $this->setLayout('layouteula');

        $kode1 = md5('insert');
        $kode2 = md5('update');
//        $kode3 = md5('batal');

        $d = new Criteria();
        $d->add(MasterUserV2Peer::USER_ID, $nama, criteria::ILIKE);
        $d->add(MasterUserV2Peer::USER_ENABLE, TRUE);
        $ds = MasterUserV2Peer::doSelectOne($d);
        if ($ds) {
            $nama_lengkap = $ds->getUserName();
            $c = new Criteria();
            $c->add(SchemaAksesV2Peer::USER_ID, $nama, criteria::ILIKE);
            $cs = SchemaAksesV2Peer::doSelectOne($c);
            if ($cs) {
                $level = $cs->getLevelId();

                if ($level == 1) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('dinas');
                    $username = $nama;


                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama, CRITERIA::ILIKE);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $dinas = $es->getUnitId();
                    }
                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                    $this->getResponse()->setCookie('nama_login', $nama);
                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }
                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);

                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    $f = new Criteria();
                    $f->add(RincianDetailPeer::KEGIATAN_CODE, $keg);
                    $f->add(RincianDetailPeer::UNIT_ID, $unit_dinas);
                    $f->add(RincianDetailPeer::DETAIL_NO, $detno);
                    $rd = RincianDetailPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getDetailName();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        $kode_rka = $unit_dinas . '.' . $keg . '.' . $detno;
                        budgetLogger::log('Komponen ' . $komponenName . ' ' . $komponenLokasi . ' berhasil dimunculkan karena telah dilakukan pemetaan lokasi dengan kode ' . $kode_rka);
                        budgetLogger::log('Menyimpan lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::tambah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil menyimpan Komponen dan Lokasi GMAP untuk komponen " . $komponenName);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        budgetLogger::log('Mengubah lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::ubah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi GMAP untuk komponen " . $komponenName);
                    }
                    return $this->redirect('dinas/edit?unit_id=' . $unit_dinas . '&kode_kegiatan=' . $keg);
                } elseif ($level == 2) {
                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $unit = $es->getUnitId();
                    }
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('peneliti');

                    $this->getResponse()->setCookie('nama', $unit);
                    $this->getResponse()->setCookie('nama_user', $nama);

                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                    $f = new Criteria();
                    $f->add(RincianDetailPeer::KEGIATAN_CODE, $keg);
                    $f->add(RincianDetailPeer::UNIT_ID, $unit_dinas);
                    $f->add(RincianDetailPeer::DETAIL_NO, $detno);
                    $rd = RincianDetailPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getDetailName();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        $kode_rka = $unit_dinas . '.' . $keg . '.' . $detno;
                        budgetLogger::log('Komponen ' . $komponenName . ' ' . $komponenLokasi . ' berhasil dimunculkan karena telah dilakukan pemetaan lokasi dengan kode ' . $kode_rka);
                        budgetLogger::log('Menyimpan lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::tambah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil menyimpan Komponen dan Lokasi GMAP untuk komponen " . $komponenName);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        budgetLogger::log('Mengubah lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::ubah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi GMAP untuk komponen " . $komponenName);
                    }
                    if ($nama == 'binaprogram' || $nama == 'asisten1' || $nama == 'asisten2' || $nama == 'asisten3' || $nama == 'bpk') {
                        return $this->redirect('view_rka/edit?kode_kegiatan=' . $keg.'&unit_id=' . $unit_dinas);
                    } else {
                        return $this->redirect('peneliti/edit?unit_id=' . $unit_dinas . '&kode_kegiatan=' . $keg);
                    }
                } elseif ($level == 9) {

//                    user_id=pengguna&unit=9999&kode_kegiatan=1.01.15.0001&detail_no=1&ket=&kode=e0df5f3dfd2650ae5be9993434e2b2c0
//                    $this->getResponse()->setCookie('nama', $unit);
//                    $this->getResponse()->setCookie('nama_user', $nama);
//
//                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                    $this->getUser()->setAuthenticated(true);
                    if (($nama == 'admin')) {
                        $this->getUser()->addCredential('admin_super');
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('user_admin', $nama);

                        $this->getUser()->setAttribute('user_id', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin_super');
                    } else {
                        $this->getUser()->addCredential('admin');
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('user_admin', $nama);

                        $this->getUser()->setAttribute('user_id', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin');
                    }
                    $f = new Criteria();
                    $f->add(RincianDetailPeer::KEGIATAN_CODE, $keg);
                    $f->add(RincianDetailPeer::UNIT_ID, $unit_dinas);
                    $f->add(RincianDetailPeer::DETAIL_NO, $detno);
                    $rd = RincianDetailPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getDetailName();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        $kode_rka = $unit_dinas . '.' . $keg . '.' . $detno;
                        budgetLogger::log('Komponen ' . $komponenName . ' ' . $komponenLokasi . ' berhasil dimunculkan karena telah dilakukan pemetaan lokasi dengan kode ' . $kode_rka);
                        budgetLogger::log('Menyimpan lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::tambah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil menyimpan Komponen dan Lokasi GMAP untuk komponen " . $komponenName);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        budgetLogger::log('Mengubah lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::ubah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi GMAP untuk komponen " . $komponenName);
                    }
                    return $this->redirect('kegiatan/edit?unit_id=' . $unit_dinas . '&kode_kegiatan=' . $keg);
                } else {
                    $salah = 1;
                }
            }
        }
    }

    public function executeGmapRevisi() {
        $nama = $this->getRequestParameter('user_id');
        $detno = $this->getRequestParameter('detail_no');
//        $l_id = $this->getRequestParameter('level');
        $keg = $this->getRequestParameter('kode_kegiatan');
//        $ket = $this->getRequestParameter('ket');
        $id = $this->getRequestParameter('id');
        $unit_dinas = $this->getRequestParameter('unit');
        //    $this->forward('default', 'module');
//        $this->forward('dinas','edit');
//      echo 'test';
//        $this->setLayout('layouteula');

        $kode1 = md5('insert');
        $kode2 = md5('update');
//        $kode3 = md5('batal');

        $d = new Criteria();
        $d->add(MasterUserV2Peer::USER_ID, $nama, criteria::ILIKE);
        $d->add(MasterUserV2Peer::USER_ENABLE, TRUE);
        $ds = MasterUserV2Peer::doSelectOne($d);
        if ($ds) {
            $nama_lengkap = $ds->getUserName();
            $c = new Criteria();
            $c->add(SchemaAksesV2Peer::USER_ID, $nama, criteria::ILIKE);
            $cs = SchemaAksesV2Peer::doSelectOne($c);
            if ($cs) {
                $level = $cs->getLevelId();

                if ($level == 1) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('dinas');
                    $username = $nama;


                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama, CRITERIA::ILIKE);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $dinas = $es->getUnitId();
                    }
                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                    $this->getResponse()->setCookie('nama_login', $nama);
                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }
                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);

                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    $f = new Criteria();
                    $f->add(DinasRincianDetailPeer::KEGIATAN_CODE, $keg);
                    $f->add(DinasRincianDetailPeer::UNIT_ID, $unit_dinas);
                    $f->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
                    $rd = DinasRincianDetailPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getDetailName();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        $kode_rka = $unit_dinas . '.' . $keg . '.' . $detno;
                        budgetLogger::log('Komponen ' . $komponenName . ' ' . $komponenLokasi . ' berhasil dimunculkan karena telah dilakukan pemetaan lokasi dengan kode ' . $kode_rka);
                        budgetLogger::log('Menyimpan lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        //historyUserLog::tambah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil menyimpan Komponen dan Lokasi GMAP untuk komponen " . $komponenName);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        budgetLogger::log('Mengubah lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        //historyUserLog::ubah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi GMAP untuk komponen " . $komponenName);
                    }
                    return $this->redirect('entri/edit?unit_id=' . $unit_dinas . '&kode_kegiatan=' . $keg);
                } elseif ($level == 2) {
                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $unit = $es->getUnitId();
                    }
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('peneliti');

                    $this->getResponse()->setCookie('nama', $unit);
                    $this->getResponse()->setCookie('nama_user', $nama);

                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                    $f = new Criteria();
                    $f->add(DinasRincianDetailPeer::KEGIATAN_CODE, $keg);
                    $f->add(DinasRincianDetailPeer::UNIT_ID, $unit_dinas);
                    $f->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
                    $rd = DinasRincianDetailPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getDetailName();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        $kode_rka = $unit_dinas . '.' . $keg . '.' . $detno;
                        budgetLogger::log('Komponen ' . $komponenName . ' ' . $komponenLokasi . ' berhasil dimunculkan karena telah dilakukan pemetaan lokasi dengan kode ' . $kode_rka);
                        budgetLogger::log('Menyimpan lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::tambah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil menyimpan Komponen dan Lokasi GMAP untuk komponen " . $komponenName);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        budgetLogger::log('Mengubah lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::ubah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi GMAP untuk komponen " . $komponenName);
                    }
                    if ($nama == 'binaprogram' || $nama == 'asisten1' || $nama == 'asisten2' || $nama == 'asisten3' || $nama == 'bpk') {
                        return $this->redirect('view_rka/editRevisi?kode_kegiatan=' . $keg.'&unit_id=' . $unit_dinas);
                    } else {
                        return $this->redirect('peneliti/editRevisi?unit_id=' . $unit_dinas . '&kode_kegiatan=' . $keg);
                    }
                } elseif ($level == 9) {

//                    user_id=pengguna&unit=9999&kode_kegiatan=1.01.15.0001&detail_no=1&ket=&kode=e0df5f3dfd2650ae5be9993434e2b2c0
//                    $this->getResponse()->setCookie('nama', $unit);
//                    $this->getResponse()->setCookie('nama_user', $nama);
//
//                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                    $this->getUser()->setAuthenticated(true);
                    if (($nama == 'admin')) {
                        $this->getUser()->addCredential('admin_super');
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('user_admin', $nama);

                        $this->getUser()->setAttribute('user_id', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin_super');
                    } else {
                        $this->getUser()->addCredential('admin');
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('user_admin', $nama);

                        $this->getUser()->setAttribute('user_id', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin');
                    }
                    $f = new Criteria();
                    $f->add(DinasRincianDetailPeer::KEGIATAN_CODE, $keg);
                    $f->add(DinasRincianDetailPeer::UNIT_ID, $unit_dinas);
                    $f->add(DinasRincianDetailPeer::DETAIL_NO, $detno);
                    $rd = DinasRincianDetailPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getDetailName();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        $kode_rka = $unit_dinas . '.' . $keg . '.' . $detno;
                        budgetLogger::log('Komponen ' . $komponenName . ' ' . $komponenLokasi . ' berhasil dimunculkan karena telah dilakukan pemetaan lokasi dengan kode ' . $kode_rka);
                        budgetLogger::log('Menyimpan lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::tambah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil menyimpan Komponen dan Lokasi GMAP untuk komponen " . $komponenName);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        budgetLogger::log('Mengubah lokasi GMAP komponen ' . $komponenName . ' ' . $komponenLokasi);

                        historyUserLog::ubah_lokasi($unit_dinas, $keg, $detno);

                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi GMAP untuk komponen " . $komponenName);
                    }
                    return $this->redirect('kegiatan/editRevisi?unit_id=' . $unit_dinas . '&kode_kegiatan=' . $keg);
                } else {
                    $salah = 1;
                }
            }
        }
    }

    public function executeGmap_waitinglist() {
        $nama = $this->getRequestParameter('user_id');
        $id_waiting = $this->getRequestParameter('id_waiting');
//        $l_id = $this->getRequestParameter('level');
        $keg = $this->getRequestParameter('kode_kegiatan');
//        $ket = $this->getRequestParameter('ket');
        $unit_dinas = $this->getRequestParameter('unit');
        //    $this->forward('default', 'module');
//        $this->forward('dinas','edit');
//      echo 'test';
//        $this->setLayout('layouteula');

        $kode1 = md5('insert');
        $kode2 = md5('update');
//        $kode3 = md5('batal');
        //$con = Propel::getConnection();
//        $c = new Criteria();
//        $c->add(RincianDetailPeer::UNIT_ID, $dinas);
//        $c->add(RincianDetailPeer::KEGIATAN_CODE, $keg);
//        $c->add(RincianDetailPeer::DETAIL_NO, $id);
//        $rd = RincianDetailPeer::doSelectOne($c);
//                    echo $u_id.'--'.$l_id;exit;
//			$nama = $this->getRequestParameter('send_user');
// echo 'ok99';exit;
        $d = new Criteria();
        $d->add(MasterUserV2Peer::USER_ID, $nama, criteria::ILIKE);
        $d->add(MasterUserV2Peer::USER_ENABLE, TRUE);
        $ds = MasterUserV2Peer::doSelectOne($d);
        if ($ds) {
            $nama_lengkap = $ds->getUserName();
            $c = new Criteria();
            $c->add(SchemaAksesV2Peer::USER_ID, $nama, criteria::ILIKE);
            $cs = SchemaAksesV2Peer::doSelectOne($c);
            if ($cs) {
                $level = $cs->getLevelId();
                if ($level == 1) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('dinas');
                    $username = $nama;


                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama, CRITERIA::ILIKE);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $dinas = $es->getUnitId();
                    }
                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                    $this->getResponse()->setCookie('nama_login', $nama);
                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }
                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);

                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    $f = new Criteria();
                    $f->add(WaitingListPUPeer::KEGIATAN_CODE, $keg);
                    $f->add(WaitingListPUPeer::UNIT_ID, $unit_dinas);
                    $f->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                    $rd = WaitingListPUPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getKomponenLokasi();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        if ($rd) {
                            $rd->setStatusHapus(false);
                            $rd->save();
                        }
                        budgetLogger::log($nama . ' menyimpan lokasi Waiting List komponen ' . $komponenName . ' ' . $komponenLokasi);
                        $this->setFlash('berhasil', "Berhasil menyimpan Lokasi Waiting List untuk komponen " . $komponenName . ' ' . $komponenLokasi);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        if ($rd) {
                            $rd->setStatusHapus(false);
                            $rd->save();
                        }
                        budgetLogger::log($nama . ' mengubah lokasi Waiting List komponen ' . $komponenName . ' ' . $komponenLokasi);
                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi Waiting List untuk komponen " . $komponenName . ' ' . $komponenLokasi);
                    } else {
                        
                    }
                    return $this->redirect('waitinglist_pu/waitinglist');
                } elseif ($level == 2) {
                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $unit = $es->getUnitId();
                    }
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('peneliti');

                    $this->getResponse()->setCookie('nama', $unit);
                    $this->getResponse()->setCookie('nama_user', $nama);

                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                    $f = new Criteria();
                    $f->add(WaitingListPUPeer::KEGIATAN_CODE, $keg);
                    $f->add(WaitingListPUPeer::UNIT_ID, $unit_dinas);
                    $f->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                    $rd = WaitingListPUPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getKomponenLokasi();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        if ($rd) {
                            $rd->setStatusHapus(false);
                            $rd->save();
                        }
                        budgetLogger::log($nama . ' menyimpan lokasi Waiting List komponen ' . $komponenName . ' ' . $komponenLokasi);
                        $this->setFlash('berhasil', "Berhasil menyimpan Lokasi Waiting List untuk komponen " . $komponenName . ' ' . $komponenLokasi);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        if ($rd) {
                            $rd->setStatusHapus(false);
                            $rd->save();
                        }
                        budgetLogger::log($nama . ' mengubah lokasi Waiting List komponen ' . $komponenName . ' ' . $komponenLokasi);
                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi Waiting List untuk komponen " . $komponenName . ' ' . $komponenLokasi);
                    }
                    return $this->redirect('waitinglist_pu/waitinglist');
                } elseif ($level == 9) {

//                    user_id=pengguna&unit=9999&kode_kegiatan=1.01.15.0001&detail_no=1&ket=&kode=e0df5f3dfd2650ae5be9993434e2b2c0
//                    $this->getResponse()->setCookie('nama', $unit);
//                    $this->getResponse()->setCookie('nama_user', $nama);
//
//                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
//                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                    $this->getUser()->setAuthenticated(true);
                    if (($nama == 'admin')) {
                        $this->getUser()->addCredential('admin_super');
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('user_admin', $nama);

                        $this->getUser()->setAttribute('user_id', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin_super');
                    } else {
                        $this->getUser()->addCredential('admin');
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('user_admin', $nama);

                        $this->getUser()->setAttribute('user_id', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin');
                    }
                    $f = new Criteria();
                    $f->add(WaitingListPUPeer::KEGIATAN_CODE, $keg);
                    $f->add(WaitingListPUPeer::UNIT_ID, $unit_dinas);
                    $f->add(WaitingListPUPeer::ID_WAITING, $id_waiting);
                    $rd = WaitingListPUPeer::doSelectOne($f);
                    if ($rd) {
                        $komponenName = $rd->getKomponenName();
                        $komponenLokasi = $rd->getKomponenLokasi();
                    }

                    if ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode1) {
                        if ($rd) {
                            $rd->setStatusHapus(false);
                            $rd->save();
                        }
                        budgetLogger::log($nama . ' menyimpan lokasi Waiting List komponen ' . $komponenName . ' ' . $komponenLokasi);
                        $this->setFlash('berhasil', "Berhasil menyimpan Lokasi Waiting List untuk komponen " . $komponenName . ' ' . $komponenLokasi);
                    } elseif ($this->getRequestParameter('user_id') && $this->getRequestParameter('kode') == $kode2) {
                        if ($rd) {
                            $rd->setStatusHapus(false);
                            $rd->save();
                        }
                        budgetLogger::log($nama . ' mengubah lokasi Waiting List komponen ' . $komponenName . ' ' . $komponenLokasi);
                        $this->setFlash('berhasil', "Berhasil mengubah Lokasi WaitingList untuk komponen " . $komponenName . ' ' . $komponenLokasi);
                    }
                    return $this->redirect('waitinglist_pu/waitinglist');
                } else {
                    $salah = 1;
                }
            }
        }
    }

}
