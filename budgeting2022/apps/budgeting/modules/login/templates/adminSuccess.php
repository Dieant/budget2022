<?php use_helper('I18N', 'Date') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>
<?php echo use_helper('Object', 'Javascript', 'Validation') ?>
<BR>
<BR>
<BR>
<?php
$cek_salah = $sf_params->get('salah');
if ($cek_salah == 1) {
    echo javascript_tag("alert('Id dan Password salah!')");
    echo javascript_tag("location.href= '" . url_for('login/admin') . "'");
}
?>
<?php echo form_tag('login/admin') ?>
<div id="sidebar">
    <div id="login" class="boxed">
        <h2 class="title">Login Admin</h2>
        <div class="content" style="min-height: 25px">
            <fieldset>
                <legend>Sign-In</legend>
                <label for="inputtext1">Nama :</label>
                <?php echo input_tag('send_user', $sf_params->get('send_user'), Array('type' => 'text', 'class' => 'inputtext1', 'id' => 'send_user')) ?>
                <label for="inputtext2">Password:</label>
                <?php echo input_password_tag('password', '', Array('type' => 'password', 'class' => 'inputtext2', 'id' => 'send_user')) ?>
                <?php echo input_hidden_tag('referer', $sf_request->getAttribute('referer')) ?>
                <input id="inputsubmit1" type="submit" name="inputsubmit1" value="Sign In" />
                <p><?php echo link_to('[ main menu ]', '@homepage') ?></p>
            </fieldset>
        </div>
    </div>
</div>
<div id="main">
    <div id="welcome" class="post">
        <h2 class="title">Administrator</h2>
        <div class="story">
            Selamat menggunakan halaman fasilitas Peneliti,</a><br />
            Dalam halaman ini anda berhak untuk :
        </div>
        <div class="meta">
            <p>- Membuat dan mengubah segala data Master yang ada dalam sistem.<br />
                - Membuat dan mengubah Login User<br />
                - Melakukan Lock pada kegiatan agar tidak dapat diubah.<br />
                - Berperan dan berhak menjadi login lain selain Administrator.<br />
                - Melakukan pergantian tahap dan melihat history.<br />
                - Melihat report.</p>
        </div>
    </div>
</div>
<!--
  <table width="34%"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="1" background="/budget2010/web/images/grs_vert.gif"></td>
      <td width="306" height="20" align="center" background="/budget2010/web/images/menu_bg.gif" class="font12"><font color="#FFFFFF">Login Administrator</font></td>
      <td width="1" background="/budget2010/web/web/images/grs_vert.gif"></td>
    </tr>
    <tr>
      <td width="1" background="/budget2010/web/images/grs_vert.gif"></td>
      <td align="center"><br>
          <table width="65%"  border="0" cellpadding="3" cellspacing="1">
            <tr align="left">
              <td class="Font10v">Username</td>
              <td colspan="2"><?php echo input_tag('send_user', $sf_params->get('send_user'), Array('type' => 'text', 'class' => 'inpText', 'id' => 'send_user')) ?></td>
            </tr>
            <tr align="left">
              <td class="Font10v">Password</td>
              <td colspan="2"><?php echo input_password_tag('password', '', Array('type' => 'password', 'class' => 'inpText', 'id' => 'send_user')) ?></td>
            </tr>
            <tr>
              <td width="24%"><?php echo input_hidden_tag('referer', $sf_request->getAttribute('referer')) ?></td>
              <td width="48%"><?php echo submit_tag('Login', array('class' => 'inpButton')); ?></td>
              <td width="28%"></td>
            </tr>
        </table></td>
      <td width="1" background="/budget2010/web/images/grs_vert.gif"></td>
    </tr>
    <tr>
      <td width="1" background="/budget2010/web/images/grs_vert.gif"></td>
      <td height="1" background="/budget2010/web/images/grs_hors.gif"></td>
      <td width="1" background="/budget2010/web/images/grs_vert.gif"></td>
    </tr>
  </table>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <table width="100%"><tr>
    <td align="center" ><?php echo link_to('[ main menu ]', '@homepage') ?></td>
  </tr></table>-->
</form>