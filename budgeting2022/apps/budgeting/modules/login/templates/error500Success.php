<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">

    <div class="error-page" style="width: 60%">
        <h2 class="headline text-yellow"> 500</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i>Maaf! Halaman yang dicari tidak ada.</h3>
            <p>
                <b>Apakah Anda mengetikkan sendiri alamat ini?</b>
            <ul>
                <li>Kemungkinan besar Anda salah ketik.</li>
                <li>Kemungkinan besar Anda akan melakukan kecurangan.</li>
                <li>Coba cek untuk meyakinkan Anda telah benar untuk ejaan, huruf besar, maupun kodenya.</li>
            </ul>
            <b>Apakah Anda memilih link ini dengan cara click pada halaman lain?</b>
            <ul>
                <li>Kalau begitu, mohon hubungi webmaster mengenai masalah ini. Terima kasih.</li>
            </ul>
            <div class="text-center">
                <a class="btn btn-flat btn-warning" href="javascript:history.back(-1)">Kembali ke halaman sebelumnya</a>    
            </div>
            </p>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
</section><!-- /.content -->