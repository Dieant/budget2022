<?php

/**
 * login actions.
 *
 * @package    budgeting
 * @subpackage login
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class loginActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeLoginPindahan() {
        //$this->forward('default', 'module');
        if ($this->getUser()->isAuthenticated()) {
            $this->getResponse()->setCookie('nama_user', '');
            $this->getResponse()->setCookie('nama', '');
            $this->getResponse()->setCookie('user_id', '');
            $this->getUser()->logOut();
        }

        $this->setLayout('login_pindahan');
    }

    public function executeSalah() {
        return sfView::SUCCESS;
    }

    protected function blowfishDecrypt($data, $key) {
        $iv = pack('H*', substr($data, 0, 16));
        $x = pack('H*', substr($data, 16));
        $result = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $x, MCRYPT_MODE_CBC, $iv);
        return $result;
    }

    protected function blowfishEncrypt($data, $key) {
        $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $cryptText = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $data, MCRYPT_MODE_CBC, $iv);
        return bin2hex($iv . $cryptText);
    }

    protected function connect_eper_new($uname, $passw) {
        $data = array(
            'login[role]' => $this->encrypt(1),
            'login[username]' => $this->encrypt($uname),
            'login[password]' => $this->encrypt($passw),
            'login[token]' => $this->encrypt(sprintf('eperformance-%s', date('d-m-Y-H')))
        );

        $url = 'https://eperformance.surabaya.go.id/2017/api-get-login-route'; // Specify your url
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        if ($this->decrypt($output) == 'Mong60') {
            $out = 'Y';
        } else {
            $out = 'N';
        }
        curl_close($ch);
        // return $this->decrypt($output);
        // var_dump($this->decrypt($output));die();
        return $out;
    }

    protected function encrypt($str) {
        $hasil = '';
        $kunci = '979a218e0632d1234567890f2935317f98d47956c7';
        for ($i = 0; $i < strlen($str); $i++) {
            $karakter = substr($str, $i, 1);
            $kuncikarakter = substr($kunci, ($i % strlen($kunci)) - 1, 1);
            $karakter = chr(ord($karakter) + ord($kuncikarakter));
            $hasil .= $karakter;
        }
        return urlencode(base64_encode($hasil));
    }

    protected function decrypt($str) {
        $str = base64_decode(urldecode($str));
        $hasil = '';
        $kunci = '979a218e0632d1234567890f2935317f98d47956c7';
        for ($i = 0; $i < strlen($str); $i++) {
            $karakter = substr($str, $i, 1);
            $kuncikarakter = substr($kunci, ($i % strlen($kunci)) - 1, 1);
            $karakter = chr(ord($karakter) - ord($kuncikarakter));
            $hasil .= $karakter;
        }
        return $hasil;
    }

    public function executeNewsatulogin() {
        $portal = $this->getRequestParameter('portal');
        $c = base64_decode($portal);
        $json_portal = json_decode($c, true);
        $ciphertext64 = $json_portal['value'];
        $key64 = "sEYcjk7OZ8YEUE0yRQWnuBI5wMvAx6AM+1fAh/ig5W4=";
        $iv64 = $json_portal['iv'];

        $key = base64_decode($key64, true);
        $iv = base64_decode($iv64, true);
        print_r($ciphertext64.'<br>'.$iv64.'<br>'.$key.'<br>'.$iv);
        $decrypted = openssl_decrypt($ciphertext64, 'AES-256-CBC', $key, 0, $iv);
        print_r($decrypted);
        echo "openssl version text: " . OPENSSL_VERSION_TEXT . "\n";
        echo "openssl version number:  " . OPENSSL_VERSION_NUMBER . "\n";
        die();
        $nip = '';
        $role = '';
        
        $c = new Criteria();
        $c->add(UserLevelPeer::ROLE_SSO, $role);
        $roles = UserLevelPeer::doSelectOne($c);
        $role_id = $roles->getLevelId();

        //$role_id = 14;
        $c = new Criteria();
        $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
        $c->add(SchemaAksesV2Peer::LEVEL_ID, $role_id);
        $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
        $c->add(MasterUserV2Peer::NIP, $nip);
        $c->add(MasterUserV2Peer::USER_ENABLE, TRUE);
        $ds = MasterUserV2Peer::doSelectOne($c);

        $nama = $ds->getUserId();

        if ($ds) {
            $cpass = NULL;
            $nama_lengkap = $ds->getUserName();
            $c = new Criteria();
            $c->add(SchemaAksesV2Peer::USER_ID, $nama);
            $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
            $cs = SchemaAksesV2Peer::doSelectOne($c);

            if ($cs) {
                $level = $cs->getLevelId();
                $cpass = $cs->getIsUbahPass();
                if ($level == 7) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('dewan');
                    $this->getUser()->setAttribute('user_id', $nama, 'dewan');
                    $this->getUser()->setAttribute('user_id', $nama, 'dewan');
                    $this->getResponse()->setCookie('nama', $nama);
                    $this->getUser()->setAttribute('nama_user', $nama, 'dewan');
                    budgetLogger::log('Dewan dengan username ' . $nama . ' Login ke sistem e-budgeting');
                    return $this->redirect('dewan/listRevisi');
                } elseif ($level == 1) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('dinas');
                    $username = $nama;
                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                    $es = UserHandleV2Peer::doSelectOne($e);

                    if ($es) {
                        $dinas = $es->getUnitId();
                    }

                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $this->getResponse()->setCookie('nama_login', $nama);
                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);

                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }

                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

                    if ($nama_lengkap == 'gis_2600') {
                        return $this->redirect('waitinglist_pu/waitinglist');
                    } else {
                        return $this->redirect('entri/eula?unit_id=' . $dinas);
                    }
                } elseif ($level == 2) {
                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    {
                        $peneliti = $es->getUnitId();
                    }    
  
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('peneliti');
                    $this->getResponse()->setCookie('nama', $peneliti);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');
                    budgetLogger::log('Peneliti dengan username ' . $nama . ' Login ke sistem e-budgeting');
                    if ($nama == 'bpk') {
                       return $this->redirect('bpk/eula');
                    }
                    return $this->redirect('peneliti/listRevisi?unit_id=' . $peneliti);
                } elseif ($level == 9) {
                    if (($nama == 'admin') || ($nama == 'superadmin_ebudgeting')) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('admin_super');
                        $this->getUser()->setAttribute('user_id', $nama, 'admin_super');
                        $this->getResponse()->setCookie('user_admin', $nama);
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin_super');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin_super');
                    } else {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('admin');
                        $this->getUser()->setAttribute('user_id', $nama, 'admin');
                        $this->getResponse()->setCookie('user_admin', $nama);
                        $this->getUser()->setAttribute('nama_user', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_login', $nama, 'admin');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin');
                    }

                    return $this->redirect('user_app/userlist');
                } elseif ($level == 12) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('viewer');
                    $this->getUser()->setAttribute('user_id', $nama, 'viewer');
                    $this->getResponse()->setCookie('nama', $nama);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getResponse()->setCookie('nama_login', $nama);
                    $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');
                    
                    if ($nama == 'anggaran' || $nama == 'bagian_hukum' || $nama == 'inspektorat' || $nama == 'bkd' || $nama == 'bkpsdm' || $nama == 'lppa' || $nama == 'bagian_organisasi' || $nama == 'bapenda'){
                        return $this->redirect('anggaran/listRevisi');                                
                    }

                    if ($nama == 'bappeko' || $nama == 'evalitbang' || $nama == 'penanda_bappeko'){
                        return $this->redirect('view_rka/listRevisi');
                    }

                    // if ($nama == 'kemenpan_rb'){
                    //     return $this->redirect('view_rka/list');
                    // }

                    if ($nama == 'prk') {
                        $this->getUser()->setAttribute('send_user', $nama);
                        $this->getUser()->setAttribute('folder', 'bappeko');
                    }

                    if(strpos($nama, 'survey') !== false) {
                            $nama='survey';
                    } 
   
                    if ($nama == 'survey'){
                        return $this->redirect('survey/sshlocked');
                    }       
                    budgetLogger::log('Viewer dengan username ' . $nama . ' Login ke sistem e-budgeting');
                    if ($nama == 'kemenpan_rb'){
                        return $this->redirect('view_rka/list');
                    }
                    else
                    {
                         return $this->redirect('view_rka/listRevisi');             
                    }
                                               
                } elseif ($level == 13) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('pptk');
                    $username = $nama;

                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $dinas = $es->getUnitId();
                    }
                    
                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                    
                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }

                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getResponse()->setCookie('nama_login', $nama);
                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

                    if ($nama_lengkap == 'gis_2600') {
                        return $this->redirect('waitinglist_pu/waitinglist');
                    } else {
                        return $this->redirect('entri/eula?unit_id=' . $dinas);
                    }
                } elseif ($level == 14) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('kpa');
                    $username = $nama;

                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                    $es = UserHandleV2Peer::doSelectOne($e);
                    if ($es) {
                        $dinas = $es->getUnitId();
                    }
                    
                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);

                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }
                    
                    $this->getResponse()->setCookie('nama_login', $nama);
                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');
       
                    if ($nama_lengkap == 'gis_2600') {
                        return $this->redirect('waitinglist_pu/waitinglist');
                    } else {
                        return $this->redirect('entri/eula?unit_id=' . $dinas);
                    }
                } elseif ($level == 15) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('pa');
                    $username = $nama;

                    $e = new Criteria();
                    $e->add(UserHandleV2Peer::USER_ID, $nama);
                    $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                    $es = UserHandleV2Peer::doSelectOne($e);

                    if ($es) {
                        $dinas = $es->getUnitId();
                    }

                    $f = new Criteria();
                    $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                    $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                    $this->getResponse()->setCookie('nama_login', $nama);

                    if ($rs_unitkerja) {
                        $nama = $rs_unitkerja->getUnitName();
                    }

                    $this->getResponse()->setCookie('nama', $dinas);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getUser()->setAttribute('user_id', $nama, 'dinas');
                    $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                    $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                    $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                    $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                    $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                    budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

                    if ($nama_lengkap == 'gis_2600') {
                        return $this->redirect('waitinglist_pu/waitinglist');
                    } else {
                        return $this->redirect('entri/eula?unit_id=' . $dinas);
                    }
                } elseif ($level == 16) {
                    $this->getUser()->setAuthenticated(true);
                    $this->getUser()->addCredential('bappeko');
                    $this->getResponse()->setCookie('nama', $nama);
                    $this->getResponse()->setCookie('nama_user', $nama);
                    $this->getResponse()->setCookie('nama_login', $nama);
                    $this->getUser()->setAttribute('user_id', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                    budgetLogger::log('Bappeko dengan username ' . $nama . ' Login ke sistem e-budgeting');
                    return $this->redirect('anggaran/listRevisi');
                }
            }
        }
    }

    public function executeSatulogin() {
        try {
            // echo "a";die();
            $salah = 0;
            //print_r('bisma');exit;
            if ($this->getRequest()->getMethod() != sfRequest::POST) {
                // echo "a";die();
                // display the form
                $this->getRequest()->setAttribute('referer', $this->getRequest()->getReferer());
            } elseif ($this->getRequest()->getMethod() == sfRequest::POST) {
                // echo "a";die();
                $nama = $this->getRequestParameter('send_user');


                $password_asli = $this->getRequestParameter('password');
                $password = md5($this->getRequestParameter('password'));
                if ($this->getRequestParameter('passwordmd5')) {
                    $password = $this->getRequestParameter('passwordmd5');
                }
                // print_r($nama.' '.$password);exit;
                $c = new Criteria();
                $c->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                $c->add(SchemaAksesV2Peer::LEVEL_ID, 13, Criteria::NOT_EQUAL);
                $c->add(SchemaAksesV2Peer::LEVEL_ID, 14, Criteria::NOT_EQUAL);
                //$c->add(SchemaAksesV2Peer::LEVEL_ID, 15, Criteria::NOT_EQUAL);
                $c->add(SchemaAksesV2Peer::LEVEL_ID, 16, Criteria::NOT_EQUAL);
                $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                $c->add(MasterUserV2Peer::USER_ID, $nama);
                $c->add(MasterUserV2Peer::USER_PASSWORD, $password);
                $c->add(MasterUserV2Peer::USER_ENABLE, TRUE);
                $ds = MasterUserV2Peer::doSelectOne($c);
                // print_r($c);die();
                if (!$ds) {
                    $nama = preg_replace("/[^[:alnum:]]/u", '', $nama);
                    $is_bappeko = FALSE;
                    if (substr($nama, 0, 1) == 'b') {
                        //echo "aaaa";die();
                        $nip = substr($nama, 1);
                        // echo $nip;die();
                        $e = new Criteria();
                        $e->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                        $e->add(MasterUserV2Peer::NIP, $nip);
                        $e->add(MasterUserV2Peer::USER_ENABLE, TRUE);
                        $e->add(SchemaAksesV2Peer::LEVEL_ID, 16);
                        $e->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                        $ds3 = MasterUserV2Peer::doSelectOne($e);
                        $is_bappeko = TRUE;
                        // if ($ds3) {
                        //     # code...
                        //     // echo "ds3 isi";die();
                        //     print_r($e);die();
                        // }
                        // else {
                        //     //echo "ds3 kosong";die();
                        //     print_r($e);die();
                        // }
                    } else {
                        $nip = $nama;
                        $e = new Criteria();
                        $e->addJoin(MasterUserV2Peer::USER_ID, SchemaAksesV2Peer::USER_ID);
                        $e->add(MasterUserV2Peer::NIP, $nama);
                        $e->add(MasterUserV2Peer::USER_ENABLE, TRUE);
                        $e->addOr(SchemaAksesV2Peer::LEVEL_ID, 13);
                        $e->addOr(SchemaAksesV2Peer::LEVEL_ID, 14);
                        $e->addOr(SchemaAksesV2Peer::LEVEL_ID, 15);
                        //$e->addOr(SchemaAksesV2Peer::LEVEL_ID, 16);
                        $e->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                        $ds3 = MasterUserV2Peer::doSelectOne($e);
                    }
                    if ($ds3) {
                        // echo "ds3 isi";die();
                        // $d = new Criteria();
                        // $d->add(PegawaiPerformancePeer::NIP, $nip);
                        // $d->add(PegawaiPerformancePeer::PASSWORD, $password);
                        // $ds2 = PegawaiPerformancePeer::doSelectOne($d);
                        // cek jika mitra bappeko
                        if ($is_bappeko == TRUE ){
                            // $nama = $nip;  
                            $ds = $ds3;                            
                        }
                        // cek jika pejabat PLT
                        // if(strpos($nama, 'A') !== false ){
                        //     $temp = array();
                        //     $temp = explode('A', $nama);
                        //     $nama = $temp[0];
                        // }

                        // if ($this->connect_eper_new($nama, $password_asli) == 'Y') {
                        //     $ds = $ds3;
                        //     $nama = $ds3->getUserId();

                        //     if (sfConfig::get('app_fasilitas_cekLogin') == 'buka') {
                        //         if (!$is_bappeko) {
                        //             //cek perubahan skpd ketika login
                        //             $c = new Criteria();
                        //             $c->add(UserHandleV2Peer::USER_ID, $nama);
                        //             $rs_skpd = UserHandleV2Peer::doSelectOne($c);
                        //             $unit_id = $rs_skpd->getUnitId();
                        //             $unit_id_lama = $rs_skpd->getUnitId();

                        //             $conp = Propel::getConnection('performance');
                        //             $query = "select p.nip, j.nama, s.skpd_kode
                        //         from " . sfConfig::get('app_default_eperformance') . ".pegawai p, " . sfConfig::get('app_default_eperformance') . ".jabatan_struktural j, " . sfConfig::get('app_default_eperformance') . ".master_skpd s
                        //         where p.nip='$nip' and p.jabatan_struktural_id=j.id and p.skpd_id=s.skpd_id";
                        //             $stmt = $conp->prepareStatement($query);
                        //             $rs = $stmt->executeQuery();
                        //             if ($rs->next()) {
                        //                 $unit_id = $rs->getString('skpd_kode');
                        //                 $jabatan = $rs->getString('nama');
                        //             }

                        //             $con = Propel::getConnection();
                        //             $con->begin();
                        //             if ($unit_id != $unit_id_lama) {
                        //                 $query = "update user_handle_v2 set unit_id='$unit_id' where user_id='$nama' and schema_id=2";
                        //                 $stmt = $con->prepareStatement($query);
                        //                 $stmt->executeQuery();

                        //                 $query = "update master_user_v2 set jabatan='$jabatan' where user_id='$nama'";
                        //                 $stmt = $con->prepareStatement($query);
                        //                 $stmt->executeQuery();

                        //                 $c = new Criteria();
                        //                 $c->add(SchemaAksesV2Peer::USER_ID, $nama);
                        //                 $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                        //                 $cs = SchemaAksesV2Peer::doSelectOne($c);
                        //                 if ($cs) {
                        //                     $role = $cs->getLevelId();
                        //                 }
                        //                 if ($role == 13) {
                        //                     $d = new Criteria();
                        //                     $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_lama);
                        //                     $d->add(DinasMasterKegiatanPeer::USER_ID_PPTK, $nama);
                        //                     $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                        //                     if ($rs_kegiatan) {
                        //                         $rs_kegiatan->setUserIdPptk(NULL);
                        //                         $rs_kegiatan->save();
                        //                     }
                        //                 } else if ($role == 14) {
                        //                     $d = new Criteria();
                        //                     $d->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id_lama);
                        //                     $d->add(DinasMasterKegiatanPeer::USER_ID_KPA, $nama);
                        //                     $rs_kegiatan = DinasMasterKegiatanPeer::doSelectOne($d);
                        //                     if ($rs_kegiatan) {
                        //                         $rs_kegiatan->setUserIdKpa(NULL);
                        //                         $rs_kegiatan->save();
                        //                     }
                        //                 }
                        //                 budgetLogger::log('Username ' . $nama . ' terjadi perubahan SKPD. (' . $unit_id_lama . ' ke ' . $unit_id . ')');
                        //             }
                        //             $con->commit();
                        //         }
                        //     }
                        // }
                        // else{
                        //     var_dump("password ga cocok dengan eperf");die();
                        //     // password ga cocok dengan eperformance
                        //     // var_dump($this->connect_eper_new($nama, $password_asli));die();
                        // }
                    }
                }
//                if ($password == md5('12345') && $nama != 'admin') {
//                    $c = new Criteria();
//                    $c->add(MasterUserV2Peer::USER_ID, $nama);
//                    $c->add(MasterUserV2Peer::USER_ENABLE, TRUE);
//                    $ds = MasterUserV2Peer::doSelectOne($c);
//                }
                if ($ds) {
                    // echo "aa";die();
                    $cpass = NULL;
                    //print_r($ds->getJenisKelamin());exit;
                    $nama_lengkap = $ds->getUserName();
                    $c = new Criteria();
                    $c->add(SchemaAksesV2Peer::USER_ID, $nama);
                    $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                    //$c->addDescendingOrderByColumn(SchemaAksesV2Peer::SCHEMA_ID);
                    /* $c -> add(SchemaAksesV2Peer::LEVEL_ID, 7); */
                    $cs = SchemaAksesV2Peer::doSelectOne($c);

                    if ($cs) {
                        $level = $cs->getLevelId();
                        $cpass = $cs->getIsUbahPass();
                        //$this->getUser()->logOut();
                        if ($level == 7) {
                            //$salah=2;                           
//                        
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('dewan');
                            $this->getUser()->setAttribute('user_id', $nama, 'dewan');
                            $this->getUser()->setAttribute('user_id', $nama, 'dewan');

                            $this->getResponse()->setCookie('nama', $nama);

                            $this->getUser()->setAttribute('nama_user', $nama, 'dewan');

                            budgetLogger::log('Dewan dengan username ' . $nama . ' Login ke sistem e-budgeting');
                            $cpass = $cs->getIsUbahPass();
                            return $this->redirect('dewan/listRevisi?menu=Belanja');
//                        
                            //irul 13 maret - lempar ke budgetting
//                        budgetLogger::log('Dewan dengan username ' . $nama . ' Login ke sistem e-budgeting');
//                        $data = $nama . '|' . $password;
////                            $this->redirect('http://gmap.surabaya-eproc.or.id/budget2016sfs/index.php/loginn/'.$nama.'/ebudget.html');
//                        $this->redirect('http://budgetting.surabaya2excellence.or.id/budget2016sf/index.php/loginn/' . $this->blowfishEncrypt($data, 'p@rl3m3nG!tuLhoh') . '/ebudget.html');
                            //irul 13 maret - lempar ke budgetting
                        } elseif ($level == 1) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('dinas');
                            $username = $nama;
                            

                            $e = new Criteria();
                            $e->add(UserHandleV2Peer::USER_ID, $nama);
                            $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                            $es = UserHandleV2Peer::doSelectOne($e);
                            if ($es) {
                                $dinas = $es->getUnitId();
                            }

                            $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                            $this->getResponse()->setCookie('nama_login', $nama);
                            $f = new Criteria();
                            $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);

                            if ($rs_unitkerja) {
                                $nama = $rs_unitkerja->getUnitName();
                            }

                            $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                            $this->getResponse()->setCookie('nama', $dinas);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                            $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                            $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                            $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                            budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                            //historyUserLog::user_login();
                            $string='kosong';
#52 - save Login user           
                            if ($nama_lengkap == 'gis_2600') {
                                return $this->redirect('waitinglist_pu/waitinglist');
                            } 
                            else if (is_null($cpass)) {
                                return $this->redirect('entri/ubahPassLogin');
                            } else if (strpos($username, $string) !== false) {
    // substring2 is found in string
                                $salah=2;  
                            }
                            else {
                                return $this->redirect('entri/eula?unit_id=' . $dinas);
                            }
                        } elseif ($level == 2) {

                            if ($ds->getEulaBudgeting() == FALSE && $nama != 'parlemen_a' && $nama != 'parlemen_b' && $nama != 'parlemen_c' && $nama != 'parlemen_d' && $nama != 'wawali') {
                                $e = new Criteria();
                                $e->add(UserHandleV2Peer::USER_ID, $nama);
                                $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                                $es = UserHandleV2Peer::doSelectOne($e);
                                {
                                    $peneliti = $es->getUnitId();
                                }
#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user           

                                $array_nama_user = array('walikota', 'asisten_walikota', 'delayu', 'putri', 'ika', 'binaprogram', 'asisten1', 'asisten2', 'asisten3', 'bpk', 'diana', 'eka', 'parlemen2', 'bppk', 'dppk', 'reza', 'masger', 'inspect', 'murni', 'sudadi', 'peninjau', 'bappeko', 'prk', 'bawas', 'inspektorat1', 'inspektorat2', 'inspektorat3', 'evalitbang', 'penanda_bappeko', 'sekdasby');
                                if (in_array($nama, $array_nama_user)) {
                                    $this->getUser()->setAuthenticated(true);
                                    $this->getUser()->addCredential('viewer');
                                    $this->getUser()->setAttribute('user_id', $nama, 'viewer');

                                    $this->getResponse()->setCookie('nama', $nama);
                                    $this->getResponse()->setCookie('nama_user', $nama);

                                    $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                                    $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                                    if ($nama == 'prk') {
                                        $this->getUser()->setAttribute('send_user', $nama);
                                        $this->getUser()->setAttribute('password', $password);
                                        $this->getUser()->setAttribute('folder', 'bappeko');

                                        budgetLogger::log('Bappeko dengan username ' . $nama . ' Login ke sistem e-budgeting');
                                    }

                                    budgetLogger::log('Viewer dengan username ' . $nama . ' Login ke sistem e-budgeting');

                                    if (is_null($cpass)) {
                                        return $this->redirect('peneliti/ubahPassLogin');
                                    }       
                                    else{
                                        return $this->redirect('view_rka/listRevisi?menu=Belanja');
                                    }

                                } else {
                                    $this->getUser()->setAuthenticated(true);
                                    $this->getUser()->addCredential('peneliti');

                                    $this->getResponse()->setCookie('nama', $peneliti);
                                    $this->getResponse()->setCookie('nama_user', $nama);

                                    $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
                                    $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
                                    $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
                                    $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                                    budgetLogger::log('Peneliti dengan username ' . $nama . ' Login ke sistem e-budgeting');
                                    if ($nama == 'bpk') {
                                        //if (sfConfig::get('app_default_coding') == 'testbed' . sfConfig::get('app_tahun_default') . 'sf') {
                                        if (is_null($cpass)) {
                                            return $this->redirect('bpk/ubahPassLogin');
                                        }
                                        else{
                                            return $this->redirect('bpk/eula');
                                        }
                                        //return $this->redirect('bpk/list');
                                        //}
                                        //return $this->redirect('peneliti/list');
                                    }
                                    if (is_null($cpass)) 
                                        return $this->redirect('peneliti/ubahPassLogin');
                                    else
                                        return $this->redirect('peneliti/listRevisi?menu=Belanja&unit_id=' . $peneliti);
                                }
                            } else {
                                budgetLogger::log('Peneliti dengan username ' . $nama . ' Login ke sistem e-budgeting');
                                $data = $nama . '|' . $password;
//                            $this->redirect('http://gmap.surabaya-eproc.or.id/budget2016sfs/index.php/loginn/'.$nama.'/ebudget.html');
                                $this->redirect('http://budgetting.surabaya2excellence.or.id/budget2016sf/index.php/loginn/' . $this->blowfishEncrypt($data, 'p@rl3m3nG!tuLhoh') . '/ebudget.html');
                                //return $this->redirect(sfConfig::get('app_path_parlemen') . 'index.php');
                            }
//						
                        } elseif ($level == 3) {
                            if ($nama == 'verifikator' || $nama == 'vssh' || $nama == 'vasb' || $nama == 'binprog_data') {
                                $this->getUser()->setAuthenticated(true);
                                $this->getUser()->addCredential('data');

                                $this->getResponse()->setCookie('nama', $nama);
                                $this->getResponse()->setCookie('nama_user', $nama);

                                $this->getUser()->setAttribute('user_id', $nama, 'data');
                                $this->getUser()->setAttribute('nama_login', $nama, 'data');
                                $this->getUser()->setAttribute('nama_user', $nama, 'data');
                                $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'data');

                                budgetLogger::log('Tim Data dengan username ' . $nama . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user                   
                            if (is_null($cpass)) {
                                return $this->redirect('shsd/ubahPassLogin');
                            }
                            else
                                return $this->redirect('shsd/sshlocked');
                            } else {
                                $this->getUser()->setAuthenticated(true);
                                $this->getUser()->addCredential('data');
                                //$this->getUser()->setAttribute('user_id', $nama, 'data');



                                $this->getUser()->setAttribute('nama_login', $nama, 'data');
                                $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'data');

                                //$this->getResponse()->setCookie('user_data', $nama);
                                $this->getUser()->setAttribute('send_user', $nama);
                                $this->getUser()->setAttribute('password', $password);
                                $this->getUser()->setAttribute('folder', 'data');
                                # print_r($this->getUser()->getAttribute('nama_login'));exit;
                                budgetLogger::log('Tim Data dengan username ' . $nama . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user           
                                //kalo-live
                                return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=data');
                                //kalo-live
//                                                $this->getUser()->setAuthenticated(true);
//						$this->getUser()->addCredential('data');
//						$this->getUser()->setAttribute('user_id', $nama, 'data');
//						
//						$this->getResponse()->setCookie('nama', $nama);
//						
//						$this->getUser()->setAttribute('nama_user',$nama,'data');
//
//						return $this->redirect('shsd?nama='.$nama);
                            }
                        }
//                        elseif ($level == 8) {
//                            $this->getUser()->setAuthenticated(true);
//                            $this->getUser()->addCredential('bappeko_gis');
//                            $this->getUser()->setAttribute('user_id', $nama, 'bappeko_gis');
//
//                            $this->getResponse()->setCookie('nama', $nama);
//                            $this->getResponse()->setCookie('nama_user', $nama);
//
//                            $this->getUser()->setAttribute('nama_user', $nama, 'bappeko_gis');
//                            $this->getUser()->setAttribute('nama_login', $nama, 'bappeko_gis');
//                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'bappeko_gis');
//
//                            budgetLogger::log('Bappeko GIS dengan username ' . $nama . ' Login ke sistem e-budgeting');
//                            
//                            return $this->redirect('shsd/sshlocked');
//                        } 
                        elseif ($level == 10) {
                            if ($nama == 'prk9999') {
                                $this->getUser()->setAuthenticated(true);
                                $this->getUser()->addCredential('bappekoprk');

                                $this->getResponse()->setCookie('nama', $nama);
                                $this->getResponse()->setCookie('nama_user', $nama);

                                $this->getUser()->setAttribute('user_id', $nama, 'bappekoprk');
                                $this->getUser()->setAttribute('nama_login', $nama, 'bappekoprk');
                                $this->getUser()->setAttribute('nama_user', $nama, 'bappekoprk');
                                $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'bappekoprk');

                                budgetLogger::log('Bappeko PRK dengan username ' . $nama . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user                       
                                // if (is_null($cpass)) {
                                //     return $this->redirect('entri/ubahPassLogin');
                                // }
                                // else
                                    return $this->redirect('bappekoprk/list');
                            } else {
                                //irul 27maret 2014 - login BAPPEKO-PRK
                                $this->getUser()->setAttribute('send_user', $nama);
                                $this->getUser()->setAttribute('password', $password);
                                $this->getUser()->setAttribute('folder', 'bappeko');

                                budgetLogger::log('Bappeko dengan username ' . $nama . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user                    

                                return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=bappeko');
                            }
                            //irul 27maret 2014 - login BAPPEKO-PRK
                        } elseif ($level == 11) {
                            //irul 27maret 2014 - login BAPPEKO-MITRA
                            if ($nama == 'mitra9999') {
                                $this->getUser()->setAuthenticated(true);
                                $this->getUser()->addCredential('bappekomitra');

                                $this->getResponse()->setCookie('nama', $nama);
                                $this->getResponse()->setCookie('nama_user', $nama);

                                $this->getUser()->setAttribute('user_id', $nama, 'bappekomitra');
                                $this->getUser()->setAttribute('nama_login', $nama, 'bappekomitra');
                                $this->getUser()->setAttribute('nama_user', $nama, 'bappekomitra');
                                $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'bappekomitra');

                                budgetLogger::log('Bappeko Mitra dengan username ' . $nama . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user                 
                                // if (is_null($cpass)) {
                                //     return $this->redirect('entri/ubahPassLogin');
                                // }
                                // else
                                    return $this->redirect('bappekomitra/list');

                            } else {
                                $this->getUser()->setAttribute('send_user', $nama);
                                $this->getUser()->setAttribute('password', $password);
                                $this->getUser()->setAttribute('folder', 'bappeko');

                                budgetLogger::log('Bappeko Mitra dengan username ' . $nama . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                                //historyUserLog::user_login();
#52 - save Login user           

                                return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=bappeko');
                            }
                            //irul 27maret 2014 - login BAPPEKO-MITRA
                        } elseif ($level == 9) {
                            if (($nama == 'admin') || ($nama == 'superadmin_ebudgeting')) {
                                $this->getUser()->setAuthenticated(true);
                                $this->getUser()->addCredential('admin_super');
                                $this->getUser()->setAttribute('user_id', $nama, 'admin_super');

                                $this->getResponse()->setCookie('user_admin', $nama);
                                $this->getUser()->setAttribute('nama_user', $nama, 'admin_super');

                                $this->getUser()->setAttribute('nama_login', $nama, 'admin_super');
                                $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin_super');
                            } 
                            // elseif ($nama == 'tim_shs'){
                            //     $this->getUser()->setAuthenticated(true);
                            //     $this->getUser()->addCredential('tim_shs');
                            //     $this->getUser()->setAttribute('user_id', $nama, 'tim_shs');

                            //     $this->getResponse()->setCookie('user_admin', $nama);
                            //     $this->getUser()->setAttribute('nama_user', $nama, 'tim_shs');

                            //     $this->getUser()->setAttribute('nama_login', $nama, 'tim_shs');
                            //     $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'tim_shs');
                            // }
                            else {
                                $this->getUser()->setAuthenticated(true);
                                $this->getUser()->addCredential('admin');
                                $this->getUser()->setAttribute('user_id', $nama, 'admin');

                                $this->getResponse()->setCookie('user_admin', $nama);
                                $this->getUser()->setAttribute('nama_user', $nama, 'admin');

                                $this->getUser()->setAttribute('nama_login', $nama, 'admin');
                                $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin');
                            }//session_start();
#52 - save Login user                            
                            //historyUserLog::user_login();
#52 - save Login user              

                            return $this->redirect('user_app/userlist');
                        } elseif ($level == 12) {

                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('viewer');
                            $this->getUser()->setAttribute('user_id', $nama, 'viewer');

                            $this->getResponse()->setCookie('nama', $nama);
                            $this->getResponse()->setCookie('nama_user', $nama);
                            $this->getResponse()->setCookie('nama_login', $nama);
                            $this->getUser()->setAttribute('nama_login', $nama, 'viewer');

                            $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                            $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                            budgetLogger::log('Viewer dengan username ' . $nama . ' Login ke sistem e-budgeting');
                            
                            if ($nama == 'anggaran' || $nama == 'bagian_hukum' || $nama == 'inspektorat' || $nama == 'bkd' || $nama == 'bkpsdm' || $nama == 'lppa' || $nama == 'bagian_organisasi' || $nama == 'bapenda'){
                                if (is_null($cpass))
                                    return $this->redirect('peneliti/ubahPassLogin');
                                else  
                                    return $this->redirect('anggaran/listRevisi');                                
                            }

                            if ($nama == 'bappeko' || $nama == 'evalitbang' || $nama == 'penanda_bappeko'){
                                if (is_null($cpass))
                                    return $this->redirect('peneliti/ubahPassLogin');
                                else  
                                    return $this->redirect('view_rka/listRevisi?menu=Belanja');
                            }

                            if ($nama == 'kemenpan_rb') {
                                return $this->redirect('view_rka/list');
                            }  


                            if(strpos($nama, 'survey') !== false) {
                                    $nama='survey';
                            } 
           
                            if ($nama == 'survey'){
                                if (is_null($cpass))
                                    return $this->redirect('peneliti/ubahPassLogin');
                                else  
                                    return $this->redirect('survey/sshlocked');
                            }    

                            if (is_null($cpass))
                                return $this->redirect('peneliti/ubahPassLogin');
                            else                       
                                return $this->redirect('view_rka/listRevisi?menu=Belanja');
                                                       
                        } elseif ($level == 13) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('pptk');
                            $username = $nama;

                            $e = new Criteria();
                            $e->add(UserHandleV2Peer::USER_ID, $nama);
                            $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                            $es = UserHandleV2Peer::doSelectOne($e);
                            if ($es) {
                                $dinas = $es->getUnitId();
                            }
                            $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                            $f = new Criteria();
                            $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                            $this->getResponse()->setCookie('nama_login', $nama);

                            if ($rs_unitkerja) {
                                $nama = $rs_unitkerja->getUnitName();
                            }

                            $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                            $this->getResponse()->setCookie('nama', $dinas);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                            $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                            $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                            $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                            budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                            //historyUserLog::user_login();
                             $string='kosong';
#52 - save Login user           
                            if ($nama_lengkap == 'gis_2600') {
                                return $this->redirect('waitinglist_pu/waitinglist');
                            } else if (is_null($cpass)) {
                                return $this->redirect('entri/ubahPassLogin');
                            } else if (strpos($username, $string) !== false) {
    // substring2 is found in string
                                $salah=2;  
                            } else {
                                return $this->redirect('entri/eula?unit_id=' . $dinas);
                            }
                        } elseif ($level == 14) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('kpa');
                            $username = $nama;

                            $e = new Criteria();
                            $e->add(UserHandleV2Peer::USER_ID, $nama);
                            $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                            $es = UserHandleV2Peer::doSelectOne($e);
                            if ($es) {
                                $dinas = $es->getUnitId();
                            }
                            $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                            $f = new Criteria();
                            $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                            $this->getResponse()->setCookie('nama_login', $nama);

                            if ($rs_unitkerja) {
                                $nama = $rs_unitkerja->getUnitName();
                            }

                            $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                            $this->getResponse()->setCookie('nama', $dinas);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                            $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                            $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                            $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                            budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                            //historyUserLog::user_login();          
                            $string='kosong';
#52 - save Login user           
                            if ($nama_lengkap == 'gis_2600') {
                                return $this->redirect('waitinglist_pu/waitinglist');
                            } else if (is_null($cpass)) {
                                return $this->redirect('entri/ubahPassLogin');
                            } else if (strpos($username, $string) !== false) {
    // substring2 is found in string
                                $salah=2;  
                            } else {
                                return $this->redirect('entri/eula?unit_id=' . $dinas);
                            }
                        } elseif ($level == 15) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('pa');
                            $username = $nama;

                            $e = new Criteria();
                            $e->add(UserHandleV2Peer::USER_ID, $nama);
                            $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                            $es = UserHandleV2Peer::doSelectOne($e);
                            if ($es) {
                                $dinas = $es->getUnitId();
                            }
                            $this->getUser()->setAttribute('user_login', $nama, 'dinas');
                            $f = new Criteria();
                            $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                            $this->getResponse()->setCookie('nama_login', $nama);

                            if ($rs_unitkerja) {
                                $nama = $rs_unitkerja->getUnitName();
                            }

                            $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                            $this->getResponse()->setCookie('nama', $dinas);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                            $this->getUser()->setAttribute('nama_login', $username, 'dinas');
                            $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');
                            $this->getUser()->setAttribute('email', $ds->getEmail(), 'otorisasi');

                            budgetLogger::log($nama . ' dengan username ' . $username . ' Login ke sistem e-budgeting');

#52 - save Login user                            
                            //historyUserLog::user_login();          
                            $string='kosong';
#52 - save Login user           
                            if ($nama_lengkap == 'gis_2600') {
                                return $this->redirect('waitinglist_pu/waitinglist');
                            } else if (is_null($cpass)) {
                                return $this->redirect('entri/ubahPassLogin');
                            } else if (strpos($username, $string) !== false) {
    // substring2 is found in string
                                $salah=2;  
                            } else {
                                return $this->redirect('entri/eula?unit_id=' . $dinas);
                            }
                        } elseif ($level == 16) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('bappeko');
                            $this->getUser()->setAttribute('user_id', $nama, 'viewer');

                            $this->getResponse()->setCookie('nama', $nama);
                            $this->getResponse()->setCookie('nama_user', $nama);
                            $this->getResponse()->setCookie('nama_login', $nama);
                            $this->getUser()->setAttribute('nama_login', $nama, 'viewer');

                            $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                            $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                            budgetLogger::log('Bappeko dengan username ' . $nama . ' Login ke sistem e-budgeting');
                            return $this->redirect('anggaran/listRevisi?menu=Belanja');
                        } else {
                            
                           $salah=1;
                        }
                    }
                } else {
                      // user tidak ditemukan di ebudgeting
                    $salah=1;    
                   

                }
            } else {
               
                    
                        $salah = 1;
            }

            if ($salah == 1) {
                // $this->setFlash('gagal', 'e-Budgeting 2022 Sedang Dalam Maintenance. Mohon bersabar');
               $this->setFlash('gagal', 'Username/Password yang Anda masukan salah.');
                return $this->redirect('@homepage');
            }
            else if ($salah == 2 ) {
                 $this->setFlash('gagal', 'e-Budgeting 2020 Sedang Dalam Maintenance.');
                //$this->setFlash('gagal', 'Username/Password yang Anda masukan salah.');
                return $this->redirect('@homepage');
            }
            else if ($salah == 3 ) {
                 $this->setFlash('gagal', 'User Tidak Ada di Aplikasi e-Budgeting.');
                //$this->setFlash('gagal', 'Username/Password yang Anda masukan salah.');
                return $this->redirect('@homepage');
            }

            
        } catch (Exception $exc) {
            echo $exc->getMessage();
            //  $this->setFlash('gagal', 'e-Budgeting Sedang Dalam Maintenance.');
            // //     //$this->setFlash('gagal', 'Username/Password yang Anda masukan salah.');
            //  return $this->redirect('@homepage');
            exit();
            
        }
    }

    public function executeLoginmonitoring() {
        $this->getUser()->clearCredentials('dinas');
        $this->getUser()->clearCredentials('pptk');
        $this->getUser()->clearCredentials('kpa');
        $this->getUser()->clearCredentials('pa');
        $this->getUser()->clearCredentials('anggaran');
        $salah = 0;

        if ($this->getRequestParameter('kunci') == md5('monitor')) {
            $nama = $this->getRequestParameter('send_user');
            //print_r($nama);exit;
            //$password = md5($this->getRequestParameter('password')) ;
            if ($this->getRequestParameter('passwordmd5')) {
                $password = $this->getRequestParameter('passwordmd5');
            }
            //print_r($nama.' '.$password);exit;
            $d = new Criteria();
            $d->add(MasterUserV2Peer::USER_ID, $nama);
            $d->add(MasterUserV2Peer::USER_PASSWORD, $password);
            $d->add(MasterUserV2Peer::USER_ENABLE, TRUE);
            $ds = MasterUserV2Peer::doSelectOne($d);
//            $e = new Criteria();
//            $e->add(MasterUserV2Peer::NIP, preg_replace("/[^[:alnum:]]/u", '|', $nama));
//            $e->add(MasterUserV2Peer::USER_PASSWORD, $password);
//            $e->add(MasterUserV2Peer::USER_ENABLE, TRUE);
//            $ds2 = MasterUserV2Peer::doSelectOne($e);
//            if ($ds || $ds2) {
//                if ($ds2) {
//                    $nama = $ds2->getUserId();
//                    $ds = $ds2;
//                }
            if ($ds) {
                //print_r($ds->getUserName());exit;
                $nama_lengkap = $ds->getUserName();
                $c = new Criteria();
                $c->add(SchemaAksesV2Peer::USER_ID, $nama);
                $c->add(SchemaAksesV2Peer::SCHEMA_ID, 2);
                //$c->addDescendingOrderByColumn(SchemaAksesV2Peer::SCHEMA_ID);
                /* $c -> add(SchemaAksesV2Peer::LEVEL_ID, 7); */
                $cs = SchemaAksesV2Peer::doSelectOne($c);
                if ($cs) {
                    $level = $cs->getLevelId();

                    //$this->getUser()->logOut();
                    if ($level == 7) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('dewan');
                        $this->getUser()->setAttribute('user_id', $nama, 'dewan');

                        $this->getResponse()->setCookie('nama', $nama);

                        $this->getUser()->setAttribute('nama_user', $nama, 'dewan');

                        return $this->redirect('dewan?nama=' . $nama);
                    } else if ($level == 1) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('dinas');
                        $this->getUser()->setAttribute('user_nama', $nama, 'dinas');

                        $e = new Criteria();
                        $e->add(UserHandleV2Peer::USER_ID, $nama);
                        $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                        $es = UserHandleV2Peer::doSelectOne($e);
                        if ($es) {
                            $dinas = $es->getUnitId();
                        }

                        $f = new Criteria();
                        $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                        $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                        $this->getResponse()->setCookie('nama_login', $nama);
                        $this->getUser()->setAttribute('nama_login', $nama, 'dinas');
                        if ($rs_unitkerja) {
                            $nama = $rs_unitkerja->getUnitName();
                        }

                        $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                        $this->getResponse()->setCookie('nama', $dinas);
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');

                        if ($nama_lengkap == 'gis_2600') {
                            return $this->redirect('waitinglist_pu/waitinglist');
                        } else {
                            return $this->redirect('entri/list?menu=Belanja&unit_id=' . $dinas);
                        }
                    } else if ($level == 2) {
//                        clear credential admin
                        $this->getUser()->clearCredentials('admin');
                        $this->getUser()->logOut();
//                        clear credential admin
                        $e = new Criteria();
                        $e->add(UserHandleV2Peer::USER_ID, $nama);
                        $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                        $es = UserHandleV2Peer::doSelectOne($e);
                        {
                            $peneliti = $es->getUnitId();
                        }

                        $array_nama_user = array('walikota', 'asisten_walikota', 'delayu', 'putri', 'ika', 'binaprogram', 'asisten1', 'asisten2', 'asisten3', 'bpk', 'diana', 'eka', 'parlemen2', 'bppk', 'dppk', 'reza', 'masger', 'inspect', 'murni', 'sudadi', 'peninjau', 'bappeko', 'prk', 'bawas', 'inspektorat1', 'inspektorat2', 'inspektorat3', 'evalitbang', 'penanda_bappeko');
                        if (in_array($nama, $array_nama_user)) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('viewer');
                            $this->getUser()->setAttribute('user_id', $nama, 'viewer');

                            $this->getResponse()->setCookie('nama', $nama);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                            $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                            if ($nama == 'prk') {
                                $this->getUser()->setAttribute('send_user', $nama);
                                $this->getUser()->setAttribute('password', $password);
                                $this->getUser()->setAttribute('folder', 'bappeko');

                                budgetLogger::log('Bappeko dengan username ' . $nama . ' Login ke sistem e-budgeting');
                            }

                            budgetLogger::log('Viewer dengan username ' . $nama . ' Login ke sistem e-budgeting');

                            return $this->redirect('view_rka/listRevisi?menu=Belanja');
                        } else {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('peneliti');

                            $this->getResponse()->setCookie('nama', $peneliti);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('user_id', $nama, 'peneliti');
                            $this->getUser()->setAttribute('nama_login', $nama, 'peneliti');
                            $this->getUser()->setAttribute('nama_user', $nama, 'peneliti');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'peneliti');

                            budgetLogger::log('Peneliti dengan username ' . $nama . ' Login ke sistem e-budgeting');
                            if ($nama == 'bpk') {
                                //if (sfConfig::get('app_default_coding') == 'testbed' . sfConfig::get('app_tahun_default') . 'sf') {
                                return $this->redirect('bpk/list');
                                //}
                                //return $this->redirect('peneliti/list');
                            }
                            return $this->redirect('peneliti/listRevisi?menu=Belanja&unit_id=' . $peneliti);
                        }
                    } elseif ($level == 3) {
                        if ($nama == 'verifikator' || $nama == 'vasb' || $nama == 'vestimasi') {
                            //                        clear credential admin
                            $this->getUser()->clearCredentials('admin');
                            $this->getUser()->logOut();
                            //                        clear credential admin


                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('data');

                            $this->getResponse()->setCookie('nama', $nama);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('user_id', $nama, 'data');
                            $this->getUser()->setAttribute('nama_login', $nama, 'data');
                            $this->getUser()->setAttribute('nama_user', $nama, 'data');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'data');

                            budgetLogger::log('Tim Data dengan username ' . $nama . ' Login ke sistem e-budgeting');

                            return $this->redirect('shsd/sshlocked');
                        } else {
                            //$this->getUser()->setAuthenticated(true);
                            //$this->getUser()->addCredential('data');
                            //$this->getUser()->setAttribute('user_id', $nama, 'data');
                            //$this->getResponse()->setCookie('user_data', $nama);
                            $this->getUser()->setAttribute('send_user', $nama);
                            $this->getUser()->setAttribute('password', $password);
                            $this->getUser()->setAttribute('folder', 'data');
                            return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=data');
                        }
                    } elseif ($level == 8) {
                        //$this->getUser()->setAuthenticated(true);
                        //$this->getUser()->addCredential('bappeko');
                        //$this->getUser()->setAttribute('user_id', $nama, 'data');
                        //$this->getResponse()->setCookie('user_data', $nama);
                        $this->getUser()->setAttribute('send_user', $nama);
                        $this->getUser()->setAttribute('password', $password);
                        $this->getUser()->setAttribute('folder', 'bappeko');
                        return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=bappeko');
                    } elseif ($level == 10) {
//                        clear credential admin
                        $this->getUser()->clearCredentials('admin');
                        $this->getUser()->logOut();
//                        clear credential admin
                        if ($nama == 'prk9999') {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('bappekoprk');

                            $this->getResponse()->setCookie('nama', $nama);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('user_id', $nama, 'bappekoprk');
                            $this->getUser()->setAttribute('nama_login', $nama, 'bappekoprk');
                            $this->getUser()->setAttribute('nama_user', $nama, 'bappekoprk');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'bappekoprk');

                            budgetLogger::log('Bappeko PRK dengan username ' . $nama . ' Login ke sistem e-budgeting');

                            return $this->redirect('bappekoprk/list');
                        } else {
                            //irul 27maret 2014 - login BAPPEKO-PRK
                            $this->getUser()->setAttribute('send_user', $nama);
                            $this->getUser()->setAttribute('password', $password);
                            $this->getUser()->setAttribute('folder', 'bappeko');

                            budgetLogger::log('Bappeko dengan username ' . $nama . ' Login ke sistem e-budgeting');

                            return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=bappeko');
                        }
                        //irul 27maret 2014 - login BAPPEKO-PRK
                    } elseif ($level == 11) {
                        //irul 27maret 2014 - login BAPPEKO-MITRA
//                        clear credential admin
                        $this->getUser()->clearCredentials('admin');
                        $this->getUser()->logOut();
//                        clear credential admin
                        if ($nama == 'mitra9999') {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('bappekomitra');

                            $this->getResponse()->setCookie('nama', $nama);
                            $this->getResponse()->setCookie('nama_user', $nama);

                            $this->getUser()->setAttribute('user_id', $nama, 'bappekomitra');
                            $this->getUser()->setAttribute('nama_login', $nama, 'bappekomitra');
                            $this->getUser()->setAttribute('nama_user', $nama, 'bappekomitra');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'bappekomitra');

                            budgetLogger::log('Bappeko Mitra dengan username ' . $nama . ' Login ke sistem e-budgeting');

                            return $this->redirect('bappekomitra/list');
                        } else {
                            $this->getUser()->setAttribute('send_user', $nama);
                            $this->getUser()->setAttribute('password', $password);
                            $this->getUser()->setAttribute('folder', 'bappeko');

                            budgetLogger::log('Bappeko Mitra dengan username ' . $nama . ' Login ke sistem e-budgeting');

                            return $this->redirect(sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $nama . '&password=' . $password . '&folder=bappeko');
                        }
                        //irul 27maret 2014 - login BAPPEKO-MITRA
                    } elseif ($level == 9) {
                        if (($nama == 'admin' || $nama == 'superadmin_ebudgeting')) {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('admin_super');
                            $this->getUser()->setAttribute('user_id', $nama, 'admin_super');

                            $this->getResponse()->setCookie('user_admin', $nama);
                            $this->getUser()->setAttribute('nama_user', $nama, 'admin_super');

                            $this->getUser()->setAttribute('nama_login', $nama, 'admin_super');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin_super');
                        } else {
                            $this->getUser()->setAuthenticated(true);
                            $this->getUser()->addCredential('admin');
                            $this->getUser()->setAttribute('user_id', $nama, 'admin');

                            $this->getResponse()->setCookie('user_admin', $nama);
                            $this->getUser()->setAttribute('nama_user', $nama, 'admin');

                            $this->getUser()->setAttribute('nama_login', $nama, 'admin');
                            $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'admin');
                        }//session_start();

                        return $this->redirect('user_app/userlist');
                    } elseif ($level == 12) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('viewer');
                        $this->getUser()->setAttribute('user_nama', $nama, 'viewer');
                        $this->getResponse()->setCookie('nama_login', $nama);
                        $this->getUser()->setAttribute('nama_login', $nama, 'viewer');

                        $this->getUser()->setAttribute('user_id', $nama, 'viewer');

                        $this->getResponse()->setCookie('nama', $dinas);
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getUser()->setAttribute('unit_id', $dinas, 'viewer');
                        $this->getUser()->setAttribute('nama', $dinas, 'viewer');
                        $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                        if(strpos($nama, 'survey') !== false) {
                                    $nama='survey';
                        } 
                        if ($nama == 'bagian_hukum' || $nama == 'inspektorat' || $nama == 'bkd' || $nama == 'bkpsdm' || $nama == 'lppa' || $nama == 'bagian_organisasi' || $nama == 'asisten1' || $nama == 'asisten2' || $nama == 'asisten3' || $nama == 'sekda' || $nama == 'bapenda')
                            //return $this->redirect('anggaran/listRevisi');
                            // return $this->redirect('shsd/sshlocked');
                            return $this->redirect('view_rka/listRevisi?menu=Belanja');
                        if ($nama == 'bappeko' || $nama == 'anggaran')
                            return $this->redirect('anggaran/listRevisi?menu=Belanja');
                        if ($nama == 'bendahara_0305')
                            return $this->redirect('anggaran/listRevisi?menu=Belanja');
                        if ($nama == 'viewer_2600')
                            return $this->redirect('anggaran/listRevisi?menu=Belanja');
                        if ($nama == 'viewer_data')
                            return $this->redirect('shsd/sshlocked');
                        if ($nama == 'survey')
                            return $this->redirect('survey/sshlocked');
                        return $this->redirect('view_rka/listRevisi?menu=Belanja');
                    } elseif ($level == 13) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('pptk');
                        $this->getUser()->setAttribute('user_nama', $nama, 'dinas');

                        $e = new Criteria();
                        $e->add(UserHandleV2Peer::USER_ID, $nama);
                        $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                        $es = UserHandleV2Peer::doSelectOne($e);
                        if ($es) {
                            $dinas = $es->getUnitId();
                        }

                        $f = new Criteria();
                        $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                        $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                        $this->getResponse()->setCookie('nama_login', $nama);
                        $this->getUser()->setAttribute('nama_login', $nama, 'dinas');
                        if ($rs_unitkerja) {
                            $nama = $rs_unitkerja->getUnitName();
                        }

                        $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                        $this->getResponse()->setCookie('nama', $dinas);
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');

                        if ($nama_lengkap == 'gis_2600') {
                            return $this->redirect('waitinglist_pu/waitinglist');
                        } else {
                            return $this->redirect('entri/list?menu=Belanja&unit_id=' . $dinas);
                        }
                    } elseif ($level == 14) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('kpa');
                        $this->getUser()->setAttribute('user_nama', $nama, 'dinas');

                        $e = new Criteria();
                        $e->add(UserHandleV2Peer::USER_ID, $nama);
                        $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                        $es = UserHandleV2Peer::doSelectOne($e);
                        if ($es) {
                            $dinas = $es->getUnitId();
                        }

                        $f = new Criteria();
                        $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                        $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                        $this->getResponse()->setCookie('nama_login', $nama);
                        $this->getUser()->setAttribute('nama_login', $nama, 'dinas');
                        if ($rs_unitkerja) {
                            $nama = $rs_unitkerja->getUnitName();
                        }

                        $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                        $this->getResponse()->setCookie('nama', $dinas);
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');

                        if ($nama_lengkap == 'gis_2600') {
                            return $this->redirect('waitinglist_pu/waitinglist');
                        } else {
                            return $this->redirect('entri/list?menu=Belanja&unit_id=' . $dinas);
                        }
                    } elseif ($level == 15) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('pa');
                        $this->getUser()->setAttribute('user_nama', $nama, 'dinas');

                        $e = new Criteria();
                        $e->add(UserHandleV2Peer::USER_ID, $nama);
                        $e->add(UserHandleV2Peer::SCHEMA_ID, 2);
                        $es = UserHandleV2Peer::doSelectOne($e);
                        if ($es) {
                            $dinas = $es->getUnitId();
                        }

                        $f = new Criteria();
                        $f->add(UnitKerjaPeer::UNIT_ID, $dinas);
                        $rs_unitkerja = UnitKerjaPeer::doSelectOne($f);
                        $this->getResponse()->setCookie('nama_login', $nama);
                        $this->getUser()->setAttribute('nama_login', $nama, 'dinas');
                        if ($rs_unitkerja) {
                            $nama = $rs_unitkerja->getUnitName();
                        }

                        $this->getUser()->setAttribute('user_id', $nama, 'dinas');

                        $this->getResponse()->setCookie('nama', $dinas);
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getUser()->setAttribute('unit_id', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama', $dinas, 'dinas');
                        $this->getUser()->setAttribute('nama_user', $nama, 'dinas');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'dinas');

                        if ($nama_lengkap == 'gis_2600') {
                            return $this->redirect('waitinglist_pu/waitinglist');
                        } else {
                            return $this->redirect('entri/list?menu=Belanja&unit_id=' . $dinas);
                        }
                    } elseif ($level == 16) {
                        $this->getUser()->setAuthenticated(true);
                        $this->getUser()->addCredential('bappeko');
                        $this->getUser()->setAttribute('user_id', $nama, 'viewer');

                        $this->getResponse()->setCookie('nama', $nama);
                        $this->getResponse()->setCookie('nama_user', $nama);
                        $this->getResponse()->setCookie('nama_login', $nama);
                        $this->getUser()->setAttribute('nama_login', $nama, 'viewer');

                        $this->getUser()->setAttribute('nama_user', $nama, 'viewer');
                        $this->getUser()->setAttribute('nama_login', $nama, 'viewer');
                        $this->getUser()->setAttribute('nama_lengkap', $nama_lengkap, 'viewer');

                        return $this->redirect('anggaran/listRevisi?menu=Belanja');
                    } else {
                        $salah = 1;
                    }
                }
            } else {
                $d = new Criteria();
                $d->add(MasterUserV2Peer::USER_ID, $nama);
                $d->add(MasterUserV2Peer::USER_PASSWORD, $password);
                $d->add(MasterUserV2Peer::USER_ENABLE, FALSE);
                $ds = MasterUserV2Peer::doSelectOne($d);
                if ($ds) {
                    $salah = 2;
                    //print_r($salah);exit;
                } else {
                    $salah = 1;
                }
            }
        } else {
            $salah = 1;
        }

        if ($salah == 1) {
            return $this->redirect('@homepage');
        } elseif ($salah == 2) {
            return $this->redirect('@homepage');
        }
    }

    public function handleErrorDewanlogin() {
        //$this->forward('login','dewanlogin');	
    }

    public function handleErrorViewerlogin() {
        //$this->forward('login','dewanlogin');	
    }

    public function executeError404() {
        $this->setLayout('layouteula');
    }

    public function executeLogout() {
        $this->getUser()->logOut();
        $this->redirect('@homepage');
    }

    public function executeLogoutDewan() {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->clearCredentials('dewan');
        $this->getUser()->getAttributeHolder()->removeNamespace('dewan');
        return $this->redirect('@homepage');
    }

    public function executeLogoutViewer() {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->clearCredentials('viewer');
        $this->getUser()->getAttributeHolder()->removeNamespace('viewer');
        return $this->redirect('@homepage');
    }

    public function executeLogoutData() {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->clearCredentials('data');
        $this->getUser()->getAttributeHolder()->removeNamespace('data');
        return $this->redirect('@homepage');
    }

    public function executeIndex() {
        //$this->forward('default', 'module');
        if ($this->getUser()->isAuthenticated()) {
            $this->getResponse()->setCookie('nama_user', '');
            $this->getResponse()->setCookie('nama', '');
            $this->getResponse()->setCookie('user_id', '');
            $this->getUser()->logOut();
        }

        // $this->setLayout('login');
        $this->setLayout('loginevin');
        // $this->setLayout('logincoba2018');
        // $this->setLayout('logincoba20182');
        // $this->setLayout('logingabung');
    }

    public function executeAdminLogin() {
        return sfView::SUCCESS;
    }

    public function executeCekadmin() {
        return sfView::SUCCESS;
    }

    public function executeLogoutAdmin() {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->clearCredentials('admin');
        $this->getUser()->logOut();
        $this->getUser()->getAttributeHolder()->removeNamespace('admin');
        $this->getUser()->getAttributeHolder()->removeNamespace('admin_super');
        $this->getUser()->getAttributeHolder()->removeNamespace('lokasi');
        $this->getUser()->getAttributeHolder()->removeNamespace('lokasi_baru');
        $this->getUser()->getAttributeHolder()->removeNamespace('simbada');
        $this->getUser()->getAttributeHolder()->removeNamespace('simbada_baru');
        $this->getUser()->getAttributeHolder()->removeNamespace('otorisasi');
        sfContext::getInstance()->getResponse()->setCookie('user_admin', '', time() - 3600, '/');

        return $this->redirect('@homepage');
    }

    public function executeDinasLogin() {
        return sfView::SUCCESS;
    }

    public function executeLogoutDinas() {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->clearCredentials('dinas');
        $this->getUser()->clearCredentials('pptk');
        $this->getUser()->clearCredentials('kpa');
        $this->getUser()->clearCredentials('pa');
        $this->getUser()->clearCredentials('anggaran');
        $this->getUser()->logOut();
        $this->getUser()->getAttributeHolder()->removeNamespace('status_dinas');
        $this->getUser()->getAttributeHolder()->removeNamespace('dinas');
        $this->getUser()->getAttributeHolder()->removeNamespace('lokasi');
        $this->getUser()->getAttributeHolder()->removeNamespace('lokasi_baru');
        $this->getUser()->getAttributeHolder()->removeNamespace('simbada');
        $this->getUser()->getAttributeHolder()->removeNamespace('simbada_baru');
        $this->getUser()->getAttributeHolder()->removeNamespace('otorisasi');
        sfContext::getInstance()->getResponse()->setCookie('nama', '', time() - 3600, '/');
        sfContext::getInstance()->getResponse()->setCookie('nama_user', '', time() - 3600, '/');
        return $this->redirect('@homepage');
    }

    public function executePenelitiLogin() {
        return sfView::SUCCESS;
    }

    public function executeLogoutPeneliti() {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->clearCredentials('peneliti');
        $this->getUser()->logOut();
        $this->getUser()->getAttributeHolder()->removeNamespace('peneliti');
        $this->getUser()->getAttributeHolder()->removeNamespace('otorisasi');
        $this->getUser()->getAttributeHolder()->removeNamespace('lokasi');
        $this->getUser()->getAttributeHolder()->removeNamespace('lokasi_baru');
        $this->getUser()->getAttributeHolder()->removeNamespace('simbada');
        $this->getUser()->getAttributeHolder()->removeNamespace('simbada_baru');
        $this->getUser()->getAttributeHolder()->removeNamespace('otorisasi');
        sfContext::getInstance()->getResponse()->setCookie('nama', '', time() - 3600, '/');
        sfContext::getInstance()->getResponse()->setCookie('nama_user', '', time() - 3600, '/');
        return $this->redirect('@homepage');
    }

    public function executeKompJs() {
        $userpassword = $this->getRequestParameter('up');
        $username = $this->getRequestParameter('un');
        $unit_id = $this->getRequestParameter('code');

        if ($unit_id == '2000' && $username == 'pendidikan' && $userpassword == md5('pendidikan')) {
            $komponen = array();

            $c_cari_komponen_ssh = new Criteria();
            $c_cari_komponen_ssh->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD');
            $c_cari_komponen_ssh->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID);
            $c_cari_komponen_ssh->addJoin(ShsdPeer::SHSD_ID, KomponenPeer::KOMPONEN_ID);
            $c_cari_komponen_ssh->setDistinct();
            $dapat_komponen_ssh = KomponenPeer::doSelect($c_cari_komponen_ssh);
            if ($dapat_komponen_ssh) {
                foreach ($dapat_komponen_ssh as $value) {
                    $array_per_komponen = array();
                    $array_per_komponen['komponen_id'] = $value->getKomponenId();
                    $array_per_komponen['komponen_name'] = $value->getKomponenName();
                    $array_per_komponen['satuan'] = $value->getSatuan();
                    $array_per_komponen['shsd_id'] = $value->getShsdId();
                    $array_per_komponen['komponen_harga'] = $value->getKomponenHarga();
//                    $array_per_komponen['komponen_show'] = $value->getKomponenShow();
                    $array_per_komponen['komponen_name'] = $value->getKomponenName();
                    $array_per_komponen['komponen_tipe'] = $value->getKomponenTipe();
//                    $array_per_komponen['komponen_confirmed'] = $value->getKomponenConfirmed();
                    $array_per_komponen['komponen_non_pajak'] = $value->getKomponenNonPajak();

                    $array_rekening = array();
                    $explode_rekening = explode('/', $value->getRekening);
                    foreach ($explode_rekening as $value_rekening) {
                        $c_rekening = new Criteria();
                        $c_rekening->add(RekeningPeer::REKENING_CODE, $value_rekening);
                        $dapat_rekening = RekeningPeer::doSelectOne($c_rekening);
                        if ($dapat_rekening) {
                            $array_rekening[] = $dapat_rekening->getRekeningCode() . '-' . $dapat_rekening->getRekeningName();
                        }
                    }
                    $array_per_komponen['rekening'] = $array_rekening;


//                    ip_address character varying(20),
//                    waktu_access timestamp without time zone,
//                    user_id character varying(30),
//                    rekening character varying(300),
//                    kelompok character varying(100),
//                    pemeliharaan integer DEFAULT 0,
//                    rek_upah character varying(20),
//                    rek_bahan character varying(20),
//                    rek_sewa character varying(20),
//                    deskripsi character varying,
//                    status_masuk character varying(10),
//                    rka_member boolean DEFAULT false,
//                    maintenance boolean DEFAULT false,
//                    status_edit character varying(100),
//                    is_potong_bpjs boolean DEFAULT false,
//                    is_iuran_bpjs boolean DEFAULT false,
//                    usulan_skpd character varying(100),
//                    is_survey_bp boolean DEFAULT false,
//                    tahap text,
                    $komponen[] = $array_per_komponen;
                }
            }
            $json = json_encode($komponen);
            print_r($json);
        } else {
            echo 'ERROR';
        }
        exit();
    }

    public function executeRkaJs() {
        $userpassword = $this->getRequestParameter('up');
        $username = $this->getRequestParameter('un');
        $unit_id = $this->getRequestParameter('code');

        $c_cari_unit_id = new Criteria();
        $c_cari_unit_id->add(UnitKerjaPeer::UNIT_ID, $unit_id);
        $dapat_unit_id = UnitKerjaPeer::doSelectOne($c_cari_unit_id);
        if (!$dapat_unit_id) {
            echo 'ERROR - gak nemu SKPD';
            exit();
        }

        if ($username == 'simbada' && $userpassword == md5('simbada')) {
            $rka = array();
            $c_cari_rincian_detail = new Criteria();
            $c_cari_rincian_detail->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
            $c_cari_rincian_detail->addAnd(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_cari_rincian_detail->addAnd(RincianDetailPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
            $c_cari_rincian_detail->addAnd(RincianDetailPeer::NILAI_ANGGARAN, 0, Criteria::GREATER_THAN);
            $c_cari_rincian_detail->addAscendingOrderByColumn(RincianDetailPeer::KEGIATAN_CODE);
            $c_cari_rincian_detail->addAscendingOrderByColumn(RincianDetailPeer::SUBTITLE);
            $c_cari_rincian_detail->addAscendingOrderByColumn(RincianDetailPeer::SUB);
            $c_cari_rincian_detail->addAscendingOrderByColumn(RincianDetailPeer::REKENING_CODE);
            $c_cari_rincian_detail->addAscendingOrderByColumn(RincianDetailPeer::KOMPONEN_NAME);
            $dapat_rincian = RincianDetailPeer::doSelect($c_cari_rincian_detail);
            if ($dapat_rincian) {
                foreach ($dapat_rincian as $value) {
                    $array_per_rincian = array();
                    $array_per_rincian['unit_id'] = $value->getUnitId();
                    $array_per_rincian['kegiatan_code'] = $value->getKegiatanCode();
                    $array_per_rincian['detail_no'] = $value->getDetailNo();

                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $value->getUnitId());
                    $c_kegiatan->addAnd(MasterKegiatanPeer::KODE_KEGIATAN, $value->getKegiatanCode());
                    $dapat_kegiatan = MasterKegiatanPeer::doSelectOne($c_kegiatan);

                    $array_per_rincian['nama_kegiatan'] = $dapat_kegiatan->getNamaKegiatan();

                    $array_per_rincian['subtitle'] = $value->getSubtitle();
                    $array_per_rincian['subsubtitle'] = $value->getSub();

                    $array_per_rincian['komponen_kode_rekening'] = $value->getRekeningCode();

                    $c_rekening = new Criteria();
                    $c_rekening->add(RekeningPeer::REKENING_CODE, $value->getRekeningCode());
                    $dapat_rekening = RekeningPeer::doSelectOne($c_rekening);

                    $array_per_rincian['komponen_nama_rekening'] = $dapat_rekening->getRekeningName();

                    $array_per_rincian['komponen_id'] = $value->getKomponenId();
                    $array_per_rincian['komponen_name'] = $value->getKomponenName() . ' ' . $value->getDetailName();
                    $array_per_rincian['komponen_harga'] = $value->getKomponenHargaAwal();
                    $array_per_rincian['komponen_pajak'] = $value->getPajak();
                    $array_per_rincian['komponen_satuan'] = $value->getSatuan();
                    $array_per_rincian['komponen_koefisien'] = $value->getKeteranganKoefisien();
                    $array_per_rincian['komponen_volume'] = $value->getVolume();
                    $array_per_rincian['komponen_anggaran'] = $value->getNilaiAnggaran();

                    $rka[] = $array_per_rincian;
                }
            }
            $json = json_encode($rka);
            print_r($json);
        } else {
            echo 'ERROR - RKA';
        }
        exit();
    }

    public function executeRekeningJs() {
        $userpassword = $this->getRequestParameter('up');
        $username = $this->getRequestParameter('un');
        $tipe_param = $this->getRequestParameter('tipe_param');
        $param = $this->getRequestParameter('param');

        if (!isset($tipe_param) || !isset($param)) {
            echo 'ERROR - PARAMETER';
        }

        if ($username == 'pendidikan' && $userpassword == md5('pendidikan')) {
            $rekening = array();

            $c_rekening = new Criteria();
            if ($tipe_param == 'nama') {
                $c_rekening->add(RekeningPeer::REKENING_NAME, '%' . $param . '%', Criteria::ILIKE);
            } else if ($tipe_param == 'kode') {
                $c_rekening->add(RekeningPeer::REKENING_CODE, '%' . $param . '%', Criteria::ILIKE);
            }
            $dapat_rekening = RekeningPeer::doSelect($c_rekening);

            if ($dapat_rekening) {
                foreach ($dapat_rekening as $value) {
                    $array_per_rekening = array();
                    $array_per_rekening['rekening_code'] = $value->getRekeningCode();
                    $array_per_rekening['rekening_nama'] = $value->getRekeningName();
                    $rekening[] = $array_per_rekening;
                }
            } else {
                echo 'ERROR - PARAMETER';
            }
            $json = json_encode($rekening);
            print_r($json);
        } else {
            echo 'ERROR - RKA';
        }
        exit();
    }

    public function executeAhpJs() {
        $userpassword = $this->getRequestParameter('up');
        $username = $this->getRequestParameter('un');

        if ($username == 'bappeko' && $userpassword == md5('bappeko')) {
            $data_kirim_ahp = array();

            $c_kegiatan = new Criteria();
            $c_kegiatan->add(MasterKegiatanPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
            $c_kegiatan->addAscendingOrderByColumn(MasterKegiatanPeer::UNIT_ID);
            $c_kegiatan->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
            $dapat_kegiatan = MasterKegiatanPeer::doSelect($c_kegiatan);

            if ($dapat_kegiatan) {
                foreach ($dapat_kegiatan as $value) {

                    $array_per_kegiatan = array();
                    $array_per_kegiatan['unit_id'] = $value->getUnitId();
                    $c_unit = new Criteria();
                    $c_unit->add(UnitKerjaPeer::UNIT_ID, $value->getUnitId());
                    $dapat_unit = UnitKerjaPeer::doSelectOne($c_unit);
                    if ($dapat_unit) {
                        $array_per_kegiatan['nama_skpd'] = $dapat_unit->getUnitName();
                    }

                    $array_per_kegiatan['id_kegiatan'] = $value->getId();
                    $array_per_kegiatan['kode_kegiatan'] = $value->getKodeKegiatan();
                    $array_per_kegiatan['nama_kegiatan'] = $value->getNamaKegiatan();
                    $array_per_kegiatan['output_kegiatan'] = $value->getOutput();
                    if ($value->getKodeMisi() <> '') {
                        $array_per_kegiatan['kode_misi'] = $value->getKodeMisi();
                        $c_misi = new Criteria();
                        $c_misi->add(MasterMisiPeer::KODE_MISI, $value->getKodeMisi());
                        $dapat_misi = MasterMisiPeer::doSelectOne($c_misi);
                        if ($dapat_misi) {
                            $array_per_kegiatan['nama_misi'] = $dapat_misi->getNamaMisi();
                        }
                    } else {
                        $array_per_kegiatan['kode_misi'] = '';
                        $array_per_kegiatan['nama_misi'] = '';
                    }

                    if ($value->getKodeTujuan() <> '') {
                        $array_per_kegiatan['kode_tujuan'] = $value->getKodeTujuan();
                        $c_tujuan = new Criteria();
                        $c_tujuan->add(MasterTujuanPeer::KODE_TUJUAN, $value->getKodeTujuan());
                        $dapat_tujuan = MasterTujuanPeer::doSelectOne($c_tujuan);
                        if ($dapat_tujuan) {
                            $array_per_kegiatan['nama_tujuan'] = $dapat_tujuan->getNamaTujuan();
                        }
                    } else {
                        $array_per_kegiatan['kode_tujuan'] = '';
                        $array_per_kegiatan['nama_tujuan'] = '';
                    }

                    if ($value->getKodeSasaran() <> '') {
                        $array_per_kegiatan['kode_sasaran'] = $value->getKodeSasaran();
                        $c_sasaran = new Criteria();
                        $c_sasaran->add(MasterSasaranPeer::KODE_SASARAN, $value->getKodeSasaran());
                        $dapat_sasaran = MasterSasaranPeer::doSelectOne($c_sasaran);
                        if ($dapat_sasaran) {
                            $array_per_kegiatan['nama_sasaran'] = $dapat_sasaran->getNamaSasaran();
                        }
                    } else {
                        $array_per_kegiatan['kode_sasaran'] = '';
                        $array_per_kegiatan['nama_sasaran'] = '';
                    }


                    $array_per_kegiatan['kode_program2'] = $value->getKodeProgram2();
                    $c_program2 = new Criteria();
                    $c_program2->add(MasterProgram2Peer::KODE_PROGRAM2, $value->getKodeProgram2());
                    $dapat_program2 = MasterProgram2Peer::doSelectOne($c_program2);
                    if ($dapat_program2) {
                        $array_per_kegiatan['nama_program2'] = $dapat_program2->getNamaProgram2();
                    } else {
                        $array_per_kegiatan['nama_program2'] = '';
                    }

                    $array_per_kegiatan['kode_urusan'] = $value->getKodeUrusan();
                    $c_urusan = new Criteria();
                    $c_urusan->add(MasterUrusanPeer::KODE_URUSAN, $value->getKodeUrusan());
                    $dapat_urusan = MasterUrusanPeer::doSelectOne($c_urusan);
                    if ($dapat_urusan) {
                        $array_per_kegiatan['nama_urusan'] = $dapat_urusan->getNamaUrusan();
                    } else {
                        $array_per_kegiatan['nama_urusam'] = '';
                    }

                    $array_subtitle = array();
                    $c_subtitle = new Criteria();
                    $c_subtitle->add(SubtitleIndikatorPeer::UNIT_ID, $value->getUnitId());
                    $c_subtitle->addAnd(SubtitleIndikatorPeer::KEGIATAN_CODE, $value->getKodeKegiatan());
                    $dapat_subtitle = SubtitleIndikatorPeer::doSelect($c_subtitle);
                    if ($dapat_subtitle) {
                        $array_per_subtitle = array();
                        foreach ($dapat_subtitle as $value_subtitle) {
                            $array_per_subtitle['id_subtitle'] = $value_subtitle->getSubId();
                            $array_per_subtitle['nama_subtitle'] = $value_subtitle->getSubtitle();
                            $array_per_subtitle['output_subtitle'] = $value_subtitle->getIndikator() . '|' . $value_subtitle->getNilai() . '|' . $value_subtitle->getSatuan();
                            $array_subtitle[] = $array_per_subtitle;
                        }


                        $array_per_kegiatan['subtitle'] = $array_subtitle;
                    } else {
                        $array_per_kegiatan['subtitle'] = $array_subtitle;
                    }
                    $data_kirim_ahp[] = $array_per_kegiatan;
                }
            } else {
                echo 'ERROR - KEGIATAN';
            }

            $json = json_encode($data_kirim_ahp);
            print_r($json);
        } else {
            echo 'ERROR - PARAMETER';
        }
        exit();
    }

    public function executeAhp2Js() {
        $userpassword = $this->getRequestParameter('up');
        $username = $this->getRequestParameter('un');

        $unit_id = $this->getRequestParameter('skpd');

        if ($username == 'bappeko' && $userpassword == 'bappeko') {
            $data_kirim_ahp = array();

            $c_rd = new Criteria();
            $c_rd->add(RincianDetailPeer::UNIT_ID, $unit_id);
//            $c_rd->addAnd(RincianDetailPeer::STATUS_HAPUS, FALSE);
            $c_rd->addAscendingOrderByColumn(RincianDetailPeer::UNIT_ID);
            $c_rd->addAscendingOrderByColumn(RincianDetailPeer::KEGIATAN_CODE);
            $c_rd->addAscendingOrderByColumn(RincianDetailPeer::DETAIL_NO);
            $dapat_rd = RincianDetailPeer::doSelect($c_rd);

            if ($dapat_rd) {
                foreach ($dapat_rd as $value) {

                    $kode_rka = $value->getUnitId() . '.' . $value->getKegiatanCode() . '.' . $value->getDetailNo();

                    $unit_name = UnitKerjaPeer::getStringUnitKerja($value->getUnitId());

                    $array_per_rd = array();
                    $array_per_rd['id_budgeting'] = $kode_rka;
                    $array_per_rd['skpd'] = $unit_name;
                    $array_per_rd['kode_kegiatan'] = $value->getKegiatanCode();
                    $array_per_rd['subtitle'] = $value->getSubtitle();
                    $array_per_rd['komponen'] = $value->getKomponenName();
                    $array_per_rd['keterangan'] = $value->getDetailName();
                    $array_per_rd['tipe'] = $value->getTipe();
                    $array_per_rd['anggaran'] = $value->getNilaiAnggaran();
                    $array_per_rd['tahap'] = $value->getTahap();
                    $array_per_rd['status_hapus'] = $value->getStatusHapus();

                    $data_kirim_ahp[] = $array_per_rd;
                }
            } else {
                echo 'ERROR - KEGIATAN';
            }

            $json = json_encode($data_kirim_ahp);
            print_r($json);
        } else {
            echo 'ERROR - PARAMETER';
        }
        exit();
    }

    public function executeMasAmir20jan() {
        $userpassword = $this->getRequestParameter('up');
        $username = $this->getRequestParameter('un');

        $unit_id = $this->getRequestParameter('skpd');

        if ($username == 'bappeko' && $userpassword == md5('bappeko')) {
            $data_kirim_ahp = array();

            $query = "select mk.unit_id, mk.kode_kegiatan, mk.nama_kegiatan, mk.output as output_kegiatan, "
                    . "si.sub_id, si.subtitle, si.indikator||'|'||si.nilai||'|'||si.satuan as output_subtitle, "
                    . "rd.komponen_id, rd.unit_id||'.'||rd.kegiatan_code||'.'||rd.detail_no kode_rka, rd.komponen_name||' '||rd.detail_name as komponen_name, "
                    . "rd.volume, rd.satuan, rd.keterangan_koefisien, rd.nilai_anggaran "
                    . "from ebudget.rincian_detail rd, ebudget.subtitle_indikator si, ebudget.master_kegiatan mk "
                    . "where mk.unit_id = si.unit_id and mk.unit_id = rd.unit_id and rd.unit_id = '$unit_id' "
                    . "and mk.kode_kegiatan = si.kegiatan_code and mk.kode_kegiatan = rd.kegiatan_code "
                    . "and si.subtitle = rd.subtitle and rd.status_hapus = false "
                    . "order by mk.unit_id, mk.kode_kegiatan, si.subtitle, komponen_name ";
            //print_r($query);
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();

            if ($rs1) {
                foreach ($rs1 as $value) {

                    $array_per_rd = array();
                    $array_per_rd['unit_id'] = $unit_id;
                    $array_per_rd['kode_kegiatan'] = $value['kode_kegiatan'];
                    $array_per_rd['nama_kegiatan'] = $value['nama_kegiatan'];
                    $array_per_rd['output_kegiatan'] = $value['output_kegiatan'];
                    $array_per_rd['sub_id'] = $value['sub_id'];
                    $array_per_rd['subtitle'] = $value['subtitle'];
                    $array_per_rd['output_subtitle'] = $value['output_subtitle'];
                    $array_per_rd['kode_rka'] = $value['kode_rka'];
                    $array_per_rd['komponen_id'] = $value['komponen_id'];
                    $array_per_rd['komponen_name'] = $value['komponen_name'];
                    $array_per_rd['volume'] = $value['volume'];
                    $array_per_rd['satuan'] = $value['satuan'];
                    $array_per_rd['keterangan_koefisien'] = $value['keterangan_koefisien'];
                    $array_per_rd['nilai_anggaran'] = $value['nilai_anggaran'];
                    $data_kirim_ahp[] = $array_per_rd;
                }
            } else {
                echo 'ERROR - KEGIATAN';
            }

            $json = json_encode($data_kirim_ahp);
            print_r($json);
        } else {
            echo 'ERROR - PARAMETER';
        }
        exit();
    }

    public function executeResetpassword(){
        $user_id = $this->getRequestParameter('send_user');
        $key = $this->getRequestParameter('kunci');

        if(md5('reset') == $key){
            $con = Propel::getConnection();
            $con->begin();
            try{
                $password_default = md5('Password'.sfConfig::get('app_tahun_default'));

                if($user_id != ''){
                    //ubah master_user_v2
                    $query_ubah_master_user = "UPDATE master_user_v2 SET user_password='$password_default' WHERE user_id='$user_id'";
                    $stmt_update_master = $con->prepareStatement($query_ubah_master_user);
                    $stmt_update_master->executeQuery();
                    
                    //ubah schema_akses_v2
                    $query_ubah_schema_akses = "UPDATE schema_akses_v2 SET is_ubah_pass=null WHERE user_id='$user_id'";
                    $stmt_update_schema = $con->prepareStatement($query_ubah_schema_akses);
                    $stmt_update_schema->executeQuery();

                    $con->commit();
                    $this->setFlash('berhasil', 'User ID telah terupdate');
                }else{
                    $con->rollback();
                    $this->setFlash('gagal', 'Gagal karena user_id kosong');
                }

            }catch (Exception $ex) {
                //echo $ex; exit;
                $con->rollback();
                $this->setFlash('gagal', 'Gagal karena ' . $ex->getMessage());
            }
        }else{
            $this->setFlash('gagal', 'Key Salah');
        }

        return $this->redirect('user_app/userlist');
    }

}
