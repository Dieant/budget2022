<?php if ($sf_request->getError('delete')): ?>
    <div class="alert alert-danger alert-dismissable fade show">
        <strong>Alert! <?php echo __('Could not delete the selected %name%', array('%name%' => 'Master kegiatan')) ?></strong> <?php echo __($sf_flash->get('delete')); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php elseif ($sf_flash->has('berhasil')): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Success!</strong> <?php echo __($sf_flash->get('berhasil')); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php elseif ($sf_flash->has('gagal')): ?>
    <div class="alert alert-danger alert-dismissable fade show">
        <strong>Alert!</strong> <?php echo __($sf_flash->get('gagal')); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </div>
<?php elseif ($sf_flash->has('warning')): ?>
    <div class="alert alert-warning alert-dismissable fade show">
        <strong>Warning!</strong> <?php echo __($sf_flash->get('warning')); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </div>
<?php endif; ?>
