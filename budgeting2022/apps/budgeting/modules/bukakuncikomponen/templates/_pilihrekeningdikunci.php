<div class="table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr>
                <th>Kode Rekening</th>
                <th>Nama Rekening</th>
                <th>Pilih</th>
            </tr>
        </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($pager->getResults() as $rekening): 
            $odd = fmod( ++$i, 2);
            ?>
            <tr class="sf_admin_row_<?php echo $odd ?>">                
                <td style="text-align: center; font-weight: bold"><?php echo $rekening->getRekeningCode() ?></td>
                <td style="text-align: left"><?php echo $rekening->getRekeningName() ?></td>
                <td style="text-align: center">
                    <input type="checkbox" name="rekening[]" value="<?php echo $rekening->getRekeningCode() ?>"> Pilih<br/>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
            echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'bukakuncikomponen/pilihrekeningdikunci?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                $activeclass = ($page == $pager->getPage()) ? 'active' : '';
                echo '<li class="page-item '.$activeclass.'">'.link_to_unless($page == $pager->getPage(), $page, "bukakuncikomponen/pilihrekeningdikunci?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'bukakuncikomponen/pilihrekeningdikunci?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
            endif;
        ?>
    </ul>
</div>