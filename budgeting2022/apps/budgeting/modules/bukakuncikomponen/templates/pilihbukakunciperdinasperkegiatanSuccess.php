<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1><?php echo 'Kunci Dinas </br>[' . $data_unit->getUnitId() . '] ' . $data_unit->getUnitName() . ' - Sub Kegiatan ' . $data_kegiatan->getKodekegiatan() ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Buka Kunci</a></li>
          <li class="breadcrumb-item active">Dinas</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('kegiatan/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card"> 
                    <div class="card-header">
                        <h3 class="card-title">Buka Kunci Dinas - Kegiatan</h3>
                    </div>
                    <?php echo form_tag('bukakuncikomponen/savebukakunciperdinasperkegiatan', array('class' => 'form-horizontal')); ?>
                    <input type="hidden" name="skpd" value="<?php echo $data_kegiatan->getUnitId() ?>"/>
                    <input type="hidden" name="kegiatan" value="<?php echo $data_kegiatan->getKodekegiatan() ?>"/>
                    <div class="card-body">
                        <blockquote class="quote-info">
                          <h5>Belanja Yang Dikunci & Rekening Yang Dibuka dari Belanja Yang Dikunci</h5>
                        </blockquote>
                        <div class="table-responsive p-0">
                            <table class="table table-hover">   
                                <thead class="head_peach">
                                    <tr>
                                        <th>Kode Belanja</th>
                                        <th>Pilih Untuk Dibuka</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($list_belanja as $belanja):
                                        $odd = fmod(++$i, 2);
                                        ?>
                                        <tr>                
                                            <td style="text-align: center; font-weight: bold"><?php echo $belanja->getKodeBelanja() ?></td>
                                            <td style="text-align: center">
                                                <input type="checkbox" name="belanja[]" value="<?php echo $belanja->getKodeBelanja() ?>"> Pilih<br/>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <table class="table table-hover">   
                                <thead class="head_peach">
                                    <tr>
                                        <th>Kode Rekening</th>
                                        <th>Nama Rekening</th>
                                        <th>Pilih Untuk Ditutup</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($list_belanja_rekening as $belanja_rekening):
                                        $odd = fmod(++$i, 2);
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>">                
                                            <td style="text-align: center; font-weight: bold"><?php echo $belanja_rekening->getRekeningCode() ?></td>
                                            <td style="text-align: left"><?php echo $belanja_rekening->getRekeningName() ?></td>
                                            <td style="text-align: center">
                                                <input type="checkbox" name="belanja_rekening[]" value="<?php echo $belanja_rekening->getRekeningCode() ?>"> Pilih<br/>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                        <blockquote class="quote-info">
                          <h5>Rekening Yang Dikunci</h5>
                        </blockquote>                        
                        <div class="table-responsive p-0">
                            <table class="table table-hover">   
                                <thead class="head_peach">
                                    <tr>
                                        <th>Kode Rekening</th>
                                        <th>Nama Rekening</th>
                                        <th>Pilih Untuk Dibuka</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($list_rekening as $rekening):
                                        $odd = fmod(++$i, 2);
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>">                
                                            <td style="text-align: center; font-weight: bold"><?php echo $rekening->getRekeningCode() ?></td>
                                            <td style="text-align: left"><?php echo $rekening->getRekeningName() ?></td>
                                            <td style="text-align: center">
                                                <input type="checkbox" name="rekening[]" value="<?php echo $rekening->getRekeningCode() ?>"> Pilih<br/>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                        <blockquote class="quote-info">
                          <h5>Komponen Yang Dikunci</h5>
                        </blockquote>
                        <div class="table-responsive p-0">
                            <table class="table table-hover">   
                                <thead class="head_peach">
                                    <tr>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Satuan</th>
                                        <th>Harga</th>
                                        <th>Rekening</th>
                                        <th>Pilih Untuk Dibuka</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($list_komponen as $komponen):
                                        $odd = fmod( ++$i, 2);
                                        ?>
                                        <tr class="sf_admin_row_<?php echo $odd ?>">                
                                            <td style="text-align: center; font-weight: bold"><?php echo $komponen->getKomponenId() ?></td>
                                            <td style="text-align: left"><?php echo $komponen->getKomponenName() ?></td>
                                            <td style="text-align: center"><?php echo $komponen->getSatuan() ?></td>
                                            <td style="text-align: right">
                                                <?php
                                                if ($komponen->getSatuan() == '%') {
                                                    echo $komponen->getKomponenHarga();
                                                } else {
                                                    echo number_format($komponen->getKomponenHarga());
                                                }
                                                ?>
                                            </td>
                                            <td style="text-align: center"><?php echo $komponen->getRekening() ?></td>
                                            <td style="text-align: center">
                                                <input type="checkbox" name="komponen[]" value="<?php echo $komponen->getKomponenId() ?>"> Pilih<br/>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <button type="submit" name="submit" class="btn btn-outline-primary btn-sm">Simpan <i class="fas fa-save"></i></button>
                                <button type="reset" name="reset" class="btn btn-outline-danger btn-sm"/>Reset <i class="fa fa-backspace"></i></button>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col --> 
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
        </div>
    </div>
</section>