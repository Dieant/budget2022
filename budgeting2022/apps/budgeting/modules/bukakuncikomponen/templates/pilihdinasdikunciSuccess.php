<?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object', 'Number', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pilih Dinas</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Buka Kunci</a></li>
          <li class="breadcrumb-item active">Dinas</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
    <?php include_partial('bukakuncikomponen/messages'); ?>
    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-filter"></i> Filter
                    </h3>
                </div>
                <div class="card-body">
                    <?php echo form_tag('bukakuncikomponen/pilihbukakunciperdinasperkegiatan', array('class' => 'form-horizontal')); ?>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Perangkat Daerah</label>
                                <?php 
                                    echo select_tag('skpd', objects_for_select($list_skpd, 'getUnitId', 'getUnitName', '', 'include_custom=--Pilih Perangkat Daerah--'), Array('id' => 'skpd1','class' => 'js-example-basic-single form-control select2')); 
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nama sub kegiatan</label>
                                <?php
                                    if ($sf_params->get('kegiatan') && $sf_params->get('skpd')) {
                                        $unit_id = $sf_params->get('skpd');
                                        $kode_kegiatan = $sf_params->get('kegiatan');
                                        $d = new Criteria();
                                        $d->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                                        $d->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
                                        $rs_masterkegiatan = MasterKegiatanParameter::doSelectOne($d);
                                        if ($rs_masterkegiatan) {
                                            echo select_tag('kegiatan', options_for_select(array($rs_masterkegiatan->getKodeKegiatan() => $rs_masterkegiatan->getNamaKegiatan()), $rs_masterkegiatan->getKodeKegiatan(), 'include_custom=---Pilih Satuan Kerja Dulu---'), Array('id' => 'keg1','class' => 'js-example-basic-single form-control select2'));
                                        }
                                    } elseif (!$sf_params->get('kegiatan')) {
                                        echo select_tag('kegiatan', options_for_select(array(), '', 'include_custom=---Pilih SKPD Dulu---'), Array('id' => 'keg1','class' => 'js-example-basic-single form-control select2'));
                                    }
                                ?>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="tombol_filter">Tombol Filter</label><br/>
                                <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                                <?php
                                    echo link_to('Reset <i class="fa fa-backspace"></i>', 'bukakuncikomponen/pilihdinasdikunci?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                                ?>
                            </div>
                            <!-- /.form-group -->
                        </div>
                        <!-- /.col -->                      
                    </div>
                    <?php echo '</form>'; ?>
                </div>
            </div>
          </div>
        </div>
    </div>
</section> 
<script>
    function pilihSemuaSKPD() {
        if (document.getElementById('semuaskpd').checked == true) {
            document.getElementById('skpd1').disabled = true;
            document.getElementById('keg1').disabled = true;
            document.getElementById('semuakegiatan').disabled = true;
        } else {
            document.getElementById('skpd1').disabled = false;
            document.getElementById('keg1').disabled = false;
            document.getElementById('semuakegiatan').disabled = false;
        }
    }

    function pilihSemuaKegiatan() {
        if (document.getElementById('semuakegiatan').checked == true) {
            document.getElementById('keg1').disabled = true;
        } else {
            document.getElementById('keg1').disabled = false;
        }
    }
    
    $("#skpd1").change(function() {
        var id = $(this).val();
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/bukakuncikomponen/pilihkegx/unit_id/" + id + ".html",
            context: document.body
        }).done(function(msg) {
            $('#keg1').html(msg);
        });

    });
</script>