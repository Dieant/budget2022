<?php

/**
 * bukakuncikomponen actions.
 *
 * @package    budgeting
 * @subpackage bukakuncikomponen
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class bukakuncikomponenActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->forward('default', 'module');
    }

    public function executeSavebukakunciperdinasperkegiatan() {
        if (!$this->getRequestParameter('skpd') || !$this->getRequestParameter('kegiatan')) {
            return $this->redirect('bukakuncikomponen/pilihdinasdikunci');
        } else {
            $unit_id = $this->getRequestParameter('skpd');
            $kode_kegiatan = $this->getRequestParameter('kegiatan');

            $ada_komponen = 0;
            if ($this->getRequestParameter('komponen')) {
                $array_komponen = array();
                $array_komponen = $this->getRequestParameter('komponen');
                $ada_komponen = 1;
            }

            $ada_rekening = 0;
            if ($this->getRequestParameter('rekening')) {
                $array_rekening = array();
                $array_rekening = $this->getRequestParameter('rekening');
                $ada_rekening = 1;
            }

            $ada_belanja = 0;
            if ($this->getRequestParameter('belanja')) {
                $array_belanja = array();
                $array_belanja = $this->getRequestParameter('belanja');
                $ada_belanja = 1;
            }

            $ada_belanja_rekening = 0;
            if ($this->getRequestParameter('belanja_rekening')) {
                $array_belanja_rekening = array();
                $array_belanja_rekening = $this->getRequestParameter('belanja_rekening');
                $ada_belanja_rekening = 1;
            }

            if ($ada_rekening == 1) {
                foreach ($array_rekening as $rekening_loop) {
                    $cek = $this->cekRekeningDikunci($unit_id, $kode_kegiatan, $rekening_loop);
                    if ($cek == 1) {
                        $c_hapus_rekening_dikunci = new Criteria();
                        $c_hapus_rekening_dikunci->add(RekeningDikunciPeer::UNIT_ID, $unit_id);
                        $c_hapus_rekening_dikunci->add(RekeningDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_hapus_rekening_dikunci->add(RekeningDikunciPeer::REKENING_CODE, $rekening_loop);
                        $data_hapus_rekening_dikunci = RekeningDikunciPeer::doSelectOne($c_hapus_rekening_dikunci);
                        $data_hapus_rekening_dikunci->delete();
                    }
                }
            }

            if ($ada_komponen == 1) {
                foreach ($array_komponen as $komponen_loop) {
                    $cek = $this->cekKomponenDikunci($unit_id, $kode_kegiatan, $komponen_loop);
                    if ($cek == 1) {
                        $c_hapus_komponen_dikunci = new Criteria();
                        $c_hapus_komponen_dikunci->add(KomponenDikunciPeer::UNIT_ID, $unit_id);
                        $c_hapus_komponen_dikunci->add(KomponenDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_hapus_komponen_dikunci->add(KomponenDikunciPeer::KOMPONEN_ID, $komponen_loop);
                        $data_hapus_komponen_dikunci = KomponenDikunciPeer::doSelectOne($c_hapus_komponen_dikunci);
                        $data_hapus_komponen_dikunci->delete();
                    }
                }
            }

            if ($ada_belanja == 1) {
                foreach ($array_belanja as $belanja_loop) {
                    $cek = $this->cekBelanjaDikunci($unit_id, $kode_kegiatan, $belanja_loop);
                    if ($cek == 1) {
                        $c_hapus_belanja_dikunci = new Criteria();
                        $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::UNIT_ID, $unit_id);
                        $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
                        $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KODE_BELANJA, $belanja_loop);
                        $data_hapus_belanja_dikunci = BelanjaDikunciPeer::doSelectOne($c_hapus_belanja_dikunci);

                        $id_belanja_dikunci = $data_hapus_belanja_dikunci->getId();

                        $cek_belanja_rekening = $this->cekRekeningDibukaBelanjaDikunci($unit_id, $kode_kegiatan, $id_belanja_dikunci, NULL);
                        if ($cek_belanja_rekening == 1) {
                            $c_hapus_belanja_rekening = new Criteria();
                            $c_hapus_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $unit_id);
                            $c_hapus_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
                            $c_hapus_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID, $id_belanja_dikunci);
                            $data_hapus_belanja_rekening = RekeningDibukaBelanjaDikunciPeer::doSelect($c_hapus_belanja_rekening);
                            foreach ($data_hapus_belanja_rekening as $value) {
                                $value->delete();
                            }
                        }

                        $data_hapus_belanja_dikunci->delete();
                    }
                }
            }

            if ($ada_belanja_rekening == 1) {
                foreach ($array_belanja_rekening as $belanja_rekening_loop) {
                    $c_hapus_belanja_rekening = new Criteria();
                    $c_hapus_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $unit_id);
                    $c_hapus_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_hapus_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::REKENING_CODE, $belanja_rekening_loop);
                    $data_hapus_belanja_rekening = RekeningDibukaBelanjaDikunciPeer::doSelectOne($c_hapus_belanja_rekening);
                    $data_hapus_belanja_rekening->delete();
                }
            }

            $this->setFlash('berhasil', 'Komponen & Rekening telah dibuka untuk SKPD dan Kegiatan tersebut.');
            return $this->redirect('bukakuncikomponen/pilihdinasdikunci');
        }
    }

    public function executePilihbukakunciperdinasperkegiatan() {
        if (!$this->getRequestParameter('skpd') || !$this->getRequestParameter('kegiatan')) {
            return $this->redirect('bukakuncikomponen/pilihdinasdikunci');
        } else {
            $unit_id = $this->getRequestParameter('skpd');
            $kode_kegiatan = $this->getRequestParameter('kegiatan');
            $c_kegiatan = new Criteria();
            $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
            $this->data_kegiatan = $data_kegiatan = DinasMasterKegiatanPeer::doSelectOne($c_kegiatan);

            $c_unitid = new Criteria();
            $c_unitid->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $this->data_unit = $data_unit = UnitKerjaPeer::doSelectOne($c_unitid);

            $c_rekening = new Criteria();
            $c_rekening->add(RekeningDikunciPeer::UNIT_ID, $unit_id);
            $c_rekening->add(RekeningDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_rekening->addJoin(RekeningDikunciPeer::REKENING_CODE, RekeningPeer::REKENING_CODE, Criteria::INNER_JOIN);
            $c_rekening->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
            $this->list_rekening = $list_rekening = RekeningPeer::doSelect($c_rekening);

            $c_komponen = new Criteria();
            $c_komponen->add(KomponenDikunciPeer::UNIT_ID, $unit_id);
            $c_komponen->add(KomponenDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_komponen->addJoin(KomponenDikunciPeer::KOMPONEN_ID, KomponenPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
            $c_komponen->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
            $this->list_komponen = $list_komponen = KomponenPeer::doSelect($c_komponen);

            $c_belanja = new Criteria();
            $c_belanja->add(BelanjaDikunciPeer::UNIT_ID, $unit_id);
            $c_belanja->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_belanja->addAscendingOrderByColumn(BelanjaDikunciPeer::KODE_BELANJA);
            $this->list_belanja = $list_belanja = BelanjaDikunciPeer::doSelect($c_belanja);

            $c_belanja_rekening = new Criteria();
            $c_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $unit_id);
            $c_belanja_rekening->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_belanja_rekening->addJoin(RekeningDibukaBelanjaDikunciPeer::REKENING_CODE, RekeningPeer::REKENING_CODE, Criteria::INNER_JOIN);
            $c_belanja_rekening->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
            $this->list_belanja_rekening = $list_belanja_rekening = RekeningPeer::doSelect($c_belanja_rekening);
        }
    }

//    irul 23sept 2k15 - fungsi form memilih SKPD & kegiatan
    public function executePilihdinasdikunci() {
        $c_skpd = new Criteria();
        $c_skpd->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_skpd->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $data_skpd = UnitKerjaPeer::doSelect($c_skpd);
    }

//    irul 23sept 2k15 - fungsi form memilih SKPD & Kegiatan     
//    irul 23sep 2k15 - fungsi membuka komponen
    public function executePilihkomponendibuka() {
        $c_skpd = new Criteria();
        $c_skpd->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_skpd->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $data_skpd = UnitKerjaPeer::doSelect($c_skpd);

        $this->processFilterspilihkomponendikunci();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/pilihkomponendikunci/filters');

        $pagers = new sfPropelPager('Komponen', 50);
        $c = new Criteria();
        $c->setDistinct();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK', Criteria::NOT_EQUAL);
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        $c->addJoin(KomponenRekeningPeer::REKENING_CODE, RekeningPeer::REKENING_CODE, Criteria::INNER_JOIN);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_NAME);
        $this->addFiltersCriteriapilihkomponendikunci($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeSaveKomponenDibuka() {
        if ($this->getRequestParameter('semuaskpd') == 'semuaskpd') {
            $pilihan_semuaskpd = 1;
        } else {
            $pilihan_semuaskpd = 0;
        }

        if ($this->getRequestParameter('semuakegiatan') == 'semuakegiatan') {
            $pilihan_semuakegiatan = 1;
        } else {
            $pilihan_semuakegiatan = 0;
        }

        $ada_komponen = 0;
        if ($this->getRequestParameter('komponen')) {
            $array_komponen = array();
            $array_komponen = $this->getRequestParameter('komponen');
            $ada_komponen = 1;
        }

        if ($pilihan_semuaskpd == 0 && !$this->getRequestParameter('skpd')) {
            echo 'MILIH SKPD DONG BOS ';
            exit();
        } else if ($pilihan_semuaskpd == 0 && $pilihan_semuakegiatan == 0 && !$this->getRequestParameter('kegiatan')) {
            echo 'MILIH KEGIATAN DONG BOS ';
            exit();
        } else if ($ada_komponen == 0) {
            echo 'MILIH KOMPONEN DONG BOS ';
            exit();
        } else {
            if ($pilihan_semuaskpd == 1) {
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
                $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan                
            } else {
                $unit_id = $this->getRequestParameter('skpd');
                if ($pilihan_semuakegiatan == 1) {
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $data_kegiatan = DInasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                } else {
                    $kode_kegiatan = $this->getRequestParameter('kegiatan');
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $pertama = TRUE;
                    foreach ($kode_kegiatan as $value) {
                        if ($pertama) {
                            $crit0 = $c_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, $value);
                            $pertama = FALSE;
                        } else {
                            $crit0->addOr($c_kegiatan->getNewCriterion(DinasMasterKegiatanPeer::KODE_KEGIATAN, $value));
                        }
                    }
                    $c_kegiatan->add($crit0);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
                }
            }
            try {
                foreach ($array_komponen as $komponen_loop) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekKomponenDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $komponen_loop);
                        if ($cek == 1) {
                            $c_hapus_komponen_dikunci = new Criteria();
                            $c_hapus_komponen_dikunci->add(KomponenDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                            $c_hapus_komponen_dikunci->add(KomponenDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                            $c_hapus_komponen_dikunci->add(KomponenDikunciPeer::KOMPONEN_ID, $komponen_loop);
                            $data_hapus_komponen_dikunci = KomponenDikunciPeer::doSelectOne($c_hapus_komponen_dikunci);
                            $data_hapus_komponen_dikunci->delete();
                        }

                        $cek_buka = $this->cekKomponenDibuka($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $komponen_loop);
                        if ($cek_buka == 0) {
                            $c_tambah_komponen_dibuka = new KomponenDibuka();
                            $c_tambah_komponen_dibuka->setUnitId($kegiatan_loop->getUnitId());
                            $c_tambah_komponen_dibuka->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                            $c_tambah_komponen_dibuka->setKomponenId($komponen_loop);
                            $c_tambah_komponen_dibuka->save();
                        }
                    }
                }
                $this->setFlash('berhasil', 'Komponen telah dibuka untuk SKPD dan Kegiatan tersebut.');
                return $this->redirect('bukakuncikomponen/pilihkomponendibuka');
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'komponen gagal dibuka karena ' . $exc->getMessage());
                return $this->redirect('bukakuncikomponen/pilihkomponendibuka');
            }
        }
    }

//    irul 23sep 2k15 - fungsi membuka komponen    
//    irul 23sep 2k15 - fungsi membuka rekening
    public function executePilihrekeningdibuka() {
        $c_skpd = new Criteria();
        $c_skpd->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_skpd->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $data_skpd = UnitKerjaPeer::doSelect($c_skpd);

        $this->processFilterspilihrekeningdikunci();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/pilihrekeningdikunci/filters');

        $pagers = new sfPropelPager('Rekening', 20);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $this->addFiltersCriteriapilihrekeningdikunci($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    public function executeSaveRekeningDibuka() {
        $savingAll521 = 0;
        $savingAll522 = 0;
        $savingAll523 = 0;

        $savingKhusus = 0;


        if ($this->getRequestParameter('semuaskpd') == 'semuaskpd') {
            $pilihan_semuaskpd = 1;
        } else {
            $pilihan_semuaskpd = 0;
        }

        if ($this->getRequestParameter('semuakegiatan') == 'semuakegiatan') {
            $pilihan_semuakegiatan = 1;
        } else {
            $pilihan_semuakegiatan = 0;
        }

        if ($this->getRequestParameter('semua521') == 'semua521') {
            $pilihan_semua521 = 1;
        } else {
            $pilihan_semua521 = 0;
        }

        if ($this->getRequestParameter('semua522') == 'semua522') {
            $pilihan_semua522 = 1;
        } else {
            $pilihan_semua522 = 0;
        }

        if ($this->getRequestParameter('semua523') == 'semua523') {
            $pilihan_semua523 = 1;
        } else {
            $pilihan_semua523 = 0;
        }

        $ada_rekening = 0;
        if ($this->getRequestParameter('rekening')) {
            $array_rekening = array();
            $array_rekening = $this->getRequestParameter('rekening');
            $ada_rekening = 1;
        }

        if ($pilihan_semuaskpd == 0 && !$this->getRequestParameter('skpd')) {
            echo 'MILIH SKPD DONG BOS ';
            exit();
        } else if ($pilihan_semuaskpd == 0 && $pilihan_semuakegiatan == 0 && !$this->getRequestParameter('kegiatan')) {
            echo 'MILIH KEGIATAN DONG BOS ';
            exit();
        } else if ($ada_rekening == 0 && $pilihan_semua521 == 0 && $pilihan_semua522 == 0 && $pilihan_semua523 == 0) {
            echo 'MILIH REKENING DONG BOS ';
            exit();
        } else {
            if ($pilihan_semuaskpd == 1) {
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
                $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan                
            } else {
                $unit_id = $this->getRequestParameter('skpd');
                if ($pilihan_semuakegiatan == 1) {
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                } else {
                    $kode_kegiatan = $this->getRequestParameter('kegiatan');
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
                }
            }

            try {
//            untuk eksekusi semua 521
                if ($pilihan_semua521 == 1) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), '5.2.1.__.__');
                        if ($cek == 1) {
                            $c_hapus_belanja_dikunci = new Criteria();
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KODE_BELANJA, '5.2.1.__.__');
                            $data_hapus_belanja_dikunci = BelanjaDikunciPeer::doSelectOne($c_hapus_belanja_dikunci);

                            $id_belanja_kunci = $data_hapus_belanja_dikunci->getId();

                            $cek_ada_rekening_dibuka_belanja_dikunci = $this->cekRekeningDibukaBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $id_belanja_kunci, NULL);
                            if ($cek_ada_rekening_dibuka_belanja_dikunci == 1) {
                                $c_hapus_rekening_dibuka_belanja_dikunci = new Criteria();
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID, $id_belanja_kunci);
                                $data_hapus_rekening_dibuka_belanja_dikunci = RekeningDibukaBelanjaDikunciPeer::doSelect($c_hapus_rekening_dibuka_belanja_dikunci);
                                foreach ($data_hapus_rekening_dibuka_belanja_dikunci as $value) {
                                    $value->delete();
                                }
                            }
                            $data_hapus_belanja_dikunci->delete();
                        }
                    }
                    $savingAll521 = 1;
                } else {
                    $savingAll521 = 1;
                }
//            untuk eksekusi semua 521
//            untuk eksekusi semua 522            
                if ($pilihan_semua522 == 1) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), '5.2.2.__.__');
                        if ($cek == 1) {
                            $c_hapus_belanja_dikunci = new Criteria();
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KODE_BELANJA, '5.2.2.__.__');
                            $data_hapus_belanja_dikunci = BelanjaDikunciPeer::doSelectOne($c_hapus_belanja_dikunci);

                            $id_belanja_kunci = $data_hapus_belanja_dikunci->getId();

                            $cek_ada_rekening_dibuka_belanja_dikunci = $this->cekRekeningDibukaBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $id_belanja_kunci, NULL);
                            if ($cek_ada_rekening_dibuka_belanja_dikunci == 1) {
                                $c_hapus_rekening_dibuka_belanja_dikunci = new Criteria();
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID, $id_belanja_kunci);
                                $data_hapus_rekening_dibuka_belanja_dikunci = RekeningDibukaBelanjaDikunciPeer::doSelect($c_hapus_rekening_dibuka_belanja_dikunci);
                                foreach ($data_hapus_rekening_dibuka_belanja_dikunci as $value) {
                                    $value->delete();
                                }
                            }
                            $data_hapus_belanja_dikunci->delete();
                        }
                    }
                    $savingAll522 = 1;
                } else {
                    $savingAll522 = 1;
                }
//            untuk eksekusi semua 522
//            untuk eksekusi semua 523            
                if ($pilihan_semua523 == 1) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), '5.2.3.__.__');
                        if ($cek == 1) {
                            $c_hapus_belanja_dikunci = new Criteria();
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                            $c_hapus_belanja_dikunci->add(BelanjaDikunciPeer::KODE_BELANJA, '5.2.3.__.__');
                            $data_hapus_belanja_dikunci = BelanjaDikunciPeer::doSelectOne($c_hapus_belanja_dikunci);

                            $id_belanja_kunci = $data_hapus_belanja_dikunci->getId();

                            $cek_ada_rekening_dibuka_belanja_dikunci = $this->cekRekeningDibukaBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $id_belanja_kunci, NULL);
                            if ($cek_ada_rekening_dibuka_belanja_dikunci == 1) {
                                $c_hapus_rekening_dibuka_belanja_dikunci = new Criteria();
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                                $c_hapus_rekening_dibuka_belanja_dikunci->add(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID, $id_belanja_kunci);
                                $data_hapus_rekening_dibuka_belanja_dikunci = RekeningDibukaBelanjaDikunciPeer::doSelect($c_hapus_rekening_dibuka_belanja_dikunci);
                                foreach ($data_hapus_rekening_dibuka_belanja_dikunci as $value) {
                                    $value->delete();
                                }
                            }
                            $data_hapus_belanja_dikunci->delete();
                        }
                    }
                    $savingAll523 = 1;
                } else {
                    $savingAll523 = 1;
                }
//            untuk eksekusi semua 523         
//            untuk eksekusi rekening khusus            
                if ($ada_rekening == 1) {
                    foreach ($array_rekening as $rekening_loop) {
                        foreach ($data_kegiatan as $kegiatan_loop) {
                            $kode_belanja_depan = substr($rekening_loop, 0, 6) . '__.__';
                            $cek_ada_belanja_dikunci = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $kode_belanja_depan);
                            if ($cek_ada_belanja_dikunci == 1) {
                                $c_ambil_belanja_dikunci = new Criteria();
                                $c_ambil_belanja_dikunci->add(BelanjaDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                                $c_ambil_belanja_dikunci->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                                $c_ambil_belanja_dikunci->add(BelanjaDikunciPeer::KODE_BELANJA, $kode_belanja_depan);
                                $data_ambil_belanja_dikunci = BelanjaDikunciPeer::doSelectOne($c_ambil_belanja_dikunci);
                                $id_ambil_belanja_kunci = $data_ambil_belanja_dikunci->getId();

                                $cek_ada_rekening_dibuka_belanja_dikunci = $this->cekRekeningDibukaBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $id_ambil_belanja_kunci, $rekening_loop);
                                if ($cek_ada_rekening_dibuka_belanja_dikunci == 0) {
                                    $c_tambah_rekening_dibuka_belanja_dikunci = new RekeningDibukaBelanjaDikunci();
                                    $c_tambah_rekening_dibuka_belanja_dikunci->setUnitId($kegiatan_loop->getUnitId());
                                    $c_tambah_rekening_dibuka_belanja_dikunci->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                                    $c_tambah_rekening_dibuka_belanja_dikunci->setBelanjaDikunciId($id_ambil_belanja_kunci);
                                    $c_tambah_rekening_dibuka_belanja_dikunci->setRekeningCode($rekening_loop);
                                    $c_tambah_rekening_dibuka_belanja_dikunci->save();
                                }
                            } else {
                                $cek = $this->cekRekeningDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $rekening_loop);
                                if ($cek == 1) {
                                    $c_hapus_rekening_dikunci = new Criteria();
                                    $c_hapus_rekening_dikunci->add(RekeningDikunciPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                                    $c_hapus_rekening_dikunci->add(RekeningDikunciPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                                    $c_hapus_rekening_dikunci->add(RekeningDikunciPeer::REKENING_CODE, $rekening_loop);
                                    $data_hapus_rekening_dikunci = RekeningDikunciPeer::doSelectOne($c_hapus_rekening_dikunci);
                                    $data_hapus_rekening_dikunci->delete();
                                }
                            }
                        }
                    }
                    $savingKhusus = 1;
                } else {
                    $savingKhusus = 1;
                }
//            untuk eksekusi rekening khusus                        
                if ($savingAll521 == 1 && $savingAll522 == 1 && $savingAll523 == 1 && $savingKhusus == 1) {
                    $this->setFlash('berhasil', 'Rekening telah dibuka untuk SKPD dan Kegiatan tersebut.');
                    return $this->redirect('bukakuncikomponen/pilihrekeningdibuka');
                } else {
                    $this->setFlash('gagal', 'Rekening gagal terbuka.');
                    return $this->redirect('bukakuncikomponen/pilihrekeningdibuka');
                }
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'Rekening gagal di buka karena ' . $exc->getMessage());
                return $this->redirect('bukakuncikomponen/pilihrekeningdibuka');
            }
        }
    }

//    irul 23sep 2k15 - fungsi membuka rekening

    public function executePilihkegx() {
        if ($this->getRequestParameter('unit_id') != '') {
            $unit_id = $this->getRequestParameter('unit_id');

            $queryku = "select kode_kegiatan, nama_kegiatan "
                    . "from " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan "
                    . "where unit_id='$unit_id' "
                    . "order by kode_kegiatan";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('kode_kegiatan') != '') {
                    $arr_tampung[$rcsb->getString('kode_kegiatan')] = $rcsb->getString('kode_kegiatan') . '-' . $rcsb->getString('nama_kegiatan');
                }
            }
            $this->isi_baru = $arr_tampung;
        }
    }

//    irul 22sept 2k15 - fungsi form memilih komponen yg dikunci 
    public function executePilihkomponendikunci() {
        $c_skpd = new Criteria();
        $c_skpd->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_skpd->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $data_skpd = UnitKerjaPeer::doSelect($c_skpd);

        $this->processFilterspilihkomponendikunci();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/pilihkomponendikunci/filters');

        $pagers = new sfPropelPager('Komponen', 50);
        $c = new Criteria();
        $c->setDistinct();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK', Criteria::NOT_EQUAL);
        $c->addJoin(KomponenPeer::KOMPONEN_ID, KomponenRekeningPeer::KOMPONEN_ID, Criteria::INNER_JOIN);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_NAME);
        $this->addFiltersCriteriapilihkomponendikunci($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterspilihkomponendikunci() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/pilihkomponendikunci/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/pilihkomponendikunci/filters');
        }
    }

    protected function addFiltersCriteriapilihkomponendikunci($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['nama_komponen_is_empty'])) {
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['komponen']) && $this->filters['komponen'] !== '') {
                    $kata = '%' . $this->filters['komponen'] . '%';
                    $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['komponen_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                    $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    public function executeSaveKomponenDikunci() {
        if ($this->getRequestParameter('semuaskpd') == 'semuaskpd') {
            $pilihan_semuaskpd = 1;
        } else {
            $pilihan_semuaskpd = 0;
        }

        if ($this->getRequestParameter('semuakegiatan') == 'semuakegiatan') {
            $pilihan_semuakegiatan = 1;
        } else {
            $pilihan_semuakegiatan = 0;
        }

        $ada_komponen = 0;
        if ($this->getRequestParameter('komponen')) {
            $array_komponen = array();
            $array_komponen = $this->getRequestParameter('komponen');
            $ada_komponen = 1;
        }

        if ($pilihan_semuaskpd == 0 && !$this->getRequestParameter('skpd')) {
            echo 'MILIH SKPD DONG BOS ';
            exit();
        } else if ($pilihan_semuaskpd == 0 && $pilihan_semuakegiatan == 0 && !$this->getRequestParameter('kegiatan')) {
            echo 'MILIH KEGIATAN DONG BOS ';
            exit();
        } else if ($ada_komponen == 0) {
            echo 'MILIH KOMPONEN DONG BOS ';
            exit();
        } else {
            if ($pilihan_semuaskpd == 1) {
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
                $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan                
            } else {
                $unit_id = $this->getRequestParameter('skpd');
                if ($pilihan_semuakegiatan == 1) {
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                } else {
                    $kode_kegiatan = $this->getRequestParameter('kegiatan');
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
                }
            }
            try {
                foreach ($array_komponen as $komponen_loop) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekKomponenDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $komponen_loop);
                        if ($cek == 0) {
                            $c_tambah_komponen_dikunci = new KomponenDikunci();
                            $c_tambah_komponen_dikunci->setUnitId($kegiatan_loop->getUnitId());
                            $c_tambah_komponen_dikunci->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                            $c_tambah_komponen_dikunci->setKomponenId($komponen_loop);
                            $c_tambah_komponen_dikunci->save();
                        }

                        $cek_buka = $this->cekKomponenDibuka($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $komponen_loop);
                        if ($cek_buka == 1) {
                            $c_hapus_komponen_dibuka = new Criteria();
                            $c_hapus_komponen_dibuka->add(KomponenDibukaPeer::UNIT_ID, $kegiatan_loop->getUnitId());
                            $c_hapus_komponen_dibuka->add(KomponenDibukaPeer::KEGIATAN_CODE, $kegiatan_loop->getKodeKegiatan());
                            $c_hapus_komponen_dibuka->add(KomponenDibukaPeer::KOMPONEN_ID, $komponen_loop);
                            $data_hapus_komponen_dibuka = KomponenDibukaPeer::doSelectOne($c_hapus_komponen_dibuka);
                            $data_hapus_komponen_dibuka->delete();
                        }
                    }
                }
                $this->setFlash('berhasil', 'Komponen telah dikunci untuk SKPD dan Kegiatan tersebut.');
                return $this->redirect('bukakuncikomponen/pilihkomponendikunci');
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'komponen gagal dikunci karena ' . $exc->getMessage());
                return $this->redirect('bukakuncikomponen/pilihkomponendikunci');
            }
        }
    }

    function cekKomponenDikunci($unit_id, $kode_kegiatan, $komponen_id) {
//        fungsi cek komponen dikunci
        $c_cek = new Criteria();
        $c_cek->add(KomponenDikunciPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
        $c_cek->add(KomponenDikunciPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::EQUAL);
        $c_cek->add(KomponenDikunciPeer::KOMPONEN_ID, $komponen_id, Criteria::EQUAL);
        $total_cek = KomponenDikunciPeer::doCount($c_cek);

        if ($total_cek == 0) {
            return 0;
        } else {
            return 1;
        }
//        fungsi cek komponen dikunci        
    }

    function cekKomponenDibuka($unit_id, $kode_kegiatan, $komponen_id) {
//        fungsi cek komponen dikunci
        $c_cek = new Criteria();
        $c_cek->add(KomponenDibukaPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
        $c_cek->add(KomponenDibukaPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::EQUAL);
        $c_cek->add(KomponenDibukaPeer::KOMPONEN_ID, $komponen_id, Criteria::EQUAL);
        $total_cek = KomponenDibukaPeer::doCount($c_cek);

        if ($total_cek == 0) {
            return 0;
        } else {
            return 1;
        }
//        fungsi cek komponen dikunci        
    }

//    irul 22sept 2k15 - fungsi form memilih komponen yg dikunci     
//    irul 22sept 2k15 - fungsi form memilih rekening yg dikunci 
    public function executePilihrekeningdikunci() {
        $c_skpd = new Criteria();
        $c_skpd->add(UnitKerjaPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
        $c_skpd->addAscendingOrderByColumn(UnitKerjaPeer::UNIT_NAME);
        $this->list_skpd = $data_skpd = UnitKerjaPeer::doSelect($c_skpd);

        $this->processFilterspilihrekeningdikunci();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/pilihrekeningdikunci/filters');

        $pagers = new sfPropelPager('Rekening', 20);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $this->addFiltersCriteriapilihrekeningdikunci($c);

        $pagers->setCriteria($c);
        $pagers->setPage($this->getRequestParameter('page', 1));
        $pagers->init();

        $this->pager = $pagers;
    }

    protected function processFilterspilihrekeningdikunci() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');
            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/pilihrekeningdikunci/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/pilihrekeningdikunci/filters');
        }
    }

    protected function addFiltersCriteriapilihrekeningdikunci($c) {
        if (isset($this->filters['select'])) {
            if ($this->filters['select'] == 1) {
                if (isset($this->filters['kode_rekening_is_empty'])) {
                    $criterion = $c->getNewCriterion(RekeningPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['rekening']) && $this->filters['rekening'] !== '') {
                    $kata = '%' . $this->filters['rekening'] . '%';
                    $c->add(RekeningPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } elseif ($this->filters['select'] == 2) {
                if (isset($this->filters['nama_rekening_is_empty'])) {
                    $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                    $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                    $c->add($criterion);
                } else if (isset($this->filters['rekening']) && $this->filters['rekening'] !== '') {
                    $kata = '%' . $this->filters['rekening'] . '%';
                    $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
                }
            } else {
                if (isset($this->filters['rekening_is_empty'])) {
                    echo 'is empty';
                    $criterion = $c->getNewCriterion(RekeningPeer::REKENING_CODE, '');
                    $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_CODE, null, Criteria::ISNULL));
                    $c->add($criterion);
                }
            }
        }
    }

    public function executeSaveRekeningDikunci() {
        $savingAll521 = 0;
        $savingAll522 = 0;
        $savingAll523 = 0;

        $savingKhusus = 0;


        if ($this->getRequestParameter('semuaskpd') == 'semuaskpd') {
            $pilihan_semuaskpd = 1;
        } else {
            $pilihan_semuaskpd = 0;
        }

        if ($this->getRequestParameter('semuakegiatan') == 'semuakegiatan') {
            $pilihan_semuakegiatan = 1;
        } else {
            $pilihan_semuakegiatan = 0;
        }

        if ($this->getRequestParameter('semua521') == 'semua521') {
            $pilihan_semua521 = 1;
        } else {
            $pilihan_semua521 = 0;
        }

        if ($this->getRequestParameter('semua522') == 'semua522') {
            $pilihan_semua522 = 1;
        } else {
            $pilihan_semua522 = 0;
        }

        if ($this->getRequestParameter('semua523') == 'semua523') {
            $pilihan_semua523 = 1;
        } else {
            $pilihan_semua523 = 0;
        }

        $ada_rekening = 0;
        if ($this->getRequestParameter('rekening')) {
            $array_rekening = array();
            $array_rekening = $this->getRequestParameter('rekening');
            $ada_rekening = 1;
        }

        if ($pilihan_semuaskpd == 0 && !$this->getRequestParameter('skpd')) {
            echo 'MILIH SKPD DONG BOS ';
            exit();
        } else if ($pilihan_semuaskpd == 0 && $pilihan_semuakegiatan == 0 && !$this->getRequestParameter('kegiatan')) {
            echo 'MILIH KEGIATAN DONG BOS ';
            exit();
        } else if ($ada_rekening == 0 && $pilihan_semua521 == 0 && $pilihan_semua522 == 0 && $pilihan_semua523 == 0) {
            echo 'MILIH REKENING DONG BOS ';
            exit();
        } else {
            if ($pilihan_semuaskpd == 1) {
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan
                $c_kegiatan = new Criteria();
                $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, '9999', Criteria::NOT_EQUAL);
                $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua skpd, maka semua kegiatan selain Dinas Latihan                
            } else {
                $unit_id = $this->getRequestParameter('skpd');
                if ($pilihan_semuakegiatan == 1) {
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
//                apabila semua kegiatan, maka semua kegiatan pada Dinas terkait
                } else {
                    $kode_kegiatan = $this->getRequestParameter('kegiatan');
                    $c_kegiatan = new Criteria();
                    $c_kegiatan->add(DinasMasterKegiatanPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                    $c_kegiatan->add(DinasMasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan, Criteria::EQUAL);
                    $data_kegiatan = DinasMasterKegiatanPeer::doSelect($c_kegiatan);
                }
            }

            try {
//            untuk eksekusi semua 521
                if ($pilihan_semua521 == 1) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), '5.2.1.__.__');
                        if ($cek == 0) {
                            $c_tambah_belanja_dikunci = new BelanjaDikunci();
                            $c_tambah_belanja_dikunci->setUnitId($kegiatan_loop->getUnitId());
                            $c_tambah_belanja_dikunci->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                            $c_tambah_belanja_dikunci->setKodeBelanja('5.2.1.__.__');
                            $c_tambah_belanja_dikunci->save();
                        }
                    }
                    $savingAll521 = 1;
                } else {
                    $savingAll521 = 1;
                }
//            untuk eksekusi semua 521
//            untuk eksekusi semua 522            
                if ($pilihan_semua522 == 1) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), '5.2.2.__.__');
                        if ($cek == 0) {
                            $c_tambah_belanja_dikunci = new BelanjaDikunci();
                            $c_tambah_belanja_dikunci->setUnitId($kegiatan_loop->getUnitId());
                            $c_tambah_belanja_dikunci->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                            $c_tambah_belanja_dikunci->setKodeBelanja('5.2.2.__.__');
                            $c_tambah_belanja_dikunci->save();
                        }
                    }
                    $savingAll522 = 1;
                } else {
                    $savingAll522 = 1;
                }
//            untuk eksekusi semua 522
//            untuk eksekusi semua 523            
                if ($pilihan_semua523 == 1) {
                    foreach ($data_kegiatan as $kegiatan_loop) {
                        $cek = $this->cekBelanjaDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), '5.2.3.__.__');
                        if ($cek == 0) {
                            $c_tambah_belanja_dikunci = new BelanjaDikunci();
                            $c_tambah_belanja_dikunci->setUnitId($kegiatan_loop->getUnitId());
                            $c_tambah_belanja_dikunci->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                            $c_tambah_belanja_dikunci->setKodeBelanja('5.2.3.__.__');
                            $c_tambah_belanja_dikunci->save();
                        }
                    }
                    $savingAll523 = 1;
                } else {
                    $savingAll523 = 1;
                }
//            untuk eksekusi semua 523         
//            untuk eksekusi rekening khusus            
                if ($ada_rekening == 1) {
                    foreach ($array_rekening as $rekening_loop) {
                        foreach ($data_kegiatan as $kegiatan_loop) {
                            $cek = $this->cekRekeningDikunci($kegiatan_loop->getUnitId(), $kegiatan_loop->getKodeKegiatan(), $rekening_loop);
                            if ($cek == 0) {
                                $c_tambah_rekening_dikunci = new RekeningDikunci();
                                $c_tambah_rekening_dikunci->setUnitId($kegiatan_loop->getUnitId());
                                $c_tambah_rekening_dikunci->setKegiatanCode($kegiatan_loop->getKodeKegiatan());
                                $c_tambah_rekening_dikunci->setRekeningCode($rekening_loop);
                                $c_tambah_rekening_dikunci->save();
                            }
                        }
                    }
                    $savingKhusus = 1;
                } else {
                    $savingKhusus = 1;
                }
//            untuk eksekusi rekening khusus                        
                if ($savingAll521 == 1 && $savingAll522 == 1 && $savingAll523 == 1 && $savingKhusus == 1) {
                    $this->setFlash('berhasil', 'Rekening telah dikunci untuk SKPD dan Kegiatan tersebut.');
                    return $this->redirect('bukakuncikomponen/pilihrekeningdikunci');
                } else {
                    $this->setFlash('gagal', 'Rekening gagal terkunci.');
                    return $this->redirect('bukakuncikomponen/pilihrekeningdikunci');
                }
            } catch (Exception $exc) {
                $this->setFlash('gagal', 'Rekening gagal karena ' . $exc->getMessage());
                return $this->redirect('bukakuncikomponen/pilihrekeningdikunci');
            }
        }
    }

    function cekRekeningDikunci($unit_id, $kode_kegiatan, $rekening_code) {
//        fungsi cek rekening dikunci
        $c_cek = new Criteria();
        $c_cek->add(RekeningDikunciPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
        $c_cek->add(RekeningDikunciPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::EQUAL);
        $c_cek->add(RekeningDikunciPeer::REKENING_CODE, $rekening_code, Criteria::EQUAL);
        $total_cek = RekeningDikunciPeer::doCount($c_cek);

        if ($total_cek == 0) {
            return 0;
        } else {
            return 1;
        }
//        fungsi cek rekening dikunci        
    }

    function cekBelanjaDikunci($unit_id, $kode_kegiatan, $kode_belanja) {
//        fungsi cek belanja dikunci
        $c_cek = new Criteria();
        $c_cek->add(BelanjaDikunciPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
        $c_cek->add(BelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::EQUAL);
        $c_cek->add(BelanjaDikunciPeer::KODE_BELANJA, $kode_belanja, Criteria::EQUAL);
        $total_cek = BelanjaDikunciPeer::doCount($c_cek);

        if ($total_cek == 0) {
            return 0;
        } else {
            return 1;
        }
//        fungsi cek belanja dikunci
    }

    function cekRekeningDibukaBelanjaDikunci($unit_id, $kode_kegiatan, $id_belanja_dikunci, $rekening_code) {
//        fungsi cek rekening dibuka belanja dikunci

        $c_cek = new Criteria();
        $c_cek->add(RekeningDibukaBelanjaDikunciPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
        $c_cek->add(RekeningDibukaBelanjaDikunciPeer::KEGIATAN_CODE, $kode_kegiatan, Criteria::EQUAL);
        $c_cek->add(RekeningDibukaBelanjaDikunciPeer::BELANJA_DIKUNCI_ID, $id_belanja_dikunci, Criteria::EQUAL);
        if ($rekening_code != NULL) {
            $c_cek->add(RekeningDibukaBelanjaDikunciPeer::REKENING_CODE, $rekening_code, Criteria::EQUAL);
        }
        $total_cek = RekeningDibukaBelanjaDikunciPeer::doCount($c_cek);

        if ($total_cek == 0) {
            return 0;
        } else {
            return 1;
        }
//        fungsi cek rekening dibuka belanja dikunci
    }

//    irul 22sept 2k15 - fungsi form memilih rekening yg dikunci 
}
