<?php

/**
 * asbnonfisik actions.
 *
 * @package    budgeting
 * @subpackage asbnonfisik
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class asbnonfisikActions extends sfActions {

    /**
     * Executes index action
     *
     */
    public function executeIndex() {
        $this->executeSublist();
    }

    public function executeSublist() {
        $this->processSortsub();

        $this->processFilterssub();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sub_kegiatan/filters');

        // pager
        $this->pager = new sfPropelPager('SubKegiatan', 25);
        $c = new Criteria();
        $c->add(SubKegiatanPeer::STATUS, 'Close');
        $this->addSortCriteriasub($c);
        $this->addFiltersCriteriasub($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processSortsub() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/sub_kegiatan/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/sub_kegiatan/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            
        }
    }

    protected function addFiltersCriteriasub($c) {
        if (isset($this->filters['sub_kegiatan_id_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_id']) && $this->filters['sub_kegiatan_id'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_id'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['sub_kegiatan_name_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_name']) && $this->filters['sub_kegiatan_name'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_name'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasub($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            $sort_column = SubKegiatanPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/sub_kegiatan/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    protected function processFilterssub() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sub_kegiatan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sub_kegiatan/filters');
        }
    }

    public function executeDetailsublist() {
        $coded = $this->getRequestParameter('coded', '');
        if ($coded != '') {
            $this->setLayout('kosong');
        }
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
        //$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
            sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
        from 
            " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
        where 
            m.sub_kegiatan_id = '" . $sub_id . "'";
        //print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
        SELECT 
        rekening.rekening_code,
        detail.detail_name as detail_name,
        detail.komponen_name,
        detail.komponen_name || ' ' || detail.detail_name as detail_name2,
        detail.komponen_harga_awal as detail_harga,
        detail.pajak,
        detail.komponen_id,
        detail.subtitle ,
        detail_no,koefisien,param,
        detail.satuan as detail_satuan,
        replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
        detail.volume * detail.komponen_harga_awal as hasil,
        (detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,


        (SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
        as x,
        (SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
        as bahaya,
        substring(kb.belanja_name,8) as belanja_name

        FROM 
        " . sfConfig::get('app_default_schema') . ".rekening rekening ,
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
        " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb

        WHERE 
        rekening.rekening_code = detail.rekening_code and
        detail.sub_kegiatan_id = '" . $sub_id . "' and 
        kb.belanja_id=rekening.belanja_id 

        ORDER BY 

        belanja_urutan,
        komponen_name

        ";
        //echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
        //$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
            //echo $r['param'];
            $input[$i] = $inputs;
            //print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
        //$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

}
