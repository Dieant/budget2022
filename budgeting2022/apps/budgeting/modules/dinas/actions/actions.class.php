<?php

/**
 * dinas actions.
 *
 * @package    budgeting
 * @subpackage dinas
 * @author     pemkot
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class dinasActions extends autodinasActions {

    //tiket #58
    //23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaitingEdit() {
        $this->unit_id = $this->getRequestParameter('unitid');
        $this->kode_kegiatan = $this->getRequestParameter('kodekegiatan');
        $this->detail_no = $this->getRequestParameter('detailno');
    }

    //tiket #58
    //23 Februari 2016 - kembalikan ke waiting list
    public function executeKembalikanWaiting() {
        $sekarang = date('Y-m-d H:i:s');
        $detail_no = $this->getRequestParameter('detail_no');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $keterangan = trim($this->getRequestParameter('keterangan'));
        if (is_null($detail_no) || is_null($unit_id) || is_null($kode_kegiatan) || is_null($keterangan) || ($this->getRequest()->hasParameter('key') != md5('kembali_waiting'))) {
            $this->setFlash('gagal', 'Gagal karena parameter kurang');
            return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else if (strlen($keterangan) < 20) {
            $this->setFlash('gagal', 'Keterangan minimal berisi 20 karakter');
            return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
        } else {
            //ambil rincian detail yang dikembalikan
            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(RincianDetailPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_rincian_detail->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $c_rincian_detail->add(RincianDetailPeer::STATUS_HAPUS, FALSE);
            $rd = RincianDetailPeer::doSelectOne($c_rincian_detail);
            $kode_rka = $rd->getUnitId() . '.' . $rd->getKegiatanCode() . '.' . $rd->getDetailNo();
            $komponen_name = $rd->getKomponenName();
            $detail_name = $rd->getDetailName();
            //ambil waitinglist pu
            $c_waiting = new Criteria();
            $c_waiting->add(WaitingListPUPeer::KODE_RKA, $kode_rka);
            $waiting = WaitingListPUPeer::doSelectOne($c_waiting);
            $id_waiting = $waiting->getIdWaiting();
            $kode_rka_waiting = $waiting->getUnitId() . '.' . $waiting->getKegiatanCode() . '.' . $waiting->getIdWaiting();
            $prioritas = $waiting->getPrioritas();

            //ambil geojsonlokasi rev1
            $c_cari_geojson = new Criteria();
            $c_cari_geojson->add(GeojsonlokasiRev1Peer::DETAIL_NO, $detail_no);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::UNIT_ID, $unit_id);
            $c_cari_geojson->addAnd(GeojsonlokasiRev1Peer::STATUS_HAPUS, FALSE);
            $dapat_geojson = GeojsonlokasiRev1Peer::doSelect($c_cari_geojson);

            //ambil geojsonlokasi waiting
            $c_hapus_geojson = new Criteria();
            $c_hapus_geojson->add(GeojsonlokasiWaitinglistPeer::ID_WAITING, $id_waiting);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::UNIT_ID, 'XXX' . $unit_id);
            $c_hapus_geojson->addAnd(GeojsonlokasiWaitinglistPeer::STATUS_HAPUS, FALSE);
            $hapus_geojson = GeojsonlokasiWaitinglistPeer::doSelect($c_hapus_geojson);

            //ambil nama skpd
            $c_unit = new Criteria();
            $c_unit->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $data_unit_kerja = UnitKerjaPeer::doSelectOne($c_unit);

            //cek lelang+eproject+edelivery
            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;
                $rd2 = new RincianDetail();
                $totNilaiSwakelola = $rd2->getCekNilaiSwakelolaDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiKontrak = $rd2->getCekNilaiKontrakDelivery2($unit_id, $kode_kegiatan, $detail_no);
                $totNilaiAlokasi = $rd2->getCekNilaiAlokasiProject($unit_id, $kode_kegiatan, $detail_no);
                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                    //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                    //$totNilaiKontrak = $totNilaiKontrak + 10;
                }
                if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                    $totNilaiHps = $rd2->getCekNilaiHPSKomponen($unit_id, $kode_kegiatan, $detail_no);
                    $lelang = $rd2->getCekLelang($unit_id, $kode_kegiatan, $detail_no, 0);
                    $ceklelangselesaitidakaturanpembayaran = $rd2->getCekLelangTidakAdaAturanPembayaran($unit_id, $kode_kegiatan, $detail_no);
                }
                if (0 < $totNilaiKontrak || 0 < $totNilaiSwakelola) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                        return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                    }
                } else if (0 < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                    return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                    return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
                }
            }

            //begin transaction
            $con = Propel::getConnection();
            $con->begin();
            try {
                //delete lokasi waiting lama
                $c_hapus_lokasi = new Criteria();
                $c_hapus_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_waiting);
                $lokasi_hapus = HistoryPekerjaanV2Peer::doSelect($c_hapus_lokasi);
                foreach ($lokasi_hapus as $lokasi_value) {
                    $lokasi_value->setStatusHapus(true);
                    $lokasi_value->save();
                }

                //delete geojsonlokasi waiting
                foreach ($hapus_geojson as $value_geojson) {
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //ambil lokasi rincian detail di history pekerjaan V2
                $c_buat_lokasi = new Criteria();
                $c_buat_lokasi->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
                $c_buat_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                $lokasi_lama = HistoryPekerjaanV2Peer::doSelect($c_buat_lokasi);

                foreach ($lokasi_lama as $dapat_lokasi_lama) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                    $gang_lama = $dapat_lokasi_lama->getGang();
                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                    $rw_lama = $dapat_lokasi_lama->getRw();
                    $rt_lama = $dapat_lokasi_lama->getRt();
                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                    $tempat_lama = $dapat_lokasi_lama->getTempat();
                    if ($jalan_lama <> '') {
                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                    }
                    if ($tempat_lama <> '') {
                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                    }
                    if ($gang_lama <> '') {
                        $gang_fix = $gang_lama . ' ';
                    }
                    if ($nomor_lama <> '') {
                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                    }
                    if ($rw_lama <> '') {
                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                    }
                    if ($rt_lama <> '') {
                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                    }
                    if ($keterangan_lama <> '') {
                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                    }
                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                    //insert lokasi waiting
                    $komponen_lokasi_fix = $rd->getKomponenName() . ' ' . $rd->getDetailName();
                    $kecamatan_lokasi_fix = $rd->getLokasiKecamatan();
                    $kelurahan_lokasi_fix = $rd->getLokasiKelurahan();
                    $lokasi_per_titik_fix = $lokasi_baru;

                    $c_insert_gis = new HistoryPekerjaanV2();
                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                    $c_insert_gis->setKodeRka($kode_rka_waiting);
                    $c_insert_gis->setStatusHapus(FALSE);
                    $c_insert_gis->setJalan(strtoupper($jalan_lama));
                    $c_insert_gis->setGang(strtoupper($gang_lama));
                    $c_insert_gis->setNomor(strtoupper($nomor_lama));
                    $c_insert_gis->setRw(strtoupper($rw_lama));
                    $c_insert_gis->setRt(strtoupper($rt_lama));
                    $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                    $c_insert_gis->setTempat(strtoupper($tempat_lama));
                    $c_insert_gis->setKomponen($komponen_lokasi_fix);
                    $c_insert_gis->setKecamatan($kecamatan_lokasi_fix);
                    $c_insert_gis->setKelurahan($kelurahan_lokasi_fix);
                    $c_insert_gis->setLokasi($lokasi_per_titik_fix);
                    $c_insert_gis->save();
                    $dapat_lokasi_lama->setStatusHapus(true);
                    $dapat_lokasi_lama->save();
                }

                foreach ($dapat_geojson as $value_geojson) {

                    //insert geojsonlokasi waiting
                    $geojson_baru = new GeojsonlokasiWaitinglist();
                    $geojson_baru->setUnitId('XXX' . $unit_id);
                    $geojson_baru->setUnitName($data_unit_kerja->getUnitName());
                    $geojson_baru->setKegiatanCode($kode_kegiatan);
                    $geojson_baru->setIdWaiting($id_waiting);
                    $geojson_baru->setSatuan($rd->getSatuan());
                    $geojson_baru->setVolume($rd->getVolume());
                    $geojson_baru->setNilaiAnggaran($rd->getNilaiAnggaran());
                    $geojson_baru->setTahun(sfConfig::get('app_tahun_default'));
                    $geojson_baru->setMlokasi($value_geojson->getMlokasi());
                    $geojson_baru->setIdKelompok($value_geojson->getIdKelompok());
                    $geojson_baru->setGeojson($value_geojson->getGeojson());
                    $geojson_baru->setKeterangan($value_geojson->getKeterangan());
                    $geojson_baru->setNmuser($value_geojson->getNmuser());
                    $geojson_baru->setLevel($value_geojson->getLevel());
                    $geojson_baru->setKomponenName($rd->getKomponenName() . ' ' . $rd->getDetailName());
                    $geojson_baru->setStatusHapus(FALSE);
                    $geojson_baru->setKeteranganAlamat($value_geojson->getKeteranganAlamat());
                    $geojson_baru->setLastCreateTime($sekarang);
                    $geojson_baru->setLastEditTime($sekarang);
                    $geojson_baru->setKoordinat($value_geojson->getKoordinat());
                    $geojson_baru->setLokasiKe($value_geojson->getLokasiKe());
                    $geojson_baru->save();

                    //delete geojsonlokasi rev1
                    $value_geojson->setStatusHapus(true);
                    $value_geojson->save();
                }

                //update prioritas
                $total_aktif = 0;
                $query = "select max(prioritas) as total "
                        . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                        . "where status_hapus = false and status_waiting = 0 "
                        . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kode_kegiatan . "'";
                $stmt = $con->prepareStatement($query);
                $rs = $stmt->executeQuery();
                while ($rs->next()) {
                    $total_aktif = $rs->getString('total');
                }
                for ($index = $prioritas; $index <= $total_aktif; $index++) {
                    $index_tambah_satu = $index + 1;
                    $c_prioritas = new Criteria();
                    $c_prioritas->add(WaitingListPUPeer::UNIT_ID, 'XXX' . $unit_id);
                    $c_prioritas->addAnd(WaitingListPUPeer::KEGIATAN_CODE, $kode_kegiatan);
                    $c_prioritas->addAnd(WaitingListPUPeer::PRIORITAS, $index);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
                    $c_prioritas->addAnd(WaitingListPUPeer::STATUS_WAITING, 0);
                    if ($waiting_prio = WaitingListPUPeer::doSelectOne($c_prioritas)) {
                        $waiting_prio->setPrioritas($index_tambah_satu);
                        $waiting_prio->save();
                    }
                }

                //update waitinglist pu
                $waiting->setSubtitle($rd->getSubtitle());
                $waiting->setKomponenId($rd->getKomponenId());
                $waiting->setKomponenName($rd->getKomponenName());
                $waiting->setKomponenRekening($rd->getRekeningCode());
                $waiting->setKomponenLokasi('(' . $lokasi_baru . ')');
                $waiting->setKomponenHargaAwal($rd->getKomponenHargaAwal());
                $waiting->setPajak($rd->getPajak());
                $waiting->setKomponenSatuan($rd->getSatuan());
                $waiting->setKoefisien($rd->getKeteranganKoefisien());
                $waiting->setVolume($rd->getVolume());
                $waiting->setTahunInput(sfConfig::get('app_tahun_default'));
                $waiting->setUpdatedAt($sekarang);
                $waiting->setKecamatan($rd->getLokasiKecamatan());
                $waiting->setKelurahan($rd->getLokasiKelurahan());
                $waiting->setIsMusrenbang($rd->getIsMusrenbang());
                $waiting->setNilaiAnggaran($rd->getNilaiAnggaran());
                $waiting->setKeterangan($keterangan);
                $waiting->setStatusHapus(false);
                $waiting->setStatusWaiting(0);
                $waiting->setKodeRka(null);
                $waiting->save();

                //update rincian detail
                $rd->setStatusHapus(true);
                $rd->save();

                budgetLogger::log('Mengembalikan komponen ' . $komponen_name . ' ' . $detail_name . ' ke waitinglist');
                $con->commit();
                $this->setFlash('berhasil', 'Telah berhasil memproses ' . $komponen_name . ' ' . $detail_name . ' kembali ke Waiting List');
                return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            } catch (Exception $ex) {
                $con->rollback();
                $this->setFlash('gagal', 'Gagal Karena ' . $ex->getMessage());
                return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kode_kegiatan);
            }
        }
    }

//irul 29sept2015 - hapusLokasi
    public function executeHapusLokasi() {
        $unit_id = $this->getRequestParameter('unit');
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $detail_no = $this->getRequestParameter('no');

        $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

        $con = Propel::getConnection();
        $con->begin();
        try {
            $sekarang = date('Y-m-d H:i:s');
            $sql = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                    . "set status_hapus = TRUE, last_edit_time = '$sekarang' "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no ";
            $stmt = $con->prepareStatement($sql);
            $stmt->executeQuery();

            $con->commit();
            $this->setFlash('berhasil', 'Berhasil menghapus Lokasi untuk kode ' . $kode_rka);
            budgetLogger::log('menghapus GIS untuk kode RKA  ' . $kode_rka);

            historyUserLog::hapus_lokasi($unit_id, $kegiatan_code, $detail_no);
        } catch (Exception $exc) {
            $con->rollback();
            $this->setFlash('gagal', 'Hapus lokasi gagal karena ' . $exc->getMessage());
        }
        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

//irul 29sept2015 - hapusLokasi    

    public function executePindahKegiatan() {
        $act = $this->getRequestParameter('act');
        $detail_no = $this->getRequestParameter('detail_no');
        $sub_id = $this->getRequestParameter('id');
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $kegiatanTujuan = $this->getRequestParameter('kegTujuan');

        if ($kegiatanTujuan == '0003') {
            $subtitle = 'Monitoring Pembangunan/Rehab Jalan';
        } else if ($kegiatanTujuan == '0011') {
            $subtitle = 'Monitoring Pembangunan/Rehab Pematusan';
        } else if ($kegiatanTujuan == '0022') {
            $subtitle = 'Monitoring Pembangunan/Rehab Jembatan';
        } else if ($kegiatanTujuan == '0023') {
            $subtitle = 'Monitoring Pembangunan/Rehab Jaringan Air Bersih';
        } else if ($kegiatanTujuan == '0033') {
            $subtitle = 'Monitoring Pembangunan Prasarana Pematusan';
        } else if ($kegiatanTujuan == '0034') {
            $subtitle = 'Monitoring Pembangunan Jembatan';
        }


        $c = new Criteria();
        $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
        //$c->add(RincianDetailPeer::KODE_SUB,$sub_id);

        $rincian_detail = RincianDetailPeer::doSelectOne($c);
        //print_r($unit_id.' kd '.$kegiatan_code.' dn '.$detail_no.' sub '.$sub_id.' end');
        //var_dump($rincian_detail);exit;
        if ($rincian_detail) {
            $rd_function = new RincianDetail();
            $newDetailNo = $rd_function->getMaxDetailNo($unit_id, $kegiatanTujuan);
            $con = Propel::getConnection();
            $con->begin();
            try {
                $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id,last_update_time,  tahap_edit,status_hapus,tahun)
                                values
                                ('" . $kegiatanTujuan . "', '" . $rincian_detail->getTipe() . "', " . $newDetailNo . ", '" . $rincian_detail->getRekeningCode() . "', '" . $rincian_detail->getKomponenId() . "', '" . $rincian_detail->getDetailName() . "', 
                                    " . $rincian_detail->getVolume() . ", '" . $rincian_detail->getKeteranganKoefisien() . "', '" . $rincian_detail->getSubtitle() . "',
                                    " . $rincian_detail->getKomponenHarga() . ", " . $rincian_detail->getKomponenHargaAwal() . ",'" . $rincian_detail->getKomponenName() . "', '" . $rincian_detail->getSatuan() . "', 
                                        " . $rincian_detail->getPajak() . ",'" . $rincian_detail->getUnitId() . "','now()', '" . sfConfig::get('app_tahap_edit') . "',
                                        'false','" . sfConfig::get('app_tahun_default') . "')";

                //print_r($query);exit;
                $stmt = $con->prepareStatement($query);
                $stmt->executeQuery();
                $this->setFlash('berhasil', 'Komponen sudah berhasil dipindah dari kegiatan ' . $kegiatan_code . ' ke kegiatan ' . $kegiatanTujuan);
                $con->commit();
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }

            $rincian_detail->setStatusHapus('TRUE');
            $rincian_detail->save();
        }

        budgetLogger::log('Memindah komponen dari  ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
    }

    public function executeCatatanGender() {
        $this->sub_id = $this->getRequestParameter('sub_id');
        $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->catatanSubtitle = $this->getRequestParameter('catatanSubtitle');
    }

    public function executeCatatanGenderDetail() {
        $catatan = $this->getRequestParameter('catatanGender');
        $sub_id = $this->getRequestParameter('sub_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $unit_id = $this->getRequestParameter('unit_id');

        $con = Propel::getConnection();
        $con->begin();
        try {
            $catatanGender = SubtitleIndikatorPeer::retrieveByPK($sub_id);
            $catatanGender->setCatatan($catatan);
            $catatanGender->save($con);
            $this->setFlash('berhasil', 'Catatan Gender sudah berhasil di ubah');
            $con->commit();
        } catch (Exception $e) {
            $this->setFlash('gagal', 'Catatan gagal di ubah karena ' . $e);
            $con->rollback();
        }
        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeSetPrioritas() {
        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $prioritas = $this->getRequestParameter('prioritas');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $unit_id = $this->getRequestParameter('unit_id');
            $sub_id = $this->getRequestParameter('sub_id');
            //print_r($prioritas.' kode '.$kode_kegiatan.' unit '.$unit_id.' sub id '.$sub_id);
            $c_cekPrioritas = new Criteria();
            $c_cekPrioritas->addAsColumn('prioritas', SubtitleIndikatorPeer::PRIORITAS);
            //$c_cekPrioritas->addSelectColumn(SubtitleIndikatorPeer::PRIORITAS);
            $c_cekPrioritas->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c_cekPrioritas->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c_cekPrioritas->add(SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
            $c_cekPrioritas->add(SubtitleIndikatorPeer::PRIORITAS, $prioritas);
            $rs_cekPrioritas = SubtitleIndikatorPeer::doCount($c_cekPrioritas);
            //var_dump($rs_cekPrioritas);exit;
            if ($rs_cekPrioritas == '1') {
                $this->setFlash('gagal', 'Prioritas gagal dirubah, karena sudah ada prioritas yang sama pada kegiatan ini');
            } else {
                $con = Propel::getConnection();
                $con->begin();
                try {
                    $subIndikator = SubtitleIndikatorPeer::retrieveByPK($sub_id);
                    $subIndikator->setPrioritas($prioritas);
                    $subIndikator->save($con);
                    $this->setFlash('berhasil', 'Prioritas sudah berhasil dirubah');
                    $con->commit();

                    historyUserLog::set_prioritas_subtitle($unit_id, $kode_kegiatan, $sub_id, $prioritas);
                } catch (Exception $e) {
                    $this->setFlash('gagal', 'Prioritas gagal dirubah karena ' . $e);
                    $con->rollback();
                }
            }
            $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
        }
    }

    public function executeUbahProfil() {

        $namaDinas = $this->getUser()->getNamaUser();
        $username = $this->getUser()->getNamaLogin();
        $this->unit_id = $this->getRequestParameter('unit_id');

        echo $this->getRequest()->getMethod();

        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            $namaUser = $this->getRequestParameter('nama');
            $nip = $this->getRequestParameter('nip');
            $email = $this->getRequestParameter('email');
            $telepon = $this->getRequestParameter('telepon');
            $jenisKelamin = $this->getRequestParameter('jenisKelamin');
            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $username);
            $rs_user = MasterUserV2Peer::doSelectOne($c_user);
            if ($rs_user) {
                $con = Propel::getConnection();
                $con->begin();
                try {
                    $newSetting = MasterUserV2Peer::retrieveByPK($username);
                    $newSetting->setUserName($namaUser);
                    $newSetting->setNip($nip);
                    $newSetting->setEmail($email);
                    $newSetting->setTelepon($telepon);
                    $newSetting->setJenisKelamin($jenisKelamin);
                    $newSetting->save($con);
                    budgetLogger::log('Username ' . $username . ' mengganti profil pada e-budgeting');
                    $this->setFlash('berhasil', 'Profil sudah berhasil dirubah');
                    $con->commit();
                } catch (Exception $e) {
                    $this->setFlash('gagal', 'Profil gagal dirubah karena ' . $e);
                    $con->rollback();
                }
            }
        }

        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
    }

    public function handleErrorUbahProfil() {
        return sfView::SUCCESS;
    }

    public function executeHapusHeaderPekerjaans() {
        if ($this->getRequestParameter('no')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".rincian_detail set sub='', kode_sub='' where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
                // print_r($sql3);exit;
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();
                budgetLogger::log('Menghapus Header dari Unit=' . $unit_id . ' dan kegiatan=' . $kegiatan_code . ' dan kode_sub=' . $kode_sub);
                $con->commit();
                $this->setFlash('berhasil', "Header Sudah Berhasil Dihapus");
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            } catch (Exception $e) {
                $this->setFlash('gagal', "Gagal karena " . $e->getMessage());
                $con->rollback();
            }
        }
    }

    public function executeEula() {
        $this->unit_id = $this->getRequestParameter('unit_id');
        if ($this->getRequestParameter('act') == 'simpan') {
            //print_r($this->getRequestParameter('unit_id').' '. $this->getRequestParameter('eula').' '. $this->getRequestParameter('eula_tolak'));exit;
            if ($this->getRequestParameter('eula_tolak') == 'Tidak Setuju') {
                return $this->redirect('login/logoutDinas');
            } else if ($this->getRequestParameter('eula') == 'Setuju') {
                return $this->redirect('dinas/list?unit_id=' . $this->getRequestParameter('unit_id'));
            }
        }
        $this->setLayout('layouteula');
    }

    public function executeAjaxEditHeader() {
        $kode_sub = $this->getRequestParameter('kodeSub');
        $unit_id = $this->getRequestParameter('unitId');
        $kode_kegiatan = $this->getRequestParameter('kegiatanCode');
        if ($this->getRequestParameter('act') == 'editHeader') {
            $c_rkam = new Criteria();
            $c_rkam->add(RkaMemberPeer::KODE_SUB, $kode_sub);
            $c_rkam->add(RkaMemberPeer::UNIT_ID, $unit_id);
            $c_rkam->add(RkaMemberPeer::KEGIATAN_CODE, $kode_kegiatan);
            $rkam = RkaMemberPeer::doSelectOne($c_rkam);

            if ($rkam) {
                $tamp = $rkam->getKomponenName() . ' ' . $rkam->getDetailName();
                $this->rkam = $rkam->getKodeSub();
            }
            $this->nama = $tamp;
            $this->kodeSub = $kode_sub;
            $this->unit_id = $unit_id;
            $this->kegiatan_code = $kode_kegiatan;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $header = $this->getRequestParameter('editHeader_' . $kode_sub);

            $con = Propel::getConnection();
            $con->begin();
            try {
                $sql2 = "update " . sfConfig::get('app_default_schema') . ".rka_member set komponen_name='$header', detail_name='' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                $stmt = $con->prepareStatement($sql2);
                $stmt->executeQuery();


                $sql3 = "update  " . sfConfig::get('app_default_schema') . ".rincian_detail set sub='$header' where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and kode_sub='$kode_sub'";
                //print_r($sql3);exit;
                $stmt2 = $con->prepareStatement($sql3);
                $stmt2->executeQuery();
                $con->commit();
                $this->setFlash('berhasil', "Nama Header sudah berhasil di ubah");
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
            } catch (Exception $e) {
                echo $e;
                $con->rollback();
            }
        }
    }

    public function executeAjaxKomponenPenyusun() {
        // print_r($this->getRequestParameter('kegiatan_code'));exit;
    }

    public function executeTampillaporan() {
        if ($this->getRequestParameter('unit_id')) {
            $this->executeBanding();
            $this->setTemplate('banding');
        }
    }

    public function executeAmbilKegiatan() {//dj
        try {
            $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->user_id = $this->getRequestParameter('user_id');
            $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set user_id = '$user_id' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set user_id = '$user_id' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan'";
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' diambil oleh ' . $user_id);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan berhasil di ambil oleh ( $user_id )");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('dinas/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        // echo $unit_id.''.$kode_kegiatan;
    }

    public function executeLepasKegiatan() {//dj
        try {
            $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->user_id = $this->getRequestParameter('user_id');
            $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan set user_id = '' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and user_id='$user_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $query = "update " . sfConfig::get('app_default_schema') . ".dinas_master_kegiatan set user_id = '' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and user_id='$user_id'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' dilepas  oleh ' . $user_id);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan berhasil dilepas oleh ( $user_id )");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('dinas/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        // echo $unit_id.''.$kode_kegiatan;
    }

    public function executePersonil() { //dj
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = $this->subtitle = $this->getRequestParameter('subtitle');
        $act = $this->act = $this->getRequestParameter('act');

        if ($act == 'tambah') {

            $c = new Criteria();
            $c->add(PersonilRkaPeer::UNIT_ID, $unit_id);
            $c->add(PersonilRkaPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(PersonilRkaPeer::SUBTITLE, $subtitle);
            $x = PersonilRkaPeer::doSelect($c);
            $this->x = $x;
        }
    }

    public function executeTambahPersonilRka() {
        //dj
        $unit_id = $this->unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $subtitle = $this->subtitle = $this->getRequestParameter('subtitle');
        $act = $this->act = $this->getRequestParameter('act');
        $nip = $this->nip = $this->getRequestParameter('nip');
        $nama = $this->nama = $this->getRequestParameter('nama');
        $buton = $this->buton = $this->getRequestParameter('addPersonil');
        $pegawai = $this->pegawai = $this->getRequestParameter('pegawai');

        $hit = count($pegawai);

        if ($buton == 'save') {
            foreach ($pegawai as $peg) {
                $d = new Criteria();
                $d->add(PegawaiPeer::NIP, $peg); //$peg adalah array yang isinya NIP
                $d->addAscendingOrderByColumn(PegawaiPeer::NIP);
                $x = PegawaiPeer::doSelect($d);
                foreach ($x as $staf) {
                    $namabaru = $staf->getNama();
                    $nipbaru = $staf->getNip();
                    $unitIdbaru = $staf->getUnitId();


                    $query = "insert into " . sfConfig::get('app_default_schema') . ".personil_rka (nip,nama,unit_id,kegiatan_code,subtitle) values('$nipbaru','$namabaru','$unitIdbaru','$kegiatan_code','$subtitle');";
                    //echo $query;
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    $this->setFlash('berhasil', "Personil  telah berhasi di tambahkan ke subtitle = $subtitle");
                }
            }
            return $this->redirect('dinas/edit?unit_id=' . $unitIdbaru . '&kode_kegiatan=' . $kegiatan_code);
        }
        if ($act == 'deletePersonil') {
            $query = "delete from " . sfConfig::get('app_default_schema') . ".personil_rka where nip='$nip' and nama='$nama' and unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();
            $this->setFlash('berhasil', "Personil $nama telah berhasi di dihapus dari subtitle = $subtitle");
            return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executePrintBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'x.xx') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
            //print_r($kode_program22);
            //exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
                        from " . sfConfig::get('app_default_schema') . ".master_program kp
                        where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
                        from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
                        where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
                        from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
                        where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail rd
                                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
                                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }

            $this->setLayout('kosong');
            $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
        }
    }

    public function executeBanding() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->kode_kegiatan = $kode_kegiatan;
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kode_kegiatan);
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->kode_program22 = substr($master_kegiatan->getKodeProgram2(), 5, 2);
            if (substr($master_kegiatan->getKodeProgram2(), 0, 4) == 'x.xx') {
                $this->kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 4);
            } else {
                $this->kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 4);
            }
            //print_r($kode_program22);
            //exit;
            $e = new Criteria();
            $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
            $es = UnitKerjaPeer::doSelectOne($e);
            if ($es) {
                $this->kode_permen = $es->getKodePermen();
            }


            $this->kode = $this->kode_urusan . '.' . $this->kode_permen . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->kode_kegiatan2 = $this->kode_urusan . '.' . $this->kode_program . '.' . $this->kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
            $this->nama_kegiatan = $master_kegiatan->getNamaKegiatan();
            $u = new Criteria();
            $u->add(MasterUrusanPeer::KODE_URUSAN, $this->kode_urusan);
            $us = MasterUrusanPeer::doSelectOne($u);
            if ($us) {
                $this->nama_urusan = $us->getNamaUrusan();
            }

            $this->kode_program = $master_kegiatan->getKodeProgram();
            $this->unit_id = $unit_id;
            $u = new Criteria();
            $u->add(UnitKerjaPeer::UNIT_ID, $unit_id);
            $us = UnitKerjaPeer::doSelectOne($u);
            if ($us) {
                $this->unit_kerja = $us->getUnitName();
            }

            $query = "select *
                        from " . sfConfig::get('app_default_schema') . ".master_program kp
                        where kp.kode_program='" . $this->kode_program . "' and kp.kode_tujuan ilike '" . $master_kegiatan->getKodeTujuan() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->kode_program1 = $rs1->getString('kode_program');
                $this->nama_program = $rs1->getString('nama_program');
            }

            $query = "select *
                        from " . sfConfig::get('app_default_schema') . ".master_program2 kp2
                        where kp2.kode_program='" . $this->kode_program . "' and kp2.kode_program2 ilike '" . $this->kode_program22 . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_program22 = $rs1->getString('nama_program2');
            }

            $query = "select *
                        from " . sfConfig::get('app_default_schema') . ".master_sasaran kp2
                        where kp2.kode_sasaran='" . $master_kegiatan->getKodeSasaran() . "'";
            //print_r($query);exit;
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->nama_sasaran = $rs1->getString('nama_sasaran');
                $this->kode_sasaran = $rs1->getString('kode_sasaran');
            }

            //$query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail_bp rd
            //               where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            //$query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".prev_rincian_detail rd
            //		where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail rd
                        		where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_semula = $rs1->getString('nilai');
            }

            //bisma $query="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail rd
            //        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            $query = "select sum(rd.nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail rd
                                        where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rd.status_hapus=FALSE";
            //$query2="select sum(rd.volume * rd.komponen_harga_awal * (100 + rd.pajak) / 100) as nilai from ". sfConfig::get('app_default_schema') .".rincian_detail rd
            //                where unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and tahun='". sfConfig::get('app_tahun_default') ."' and status_hapus=FALSE";
            $con = Propel::getConnection();

            $stmt = $con->prepareStatement($query);
            $rs1 = $stmt->executeQuery();
            while ($rs1->next()) {
                $this->total_sekarang = $rs1->getString('nilai');
            }
        }
    }

    public function executeUbahPass() {
        $namaDinas = $this->getUser()->getNamaUser();
        $username = $this->getUser()->getNamaLogin();

        if ($this->getRequest()->getMethod() == sfRequest::POST) {
            //$namaUser=$this->getRequestParameter('nama');
            //$nip=$this->getRequestParameter('nip');
            $pass_lama = $this->getRequestParameter('pass_lama');
            $pass_baru = $this->getRequestParameter('pass_baru');
            $ulang_pass_baru = $this->getRequestParameter('ulang_pass_baru');
            $md5_pass_lama = md5($pass_lama);
            $md5_pass_baru = md5($pass_baru);

            $c_user = new Criteria();
            $c_user->add(MasterUserV2Peer::USER_ID, $username);
            $c_user->add(MasterUserV2Peer::USER_PASSWORD, $md5_pass_lama);
            $rs_user = MasterUserV2Peer::doSelectOne($c_user);
            if ($rs_user) {
                if ($pass_baru == $ulang_pass_baru) {
                    $con = Propel::getConnection();
                    $con->begin();
                    try {
                        $newSetting = MasterUserV2Peer::retrieveByPK($username);
                        //$newSetting->setUserName($namaUser);
                        //$newSetting->setNip($nip);
                        $newSetting->setUserPassword($md5_pass_baru);
                        $newSetting->save($con);
                        budgetLogger::log('Username  ' . $username . ' telah mengganti password pada e-budgeting');
                        $this->setFlash('berhasil', 'Password sudah berhasil dirubah');
                        $con->commit();
                    } catch (Exception $e) {
                        $this->setFlash('gagal', 'Password gagal dirubah karena ' . $e);
                        $con->rollback();
                    }
                }
            }
        }
        $settingPass = new Criteria();
        $settingPass->add(MasterUserV2Peer::USER_ID, $username);
        $this->profil = $rs_settingPass = MasterUserV2Peer::doSelectOne($settingPass);
    }

    public function handleErrorUbahPass() {
        return sfView::SUCCESS;
    }

    public function executeSetting() {
        $dinas = $this->getRequest()->getCookie('nama');
        $c = new Criteria();
        $c->add(UnitKerjaPeer::UNIT_ID, $dinas);
        $x = UnitKerjaPeer::doSelectOne($c);
        if ($x) {
            $this->kepala_nama = $x->getKepalaNama();
            $this->kepala_pangkat = $x->getKepalaPangkat();
            $this->kepala_nip = $x->getKepalaNip();
        }
        $this->set = 3;
        return sfView::SUCCESS;
    }

    public function executeSimpanset() {
        $dinas = $this->getRequest()->getCookie('nama');
        $nama_kepala = $this->getRequestParameter('kepala_nama');
        $kepala_pangkat = $this->getRequestParameter('kepala_pangkat');
        $kepala_nip = $this->getRequestParameter('kepala_nip');

        $query = "update unit_kerja set kepala_nama='" . $nama_kepala . "', kepala_pangkat='" . $kepala_pangkat . "', kepala_nip='" . $kepala_nip . "' where unit_id='" . $dinas . "'";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        budgetLogger::log('Username  ' . $user_id . ' telah mengganti setting pada e-budgeting');

        $this->setFlash('berhasil', 'Data Profil SKPD telah berhasil diganti.');
        $stmt->executeQuery();
        return $this->redirect('dinas/setting');
    }

    public function executeSabomedit() {
        $coded = $this->getRequestParameter('coded', '');
        return sfView::SUCCESS;
    }

    public function executeSabomlist() {

        $this->processSortsabom();

        $this->processFilterssabom();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 20);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'OM');

        $this->addSortCriteriasabom($c);
        $this->addFiltersCriteriasabom($c);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilterssabom() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortsabom() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriasabom($c) {
        $cek = $this->getRequestParameter('search_option');
        if ($cek == 'shsd_id') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addSortCriteriasabom($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeDetailsublist() {
        $coded = $this->getRequestParameter('coded', '');
        if ($coded != '') {
            $this->setLayout('kosong');
        }
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
        //$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
            sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
        from 
            " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
        where 
            m.sub_kegiatan_id = '" . $sub_id . "'";
        //print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
        SELECT 
        rekening.rekening_code,
        detail.detail_name as detail_name,
        detail.komponen_name,
        detail.komponen_name || ' ' || detail.detail_name as detail_name2,
        detail.komponen_harga_awal as detail_harga,
        detail.pajak,
        detail.komponen_id,
        detail.subtitle ,
        detail_no,koefisien,param,
        detail.satuan as detail_satuan,
        replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
        detail.volume * detail.komponen_harga_awal as hasil,
        (detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,


        (SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
        as x,
        (SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
        as bahaya,
        substring(kb.belanja_name,8) as belanja_name

        FROM 
        " . sfConfig::get('app_default_schema') . ".rekening rekening ,
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
        " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb

        WHERE 
        rekening.rekening_code = detail.rekening_code and
        detail.sub_kegiatan_id = '" . $sub_id . "' and 
        kb.belanja_id=rekening.belanja_id 

        ORDER BY 

        belanja_urutan,
        komponen_name

        ";
        //echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
        //$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
            //echo $r['param'];
            $input[$i] = $inputs;
            //print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
        //$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeSublist() {
        $this->processSortsub();

        $this->processFilterssub();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/sub_kegiatan/filters');

        // pager
        $this->pager = new sfPropelPager('SubKegiatan', 25);
        $c = new Criteria();
        $c->add(SubKegiatanPeer::STATUS, 'Close');
        $this->addSortCriteriasub($c);
        $this->addFiltersCriteriasub($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processSortsub() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/sub_kegiatan/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/sub_kegiatan/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            
        }
    }

    protected function addFiltersCriteriasub($c) {
        if (isset($this->filters['sub_kegiatan_id_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_id']) && $this->filters['sub_kegiatan_id'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_id'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['sub_kegiatan_name_is_empty'])) {
            $criterion = $c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, '');
            $criterion->addOr($c->getNewCriterion(SubKegiatanPeer::SUB_KEGIATAN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['sub_kegiatan_name']) && $this->filters['sub_kegiatan_name'] !== '') {
            $kata = '%' . $this->filters['sub_kegiatan_name'] . '%';
            $c->add(SubKegiatanPeer::SUB_KEGIATAN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasub($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/sub_kegiatan/sort')) {
            $sort_column = SubKegiatanPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/sub_kegiatan/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    protected function processFilterssub() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/sub_kegiatan/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/sub_kegiatan/filters');
        }
    }

    public function executeSabfisikedit() {
        $coded = $this->getRequestParameter('coded', '');

        return sfView::SUCCESS;
    }

    public function executeSablist() {
        $this->processSortsab();

        $this->processFilterssab();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK');
        $c->add(KomponenPeer::KOMPONEN_SHOW, 'TRUE');
        $this->addSortCriteriasab($c);
        $this->addFiltersCriteriasab($c);
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFilterssab() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortsab() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriasab($c) {
        if (isset($this->filters['komponen_id_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
            $kata = '%' . $this->filters['komponen_id'] . '%';
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_name_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '') {
            $kata = '%' . $this->filters['komponen_name'] . '%';
            $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriasab($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeHspkedit() {
        return sfView::SUCCESS;
    }

    public function executeHspklist() {
        $this->processSorthspk();

        $this->processFiltershspk();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $this->addSortCriteriahspk($c);
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'HSPK');
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addFiltersCriteriahspk($c);


        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltershspk() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSorthspk() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriahspk($c) {
        if (isset($this->filters['komponen_id_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
            $kata = '%' . $this->filters['komponen_id'] . '%';
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->add(KomponenPeer::KOMPONEN_ID, $kata, Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_name_is_empty'])) {
            $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
            $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['komponen_name']) && $this->filters['komponen_name'] !== '') {
            $kata = '%' . $this->filters['komponen_name'] . '%';
            $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
        }
        if (isset($this->filters['komponen_barang']) && $this->filters['komponen_barang'] !== '0') {
            $kode = trim($this->filters['komponen_barang'] . '%');
            $c->add(KomponenPeer::KOMPONEN_ID, strtr($kode, '*', '%'), Criteria::ILIKE);
        }
    }

    protected function addSortCriteriahspk($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeShsdlist() {
        $this->processSortshsd();

        $this->processFiltersshsd();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/shsd/filters');

        // pager
        $this->pager = new sfPropelPager('Shsd', 20);
        $c = new Criteria();
        //$this->addSortCriteria($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');
        //print_r($dinas_array['nama']);exit;
        if (($dinas_array['nama'] != '0305') and ( $dinas_array['nama'] != '0600') and ( $dinas_array['nama'] != '0900')) {
            //print_r($dinas_array['nama']);exit;
            $c->add(ShsdPeer::SHSD_ID, '23.01.01.05.01.08', Criteria::NOT_EQUAL);
        }
        $this->addFiltersShsdCriteria($c);
        $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersshsd() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/shsd/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/shsd/filters');
        }
    }

    protected function processSortshsd() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/shsd/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/shsd/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/shsd/sort')) {
            
        }
    }

    protected function addFiltersShsdCriteria($c) {
        if (isset($this->filters['shsd_id_is_empty'])) {
            $criterion = $c->getNewCriterion(ShsdPeer::SHSD_ID, '');
            $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_ID, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['shsd_id']) && $this->filters['shsd_id'] !== '') {
            $kata = '%' . $this->filters['shsd_id'] . '%';
            $c->add(ShsdPeer::SHSD_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        }
        if (isset($this->filters['shsd_name_is_empty'])) {
            $criterion = $c->getNewCriterion(ShsdPeer::SHSD_NAME, '');
            $criterion->addOr($c->getNewCriterion(ShsdPeer::SHSD_NAME, null, Criteria::ISNULL));
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
            $c->add($criterion);
        } else if (isset($this->filters['shsd_name']) && $this->filters['shsd_name'] !== '') {
            $kata = '%' . $this->filters['shsd_name'] . '%';
            $c->add(ShsdPeer::SHSD_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            $c->addAscendingOrderByColumn(ShsdPeer::SHSD_ID);
        }
    }

    public function executeRekeningList() {
        $this->processFiltersrekening();
        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/rekening/filters');

        $this->pager = new sfPropelPager('Rekening', 25);
        $c = new Criteria();
        $c->addAscendingOrderByColumn(RekeningPeer::REKENING_CODE);
        $this->addFiltersCriteriarekeningList($c);

        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersrekening() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/rekening/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/rekening/filters');
        }
    }

    protected function addFiltersCriteriarekeningList($c) {
        $cek = $this->getRequestParameter('search_option');

        if ($cek == 'rekening_code') {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_CODE, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_CODE, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_CODE, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } elseif ($cek == 'rekening_name') {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['rekening_code_is_empty'])) {
                $criterion = $c->getNewCriterion(RekeningPeer::REKENING_NAME, '');
                $criterion->addOr($c->getNewCriterion(RekeningPeer::REKENING_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['rekening_code']) && $this->filters['rekening_code'] !== '') {
                $kata = '%' . $this->filters['rekening_code'] . '%';
                $c->add(RekeningPeer::REKENING_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    public function executeShsdlockedlist() {
        $this->processSortshsdlocked();

        $this->processFiltersshsdlocked();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/komponen/filters');

        // pager
        $this->pager = new sfPropelPager('Komponen', 25);
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_TIPE, 'SHSD');
        $c->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
        $this->addSortCriteriashsdlocked($c);
        $this->addFiltersCriteriashsdlocked($c);
        $dinas_array = $this->getUser()->getAttributeHolder()->getAll('dinas');

        //print_r($dinas_array['nama']);exit;
        if (($dinas_array['nama'] <> '0305') and ( $dinas_array['nama'] <> '0600') and ( $dinas_array['nama'] <> '0900')) {
            $c->add(KomponenPeer::KOMPONEN_ID, '23.01.01.05.01.08', Criteria::NOT_EQUAL);
        }
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function processFiltersshsdlocked() {
        if ($this->getRequest()->hasParameter('filter')) {
            $filters = $this->getRequestParameter('filters');

            $this->getUser()->getAttributeHolder()->removeNamespace('sf_admin/komponen/filters');
            $this->getUser()->getAttributeHolder()->add($filters, 'sf_admin/komponen/filters');
        }
    }

    protected function processSortshsdlocked() {
        if ($this->getRequestParameter('sort')) {
            $this->getUser()->setAttribute('sort', $this->getRequestParameter('sort'), 'sf_admin/komponen/sort');
            $this->getUser()->setAttribute('type', $this->getRequestParameter('type', 'asc'), 'sf_admin/komponen/sort');
        }

        if (!$this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            
        }
    }

    protected function addFiltersCriteriashsdlocked($c) {
        $cek = $this->getRequestParameter('search_option');
        //print_r($cek);exit;
        if ($cek == 'shsd_id') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_ID, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_ID, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_ID, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } elseif ($cek == 'shsd_name') {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        } else {
            if (isset($this->filters['komponen_id_is_empty'])) {
                $criterion = $c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, '');
                $criterion->addOr($c->getNewCriterion(KomponenPeer::KOMPONEN_NAME, null, Criteria::ISNULL));
                $c->add($criterion);
            } else if (isset($this->filters['komponen_id']) && $this->filters['komponen_id'] !== '') {
                $kata = '%' . $this->filters['komponen_id'] . '%';
                $c->add(KomponenPeer::KOMPONEN_NAME, strtr($kata, '*', '%'), Criteria::ILIKE);
            }
        }
    }

    protected function addSortCriteriashsdlocked($c) {
        if ($sort_column = $this->getUser()->getAttribute('sort', null, 'sf_admin/komponen/sort')) {
            $sort_column = KomponenPeer::translateFieldName($sort_column, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);
            if ($this->getUser()->getAttribute('type', null, 'sf_admin/komponen/sort') == 'asc') {
                $c->addAscendingOrderByColumn($sort_column);
            } else {
                $c->addDescendingOrderByColumn($sort_column);
            }
        }
    }

    public function executeLihatSsh() { //fungsi mbulet sesuai mas punya mas bayu. *maafkan*
        $id = $this->getRequestParameter('komponen_id');
        $c = new Criteria();
        $c->add(KomponenPeer::KOMPONEN_ID, $id);
        $ssh = $this->cs = KomponenPeer::doSelectOne($c);

        $shsd_id = $ssh->getShsdId();
        $c_spek = new Criteria();
        $c_spek->add(ShsdPeer::SHSD_ID, $shsd_id);
        $this->spek = SHsdPeer::doSelectOne($c_spek);
        $this->setLayout('popup');
    }

    public function executeLihatSAB() {
        //$coded='';
        $coded = $this->getRequestParameter('coded', '');

        $komponen_id = $this->getRequestParameter('komponen_id');
        //print_r(substr(trim($komponen_id),0,2));exit;
        if (substr(trim($komponen_id), 0, 2) <= 28) {
            return $this->redirect('dinas/lihatsabfisik?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) < 30) {
            return $this->redirect('dinas/lihatsublist?id=' . $komponen_id . '&coded=' . $coded);
        } elseif (substr($komponen_id, 0, 2) >= 30) {

            return $this->redirect('dinas/lihatsabom?id=' . $komponen_id . '&coded=' . $coded);
        }
        $this->setLayout('popup');
    }

    public function executeLihatsabfisik() {
        $coded = $this->getRequestParameter('coded', '');
        $this->komponen_id = $this->getRequestParameter('komponen_id');
        $this->setLayout('popup');
    }

    public function executeLihatsublist() {
        $coded = $this->getRequestParameter('coded', '');
        $sub_id = $this->getRequestParameter('id');
        $input = array();
        $c = new Criteria();
        $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sub_id);
        //$c -> add (SubKegiatanPeer::STATUS, 'Close') ;
        $cs = SubKegiatanPeer::doSelectOne($c);
        if ($cs) {
            $this->sub_kegiatan_id = $cs->getSubKegiatanId();
            $this->sub_kegiatan_name = $cs->getSubKegiatanName();
            $this->param = $cs->getParam();
            $this->satuan = $cs->getSatuan();
        }

        $d = new Criteria();
        $d->add(KomponenMemberPeer::KOMPONEN_ID, $sub_id);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::SUBTITLE);
        $d->addAscendingOrderByColumn(KomponenMemberPeer::TIPE);
        $ds = KomponenMemberPeer::doSelect($d);
        $this->komponen_penyusun = $ds;
        $total = 0;
        foreach ($ds as $x) {
            $total = $total + $x->getMemberTotal();
        }
        $this->total_penyusun = $total;
        $sql = "select 
        sum(m.volume*m.komponen_harga_awal*(100+m.pajak)/100) as nilai_satuan
        from 
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member m
        where 
        m.sub_kegiatan_id = '" . $sub_id . "'";
        //print_r($sql);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $nilai_satuan = $rs->getString('nilai_satuan');
        }
        $this->nilai_satuan = $nilai_satuan;

        $query = "
        SELECT 
        rekening.rekening_code,
        detail.detail_name as detail_name,
        detail.komponen_name,
        detail.komponen_name || ' ' || detail.detail_name as detail_name2,
        detail.komponen_harga_awal as detail_harga,
        detail.pajak,
        detail.komponen_id,
        detail.subtitle ,
        detail_no,koefisien,param,
        detail.satuan as detail_satuan,
        replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
        detail.volume * detail.komponen_harga_awal as hasil,
        (detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,


        (SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
        as x,
        (SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
        as bahaya,
        substring(kb.belanja_name,8) as belanja_name

        FROM 
        " . sfConfig::get('app_default_schema') . ".rekening rekening ,
        " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
        " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb

        WHERE 
        rekening.rekening_code = detail.rekening_code and
        detail.sub_kegiatan_id = '" . $sub_id . "' and 
        kb.belanja_id=rekening.belanja_id 

        ORDER BY 

        belanja_urutan,
        komponen_name

        ";
        //echo $query;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs = $stmt->executeQuery();
        $this->r = $rs;
        //$this->input = Array();
        $keterangan_koefisien = array();
        $detail_harga = array();
        $hasil_kali = array();
        $hasil = array();
        $detail_name = array();
        $detail_satuan = array();
        $pajak = array();
        $belanja_name = array();
        $this->keterangan_koefisien = Array();
        $this->detail_harga = Array();
        $this->hasil_kali = Array();
        $this->hasil = Array();
        $this->detail_name = array();
        $this->detail_satuan = array();
        $this->pajak = array();
        $this->belanja_name = array();
        $i = 0;
        while ($rs->next()) {
            $pars = explode("|", $rs->getString('param'));

            for ($j = 0; $j < count($pars); $j++) {
                if ($j == 0)
                    $inputs = $pars[$j];
                else
                    $inputs = $inputs . ',<BR>' . $pars[$j];
            }
            //echo $r['param'];
            $input[$i] = $inputs;
            //print_r($inputs);exit;

            $detail_name[$i] = $rs->getString('detail_name2');
            $detail_satuan[$i] = $rs->getString('detail_satuan');
            $pajak[$i] = $rs->getString('pajak');
            $belanja_name[$i] = $rs->getString('belanja_name');

            $keterangan_koefisien[$i] = $rs->getString('keterangan_koefisien');
            $nilai_satuan+=$rs->getString('hasil_kali');

            $detail_harga[$i] = number_format($rs->getString('detail_harga'), 0, ",", ".");
            $hasil_kali[$i] = number_format($rs->getString('hasil_kali'), 0, ",", ".");

            $hasil[$i] = number_format($rs->getString('hasil'), 0, ",", ".");
            $i = $i + 1;
        }
        $this->banyak = $i;
        //$this->nilai_satuan = $nilai_satuan;
        $this->input = $input;
        $this->keterangan_koefisien = $keterangan_koefisien;
        $this->detail_harga = $detail_harga;
        $this->hasil_kali = $hasil_kali;
        $this->hasil = $hasil;
        $this->detail_name = $detail_name;
        $this->detail_satuan = $detail_satuan;
        $this->pajak = $pajak;
        $this->belanja_name = $belanja_name;
        return sfView::SUCCESS;
    }

    public function executeLihatsabom() {
        $coded = $this->getRequestParameter('coded', '');
        //return sfView::SUCCESS;
        $this->setLayout('popup');
    }

    public function executeSimpanSubKegiatan() {
        $sub_kegiatan_id = $this->getRequestParameter('sub_kegiatan_id');
        $kodesub = $this->getRequestParameter('kodesub');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $unit_id = $this->getRequestParameter('unit_id');
        //djiebrats begin
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($sub_kegiatan_id);exit;
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            //$user->setAttribute('nama',$this->getRequestParameter('lokasi'),'lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan1'), 'lokasi');
            $user->setAttribute('id', $sub_kegiatan_id, 'lokasi');
            $user->setAttribute('kegiatan', $kegiatan_code, 'lokasi');
            $user->setAttribute('unit', $unit_id, 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('barusub', 'barusub', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        //djiebrats end
        if ($this->getRequestParameter('lokasipeta') == 'lokasi') {
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan'), 'lokasi');
            $user->setAttribute('id', $sub_kegiatan_id, 'lokasi');
            $user->setAttribute('kegiatan', $kegiatan_code, 'lokasi');
            $user->setAttribute('unit', $unit_id, 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('barusub', 'barusub', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simpansub') == 'simpan') {
            $rd_function = new RincianDetail();

            $jumlah = $this->getRequestParameter('jumlah_koef');
            $total = $this->getRequestParameter('total_sub_kegiatan');

            $perkalian_keoef = 1;
            for ($q = 0; $q < $jumlah; $q++) {
                $perkalian_keoef_baru = $this->getRequestParameter('param_' . $q);
                $perkalian_keoef = $perkalian_keoef * $perkalian_keoef_baru;
            }
//
//            echo $total . ' ' . $jumlah . ' ' . $perkalian_keoef;
//            exit();

            $array_buka_pagu_dinas_khusus = array('9999');
            $array_buka_pagu_kegiatan_khusus = array('');
            if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                $status_pagu_rincian = $rd_function->getBatasPaguPerDinasSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
            } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
            } else {
                if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                    $kecamatan_unit_id = substr($unit_id, 0, 2);
                    if (1 == 0) {
                        $status_pagu_rincian = 0;
                    } else {
                        if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                            //batas pagu per dinas
                            $status_pagu_rincian = $rd_function->getBatasPaguPerDinasSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
                        } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                            //batas pagu per kegiatan 
                            $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanSubKegiatan($unit_id, $kegiatan_code, $perkalian_keoef, $total);
                        }
                    }
                } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                    $status_pagu_rincian = 0;
                }
            }

            //menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.
//            $status_pagu_rincian = $rd_function->getNilaiPaguDanSelisih($unit_id);
            if ($status_pagu_rincian == '1') {
                $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
            //  else if ($status_pagu_rincian == '0') { 
            // $this->setFlash('berhasil', 'berhasil disimpan.');
            // return $this->redirect('dinas/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
            //end of fasilitas
            $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where kode_sub ilike 'RSUB%'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($sql);
            $rs = $stmt->executeQuery();
            while ($rs->next()) {
                $kodesub = $rs->getString('kode_sub');
            }
            $kode = substr($kodesub, 4, 5);
            $kode+=1;
            if ($kode < 10) {
                $kodesub = 'RSUB0000' . $kode;
            } elseif ($kode < 100) {
                $kodesub = 'RSUB000' . $kode;
            } elseif ($kode < 1000) {
                $kodesub = 'RSUB00' . $kode;
            } elseif ($kode < 10000) {
                $kodesub = 'RSUB0' . $kode;
            } elseif ($kode < 100000) {
                $kodesub = 'RSUB' . $kode;
            }

            if ($this->getRequestParameter('keterangan')) {
                $m_lokasi_ada = 'tidak ada';
                $lokasi = $this->getRequestParameter('keterangan');
                $query = "select (1) as ada from v_lokasi where nama ilike '$lokasi'";
                //print_r($query);exit;
                $con = Propel::getConnection(VLokasiPeer::DATABASE_NAME);
                $stmt = $con->prepareStatement($query);
                $rs_cek = $stmt->executeQuery();
                while ($rs_cek->next()) {
                    //print_r($m_lokasi_ada);
                    if ($rs_cek->getString('ada') == '1') {
                        $m_lokasi_ada = 'ada';
                    }
                }
            }
            //print_r($m_lokasi_ada);exit;

            if ($m_lokasi_ada == 'tidak ada') {
                $this->setFlash('error_lokasi', 'Lokasi yang dimasukkan tidak ada dalam G.I.S');
                return $this->redirect("dinas/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
            }

            if ($this->getRequestParameter('subtitle')) {
                $kode_sub = $this->getRequestParameter('subtitle');
                $c = new Criteria();
                $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
                $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                if ($rs_subtitle) {
                    $subtitle = $rs_subtitle->getSubtitle();
                }
            }

            $keter = $this->getRequestParameter('keterangan1') . ' ' . $this->getRequestParameter('keterangan');
            $detail_name = str_replace("'", " ", $keter);
            //$subtitle = $this->getRequestParameter('subtitle');
            $query = "SELECT * from " . sfConfig::get('app_default_schema') . ".sub_kegiatan where sub_kegiatan_id='" . $sub_kegiatan_id . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();

            while ($rs->next()) {
                $sub_kegiatan_name = $rs->getString('sub_kegiatan_name');
                $subtitle2 = $sub_kegiatan_name . " " . $keter;
                $param = $rs->getString('param');
                $satuan = $rs->getString('satuan');
                $pembagi = $rs->getString('pembagi');
                $new_subtitle = $sub_kegiatan_name . " " . $keter . " " . $subtitle;
                if (($sub_kegiatan_id != '') && ($new_subtitle != '')) {
                    $query = "select kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle='$subtitle' and from_sub_kegiatan='$sub_kegiatan_id' and detail_name='$detail_name'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_cek = $stmt->executeQuery();
                    while ($rs_cek->next()) {
                        if ($rs_cek->getString('kode_sub')) {
                            $kodesub = $rs_cek->getString('kode_sub');
                            $this->setFlash('error_lokasi', 'ASB Non Fisik dengan Subtitle dengan Kegiatan Sudah Ada. Mohon Diperhatikan Kembali');
                            return $this->redirect("dinas/pilihSubKegiatan?id=$sub_kegiatan_id&kode_kegiatan=$kegiatan_code&unit_id=$unit_id&sub=Pilih");
                        }
                    }
                }
            }
            $arrparamsub = explode("|", $param);
            $arr_pembagi = explode("|", $pembagi);
            $keterangan = '';
            $arr_input = '';
            $jumlah = count($arrparamsub);
            //$j=0;
            for ($i = 0; $i < $jumlah; $i++) {
                //print_r(str_replace(' ','',$arrparamsub[$i]).'pppppp<br>');
                //echo $this->getRequestParameter('param_'.str_replace(' ','',$arrparamsub[$i]));

                if ($this->getRequestParameter('param_' . $i) != '') {
                    if ($i == 0) {
                        $arr_input = $this->getRequestParameter('param_' . $i);
                        //print_r($arr_input).'<br>';
                    }
                    if ($i != 0) {
                        $arr_input = $arr_input . '|' . $this->getRequestParameter('param_' . $i);
                    }
                }
            }
            //print_r($arr_input);exit;
            $arrsatuansub = explode("|", $satuan);
            //$arr_input=implode("|",$inp);
            $inp = array();
            $inp = explode("|", $arr_input);
            $arr_pembagi = explode("|", $pembagi);
            $pembagi = 1;
            $ket_pembagi = '';
            //$pembagi='';
            for ($j = 0; $j < count($arrparamsub); $j++) {
                if ($j >= 0) {
                    if (isset($inp[$j])) {
                        if ($inp[$j] != 0)
                            $keterangan = $keterangan . '<tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                    }else {
                        if ((isset($arrparamsub[$j])) && (isset($arrsatuansub[$j])) && (isset($inp[$j]))) {
                            $keterangan = '<table><tr><td class="Font8v">' . $arrparamsub[$j] . '</td><td class="Font8v">' . $inp[$j] . ' ' . $arrsatuansub[$j] . '</td></tr>';
                        }
                    }
                }
                if (isset($arr_pembagi[$j])) {
                    if ($arr_pembagi[$j] == 't') {
                        if ($inp[$j] and $inp[$j] != 0)
                            $pembagi = $pembagi * $inp[$j];
                        $ket_pembagi = $ket_pembagi . "," . $arrparamsub[$j];
                    }
                }
            }
            $keterangan = $keterangan . '</table>';
            $ket_pembagi = substr($ket_pembagi, 1);

            $query = "INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter(unit_id,kegiatan_code,from_sub_kegiatan,sub_kegiatan_name,subtitle,detail_name,new_subtitle,param,keterangan,ket_pembagi,pembagi,kode_sub) VALUES(
        '" . $unit_id . "','" . $kegiatan_code . "','" . $sub_kegiatan_id . "','" . $sub_kegiatan_name . "','" . $subtitle . "','" . $keter . "', '" . $new_subtitle . "','" . $arr_input . "','" . $keterangan . "','" . $ket_pembagi . "'," . $pembagi . ",'" . $kodesub . "')";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            //echo $query.'<BR>';
            // budgetLogger::log($deskripsi);
            $stmt->executeQuery();

            $query = "SELECT *, satuan as satuan_asli from " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member where sub_kegiatan_id='" . $sub_kegiatan_id . "'";

            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $rs = $stmt->executeQuery();
            //$volume=1;
            while ($rs->next()) {
                if ($rs->getString('param') == '') {
                    $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    $volume = $rs->getString('volume');
                } else {
                    $arrparam = array();
                    $arrparam = explode("|", $rs->getString('param'));

                    $volume = $rs->getString('volume');

                    $keterangan_koefisien = '';
                    if ($rs->getString('keterangan_koefisien')) {
                        $keterangan_koefisien = $rs->getString('keterangan_koefisien');
                    }
                    for ($j = 0; $j < count($arrparam); $j++) {

                        for ($i = 0; $i < count($arrparamsub); $i++) {

                            if (str_replace(' ', '', $arrparam[$j]) == str_replace(' ', '', $arrparamsub[$i])) {

                                $volume = $volume * $inp[$i];

                                if ($keterangan_koefisien != '') {

                                    $keterangan_koefisien = $keterangan_koefisien . ' X ' . $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                } else {

                                    $keterangan_koefisien = $inp[$i] . ' ' . $arrsatuansub[$i] . '(Fix)';
                                }
                            }
                        }
                    }
                }


                if (stristr($keterangan_koefisien, "'")) {
                    $keterangan_koefisien = addslashes($keterangan_koefisien);
                    $detail_name = addslashes($rs->getString('detail_name'));
                }
                $query1 = "SELECT max(detail_no) AS maxno FROM " . sfConfig::get('app_default_schema') . ".rincian_detail 
                     WHERE kegiatan_code='" . $kegiatan_code . "'  and unit_id='" . $unit_id . "'";


                $con = Propel::getConnection();
                $stmt1 = $con->prepareStatement($query1);
                $rs1 = $stmt1->executeQuery();
                //$maxno=0;
                //echo $query1;exit; 
                while ($rs1->next()) {
                    $maxno = $rs1->getInt('maxno');
                    if (!$maxno) {
                        $maxno = 1;
                    } else {
                        $maxno = $maxno + 1;
                    }
                }
                $pajakawal = $rs->getInt('pajak');

                if (!$pajakawal) {
                    $pajakawal = 0;
                }

                $rekening_code = $rs->getString('rekening_code');
                $komponen_id = $rs->getString('komponen_id');
                $detail_name = $rs->getString('detail_name');
                $komponen_harga_awal = $rs->getString('komponen_harga_awal');
                $komp = new Criteria();
                $komp->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                $terpilih_komponen = KomponenPeer::doSelectOne($komp);
                if ($terpilih_komponen) {
                    $komponen_name = $terpilih_komponen->getKomponenName();
                }

                if ($this->getRequestParameter('subtitle')) {
                    $kode_sub = $this->getRequestParameter('subtitle');
                    $c = new Criteria();
                    $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
                    $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
                    $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }
                }
                $user = $this->getUser();
                $dinas = $user->setAttribute('nama', '', 'dinas');

                $query2 = "
        INSERT INTO " . sfConfig::get('app_default_schema') . ".rincian_detail 
        (kegiatan_code,tipe,unit_id,detail_no,rekening_code,komponen_id,detail_name,volume,keterangan_koefisien,subtitle,komponen_harga,pajak,komponen_harga_awal,
        komponen_name,satuan,from_sub_kegiatan,sub,kode_sub,last_update_user,tahap_edit,tahun)
        VALUES 
        ('" . $kegiatan_code . "','SUB','" . $unit_id . "'," . $maxno . ",'" . $rekening_code . "','" . $komponen_id . "','" . $detail_name . "'," . $volume . ",'" . $keterangan_koefisien . "','" . $subtitle . "',
        " . $komponen_harga_awal * (($pajakawal + 100) / 100) . "," . $pajakawal . "," . $komponen_harga_awal . ",'" . $komponen_name . "','" . $rs->getString('satuan_asli') . "',
            '" . $sub_kegiatan_id . "','" . $new_subtitle . "','" . $kodesub . "','" . $dinas . "','" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "')
        ";

                if (($volume > 0) and ( $keterangan_koefisien != '')) {
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query2);
                    //$tampQuery=$tampQuery.'<br>'.$query2;
                    $stmt->executeQuery();
                }
            }
            //echo $tampQuery;exit;
            $this->setFlash('berhasil', 'Sub Kegiatan Baru Telah Tersimpan');
            return $this->redirect("dinas/edit?unit_id=" . $unit_id . "&kode_kegiatan=" . $kegiatan_code);
            // }//eof status
        }//eof simpan
    }

    public function executePilihSubKegiatan() {
        //sholeh begin
        $user = $this->getUser();
        $lokasiSession = $user->getAttribute('nama', '', 'lokasi');

        $this->lokasiSession = $lokasiSession;

        $user->setAttribute('nama', '', 'lokasi');
        $user->setAttribute('nama', '', 'lokasi_baru');
        //sholeh end

        $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rincian_detail where kode_sub ilike 'RSUB' and sub<>''";
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($sql);
        $rs = $stmt->executeQuery();
        while ($rs->next()) {
            $kodesub = $rs->getString('kode_sub');
        }
        $kode = substr($kodesub, 4, 5);
        $kode+=1;
        if ($kode < 10) {
            $kodesub = 'RSUB0000' . $kode;
        } elseif ($kode < 100) {
            $kodesub = 'RSUB000' . $kode;
        } elseif ($kode < 1000) {
            $kodesub = 'RSUB00' . $kode;
        } elseif ($kode < 10000) {
            $kodesub = 'RSUB0' . $kode;
        } elseif ($kode < 100000) {
            $kodesub = 'RSUB' . $kode;
        }
        $this->kodesub = $kodesub;
        return sfView::SUCCESS;
    }

    public function executeHapusSubPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $kode_sub = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');

            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            /*
              if($tahap=='')
              {
              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();


              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
              else if($tahap!='')
              {
              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set volume=0,keterangan_koefisien='0'
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "update ". sfConfig::get('app_default_schema') .".rincian_detail set sub='',kode_sub=''
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();

              $query = "delete from ". sfConfig::get('app_default_schema') .".rincian_sub_parameter
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and kode_sub='$kode_sub'";
              // print_r($query);exit;
              $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
              $statement=$con->prepareStatement($query);
              $statement->executeQuery();
              }
             */


            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::KODE_SUB, $kode_sub);
            $jumlah = RincianDetailPeer::doCount($c);
            $volume = 0;
            $keterangan_koefisien = '0';
            $sub = '';
            $kode_sub = '';
            $kode_jasmas = '';
            $nilaiBaru = 0;
            if ($jumlah == 1) {
                $rincian_detail = RincianDetailPeer::doSelectOne($c);
                if ($rincian_detail) {
                    $detail_no = $rincian_detail->getDetailNo();
                    $pajak = $rincian_detail->getPajak();
                    $harga = $rincian_detail->getKomponenHargaAwal();
                    //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                    $nilaiBaru = $rincian_detail->getNilaiAnggaran();
                    //sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = '.$nilaiBaru);
                    $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                    //$rincian_detail->setVolume($volume);
                    $rincian_detail->setDetailName($detail_name);
                    $rincian_detail->setSubtitle($subtitle);
                    $rincian_detail->setSub($sub);
                    $rincian_detail->setKodeSub($kode_sub);
                    $rincian_detail->setKecamatan($kode_jasmas);

                    $rincian_detail->setStatusHapus(TRUE);
                    //djieb: start edit from here
                    //melihat apakah komponen sudah terpakai belum di eproject
                    $nilaiTerpakai = 0; //default 0
                    $query = "SELECT
                              s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting, sum(dp.ALOKASI) as jml
                            from
                              " . sfConfig::get('app_default_eproject') . ".detail_pekerjaan dp,
                              " . sfConfig::get('app_default_eproject') . ".pekerjaan p,
                              " . sfConfig::get('app_default_eproject') . ".detail_kegiatan dk,
                              " . sfConfig::get('app_default_eproject') . ".kegiatan k,
                              " . sfConfig::get('app_default_eproject') . ".skpd s
                            where
                              ((p.STATE<>0 and p.STATE IS NOT NULL) or p.pernah_realisasi=1 or pernah_konfirmasi=1) and
                              dp.pekerjaan_id=p.id  and p.kegiatan_id=k.id and s.kode='$unit_id' and k.kode='$kegiatan_code' and dk.id_budgeting=$detail_no
                              and k.skpd_id=s.id and dp.detail_kegiatan_id=dk.id
                            group by s.kode,k.kode,dk.kode_detail_kegiatan,dk.id_budgeting";
                    //print_r($query);exit;
                    //diambil nilai terpakai
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_eprojects = $statement->executeQuery();
                    $this->rs_eproject = $rs_eproject;
                    while ($rs_eprojects->next()) {
                        $nilaiTerpakai = $rs_eprojects->getString('jml');
                        $jumlahRows = $rs_eprojects->getRow();
                    }

                    if ($nilaiBaru < $nilaiTerpakai) {

                        $this->setFlash('gagal', 'Mohon maaf , untuk Rincian Kegiatan ini sudah terpakai di pekerjaan, sejumlah ' . number_format($nilaiTerpakai, 0, ',', '.'));
                        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                    budgetLogger::log('Menghapus Sub Kegiatan dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . ' dan kode sub :' . $this->getRequestParameter('no') . ' dan detail no (' . $detail_no . '); komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else {
                    sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                    $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
            } elseif ($jumlah > 1) {
                $rincian_detail = RincianDetailPeer::doSelect($c);

                $gagal = 'salah';
                foreach ($rincian_detail as $rincian_details) {
                    $detail_no = $rincian_details->getDetailNo();
                    $query = "update " . sfConfig::get('app_default_schema') . ".rincian_detail set status_hapus=true,   tahap_edit='" . sfConfig::get('app_tahap_edit') . "' where
                                    unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                    //echo $query;
                    $con = Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    budgetLogger::log('Menghapus Sub Kegiatan dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . ' dan kode sub :' . $this->getRequestParameter('no') . ' dan detail no (' . $detail_no . '); komponen_id:' . $rincian_details->getKomponenId() . '; komponen_name:' . $rincian_details->getKomponenName());
                    $statement->executeQuery();

                    //cek paketan
                    $d = new Criteria();
                    $d->add(RincianDetailPeer::UNIT_ID, $unit_id);
                    $d->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $d->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                    $cari_rincian_detail = RincianDetailPeer::doSelectOne($d);
                    if ($cari_rincian_detail) {
                        /*
                          $host='pengendalian.surabaya2excellence.or.id/eproject2009';
                          $port=8080;
                          $body='';
                          $headers='';
                          $tahun=sfConfig::get('app_sinkronisasi_tahun',date('Y'));


                          $tahun=substr($tahun,2,2);//ambil 2 digit terakhir;
                          $kode_detail_kegiatan=sprintf("%s.%s.%s.%s",$unit_id,$kegiatan_code,'09',$detail_no);
                          $url='http://pengendalian.surabaya2excellence.or.id/eproject2010/sinkronisasi/ubahDetailKegiatan.shtml?kode_detail_kegiatan='.$kode_detail_kegiatan;
                          sfContext::getInstance()->getLogger()->debug('{eProject}akan mengambil data dari eproject '.$kode_detail_kegiatan);
                          $cek_eproject=HttpHelper::httpGet($host,$port,$url,$body,$headers); //hasilnya bisa true bisa false

                          //hasil xml dari eproject masuk ke variabel body
                          $nilaiTerpakai=0;
                          $httpOK=false;
                          if ($cek_eproject && ( strpos($body,'<?xml')!==false ) && ( strpos($body,'nilai="')!==false ) )
                          {
                          $doc=new DomDocument("1.0");
                          $doc->loadXml($body);
                          $detail_kegiatan=$doc->documentElement;//root xml
                          $nilaiTerpakai=$detail_kegiatan->getAttribute('nilai');
                          $httpOK=true;
                          }
                          sfContext::getInstance()->getLogger()->debug('{eProject} rincian terpakai ketemu, nilai terpakai = '.$nilaiTerpakai);
                          if ($nilaiBaru<$nilaiTerpakai)
                          {
                          $gagal = 'benar';
                          }
                         */
                    }
                }

                if ($gagal == 'benar') {
                    $this->setFlash('gagal', 'Mohon maaf , untuk Rincian ASB Non Fisik Kegiatan ini sudah terpakai di pekerjaan');
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } elseif ($gagal == 'salah') {
                    //echo $keterangan_koefisien;
                    //print_r($rincian_detail);	
                    /*
                      $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                      $rincian_detail->setVolume($volume);
                      $rincian_detail->setDetailName($detail_name);
                      $rincian_detail->setSubtitle($subtitle);
                      $rincian_detail->setSub($sub);
                      $rincian_detail->setKodeSub($kode_sub);
                      $rincian_detail->setKecamatan($kode_jasmas);
                      $rincian_detail->save();
                     */
                    /*
                      $query="update ". sfConfig::get('app_default_schema') .".rincian_detail set volume=0, keterangan_koefisien='0 ' || satuan where
                      unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no=$detail_no";
                      echo $query;
                      $con=Propel::getConnection(RincianSubParameterPeer::DATABASE_NAME);
                      $statement=$con->prepareStatement($query);
                      $statement->executeQuery();
                     */
                    //$this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan');
                    //return $this->redirect('dinas/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
                }
            } else {
                sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = ' . $nilaiBaru};
                $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }

            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }
            /*
              $query = "select *
              from ". sfConfig::get('app_default_schema') .".rincian_detail
              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and volume>0 order by sub,rekening_code,komponen_name";
             */

            $query = "select *
                              from " . sfConfig::get('app_default_schema') . ".rincian_detail
                              where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '$nama_subtitle' and status_hapus=false order by sub,rekening_code,komponen_name";
            $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeHapusPekerjaans() {
        if ($this->getRequestParameter('id')) {

            $detail_no = $this->getRequestParameter('no');
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $tahap = '';
            $c = new Criteria();
            $c->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_kegiatan = MasterKegiatanPeer::doSelectOne($c);
            if ($rs_kegiatan) {
                $tahap = $rs_kegiatan->getTahap();
            }
            //$volume=0;
            //$keterangan_koefisien='0';
            //$sub='';
            //$kode_sub='';
            $kode_jasmas = '';

            $sekarang = date('Y-m-d H:i:s');

            $ada_waitinglist = 0;
            $rd_cari_waitinglist = new Criteria();
            $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
            $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);
            $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);

            if ($ada_waitinglist > 0) {
                $this->setFlash('gagal', 'Mohon maaf, rincian RKA tidak dapat dihapus karena komponen waiting list. Silahkan mengenolkan saja.');
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }


            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rincian_detail = RincianDetailPeer::doSelectOne($c);
            if ($rincian_detail) {
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;

                $lelang = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;

                if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                    $totNilaiAlokasi = $rincian_detail->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $lelang = $rincian_detail->getCekLelang($unit_id, $kegiatan_code, $detail_no, 0);
                        if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                            $totNilaiSwakelola = $rincian_detail->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $totNilaiKontrak = $rincian_detail->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                            $ceklelangselesaitidakaturanpembayaran = $rincian_detail->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                        }
                    }
                }

                //irul 27 feb 2014 - hapus komponen
                if ($totNilaiAlokasi == 0 && $totNilaiKontrak == 0 && $totNilaiSwakelola == 0 && $lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                    $rincian_detail->setStatusHapus('TRUE');
                    $rincian_detail->setLastEditTime($sekarang);
                    $rincian_detail->setTahap($tahap);

                    $this->setFlash('berhasil', 'Komponen ' . $rincian_detail->getKomponenName() . ' berhasil dihapus ');
                    budgetLogger::log('Menghapus komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());

                    //irul 10 september 2014 - status hapus data gmap diset true
                    $con = Propel::getConnection();
                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                            . "set status_hapus = true, last_edit_time = '$sekarang' "
                            . "where unit_id='" . $rincian_detail->getUnitId() . "' and kegiatan_code='" . $rincian_detail->getKegiatanCode() . "' and detail_no ='" . $rincian_detail->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                    $stmt = $con->prepareStatement($query);
                    $stmt->executeQuery();
                    //irul 10 september 2014 - status hapus data gmap diset true

                    $rincian_detail->save();

                    historyUserLog::hapus_komponen($unit_id, $kegiatan_code, $detail_no);

                    $kode_rka_fix = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                    $c_update_history = new Criteria();
                    $c_update_history->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                    $dapat_history = HistoryPekerjaanV2Peer::doSelectOne($c_update_history);
                    if ($dapat_history) {
                        $dapat_history->setStatusHapus(TRUE);
                        $dapat_history->save();
                    }

                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else {
                    if ($totNilaiKontrak > 0 || $totNilaiSwakelola > 0) {
                        if ($totNilaiKontrak == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        } else if ($totNilaiSwakelola == 0) {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiKontrak, 0, ',', '.'));
                        } else {
                            $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah terpakai di eDelivery sebesar Rp ' . number_format($totNilaiSwakelola, 0, ',', '.'));
                        }
                    } else if ($totNilaiAlokasi > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, ' . $rincian_detail->getKomponenName() . ' ini sudah dialokasikan di eProject sebesar Rp ' . number_format($totNilaiAlokasi, 0, ',', '.'));
                    } else if ($lelang > 0) {
                        $this->setFlash('gagal', 'Mohon maaf, Komponen sedang dalam proses lelang');
                    } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                        $this->setFlash('gagal', 'Proses lelang selesai + Belum ada Aturan Pembayaran. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                    }
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
                //irul 27 feb 2014 - hapus komponen
            }
        } else {
            //sfContext::getInstance()->getLogger()->debug{'{eProject} rincian detail tidak ketemu, nilai baru = '.$nilaiBaru};
            $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
            return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
        }
    }

    public function executeBaruKegiatan() {
        //print_r($this->getRequest());exit;
        if ($this->getRequestParameter('cari') == 'cari') {
            //print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'lokasi');
            $user->setAttribute('sub', $this->getRequestParameter('sub'), 'lokasi');
            $user->setAttribute('pajak', $this->getRequestParameter('pajak'), 'lokasi');
            $user->setAttribute('rekening', $this->getRequestParameter('rekening'), 'lokasi');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'lokasi');
            $user->setAttribute('baru', 'baru', 'lokasi');

            return $this->forward('lokasi', 'list');
        }
        if ($this->getRequestParameter('simbada') == 'simbada') {
            //print_r($this->getRequestParameter('simbada'));exit;
            $user = $this->getUser();
            $user->removeCredential('simbada');
            $user->addCredential('simbada');
            $user->setAttribute('nama', $this->getRequestParameter('keterangan'), 'simbada');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'simbada');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'simbada');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'simbada');
            $user->setAttribute('subtitle', $this->getRequestParameter('subtitle'), 'simbada');
            $user->setAttribute('sub', $this->getRequestParameter('sub'), 'simbada');
            $user->setAttribute('pajak', $this->getRequestParameter('pajak'), 'simbada');
            $user->setAttribute('rekening', $this->getRequestParameter('rekening'), 'simbada');
            $user->setAttribute('tipe', $this->getRequestParameter('tipe'), 'simbada');
            $user->setAttribute('baru', 'baru', 'simbada');

            //return $this->forward('simbada','list');
            return $this->forward('perlengkapan ', 'list');
        }
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $subtitle = $this->getRequestParameter('subtitle');
            $sub = $this->getRequestParameter('sub');
            $tipe = $this->getRequestParameter('tipe');
            $rekening = $this->getRequestParameter('rekening');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('id');
            $isBlud = $this->getRequestParameter('blud');
            $isMusrenbang = $this->getRequestParameter('musrenbang');
            $isHibah = $this->getRequestParameter('hibah');
            $akrual_code = $this->getRequestParameter('akrual_code');

            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            $rs_est_fisik = KomponenPeer::doSelectOne($c);
            $est_fisik = $rs_est_fisik->getIsEstFisik();
            $tipe2 = $rs_est_fisik->getKomponenTipe2();

            $lokasi_baru = '';
            $lokasi_array = array();

            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }
#51 - GMAP simpan
//            if ($tipe == 'FISIK' && !$this->getRequestParameter('lokasi')) {
//                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
//                return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//            }
#51 - GMAP simpan            
            if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) && (!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan') )) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }
#51 - GMAP simpan
            if (($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) && !$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
            }

            $ada_fisik = 'FALSE';
            if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {

                $ada_fisik = 'TRUE';
                $keisi = 0;

//                    ambil lama
                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                if (count($lokasi_lama) > 0) {
                    foreach ($lokasi_lama as $value_lokasi_lama) {
                        $c_cari_lokasi = new Criteria();
                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                        if ($dapat_lokasi_lama) {

                            $jalan_fix = '';
                            $gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';

                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                            $gang_lama = $dapat_lokasi_lama->getGang();
                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                            $rw_lama = $dapat_lokasi_lama->getRw();
                            $rt_lama = $dapat_lokasi_lama->getRt();
                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                            if ($jalan_lama <> '') {
                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                            }

                            if ($tempat_lama <> '') {
                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                            }

                            if ($gang_lama <> '') {
                                $gang_fix = $gang_lama . ' ';
                            }

                            if ($nomor_lama <> '') {
                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                            }

                            if ($rw_lama <> '') {
                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                            }

                            if ($rt_lama <> '') {
                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                            }

                            if ($keterangan_lama <> '') {
                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                            }

                            if ($keisi == 0) {
                                $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                $keisi++;
                            } else {
                                $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                $keisi++;
                            }
                        }
                    }
                }

//                    buat baru
                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                $total_array_lokasi = count($lokasi_jalan);

                for ($i = 0; $i < $total_array_lokasi; $i++) {
                    $jalan_fix = '';
                    $gang_fix = '';
                    $tipe_gang_fix = '';
                    $nomor_fix = '';
                    $rw_fix = '';
                    $rt_fix = '';
                    $keterangan_fix = '';
                    $tempat_fix = '';
                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                        if (trim($lokasi_jalan[$i]) <> '') {
                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                        }

                        if (trim($lokasi_tempat[$i]) <> '') {
                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                        }

                        if (trim($tipe_gang[$i]) <> '') {
                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                        } else {
                            $tipe_gang_fix = 'GG. ';
                        }

                        if (trim($lokasi_gang[$i]) <> '') {
                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                        }

                        if (trim($lokasi_nomor[$i]) <> '') {
                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                        }

                        if (trim($lokasi_rw[$i]) <> '') {
                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                        }

                        if (trim($lokasi_rt[$i]) <> '') {
                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                        }

                        if (trim($lokasi_keterangan[$i]) <> '') {
                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                        }

                        if ($keisi == 0) {
                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        } else {
                            $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                            $keisi++;
                        }
                    }
                }
                if ($keisi == 0) {
                    $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                    return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
                }
            }
#51 - GMAP simpan            
            //irul - ambil tahap tabel master_kegiatan
            $c_master_kegiatan = new Criteria();
            $c_master_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
            $c_master_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
            $rs_master_kegiatan = MasterKegiatanPeer::doSelectOne($c_master_kegiatan);
            $tahap = $rs_master_kegiatan->getTahap();
            //irul - ambil tahap tabel master_kegiatan
            //        irul 31januari2015 - fungsi set BPJS
//            $isPotongBpjs = $this->getRequestParameter('is_potong_bpjs');
//            $isIuranBpjs = $this->getRequestParameter('is_iuran_bpjs');
            $c_cek_komponen = new Criteria();
            $c_cek_komponen->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
            $data_komponen = KomponenPeer::doSelectOne($c_cek_komponen);
            if ($data_komponen) {
                if ($data_komponen->getIsPotongBpjs() == true) {
                    $potongBPJS = 'TRUE';
                } else {
                    $potongBPJS = 'FALSE';
                }

                if ($data_komponen->getIsIuranBpjs() == true) {
                    $iuranBPJS = 'TRUE';
                } else {
                    $iuranBPJS = 'FALSE';
                }
            } else {
                $this->setFlash('gagal', 'Komponen Tidak Ada');
                return $this->redirect("dinas/editKegiatan");
            }
            //        irul 31januari2015 - fungsi set BPJS
//            tambahan untuk cek JKN atau JKK
            if ($komponen_id == '23.01.01.04.17') {
                $ini_komponen_jkn = 'TRUE';
            } else {
                $ini_komponen_jkn = 'FALSE';
            }
            if ($komponen_id == '23.01.01.04.18') {
                $ini_komponen_jkk = 'TRUE';
            } else {
                $ini_komponen_jkk = 'FALSE';
            }
//            tambahan untuk cek JKN atau JKK
            // irul 18maret 2014 - simpan catatan
            $note_skpd = '';
            $note_skpd = $this->getRequestParameter('catatan');

            if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'tutup') {
                $apakah_murni = 1;
            } else {
                $apakah_murni = 0;
            }

//            <?php if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'buka') {

            if (strlen(str_replace(' ', '', $note_skpd)) >= 15 || $apakah_murni == 1) {
                // irul 18maret 2014 - simpan catatan   

                if ($isMusrenbang == 1) {
                    $isMusrenbang = 'TRUE';
                } else if ($isMusrenbang <> 1) {
                    $isMusrenbang = 'FALSE';
                }
                $this->isMusrenbang = $isMusrenbang;

                if ($isHibah == 1) {
                    $isHibah = 'TRUE';
                } else if ($isHibah <> 1) {
                    $isHibah = 'FALSE';
                }
                $this->isHibah = $isHibah;


                if ($isBlud == 1) {
                    $isBlud = 'TRUE';
                    $this->isBlud = $isBlud;
                } else if ($isBlud <> 1) {
//                echo $isBlud;exit;
                    $isBlud = 'FALSE';
                    $this->isBlud = $isBlud;
                }

                if ($isPotongBPJS == 1) {
                    $isPotongBPJS = 'TRUE';
                    $this->isPotongBPJS = $isPotongBPJS;
                } else if ($isPotongBPJS <> 1) {
//                echo $isBlud;exit;
                    $isPotongBPJS = 'FALSE';
                    $this->isPotongBPJS = $isPotongBPJS;
                }
                //begin of kecamatan kelurahan
                if ($this->getRequestParameter('kecamatan')) {
                    $lokasi_kec = $this->getRequestParameter('kecamatan');
                    $lokasi_kel = $this->getRequestParameter('kelurahan');
                    $kec = new Criteria();
                    $kec->add(KecamatanPeer::ID, $lokasi_kec);
                    $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                    $rs_kec = KecamatanPeer::doSelectOne($kec);
                    if ($rs_kec) {
                        $kecamatan = $rs_kec->getNama();
                    }

                    $kel = new Criteria();
                    $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                    $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                    $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                    if ($rs_kel) {
                        $kelurahan = $rs_kel->getNamaKelurahan();
//                          $rincian_detail->setLokasiKecamatan($kecamatan);
//                          $rincian_detail->setLokasiKelurahan($kelurahan);
                    } else {
                        $kecamatan = '';
                        $kelurahan = '';
//                            $rincian_detail->setDetailName($detail_name);
                    }
                    //                echo $lokasi_kec.'--'.$lokasi_kel.'--'.$kecamatan.'--'.$kelurahan;exit;
                }
//            echo $isBlud;exit;
                //print_r($sub);exit;
                //untuk komponen penyusun $pegawai=$this->pegawai=$this->getRequestParameter('pegawai');
                $komponen_penyusun = $this->getRequestParameter('penyusun');
                //print_r($this->getRequestParameter('penyusun'));exit;

                $volumePenyusun = array();
                foreach ($komponen_penyusun as $penyusun) {
                    $cekPenyusun = new Criteria();
                    $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun);
                    $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
                    foreach ($rs_cekPenyusun as $komPenyusun) {
                        $kdChekBox = str_replace(".", "_", $komPenyusun->getKomponenId());
                        $nilaiDariWeb = $this->getRequestParameter('volPenyu_' . $kdChekBox);
                        $volumePenyusun[$komPenyusun->getKomponenId()] = $nilaiDariWeb;
                    }
                }

                $user = $this->getUser();
                $dinas = sfContext::getInstance()->getUser()->getNamaLogin();

                $vol1 = $this->getRequestParameter('vol1');
                $vol2 = $this->getRequestParameter('vol2');
                $vol3 = $this->getRequestParameter('vol3');
                $vol4 = $this->getRequestParameter('vol4');
                //menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.
                $rd_function = new RincianDetail();

                $status_pagu_uk = 0;
                $array_buka_pagu_uk_khusus = array();
                if (!in_array($unit_id, $array_buka_pagu_uk_khusus)) {
                    if ($rekening == '5.2.1.04.01') {
                        $status_pagu_uk = $rd_function->getBatasPaguPerDinasUK($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                        $nilai_maks_uk = $rd_function->getNilaiPaguUKMaks($unit_id);
                        if ($status_pagu_uk == 1) {
                            $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks UK untuk SKPD Anda sebesar ' . number_format($nilai_maks_uk));
                            return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                        }
                    }
                }

                //irul 19juli 2014 - ENTRI BUDGET 2015 BATAS PAGU DINAS REQUEST                
                $array_buka_pagu_dinas_khusus = array('9999');
                $array_buka_pagu_kegiatan_khusus = array('');
                if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                } else {
                    if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                        $kecamatan_unit_id = substr($unit_id, 0, 2);
                        if (1 == 0) {
                            $status_pagu_rincian = 0;
                        } else {
                            if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                                //batas pagu per dinas
                                $status_pagu_rincian = $rd_function->getBatasPaguPerDinas($unit_id, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                            } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                                //batas pagu per kegiatan 
                                $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatan($unit_id, $kegiatan_code, $komponen_id, $pajak, $vol1, $vol2, $vol3, $vol4, $komponen_penyusun, $volumePenyusun);
                            }
                        }
                    } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                        $status_pagu_rincian = 0;
                    }
                }

                if ($akrual_code) {
                    $akrual_code_baru = $akrual_code . '|01';
                    $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
                    $akrual_code_baru = $akrual_code_baru . $no_akrual_code;
                }

                //batasan pagu
                if ($status_pagu_rincian == '1') {
                    $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena. nilai total RKA Melebihi total Pagu SKPD.');
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else if ($status_pagu_rincian == '0') {

                    // $this->setFlash('berhasil', 'berhasil disimpan.');
                    // return $this->redirect('dinas/edit?unit_id='.$unit_id.'&kode_kegiatan='.$kegiatan_code);
                    //end of fasilitas 
                    $c = new Criteria();
                    $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id);
                    $rs_komponen = KomponenPeer::doSelectOne($c);
                    if ($rs_komponen) {
                        $komponen_harga = $rs_komponen->getKomponenHarga();
                        $komponen_name = $rs_komponen->getKomponenName();
                        $satuan = $rs_komponen->getSatuan();
                    }
                    $detail_no = 0;
                    $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs_max = $stmt->executeQuery();
                    while ($rs_max->next()) {
                        $detail_no = $rs_max->getString('nilai');
                    }
                    $detail_no+=1;
                    $detail_name = '';
                    $kode_jasmas = '';

                    if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
#51 - GMAP simpan                        
//                        $detail_name = $this->getRequestParameter('lokasi');
                        $detail_name = $lokasi_baru;
                        $ada_fisik = 'TRUE';
#51 - GMAP simpan                        
                        $kode_jasmas = $this->getRequestParameter('jasmas');
                    } else if ($tipe2 != 'KONSTRUKSI' && $tipe != 'FISIK' && !$est_fisik) {
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                        $estimasi_opsi_lokasi = array('');
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                        if (in_array($komponen_id, $estimasi_opsi_lokasi)) {
#51 - GMAP simpan                        
//                        $detail_name = $this->getRequestParameter('lokasi');
                            $detail_name = $lokasi_baru;
#51 - GMAP simpan                        
                            $kode_jasmas = $this->getRequestParameter('jasmas');
                        } else {
                            //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan    
                            $detail_name = $this->getRequestParameter('keterangan');
                            //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan                            
                        }
                        //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
                    }

                    $kode_sub = '';
                    $sub = '';
#51 - GMAP comment                    
//                    if ($this->getRequestParameter('lokasi')) {
//                        $kode_lokasi = '';
//                        $detail_name = $this->getRequestParameter('lokasi');
//                        //sholeh begin
//                        $detail_names = explode(',', $detail_name);
//                        $adaSalah = false;
//                        foreach ($detail_names as $name) {
//                            $c = new Criteria();
//                            $c->add(VLokasiPeer::NAMA, trim($name));
//                            $rs_lokasi = VLokasiPeer::doSelectOne($c);
//                            if (!rs_lokasi)
//                                $adaSalah = true;
//                        }
//
//                        if ($adaSalah) {
//                            $this->setFlash('lokasitidakada', 'Lokasi Tidak Ada atau Tidak Valid dengan Data G.I.S');
//                            return $this->redirect("dinas/buatbaru?lokasi=$detail_name&kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//                        }
//                        //sholeh end
//                    }
#51 - GMAP comment                    
                    //ditutup sementara simbada
                    /* if($rs_komponen->getMaintenance()=='true' &&  $this->getRequestParameter('keterangan')=='' && $rekening !='5.2.2.27.01')
                      {
                      $this->setFlash('lokasitidakada', 'Data Simbada belum diisi');
                      return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=".md5('terbaru')."&commit=Pilih");
                      } */

#51 - GMAP comment                                        
                    //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
//                    $estimasi_opsi_lokasi = array('');
//                    //irul 2may 2014 awal-comment  untuk menampilka opsi lokasi pada estimasi pembuatan separator jalan
//                    if ($rekening == '5.2.2.27.01' && $tipe == 'FISIK' || in_array($komponen_id, $estimasi_opsi_lokasi)) {
//                        if (!$this->getRequestParameter('lokasi')) {
//                            $this->setFlash('lokasitidakada', 'Untuk Komponen Fisik diharapkan Mengisi Data Lokasi');
//                            return $this->redirect("dinas/buatbaru?kegiatan=$kegiatan_code&subtitle=$subtitle&rekening=$rekening&pajak=$pajak&unit=$unit_id&tipe=$tipe&komponen=$komponen_id&baru=" . md5('terbaru') . "&commit=Pilih");
//                        }
//                    }
#51 - GMAP comment                                        

                    if ($this->getRequestParameter('keterangan')) {
                        $detail_name = $this->getRequestParameter('keterangan');
                    }

                    if ($this->getRequestParameter('sub')) {
                        $kode_sub = $this->getRequestParameter('sub');

                        $d = new Criteria();
                        $d->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                        $d->add(RincianSubParameterPeer::KEGIATAN_CODE, $kegiatan_code);
                        $d->add(RincianSubParameterPeer::UNIT_ID, $unit_id);
                        $rs_rinciansubparameter = RincianSubParameterPeer::doSelectOne($d);
                        if ($rs_rinciansubparameter) {
                            $sub = $rs_rinciansubparameter->getNewSubtitle();
                            $kodesub = $rs_rinciansubparameter->getKodeSub();
                        }
                        //print_r($kode_sub);exit;
                    }
                    $kode_subtitle = $this->getRequestParameter('subtitle');

                    $c = new Criteria();
                    $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
                    $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }

                    $volume = 0;
                    $keterangan_koefisien = '';
                    $user = $this->getUser();
                    $dinas = $user->setAttribute('nama', '', 'dinas');
                    if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                        if ($this->getRequestParameter('vol2') == '') {
                            $vol2 = 1;
                            $volume = $this->getRequestParameter('vol1') * $vol2;
                            $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                        } else if (!$this->getRequestParameter('vol2') == '') {
                            $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                            $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                        }
                        if ($this->getRequestParameter('vol3') == '') {
                            $vol3 = 1;
                            $volume = $volume * $vol3;
                        } else if (!$this->getRequestParameter('vol3') == '') {
                            $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                            $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                        }
                        if ($this->getRequestParameter('vol4') == '') {
                            $vol4 = 1;
                            $volume = $volume * $vol4;
                        } else if (!$this->getRequestParameter('vol4') == '') {
                            $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                            $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                        }
                    }

//                    tambahan untuk gmap                    
                    $detailno_fisik = 0;
//                    tambahan untuk gmap           

                    if ($this->getRequestParameter('status') == 'pending') {
                        $detail_no = 0;
                        $query = "select max(detail_no) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $rs_max = $stmt->executeQuery();
                        while ($rs_max->next()) {
                            $detail_no = $rs_max->getString('nilai');
                        }
                        $detail_no+=1;

                        $sql = "select rekening_asli, komponen_name, detail_name from " . sfConfig::get('app_default_schema') . ".rka_member where kode_sub='$kode_sub'";
                        //print_r($sql);exit;
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($sql);
                        $rs2 = $stmt->executeQuery();
                        while ($rs2->next()) {
                            $komponen_name2 = $rs2->getString('komponen_name');
                            $detail_name2 = $rs2->getString('detail_name');
                            $rekening_asli = $rs2->getString('rekening_asli');
                        }
                        $subSubtitle = $komponen_name2 . ' ' . $detail_name2;
                        $subSubtitle = trim($subSubtitle);

                        $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah 
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time,tahun)
                                values
                                ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name . "', " . $volume . ", '" . $keterangan_koefisien . "',
                                    '" . $subtitle . "', " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                                        '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahun_default') . "')";

                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        budgetLogger::log('Menambah komponen baru(bermasalah) dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                        $stmt->executeQuery();
                        $this->setFlash('berhasil', 'Penyimpanan Telah Berhasil, Tetapi Tidak Masuk Ke RKA, Harap Hubungi Penyelia Anda');
                        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    } else {
                        //bisma : 21-5-2012 : Mulai IF dari sini. 
                        //jika tidak OK dan tidak pending
                        //bisma : contreng
                        //print_r('bisma');exit;
                        $sub_koRek = substr($rekening, 0, 5);

                        if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                            $ada_fisik = 'TRUE';
                        }

                        if ($sub_koRek == '5.2.3' || $sub_koRek == '5.2.2') {
                            $sql = "select max(kode_sub) as kode_sub from " . sfConfig::get('app_default_schema') . ".rka_member "
                                    . "where kode_sub ilike 'RKAM%'";
                            $con = Propel::getConnection();
                            $stmt = $con->prepareStatement($sql);
                            $rs = $stmt->executeQuery();
                            while ($rs->next()) {
                                $kodesub = $rs->getString('kode_sub');
                            }
                            $kode = substr($kodesub, 4, 5);
                            $kode+=1;
                            if ($kode < 10) {
                                $kodesub = 'RKAM0000' . $kode;
                            } elseif ($kode < 100) {
                                $kodesub = 'RKAM000' . $kode;
                            } elseif ($kode < 1000) {
                                $kodesub = 'RKAM00' . $kode;
                            } elseif ($kode < 10000) {
                                $kodesub = 'RKAM0' . $kode;
                            } elseif ($kode < 100000) {
                                $kodesub = 'RKAM' . $kode;
                            }

                            $con = Propel::getConnection();
                            $con->begin();
                            try {
                                $detail_no = 0;
                                $queryDetailNo = "select max(detail_no) as nilai "
                                        . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($queryDetailNo);
                                $rs_max = $stmt->executeQuery();
                                while ($rs_max->next()) {
                                    $detail_no = $rs_max->getString('nilai');
                                }
                                $detail_no+=1;

//                                tambahan untuk gmap
                                if ($tipe2 == 'KONSTRUKSI' || $tipe == 'FISIK' || $est_fisik) {
                                    $ada_fisik = 'TRUE';
                                    $detailno_fisik = $detail_no;
                                }
//                                tambahan untuk gmap  

                                if ($detail_name != '')
                                    $detail_name_baru = "(" . $detail_name . ")";
                                else
                                    $detail_name_baru = $detail_name;
                                if ($kode_sub) {
                                    //$kode_sub : berasal dari sub subtitle
                                    $sql = "select rekening_asli, komponen_name, detail_name "
                                            . "from " . sfConfig::get('app_default_schema') . ".rka_member "
                                            . "where kode_sub='$kode_sub'";
                                    //print_r($sql);exit;
                                    $con = Propel::getConnection();
                                    $stmt = $con->prepareStatement($sql);
                                    $rs2 = $stmt->executeQuery();
                                    while ($rs2->next()) {
                                        $komponen_name2 = $rs2->getString('komponen_name');
                                        $detail_name2 = $rs2->getString('detail_name');
                                        $rekening_asli = $rs2->getString('rekening_asli');
                                    }
                                    $subSubtitle = $komponen_name2 . ' ' . $detail_name2;
                                    $subSubtitle = trim($subSubtitle);

                                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                                lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,akrual_code,tipe2)
                                values
                                ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "',
                                    " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                                        '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', '$akrual_code', '$tipe2')";
                                    $stmt = $con->prepareStatement($query);
                                    $stmt->executeQuery();

                                    historyUserLog::tambah_komponen($unit_id, $kegiatan_code, $detail_no);
                                } else {
                                    $subSubtitle = $komponen_name . ' ' . $detail_name;
                                    $subSubtitle = trim($subSubtitle);

                                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                                lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,akrual_code,tipe2)
                                values
                                ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "',
                                    " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kodesub . "', '" . $subSubtitle . "',
                                        '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isMusrenbang . "','" . $isHibah . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', '$akrual_code', '$tipe2')";
                                    $stmt = $con->prepareStatement($query);
                                    $stmt->executeQuery();

                                    historyUserLog::tambah_komponen($unit_id, $kegiatan_code, $detail_no);

                                    $queryInsert2RkaMember = " insert into " . sfConfig::get('app_default_schema') . ".rka_member (kode_sub,unit_id,kegiatan_code,detail_no,komponen_id,komponen_name,detail_name,rekening_asli,tahun )
                                        values ('$kodesub','$unit_id','$kegiatan_code',$detail_no,'$komponen_id','$komponen_name','$detail_name','$rekening','" . sfConfig::get('app_tahun_default') . "')";
                                    //print_r($queryInsert2RkaMember);exit;
                                    $stmt2 = $con->prepareStatement($queryInsert2RkaMember);
                                    $stmt2->executeQuery();

                                    $detailNamePenyusun = '';
                                    $rekening_induk = $rekening;
                                    $temp_lain = 8;
                                    foreach ($komponen_penyusun as $penyusun) {
                                        $cekPenyusun = new Criteria();
                                        $cekPenyusun->add(KomponenPeer::KOMPONEN_ID, $penyusun);
                                        $rs_cekPenyusun = KomponenPeer::doSelect($cekPenyusun);
                                        //print_r($rs_cekPenyusun);
                                        foreach ($rs_cekPenyusun as $komponenPenyusun) {
                                            if ($akrual_code) {
                                                if ($komponenPenyusun->getKodeAkrualKomponenPenyusun()) {
                                                    $akrual_code_penyusun = $akrual_code . '|02|' . $komponenPenyusun->getKodeAkrualKomponenPenyusun() . $no_akrual_code;
                                                } else {
                                                    if ($temp_lain < 10)
                                                        $akrual_code_penyusun = $akrual_code . '|02|0' . $temp_lain . $no_akrual_code;
                                                    else
                                                        $akrual_code_penyusun = $akrual_code . '|02|' . $temp_lain . $no_akrual_code;
                                                    $temp_lain++;
                                                }
                                            }
                                            $detail_no = 0;
                                            $query = "select max(detail_no) as nilai "
                                                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                                                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code'";
                                            $con = Propel::getConnection();
                                            $stmt = $con->prepareStatement($query);
                                            $rs_max = $stmt->executeQuery();
                                            while ($rs_max->next()) {
                                                $detail_no = $rs_max->getString('nilai');
                                            }
                                            $detail_no+=1;

                                            if ($komponenPenyusun->getKomponenNonPajak() == TRUE) {
                                                $pajakPenyusun = 0;
                                            } else {
                                                $pajakPenyusun = 10;
                                            }

                                            $kodeChekBox = str_replace(".", "_", $komponenPenyusun->getKomponenId());

                                            $subSubtitle = $komponen_name . ' ' . $detail_name;
                                            $subSubtitle = trim($subSubtitle);

                                            $query2 = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,rekening_code_asli,is_blud,lokasi_kecamatan,lokasi_kelurahan,note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,akrual_code,tipe2)
                                                values
                                                ('" . $kegiatan_code . "', 'SSH', " . $detail_no . ", '" . $rekening_induk . "', '" . $komponenPenyusun->getKomponenId() . "', '" . $detailNamePenyusun . "', " . $this->getRequestParameter('volPenyu_' . $kodeChekBox) . ", '" . $this->getRequestParameter('volPenyu_' . $kodeChekBox) . ' ' . $komponenPenyusun->getSatuan() . "', '" . $subtitle . "',
                                                    " . $komponenPenyusun->getKomponenHarga() . ", " . $komponenPenyusun->getKomponenHarga() . ",'" . $komponenPenyusun->getKomponenName() . "', '" . $komponenPenyusun->getSatuan() . "', " . $pajakPenyusun . ",'" . $unit_id . "','" . $kodesub . "', '" . $subSubtitle . "',
                                                        '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','','" . $isBlud . "','" . $kecamatan . "','" . $kelurahan . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "', '$akrual_code_penyusun', '" . $komponenPenyusun->getKomponenTipe2() . "')";
                                            $stmt = $con->prepareStatement($query2);
                                            budgetLogger::log('Menambah Komponen penyusun baru dengan komponen name ' . $komponenPenyusun->getKomponenName() . '(' . $komponenPenyusun->getKomponenId() . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code . ' pada KodeSub:' . $kodesub);

                                            $stmt->executeQuery();

                                            historyUserLog::tambah_komponen_penyusun($unit_id, $kegiatan_code, $detail_no);
                                        }
                                    }
                                }

                                //untuk komponen fisik dan maintenance = true, insert ke db gis_budgeting tabel master_lokasi simbada 
                                //jadi semua yang bertipe fisik dan maintenance = true akan mengambil data dari simbada dan harus didata atau di catat di gis
                                if (($tipe == 'FISIK' || $est_fisik) && $rs_komponen->getMaintenance() == 'true') {
                                    $kodeSimbada = $this->maxKodeSimbada();
                                    $con2 = propel::getConnection('gis');
                                    $con2->begin();
                                    try {
                                        $masterLokasiSimbada = new masterLokasiSimbada();
                                        $masterLokasiSimbada->setKodeLokasiSimbada($kodeSimbada);
                                        $masterLokasiSimbada->setNamaLokasiSimbada($detail_name);
                                        $masterLokasiSimbada->setCatatan($unit_id . '||' . $kegiatan_code . '||' . $detail_no . '||' . $komponen_name);
                                        $masterLokasiSimbada->save($con2);
                                        $con2->commit();
                                    } catch (Exception $e) {
                                        $this->setFlash('gagal', 'data simbada gagal ditambahkan ke gis karena ' . $e);
                                        $con2->rollback();
                                    }
                                }
                                //end komponen fisik dan maintenance

                                $con->commit();

                                budgetLogger::log('Menambah Komponen baru dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);

                                $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
                            } catch (Exception $e) {
                                $con->rollback();
                                $this->setFlash('gagal', 'Tidak berhasil tersimpan karena ' . $e->getMessage());
                                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                            }
                        } else {
                            //jika tidak $sub_koRek=='5.2.3' || $sub_koRek=='5.2.2'
                            //print_r('aneh'.$kode_sub);exit;
                            if ($tipe == 'FISIK' || $est_fisik) {
                                $ada_fisik = 'TRUE';
                            }
                            if ($detail_name != '')
                                $detail_name_baru = "(" . $detail_name . ")";
                            else
                                $detail_name_baru = $detail_name;

                            if ($kode_sub) {
                                $sql = "select rekening_asli, komponen_name, detail_name from " . sfConfig::get('app_default_schema') . ".rka_member where kode_sub='$kode_sub'";
                                //print_r($sql);exit;
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($sql);
                                $rs2 = $stmt->executeQuery();
                                while ($rs2->next()) {
                                    $komponen_name2 = $rs2->getString('komponen_name');
                                    $detail_name2 = $rs2->getString('detail_name');
                                    $rekening_asli = $rs2->getString('rekening_asli');
                                }
                                $subSubtitle = $komponen_name2 . ' ' . $detail_name2;
                                $subSubtitle = trim($subSubtitle);

                                $con = Propel::getConnection();
                                $con->begin();
                                try {
                                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                                (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal,
                                komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                                note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn,is_iuran_jkk,akrual_code)
                                values
                                ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "',
                                    " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $subSubtitle . "',
                                        '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isMusrenbang . "','" . $isHibah . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "', '$akrual_code')";
                                    $stmt = $con->prepareStatement($query);
                                    budgetLogger::log('Menambah komponen baru dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                                    $stmt->executeQuery();
                                    $con->commit();

                                    historyUserLog::tambah_komponen($unit_id, $kegiatan_code, $detail_no);
                                } catch (Exception $e) {
                                    echo $e;
                                    $con->rollback();
                                }
                            } else {
                                $con = Propel::getConnection();
                                $con->begin();
                                try {
                                    $query = "insert into " . sfConfig::get('app_default_schema') . ".rincian_detail
                        (kegiatan_code, tipe, detail_no, rekening_code, komponen_id, detail_name, volume, keterangan_koefisien, subtitle, komponen_harga, komponen_harga_awal, 
                        komponen_name, satuan, pajak, unit_id, kode_sub, sub, kecamatan, last_update_user, last_update_time, tahap_edit,tahun,is_blud,is_musrenbang,is_hibah,
                        note_skpd,is_potong_bpjs,is_iuran_bpjs,tahap,is_iuran_jkn, is_iuran_jkk)
                        values
                        ('" . $kegiatan_code . "', '" . $tipe . "', " . $detail_no . ", '" . $rekening . "', '" . $komponen_id . "', '" . $detail_name_baru . "', " . $volume . ", '" . $keterangan_koefisien . "', '" . $subtitle . "',
                            " . $komponen_harga . ", " . $komponen_harga . ",'" . $komponen_name . "', '" . $satuan . "', " . $pajak . ",'" . $unit_id . "','" . $kode_sub . "', '" . $sub . "',
                                '" . $kode_jasmas . "', '" . $dinas . "', now(),'" . sfConfig::get('app_tahap_edit') . "','" . sfConfig::get('app_tahun_default') . "','" . $isBlud . "','" . $isMusrenbang . "','" . $isHibah . "','" . $note_skpd . "','" . $potongBPJS . "','" . $iuranBPJS . "','" . $tahap . "','" . $ini_komponen_jkn . "','" . $ini_komponen_jkk . "')";
                                    $stmt = $con->prepareStatement($query);
                                    budgetLogger::log('Menambah komponen baru dengan komponen name ' . $komponen_name . '(' . $komponen_id . ') pada dinas:' . $unit_id . ' dengan kode kegiatan :' . $kegiatan_code);
                                    $stmt->executeQuery();
                                    $con->commit();

                                    historyUserLog::tambah_komponen($unit_id, $kegiatan_code, $detail_no);
                                } catch (Exception $e) {
                                    $con->rollback();
                                    $this->setFlash('gagal', 'Tidak berhasil tersimpan karena ' . $e->getMessage());
                                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                                }
                            }
                            $this->setFlash('berhasil', 'Komponen Telah Berhasil Disimpan');
                        }//eof else $sub_koRek=='5.2.3' || $sub_koRek=='5.2.2'
                    }
//tambahan untuk gmap 9juni
                    if ($ada_fisik == 'TRUE') {
//                        $this->setFlash('gagal', 'Komponen Pekerjaan Fisik akan muncul setelah Anda memetakan lokasi pada Maps yang disediakan.');
                        $rd_cari = new Criteria();
                        $rd_cari->add(RincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                        $rd_cari->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                        $rd_cari->add(RincianDetailPeer::DETAIL_NO, $detailno_fisik, Criteria::EQUAL);
                        $rd_dapat = RincianDetailPeer::doSelectOne($rd_cari);
//                        $rd_dapat->setStatusHapus(TRUE);
//                        $rd_dapat->save();

                        $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detailno_fisik;

                        budgetLogger::log('Komponen Fisik' . $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName() . ' disembunyikan apabila tidak melakukan pemetaan dengan kode ' . $kode_rka);

                        $keisi = 0;

//                    ambil lama
                        $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                        if (count($lokasi_lama) > 0) {
                            foreach ($lokasi_lama as $value_lokasi_lama) {
                                $c_cari_lokasi = new Criteria();
                                $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                                $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                                $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                                if ($dapat_lokasi_lama) {

                                    $jalan_fix = '';
                                    $gang_fix = '';
                                    $nomor_fix = '';
                                    $rw_fix = '';
                                    $rt_fix = '';
                                    $keterangan_fix = '';
                                    $tempat_fix = '';

                                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                                    $gang_lama = $dapat_lokasi_lama->getGang();
                                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                                    $rw_lama = $dapat_lokasi_lama->getRw();
                                    $rt_lama = $dapat_lokasi_lama->getRt();
                                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                                    $tempat_lama = $dapat_lokasi_lama->getTempat();

                                    if ($jalan_lama <> '') {
                                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                                    }

                                    if ($tempat_lama <> '') {
                                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                                    }

                                    if ($gang_lama <> '') {
                                        $gang_fix = $gang_lama . ' ';
                                    }

                                    if ($nomor_lama <> '') {
                                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                                    }

                                    if ($rw_lama <> '') {
                                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                                    }

                                    if ($rt_lama <> '') {
                                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                                    }

                                    if ($keterangan_lama <> '') {
                                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                                    }

                                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                    $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detailno_fisik;
                                    $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                    $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                    $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                    $lokasi_per_titik = $lokasi_baru;

                                    $c_insert_gis = new HistoryPekerjaanV2();
                                    $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                    $c_insert_gis->setKodeRka($rka_lokasi);
                                    $c_insert_gis->setStatusHapus(FALSE);
                                    $c_insert_gis->setJalan(strtoupper($jalan_lama));
                                    $c_insert_gis->setGang(strtoupper($gang_lama));
                                    $c_insert_gis->setNomor(strtoupper($nomor_lama));
                                    $c_insert_gis->setRw(strtoupper($rw_lama));
                                    $c_insert_gis->setRt(strtoupper($rt_lama));
                                    $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                                    $c_insert_gis->setTempat(strtoupper($tempat_lama));
                                    $c_insert_gis->setKomponen($komponen_lokasi);
                                    $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                    $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                    $c_insert_gis->setLokasi($lokasi_per_titik);
                                    $c_insert_gis->save();
                                }
                            }
                        }

//                    buat baru
                        $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                        $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                        $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                        $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                        $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                        $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                        $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                        $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                        $total_array_lokasi = count($lokasi_jalan);

                        for ($i = 0; $i < $total_array_lokasi; $i++) {
                            $jalan_fix = '';
                            $gang_fix = '';
                            $tipe_gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';
                            if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                                if (trim($lokasi_jalan[$i]) <> '') {
                                    $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                                }

                                if (trim($lokasi_tempat[$i]) <> '') {
                                    $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                                }

                                if (trim($tipe_gang[$i]) <> '') {
                                    $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                                } else {
                                    $tipe_gang_fix = 'GG. ';
                                }

                                if (trim($lokasi_gang[$i]) <> '') {
                                    $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                                }

                                if (trim($lokasi_nomor[$i]) <> '') {
                                    $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                                }

                                if (trim($lokasi_rw[$i]) <> '') {
                                    $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                                }

                                if (trim($lokasi_rt[$i]) <> '') {
                                    $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                                }

                                if (trim($lokasi_keterangan[$i]) <> '') {
                                    $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                                }


                                $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detailno_fisik;
                                $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                $lokasi_per_titik = $lokasi_baru;

                                $c_insert_gis = new HistoryPekerjaanV2();
                                $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                $c_insert_gis->setKodeRka($rka_lokasi);
                                $c_insert_gis->setStatusHapus(FALSE);
                                $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                                $c_insert_gis->setGang(strtoupper($gang_fix));
                                $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                                $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                                $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                                $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                                $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                                $c_insert_gis->setKomponen($komponen_lokasi);
                                $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                $c_insert_gis->setLokasi($lokasi_per_titik);
                                $c_insert_gis->save();
                            }
                        }

                        $con = Propel::getConnection();
                        $query2 = "select * from master_kelompok_gmap where '" . $rd_dapat->getKomponenId() . "' ilike kode_kelompok||'%'";
                        $stmt2 = $con->prepareStatement($query2);
                        $rs2 = $stmt2->executeQuery();
                        while ($rs2->next()) {
                            $id_kelompok = $rs2->getString('id_kelompok');
                        }

                        if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                            $id_kelompok = 19;

                            if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                $id_kelompok = 100;
                            } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                $id_kelompok = 101;
                            }
                        }

                        historyUserLog::tambah_komponen_fisik_no_lokasi($unit_id, $kegiatan_code, $detailno_fisik);

                        return $this->redirect(sfConfig::get('app_path_gmap') . 'insertBaru.php?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code . '&detail_no=' . $detailno_fisik . '&satuan=' . $rd_dapat->getSatuan() . '&volume=' . $rd_dapat->getVolume() . '&nilai_anggaran=' . $rd_dapat->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=2&nm_user=' . $this->getUser()->getNamaLogin() . '&lokasi_ke=1');
                    } else {
                        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                    //tambahan untuk gmap 9juni
                }
                //eof status_pagu_rincian
//irul -> UNTUK REVISI
            } else {
                // irul 18maret 2014 - simpan catatan   
                $this->setFlash('gagal', 'Mohon maaf, Inputan Catatan Pergeseran Anggaran minimal 15 karakter');
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
            }
            // irul 18maret 2014 - simpan catatan   
            //irul -> UNTUK REVISI
        }//EOF simpan
    }

    protected function maxKodeSimbada() {
        $c = new Criteria();
        $c->clearSelectColumns();
        $c->addSelectColumn('MAX(' . masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA . ') ');
        $c->add(masterLokasiSimbadaPeer::KODE_LOKASI_SIMBADA, '%SIM%', Criteria::ILIKE);
        $rs = masterLokasiSimbadaPeer::doSelectRS($c);
        $rs->next();
//print_r($max);exit;
        $kodesimbada = $rs->get(1);
        ;
        /*

          $sql = "select max(kode_lokasi_simbada) as kode_simbada from gis_budgeting.public.master_lokasi_simbada where kode_sub ilike 'SIM%'";
          $con=Propel::getConnection();
          $stmt=$con->prepareStatement($sql);
          $rs=$stmt->executeQuery();
          while($rs->next())
          {
          $kodesimbada = $rs->getString('kode_simbada');
          } */
        $kode = substr($kodesimbada, 3, 5);
        $kode+=1;
        if ($kode < 10) {
            $kodesimbada = 'SIM0000' . $kode;
        } elseif ($kode < 100) {
            $kodesimbada = 'SIM000' . $kode;
        } elseif ($kode < 1000) {
            $kodesimbada = 'SIM00' . $kode;
        } elseif ($kode < 10000) {
            $kodesimbada = 'SIM0' . $kode;
        } elseif ($kode < 100000) {
            $kodesimbada = 'SIM' . $kode;
        }
        return $kodesimbada;
    }

    public function executeBuatbaru() {
        if ($this->getRequestParameter('baru') == md5('terbaru')) {
            $user = $this->getUser();

//sholeh begin
            $lokasiSession = $user->getAttribute('nama', '', 'lokasi');
            $this->lokasiSession = $lokasiSession;

            $user->setAttribute('nama', '', 'lokasi');
            $user->setAttribute('nama', '', 'lokasi_baru');
//sholeh end
//bisma begin
            $simbadaSession = $user->getAttribute('nama', '', 'simbada');
            $this->simbadaSession = $simbadaSession;

            $user->setAttribute('nama', '', 'simbada');
            $user->setAttribute('nama', '', 'simbada_baru');
//bisma end

            $user->removeCredential('lokasi');
            $user->removeCredential('simbada');

            $kode_kegiatan = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');
            $pajak = $this->getRequestParameter('pajak');
            $komponen_id = $this->getRequestParameter('komponen');
//print_r($komponen_id);exit;
            $tipe = $this->getRequestParameter('tipe');
            $kode_rekening = $this->getRequestParameter('rekening');

            if ($kode_rekening == '0') {
                $this->setFlash('gagal', 'Kode rekening belum dipilih');
                $cari = $this->getRequestParameter('cari');
                $this->redirect("dinas/carikomponen?filters[nama_komponen]=$cari&kegiatan=$kode_kegiatan&unit=$unit_id&filter=cari");
            }

            $komponen_id = trim($komponen_id);
            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $komponen_id, Criteria::ILIKE);
            $rs_komponen = KomponenPeer::doSelectOne($c);
            if ($rs_komponen) {

                $this->rs_komponen = $rs_komponen;
            }
//print_r($kode_rekening);exit;
            $sub_koRek = substr($kode_rekening, 0, 5);
//bisma : contreng
            /*  if($sub_koRek=='5.2.3' || $kode_rekening=='5.2.2.01.01' || $kode_rekening=='5.2.2.01.03' || $kode_rekening=='5.2.2.01.10' || $kode_rekening=='5.2.2.01.10' || $kode_rekening=='5.2.2.01.14'
              || $kode_rekening=='5.2.2.01.15' || $kode_rekening=='5.2.2.01.16' || $kode_rekening=='5.2.2.01.18' || $kode_rekening=='5.2.2.01.19' || $kode_rekening=='5.2.2.02.01'
              || $kode_rekening=='5.2.2.02.02' || $kode_rekening=='5.2.2.02.05' || $kode_rekening=='5.2.2.19.01' || $kode_rekening=='5.2.2.19.02' || $kode_rekening=='5.2.2.19.03'
              || $kode_rekening=='5.2.2.19.04')

             */
//if($sub_koRek=='5.2.3')
            if ($sub_koRek == '5.2.3' || $sub_koRek == '5.2.2' || $rekening_code == '5.2.2.01.01' || $rekening_code == '5.2.2.01.03' || $rekening_code == '5.2.2.01.10' || $rekening_code == '5.2.2.01.10' || $rekening_code == '5.2.2.01.14' || $rekening_code == '5.2.2.01.15' || $rekening_code == '5.2.2.01.16' || $rekening_code == '5.2.2.01.18' || $rekening_code == '5.2.2.01.19' || $rekening_code == '5.2.2.02.01' || $rekening_code == '5.2.2.02.02' || $rekening_code == '5.2.2.02.05' || $rekening_code == '5.2.2.19.01' || $rekening_code == '5.2.2.19.02' || $rekening_code == '5.2.2.19.03' || $rekening_code == '5.2.2.19.04') {

                $c_penyusun = new Criteria();
                $c_penyusun->add(KomponenPeer::RKA_MEMBER, TRUE);
                $c_penyusun->add(KomponenPeer::KOMPONEN_ID, '23.01.01.03.%', Criteria::ILIKE);
                $c_penyusun->setLimit(25);
                $c_penyusun->addAscendingOrderByColumn(KomponenPeer::KOMPONEN_ID);
                $rs_penyusun = KomponenPeer::doSelect($c_penyusun);
                $this->rs_penyusun = $rs_penyusun;
            }

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $d->add(SubtitleIndikatorPeer::PRIORITAS, '0', Criteria::NOT_EQUAL);
            $d->add(SubtitleIndikatorPeer::LOCK_SUBTITLE, FALSE);
            $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = SubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $g = new Criteria();
            $g->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $g->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($g);

            $kec = new Criteria();
            $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
            $rs_kec = KecamatanPeer::doSelect($kec);
            $this->nama_kecamatan_kel = $rs_kec;

            $c = new Criteria();
            $c->add(RekeningPeer::REKENING_CODE, $kode_rekening);
            if ($rs_rekening = RekeningPeer::doSelectOne($c)) {
                $akrual_rek = $rs_rekening->getAkrualKonstruksi();
            }
            $sub = "char_length(akrual_code)>10";
            $c = new Criteria();
            $c->add(AkrualPeer::AKRUAL_CODE, $sub, Criteria::CUSTOM);
            if ($akrual_rek) {
                $c->addAnd(AkrualPeer::AKRUAL_CODE, $akrual_rek . '%', Criteria::ILIKE);
            } elseif ($rs_komponen->getKomponenTipe2() == 'ATRIBUSI' || $rs_komponen->getKomponenTipe2() == 'PERENCANAAN' || $rs_komponen->getKomponenTipe2() == 'PENGAWASAN') {
                $c->addAnd(AkrualPeer::AKRUAL_CODE, '1.3.1%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.3%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.4%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.3.5%', Criteria::ILIKE);
                $c->addOr(AkrualPeer::AKRUAL_CODE, '1.5.3%', Criteria::ILIKE);
            }
            $c->addAscendingOrderByColumn(AkrualPeer::AKRUAL_CODE);
            $rs_akrualcode = AkrualPeer::doSelect($c);
            $this->rs_akrualcode = $rs_akrualcode;
        }
    }

    public function executeCarikomponen() {
        $kegiatan_code = $this->getRequestParameter('kegiatan');
        $unit_id = $this->getRequestParameter('unit');
        $status = 'LOCK';
        $c = new Criteria();
        $c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianPeer::UNIT_ID, $unit_id);
        $v = RincianPeer::doSelectOne($c);
        if ($v) {
            if ($v->getRincianLevel() == 1) {
                $status = 'LOCK';
            } else if ($v->getRincianLevel() == 2) {
                $status = 'OPEN';
            } else if ($v->getRincianLevel() == 3) {
                $status = 'LOCK';
            }

            if ($v->getLock() == TRUE) {
                $status = 'LOCK';
            }
        }
        if ($status == 'OPEN') {
            $this->filters = $this->getRequestParameter('filters');
            if (isset($this->filters['nama_komponen']) && $this->filters['nama_komponen'] !== '') {
                $cari = $this->filters['nama_komponen'];
            }
            $this->cari = $cari;
        } else {
            $this->setFlash('berhasil', 'Anda Tidak Berhak Memasuki Halaman Sebelumnya');
            $this->redirect("dinas/edit?kode_kegiatan=$kegiatan_code&unit_id=$unit_id");
        }
//print_r($this->getRequestParameter('kegiatan'));exit;
    }

    public function executePilihsubx() {
        $x = new Criteria();
        $x->addSelectColumn(RincianDetailPeer::SUBTITLE);
        $x->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
        $x->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
        if ($this->getRequestParameter('b') != '') {
            $s = new Criteria();
            $s->add(RincianDetailPeer::KEGIATAN_CODE, $this->getRequestParameter('kegiatan_code'));
            $s->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit_id'));
            $s->setDistinct();
            $r = RincianDetailPeer::doselect($s);
            $kodekegiatan = $this->getRequestParameter('kegiatan_code');
            $unitid = $this->getRequestParameter('unit_id');
            $kode_sub = $this->getRequestParameter('b');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_sub);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $subtitle = $rs_subtitle->getSubtitle();
            }

            $queryku = "select distinct kode_sub,sub from " . sfConfig::get('app_default_schema') . ".rincian_detail
                        where subtitle ilike '" . $subtitle . "%' and kegiatan_code='$kodekegiatan' and unit_id='$unitid' order by sub";


            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($queryku);
            $rcsb = $stmt->executeQuery();

            $arr_tampung = array();

            $this->isi_baru = array();
            while ($rcsb->next()) {
                if ($rcsb->getString('sub') != '') {
                    $arr_tampung[$rcsb->getString('kode_sub')] = $rcsb->getString('sub');
                }
            }
            $this->isi_baru = $arr_tampung;
        }
    }

    public function executeEditKegiatan() {
//print_r($this->getRequest());exit;
        $user = $this->getUser();
        $user->removeCredential('lokasi');
        if ($this->getRequestParameter('edit') == md5('ubah')) {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $rs_rinciandetail = RincianDetailPeer::doSelectOne($c);
            if ($rs_rinciandetail) {
                $volumeDiBudgeting = $rs_rinciandetail->getVolume();
                $pajak = $rs_rinciandetail->getPajak();
                $jasmas = $rs_rinciandetail->getKecamatan();
                $harga = $rs_rinciandetail->getKomponenHargaAwal();
                $nilaiDiBudgeting = $harga * $volumeDiBudgeting * (100 + $pajak) / 100;
                $this->nilaiMax = $nilaiDiBudgeting - $warning;
                $this->rs_rinciandetail = $rs_rinciandetail;

                $c = new Criteria();
                $c->add(KomponenPeer::KOMPONEN_ID, $rs_rinciandetail->getKomponenId());
                $rs_est_fisik = KomponenPeer::doSelectOne($c);
                $this->est_fisik = $rs_est_fisik->getIsEstFisik();
            }

            $d = new Criteria();
            $d->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $d->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kegiatan_code);
            $d->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitleindikator = SubtitleIndikatorPeer::doSelect($d);
            $this->rs_subtitleindikator = $rs_subtitleindikator;

            $e = new Criteria();
            $e->addAscendingOrderByColumn(SatuanPeer::SATUAN_NAME);
            $rs_satuan = SatuanPeer::doSelect($e);
            $this->rs_satuan = $rs_satuan;

            $f = new Criteria();
//$f->add(JasmasPeer::KODE_JASMAS, $jasmas);
            $f->addAscendingOrderByColumn(JasmasPeer::NAMA);
            $rs_jasmas = JasmasPeer::doSelect($f);
            $this->rs_jasmas = $rs_jasmas;

            $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

            $g = new Criteria();
            $g->add(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka);
            $g->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
            $g->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $g->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::ID_HISTORY);
            $geojson = HistoryPekerjaanV2Peer::doSelect($g);
            if ($geojson) {
                $this->rs_geojson = $geojson;
            }

            $h = new Criteria();
            $h->setDistinct(HistoryPekerjaanV2Peer::LOKASI);
            $h->add(HistoryPekerjaanV2Peer::TAHUN, 2015, Criteria::GREATER_THAN);
            $h->add(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
            $h->addAscendingOrderByColumn(HistoryPekerjaanV2Peer::LOKASI);
            $this->rs_jalan = $rs_jalan = HistoryPekerjaanV2Peer::doSelect($h);
        }

        if ($this->getRequestParameter('cari') == 'cari') {
//print_r($this->getRequestParameter('lokasi'));exit;
            $user = $this->getUser();
            $user->removeCredential('lokasi');
            $user->addCredential('lokasi');
            $user->setAttribute('nama', $this->getRequestParameter('lokasi'), 'lokasi');
            $user->setAttribute('kegiatan', $this->getRequestParameter('kegiatan'), 'lokasi');
            $user->setAttribute('unit', $this->getRequestParameter('unit'), 'lokasi');
            $user->setAttribute('id', $this->getRequestParameter('id'), 'lokasi');
            $user->setAttribute('ubah', 'ubah', 'lokasi');

            return $this->forward('lokasi', 'list');
        }//end of cari
        if ($this->getRequestParameter('simpan') == 'simpan') {
            $unit_id = $this->getRequestParameter('unit');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $detail_no = $this->getRequestParameter('id');

            $c_rincian_detail = new Criteria();
            $c_rincian_detail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincian_detail->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincian_detail->add(RincianDetailPeer::DETAIL_NO, $detail_no);
            $data_rincian_detail = RincianDetailPeer::doSelectOne($c_rincian_detail);

//            ambil untuk mbalikin ke WaitingList
            $data_lama_volume = $data_rincian_detail->getVolume();
            $data_lama_komponen_harga_awal = $data_rincian_detail->getKomponenHargaAwal();
            $data_lama_pajak = $data_rincian_detail->getPajak();
            $data_lama_keterangan_koefisien = $data_rincian_detail->getKeteranganKoefisien();
            $data_lama_satuan = $data_rincian_detail->getSatuan();
//            ambil untuk mbalikin ke WaitingList
            $c = new Criteria();
            $c->add(KomponenPeer::KOMPONEN_ID, $data_rincian_detail->getKomponenId());
            $rs_est_fisik = KomponenPeer::doSelectOne($c);
            $est_fisik = $rs_est_fisik->getIsEstFisik();

            $tipe2 = $data_rincian_detail->getTipe2();

            $lokasi_baru = '';
            $lokasi_array = array();

            $blud = 'false';
            if ($unit_id == '0300' || $unit_id == '1800') {
                if (is_null($this->getRequestParameter('blud'))) {
                    $blud = 'false';
                } else if ($this->getRequestParameter('blud') == 1) {
                    $blud = 'true';
                }
            }

            $musrenbang = 'false';
            if (is_null($this->getRequestParameter('musrenbang'))) {
                $musrenbang = 'false';
            } else if ($this->getRequestParameter('musrenbang') == 1) {
                $musrenbang = 'true';
            }

            $hibah = 'false';
            if (is_null($this->getRequestParameter('hibah'))) {
                $hibah = 'false';
            } else if ($this->getRequestParameter('hibah') == 1) {
                $hibah = 'true';
            }

            if (!$this->getRequestParameter('subtitle')) {
                $this->setFlash('gagal', 'Subtitle Belum Dipilih');
                return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
            }
            if (($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) && (!$this->getRequestParameter('kecamatan') || !$this->getRequestParameter('kelurahan') )) {
                $this->setFlash('gagal', 'Untuk komponen Fisik, silahkan mengisi keterangan Kecamatan & Kelurahan');
                return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
            }
            if (($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) && !$this->getRequestParameter('lokasi_jalan') && !$this->getRequestParameter('lokasi_lama')) {
                $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
            }

//irul -> UNTUK REVISI
//irul 8mei2014 - simpan catatan skpd            
            $noteskpd = '';
            $noteskpd = $this->getRequestParameter('catatan');
            if (sfConfig::get('app_fasilitas_bukaCatatanPergeseran') == 'tutup') {
                $apakah_murni = 1;
            } else {
                $apakah_murni = 0;
            }

            if (strlen(str_replace(' ', '', $noteskpd)) < 15 && $apakah_murni == 0) {
                $this->setFlash('gagal', 'Mohon maaf, Inputan Catatan Pergeseran Anggaran minimal 15 karakter');
                return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
            } else {
                //irul 8mei2014 - simpan catatan skpd
                //irul -> UNTUK REVISI
                //untuk cek rekening
                $c = new Criteria();
                $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                $rs_rdRekening = RincianDetailPeer::doSelectOne($c);
                if ($rs_rdRekening) {
                    $rekening_lama = $rs_rdRekening->getRekeningCode();
                    $vol_Lama = $rs_rdRekening->getVolume();
                }

                $volume = 0;
                $keterangan_koefisien = '';

                if ($this->getRequestParameter('vol1') || $this->getRequestParameter('vol2') || $this->getRequestParameter('vol3') || $this->getRequestParameter('vol4')) {
                    if ($this->getRequestParameter('vol2') == '') {
                        $vol2 = 1;
                        $volume = $this->getRequestParameter('vol1') * $vol2;
                        $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1');
                    } else if (!$this->getRequestParameter('vol2') == '') {
                        $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2');
                        $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2');
                    }
                    if ($this->getRequestParameter('vol3') == '') {
                        $vol3 = 1;
                        $volume = $volume * $vol3;
                    } else if (!$this->getRequestParameter('vol3') == '') {
                        $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3');
                        $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3');
                    }
                    if ($this->getRequestParameter('vol4') == '') {
                        $vol4 = 1;
                        $volume = $volume * $vol4;
                    } else if (!$this->getRequestParameter('vol4') == '') {
                        $volume = $this->getRequestParameter('vol1') * $this->getRequestParameter('vol2') * $this->getRequestParameter('vol3') * $this->getRequestParameter('vol4');
                        $keterangan_koefisien = $this->getRequestParameter('vol1') . ' ' . $this->getRequestParameter('volume1') . ' X ' . $this->getRequestParameter('vol2') . ' ' . $this->getRequestParameter('volume2') . ' X ' . $this->getRequestParameter('vol3') . ' ' . $this->getRequestParameter('volume3') . ' X ' . $this->getRequestParameter('vol4') . ' ' . $this->getRequestParameter('volume4');
                    }
                }

                //menambah fasilitas jika nilai rincian lebih dari pagu, maka dinas tidak bisa mengedit.

                $rd_function = new RincianDetail();

                $status_pagu_uk = 0;
                $array_buka_pagu_uk_khusus = array();
                if (!in_array($data_rincian_detail->getUnitId(), $array_buka_pagu_uk_khusus)) {
                    if ($data_rincian_detail->getRekeningCode() == '5.2.1.04.01') {
                        $status_pagu_uk = $rd_function->getBatasPaguPerDinasforEditUK($unit_id, $kegiatan_code, $detail_no, $volume);
                        $nilai_maks_uk = $rd_function->getNilaiPaguUKMaks($unit_id);
                        if ($status_pagu_uk == 1) {
                            $this->setFlash('gagal', 'Komponen tidak berhasil ditambahkan karena nilai Maks UK untuk SKPD Anda sebesar ' . number_format($nilai_maks_uk));
                            return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                        }
                    }
                }

                //irul 19juli2014 -ENTRI BUDGET 2015
                $array_buka_pagu_dinas_khusus = array('9999');
                $array_buka_pagu_kegiatan_khusus = array('');
                if (in_array($unit_id, $array_buka_pagu_dinas_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
                } else if (in_array($unit_id, $array_buka_pagu_kegiatan_khusus)) {
                    $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
                } else {
                    if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'buka') {
                        $kecamatan_unit_id = substr($unit_id, 0, 2);
                        if (1 == 0) {
                            $status_pagu_rincian = 0;
                        } else {
                            if (sfConfig::get('app_fasilitas_paguDinasBerdasarDinas') == 'buka') {
                                //batas pagu per dinas
                                $status_pagu_rincian = $rd_function->getBatasPaguPerDinasforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
                            } else if (sfConfig::get('app_fasilitas_paguDinasBerdasarKegiatan') == 'buka') {
                                //batas pagu per kegiatan 
                                $status_pagu_rincian = $rd_function->getBatasPaguPerKegiatanforEdit($unit_id, $kegiatan_code, $detail_no, $volume);
                            }
                        }
                    } else if (sfConfig::get('app_fasilitas_batasPaguDinas') == 'tutup') {
                        $status_pagu_rincian = 0;
                    }
                }

                if ($status_pagu_rincian == '1') {
                    $this->setFlash('gagal', 'Komponen tidak berhasil diubah karena. nilai total RKA Melebihi total Pagu kegiatan pada SKPD.');
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                } else if ($status_pagu_rincian == '0') {
                    //end of fasilitas
                    if ($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) {

                        $keisi = 0;

//                    ambil lama
                        $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                        if (count($lokasi_lama) > 0) {
                            foreach ($lokasi_lama as $value_lokasi_lama) {
                                $c_cari_lokasi = new Criteria();
                                $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                                $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                                $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                                if ($dapat_lokasi_lama) {

                                    $jalan_fix = '';
                                    $gang_fix = '';
                                    $nomor_fix = '';
                                    $rw_fix = '';
                                    $rt_fix = '';
                                    $keterangan_fix = '';
                                    $tempat_fix = '';

                                    $jalan_lama = $dapat_lokasi_lama->getJalan();
                                    $gang_lama = $dapat_lokasi_lama->getGang();
                                    $nomor_lama = $dapat_lokasi_lama->getNomor();
                                    $rw_lama = $dapat_lokasi_lama->getRw();
                                    $rt_lama = $dapat_lokasi_lama->getRt();
                                    $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                                    $tempat_lama = $dapat_lokasi_lama->getTempat();

                                    if ($jalan_lama <> '') {
                                        $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                                    }

                                    if ($tempat_lama <> '') {
                                        $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                                    }

                                    if ($gang_lama <> '') {
                                        $gang_fix = $gang_lama . ' ';
                                    }

                                    if ($nomor_lama <> '') {
                                        $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                                    }

                                    if ($rw_lama <> '') {
                                        $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                                    }

                                    if ($rt_lama <> '') {
                                        $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                                    }

                                    if ($keterangan_lama <> '') {
                                        $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                                    }

                                    if ($keisi == 0) {
                                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                        $keisi++;
                                    } else {
                                        $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                        $keisi++;
                                    }
                                }
                            }
                        }

//                    buat baru

                        $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                        $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                        $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                        $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                        $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                        $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                        $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                        $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                        $total_array_lokasi = count($lokasi_jalan);

                        for ($i = 0; $i < $total_array_lokasi; $i++) {
                            $jalan_fix = '';
                            $gang_fix = '';
                            $tipe_gang_fix = '';
                            $nomor_fix = '';
                            $rw_fix = '';
                            $rt_fix = '';
                            $keterangan_fix = '';
                            $tempat_fix = '';
                            if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                                if (trim($lokasi_jalan[$i]) <> '') {
                                    $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                                }

                                if (trim($lokasi_tempat[$i]) <> '') {
                                    $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                                }

                                if (trim($tipe_gang[$i]) <> '') {
                                    $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                                } else {
                                    $tipe_gang_fix = 'GG. ';
                                }

                                if (trim($lokasi_gang[$i]) <> '') {
                                    $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                                }

                                if (trim($lokasi_nomor[$i]) <> '') {
                                    $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                                }

                                if (trim($lokasi_rw[$i]) <> '') {
                                    $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                                }

                                if (trim($lokasi_rt[$i]) <> '') {
                                    $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                                }

                                if (trim($lokasi_keterangan[$i]) <> '') {
                                    $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                                }

                                if ($keisi == 0) {
                                    $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                    $keisi++;
                                } else {
                                    $lokasi_baru = $lokasi_baru . ', ' . $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;
                                    $keisi++;
                                }
                            }
                        }
                        if ($keisi == 0) {
                            $this->setFlash('gagal', 'Lokasi Belum Dipilih');
                            return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        }
                    }


                    $detail_name = '';
                    $kode_sub = '';
                    $sub = '';
                    //$kode_jasmas='';
                    $kode_jasmas = $this->getRequestParameter('jasmas');

                    if ($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) {
                        $kode_lokasi = '';
                        if ($lokasi_baru == '') {
                            $detail_name = '';
                        } else {
                            $detail_name = '(' . $lokasi_baru . ')';
                        }
                        $kode_jasmas = $this->getRequestParameter('jasmas');
                        $c = new Criteria();
                        $c->add(VLokasiPeer::NAMA, $detail_name);
                        $rs_lokasi = VLokasiPeer::doSelectOne($c);
                        if ($rs_lokasi) {
                            $kode_lokasi = $rs_lokasi->getKode();
                        }
                        /*
                          if($kode_lokasi=='')
                          {
                          $this->setFlash('lokasitidakada', 'Lokasi Tidak Ada atau Tidak Valid dengan Data G.I.S');
                          return $this->redirect("dinas/editKegiatan?id=$detail_no&unit=$unit_id&kegiatan=$kegiatan_code&edit=".md5('ubah'));
                          }
                         */
                    } else {
                        $detail_name = $this->getRequestParameter('keterangan');
                        $kode_jasmas = '';
                    }
                    if ($this->getRequestParameter('sub')) {
                        $kode_sub = $this->getRequestParameter('sub');
                        //print_r($kode_sub);exit;
                        $cekKodeSub = substr($kode_sub, 0, 4);
                        if ($cekKodeSub == 'RKAM') {//RKA Member
                            $C_RKA = new Criteria();
                            $C_RKA->add(RkaMemberPeer::KODE_SUB, $kode_sub);
                            $rs_rkam = RkaMemberPeer::doSelectOne($C_RKA);
                            if ($rs_rkam) {
                                $sub = $rs_rkam->getKomponenName();
                                $sub = trim($sub);
                            }
                        } else {
                            $d = new Criteria();
                            $d->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
                            $rs_rinciansubparameter = RincianSubParameterPeer::doSelectOne($d);
                            if ($rs_rinciansubparameter) {
                                $sub = $rs_rinciansubparameter->getNewSubtitle();
                                $sub = trim($sub);
                            }
                        }
                    }
                    $kode_subtitle = $this->getRequestParameter('subtitle');

                    $c = new Criteria();
                    $c->add(SubtitleIndikatorPeer::SUB_ID, $kode_subtitle);
                    $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
                    if ($rs_subtitle) {
                        $subtitle = $rs_subtitle->getSubtitle();
                    }

                    $sekarang = date('Y-m-d H:i:s');

                    //irul - ambil tahap tabel master_kegiatan
                    $c_master_kegiatan = new Criteria();
                    $c_master_kegiatan->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
                    $c_master_kegiatan->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
                    $rs_master_kegiatan = MasterKegiatanPeer::doSelectOne($c_master_kegiatan);
                    $tahap = $rs_master_kegiatan->getTahap();
                    //irul - ambil tahap tabel master_kegiatan

                    $c = new Criteria();
                    $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
                    $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                    $rincian_detail = RincianDetailPeer::doSelectOne($c);
                    if ($rincian_detail) {
                        $volumeDiBudgeting = $rincian_detail->getVolume();
                        $pajak = $rincian_detail->getPajak();
                        $harga = $rincian_detail->getKomponenHargaAwal();

                        $nilaiDiBudgeting = $harga * $volumeDiBudgeting * (100 + $pajak) / 100;
                        //$nilaiBaru = round($harga * $volume * (100 + $pajak) / 100);
                        $nilaiBaru = $rincian_detail->getNilaiAnggaran();
                        sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                        $rincian_detail->setKeteranganKoefisien($keterangan_koefisien);
                        $rincian_detail->setDetailName($detail_name);
                        $rincian_detail->setVolume($volume);
                        $rincian_detail->setSubtitle($subtitle);
                        $rincian_detail->setSub($sub);
                        $rincian_detail->setKodeSub($kode_sub);
                        $rincian_detail->setKecamatan($kode_jasmas);
                        $rincian_detail->setNoteSkpd($noteskpd);
                        $rincian_detail->setLastEditTime($sekarang);
                        $rincian_detail->setTahap($tahap);

                        //begin of kecamatan kelurahan
                        if ($this->getRequestParameter('kecamatan')) {
                            $lokasi_kec = $this->getRequestParameter('kecamatan');
                            $lokasi_kel = $this->getRequestParameter('kelurahan');
                            $kec = new Criteria();
                            $kec->add(KecamatanPeer::ID, $lokasi_kec);
                            $kec->addAscendingOrderByColumn(KecamatanPeer::NAMA);
                            $rs_kec = KecamatanPeer::doSelectOne($kec);
                            if ($rs_kec) {
                                $kecamatan = $rs_kec->getNama();
                            }

                            $kel = new Criteria();
                            $kel->add(KelurahanKecamatanPeer::OID, $lokasi_kel);
                            $kel->addAscendingOrderByColumn(KelurahanKecamatanPeer::NAMA_KECAMATAN);
                            $rs_kel = KelurahanKecamatanPeer::doSelectOne($kel);
                            if ($rs_kel) {
                                $kelurahan = $rs_kel->getNamaKelurahan();
                                $rincian_detail->setLokasiKecamatan($kecamatan);
                                $rincian_detail->setLokasiKelurahan($kelurahan);
                            } else {
                                $kecamatan = '';
                                $kelurahan = '';
                            }
                        }

                        $totNilaiSwakelola = 0;
                        $totNilaiKontrak = 0;
                        $totNilaiAlokasi = 0;
                        $totNilaiHps = 0;
                        $ceklelangselesaitidakaturanpembayaran = 0;
                        $totNilaiKontrakTidakAdaAturanPembayaran = 0;
                        $lelang = 0;

                        $rd = new RincianDetail();
                        //if(!($unit_id == '2600' && $kegiatan_code == '1.03.31.0002' && ($rincian_detail->getKomponenId() == '23.02.04.04.98'))){
                        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                            $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);
                            if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                    $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                                    $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);

                                    $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                                    $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                                    $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kode_kegiatan, $detail_no);
                                }
                            }
                        }
                        //}
                        //irul 25 february - tarik otomatis perkomponen
                        if (($nilaiBaru < $totNilaiKontrak) || ($nilaiBaru < $totNilaiSwakelola)) {
                            if ($totNilaiKontrak == 0) {
                                $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                            } else if ($totNilaiSwakelola == 0) {
                                $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                            } else {
                                $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                            }
                            return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($nilaiBaru < $totNilaiHps) {
                            $this->setFlash('gagal', 'Mohon maaf , lebih kecil dari Nilai HPS Per Komponen , sejumlah Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                            return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                            $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun Belum ada isian Aturan Pembayaran eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu ');
                            return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($lelang > 0) {
                            $this->setFlash('gagal', 'Sedang dalam Proses Lelang untuk komponen ini');
                            return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                            $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                            return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                        } else {
                            //irul 25 february - tarik otomatis perkomponen
                            if ($nilaiBaru < $totNilaiAlokasi) {
                                $I = new Criteria();
                                $I->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
                                $I->add(RincianDetailBpPeer::KEGIATAN_CODE, $kegiatan_code);
                                $I->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
                                $rd_bp = RincianDetailBpPeer::doSelectOne($I);
                                if ($rd_bp) {
                                    try {
                                        $rd_bp->setKeteranganKoefisien($keterangan_koefisien);
                                        $rd_bp->setVolume($volume);
                                        $rd_bp->setDetailName($detail_name);
                                        $rd_bp->setSubtitle($subtitle);
                                        $rd_bp->setSub($sub);
                                        $rd_bp->setNoteSkpd($noteskpd);
                                        $rd_bp->setKodeSub($kode_sub);
                                        $rd_bp->setKecamatan($kode_jasmas);
                                        $rd_bp->setIsPerKomponen('true');
                                        $rd_bp->setTahap($tahap);
                                        $rd_bp->save();

                                        budgetLogger::log('Mengupdate volume eProject untuk komponen menjadi ' . $volume . ' dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                                    } catch (Exception $ex) {
                                        $this->setFlash('gagal', 'Gagal karena' . $ex);
                                        return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                                    }
                                }
                            }
                            //irul 25 february - tarik otomatis perkomponen
                            //irul 27 feb 2014 - fitur BLUD
                            if ($unit_id == '1800' || $unit_id == '0300') {
                                if ($blud == 'false') {
                                    $rincian_detail->setIsBlud(false);
                                } else {
                                    $rincian_detail->setIsBlud(true);
                                }
                            }
                            //irul 27 feb 2014 - fitur BLUD

                            if ($musrenbang == 'false') {
                                $rincian_detail->setIsMusrenbang(false);
                            } else {
                                $rincian_detail->setIsMusrenbang(true);
                            }

                            if ($hibah == 'false') {
                                $rincian_detail->setIsHibah(false);
                            } else {
                                $rincian_detail->setIsHibah(true);
                            }

                            if ($this->getRequestParameter('akrual_code')) {
                                $akrual_code = $this->getRequestParameter('akrual_code');
                                $akrual_lama = explode('|', $data_rincian_detail->getAkrualCode());

                                if ($akrual_lama[0] != $akrual_code) {
                                    $akrual_code_baru = $akrual_code . '|01';
                                    $no_akrual_code = DinasRincianDetailPeer::AmbilUrutanAkrual($akrual_code_baru);
                                    $akrual_code_baru = $akrual_code . '|' . $akrual_lama[1] . $no_akrual_code;
                                } else {
                                    $akrual_code_baru = $data_rincian_detail->getAkrualCode();
                                }

                                $rincian_detail->setAkrualCode($akrual_code_baru);
                            }

                            sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
                            budgetLogger::log('Mengubah komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                            $rincian_detail->save();

                            historyUserLog::ubah_komponen_volume($unit_id, $kegiatan_code, $detail_no, $volumeDiBudgeting);

                            $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan.');

                            //irul 10 desember 2014 - edit data gmap
                            $sekarang = date('Y-m-d H:i:s');
                            $c2 = new Criteria();
                            $c2->add(RincianDetailPeer::UNIT_ID, $unit_id);
                            $c2->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                            $c2->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                            $rincian_detail2 = RincianDetailPeer::doSelectOne($c2);
                            if ($rincian_detail2) {
                                $query_cek_gmap = "select count(*) as jumlah "
                                        . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                                $con = Propel::getConnection();
                                $stmt = $con->prepareStatement($query_cek_gmap);
                                $rs = $stmt->executeQuery();
                                while ($rs->next()) {
                                    $jumlah = $rs->getString('jumlah');
                                }
                                if ($jumlah > 0) {
                                    $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                            . "set last_edit_time = '$sekarang', satuan = '" . $rincian_detail2->getSatuan() . "', volume = " . $rincian_detail2->getVolume() . ", nilai_anggaran = " . $rincian_detail2->getNilaiAnggaran() . ", komponen_name = '" . $rincian_detail2->getKomponenName() . "'  "
                                            . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                                    $stmt = $con->prepareStatement($query);
                                    $stmt->executeQuery();
                                }
                            }
                            //irul 10 desember 2014 - edit data gmap


                            if ($tipe2 == 'KONSTRUKSI' || $data_rincian_detail->getTipe() == 'FISIK' || $est_fisik) {
                                $rd_cari = new Criteria();
                                $rd_cari->add(RincianDetailPeer::UNIT_ID, $unit_id, Criteria::EQUAL);
                                $rd_cari->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code, Criteria::EQUAL);
                                $rd_cari->add(RincianDetailPeer::DETAIL_NO, $detail_no, Criteria::EQUAL);
                                $rd_dapat = RincianDetailPeer::doSelectOne($rd_cari);

                                $kode_rka_fix = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;

                                $c_cari_history = new Criteria();
                                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::KODE_RKA, $kode_rka_fix);
                                $c_cari_history->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                                $dapat_history = HistoryPekerjaanV2Peer::doSelect($c_cari_history);
                                if ($dapat_history) {
                                    foreach ($dapat_history as $value_history) {
                                        $id_history = $value_history->getIdHistory();

                                        $c_cari_hapus_history = new Criteria();
                                        $c_cari_hapus_history->addAnd(HistoryPekerjaanV2Peer::ID_HISTORY, $id_history);
                                        $dapat_history_hapus = HistoryPekerjaanV2Peer::doSelectOne($c_cari_hapus_history);
                                        if ($dapat_history_hapus) {
                                            $dapat_history_hapus->delete();
                                        }
                                    }
                                }

                                $keisi = 0;

//                    ambil lama
                                $lokasi_lama = $this->getRequestParameter('lokasi_lama');

                                if (count($lokasi_lama) > 0) {
                                    foreach ($lokasi_lama as $value_lokasi_lama) {
                                        $c_cari_lokasi = new Criteria();
                                        $c_cari_lokasi->add(HistoryPekerjaanV2Peer::LOKASI, $value_lokasi_lama);
                                        $c_cari_lokasi->addAnd(HistoryPekerjaanV2Peer::STATUS_HAPUS, FALSE);
                                        $dapat_lokasi_lama = HistoryPekerjaanV2Peer::doSelectOne($c_cari_lokasi);
                                        if ($dapat_lokasi_lama) {

                                            $jalan_fix = '';
                                            $gang_fix = '';
                                            $nomor_fix = '';
                                            $rw_fix = '';
                                            $rt_fix = '';
                                            $keterangan_fix = '';
                                            $tempat_fix = '';

                                            $jalan_lama = $dapat_lokasi_lama->getJalan();
                                            $gang_lama = $dapat_lokasi_lama->getGang();
                                            $nomor_lama = $dapat_lokasi_lama->getNomor();
                                            $rw_lama = $dapat_lokasi_lama->getRw();
                                            $rt_lama = $dapat_lokasi_lama->getRt();
                                            $keterangan_lama = $dapat_lokasi_lama->getKeterangan();
                                            $tempat_lama = $dapat_lokasi_lama->getTempat();

                                            if ($jalan_lama <> '') {
                                                $jalan_fix = 'JL. ' . strtoupper($jalan_lama) . ' ';
                                            }

                                            if ($tempat_lama <> '') {
                                                $tempat_fix = '(' . strtoupper($tempat_lama) . ') ';
                                            }

                                            if ($gang_lama <> '') {
                                                $gang_fix = $gang_lama . ' ';
                                            }

                                            if ($nomor_lama <> '') {
                                                $nomor_fix = 'NO ' . strtoupper($nomor_lama) . ' ';
                                            }

                                            if ($rw_lama <> '') {
                                                $rw_fix = 'RW ' . strtoupper($rw_lama) . ' ';
                                            }

                                            if ($rt_lama <> '') {
                                                $rt_fix = 'RT ' . strtoupper($rt_lama) . ' ';
                                            }

                                            if ($keterangan_lama <> '') {
                                                $keterangan_fix = '' . strtoupper($keterangan_lama) . ' ';
                                            }

                                            $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                            $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
                                            $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                            $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                            $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                            $lokasi_per_titik = $lokasi_baru;

                                            $c_insert_gis = new HistoryPekerjaanV2();
                                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                            $c_insert_gis->setKodeRka($rka_lokasi);
                                            $c_insert_gis->setStatusHapus(FALSE);
                                            $c_insert_gis->setJalan(strtoupper($jalan_lama));
                                            $c_insert_gis->setGang(strtoupper($gang_lama));
                                            $c_insert_gis->setNomor(strtoupper($nomor_lama));
                                            $c_insert_gis->setRw(strtoupper($rw_lama));
                                            $c_insert_gis->setRt(strtoupper($rt_lama));
                                            $c_insert_gis->setKeterangan(strtoupper($keterangan_lama));
                                            $c_insert_gis->setTempat(strtoupper($tempat_lama));
                                            $c_insert_gis->setKomponen($komponen_lokasi);
                                            $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                            $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                            $c_insert_gis->setLokasi($lokasi_per_titik);
                                            $c_insert_gis->save();
                                        }
                                    }
                                }

//                    buat baru
                                $lokasi_jalan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_jalan')));
                                $lokasi_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_gang')));
                                $tipe_gang = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('tipe_gang')));
                                $lokasi_nomor = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_nomor')));
                                $lokasi_rw = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rw')));
                                $lokasi_rt = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_rt')));
                                $lokasi_keterangan = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_keterangan')));
                                $lokasi_tempat = str_replace('\'', '', str_replace('"', '', $this->getRequestParameter('lokasi_tempat')));

                                $total_array_lokasi = count($lokasi_jalan);

                                for ($i = 0; $i < $total_array_lokasi; $i++) {
                                    $jalan_fix = '';
                                    $gang_fix = '';
                                    $tipe_gang_fix = '';
                                    $nomor_fix = '';
                                    $rw_fix = '';
                                    $rt_fix = '';
                                    $keterangan_fix = '';
                                    $tempat_fix = '';
                                    if (trim($lokasi_jalan[$i]) <> '' || trim($lokasi_tempat[$i]) <> '') {

                                        if (trim($lokasi_jalan[$i]) <> '') {
                                            $jalan_fix = 'JL. ' . strtoupper(trim($lokasi_jalan[$i])) . ' ';
                                        }

                                        if (trim($lokasi_tempat[$i]) <> '') {
                                            $tempat_fix = '(' . strtoupper(trim($lokasi_tempat[$i])) . ') ';
                                        }

                                        if (trim($tipe_gang[$i]) <> '') {
                                            $tipe_gang_fix = strtoupper(trim($tipe_gang[$i])) . '. ';
                                        } else {
                                            $tipe_gang_fix = 'GG. ';
                                        }

                                        if (trim($lokasi_gang[$i]) <> '') {
                                            $gang_fix = $tipe_gang_fix . '' . strtoupper(trim($lokasi_gang[$i])) . ' ';
                                        }

                                        if (trim($lokasi_nomor[$i]) <> '') {
                                            $nomor_fix = 'NO ' . strtoupper(trim($lokasi_nomor[$i])) . ' ';
                                        }

                                        if (trim($lokasi_rw[$i]) <> '') {
                                            $rw_fix = 'RW ' . strtoupper(trim($lokasi_rw[$i])) . ' ';
                                        }

                                        if (trim($lokasi_rt[$i]) <> '') {
                                            $rt_fix = 'RT ' . strtoupper(trim($lokasi_rt[$i])) . ' ';
                                        }

                                        if (trim($lokasi_keterangan[$i]) <> '') {
                                            $keterangan_fix = '' . strtoupper(trim($lokasi_keterangan[$i])) . ' ';
                                        }


                                        $lokasi_baru = $tempat_fix . '' . $jalan_fix . '' . $gang_fix . '' . $nomor_fix . '' . $rw_fix . '' . $rt_fix . '' . $keterangan_fix;

                                        $rka_lokasi = $unit_id . '.' . $kegiatan_code . '.' . $detail_no;
                                        $komponen_lokasi = $rd_dapat->getKomponenName() . ' ' . $rd_dapat->getDetailName();
                                        $kecamatan_lokasi = $rd_dapat->getLokasiKecamatan();
                                        $kelurahan_lokasi = $rd_dapat->getLokasiKelurahan();
                                        $lokasi_per_titik = $lokasi_baru;

                                        $c_insert_gis = new HistoryPekerjaanV2();
                                        $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                        $c_insert_gis->setKodeRka($rka_lokasi);
                                        $c_insert_gis->setStatusHapus(FALSE);
                                        $c_insert_gis->setJalan(strtoupper(trim($lokasi_jalan[$i])));
                                        $c_insert_gis->setGang(strtoupper($gang_fix));
                                        $c_insert_gis->setNomor(strtoupper(trim($lokasi_nomor[$i])));
                                        $c_insert_gis->setRw(strtoupper(trim($lokasi_rw[$i])));
                                        $c_insert_gis->setRt(strtoupper(trim($lokasi_rt[$i])));
                                        $c_insert_gis->setKeterangan(strtoupper(trim($lokasi_keterangan[$i])));
                                        $c_insert_gis->setTempat(strtoupper(trim($lokasi_tempat[$i])));
                                        $c_insert_gis->setKomponen($komponen_lokasi);
                                        $c_insert_gis->setKecamatan($kecamatan_lokasi);
                                        $c_insert_gis->setKelurahan($kelurahan_lokasi);
                                        $c_insert_gis->setLokasi($lokasi_per_titik);
                                        $c_insert_gis->save();
                                    }
                                }

//                                kalo waitinglist dinolin
                                $ada_waitinglist = 0;
                                $rd_cari_waitinglist = new Criteria();
                                $rd_cari_waitinglist->add(WaitingListPUPeer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no, Criteria::EQUAL);
                                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_WAITING, 1);
                                $rd_cari_waitinglist->addAnd(WaitingListPUPeer::STATUS_HAPUS, FALSE);

                                $ada_waitinglist = WaitingListPUPeer::doCount($rd_cari_waitinglist);

                                if ($ada_waitinglist > 0 && ($unit_id == '2600' || $unit_id == '2300') && $rd_dapat->getNilaiAnggaran() == 0) {
                                    $rd_dapat_waitinglist = WaitingListPUPeer::doSelectOne($rd_cari_waitinglist);

                                    $total_aktif = 0;
                                    $query = "select count(*) as total "
                                            . "from " . sfConfig::get('app_default_schema') . ".waitinglist_pu "
                                            . "where status_hapus = false and status_waiting = 0 "
                                            . "and unit_id = 'XXX$unit_id' and kegiatan_code = '" . $kegiatan_code . "'";
                                    $stmt = $con->prepareStatement($query);
                                    $rs = $stmt->executeQuery();
                                    while ($rs->next()) {
                                        $total_aktif = $rs->getString('total');
                                    }
                                    $total_aktif++;

                                    $balik_waitinglist = new WaitingListPU();
                                    $balik_waitinglist->setUnitId('XXX' . $unit_id);
                                    $balik_waitinglist->setKegiatanCode($kegiatan_code);
                                    $balik_waitinglist->setSubtitle($rd_dapat->getSubtitle());
                                    $balik_waitinglist->setKomponenId($rd_dapat->getKomponenId());
                                    $balik_waitinglist->setKomponenName($rd_dapat->getKomponenName());
                                    $balik_waitinglist->setKomponenLokasi($rd_dapat->getDetailName());
                                    $balik_waitinglist->setKomponenHargaAwal($data_lama_komponen_harga_awal);
                                    $balik_waitinglist->setPajak($data_lama_pajak);
                                    $balik_waitinglist->setKomponenSatuan($data_lama_satuan);
                                    $balik_waitinglist->setKomponenRekening($rd_dapat->getRekeningCode());
                                    $balik_waitinglist->setKoefisien($data_lama_keterangan_koefisien);
                                    $balik_waitinglist->setVolume($data_lama_volume);
                                    $balik_waitinglist->setTahunInput($rd_dapat->getTahun());
                                    $balik_waitinglist->setCreatedAt($sekarang);
                                    $balik_waitinglist->setUpdatedAt($sekarang);
                                    $balik_waitinglist->setStatusHapus(FALSE);
                                    $balik_waitinglist->setStatusWaiting(0);
                                    $balik_waitinglist->setKodeJasmas($rd_dapat->getKecamatan());
                                    $balik_waitinglist->setKecamatan($rd_dapat->getLokasiKecamatan());
                                    $balik_waitinglist->setKelurahan($rd_dapat->getLokasiKelurahan());
                                    $balik_waitinglist->setIsMusrenbang($rd_dapat->getIsMusrenbang());
                                    $balik_waitinglist->setPrioritas($total_aktif);
                                    $balik_waitinglist->setNilaiEe($rd_dapat_waitinglist->getNilaiEe());
                                    $balik_waitinglist->setKeterangan($rd_dapat_waitinglist->getKeterangan());
                                    $balik_waitinglist->save();

                                    $id_baru = $balik_waitinglist->getIdWaiting();

                                    $c_cari_history_lama = new Criteria();
                                    $c_cari_history_lama->add(HistoryPekerjaanV2Peer::KODE_RKA, $unit_id . '.' . $kegiatan_code . '.' . $detail_no);
                                    $c_cari_history_lama->addAnd(HistoryPekerjaanV2Peer::TAHUN, sfConfig::get('app_tahun_default'));
                                    $dapat_history_lama = HistoryPekerjaanV2Peer::doSelect($c_cari_history_lama);
                                    if ($dapat_history_lama) {
                                        foreach ($dapat_history_lama as $value_history_lama) {
                                            $c_insert_gis = new HistoryPekerjaanV2();
                                            $c_insert_gis->setTahun(sfConfig::get('app_tahun_default'));
                                            $c_insert_gis->setKodeRka('XXX' . $unit_id . '.' . $kegiatan_code . '.' . $id_baru);
                                            $c_insert_gis->setStatusHapus(FALSE);
                                            $c_insert_gis->setJalan($value_history_lama->getJalan());
                                            $c_insert_gis->setGang($value_history_lama->getGang());
                                            $c_insert_gis->setNomor($value_history_lama->getNomor());
                                            $c_insert_gis->setRw($value_history_lama->getRw());
                                            $c_insert_gis->setRt($value_history_lama->getRt());
                                            $c_insert_gis->setKeterangan($value_history_lama->getKeterangan());
                                            $c_insert_gis->setTempat($value_history_lama->getTempat());
                                            $c_insert_gis->setKomponen($value_history_lama->getKomponen());
                                            $c_insert_gis->setKecamatan($value_history_lama->getKecamatan());
                                            $c_insert_gis->setKelurahan($value_history_lama->getKelurahan());
                                            $c_insert_gis->setLokasi($value_history_lama->getLokasi());
                                            $c_insert_gis->save();
                                        }
                                    }
                                }
//                                kalo waitinglist dinolin
                            }
                        }
                    } else {
                        $this->setFlash('gagal', 'Mohon maaf, rincian yang dicari tidak ditemukan dalam database');
                        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                    }
                    return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
                }
                //eof fasilitas status_pagu_rincian
                //irul -> UNTUK REVISI
                //irul 8mei2014 - simpan catatan skpd
            }
//irul 8mei2014 - simpan catatan skpd
//irul -> UNTUK REVISI
//eof if simpan
        }
    }

    public function executeGetPekerjaans() {
        if ($this->getRequestParameter('id')) {
            $sub_id = $this->getRequestParameter('id');
            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::SUB_ID, $sub_id);
            $c->add(SubtitleIndikatorPeer::PRIORITAS, 0, Criteria::NOT_EQUAL);
            $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);
            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
            }

            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(RincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(RincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(RincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(RincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
//$c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::SUB);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addDescendingOrderByColumn(RincianDetailPeer::NILAI_ANGGARAN);

            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::LOKASI_KECAMATAN);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::LOKASI_KELURAHAN);
            $c_rincianDetail->addAscendingOrderByColumn(RincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = RincianDetailPeer::doSelect($c_rincianDetail);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeGetPekerjaansMasalah() {
        if ($this->getRequestParameter('id') == '0') {
            $sub_id = $this->getRequestParameter('id');
            $kegiatan_code = $this->getRequestParameter('kegiatan');
            $unit_id = $this->getRequestParameter('unit');

            $query = "select * "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail_masalah "
                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and tahun='" . sfConfig::get('app_tahun_default') . "' "
                    . "order by sub,kode_sub,rekening_code,komponen_name";
//print($query);exit;
            $con = Propel::getConnection();
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();

            $this->rs_rinciandetail = $rs_rinciandetail;
            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
            $this->setLayout('kosong');
        }
    }

    public function executeEdit() {
        if ($this->getRequestParameter('unit_id') && $this->getUser()->getUnitId() == $this->getRequestParameter('unit_id')) {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

            $c = new Criteria();
            $c->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $c->add(SubtitleIndikatorPeer::SUBTITLE, 'Penunjang Kinerja%', Criteria::NOT_ILIKE);
            $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::PRIORITAS);
            $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $rs_subtitle = SubtitleIndikatorPeer::doSelect($c);

            $this->rs_subtitle = $rs_subtitle;
            $this->rinciandetail = '';
            $this->rs_rinciandetail = '';

            if (sfConfig::get('app_fasilitas_warningRekening') == 'buka') {
//untuk warning
                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_1 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_2 = $rs->getString('tot');

                $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";
                $con = Propel::getConnection();
                $statement = $con->prepareStatement($query);
                $rs = $statement->executeQuery();
                while ($rs->next())
                    $total_menjadi_3 = $rs->getString('tot');

                if ($unit_id == '9999') {
                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_1 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_2 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".titiknol_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_3 = $rs->getString('tot');
                } else {
                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.1%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_1 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.2%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_2 = $rs->getString('tot');

                    $query = "select sum(nilai_anggaran) as tot
                              from " . sfConfig::get('app_default_schema') . ".prev_rincian_detail
                              where status_hapus=FALSE and unit_id='$unit_id' and kegiatan_code='$kode_kegiatan' and rekening_code like '5.2.3%' and status_hapus=false";

                    $con = Propel::getConnection();
                    $statement = $con->prepareStatement($query);
                    $rs = $statement->executeQuery();
                    while ($rs->next())
                        $total_semula_3 = $rs->getString('tot');
                }



                $this->selisih_1 = number_format($total_menjadi_1 - $total_semula_1, 0, ',', '.');
                $this->selisih_2 = number_format($total_menjadi_2 - $total_semula_2, 0, ',', '.');
                $this->selisih_3 = number_format($total_menjadi_3 - $total_semula_3, 0, ',', '.');

                $this->total_menjadi_1 = number_format($total_menjadi_1, 0, ',', '.');
                $this->total_menjadi_2 = number_format($total_menjadi_2, 0, ',', '.');
                $this->total_menjadi_3 = number_format($total_menjadi_3, 0, ',', '.');

                $this->total_semula_1 = number_format($total_semula_1, 0, ',', '.');
                $this->total_semula_2 = number_format($total_semula_2, 0, ',', '.');
                $this->total_semula_3 = number_format($total_semula_3, 0, ',', '.');
//end of warning
            }
        } else {
            $this->forward404();
        }
    }

    public function executeList() {

        $this->processSort();

        $this->processFilters();

        $this->filters = $this->getUser()->getAttributeHolder()->getAll('sf_admin/master_kegiatan/filters');

// pager
        $this->pager = new sfPropelPager('MasterKegiatan', 20);
        $c = new Criteria();
        $this->addSortCriteria($c);
        $user = $this->getUser();
        $nama = $user->getNamaUser();
        $d = new Criteria();
        $d->add(UnitKerjaPeer::UNIT_NAME, $nama);
        $rs_unitkerja = UnitKerjaPeer::doSelectOne($d);
        if ($rs_unitkerja) {
            $nama = $rs_unitkerja->getUnitId();
        }
        if ($nama != '') {
            $c->add(MasterKegiatanPeer::UNIT_ID, $nama, Criteria::ILIKE);
        }
        $this->addFiltersCriteria($c);
        $c->addAscendingOrderByColumn(MasterKegiatanPeer::KODE_KEGIATAN);
        $this->pager->setCriteria($c);
        $this->pager->setPage($this->getRequestParameter('page', 1));
        $this->pager->init();
    }

    protected function addFiltersCriteria($c) {
//print_r($this->getRequestParameter('unit_name'));exit;
        if (isset($this->filters['nama_kegiatan_is_empty'])) {
            $criterion = $c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, '');
            $criterion->addOr($c->getNewCriterion(MasterKegiatanPeer::NAMA_KEGIATAN, null, Criteria::ISNULL));
            $c->add($criterion);
        } else if (isset($this->filters['nama_kegiatan']) && $this->filters['nama_kegiatan'] !== '') {
            $c->add(MasterKegiatanPeer::NAMA_KEGIATAN, '%' . $this->filters['nama_kegiatan'] . '%', Criteria::ILIKE);
        }
    }

    public function executeTemplateRKA() {
//$this->setTemplate('templateRKA');
        $this->setLayout('kosong');
        $this->getResponse()->addStylesheet('tampilan_print2', '', array('media' => 'print'));
    }

    public function executeGetHeader() {
        $c = new Criteria();
        $c->add(MasterKegiatanPeer::KODE_KEGIATAN, $this->getRequestParameter('kegiatan'));
        $c->add(MasterKegiatanPeer::UNIT_ID, $this->getRequestParameter('unit'));
        $master_kegiatan = MasterKegiatanPeer::doSelectOne($c);
        if ($master_kegiatan) {
            $this->master_kegiatan = $master_kegiatan;
        }
        $this->setLayout('kosong');
    }

//irul 14 feb 2014 - ubah harga dasar
    public function executeUbahhargadasar() {
        $unit_id = $this->getRequestParameter('unit_id');
        $detail_no = $this->getRequestParameter('detail_no');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $komponen_harga_awal = $this->getRequestParameter('komponen_harga_awal');

        $c_mk = new Criteria();
        $c_mk->add(MasterKegiatanPeer::UNIT_ID, $unit_id);
        $c_mk->add(MasterKegiatanPeer::KODE_KEGIATAN, $kegiatan_code);
        $data_master_kegiatan = MasterKegiatanPeer::doSelectOne($c_mk);

        $c = new Criteria();
        $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
        $rincian_detail = RincianDetailPeer::doSelectOne($c);
        if ($rincian_detail) {
            $volumeDiBudgeting = $rincian_detail->getVolume();
            $pajak = $rincian_detail->getPajak();
            $harga = $rincian_detail->getKomponenHargaAwal();
            $satuan = $rincian_detail->getSatuan();

            $nilaiBaru = $komponen_harga_awal * 1;
            sfContext::getInstance()->getLogger()->debug('{eProject} rincian detail ketemu, nilai baru = ' . $nilaiBaru);
            $rincian_detail->setKeteranganKoefisien('1 Paket');
            $rincian_detail->setVolume(1);
            $rincian_detail->setKomponenHargaAwal($komponen_harga_awal);
            $rincian_detail->setTahapEdit(sfConfig::get('app_tahap_edit'));
            $rincian_detail->setTahap($data_master_kegiatan->getTahap());
            $rincian_detail->setPajak(0);
            $rincian_detail->setSatuan('Paket');
            $rincian_detail->setStatusLelang('lock');
            $rincian_detail->setHargaSebelumSisaLelang($harga);
            if ($satuan_semula == '') {
                $rincian_detail->setSatuanSemula($satuan);
            }

            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka' && sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                $nilaiTerpakai = 0;
                $totNilaiSwakelola = 0;
                $totNilaiKontrak = 0;
                $totNilaiAlokasi = 0;
                $totNilaiHps = 0;
                $ceklelangselesaitidakaturanpembayaran = 0;
                $lelang = 0;

                $rd = new RincianDetail();
                $totNilaiSwakelola = $rd->getCekNilaiSwakelolaDelivery2($unit_id, $kegiatan_code, $detail_no);
                $totNilaiKontrak = $rd->getCekNilaiKontrakDelivery2($unit_id, $kegiatan_code, $detail_no);
                $totNilaiAlokasi = $rd->getCekNilaiAlokasiProject($unit_id, $kegiatan_code, $detail_no);

                if ($totNilaiSwakelola > 0) {
                    $totNilaiSwakelola = $totNilaiSwakelola;
                    //$totNilaiSwakelola = $totNilaiSwakelola + 10;
                }
                if ($totNilaiKontrak > 0) {
                    $totNilaiKontrak = $totNilaiKontrak;
                    //$totNilaiKontrak = $totNilaiKontrak + 10;
                }

                if (!($rincian_detail->getKomponenId() == '27.01.01.01' || $rincian_detail->getKomponenId() == '27.01.01.152')) {
                    if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                        $totNilaiHps = $rd->getCekNilaiHPSKomponen($unit_id, $kegiatan_code, $detail_no);
                        $lelang = $rd->getCekLelang($unit_id, $kegiatan_code, $detail_no, $rincian_detail->getNilaiAnggaran());
                        $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($unit_id, $kegiatan_code, $detail_no);
                        $totNilaiKontrakTidakAdaAturanPembayaran = $rd->getCekNilaiDeliveryBelumAdaAturanPembayaran2($unit_id, $kegiatan_code, $detail_no);
                    }
                }

                if ($nilaiBaru < $totNilaiKontrak || $nilaiBaru < $totNilaiSwakelola) {
                    if ($totNilaiKontrak == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Swakelola Rp.' . number_format($totNilaiSwakelola, 0, ',', '.'));
                    } else if ($totNilaiSwakelola == 0) {
                        $this->setFlash('gagal', 'Mohon maaf , untuk komponen  ini sudah terpakai di edelivery, sejumlah Kontrak Rp.' . number_format($totNilaiKontrak, 0, ',', '.'));
                    }
                } else if ($nilaiBaru < $totNilaiHps) {
                    $this->setFlash('gagal', 'Mohon maaf , nilai HPS sebesar Rp.' . number_format($totNilaiHps, 0, ',', '.'));
                } else if ($ceklelangselesaitidakaturanpembayaran == 1) {
                    $this->setFlash('gagal', 'Proses Lelang untuk komponen ini telah selesai, namun belum ada Aturan Pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                } else if ($lelang > 0) {
                    $this->setFlash('gagal', 'Sedang Proses Lelang untuk komponen ini');
                } else if ($totNilaiKontrakTidakAdaAturanPembayaran == 1) {
                    $this->setFlash('gagal', 'Komponen ini belum ada isian aturan pembayaran di eDelivery. Silahkan mengisi Aturan Pembayaran terlebih dahulu.');
                } else {
                    if ($nilaiBaru < $totNilaiAlokasi) {
                        $I = new Criteria();
                        $I->add(RincianDetailBpPeer::UNIT_ID, $unit_id);
                        $I->add(RincianDetailBpPeer::KEGIATAN_CODE, $kegiatan_code);
                        $I->add(RincianDetailBpPeer::DETAIL_NO, $detail_no);
                        $rd_bp = RincianDetailBpPeer::doSelectOne($I);
                        if ($rd_bp) {
                            try {
                                $rd_bp->setKeteranganKoefisien('1 Paket');
                                $rd_bp->setVolume(1);
                                $rd_bp->setKomponenHargaAwal($komponen_harga_awal);
                                $rd_bp->setTahapEdit(sfConfig::get('app_tahap_edit'));
                                $rd_bp->setTahap($data_master_kegiatan->getTahap());
                                $rd_bp->setPajak(0);
                                $rd_bp->setIsPerKomponen(true);
                                $rd_bp->setSatuan('Paket');
                                $rd_bp->setStatusLelang('lock');
                                $rd_bp->setHargaSebelumSisaLelang($harga);
                                $rd_bp->save();
                                budgetLogger::log('Mengupdate volume eProject untuk komponen menjadi 1 dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                            } catch (Exception $ex) {
                                $this->setFlash('gagal', 'Gagal karena' . $ex);
                                return $this->redirect('dinas/editKegiatan?id=' . $detail_no . '&unit=' . $unit_id . '&kegiatan=' . $kegiatan_code . '&edit=' . md5('ubah'));
                            }
                        }
                    }

                    budgetLogger::log('Mengubah harga dasar komponen dari unit id :' . $unit_id . ' dengan kode :' . $kegiatan_code . '; detail_no :' . $detail_no . '; komponen_id:' . $rincian_detail->getKomponenId() . '; komponen_name:' . $rincian_detail->getKomponenName());
                    $rincian_detail->save();

                    historyUserLog::sisa_lelang_komponen($unit_id, $kegiatan_code, $detail_no, $harga, $volumeDiBudgeting, $satuan);

                    //irul 10 desember 2014 - edit data gmap
                    $sekarang = date('Y-m-d H:i:s');
                    $c2 = new Criteria();
                    $c2->add(RincianDetailPeer::UNIT_ID, $unit_id);
                    $c2->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
                    $c2->add(RincianDetailPeer::DETAIL_NO, $detail_no);
                    $rincian_detail2 = RincianDetailPeer::doSelectOne($c2);
                    if ($rincian_detail2) {
                        $query_cek_gmap = "select count(*) as jumlah "
                                . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query_cek_gmap);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $jumlah = $rs->getString('jumlah');
                        }
                        if ($jumlah > 0) {
                            $query = "update " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                                    . "set last_edit_time = '$sekarang', satuan = '" . $rincian_detail2->getSatuan() . "', volume = " . $rincian_detail2->getVolume() . ", nilai_anggaran = " . $rincian_detail2->getNilaiAnggaran() . ", komponen_name = '" . $rincian_detail2->getKomponenName() . "'  "
                                    . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and detail_no = $detail_no and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                            $stmt = $con->prepareStatement($query);
                            $stmt->executeQuery();
                        }
                    }
                    //irul 10 desember 2014 - edit data gmap
                    $this->setFlash('berhasil', 'Perubahan Telah Berhasil Dilakukan. Nilai eProject : ' . number_format($nilaiTerpakai, 0, ',', '.'));
                }
                return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
//EOF Cek ke deliveri dan eproject
            }
        }
    }

    public function executeNoteSkpd() {
//            return $this->redirect('dinas/noteSkpd?unit_id='.$this->getRequestParameter('unit_id'));


        if ($this->getRequestParameter('act') == 'editHeader') {
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');

            $c = new Criteria();
//            $c->addSelectColumn('note_skpd');
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(RincianDetailPeer::UNIT_ID, $this->getRequestParameter('unit'));
            $c->add(RincianDetailPeer::DETAIL_NO, $this->id);
            $notes = RincianDetailPeer::doSelectOne($c);
            $note = $notes->getNoteSkpd();
//            var_dump($note);
            $this->note_skpd = $note;
        }
        if ($this->getRequestParameter('act') == 'simpan') {
            $catatan = $this->getRequestParameter('catatan');
            $this->unit_id = $this->getRequestParameter('unit');
            $this->id = $this->getRequestParameter('id');
            $this->kegiatan = $this->getRequestParameter('kegiatan');
//                 echo $this->unit_id.'.'.$catatan;
            $c = new Criteria();
            $c->add(RincianDetailPeer::UNIT_ID, $this->unit_id);
            $c->add(RincianDetailPeer::KEGIATAN_CODE, $this->kegiatan);
            $c->add(RincianDetailPeer::DETAIL_NO, $this->id);
            $rincian_detail = RincianDetailPeer::doSelectOne($c);

            if (strlen(str_replace(' ', '', $catatan)) >= 15) {
                if ($rincian_detail) {

                    $c = new Criteria();
                    $c->add(SubtitleIndikatorPeer::UNIT_ID, $this->unit_id);
                    $c->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $this->kegiatan);
                    $c->add(SubtitleIndikatorPeer::SUBTITLE, $rincian_detail->getSubtitle());
                    $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::PRIORITAS);
                    $c->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
                    $rs_subtitle = SubtitleIndikatorPeer::doSelectOne($c);

                    $rincian_detail->setNoteSkpd($catatan);
                    $rincian_detail->save();

                    //return $this->layout('kosong');
                    return $this->redirect('dinas/edit?unit_id=' . $rincian_detail->getUnitId() . '&kode_kegiatan=' . $rincian_detail->getKegiatanCode());
                }
            } else {
                $this->setFlash('gagal', 'Mohon maaf , untuk inputan catatan minimal 15 karakter');
                return $this->redirect('dinas/edit?unit_id=' . $rincian_detail->getUnitId() . '&kode_kegiatan=' . $rincian_detail->getKegiatanCode());
            }
        }
    }

    public function executeSimulasi() {
//            $sub_id = $this->getRequestParameter('sub_id');
//            $this->sub_id = $sub_id;

        if ($this->getRequestParameter('sub_id')) {
//                    echo 'masuk';
            $sub_id = $this->getRequestParameter('sub_id');
            $c = new Criteria();
            $c->add(BappekoSubtitleIndikatorPeer::SUB_ID, $sub_id);
            $rs_subtitle = BappekoSubtitleIndikatorPeer::doSelectOne($c);

            if ($rs_subtitle) {
                $unit_id = $rs_subtitle->getUnitId();
                $kegiatan_code = $rs_subtitle->getKegiatanCode();
                $subtitle = $rs_subtitle->getSubtitle();
                $nama_subtitle = trim($subtitle);
//                                echo $unit_id;
            }
            $this->nama_subtitle = $nama_subtitle;
            $query = "select *  from " . sfConfig::get('app_default_schema') . ".bappeko_rincian_detail
				  where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and subtitle ilike '%$nama_subtitle%' and status_hapus=false order by sub,rekening_code,komponen_name";

            $con = Propel::getConnection(BappekoRincianDetailPeer::DATABASE_NAME);
            $statement = $con->prepareStatement($query);
            $rs_rinciandetail = $statement->executeQuery();
            $this->rs_rinciandetail = $rs_rinciandetail;
//                        var_dump($query);
            $c_rincianDetail = new Criteria();
            $c_rincianDetail->add(BappekoRincianDetailPeer::UNIT_ID, $unit_id);
            $c_rincianDetail->add(BappekoRincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
            $c_rincianDetail->add(BappekoRincianDetailPeer::SUBTITLE, $nama_subtitle, Criteria::ILIKE);
            $c_rincianDetail->add(BappekoRincianDetailPeer::STATUS_HAPUS, false);
            $c_rincianDetail->add(BappekoRincianDetailPeer::TAHUN, sfConfig::get('app_tahun_default'));
            $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::KODE_SUB);
            $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::REKENING_CODE);
            $c_rincianDetail->addAscendingOrderByColumn(BappekoRincianDetailPeer::KOMPONEN_NAME);
            $rs_rd = BappekoRincianDetailPeer::doSelect($c_rincianDetail);
//                        var_dump($rs_rd);
            $this->rs_rd = $rs_rd;

            $this->id = $sub_id;
            $this->rinciandetail = 'ada';
//			$this->setLayout('kosong');
            $this->setLayout('layoutPop');
        }
    }

    public function executeHome() {
        if ($this->getRequestParameter('Setuju')) {
//                return $this->redirect('dinas/home');
        }
    }

    public function executePilihKelurahan() {
        $this->id_kecamatan = $this->getRequestParameter('b');
    }

//irul 23 jan 2014
    public function executeCatatan() {
        $this->unit_id = $this->getRequestParameter('unit_id');
        $this->kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
        $this->catatan = $this->getRequestParameter('catatan');
    }

    public function executeCatatanDetail() {
        $catatan = $this->getRequestParameter('catatan');
        $unit_id = $this->getRequestParameter('unit_id');
        $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');

        $query = "update " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                . "set catatan='$catatan' where unit_id='$unit_id' and kode_kegiatan='$kode_kegiatan' and tahun='" . sfConfig::get('app_tahun_default') . "'";

        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $stmt->executeQuery();
        return $this->redirect('dinas/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

//irul 23 jan 2014
//irul 18 maret 2014
    public function executeNoteSkpdHeader() {
        $catatan = $this->getRequestParameter('catatan');
        $this->unit_id = $this->getRequestParameter('unit');
        $this->id = $this->getRequestParameter('id');
        $this->kegiatan = $this->getRequestParameter('kegiatan');
    }

//irul 18 maret 2014

    public function executeKonfirmasikegiatan() {
        try {
            $unit_id = $this->getRequestParameter('unit_id');
            $kode_kegiatan = $this->getRequestParameter('kode_kegiatan');
            $user_id = $this->getRequestParameter('user_id');

            $query = "update " . sfConfig::get('app_default_schema') . ".rincian 
                set rincian_confirmed = true, rincian_level = 3, lock = true, rincian_selesai = true 
                    where unit_id='$unit_id' and kegiatan_code = '$kode_kegiatan'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query);
            $stmt->executeQuery();

            budgetLogger::log('kode Kegiatan ' . $kode_kegiatan . ' dari unit id ' . $unit_id . ' telah dikonfirmasi oleh ' . $user_id);
            $this->setFlash('berhasil', "Kegiatan $kode_kegiatan telah berhasil dikonfirmasi dan dikunci oleh $user_id. Untuk perubahan yang bersifat Urgent, silahkan menghubungi Penyelia Anda.");
        } catch (Exception $exc) {
            $this->setFlash('gagal', $exc->getTraceAsString());
        }
        return $this->redirect('dinas/list?unit_id=' . $unit_id . '&kode_kegiatan=' . $kode_kegiatan);
    }

    public function executeSimpanCatatan() {
        $unit_id = $this->getRequestParameter('unit_id');
        $kegiatan_code = $this->getRequestParameter('kegiatan_code');
        $detail_no = $this->getRequestParameter('detail_no');
        $catatan_skpd = $this->getRequestParameter('catatan_skpd');

        $c = new Criteria();
        $c->add(RincianDetailPeer::UNIT_ID, $unit_id);
        $c->add(RincianDetailPeer::KEGIATAN_CODE, $kegiatan_code);
        $c->add(RincianDetailPeer::DETAIL_NO, $detail_no);
        $rs_rinciandetail = RincianDetailPeer::doSelectOne($c);
        $rs_rinciandetail->setNoteSkpd($catatan_skpd);
        $rs_rinciandetail->save();
        $this->setFlash('berhasil', 'Berhasil menyimpan Catatan SKPD.');

        return $this->redirect('dinas/edit?unit_id=' . $unit_id . '&kode_kegiatan=' . $kegiatan_code);
    }

    public function executeAjaxrefreshjalan() {
        
    }

}

?>
