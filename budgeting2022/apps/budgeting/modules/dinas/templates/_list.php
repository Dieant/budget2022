<div class="card-body table-responsive p-0">
    <table class="table table-hover">
        <thead class="head_peach">
            <tr> 
                <?php include_partial('list_th_tabular') ?>
                <th><?php echo __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($pager->getResults() as $master_kegiatan): $odd = fmod( ++$i, 2)
                ?>
                <tr class="sf_admin_row_<?php echo $odd ?>">
                    <?php include_partial('list_td_tabular', array('master_kegiatan' => $master_kegiatan)) ?>
                    <?php include_partial('list_td_tahap', array('master_kegiatan' => $master_kegiatan)) ?>
                    <?php include_partial('list_td_actions', array('master_kegiatan' => $master_kegiatan)) ?>
                </tr>
            <?php endforeach; ?>
            <tr class="sf_admin_row_3">
                <td colspan="2">&nbsp;</td>
                <?php if (sfConfig::get('app_tahap_edit') == 'murni') { ?>
                    <td align="right">
                        <?php
                        $query = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='" . $master_kegiatan->getUnitId() . "'";
                        $con = Propel::getConnection();
                        $statement = $con->prepareStatement($query);
                        $rs_nilai = $statement->executeQuery();
                        while ($rs_nilai->next()) {
                            $nilai_awal = $rs_nilai->getString('nilai');
                        }
                        echo "<b>".number_format($nilai_awal, 0, ',', '.')."</b>";
                        ?>
                    </td>
                <?php } ?>
                <!-- <td align="right">
                    <?php
                    //irul 19 feb 2014 - ngikut penyelia
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    if (sfConfig::get('app_tahap_edit') == 'murni') {
                        //digunakan ketika awal anggaran
                        $query = "select sum(alokasi_dana) as nilai from " . sfConfig::get('app_default_schema') . ".master_kegiatan where unit_id='$unit_id'";
                        //digunakan ketika sudah mulai akan dibandingkan dan tidak awal anggaran namun masih dalam tahap murni
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".murni_rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
                    }
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai_awal = $rs_nilai->getString('nilai');
                    }
                    echo number_format($nilai_awal, 0, ',', '.');
                    //irul 19 feb 2014 - ngikut penyelia
                    ?>
                </td> -->
                <td align="right">
                    <?php
                    //irul 19 feb 2014 - ngikut penyelia
                    $kode_kegiatan = $master_kegiatan->getKodeKegiatan();
                    $unit_id = $master_kegiatan->getUnitId();
                    $query = "select sum(nilai_anggaran) as nilai from " . sfConfig::get('app_default_schema') . ".rincian_detail where unit_id='$unit_id' and status_hapus=FALSE";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $nilai = $rs_nilai->getString('nilai');
                    }
                    echo "<b>".number_format($nilai, 0, ',', '.')."</b>";
                    //irul 19 feb 2014 - ngikut penyelia
                    ?>
                </td>
                <!-- <td align="right">
                    <?php
                    if (sfConfig::get('app_tahap_edit') == 'murni') {
                        $selisih = $nilai_awal - $nilai;
                    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
                        $selisih = $nilai - $nilai_awal;
                    }

                    echo number_format($selisih, 0, ',', '.');
                    ?>
                </td> -->
                <!-- <td align="right">
                    <?php
                    //irul 19 feb 2014 - ngikut penyelia
                    $query = "select sum(tambahan_pagu) as pagu_tambahan "
                            . "from " . sfConfig::get('app_default_schema') . ".master_kegiatan "
                            . "where unit_id='$unit_id' ";
                    $con = Propel::getConnection(RincianDetailPeer::DATABASE_NAME);
                    $statement = $con->prepareStatement($query);
                    $rs_nilai = $statement->executeQuery();
                    while ($rs_nilai->next()) {
                        $pagu_tambahan = $rs_nilai->getString('pagu_tambahan');
                    }
                    echo number_format($pagu_tambahan, 0, ',', '.');
                    //irul 19 feb 2014 - ngikut penyelia
                    ?>
                </td> -->
                <td colspan="4">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix">
    <ul class="pagination pagination-sm m-0 float-left">
        <?php
        echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults()) 
        ?>
    </ul>
    <ul class="pagination pagination-sm m-0 float-right">
        <?php 
        if ($pager->haveToPaginate()): 
            echo '<li class="page-item">'.link_to('Previous', 'dinas/list?page=' . $pager->getPreviousPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Previous'), 'title' => __('Previous'))).'</li>';

            foreach ($pager->getLinks() as $page):
                echo '<li class="page-item">'.link_to_unless($page == $pager->getPage(), $page, "dinas/list?page={$page}", array('class' => 'page-link')).'</li>';
            endforeach;
            echo '<li class="page-item">'.link_to('Next', 'dinas/list?page=' . $pager->getNextPage(), array('class' => 'page-link', 'align' => 'absmiddle', 'alt' => __('Next'), 'title' => __('Next'))).'</li>'; 
        endif;
        ?>
    </ul>
</div>