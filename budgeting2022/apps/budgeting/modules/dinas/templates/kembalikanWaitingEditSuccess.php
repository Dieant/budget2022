<?php  use_helper('Javascript','Form','Object'); ?>
<div>
  <?php
	echo form_tag("kegiatan/kembalikanWaiting");
        echo 'Keterangan :';
        echo textarea_tag('keterangan', null, array('placeholder'=>'Isi alasan kembali ke Waiting List (min 20 karakter)', 'minlength'=>'20'));
        echo input_hidden_tag('unit_id', $unit_id);
        echo input_hidden_tag('kode_kegiatan', $kode_kegiatan);
        echo input_hidden_tag('detail_no', $detail_no);
        echo input_hidden_tag('key', md5('kembali_waiting'));
        echo submit_tag(' OK ', array('class'=>'btn btn-default btn-flat btn-sm')) ;
?>
</div>