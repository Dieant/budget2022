<?php use_helper('I18N', 'Date', 'Object', 'Javascript', 'Validation') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Sub Kegiatan - Kertas Kerja</h1>
    </div>
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Lembar Kerja</a></li>
          <li class="breadcrumb-item active">Kertas Kerja</li>
      </ol>
  </div>
</div>
</div><!-- /.container-fluid -->
</section>
<!-- Main content -->

<!-- Main content -->
<section class="content">
    <?php include_partial('dinas/list_messages'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-filter"></i> Filter
                        </h3>
                    </div>
                    <div class="card-body">
                        <?php echo form_tag('dinas/list', array('method' => 'get', 'class' => 'form-horizontal')) ?>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Nama sub kegiatan</label>
                                    <?php
                                    echo input_tag('filters[nama_kegiatan]', isset($filters['nama_kegiatan']) ? $filters['nama_kegiatan'] : null, array('class' => 'form-control'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="tombol_filter">Tombol Filter</label><br/>
                                    <button type="submit" name="filter" class="btn btn-outline-primary btn-sm">Filter <i class="fas fa-search"></i></button>
                                    <?php
                                    echo link_to('Reset <i class="fa fa-backspace"></i>', 'dinas/list?filter=filter', array('class' => 'btn btn-outline-danger btn-sm'));
                                    ?>
                                </div>
                            </div>
                            <!-- /.col -->                      
                        </div>
                        <?php 
                        '</form>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">                 
                    <?php if (!$pager->getNbResults()): ?>
                        <?php echo __('no result') ?>
                        <?php else: ?>
                            <?php include_partial("dinas/list", array('pager' => $pager)) ?>
                    <?php endif; ?>            
                </div>
            </div>
        </div>
    </div>
</section>