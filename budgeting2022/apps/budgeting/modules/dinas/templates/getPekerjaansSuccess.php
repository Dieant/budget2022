<?php use_helper('Url', 'Javascript', 'Form', 'Object'); ?>
<?php
$status = 'CLOSED';
$i = 0;
$kode_sub = '';
$temp_rekening = '';

foreach ($rs_rd as $rd):
    $est_fisik = FALSE;
    $c = new Criteria();
    $c->add(KomponenPeer::KOMPONEN_ID, $rd->getKomponenId());
    $c->add(KomponenPeer::IS_EST_FISIK, TRUE);
    if ($rs_est_fisik = KomponenPeer::doSelectOne($c))
        $est_fisik = TRUE;

    $odd = fmod($i++, 2);
    $unit_id = $rd->getUnitId();
    $kegiatan_code = $rd->getKegiatanCode();

    if ($kode_sub != $rd->getKodeSub()) 
    {
        $kode_sub = $rd->getKodeSub();
        $sub = $rd->getSub();
        $cekKodeSub = substr($kode_sub, 0, 4);

        if ($cekKodeSub == 'RKAM') 
        {
            $C_RKA = new Criteria();
            $C_RKA->add(RkaMemberPeer::KODE_SUB, $kode_sub);
            $C_RKA->addAscendingOrderByColumn(RkaMemberPeer::KODE_SUB);
            $rs_rkam = RkaMemberPeer::doSelectOne($C_RKA);
            if ($rs_rkam) 
            {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7">
                        <?php
                        if (($status == 'OPEN') and ( $rd->getLockSubtitle() <> 'LOCK' and substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_')) {
                            echo link_to_function(image_tag('/sf/sf_admin/images/edit.png'), 'editHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                            echo link_to_function(image_tag('/sf/sf_admin/images/cancel.png'), 'hapusHeaderKegiatan(' . $id . ',"' . $kegiatan_code . '","' . $unit_id . '","' . $kode_sub . '")');
                        }
                        ?>
                    <div id="<?php echo 'header_' . $rs_rkam->getKodeSub() ?>"><b> .:. <?php echo $rs_rkam->getKomponenName() . ' ' . $rs_rkam->getDetailName(); ?></b></div>
                    </td>
                    <td align="right">
                        <?php
                        echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.');
                        ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            }
        } 
        else 
        { //else form SUB
            $c = new Criteria();
            $c->add(RincianSubParameterPeer::KODE_SUB, $kode_sub);
            $rs_subparameter = RincianSubParameterPeer::doSelectOne($c);
            if ($rs_subparameter) 
            {
                ?>
                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                    <td colspan="7">
                        <b> :. <?php echo $rs_subparameter->getSubKegiatanName() . ' ' . $rs_subparameter->getDetailName(); ?></b>
                    </td>
                    <td align="right">
                        <?php echo number_format($rd->getTotalKodeSub($kode_sub), 0, ',', '.'); ?>
                    </td>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <?php
            } else {
                $ada = 'tidak';
                $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_sub_parameter "
                . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and new_subtitle ilike '%$sub%'";
                $con = Propel::getConnection();
                $stmt = $con->prepareStatement($query);
                $t = $stmt->executeQuery();
                while ($t->next()) 
                {
                    if ($t->getString('kode_sub')) 
                    {
                        ?>
                        <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                            <td colspan="7">
                                <b> :. <?php echo $t->getString('sub_kegiatan_name') . ' ' . $t->getString('detail_name'); ?></b>
                            </td>
                            <td align="right">
                                <?php
                                echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                $ada = 'ada';
                                ?> 
                            </td>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                        <?php
                    }
                }

                if ($ada == 'tidak') 
                {
                    if ($kode_sub != '') 
                    {
                        $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail "
                        . "where unit_id='$unit_id' and kegiatan_code='$kegiatan_code' and sub = '$sub' and status_hapus=false";
                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $t = $stmt->executeQuery();
                        while ($t->next()) 
                        {
                            if ($t->getString('kode_sub')) 
                            {
                                ?>
                                <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                                    <td colspan="7">
                                    <b> :. <?php echo $t->getString('komponen_name') . ' ' . $t->getString('detail_name'); ?></b>
                                    </td>
                                    <td align="right">
                                        <?php
                                        echo number_format($rd->getTotalSub($sub), 0, ',', '.');
                                        $ada = 'ada';
                                        ?>
                                    </td>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                        <?php
                            }
                        }
                    }
                }
            }
        }    
    }

    $rekening_code = $rd->getRekeningCode();
    if ($temp_rekening != $rekening_code) {
        $temp_rekening = $rekening_code;
        $c = new Criteria();
        $c->add(RekeningPeer::REKENING_CODE, $rekening_code);
        $rs_rekening = RekeningPeer::doSelectOne($c);
        if ($rs_rekening) {
            $rekening_name = $rs_rekening->getRekeningName();
            ?>
            <tr class="pekerjaans_<?php echo $id ?>" bgcolor="#e6e6e6">
                <td colspan="7"><i><?php echo $rekening_code . ' ' . $rekening_name ?></i> </td>
                <td colspan="3">&nbsp;</td>
            </tr>
            <?php
        }
    }
    ?>
    <tr class="pekerjaans_<?php echo $id ?>">
        <td>
            <?php
            $kegiatan = $rd->getKegiatanCode();
            $unit = $rd->getUnitId();
            $no = $rd->getDetailNo();
            $sub = $rd->getSubtitle();

            $benar_musrenbang = 0;
            if ($rd->getIsMusrenbang() == 'TRUE') {
                $benar_musrenbang = 1;
            }
            $benar_multiyears = 0;
            if ($rd->getThKeMultiyears() <> null && $rd->getThKeMultiyears() > 0) {
                $benar_multiyears = 1;
            }      
            $benar_hibah = 0;
            if ($rd->getIsHibah() == 'TRUE') {
                $benar_hibah = 1;
            }
            if ($status == 'OPEN' && $rd->getLockSubtitle() <> 'LOCK') 
            {
            ?>
                <div class="btn-group">
                    <?php
                    echo link_to('<i class="fa fa-edit"></i> Edit Komponen', 'dinas/editKegiatan?id=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode() . '&edit=' . md5('ubah'), array('class' => 'btn btn-default btn-flat btn-sm'));
                    if (sfConfig::get('app_tahap_edit') == 'murni' && $benar_hibah == 0 && $benar_musrenbang == 0 && $benar_multiyears == 0 && substr($sf_user->getNamaLogin(), 0, 3) <> 'pa_') 
                    {

                    }
                    ?>
                </div>
                <div class="clearfix"></div>
                <?php
                //irul 25agustus 2014 - fungsi GMAP
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) 
                {
                    $con = Propel::getConnection();
                    $kode_rka = $unit_id . '.' . $kegiatan_code . '.' . $rd->getDetailNo();
                    $query_cek_waiting = "select id_waiting from ebudget.waitinglist_pu where kode_rka = '" . $kode_rka . "' ";
                    $stmt_cek_waiting = $con->prepareStatement($query_cek_waiting);
                    $rs_cek_waiting = $stmt_cek_waiting->executeQuery();
                    if ($rs_cek_waiting->next()) 
                    {
                    ?>
                        <div id="tempat_ajax_<?php echo str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() ?>">
                            <?php echo link_to_function('<i class="fa fa-trash"></i> Kembalikan ke Waiting List', 'execKembalikanWaiting("' . $rd->getDetailNo() . '","' . $rd->getUnitId() . '","' . $rd->getKegiatanCode() . '", "' . str_replace('.', '_', $kegiatan_code) . '_' . $rd->getDetailNo() . '")', array('class' => 'btn btn-default btn-flat btn-sm')); ?>
                        </div>
                    <?php
                    }
                    $id_kelompok = 0;
                    $tot = 0;
                    $query = "select count(*) as tot "
                    . "from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 "
                    . "where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' "
                    . "and tahun = '" . sfConfig::get('app_tahun_default') . "' and status_hapus = FALSE";
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $tot = $rs->getString('tot');
                    }
                    if ($tot == 0) 
                    {
                        $con = Propel::getConnection();
                        $c2 = new Criteria();
                        $c2->add(KomponenPeer::KOMPONEN_NAME, $rd->getKomponenName(), Criteria::ILIKE);
                        $c2->add(KomponenPeer::KOMPONEN_TIPE, 'FISIK', Criteria::EQUAL);
                        $c2->addOr(KomponenPeer::KOMPONEN_TIPE, 'EST', Criteria::EQUAL);
                        $c2->addOr(KomponenPeer::KOMPONEN_TIPE2, 'KONSTRUKSI', Criteria::EQUAL);
                        $rd2 = KomponenPeer::doSelectOne($c2);
                        if ($rd2) {
                            $komponen_id = $rd2->getKomponenId();
                            $satuan = $rd2->getSatuan();
                        } else {
                            $komponen_id = '0';
                            $satuan = '';
                        }

                        if ($komponen_id == '0') {
                            $query2 = "select * from master_kelompok_gmap "
                            . "where '" . $rd->getKomponenName() . "' ilike nama_objek||'%'";
                        } else {
                            $query2 = "select * from master_kelompok_gmap "
                            . "where '" . $komponen_id . "' ilike kode_kelompok||'%'";
                        }
                        $stmt2 = $con->prepareStatement($query2);
                        $rs2 = $stmt2->executeQuery();
                        while ($rs2->next()) {
                            $id_kelompok = $rs2->getString('id_kelompok');
                        }

                        if ($id_kelompok == '' || $id_kelompok == 0 || $id_kelompok == null) {
                            $id_kelompok = 19;
                            if (in_array($satuan, array('Kegiatan', 'Lokasi', 'M2', 'M²', 'm3', 'Paket', 'Set'))) {
                                $id_kelompok = 100;
                            } elseif (in_array($satuan, array('m', 'M', 'M1', 'Meter', 'Titik', 'Unit'))) {
                                $id_kelompok = 101;
                            }
                        }
                    } else {
                        $con = Propel::getConnection();
                        $query = "select * from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                        where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $mlokasi = $rs->getString('mlokasi');
                            $id_kelompok = $rs->getString('id_kelompok');
                        }

                        $query = "select max(lokasi_ke) as total_lokasi from " . sfConfig::get('app_default_gis') . ".geojsonlokasi_rev1 
                        where unit_id='" . $rd->getUnitId() . "' and kegiatan_code='" . $rd->getKegiatanCode() . "' and detail_no ='" . $rd->getDetailNo() . "' and tahun = '" . sfConfig::get('app_tahun_default') . "'";
                        $stmt = $con->prepareStatement($query);
                        $rs = $stmt->executeQuery();
                        while ($rs->next()) {
                            $total_lokasi = $rs->getString('total_lokasi');
                        }
                    }
                    ?>
                    <div class="btn-group">                            
                        <?php
                        echo link_to_function('<i class="fa fa-map-marker"></i>', '', array('class' => 'btn btn-default btn-flat btn-sm', 'disable' => true));
                        if ($tot == 0) {
                            $array_bersih_kurung = array('(', ')');
                            $lokasi_bersih_kurung = str_replace($array_bersih_kurung, '', $lokasi);
                            echo link_to('<i class="fa fa-edit"></i> Input Lokasi', sfConfig::get('app_path_gmap') . 'insertBaru.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                        } else {
                            echo link_to('<i class="fa fa-edit"></i> Edit Lokasi', sfConfig::get('app_path_gmap') . 'updateData.php?unit_id=' . $rd->getUnitId() . '&kode_kegiatan=' . $rd->getKegiatanCode() . '&detail_no=' . $rd->getDetailNo() . '&satuan=' . $rd->getSatuan() . '&volume=' . $rd->getVolume() . '&nilai_anggaran=' . $rd->getNilaiAnggaran() . '&tahun=' . sfConfig::get('app_tahun_default') . '&mlokasi=' . $mlokasi . '&id_kelompok=' . $id_kelompok . '&th_load=0&level=1&nm_user=' . $sf_user->getNamaLogin() . '&total_lokasi=' . $total_lokasi . '&lokasi_ke=1', array('class' => 'btn btn-default btn-flat btn-sm'));
                            ?>
                            <button type="button" class="btn btn-default dropdown-toggle btn-flat btn-sm" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                                <?php
                                echo link_to('<i class="fa fa-trash"></i> Hapus Lokasi', 'dinas/hapusLokasi?id=' . $id . '&no=' . $rd->getDetailNo() . '&unit=' . $rd->getUnitId() . '&kegiatan=' . $rd->getKegiatanCode(), Array('confirm' => 'Yakin untuk menghapus Lokasi untuk Komponen ' . $rd->getKomponenName() . ' ' . $rd->getDetailName() . ' ?', 'class' => 'dropdown-item'));
                                ?>
                            </div>
                        <?php
                        }
                        ?>
                    </div>                
                 <?php
                }
            }
            ?>
        </td>
    <?php
    if (($rd->getNotePeneliti() != '' and $rd->getNotePeneliti() != NULL ) or ( $rd->getNoteSkpd() != '' and $rd->getNoteSkpd() != NULL ) and $rd->getStatusHapus() == false)
    {
    ?>
        <td style="background: #e8f3f1">
            <?php
            if ($benar_musrenbang == 1) {
                echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
            }
            if ($benar_hibah == 1) {
                echo '&nbsp;<span class="badge badge-success">Hibah</span>';
            }
            if ($rd->getKecamatan() <> '') {
                echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
            }
            if ($benar_multiyears == 1) {
                echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
            }
            if ($rd->getIsBlud() == 1) {
                echo '&nbsp;<span class="badge badge-info">BLUD</span>';
            }
            echo '<br/>';
            echo $rd->getKomponenName();
            if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                echo ' ' . $rd->getDetailName() . '<br/>';
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                    if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                        echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                    }
                }
            }
            if ($rd->getLockSubtitle() == 'LOCK') {
                echo image_tag('/images/gembok.gif', array('width' => '25', 'height' => '25'));
            }
            $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen "
            . "where komponen_id='" . $rd->getKomponenId() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                    echo image_tag('/images/newanima.gif');
                }
            }
            ?>
        </td>
        <td style="background: #e8f3f1" align="center"><?php echo $rd->getSatuan(); ?></td>
        <td style="background: #e8f3f1" align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
        <td style="background: #e8f3f1" align="right">
            <?php
            if ($rd->getSatuan() == '%') {
                echo $rd->getKomponenHargaAwal();
            } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
            } else {
                echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
            }
            if ($rd->getLockSubtitle() <> 'LOCK' && $rd->getNoteSkpd() <> '') {
                if ($rd->getStatusLelang() == 'lock') {
                    echo image_tag('/images/bahaya2.gif');
                    echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                } else if ($rd->getStatusLelang() == 'unlock') {
                    $lelang = 0;
                    $ceklelangselesaitidakaturanpembayaran = 0;
                    if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                        if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                            $lelang = $rd->getCekLelang($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo(), $rd->getNilaiAnggaran());
                            if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo());
                            }
                        }
                    }

                    if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                        echo form_tag("dinas/ubahhargadasar?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                        echo input_tag("komponen_harga_awal", '', array('style' => 'width:50px', 'value' => $rd->getKomponenHargaAwal()));
                        echo "<br>";
                        echo submit_tag(' OK ');
                        echo "</form>";
                    } else {
                        if ($lelang > 0) {
                            echo '<br/><span class="badge badge-danger">[Lelang Sedang Berjalan]</span>';
                        }
                        if ($ceklelangselesaitidakaturanpembayaran == 1) {
                            echo '<br/><span class="badge badge-danger">[Belum ada Aturan Pembayaran]</span>';
                        }
                    }
                    echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                }
            }
            ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php echo $rd->getPajak() . '%'; ?>
        </td>
        <td style="background: #e8f3f1" align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $pajak = $rd->getPajak();
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, ',', '.');
            ?>
        </td>
        <td style="background: #e8f3f1" align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <td style="background: #e8f3f1">
            <?php
            if ($rd->getLockSubtitle() <> 'LOCK' && $rd->getStatusLelang() == 'unlock' && $rd->getNoteSkpd() == '') {
                $detno = $rd->getDetailNo();
                echo 'Catatan SKPD:<br/>';
                echo form_tag("dinas/simpanCatatan?unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$detno");
                echo input_tag("catatan_skpd", '', array('style' => 'width:100px'));
                echo "<br>";
                echo submit_tag(' OK ');
                echo "</form>";
                echo "<br/><font style='font-weight: bold; color: red'>-Isi kolom ini untuk membuka Field Penggunaan Sisa Lelang.-</font>";
            }
            ?>
        </td>
    <?php } else { ?>
        <td>
            <?php
            if ($benar_musrenbang == 1) {
                echo '&nbsp;<span class="badge badge-success">Musrenbang</span>';
            }
            if ($benar_hibah == 1) {
                echo '&nbsp;<span class="badge badge-success">Hibah</span>';
            }
            if ($rd->getKecamatan() <> '') {
                echo '&nbsp;<span class="badge badge-warning">Jasmas</span>';
            }
            if ($benar_multiyears == 1) {
                echo '&nbsp;<span class="badge badge-primary">Multiyears Tahun ke ' . $rd->getThKeMultiyears() . '</span>';
            }
            if ($rd->getIsBlud() == 1) {
                echo '&nbsp;<span class="badge badge-info">BLUD</span>';
            }
            echo '<br/>';
            echo $rd->getKomponenName();
            if (sfConfig::get('app_fasilitas_keteranganKomponen') == 'buka') {
                echo ' ' . $rd->getDetailName() . '<br/>';
                if ($rd->getTipe2() == 'KONSTRUKSI' || $rd->getTipe() == 'FISIK' || $est_fisik) {
                    if ($rd->getLokasiKecamatan() <> '' && $rd->getLokasiKelurahan() <> '') {
                        echo '[' . $rd->getLokasiKelurahan() . ' - ' . $rd->getLokasiKecamatan() . ']';
                    }
                }
            }
            if ($rd->getLockSubtitle() == 'LOCK')
                echo image_tag('/images/gembok.gif', array('width' => '25', 'height' => '25'));
            $query2 = "select tahap from " . sfConfig::get('app_default_schema') . ".komponen where komponen_id='" . $rd->getKomponenId() . "'";
            $con = Propel::getConnection();
            $stmt = $con->prepareStatement($query2);
            $t = $stmt->executeQuery();
            while ($t->next()) {
                if ($t->getString('tahap') == DinasMasterKegiatanPeer::getTahapKegiatan($rd->getUnitId(), $rd->getKegiatanCode())) {
                    echo image_tag('/images/newanima.gif');
                }
            }
            if ($rd->getStatusLelang() == 'lock' || $rd->getStatusLelang() == 'unlock') {
                echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
            }     
            ?>
        </td>
        <td align="center"><?php echo $rd->getSatuan() ?></td>
        <td align="center"><?php echo $rd->getKeteranganKoefisien(); ?></td>
        <td align="right">
            <?php
                if ($rd->getSatuan() == '%') {
                    echo $rd->getKomponenHargaAwal();
                } elseif ($rd->getKomponenHargaAwal() != floor($rd->getKomponenHargaAwal())) {
                    echo number_format($rd->getKomponenHargaAwal(), 2, ',', '.');
                } else {
                    echo number_format($rd->getKomponenHargaAwal(), 0, ',', '.');
                }
                if ($rd->getLockSubtitle() <> 'LOCK' && $rd->getNoteSkpd() <> '') {
                    if ($rd->getStatusLelang() == 'lock') {
                        echo image_tag('/images/bahaya2.gif');
                        echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                    } else if ($rd->getStatusLelang() == 'unlock') {
                        $lelang = 0;
                        $ceklelangselesaitidakaturanpembayaran = 0;

                        if (sfConfig::get('app_fasilitas_cekeProject') == 'buka') {
                            if (sfConfig::get('app_fasilitas_cekServer') == 'buka') {
                                $lelang = $rd->getCekLelang($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo(), $rd->getNilaiAnggaran());
                                if (sfConfig::get('app_fasilitas_cekeDelivery') == 'buka') {
                                    $ceklelangselesaitidakaturanpembayaran = $rd->getCekLelangTidakAdaAturanPembayaran($rd->getUnitId(), $rd->getKegiatanCode(), $rd->getDetailNo());
                                }
                            }
                        }

                        if ($lelang == 0 && $ceklelangselesaitidakaturanpembayaran == 0) {
                            echo form_tag("dinas/ubahhargadasar?id=$id&unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$no");
                            echo input_tag("komponen_harga_awal", '', array('style' => 'width:50px', 'value' => $rd->getKomponenHargaAwal()));
                            echo "<br>";
                            echo submit_tag(' OK ');
                            echo "</form>";
                        } else {
                            if ($lelang > 0) {
                                echo '<br/><span class="badge badge-danger">[Lelang Sedang Berjalan]</span>';
                            }
                            if ($ceklelangselesaitidakaturanpembayaran == 1) {
                                echo '<br/><span class="badge badge-danger">[Belum ada Aturan Pembayaran]</span>';
                            }
                        }
                        echo '<br/><span class="badge badge-danger">[Telah dilakukan Penggunaan Sisa Lelang, tidak dapat mengedit volume]</span>';
                    }
                }
                ?>   
        </td>
        <td align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $hasil = $volume * $harga;
            echo number_format($hasil, 0, ',', '.');
            ?>
        </td>
        <td align="right">
            <?php echo $rd->getPajak() . '%'; ?>
        </td>
        <td align="right">
            <?php
            $volume = $rd->getVolume();
            $harga = $rd->getKomponenHargaAwal();
            $pajak = $rd->getPajak();
            $total = $rd->getNilaiAnggaran();
            echo number_format($total, 0, ',', '.');
            ?>
        </td>
        <td align="center">
            <?php
            $rekening = $rd->getRekeningCode();
            $rekening_code = substr($rekening, 0, 6);
            $c = new Criteria();
            $c->add(KelompokBelanjaPeer::BELANJA_CODE, $rekening_code);
            $rs_rekening = KelompokBelanjaPeer::doSelectOne($c);
            if ($rs_rekening) {
                echo $rs_rekening->getBelanjaName();
            }
            ?>
        </td>
        <td>
            <?php
            if ($rd->getLockSubtitle() <> 'LOCK' && $rd->getStatusLelang() == 'unlock' && $rd->getNoteSkpd() == '') {
                $detno = $rd->getDetailNo();
                echo 'Catatan SKPD:<br/>';
                echo form_tag("dinas/simpanCatatan?unit_id=$unit_id&kegiatan_code=$kegiatan_code&detail_no=$detno");
                echo input_tag("catatan_skpd", '', array('style' => 'width:100px'));
                echo "<br>";
                echo submit_tag(' OK ');
                echo "</form>";
                echo "<br/><font style='font-weight: bold; color: red'>-Isi kolom ini untuk membuka Field Penggunaan Sisa Lelang.-</font>";
            }
            ?>
        </td>
    <?php } ?>
    </tr>
<?php
endforeach;
?>
<script>
    function execKembalikanWaiting(detailno, unitid, kodekegiatan, id) {
        $.ajax({
            url: "/<?php echo sfConfig::get('app_default_coding'); ?>/index.php/dinas/kembalikanWaitingEdit/unitid/" + unitid + "/kodekegiatan/" + kodekegiatan + "/detailno/" + detailno + ".html",
            context: document.body
        }).done(function (msg) {
            $('#tempat_ajax_' + id).html(msg);
        });
    }
</script>