<?php
use_helper('Url', 'Javascript', 'Form', 'Object', 'Validation');
?>
<div class="row" id="depan_<?php echo $sf_params->get('id') ?>">
  <div class="col">
    <div class="card">
      <div class="card-body">
        <?php
          $kode_kegiatan = $sf_params->get('kegiatan');
          $unit_id = $sf_params->get('unit');

          $kode_program22 = substr($master_kegiatan->getKodeProgram2(), 9, 2);
          if (substr($master_kegiatan->getKodeProgram2(), 0, 8) == 'X.XX') {
              $kode_urusan = substr($master_kegiatan->getKodeUrusan(), 0, 8);
          } else {
              $kode_urusan = substr($master_kegiatan->getKodeProgram2(), 0, 8);
          }
          $e = new Criteria();
          $e->add(UnitKerjaPeer::UNIT_ID, $master_kegiatan->getUnitId());
          $es = UnitKerjaPeer::doSelectOne($e);
          if ($es) {
              $kode_permen = $es->getKodePermen();
          }
          $kode = $kode_urusan . '.' . $kode_permen . '.' . $kode_program22 . '.' . $master_kegiatan->getKodeKegiatan();
          $temp_kode = explode('.', $kode);
          if (count($temp_kode) >= 5) {
              $kode_kegiatan_pakai = $temp_kode[0] . '.' . $temp_kode[1] . '.' . $temp_kode[3] . '.' . $temp_kode[4];
          } else {
              $kode_kegiatan_pakai = $kode;
          }
          $query = "select mk.kode_kegiatan, mk.kode_head_kegiatan, mh.nama_head_kegiatan, mk.kegiatan_id, mk.nama_kegiatan, mk.alokasi_dana, mk.kode_program2, uk.unit_name, uk.unit_id, mk.kode_urusan, mk.kode_bidang "
          . "from " . sfConfig::get('app_default_schema') . ".master_kegiatan mk, " . sfConfig::get('app_default_schema') . ".master_head_kegiatan mh, unit_kerja uk "
          . "where mk.kode_kegiatan = '" . $kode_kegiatan . "' and mk.unit_id = '" . $unit_id . "' and mh.unit_id = '" . $unit_id . "' "
          . "and mk.kode_head_kegiatan = mh.kode_head_kegiatan and uk.unit_id=mk.unit_id";
          $con = Propel::getConnection();
          $stmt = $con->prepareStatement($query);
          $rs = $stmt->executeQuery();
          $ranking = '';
          while ($rs->next()) 
          {
              $kode_head_kegiatan = $rs->getString('kode_head_kegiatan');
              $nama_head_kegiatan = $rs->getString('nama_head_kegiatan');
              $kegiatan_kode = $rs->getString('kegiatan_id');
              $nama_kegiatan = $rs->getString('nama_kegiatan');
              $alokasi = $rs->getString('alokasi_dana');
              $kode_unit = $rs->getString('unit_id');
              $organisasi = $rs->getString('unit_name');
              $program_p_13 = $rs->getString('kode_program2');
              $kode_urusan = $rs->getString('kode_urusan');
              $kode_bidang = $rs->getString('kode_bidang');
          }

          $query = "select distinct(rd.subtitle) "
          . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd "
          . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "'";
          $con = Propel::getConnection();
          $stmt = $con->prepareStatement($query);
          $rs = $stmt->executeQuery();

          $kode_kegiatan_baru = substr($master_kegiatan->getKodeKegiatan(), 12, 4);
          $kode_tulis = $kode_urusan . '.' . $kode_permen . '.' . $kode_program22 . '.' . $kode_kegiatan_baru;
        ?>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Organisasi</div>
                <div class="col-md-9"><?php echo $kode_unit . ' ' . $organisasi ?></div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Urusan</div>
                <div class="col-md-9">
                    <?php
                    $urusan = '';
                    $u = new Criteria();
                    $u->add(MasterUrusanPeer::KODE_URUSAN, $kode_urusan);
                    $us = MasterUrusanPeer::doSelectOne($u);
                    if ($us) {
                        $nama_urusan = $us->getNamaUrusan();
                    }
                    $urusan = $kode_urusan . ' ' . $nama_urusan;
                    echo $urusan;
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Bidang Urusan</div>
                <div class="col-md-9">
                    <?php
                    $bidang = '';
                    $u = new Criteria();
                    $u->add(MasterBidangPeer::KODE_BIDANG, $kode_bidang);
                    $us = MasterBidangPeer::doSelectOne($u);
                    if ($us) {
                        $nama_bidang = $us->getNamaBidang();
                    }
                    $bidang = $kode_bidang . ' ' . $nama_bidang;
                    echo $bidang;
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Program RPJM / Program 13</div>
                <div class="col-md-9">
                    <?php
                    $program13 = '';
                    $query = "select * from " . sfConfig::get('app_default_schema') . ".master_program2 kp where kp.kode_program='" . $kode_program . "' and kp.kode_program2='" . $program_p_13 . "'"; 
                    $query = "select *
                    from " . sfConfig::get('app_default_schema') . ".master_program2 kp
                    where kp.kode_program2='" . $program_p_13 . "'";

                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs1 = $stmt->executeQuery();
                    while ($rs1->next()) {
                        $program13 = $rs1->getString('kode_program2') . ' ' . $rs1->getString('nama_program2');
                    }
                    echo $program13;
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Kegiatan</div>
                <div class="col-md-9"><?php echo $kode_head_kegiatan . ' ' . $nama_head_kegiatan ?></div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Sub Kegiatan</div>
                <div class="col-md-9"><?php echo $kegiatan_kode. ' '.$nama_kegiatan ?></div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Output Sub Kegiatan</div>
                <div class="col-md-9">
                    <table class="table table-responsive">
                        <thead>
                            <tr class="text-center">
                                <th style="padding:5px">Tolak Ukur Kinerja</th>
                                <th style="padding:5px">Kelompok Sasaran</th>
                                <th style="padding:5px">Lokasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $di = new Criteria();
                            $di->add(OutputSubtitlePeer::UNIT_ID, $unit_id);
                            $di->add(OutputSubtitlePeer::KODE_KEGIATAN, $kode_kegiatan);
                            $vi = OutputSubtitlePeer::doSelect($di);
                            foreach ($vi as $value):
                                $kinerja = $value->getOutput();
                                $kinerja_output = str_replace("|", " ", $kinerja);
                                $kelompok = $value->getKelompokSasaran();
                                $lokasi = $value->getLokasi();
                                ?>
                                <tr class="text-center">
                                    <td style="padding:5px">
                                        <?php echo $kinerja_output; ?>
                                    </td>
                                    <td style="padding:5px">
                                        <?php echo $kelompok; ?>
                                    </td>
                                    <td style="padding:5px">
                                     <?php echo $lokasi; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>  
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Alokasi</div>
                <div class="col-md-9">
                    <?php
                    echo 'Rp. ' . number_format($alokasi, 0, ',', '.'); 
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-3 text-right text-bold">Total</div>
                <div class="col-md-9">
                    <?php
                    $query = "select sum(nilai_anggaran) as total "
                    . "from " . sfConfig::get('app_default_schema') . ".rincian_detail rd "
                    . "where rd.kegiatan_code = '" . $kode_kegiatan . "' and rd.unit_id = '" . $unit_id . "' "
                    . "and rd.status_hapus = false ";
                    $con = Propel::getConnection();
                    $stmt = $con->prepareStatement($query);
                    $rs = $stmt->executeQuery();
                    while ($rs->next()) {
                        $total = $rs->getFloat('total');
                    }
                    echo 'Rp. ' . number_format($total, 0, ',', '.');
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>