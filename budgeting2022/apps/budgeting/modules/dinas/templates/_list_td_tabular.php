<?php
use_helper('Url', 'Javascript', 'Form', 'Object');
$kode = $master_kegiatan->getKodeKegiatan();
$unit = $master_kegiatan->getUnitId();

$query = "SELECT * from " . sfConfig::get('app_default_schema') . ".rincian_detail 
where ((note_peneliti is not Null and note_peneliti <> '') or (note_skpd is not Null and note_skpd <> '')) and unit_id = '$unit' and kegiatan_code = '$kode' and status_hapus = false";
//diambil nilai terpakai
$con = Propel::getConnection();
$statement = $con->prepareStatement($query);
$rs = $statement->executeQuery();
$jml = $rs->getRecordCount();

$query2 = "SELECT * from " . sfConfig::get('app_default_schema') . ".master_kegiatan 
where catatan is not Null and trim(catatan) <> '' and unit_id = '$unit' and kode_kegiatan = '$kode' ";
//diambil nilai terpakai
$con = Propel::getConnection();
$statement2 = $con->prepareStatement($query2);
$rs = $statement2->executeQuery();
$jml2 = $rs->getRecordCount();

if ($jml > 0 or $jml2 > 0) {
    ?>
    <td style="background: #e8f3f1">
    <?php
        echo $master_kegiatan->getKodeKegiatan();
        if ($master_kegiatan->getGender() == 'TRUE') {
            echo ' <font color=blue><b>(ARG)</b></font>';
        }
    ?>
    </td>
    <td style="background: #e8f3f1">
    <?php 
        echo $master_kegiatan->getNamakegiatan();
        if ($master_kegiatan->getUserId() != '') {
            echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
        }
        echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ] '; 
    ?>
    </td>
    <?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
    ?>
    <td align="right" style="background: #e8f3f1">
        <?php
        //digunakan ketika awal anggaran
        echo number_format($master_kegiatan->getAlokasidana(),0,',','.') 
        ?>
        <?php
        //digunakan ketika sudah mulai di edit dan dibandingkan
        //echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan))
        ?>
    </td>

    <?php
    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
    ?>
    <!-- <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td> -->
    <?php } ?>
    <td align="right" style="background: #e8f3f1"><?php echo get_partial('nilairincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    <!-- <td align="right" style="background: #e8f3f1"><?php echo get_partial('selisih', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td> -->
    <!-- <td align="right" style="background: #e8f3f1"><?php echo get_partial('tambahan_pagu', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td> -->
    <td style="background: #e8f3f1"><?php echo get_partial('posisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    
<?php
    } else {
?>
    <td>
    <?php
        echo $master_kegiatan->getKodeKegiatan();
        if ($master_kegiatan->getGender() == 'TRUE') {
            echo ' <font color=blue><b>(ARG)</b></font>';
        }
    ?>
    </td>
    <td>
    <?php 
        echo $master_kegiatan->getNamakegiatan();
        if ($master_kegiatan->getUserId() != '') {
            echo ' <font color=#FF0000>(' . $master_kegiatan->getUserId() . ')</font><br/>';
        }
        echo 'Kode: [ '.$master_kegiatan->getKegiatanId().' ] '; 
    ?>
    </td>
    <?php
    if (sfConfig::get('app_tahap_edit') == 'murni') {
        ?>
        <td align="right" >
            <?php
            //digunakan ketika awal anggaran
            echo number_format($master_kegiatan->getAlokasidana(),0,',','.') 
            ?>
            <?php
            //digunakan ketika sudah mulai di edit dan dibandingkan
            //echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan))
            ?>
        </td>

    <?php
    } else if (sfConfig::get('app_tahap_edit') != 'murni') {
    ?>
    <td align="right" ><?php echo get_partial('nilaisemula', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    <?php } ?>
    <td align="right"><?php echo get_partial('nilairincian', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
    <!-- <td align="right"><?php echo get_partial('selisih', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td> -->
    <!-- <td align="right"><?php echo get_partial('tambahan_pagu', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td> -->
    <td><?php echo get_partial('posisi', array('type' => 'list', 'master_kegiatan' => $master_kegiatan)) ?></td>
<?php } ?>
