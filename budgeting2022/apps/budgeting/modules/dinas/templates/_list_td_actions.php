<td class='tombol_actions'>
    <?php
    $kegiatan_code = $master_kegiatan->getKodeKegiatan();
    $unit_id = $master_kegiatan->getUnitId();

    $c = new Criteria();
    $c->add(RincianPeer::KEGIATAN_CODE, $kegiatan_code);
    $c->add(RincianPeer::UNIT_ID, $unit_id);
    $rs_rincian = RincianPeer::doSelectOne($c);
    if ($rs_rincian) {
            $posisi = $rs_rincian->getRincianLevel();
            $lock_kegiatan = $rs_rincian->getLock();
        }
    if ($sf_user->getNamaLogin() == $master_kegiatan->getUserId() or substr($sf_user->getNamaLogin(), 0, 3) == 'pa_') {
            echo link_to('<i class="fa fa-edit"></i>', 'dinas/edit?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan() . '&tahap=' . $master_kegiatan->getTahap(), array('alt' => __('Melihat Rincian'), 'title' => __('Melihat Rincian'), 'class' => 'btn btn-outline-primary btn-sm'));
            echo link_to('<i class="fa fa-th-list"></i>', 'report/tampillaporan?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('laporan perbandingan'), 'title' => __('laporan perbandingan'), 'class' => 'btn btn-outline-info btn-sm'));
            echo link_to('<i class="fa fa-list-ol"></i>', 'report/bandingRekeningSimple?unit_id=' . $master_kegiatan->getUnitId() . '&kode_kegiatan=' . $master_kegiatan->getKodeKegiatan(), array('alt' => __('Laporan Perbandingan Rekening'), 'title' => __('Laporan Perbandingan Rekening'), 'class' => 'btn btn-outline-warning btn-sm'));
        }
    ?>
</td>