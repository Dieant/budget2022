<?php
use_stylesheet('/css/TableCSS.css');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div>
    <h2 style="text-align: center">PEDOMAN PENYUSUNAN RKA <?php echo sfConfig::get('app_tahun_default'); ?></h2>
</div>
<div class="CSSTableGenerator">
    <table>
        <tr><td></td></tr>
        <tr>
            <td>1.</td>
            <td> Penganggaran tambahan penghasilan bagi Tenaga Harian Lepas dan Pekerja Harian dibatasi maksimal 2(dua) kegiatan.</td>
        </tr>
        <tr>
            <td>2.</td>
            <td> - Belanja Bahan Bakar Minyak/Gas Kendaraan Roda Dua maksimal 2 liter/hari.</td>
        </tr>
        <tr>
            <td></td>
            <td> - Belanja Bahan Bakar Minyak/Gas Kendaraan Roda Empat maksimal 8 liter/hari.</td>
        </tr>
        <tr>
            <td></td>
            <td> - Belanja Bahan Bakar Minyak/Gas Kendaraan Patroli SATPOL PP, Patroli Linmas, Patroli Dishub, dan Kendaraan Operasional maksimal 15 liter/hari.</td>
        </tr>
        <tr>
            <td>3.</td>
            <td> Penganggaran air galon dibatasi sesuai dengan kebutuhan nyata yang didasarkan atas pelaksanaan tugas dan fungsi SKPD.</td>
        </tr>
        <tr>
            <td>4.</td>
            <td> Penyelenggaraan kegiatan rapat, pendidikan dan pelatihan, bimbingan teknis atau sejenisnya diprioritaskan untuk menggunakan fasilitas aset daerah, seperti ruang rapat atau aula yang sudah tersedia milik Pemerintah Kota Surabaya.</td>
        </tr>
        <tr>
            <td>5.</td>
            <td> Belanja Transport Lokal (rekening 5.2.2.21.01) hanya dianggarkan untuk diberikan kepada masyarakat yang mengikuti kegiatan pelatihan, sosialisasi, bimbingan teknis/workshop dan sejenisnya.</td>
        </tr>
        <tr>
            <td>6.</td>
            <td> Belanja Barang Yang Akan Diserahkan Kepada Masyarakat (rekening 5.2.2.27.01), pemberian barang pada rekening tersebut harus mendukung penyelenggaraan kegiatan dan memberikan manfaat yang dapat diperoleh langsung oleh masyarakat.</td>
        </tr>
<!--    <tr><td style="text-align: center"><?php echo button_to('setuju', 'dinas/list'); ?></td></tr>-->
    </table>
</div>























