<?php use_helper('Object', 'Javascript', 'Number', 'Validation') ?>
<?php use_stylesheet('/sf/sf_admin/css/main') ?>

<?php $cari = $sf_params->get('cari'); ?>
<?php
$salah = $sf_params->get('salah');
if ($salah == '1') {
    $ket = ':. Ada Kolom yang tidak diisi .:';
}
?>
<?php
$kode_kegiatan = $sf_params->get('kode_kegiatan');
$unit_id = $sf_params->get('unit_id');
$tipe = $sf_params->get('tipe');
$query = "select detail_no from " . sfConfig::get('app_default_schema') . ".rincian_detail where kegiatan_code='" . $kode_kegiatan . "' and unit_id='" . $unit_id . "' order by detail_no";
$con = Propel::getConnection();
$stmt = $con->prepareStatement($query);
$rs = $stmt->executeQuery();
$detail_no = 0;
while ($rs->next()) {
    $detail_no = $rs->getString('detail_no');
}
$detail_no = $detail_no + 1;
$rekening_code = $sf_params->get('kode_rekening');
$komponen_name = $sf_params->get('komponen');
$satuan = $sf_params->get('satuan');
$komponen_harga = $sf_params->get('harga');
$pajak = $sf_params->get('pajak');
$komponen_id = $sf_params->get('id');
?>

<div id="sf_admin_container">
    <div class="sf_admin_filters">
        <?php echo form_tag('dinas/carikomponen', array('method' => 'get')) ?>

        <fieldset>
            <h2><?php echo 'Mencari Komponen Berdasarkan Nama Komponen' ?></h2>
            <div class="form-row">
                <label for="filters_nama_komponen"><?php echo 'Nama komponen:' ?></label>
                <div class="content" style="min-height: 25px">
                    <?php
                    echo input_tag('filters[nama_komponen]', isset($filters['nama_komponen']) ? $filters['nama_komponen'] : null, array(
                        'size' => NULL,
                    ))
                    ?>
                </div>
            </div>

        </fieldset> 
        <?php
        echo input_hidden_tag('kegiatan', $kode_kegiatan);
        echo input_hidden_tag('unit', $unit_id);
        ?>
        <ul class="sf_admin_actions">
            <li><?php echo submit_tag('cari', 'name=filter class=sf_admin_action_filter') ?></li>
        </ul>

        </form>
    </div>
    <?php echo form_tag('dinas/simpanSubKegiatan') ?>
    <?php
    $c = new Criteria();
    $c->add(SubKegiatanPeer::SUB_KEGIATAN_ID, $sf_params->get('id'));
    $cs = SubKegiatanPeer::doSelectOne($c);
    if ($cs) {
        ?>
        <table cellspacing="0" class="sf_admin_list">
            <tbody>

                <?php
                if ($salah == 1) {
                    echo '<tr><td colspan="3"><small><b>:. Ada Kolom Yang Belum Diisi .:</b></small></td></tr>';
                }
                if ($sf_flash->has('error_lokasi')):
                    ?>
                <div class="form-errors">
                    <h2><?php echo ($sf_flash->get('error_lokasi')) ?></h2>
                </div>

            <?php endif; ?>

            <?php echo input_hidden_tag('cari_awal', $cari) ?>
            <?php echo input_hidden_tag('kelompok_belanja', $sf_params->get('kelompok_belanja')) ?>
            <?php echo input_hidden_tag('kegiatan_code', $kode_kegiatan) ?>
            <?php echo input_hidden_tag('unit_id', $unit_id) ?>
            <?php echo input_hidden_tag('detail_no', $detail_no) ?>
            <?php echo input_hidden_tag('rekening_code', $rekening_code) ?>
            <?php echo input_hidden_tag('komponen_name', $komponen_name) ?>
            <?php echo input_hidden_tag('satuan', $satuan) ?>
            <?php echo input_hidden_tag('pajak', $pajak) ?>
            <?php echo input_hidden_tag('tipe', $tipe) ?>
            <?php echo input_hidden_tag('komponen_id', $komponen_id) ?>
            <?php echo input_hidden_tag('komponen_harga', $komponen_harga) ?>

            <tr class="sf_admin_row_1">
                <td align="left" nowrap class="Font8Boldv">Nama Sub Kegiatan </td>
                <td align="left" nowrap><?php
                    echo '<b>' . $cs->getSubKegiatanName() . '</b>';
                    echo input_hidden_tag('sub_kegiatan_id', $cs->getSubKegiatanId());
                    ?></td>
            </tr>
            <tr class="sf_admin_row_0">
                <td align="left" nowrap class="Font8Boldv">Satuan</td>
                <td align="left" nowrap><?php echo '<b>' . $cs->getSatuan() . '</b>' ?></td>
            </tr>


            <?php
            //print_r($cs->getSubKegiatanId());
            $kodeSubKomponen = $cs->getSubKegiatanId();
            //	$tipe = $sf_params->get('tipe');
            if ($kodeSubKomponen == 'PJU001 ' || $kodeSubKomponen == 'PJU002' || $kodeSubKomponen == 'PJU003') {
                if ($sf_params->get('lokasi')) {

                    //sholeh begin

                    $lokasi = base64_decode($sf_params->get('lokasi'));
                    if ($lokasiSession) {

                        if (strpos($lokasiSession, $lokasi) === false)
                        //  echo $lokasiSession.' -------- '.$lokasi;
                            $lokasi = $lokasiSession . ', ' . $lokasi;
                    }

                    //sholeh end
                }
                elseif (!$sf_params->get('lokasi')) {
                    $lokasi = '';
                }
                ?>
                <tr class="sf_admin_row_0" align='right' valign="top">
                    <td align="left" nowrap class="Font8Boldv">Lokasi</td>

                    <td align="left">
                        <?php
                        echo textarea_tag('keterangan1', $lokasi, 'size=140x3') . submit_tag('cari', 'name=cari') . '<br><font color="magenta">[untuk multi lokasi, tambahkan lokasi dengan klik "cari" lagi]<br></font>';
                        echo '<br> Usulan dari : ';
                        echo select_tag('jasmas', objects_for_select($rs_jasmas, 'getKodeJasmas', 'getNama', $sf_params->get('jasmas'), 'include_custom=--Pilih--'));
                        echo '<br />';
                        if (!$lokasi == '') {
                            $kode_lokasi = '';
                            $banyak = 0;
                            $c = new Criteria();
                            $c->add(VLokasiPeer::NAMA, $lokasi, Criteria::ILIKE);
                            $rs_lokasi = VLokasiPeer::doSelectOne($c);
                            if ($rs_lokasi) {
                                $kode_lokasi = $rs_lokasi->getKode();
                            }

                            $sql = new Criteria();
                            if ($kode_lokasi) {
                                $kod = '%' . $kode_lokasi . '%';
                                $sql->add(HistoryPekerjaanPeer::KODE, strtr($kod, '*', '%'), Criteria::ILIKE);

                                $sql->addAscendingOrderByColumn(HistoryPekerjaanPeer::TAHUN);
                                $sqls = HistoryPekerjaanPeer::doSelect($sql);
                                ?>
                                <table cellspacing="0" class="sf_admin_list">
                                    <thead>
                                        <tr>
                                            <th>Tahun</th>
                                            <th>Lokasi</th>
                                            <th>Nilai</th>
                                            <th>volume</th>
                                        <tr>
                                    </thead>
                                    <?php
                                    $tahun = '';
                                    $total_lokasi = 0;
                                    foreach ($sqls as $vx) {
                                        //while ($rsPeta->next())
                                        if ($tahun == '') {
                                            $tahun = $vx->getTahun();
                                        }
                                        //$s -> addOr(SubtitleIndikatorPeer::SUBTITLE, $vx->getSubtitle());
                                        if (($tahun <> $vx->getTahun()) && ($tahun <> '')) {
                                            $tahun = $vx->getTahun();
                                            ?>
                                            <tbody>
                                                <tr class="sf_admin_row_1" align='right'>
                                                    <td>&nbsp;</td>
                                                    <td align="right">Total :</td>
                                                    <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); ?></td>
                                                    <td> <?php $total_lokasi = 0 ?></td>
                                                </tr>
                                                <tr><td colspan="4"></td></tr>
                                                <tr>
                                                    <td> <?php echo $vx->getTahun(); ?></td>
                                                    <td> <?php echo $vx->getLokasi(); ?></td>
                                                    <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); ?></td>
                                                    <td> <?php echo $vx->getVolume(); ?></td>
                                                </tr>
                                                <?php
                                                $total_lokasi+=$vx->getNilai();
                                            } else {
                                                ?>
                                                <tr>
                                                    <td> <?php echo $vx->getTahun(); //$rsPeta->getInt('tahun')       ?></td>
                                                    <td> <?php echo $vx->getLokasi(); //$rsPeta->getString('lokasi')       ?></td>
                                                    <td align="right"> <?php echo number_format($vx->getNilai(), 0, ',', '.'); //$rsPeta->getString('nilai')      ?></td>
                                                    <td> <?php echo $vx->getVolume(); //$rsPeta->getString('nomor')      ?></td>
                                                </tr>
                                                <?php
                                                $total_lokasi+=$vx->getNilai();
                                            }
                                            if ($vx->getNilai() > 0) {
                                                $banyak+=1;
                                            }
                                        }
                                        ?>
                                        <tr class="sf_admin_row_1" align='right'>
                                            <td> </td>
                                            <td align="right">Total :</td>
                                            <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai')      ?></td>
                                            <td> <?php $total_lokasi = 0 ?></td>
                                        </tr>
                                        <tr><td colspan="4">&nbsp;</td></tr>
                                        <?php
                                        $query = "select * from " . sfConfig::get('app_default_schema') . ".rincian_detail where detail_name ilike '$lokasi'";
                                        $con = Propel::getConnection();
                                        $stmt = $con->prepareStatement($query);
                                        $rs_rincianlokasi = $stmt->executeQuery();
                                        while ($rs_rincianlokasi->next()) {
                                            ?>
                                            <tr>
                                                <td><?php echo sfConfig::get('app_tahun_default') ?></td>
                                                <td> <?php echo $rs_rincianlokasi->getString('komponen_name') . ' ' . $rs_rincianlokasi->getString('detail_name') ?></td>
                                                <td align="right">
                                                    <?php
                                                    $pajak = $rs_rincianlokasi->getString('pajak');
                                                    $harga = $rs_rincianlokasi->getString('komponen_harga_awal');
                                                    $volume = $rs_rincianlokasi->getString('volume');
                                                    $nilai = ($volume * $harga * (100 + $pajak) / 100);
                                                    echo number_format($nilai, 0, ',', '.');
                                                    ?></td>
                                                <td> <?php echo $rs_rincianlokasi->getString('keterangan_koefisien'); ?></td>
                                            </tr>
                                            <?php
                                            if ($volume > 0) {
                                                $banyak+=1;
                                            }
                                            $total_lokasi+=$nilai;
                                        }
                                        ?>
                                        <tr class="sf_admin_row_1" align='right'>
                                            <td> </td>
                                            <td align="right">Total :</td>
                                            <td align="right"> <?php echo number_format($total_lokasi, 0, ',', '.'); //$rsPeta->getString('nilai')       ?></td>
                                            <td> <?php $total_lokasi = 0 ?></td>
                                        </tr>
                                        <tr><td colspan="4">&nbsp;</td></tr>
                                    </tbody>

                                </table>
                                <?php
                            }
                            if ($banyak <= 0) {
                                echo '<span style="color:green;"> <small> :. Belum pernah ada Pekerjaan .: </small></span>';
                                echo input_hidden_tag('status', 'ok');
                            }

                            if ($banyak > 0) {
                                echo '<span style="color:red;">  <small> :. Pekerjaan Pada Lokasi Ini Sudah Ada, Dapat Disimpan, Tetapi Tidak Dapat Masuk RKA .: </small></span>';
                                echo input_hidden_tag('status', 'pending');
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr valign="middle" class="sf_admin_row_1">
                    <td align="left" class="Font8Boldv">Keterangan</td>
                    <td align="left" nowrap><?php echo textarea_tag('keterangan1', '', 'class=inpText') ?> </td>
                </tr>
                <?php
            }
            ?>







                                  <!--  <tr valign="middle" class="sf_admin_row_1">
                                     <td align="left" class="Font8Boldv">Keterangan</td>
                                                  <td align="left" nowrap><?php echo textarea_tag('keterangan1', '', 'class=inpText') ?> </td>
                                    </tr> -->
                                                <!--<tr valign="middle" class="sf_admin_row_0">
                                     <td align="left" class="Font8Boldv">Lokasi</td>
                                                  <td align="left" nowrap><?php echo input_tag('keterangan', base64_decode($sf_params->get('lokasi')), 'class=inpText') . ' ' . submit_tag("lokasi", "name=lokasipeta class=inpButton"); ?></td>
                                    </tr>-->
            <?php
            $x = new Criteria();
            $x->add(SubtitleIndikatorPeer::KEGIATAN_CODE, $kode_kegiatan);
            $x->add(SubtitleIndikatorPeer::UNIT_ID, $unit_id);
            $x->addAscendingOrderByColumn(SubtitleIndikatorPeer::SUBTITLE);
            $v = SubtitleIndikatorPeer::doselect($x);
            ?>
            <tr valign="middle" class="sf_admin_row_1">
                <td align="left" class="Font8Boldv">Masukan dalam subtitle</td>
                <td align="left" nowrap><?php echo select_tag('subtitle', objects_for_select($v, 'getSubId', 'getSubtitle', 0)); //options_for_select($subtitle))       ?> </td>
            </tr>
            <tr valign="middle" class="sf_admin_row_0">
                <td align="left" class="Font8Boldv">Koefisien</td>
                <td align="left" nowrap>
                    <table>
                        <?php
                        $param = explode('|', $cs->getParam());
                        $status = explode('|', $cs->getSatuan());
                        $jumlah = count($status);

                        for ($i = 0; $i < $jumlah; $i = $i + 1) {
                            //echo $param[$i];
                            //str_replace(' ','',$param[$i])
                            ?>
                            <tr>
                                <td><?php echo $param[$i] ?></td>
                                <td><?php echo input_tag('param_' . $i, ' ', 'class=inpText') ?></td>
                                <td><?php echo '_' . $status[$i] ?></td>
                            </tr>
                            <p><?php // echo $param[$i] . ' ' . input_tag('param_' . $i, ' ', 'class=inpText') . '_' . $status[$i];   ?></p>
                            <?php
                        }
                        ?>
                        <?php echo input_hidden_tag('jumlah_koef', $jumlah); ?>
                    </table>
                </td>
            </tr>
            <tr class="sf_admin_row_0">
                <td width="24%">                </td>
                <td align="right"><?php echo submit_tag('simpan', "name=simpansub class=inpButton") . ' ' . button_to('kembali', '#', array('onClick' => "javascript:history.back()")) ?></td>
            </tr>
            <tr valign="middle" class="sf_admin_row_0">
                <td colspan="2" align="left" class="Font8Boldv">
                    <table width="760"  border="0" cellpadding="3" cellspacing="1" bgcolor="#333333" class="sf_admin_list">
                        <tr bgcolor="#CCCCFF" class="sf_admin_row_1">
                            <td colspan="3"> <strong>Komponen Penyusun </strong></td>

                            <td colspan="7" >&nbsp;    		    </td>

                        </tr>

                        <tr>
                            <td align="center" bgcolor="#9999FF"><strong>Komponen</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Satuan</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Input</strong></td>

                            <td align="center" bgcolor="#9999FF"><strong>Volume Awal </strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Harga</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Koefisien</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Hasil</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>PPN</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Total</strong></td>
                            <td align="center" bgcolor="#9999FF"><strong>Belanja</strong></td>



                        </tr>

                        <?php
                        $query = "
	SELECT 
		rekening.rekening_code,
		detail.detail_name as detail_name,
		detail.komponen_name,
		detail.komponen_name || ' ' || detail.detail_name as detail_name2,
		detail.komponen_harga_awal as detail_harga,
		detail.pajak,
		detail.komponen_id,
	 detail.subtitle ,
	 detail_no,koefisien,param,
		detail.satuan as detail_satuan,
		replace(detail.keterangan_koefisien,'<*>','X') as keterangan_koefisien,
		detail.volume * detail.komponen_harga_awal as hasil,
		(detail.volume * detail.komponen_harga_awal  * (100+detail.pajak)/100) as hasil_kali,
		

		(SELECT '2' FROM " . sfConfig::get('app_default_schema') . ".komponen komponen where komponen.komponen_id=detail.komponen_id AND komponen.komponen_tipe='EST')
		 as x,
		(SELECT 'bahaya' FROM " . sfConfig::get('app_default_schema') . ".komponen komponen where komponen.komponen_id=detail.komponen_id AND komponen_show=FALSE)
		 as bahaya,
		 substring(kb.belanja_name,8) as belanja_name
		 
	FROM 
		" . sfConfig::get('app_default_schema') . ".rekening rekening ,
		 " . sfConfig::get('app_default_schema') . ".sub_kegiatan_member detail ,
		 " . sfConfig::get('app_default_schema') . ".kelompok_belanja kb
		
	WHERE 
	rekening.rekening_code = detail.rekening_code and
		detail.sub_kegiatan_id = '" . $sf_params->get('id') . "' and 
		 kb.belanja_id=rekening.belanja_id 
		
	ORDER BY 
		
		belanja_urutan,
		komponen_name
	
	";

                        $con = Propel::getConnection();
                        $stmt = $con->prepareStatement($query);
                        $r = $stmt->executeQuery();
                        $total = 0;
                        while ($r->next()) {
                            ?>
                            <tr valign="top" bgcolor="#FFFFFF">
                                <td align="left" class="Font8v{$r.x}"> <p><?php echo $r->getString('detail_name2') ?> 	</td>

                                <td align="center" nowrap class="Font8v{$r.x}"><?php echo $r->getString('detail_satuan') ?></td>
                                <?php
                                $pars = explode("|", $r->getString('param'));

                                for ($j = 0; $j < count($pars); $j++) {
                                    if ($j == 0)
                                        $inputs = $pars[$j];
                                    else
                                        $inputs = $inputs . ',<BR>' . $pars[$j];
                                }
                                //echo $r['param'];
                                $pars = $inputs;
                                ?>
                                <td align="center"  class="Font8v"><?php echo $pars ?></td>
                                <td align="center" class="Font8v"><?php echo $r->getString('keterangan_koefisien') ?></td>

                                <td align="right" nowrap class="Font8v"><?php echo $r->getString('detail_harga') ?></td>

                                <td align="right" nowrap class="Font8v"><?php echo $r->getString('koefisien') ?></td>
                                <td align="right" nowrap class="Font8v"><?php echo number_format($r->getString('hasil'), 0, ',', '.') ?></td>
                                <td align="right" nowrap class="Font8v"><?php echo $r->getString('pajak') ?> % </td>
                                <td align="right" nowrap class="Font8v"><?php echo number_format($r->getString('hasil_kali'), 0, ',', '.') ?></td>
                                <td align="right" nowrap class="Font8v"><?php echo $r->getString('belanja_name') ?></td>

                            </tr>
                            <?php
                            $total = $total + $r->getString('hasil_kali');
                        }
                        ?>
                        <tr bgcolor="#FFFFFF">
                            <td colspan="8" align="right" nowrap class="Font8v">Total  : </td>
                            <td align="right" nowrap class="Font8v"><?php echo number_format($total, 0, ',', '.') ?></td>
                            <td align="right" nowrap class="Font8v"><?php echo input_hidden_tag('total_sub_kegiatan', $total); ?></td>
                        </tr>
                    </table>
                    <?php
                }
                ?>
    </table>
</form>

</div>
