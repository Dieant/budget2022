<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo sfConfig::get('app_default_title') ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/favicon_grms.png' ?>" type="image/x-icon" />
    <?php use_stylesheet('/login_v1/css/owl.carousel.min.css'); ?>
    <?php use_stylesheet('/login_v1/fonts/icomoon/style.css'); ?>
    <!-- Bootstrap CSS -->
    <?php use_stylesheet('/login_v1/css/bootstrap.min.css'); ?> 
    <!-- Style -->
    <?php use_stylesheet('/login_v1/css/style.css'); ?>  
  </head>
  <style>
    .content {
      background-image: linear-gradient( 180deg, #C3E1FF 1%, #f8fafb) !important;
    }
    .mb-4, .my-4 {
      margin-top: 0.5rem!important;
    }
    </style>
  <body>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="<?php echo sfConfig::get('app_path_default_sf') . 'login_v1/images/ebudgeting.png' ?>" alt="Image" class="img-fluid">
        </div> 
        <div class="col-md-6 contents">
          <div class="row justify-content-center">
            <div class="col-md-8">
              <div class="mb-4">&nbsp;&nbsp;&nbsp;&nbsp;</div>
              <div class="mb-4">
              <h3>Login Account</h3>
              <p class="mb-4">Silahkan Masuk</p>
            </div>
              <?php echo form_tag('login/satulogin'); ?>
              <?php if ($sf_flash->has('gagal')) 
              { 
              ?> 
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo ($sf_flash->get('gagal')) ?>.
                </div>
              <?php
              }
              ?>
              <div class="form-group first">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="send_user">
              </div>
              <div class="form-group last mb-4">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
              </div>
              <input type="submit" value="Log In" class="btn btn-block btn-primary"> 
            </form>
            <div class="row text-center" style="margin-top:50px; margin-left: auto; margin-right: auto; display: block; padding: 10px;">
              <div class="col-mb-12">
                <span class="text-center" style="margin-top: 10px;">Copyright © 2022<br/> Bagian Administrasi Pembangunan<br/> Pemerintah Kota Surabaya</span>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--===============================================================================================-->
  <?php use_javascript('/login_v1/js/jquery-3.3.1.min.js') ?>
  <?php use_javascript('/login_v1/js/js/popper.min.js') ?>
  <?php use_javascript('/login_v1/js/bootstrap.min.js') ?>
  <?php use_javascript('/login_v1/js/main.js') ?> 
  <!--===============================================================================================-->
  </body>
</html>