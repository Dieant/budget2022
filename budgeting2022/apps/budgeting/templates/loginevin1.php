<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo sfConfig::get('app_default_title') ?></title>
  <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
  <?php use_stylesheet('/login/vendor/bootstrap/css/bootstrap.min.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/fonts/iconic/css/material-design-iconic-font.min.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/vendor/animate/animate.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/vendor/css-hamburgers/hamburgers.min.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/vendor/animsition/css/animsition.min.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/vendor/select2/select2.min.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/vendor/daterangepicker/daterangepicker.css'); ?>
<!--===============================================================================================-->
  <?php use_stylesheet('/login/css/util.css'); ?>
  <?php use_stylesheet('/login/css/main.css'); ?>
<!--===============================================================================================-->
</head>
<style type="text/css">
  .wrap-login100 {
    -moz-transform: scale(0.9, 0.9); /* Moz-browsers */
    zoom: 0.9; /* Other non-webkit browsers */
    zoom: 90%; /* Webkit browsers */
  }
</style>
<body>
  <div class="limiter">
    <div class="container-login100" style="background-image: url('<?php echo sfConfig::get('app_path_default_sf') . 'login/images/jembatan.jpg' ?>');">
      <div class="wrap-login100">
        <?php echo form_tag('login/satulogin'); ?>
          <span class="login100-form-logo">
            <i class=""><img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>"></i>
          </span>

          <span class="login100-form-title p-b-34 p-t-27">
            <?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?>
          </span>

          <?php if ($sf_flash->has('gagal')) { ?>
              <div class="row">
                  <div class="col-xs-12">    
                      <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                          <?php echo ($sf_flash->get('gagal')) ?>.
                      </div>
                  </div>
              </div>
              <?php
          }
          ?>

          <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input class="input100" type="text" name="send_user" placeholder="Username" required>
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password" required>
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn">
              Login
            </button>
          </div>

        <?php echo '</form>'; ?>
        <br/>

        <div class='row text-center' style="margin-left: auto; margin-right: auto; display: block; padding: 10px;">
          <div class="col-xs-12">
            <div class="btn-group-justified">
              <?php echo link_to('PORTAL', 'https://ebudgeting.surabaya.go.id/new_portal/', array('class' => 'btn btn-flat btn-info text-bold', 'target' => '_blank')); ?>
              <?php echo link_to('SOP', sfConfig::get('app_path_pdf') . 'SOP_eBudgeting.pdf', array('class' => 'btn btn-flat btn-info text-bold', 'target' => '_blank')); ?>    
              <?php echo link_to('MANUAL', sfConfig::get('app_path_pdf') . 'Manual_e-Revisi.pdf', array('class' => 'btn btn-flat btn-info text-bold', 'target' => '_blank')); ?>  
              <?php echo link_to('ALUR', '#', array('class' => 'btn btn-flat btn-info text-bold', 'data-toggle' => 'modal', 'data-target' => '#modalAlur')); ?>

              <!-- Modal -->
              <div class="modal fade" id="modalAlur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog"  style="width: 60%">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Alur <?php echo sfConfig::get('app_default_aplikasi'); ?></h4>
                    </div>
                    <div class="modal-body">
                      <div role="tabpanel">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#alur_awal" aria-controls="alur_awal" role="tab" data-toggle="tab">Alur Awal</a></li>
                              <li role="presentation"><a href="#alur_revisi" aria-controls="alur_revisi" role="tab" data-toggle="tab">Alur Revisi</a></li>
                          </ul>
                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="alur_awal">
                                  <?php echo image_tag('Alur.png', array('class' => 'img-responsive', 'width' => '100%')) ?>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="alur_revisi">
                                  <?php echo image_tag('Revisi.png', array('class' => 'img-responsive', 'width' => '100%')) ?>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div> 


















              000000000000000000000
            </div>
            <br/>
            <p class=" text-center" style="margin-top: 10px;color: white;">Copyright &copy; <?php echo sfConfig::get('app_tahun_default') ?></p>
            <p class=" text-center"><a href="https://ap.surabaya.go.id" style="color: black; font-size: 10px;"><b>Bagian Administrasi Pembangunan - Pemerintah Kota Surabaya</b></a></p>
            <p class=" text-center" style="color: white;">All rights reserved</p>
          </div>
      </div>
    </div>
  </div>
  </div>

  <div id="dropDownSelect1"></div>
  
<!--===============================================================================================-->
  <?php use_javascript('/login/vendor/jquery/jquery-3.2.1.min.js') ?>
<!--===============================================================================================-->
  <?php use_javascript('/login/vendor/animsition/js/animsition.min.js') ?>
<!--===============================================================================================-->
  <?php use_javascript('/login/vendor/bootstrap/js/popper.js') ?>
  <?php use_javascript('/login/vendor/bootstrap/js/bootstrap.min.js') ?>
<!--===============================================================================================-->
  <?php use_javascript('/login/vendor/select2/select2.min.js') ?>
<!--===============================================================================================-->
  <?php use_javascript('/login/vendor/daterangepicker/moment.min.js') ?>
  <?php use_javascript('/login/vendor/daterangepicker/daterangepicker.js') ?>
<!--===============================================================================================-->
  <?php use_javascript('/login/vendor/countdowntime/countdowntime.js') ?>
<!--===============================================================================================-->
  <?php use_javascript('/login/js/main.js') ?>

</body>
</html>