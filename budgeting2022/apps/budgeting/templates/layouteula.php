<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title><?php echo sfConfig::get('app_default_title') ?></title>
    <!--<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/favicon_grms.png' ?>"
        type="image/x-icon" />

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Main style -->
    <?php use_stylesheet('/css/main.css'); ?>
    <!-- Font Awesome Icons -->
    <?php use_stylesheet('/AdminLTE3/plugins/fontawesome-free/css/all.min.css'); ?>
    <!-- Select2 -->
    <?php use_stylesheet('/AdminLTE3/plugins/select2/css/select2.min.css'); ?>
    <!-- IonIcons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <?php use_stylesheet('/AdminLTE3/dist/css/adminlte.min.css'); ?>
    <!-- Toastr -->
    <?php use_stylesheet('/AdminLTE3/plugins/toastr/toastr.min.css'); ?>

    <!-- jQuery -->
    <?php use_javascript('/AdminLTE3/plugins/jquery/jquery.min.js') ?>
    <!-- Bootstrap -->
    <?php use_javascript('/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>
    <!-- AdminLTE -->
    <?php use_javascript('/AdminLTE3/dist/js/adminlte.js') ?>

    <!-- OPTIONAL SCRIPTS -->
    <?php use_javascript('/AdminLTE3/plugins/chart.js/Chart.min.js') ?>
    <!-- AdminLTE for demo purposes -->
    <?php use_javascript('/AdminLTE3/dist/js/demo.js') ?>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <?php use_javascript('/AdminLTE3/dist/js/pages/dashboard3.js') ?>
    <!-- Select2 -->
    <?php use_javascript('/AdminLTE3/plugins/select2/js/select2.full.min.js') ?>
    <!-- Toastr -->
    <?php use_javascript('/AdminLTE3/plugins/toastr/toastr.min.js') ?>
</head>

<body class="hold-transition layout-top-nav">
    <!-- Site wrapper -->
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
        </nav>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?php echo $sf_data->getRaw('sf_content') ?>
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <strong>Copyright &copy; 2022 <a href="https://ap.surabaya.go.id" target="_blank">Bagian Administrasi Pembangunan</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.1.0-rc
            </div>
        </footer>
    </div><!-- ./wrapper -->
</body>

</html>