<!DOCTYPE html>
<html>
<head>
  <?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
  <meta charset="UTF-8">
  <title><?php echo sfConfig::get('app_default_title') ?></title>
  <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
  <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Hind:300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
  
      <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.css'); ?> 
      <?php use_stylesheet('/login-form3/css/styleb.css'); ?>
        <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>

        <!-- jQuery 2.1.3 -->
        <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>
        <!-- Bootstrap 3.3.2 JS -->
        <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
        <?php use_javascript('/js/typed.js') ?>

  
</head>

<body class="bckg">
<!--   <div id="login-button">
  <img src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png">
  </img>
</div> -->
<div class="container-fluid">
  <div class="row" >
    <div class="col-sm-4 col-sm-push-4 col-md-4 col-md-push-4 coba" style="background-color:">
      <div id="kotaklogin" style="">
        <div class="bbb">
          <img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" class="zz" />

          <?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?>
          <!-- e-Budgeting 2017  -->
        </div>
        <hr>

                <?php if ($sf_flash->has('gagal')) { ?>
                    <div class="row">
                        <div class="col-xs-12">    
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                <?php echo ($sf_flash->get('gagal')) ?>.
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="row" style="margin-bottom: 15px">
                    <div class="col-xs-12 text-center">    
                        <div class="running-login-box-msg"></div>    
                    </div>
                </div>


        <!-- <form> -->
        <?php echo form_tag('login/satulogin'); ?>
          <input type="text" placeholder="Username" required name="send_user">
          <input type="password" required name="password" placeholder="Password">
          <!-- <a type="submit" href="#" class="bekasa">LOGIN</a> -->
          <button type="submit" class="button1">LOGIN</button>
          <!-- <button type="submit" class="btn btn-primary btn-block btn-flat text-bold">Log In&nbsp;&nbsp;<i class="glyphicon glyphicon-log-in"></i></button> -->
        <?php echo '</form>'; ?> 
      </div>

    </div>
  </div>
  <div class="row">
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xs-push-4 col-sm-push-4 col-md-push-4 col-lg-push-4" style="background-color: ">
        <p class="text-center">
          <a target="_blank" href="https://ebudgeting.surabaya.go.id/new_portal/" class="linkmanual">Portal</a>
        </p>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xs-push-4 col-sm-push-4 col-md-push-4 col-lg-push-4" style="background-color: ">
        <p class="text-center"><a target="_blank" href="http://budget.localhost/budget2017sf/pdf/SOP_eBudgeting.pdf" class="linkmanual">SOP</a></p>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xs-push-4 col-sm-push-4 col-md-push-4 col-lg-push-4" style="background-color: ">
        <p class="text-center"><a target="_blank" href="http://budget.localhost/budget2017sf/pdf/Manual_e-Revisi.pdf" class="linkmanual">Manual</a></p>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xs-push-4 col-sm-push-4 col-md-push-4 col-lg-push-4" style="background-color: ">
        <p class="text-center"><a data-toggle="modal" data-target="#modalAlur" href="#" class="linkmanual">Alur</a></p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 col-md-4 col-sm-push-4 col-md-push-4 copy">
      <p class=" text-center" style="margin-top: 10px;">Copyright &copy; <?php echo sfConfig::get('app_tahun_default') ?></p>
      <p class=" text-center"><a href="https://ap.surabaya.go.id"><b>Bagian Administrasi Pembangunan - Pemerintah Kota Surabaya</b></a></p>
      <p class=" text-center">All rights reserved</p>
    </div>
  </div>

                <!-- Modal -->
                <div class="modal fade" id="modalAlur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog"  style="width: 60%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Alur <?php echo sfConfig::get('app_default_aplikasi'); ?></h4>
                            </div>
                            <div class="modal-body">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#alur_awal" aria-controls="alur_awal" role="tab" data-toggle="tab">Alur Awal</a></li>
                                        <li role="presentation"><a href="#alur_revisi" aria-controls="alur_revisi" role="tab" data-toggle="tab">Alur Revisi</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="alur_awal">
                                            <?php echo image_tag('Alur.png', array('class' => 'img-responsive', 'width' => '100%')) ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="alur_revisi">
                                            <?php echo image_tag('Revisi.png', array('class' => 'img-responsive', 'width' => '100%')) ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
  
</div>
<!--         <p class="copy text-center">Bagian Administrasi Pembangunan - Pemerintah Kota Surabaya</p>
        <p class="copy text-center">All rights reserved</p> -->
    <?php use_javascript('/login-form3/js/jquery_2.1.3_jquery.min.js') ?>
    <?php use_javascript('/login-form3/js/TweenMax.min.js') ?>
    <?php use_javascript('/login-form3/js/index.js') ?>
    
</body>
</html>
