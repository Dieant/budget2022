<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo sfConfig::get('app_default_title') ?></title>
        <!--<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/favicon_grms.png' ?>" type="image/x-icon" />

        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Main style -->
        <?php use_stylesheet('/css/main.css'); ?>
        <!-- Font Awesome Icons -->
        <?php use_stylesheet('/AdminLTE3/plugins/fontawesome-free/css/all.min.css'); ?>
        <!-- Select2 -->
        <?php use_stylesheet('/AdminLTE3/plugins/select2/css/select2.min.css'); ?>
        <!-- IonIcons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE3/dist/css/adminlte.min.css'); ?>
        <!-- Toastr -->
        <?php use_stylesheet('/AdminLTE3/plugins/toastr/toastr.min.css'); ?>

        <!-- jQuery -->
        <?php use_javascript('/AdminLTE3/plugins/jquery/jquery.min.js') ?>
        <!-- Bootstrap -->
        <?php use_javascript('/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>
        <!-- AdminLTE -->
        <?php use_javascript('/AdminLTE3/dist/js/adminlte.js') ?>

        <!-- OPTIONAL SCRIPTS -->
        <?php use_javascript('/AdminLTE3/plugins/chart.js/Chart.min.js') ?>
        <!-- AdminLTE for demo purposes -->
        <?php use_javascript('/AdminLTE3/dist/js/demo.js') ?>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <?php use_javascript('/AdminLTE3/dist/js/pages/dashboard3.js') ?>
        <!-- Select2 -->
        <?php use_javascript('/AdminLTE3/plugins/select2/js/select2.full.min.js') ?>
        <!-- Toastr -->
        <?php use_javascript('/AdminLTE3/plugins/toastr/toastr.min.js') ?>
    </head>

    <body class="sidebar-mini sidebar-collapse sidebar-closed">
    <?php
        if (sfConfig::get('app_tahap_edit') == 'murni') {
            $nama_sistem = 'Belanja Daerah';
            $tag = 'Pra';
        } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
            $nama_sistem = 'Belanja Daerah PAK';
            $tag = 'PAK';
        } elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
            $nama_sistem = 'Belanja Daerah Penyesuaian';
            $tag = 'Rev';
        } else {
            $nama_sistem = 'Belanja Daerah Revisi';
            $tag = 'Rev';
        }
    ?>
        <!-- Site wrapper -->
        <div class="wrapper">
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                  </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item d-none d-sm-inline-block">
                      <a href="#" class="nav-link"><i class="fas fa-info-circle"></i> Bantuan</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                      <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                  </li>
                  <li class="nav-item dropdown">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
                      <i class="fas fa-power-off"></i>
                    </a>
                    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow" style="left:auto; right: 5% !important">
                      <li>
                        <?php echo link_to('<i class="fas fa-cog"></i> Ubah Password', 'peneliti/ubahPass', 'class=dropdown-item'); ?>
                      </li>
                      <li>
                        <?php echo link_to('<i class="fas fa-sign-out-alt"></i> Logout', 'login/logoutPeneliti', 'class=dropdown-item'); ?>
                      </li>
                    </ul>
                  </li>
                </ul>
            </nav>
            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                  <img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/favicon_grms.png' ?>" class="brand-image img-circle elevation-3" style="opacity: .8">
                  <span class="brand-text font-weight-light">e-Budgeting 2022</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                  <!-- Sidebar user panel (optional) -->
                  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                      <img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/avatar.png' ?>" class="img-circle elevation-2">
                    </div>
                    <div class="info">
                      <a href="#" class="d-block"><?php echo $sf_user->getNamaUser(); ?></a>
                    </div>
                  </div>

                  <!-- SidebarSearch Form -->
                  <div class="form-inline">
                    <div class="input-group" data-widget="sidebar-search">
                      <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                      <div class="input-group-append">
                        <button class="btn btn-sidebar">
                          <i class="fas fa-search fa-fw"></i>
                        </button>
                      </div>
                    </div>
                  </div>

                  <!-- Sidebar Menu -->
                  <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                      <li class="nav-item menu-open">
                        <a href="https://ebudgeting.surabaya.go.id/2022/report/dashboard.html" class="nav-link active">
                          <i class="nav-icon fas fa-tachometer-alt"></i>
                          <p>
                            Dashboard
                            <i class="right fas fa-angle"></i>
                          </p>
                        </a>
                      </li>
                      <?php if ($sf_user->getNamaUser() == 'inspect' || $sf_user->getNamaUser() == 'peninjau' || $sf_user->getNamaUser() == 'masger' || $sf_user->getNamaUser() == 'bppk' || $sf_user->getNamaUser() == 'dppk') 
                      {
                      ?>
                      <li class="nav-header">LEMBAR KERJA</li>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-copy"></i>
                          <p>
                            Kertas Kerja
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-danger right"><?php echo $tag ?></span>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;'.$nama_sistem.'</p>','peneliti/listRevisi?menu=Belanja', 'class=nav-link'); ?>
                          </li>  
                          <!-- <li class="nav-item">
                            <?php //echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Pendapatan Daerah</p>','peneliti/listRevisi?menu=Pendapatan', 'class=nav-link'); ?>
                          </li> -->
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Kertas Kerja</p>','peneliti/list', 'class=nav-link'); ?>
                          </li>
                           <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Komponen '.$nama_sistem.'</p>','peneliti/krkaRevisi', 'class=nav-link'); ?>
                          </li>                         
                        </ul>
                      </li>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-bullhorn"></i>
                          <p>
                            Request Penyelia
                            <i class="fas fa-angle-left right"></i>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Buat Request</p>','peneliti/buatRequestEdit', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;List Request</p>','peneliti/requestlist', 'class=nav-link'); ?>
                          </li>
                        </ul>
                      </li>
                      <li class="nav-header">MASTER</li>
                      <li class="nav-item">
                        <?php echo link_to('<i class="nav-icon fas fa-users"></i><p>&nbsp;Pengguna</p>','peneliti/userlist', 'class=nav-link'); ?>
                      </li>
                      <?php  
                      }
                      if ($sf_user->getNamaUser() == 'bpk') 
                      {
                      ?>
                      <li class="nav-header">LEMBAR KERJA</li>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-copy"></i>
                          <p>
                            Kertas Kerja
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-danger right"><?php echo $tag ?></span>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Kertas Kerja</p>','bpk/list', 'class=nav-link'); ?>
                          </li>
                        </ul>
                      </li>
                      <li class="nav-item">
                        <?php echo link_to('<i class="nav-icon fas fa-chart-pie"></i><p>&nbsp;Laporan</p>','report/report', 'class=nav-link'); ?>
                      </li>
                      <?php } elseif ($sf_user->getNamaUser() != 'bpk' && $sf_user->getNamaUser() != 'inspect' && $sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'dppk' && $sf_user->getNamaUser() != 'bppk' && $sf_user->getNamaUser() != 'masger') 
                      {
                      ?>
                      <li class="nav-header">LEMBAR KERJA</li>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-copy"></i>
                          <p>
                            Kertas Kerja
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-danger right"><?php echo $tag ?></span>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp'.$nama_sistem.'</p>','peneliti/listRevisi?menu=Belanja', 'class=nav-link'); ?>
                          </li>  
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Pendapatan Daerah</p>','peneliti/listRevisi?menu=Pendapatan', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Kertas Kerja</p>','peneliti/list', 'class=nav-link'); ?>
                          </li>
                           <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Komponen '.$nama_sistem.'</p>','peneliti/krkaRevisi', 'class=nav-link'); ?>
                          </li>
                        </ul>
                      </li>
                      <li class="nav-item">
                        <?php echo link_to('<i class="nav-icon fas fa-chart-pie"></i><p>&nbsp;Laporan</p>','report/report', 'class=nav-link'); ?>
                      </li>
                      <?php
                        $boleh = false;
                        $c_user_handle = new Criteria();
                        $c_user_handle->add(UserHandleV2Peer::USER_ID, $sf_user->getNamaLogin(), Criteria::EQUAL);
                        $user_handles = UserHandleV2Peer::doSelect($c_user_handle);
                        // $user_kecuali = array('adam.yulian','adhitiya');

                        foreach($user_handles as $user_handle){
                            if($user_handle->getStatusUser() != 'shs'){
                                $boleh = true;
                            }
                        }
                      ?>
                      <?php if($boleh==true) { ?>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-bullhorn"></i>
                          <p>
                            Request Penyelia
                            <i class="fas fa-angle-left right"></i>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Buat Request</p>','peneliti/buatRequestEdit', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;List Request</p>','peneliti/requestlist', 'class=nav-link'); ?>
                          </li>
                        </ul>
                      </li>
                      <?php } ?>
                      <li class="nav-header">MASTER</li>
                      <li class="nav-item">
                        <?php echo link_to('<i class="nav-icon fas fa-users"></i><p>&nbsp;Pengguna</p>','peneliti/userlist', 'class=nav-link'); ?>
                      </li>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fas fa-tree"></i>
                          <p>
                            Standar Harga
                            <i class="fas fa-angle-left right"></i>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;SHS</p>','shsd/sshlocked', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;HSPK</p>','hspk/hspklist', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;ASB</p>','asb/asblist', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Estimasi</p>', 'estimasi/estlist', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;BTL</p>', 'estimasi/btllist', 'class=nav-link'); ?>
                          </li>
                        </ul>
                      </li>
                      <li class="nav-item">
                        <?php echo link_to('<i class="fas fa-database nav-icon"></i><p>&nbsp;Rekening</p>', 'peneliti/rekeningList', 'class=nav-link'); ?>
                      </li>
                      <?php $penyelia_bpkad = array('fajar','rachma','nia','agus.santoso','adon','anggy'); ?>
                      <?php if($boleh == false && !in_array($sf_user->getNamaLogin(), $penyelia_bpkad)) { ?>
                      <li class="nav-header">Verifikasi Usulan</li> 
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fab fa-telegram-plane"></i>
                          <p>
                            Usulan SHS
                            <i class="fas fa-angle-left right"></i>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Tambah Usulan SHS</p>', 'usulan_ssh/usulansshbaru', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Konfirmasi Usulan SHS</p>', 'usulan_ssh/usulansshkonfirmasi', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;List Usulan SHS</p>', 'usulan_ssh/usulansshlistdinas', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;List Usulan Dinas</p>', 'usulan_ssh/ceklistusulandinas', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Perbaikan Usulan SHS</p>', 'usulan_ssh/usulanperbaikanlist', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;List Usulan Terverifikasi</p>', 'usulan_ssh/usulansshverifikasi', 'class=nav-link'); ?>
                          </li>
                          <li class="nav-item">
                            <?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Rekap List Usulan SHS</p>', 'usulan_ssh/usulansshlist', 'class=nav-link'); ?>
                          </li>
                        </ul>
                      </li>
                      <?php } ?>
                      <?php 
                      } 
                      if ($sf_user->getNamaLogin() == 'admin' || $sf_user->getNamaLogin() == 'perancangan_2600' || $sf_user->getNamaLogin() == 'pematusan_2600' || $sf_user->getNamaLogin() == 'jalan_2600' || $sf_user->getNamaLogin() == 'permukiman_2300' || $sf_user->getNamaLogin() == 'sarpras_2800') 
                      { 
                      ?>
                      <li class="nav-header">Pekerjaan</li>
                      <li class="nav-item">
                        <a href="#" class="nav-link">
                          <i class="nav-icon fab fa-product-hunt"></i>
                          <p>
                            Waiting List
                            <i class="fas fa-angle-left right"></i>
                          </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item"><?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Waiting List '.$nama_sistem.'</p>', 'waitinglist_pu/waitinglist', 'class=nav-link'); ?></li>
                            <li class="nav-item"><?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Waiting List Kertas Kerja</p>', 'waitinglist_pu/waitinglistrka', 'class=nav-link'); ?></li>
                            <li class="nav-item"><?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Tambah Komponen</p>', 'waitinglist_pu/waitingcari', 'class=nav-link'); ?></li>
                            <li class="nav-item"><?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Upload Komponen</p>', 'waitinglist_pu/waitingupload', 'class=nav-link'); ?></li>
                            <li class="nav-item"><?php echo link_to('<i class="far fa-circle nav-icon"></i><p>&nbsp;Log</p>', 'waitinglist_pu/seluruhwaitinglist', 'class=nav-link'); ?></li>
                        </ul>
                      </li>
                      <?php } ?>
                    </ul>
                  </nav>
                </div>
            <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $sf_data->getRaw('sf_content') ?>
            </div><!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2022 <a href="https://ap.surabaya.go.id" target="_blank">Bagian Administrasi Pembangunan</a>.</strong>
                All rights reserved.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 3.1.0-rc
                </div>
            </footer>

        </div><!-- ./wrapper -->
        <script>
            $(document).ready(function () {
                $(".js-example-basic-single").select2();
                $('.select2').select2()
                //Initialize Select2 Elements
                $('.select2bs4').select2({
                    theme: 'bootstrap4'
                });
            });
        </script>
    </body>
</html>