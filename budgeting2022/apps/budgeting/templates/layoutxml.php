<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo sfConfig::get('app_default_title') ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
        <!-- Bootstrap 3.3.2 -->
        <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.min.css'); ?>
        <!-- Font Awesome Icons -->
        <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Ionicons -->
        <?php use_stylesheet('/AdminLTE/ionicons/ionicons.min.css'); ?>
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-purple.min.css'); ?>
        <!-- PacePage -->
        <?php use_stylesheet('/AdminLTE/plugins/pace/pace.css'); ?>
        
        <!-- jQuery 2.1.3 -->
        <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>
        <!-- Bootstrap 3.3.2 JS -->
        <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
        <!-- SlimScroll -->
        <?php use_javascript('/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') ?>
        <!-- FastClick -->
        <?php use_javascript('/AdminLTE/plugins/fastclick/fastclick.min.js') ?>
        <!-- AdminLTE App -->
        <?php use_javascript('/AdminLTE/dist/js/app.min.js') ?>
        <!-- PacePage -->
        <?php use_javascript('/AdminLTE/plugins/pace/pace.js') ?>
        <!-- AdminLTE for demo purposes -->
        <?php // use_javascript('/AdminLTE/dist/js/demo.js') ?>

    </head>
    <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
    <body class="skin-purple sidebar-collapse fixed">
        <!-- Site wrapper -->
        <div class="wrapper">

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $sf_data->getRaw('sf_content') ?>
            </div><!-- /.content-wrapper -->
            
        </div><!-- ./wrapper -->
    </body>
    <script>$(document).ajaxStart(function() { Pace.restart(); });</script>
</html>