<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo sfConfig::get('app_default_title') ?></title>
        <!--<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
        <!-- Bootstrap 3.3.2 -->
        <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.min.css'); ?>
        <!-- Font Awesome Icons -->
        <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Ionicons -->
        <?php use_stylesheet('/AdminLTE/ionicons/ionicons.min.css'); ?>
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-green-light.min.css'); ?>
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-black.min.css'); ?>
        <!-- PacePage -->
        <?php use_stylesheet('/AdminLTE/plugins/pace/pace.css'); ?>

        <!--        css sf_admin symfony-->
        <?php use_stylesheet('/sf/sf_admin/css/main') ?>

        <!-- jQuery 2.1.3 -->
        <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>

        <!-- Bootstrap 3.3.2 JS -->
        <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
        <!-- SlimScroll -->
        <?php use_javascript('/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') ?>
        <!-- FastClick -->
        <?php use_javascript('/AdminLTE/plugins/fastclick/fastclick.min.js') ?>

        <!-- AdminLTE App -->
        <?php use_javascript('/AdminLTE/dist/js/app.min.js') ?>

        <!-- PacePage -->
        <?php use_javascript('/AdminLTE/plugins/pace/pace.js') ?>
        <!-- AdminLTE for demo purposes -->
        <?php // use_javascript('/AdminLTE/dist/js/demo.js') ?>

        <?php use_stylesheet('/select2/select2.css'); ?>
        <?php use_stylesheet('/select2/select2-bootstrap.css'); ?>
        <?php use_javascript('/select2/select2.min.js'); ?>

    </head>
    
    <style>
        .skin-black .main-header>.logo{
            color: red;
        }
        
    </style>
    <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
    <?php
    $skin = 'green';
    // if (sfConfig::get('app_default_coding') == 'testbed' . sfConfig::get('app_tahun_default') . 'sf') {
    //     $skin = 'black';
    // }
    ?>
    <body class="skin-<?php echo $skin ?> sidebar-collapse fixed">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <a href="#" class="logo">
                    <b>
                        <?php
                        if (sfConfig::get('app_default_coding') == 'testbed' . sfConfig::get('app_tahun_default') . 'sf') {
                            echo 'TESBED ' . sfConfig::get('app_tahun_default');
                        } else {
                            echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default');
                        }
                        ?>
                    </b>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        &nbsp;Main Menu
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-exclamation-circle"></i>
                                    <span class="hidden-xs">Bantuan</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="footer">
                                        <a target="_blank" href="https://chrome.google.com/webstore/detail/save-as-pdf/kpdjmbiefanbdgnkcikhllpmjnnllbbc">Plugin Browser Simpan Halaman Web<br/>(Chrome)</a>
                                    </li>
                                    <li class="footer">
                                        <a target="_blank" href="https://addons.mozilla.org/id/firefox/addon/save-as-pdf/">Plugin Browser Simpan Halaman Web<br/>(Firefox)</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php echo image_tag('user.png', 'class=user-image') ?>
                                    <span class="hidden-xs"><?php echo $sf_user->getNamaUser(); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <?php echo image_tag('user.png', 'class=img-circle') ?>
                                        <p style="text-align: center">
                                            <?php
                                            $c = new Criteria();
                                            $c->add(UnitKerjaPeer::UNIT_NAME, $sf_user->getNamaUser());
                                            $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                                            if ($rs_unitkerja) {
                                                $unit_id = $rs_unitkerja->getUnitId();
                                            }
                                            //print_r($unit_id);exit;
                                            $c = new Criteria();
                                            $c->add(UserHandleV2Peer::UNIT_ID, $unit_id);
                                            $rs_handle = UserHandleV2Peer::doSelect($c);
                                            foreach ($rs_handle as $v) {
                                                $nama_peneliti = $v->getUserId();
                                                if ($nama_peneliti) {
                                                    $b = new Criteria();
                                                    $b->add(SchemaAksesV2Peer::USER_ID, $nama_peneliti);
                                                    $bs = SchemaAksesV2Peer::doSelectOne($b);
                                                    if ($bs) {
                                                        if (($bs->getLevelId() == '2') && ($nama_peneliti != 'inspect') && ($nama_peneliti != 'peninjau') && ($nama_peneliti != 'parlemen2') && ($nama_peneliti != 'walikota') && ($nama_peneliti != 'masger') && ($nama_peneliti != 'binaprogram') && ($nama_peneliti != 'asisten1') && ($nama_peneliti != 'asisten2') && ($nama_peneliti != 'asisten3') && ($nama_peneliti != 'bpk') && ($nama_peneliti != 'prk') && ($nama_peneliti != 'bappeko') && ($nama_peneliti != 'bawas') && ($nama_peneliti != 'inspektorat1') && ($nama_peneliti != 'inspektorat2') && ($nama_peneliti != 'inspektorat3')) {
                                                            $penyelia = $nama_peneliti;
                                                        } elseif (($bs->getLevelId() == '8') && ($nama_peneliti != 'prk') && ($nama_peneliti != 'perencana') && ($nama_peneliti != 'ugm')) {
                                                            $penyelia_bappeko = $nama_peneliti;
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                            <?php echo $sf_user->getNamaUser(); ?> 
                                            <small>(<?php echo $sf_user->getNamaLengkap(); ?>)</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="col-xs-12 text-center">
                                            Penyelia Administrasi Pembangunan : <?php echo $penyelia; ?>
                                        </div>
                                    </li>
                                    <li class="user-body">
                                        <?php if ($sf_user->getNamaLengkap() == 'gis_2600') { ?>
                                            <div class="col-xs-12 text-center">
                                                <?php
                                                echo link_to('Profil User', 'entri/ubahProfil');
                                                ?>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-xs-6 text-center">
                                                <?php
                                                echo link_to('Profil Kepala SKPD', 'entri/setting');
                                                ?>
                                            </div>
                                            <div class="col-xs-6 text-center">
                                                <?php
                                                echo link_to('Profil User', 'entri/ubahProfil');
                                                ?>
                                            </div>
                                        <?php } ?>

                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <?php
                                            echo button_to('Change Password', 'entri/ubahPass', 'class=btn btn-default btn-flat');
                                            ?>
                                        </div>
                                        <div class="pull-right">
                                            <?php echo button_to('Logout', 'login/logoutDinas', 'class=btn btn-default btn-flat') ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header> 

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN MENU</li>
                        <li>
                            <?php
                            echo link_to('<i class="fa fa-file-text"></i> Data Pengguna', 'dinasadmin/userlist');
                            ?>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $sf_data->getRaw('sf_content') ?>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.3
                </div>
                <strong>Copyright &copy; <?php echo sfConfig::get('app_tahun_default') ?>  <a href="https://ap.surabaya.go.id" target="_blank">Bagian Administrasi Pembangunan</a> - <a href="https://www.surabaya.go.id" target="_blank">Pemerintah Kota Surabaya</a>.</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->
        <script>$(document).ajaxStart(function () {
                Pace.restart();
            });</script>
    </body>
</html>