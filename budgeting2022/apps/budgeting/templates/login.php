<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?php echo sfConfig::get('app_default_title') ?></title>
  <link href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" rel="icon" type="image/png">
  <!-- Fonts -->  
  <?php use_stylesheet('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'); ?>
  <!-- Icons -->
  <?php use_stylesheet('/argon/assets/vendor/nucleo/css/nucleo.css'); ?>
  <?php use_stylesheet('/argon/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>  
  <!-- Argon CSS -->
  <?php use_stylesheet('/argon/assets/css/argon.css?v=1.0.0'); ?>
 
</head>

<body class="bg-default">
  <div class="main-content">
    <!-- Navbar -->
    
    <!-- Header -->
   <div class="header py-7 py-lg-8" style="background-image: url('<?php echo sfConfig::get('app_path_default_sf') . 'images/cak.jpg' ?>'); opacity: 0.8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
            <!-- <img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" /> -->
            <img width="380px" src="<?php echo sfConfig::get('app_path_default_sf') . 'images/ap.png' ?>" />
              <h1 class="text-white font-weight-bold"><?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?></h1>
              <!-- <p class="text-white text-light">Selamat Datang di Aplikasi <?php echo sfConfig::get('app_default_aplikasi')?></p> -->
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-8">
      <div class="row justify-content-center">
        <div class="col-lg-7 col-md-7">
          <div class="card border-0" style="background-color: transparent;">
            
            <div class="card-body px-lg-5 py-lg-5">                            
             <?php echo form_tag('login/satulogin'); ?>   
             <?php if ($sf_flash->has('gagal')) { ?>
              <div class="row">
                  <div class="col-lg-12">    
                      <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                          <?php echo ($sf_flash->get('gagal')) ?>.
                      </div>
                  </div>
              </div>
              <?php
          }
          ?>
                 <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                    </div>
                    <input class="form-control" placeholder="Username" type="text" name="send_user"
                    >
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-key-25"></i></span>
                    </div>
                    <input class="form-control" placeholder="Password" type="password" name="password">
                  </div>
                </div>
                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                  <label class="custom-control-label" for=" customCheckLogin">
                    <span class="text-muted">Remember me</span>
                  </label>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4" >&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;&nbsp;&nbsp;&nbsp;</button>
                </div>
               <?php echo '</form>';?>
<!-- 
              <div class="btn-wrapper text-center">
                <a href="https://ebudgeting.surabaya.go.id/new_portal/info/Alur_Penyusunan.pdf" class="btn btn-neutral btn-icon" style="margin: 5px;">
                  <span class="btn-inner--icon"><i class="ni ni-map-big"></i></span>
                  <span class="btn-inner--text">ALUR</span>
                </a>
                <a href="https://ebudgeting.surabaya.go.id/new_portal/info/SOP_eBudgeting.pdf" class="btn btn-neutral btn-icon">
                  <span class="btn-inner--icon"><i class="ni ni-world-2"></i></span>
                  <span class="btn-inner--text">SOP&nbsp;</span>
                </a>
                <a href="https://ebudgeting.surabaya.go.id/new_portal/info/Manual_e-Revisi.pdf" class="btn btn-neutral btn-icon" style="margin-left:-5px;">
                  <span class="btn-inner--icon"><i class="ni ni-credit-card"></i></span>
                  <span class="btn-inner--text">MANUAL</span>
                </a>
                <a href="https://ebudgeting.surabaya.go.id/new_portal/" class="btn btn-neutral btn-icon">
                  <span class="btn-inner--icon"><i class="ni ni-single-copy-04"></i></span>
                  <span class="btn-inner--text">PORTAL</span>
                </a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
 <!-- <footer class="py-5">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-8">
          <div class="copyright text-center text-xl-left text-muted">
            
           Copyright &copy; 2020 <a href="https://ap.surabaya.go.id" class="font-weight-bold ml-1" target="_blank">Bagian Administrasi Pembangunan - Pemerintah Kota Surabaya</a>
          </div>
        </div>
      </div>
    </div>
  </footer>  -->
  <!-- Argon Scripts -->
  <!-- Core -->
   <?php use_javascript('/argon/assets/vendor/jquery/dist/jquery.min.js') ?>
   <?php use_javascript('/argon/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>  
  <!-- Argon JS -->
  <?php use_javascript('/argon/assets/js/argon.js?v=1.0.0') ?>  
</body>
</html>
