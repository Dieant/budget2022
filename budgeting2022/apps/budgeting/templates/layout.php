<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo sfConfig::get('app_default_title') ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
        <!-- Bootstrap 3.3.2 -->
        <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.min.css'); ?>
        <!-- Font Awesome Icons -->
        <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Ionicons -->
        <?php use_stylesheet('/AdminLTE/ionicons/ionicons.min.css'); ?>
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-green-light.min.css'); ?>
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-black.min.css'); ?>
        <!-- PacePage -->
        <?php use_stylesheet('/AdminLTE/plugins/pace/pace.css'); ?>

        <!--        css sf_admin symfony-->
        <?php use_stylesheet('/sf/sf_admin/css/main') ?>

        <!-- jQuery 2.1.3 -->
        <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>
        <!-- Bootstrap 3.3.2 JS -->
        <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
        <!-- SlimScroll -->
        <?php use_javascript('/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') ?>
        <!-- FastClick -->
        <?php use_javascript('/AdminLTE/plugins/fastclick/fastclick.min.js') ?>
        <!-- AdminLTE App -->
        <?php use_javascript('/AdminLTE/dist/js/app.min.js') ?>
        <!-- PacePage -->
        <?php use_javascript('/AdminLTE/plugins/pace/pace.js') ?>
        <!-- AdminLTE for demo purposes -->
        <?php // use_javascript('/AdminLTE/dist/js/demo.js') ?>

        <?php use_stylesheet('/select2/select2.css'); ?>
        <?php use_stylesheet('/select2/select2-bootstrap.css'); ?>
        <?php use_javascript('/select2/select2.min.js'); ?>

    </head>
    
    <style>
        .skin-black .main-header>.logo{
            color: red;
        }
       /*.box.box-solid.box-primary>.box-header {
         color: #fff;
         background: #605ca8;
         background-color: #605ca8;
        }*/
    </style>
    <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
    <?php
    $skin = 'yellow';
    // if (sfConfig::get('app_default_coding') == 'testbed' . sfConfig::get('app_tahun_default') . 'sf') {
    //     $skin = 'black';
    // }
    ?>
    <body class="skin-<?php echo $skin ?> sidebar-collapse fixed">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="#" class="logo">
                    <b>
                        <?php
                        if (sfConfig::get('app_default_coding') == 'testbed' . sfConfig::get('app_tahun_default') . 'sf') {
                            echo 'TESBED ' . sfConfig::get('app_tahun_default');
                        } else {
                            echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default');
                        }
                        ?>
                    </b>
                </a>
                <nav class="navbar navbar-static-top" role="navigation"></nav>
            </header>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $sf_data->getRaw('sf_content') ?>
            </div><!-- /.content-wrapper -->

        </div><!-- ./wrapper -->
        <script>$(document).ajaxStart(function () {
                Pace.restart();
            });</script>
    </body>
</html>