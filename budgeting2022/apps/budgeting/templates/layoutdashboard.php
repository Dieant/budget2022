<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo sfConfig::get('app_default_title') ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
        <!-- Icons -->
        <?php use_stylesheet('/argon/assets/vendor/nucleo/css/nucleo.css'); ?>
        <?php use_stylesheet('/argon/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css'); ?>       
        <!-- Bootstrap 3.3.2 -->
        <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.min.css'); ?>
        <!-- Font Awesome Icons -->
        <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Ionicons -->
        <?php use_stylesheet('/AdminLTE/ionicons/ionicons.min.css'); ?>
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-green-light.min.css'); ?>
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-black.min.css'); ?>
        <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-blue-light.min.css'); ?>
        <!-- PacePage -->
        <?php use_stylesheet('/AdminLTE/plugins/pace/pace.css'); ?>
        
        <!-- jQuery 2.1.3 -->
        <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>
        <!-- Bootstrap 3.3.2 JS -->
        <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
        <!-- SlimScroll -->
        <?php use_javascript('/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') ?>
        <!-- FastClick -->
        <?php use_javascript('/AdminLTE/plugins/fastclick/fastclick.min.js') ?>
        <!-- AdminLTE App -->
        <?php use_javascript('/AdminLTE/dist/js/app.min.js') ?>
        <!-- PacePage -->
        <?php use_javascript('/AdminLTE/plugins/pace/pace.js') ?>
        <!---chartJS--->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>
        <!-- AdminLTE for demo purposes -->
        <?php // use_javascript('/AdminLTE/dist/js/demo.js') ?>

    </head>
    <style>
        .skin-black .main-header>.logo{
            color: red;
        }
        .highcharts-figure, .highcharts-data-table table {
            min-width: 310px; 
            max-width: 800px;
            margin: 1em auto;
        }

        #container {
            height: 700px;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }
        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }
        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }
        .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
            padding: 0.5em;
        }
        .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }
        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }
        .font-number h3{
           font-size: 32px;
        }
    </style>
    <?php
    $skin = 'black';
    ?>
    <body class="hold-transition skin-<?php echo $skin ?> sidebar-collapse sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header">
                <a href="#" class="logo">
                    <img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" alt="..." width="20">
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" style="background-color: #367ea8">
                        &nbsp;e-Budgeting
                    </a>
                </nav>
            </header>
            <!-- Content Wrapper. Contains page content -->
            <div class="content">
                <?php echo $sf_data->getRaw('sf_content') ?>
            </div><!-- /.content-wrapper -->
            </div><!-- ./wrapper -->
    </body>
</html>