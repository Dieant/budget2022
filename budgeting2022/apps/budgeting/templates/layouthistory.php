<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>

        <?php include_http_metas() ?>
        <?php include_metas() ?>

        <?php include_title() ?>

        <?php use_helper('I18N', 'Date', 'Validation') ?>
        <?php use_stylesheet('/font_awesome/css/font-awesome.min.css'); ?>
        <?php use_stylesheet('/css/irul-baru2015.css'); ?>

        <link rel="shortcut icon" href="http://budgeting.surabaya2excellence.or.id/gentong.ico"  type="image/x-icon" />

    </head>
    <body>
        <?php
        if (sfConfig::get('app_tahap_edit') == 'murni') {
            $nama_sistem = 'Pra-RKA';
        } elseif (sfConfig::get('app_tahap_edit') == 'pak') {
            $nama_sistem = 'PAK';
        } elseif (sfConfig::get('app_tahap_edit') == 'penyesuaian') {
            $nama_sistem = 'Penyesuaian';
        } else {
            $nama_sistem = 'Revisi';
        }
        ?>
        <div id="budget_header_wrapper">

            <div id="budget_banner" align="center">
            </div>

            <div id="budget_menu">
                <?php if ($sf_user->getCredentialMember() == 'admin' || $sf_user->getCredentialMember() == 'admin_super') { ?>
                    <ul>
                        <li><?php echo link_to('Pengguna', 'user_app/userlist'); ?></li>
                        <li><?php echo link_to('REPORT', 'report/report'); ?></li>
                        <li><?php echo link_to('RKA', 'kegiatan/list'); ?></li>
                        <li><?php echo link_to('REKAP', 'admin/rekap'); ?></li>
                        <li><?php echo link_to('MASALAH', 'masalah/list'); ?></li>
                        <?php
                        $nama = $sf_request->getCookie('user_admin');
                        if (($nama == 'admin')) {
                            $userData = 'data';
                            $passwordData = 'suroboyo';
                            ?>  
                            <li><?php echo link_to('APP', 'admin/statusApp'); ?></li>
                            <li><?php echo link_to('USER LOG', 'admin/userLog'); ?></li>
                            <!--<li><?php echo link_to('SMARTY', sfConfig::get('app_path_default') . 'security/login.php?folder=admin', array('target' => 'blank')); ?></li>-->
                            <li><?php echo link_to('DATA', sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $userData . '&password=' . $passwordData . '&folder=data', array('target' => 'blank')); ?></li>
                            <?php
                        }
                    } else if ($sf_user->getCredentialMember() == 'dinas') {
                        ?>
                        <ul>
                            <li><?php echo link_to('RKA', 'dinas/list'); ?></li>
                            <!-- ditutup dulu waktu pembahasan di dewan-->
                            <li><?php echo link_to('KOMPONEN', 'dinas/shsdlockedlist'); ?></li>
                            <!--<li><a href="#" accesskey="3" title="">ASB PENELITIAN</a></li>-->
                            <li><?php echo link_to('SETTING', 'dinas/setting'); ?></li>
                            <li><?php echo link_to('PERNYATAAN', 'dinas/eula'); ?></li>
                            <li><?php echo link_to('VALIDASI', 'digisign/validasi'); ?></li> 
                        </ul>
                    <?php } else if ($sf_user->getCredentialMember() == 'peneliti') {
                        ?>
                        <ul>
                            <li><?php echo link_to('HOME', 'peneliti/list'); ?></li>

                            <?php
                            if ($sf_user->getNamaUser() != 'parlemen2' && $sf_user->getNamaUser() != 'peninjau') {
                                ?>
                                <li><?php echo link_to('RKA', 'peneliti/list'); ?></li>
                                <li><?php echo link_to('KOMPONEN', 'peneliti/shsdlockedlist'); ?></li>
                                <li><?php echo link_to('REPORT', 'report/report'); ?></li>
                                <li><?php echo link_to('VALIDASI', 'digisign/validasi'); ?></li>
                                <?php
                            }
                            if ($sf_user->getNamaUser() == 'parlemen2' && $sf_user->getNamaUser() != 'peninjau') {
                                ?>
                                <li><?php //echo link_to('RKA+Penyesuaian', 'report/report');            ?></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </ul>
            </div><!-- end of budget_menu -->
        </div> <!-- end of header_wrapper -->

        <div class="info">
            <?php
            if ($sf_user->getCredentialMember() == 'peneliti') {
                if ($sf_user->getNamaUser() != 'peninjau' && $sf_user->getNamaUser() != 'wawali' && $sf_user->getNamaUser() != 'walikota' && $sf_user->getNamaUser() != 'asisten_walikota' && $sf_user->getNamaUser() != 'sekda' && $sf_user->getNamaUser() != 'asisten' && $sf_user->getNamaUser() != 'parlemen2') {
                    echo 'anda login peneliti sebagai: <b>';
                    echo $sf_user->getNamaLengkap() . ' (' . ($sf_user->getNamaUser()) . ')</b>&nbsp&nbsp;';
                    echo button_to('ubah password', 'peneliti/ubahPass') . '&nbsp;' . button_to('logout', 'login/logoutPeneliti');
                    ?> 
                    <?php
                } else {

                    echo 'anda login sebagai: <b>';
                    echo ($sf_user->getNamaUser()) . '</b>&nbsp&nbsp;';
                    echo button_to('ubah password', 'peneliti/ubahPass') . '&nbsp;' . button_to('logout', 'login/logoutPeneliti');
                }
            } else if ($sf_user->getCredentialMember() == 'dinas') {
                echo 'anda login dinas sebagai: <b>';
                echo $sf_user->getNamaLengkap() . '(' . $sf_user->getNamaUser() . ')</b>&nbsp;';
                $c = new Criteria();
                $c->add(UnitKerjaPeer::UNIT_NAME, $sf_user->getNamaUser());
                $rs_unitkerja = UnitKerjaPeer::doSelectOne($c);
                if ($rs_unitkerja) {
                    $unit_id = $rs_unitkerja->getUnitId();
                }
                //print_r($unit_id);exit;
                $c = new Criteria();
                $c->add(UserHandleV2Peer::UNIT_ID, $unit_id);
                $rs_handle = UserHandleV2Peer::doSelect($c);
                foreach ($rs_handle as $v) {
                    $nama_peneliti = $v->getUserId();
                    if ($nama_peneliti) {
                        $b = new Criteria();
                        $b->add(SchemaAksesV2Peer::USER_ID, $nama_peneliti);
                        $bs = SchemaAksesV2Peer::doSelectOne($b);
                        if ($bs) {
                            if (($bs->getLevelId() == '2') && ($nama_peneliti != 'peninjau') && ($nama_peneliti != 'parlemen2') && ($nama_peneliti != 'walikota') && ($nama_peneliti != 'asisten_walikota')) {
                                $penyelia = $nama_peneliti;
                            } elseif (($bs->getLevelId() == '8') && ($nama_peneliti != 'prk') && ($nama_peneliti != 'perencana') && ($nama_peneliti != 'ugm')) {
                                $penyelia_bappeko = $nama_peneliti;
                            }
                        }
                    }
                }
                // ditutup dulu waktu pembahasan di dewan
                echo 'Nama Penyelia : <b>' . $penyelia . ' </b>[Administrasi Pembangunan], <b>' . $penyelia_bappeko . '</b> [Bappeko] ) &nbsp;';
                echo button_to('Ubah Profil', 'dinas/ubahProfil') . '&nbsp;' . button_to('Ubah Password', 'dinas/ubahPass') . '&nbsp;' . button_to('Logout', 'login/logoutDinas');
            } else if ($sf_user->getCredentialMember() == 'admin' || $sf_user->getCredentialMember() == 'admin_super') {
                ?>

                <?php
                echo 'Anda Login sebagai:<b> Administrator Administrasi Pembangunan ( ';
                echo ($sf_user->getNamaUser()) . ')</b>&nbsp&nbsp;';
                echo button_to('ubah password', 'admin/ubahPass') . '&nbsp;' . button_to('logout', 'login/logoutAdmin');
            }
            ?>
        </div>

        <div id="budget_content_wrapper_outer">
            <div id="budget_content_wrapper_inner">
                <div id="budget_content_isi">
                    <?php echo $sf_data->getRaw('sf_content') ?>
                </div>
            </div>
        </div>


        <div id="budget_footer_wrapper">
            <div id="budget_footer">
                Copyright © <?php echo sfConfig::get('app_tahun_default') ?> <a href="http://www.surabaya2excellence.or.id" target="_blank">Bagian Administrasi Pembangunan</a> <a href="http://www.surabaya.go.id" target="_blank">Pemerintah Kota Surabaya</a>
            </div> <!-- end of footer -->
        </div> <!-- end of footer_wrapper -->



    </body>
</html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
            <title><?php echo sfConfig::get('app_default_title') ?></title>
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
                <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
                <!-- Bootstrap 3.3.2 -->
                <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.min.css'); ?>
                <!-- Font Awesome Icons -->
                <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
                <!-- Ionicons -->
                <?php use_stylesheet('/AdminLTE/ionicons/ionicons.min.css'); ?>
                <!-- Theme style -->
                <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>
                <!-- AdminLTE Skins. Choose a skin from the css/skins 
                     folder instead of downloading all of them to reduce the load. -->
                <?php use_stylesheet('/AdminLTE/dist/css/skins/skin-green-light.min.css'); ?>
                <!-- PacePage -->
                <?php use_stylesheet('/AdminLTE/plugins/pace/pace.css'); ?>

                <!-- jQuery 2.1.3 -->
                <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>
                <!-- Bootstrap 3.3.2 JS -->
                <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
                <!-- SlimScroll -->
                <?php use_javascript('/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') ?>
                <!-- FastClick -->
                <?php use_javascript('/AdminLTE/plugins/fastclick/fastclick.min.js') ?>
                <!-- AdminLTE App -->
                <?php use_javascript('/AdminLTE/dist/js/app.min.js') ?>
                <!-- PacePage -->
                <?php use_javascript('/AdminLTE/plugins/pace/pace.js') ?>
                <!-- AdminLTE for demo purposes -->
                <?php // use_javascript('/AdminLTE/dist/js/demo.js') ?>

                </head>
                <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
                <body class="skin-green-light sidebar-collapse fixed">
                    <!-- Site wrapper -->
                    <div class="wrapper">

                        <header class="main-header">
                            <a href="#" class="logo"><b><?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?></b></a>
                            <!-- Header Navbar: style can be found in header.less -->
                            <nav class="navbar navbar-static-top" role="navigation">
                                <!-- Sidebar toggle button-->
                                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </a>
                                <div class="navbar-custom-menu">
                                    <ul class="nav navbar-nav">
                                        <!-- User Account: style can be found in dropdown.less -->
                                        <li class="dropdown user user-menu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <?php echo image_tag('user.png', 'class=user-image') ?>
                                                <span class="hidden-xs"><?php echo $sf_user->getNamaUser(); ?></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <!-- User image -->
                                                <li class="user-header">
                                                    <?php echo image_tag('user.png', 'class=img-circle') ?>
                                                    <p style="text-align: center">
                                                        <?php echo $sf_user->getNamaUser(); ?>
                                                        <small>(<?php echo $sf_user->getNamaLengkap(); ?>)</small>
                                                    </p>
                                                </li>
                                                <!-- Menu Footer-->
                                                <li class="user-footer">
                                                    <div class="pull-left">
                                                        <?php echo button_to('Change Password', 'shsd/ubahPass', 'class=btn btn-default btn-flat') ?>
                                                    </div>
                                                    <div class="pull-right">
                                                        <?php echo button_to('Logout', 'login/logoutAdmin', 'class=btn btn-default btn-flat') ?>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </header>

                        <!-- =============================================== -->

                        <!-- Left side column. contains the sidebar -->
                        <aside class="main-sidebar">
                            <!-- sidebar: style can be found in sidebar.less -->
                            <section class="sidebar">
                                <!-- sidebar menu: : style can be found in sidebar.less -->
                                <ul class="sidebar-menu">
                                    <li class="header">MAIN MENU</li>
                                    <?php if ($sf_user->getCredentialMember() == 'admin' || $sf_user->getCredentialMember() == 'admin_super') { ?>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-users"></i> Pengguna', 'user_app/userlist'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-file-text"></i> ' . $nama_sistem, 'kegiatan/listRevisi'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-file-text"></i> RKA', 'kegiatan/list'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-briefcase"></i> Request', 'kegiatan/requestlist'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-briefcase"></i> Komponen', 'admin/krka'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-briefcase"></i> Komponen ' . $nama_sistem, 'admin/krkaRevisi'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-archive"></i> Report', 'report/report'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-history"></i> User Log', 'admin/userLog'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-history"></i> Log Approval', 'report/logApproval'); ?>
                                        </li>
                                        <?php
                                        $nama = $sf_request->getCookie('user_admin');
                                        if (($nama == 'admin' or $nama == 'superadmin_ebudgeting')) {
                                            $userData = 'data';
                                            $passwordData = 'admdata';
                                            ?>  
                                            <li>
                                                <?php echo link_to('<i class="fa fa-wrench"></i> APP', 'admin/statusApp'); ?>
                                            </li>
                                            <!--                                            <li>
                                            <?php echo link_to('<i class="fa fa-desktop"></i> Smarty', sfConfig::get('app_path_default') . 'security/login.php?folder=admin', array('target' => 'blank')); ?>
                                                                                        </li>-->
                                            <li>
                                                <?php echo link_to('<i class="fa fa-desktop"></i> Data', sfConfig::get('app_path_default') . 'security/login2.php?send_user=' . $userData . '&password=' . $passwordData . '&folder=data', array('target' => 'blank')); ?>
                                            </li>
                                            <?php
                                        }
                                        ?>    
                                    <?php } else if ($sf_user->getCredentialMember() == 'dinas') { ?>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-file-text"></i> RKA', 'dinas/list'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-briefcase"></i> Komponen', 'dinas/shsdlockedlist'); ?>
                                        </li>
                                        <li>
                                            <?php echo link_to('<i class="fa fa-wrench"></i> Pernyataan', 'dinas/eula'); ?>
                                        </li>
                                    <?php } else if ($sf_user->getCredentialMember() == 'peneliti') { ?>

                                    <?php }
                                    ?>
                                </ul>
                            </section>
                            <!-- /.sidebar -->
                        </aside>

                        <!-- =============================================== -->

                        <!-- Content Wrapper. Contains page content -->
                        <div class="content-wrapper">
                            <?php echo $sf_data->getRaw('sf_content') ?>
                        </div><!-- /.content-wrapper -->

                        <footer class="main-footer">
                            <div class="pull-right hidden-xs">
                                <b>Version</b> 1.3
                            </div>
                            <strong>Copyright &copy; <?php echo sfConfig::get('app_tahun_default') ?>  <a href="https://ap.surabaya.go.id" target="_blank">Bagian Administrasi Pembangunan</a> - <a href="https://www.surabaya.go.id" target="_blank">Pemerintah Kota Surabaya</a>.</strong> All rights reserved.
                        </footer>
                    </div><!-- ./wrapper -->
                    <script>$(document).ajaxStart(function () {
                            Pace.restart();
                        });</script>
                </body>
                </html>