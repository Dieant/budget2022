<!DOCTYPE html>
<html>
<head>
  <?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>
  <meta charset="UTF-8">
  <title><?php echo sfConfig::get('app_default_title') ?></title>
  <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
  <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Hind:300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
  
  
      <?php use_stylesheet('/login-form3/css/stylea.css'); ?>

  
</head>

<body>
<!--   <div id="login-button">
  <img src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png">
  </img>
</div> -->
<div id="container">
  <h1><?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?></h1>
  <span class="close-btn">
  </span>

  <form>
    <input type="text" name="username" placeholder="Username">
    <input type="password" name="pass" placeholder="Password">
    <a href="#">Log in</a>

</form>
</div>

<!-- Forgotten Password Container -->

  <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.16.1/TweenMax.min.js'></script> -->
<!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <?php use_javascript('/login-form3/js/jquery_2.1.3_jquery.min.js') ?>
    <?php use_javascript('/login-form3/js/TweenMax.min.js') ?>
    <?php use_javascript('/login-form3/js/index.js') ?>
    
</body>
</html>
