<!DOCTYPE html>
<html>
    <head>
        <?php use_helper('I18N', 'Date', 'Url', 'Javascript', 'Form', 'Object') ?>

        <meta charset="UTF-8">
        <title><?php echo sfConfig::get('app_default_title') ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>" type="image/x-icon" />
        <!-- Bootstrap 3.3.2 -->
        <?php use_stylesheet('/AdminLTE/bootstrap/css/bootstrap.min.css'); ?>
        <!-- Font Awesome Icons -->
        <?php use_stylesheet('/font-awesome/css/font-awesome.min.css'); ?>
        <!-- Theme style -->
        <?php use_stylesheet('/AdminLTE/dist/css/AdminLTE.min.css'); ?>
        <!-- iCheck -->
        <?php use_stylesheet('/AdminLTE/plugins/iCheck/square/purple.css'); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->



        <!-- jQuery 2.1.3 -->
        <?php use_javascript('/AdminLTE/plugins/jQuery/jQuery-2.1.3.min.js') ?>
        <!-- Bootstrap 3.3.2 JS -->
        <?php use_javascript('/AdminLTE/bootstrap/js/bootstrap.min.js') ?>
        <?php use_javascript('/js/typed.js') ?>
    </head>
    <body class="login-page"
          style="background: rgb(242,245,246); /* Old browsers */
          background: -moz-radial-gradient(center, ellipse cover, rgba(242,245,246,1) 0%, rgba(227,234,237,1) 37%, rgba(200,215,220,1) 100%); /* FF3.6+ */
          background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(242,245,246,1)), color-stop(37%,rgba(227,234,237,1)), color-stop(100%,rgba(200,215,220,1))); /* Chrome,Safari4+ */
          background: -webkit-radial-gradient(center, ellipse cover, rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* Chrome10+,Safari5.1+ */
          background: -o-radial-gradient(center, ellipse cover, rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* Opera 12+ */
          background: -ms-radial-gradient(center, ellipse cover, rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* IE10+ */
          background: radial-gradient(ellipse at center, rgba(242,245,246,1) 0%,rgba(227,234,237,1) 37%,rgba(200,215,220,1) 100%); /* W3C */
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f5f6', endColorstr='#c8d7dc',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */">


        <script>
            $(document).ready(function() {
                $(".running-login-box-msg").typed({
                    strings: ["Selamat Datang  ", "Aplikasi <?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?>  ", "Aplikasi Penyusunan Anggaran Pemkot Surabaya","Silahkan Log In  "],
                    typeSpeed: 0
                });
            });
        </script>
        <style>
            .running-login-box-msg{
                opacity: 1;
                display: inline;
            }
            .box.box-solid.box-primary>.box-header {
         color: #fff;
         background: #605ca8;
         background-color: #605ca8;
        }
        </style>
        <div class="login-box">
            <!--            <div class="login-logo">
                            <a href="#"><b><?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?></b></a>
                        </div> /.login-logo -->
            <div class="login-box-body" style="box-shadow:3px 3px 0px #cccccc,7px 7px 0px #bbbbbb,11px 11px 0px #aaaaaa;">
                <div class="row">
                    <div class="col-xs-12" style="color: black">    
                        <h2><img src="<?php echo sfConfig::get('app_path_default_sf') . 'images/Ebudgeting.png' ?>"/> <?php echo sfConfig::get('app_default_aplikasi') . ' ' . sfConfig::get('app_tahun_default') ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">    
                        <hr style="background-color: black"/>
                    </div>
                </div>
                <?php if ($sf_flash->has('gagal')) { ?>
                    <div class="row">
                        <div class="col-xs-12">    
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                <?php echo ($sf_flash->get('gagal')) ?>.
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="row" style="margin-bottom: 15px">
                    <div class="col-xs-12 text-center">    
                        <div class="running-login-box-msg"></div>    
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        &nbsp;
                    </div>
                    <div class="col-xs-8">
                        <?php echo form_tag('login/satulogin') ?>                        

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Username" required name="send_user"/>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="Password" required name="password"/>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-offset-2 col-xs-8 col-xs-offset-2">
                                <button type="submit" class="btn btn-primary btn-block btn-flat text-bold">Log In</button>
                            </div><!-- /.col -->
                        </div>
                        <?php echo '</form>'; ?> 
                    </div>
                    <div class="col-xs-2">
                        &nbsp;
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">    
                        <hr style="background-color: black"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-8" style="padding-right: 0">
                        <div class="btn-group-justified">
                            <?php echo link_to('PORTAL', 'https://ebudgeting.surabaya.go.id/new_portal/', array('class' => 'btn btn-flat btn-primary text-bold', 'target' => '_blank')); ?>    
                            <?php echo link_to('SOP', sfConfig::get('app_path_pdf') . 'SOP_eBudgeting.pdf', array('class' => 'btn btn-flat btn-primary text-bold', 'target' => '_blank')); ?>    
                        </div>
                    </div>
                    <div class="col-xs-4" style="padding-left: 0">
                        <button type="button" class="btn btn-primary btn-block btn-flat text-bold" data-toggle="modal" data-target="#modalAlur">ALUR</button>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="modalAlur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog"  style="width: 60%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Alur <?php echo sfConfig::get('app_default_aplikasi'); ?></h4>
                            </div>
                            <div class="modal-body">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#alur_awal" aria-controls="alur_awal" role="tab" data-toggle="tab">Alur Awal</a></li>
                                        <li role="presentation"><a href="#alur_revisi" aria-controls="alur_revisi" role="tab" data-toggle="tab">Profile</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="alur_awal">
                                            <?php echo image_tag('Alur.png', array('class' => 'img-responsive', 'width' => '100%')) ?>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="alur_revisi">
                                            <?php echo image_tag('Revisi.png', array('class' => 'img-responsive', 'width' => '100%')) ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
    </body>
</html>