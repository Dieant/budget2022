
-----------------------------------------------------------------------------
-- history_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE history_pekerjaan CASCADE;


CREATE TABLE history_pekerjaan
(
	"tahun" INT2  NOT NULL,
	"lokasi" VARCHAR(500),
	"nilai" DOUBLE PRECISION,
	"kode" VARCHAR,
	"nomor" INTEGER  NOT NULL,
	"volume" VARCHAR
);

COMMENT ON TABLE history_pekerjaan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- kecamatan
-----------------------------------------------------------------------------

DROP TABLE kecamatan CASCADE;


CREATE TABLE kecamatan
(
	"nama" VARCHAR(150),
	"id" INTEGER
);

COMMENT ON TABLE kecamatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- kelompok_dinas
-----------------------------------------------------------------------------

DROP TABLE kelompok_dinas CASCADE;


CREATE TABLE kelompok_dinas
(
	"kelompok_id" NUMERIC(2)  NOT NULL,
	"kelompok_name" VARCHAR(150),
	"front_code" VARCHAR(20),
	PRIMARY KEY ("kelompok_id")
);

COMMENT ON TABLE kelompok_dinas IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- komentar
-----------------------------------------------------------------------------

DROP TABLE komentar CASCADE;

DROP SEQUENCE komentar_seq;

CREATE SEQUENCE komentar_seq;


CREATE TABLE komentar
(
	"no" INTEGER  NOT NULL,
	"judul" VARCHAR(255),
	"isi" VARCHAR,
	"user_id" VARCHAR(20),
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"locked" BOOLEAN default 'f',
	"tipe" INT2,
	PRIMARY KEY ("no")
);

COMMENT ON TABLE komentar IS '';


SET search_path TO public;
CREATE INDEX "fki_" ON komentar ("user_id");

ALTER TABLE komentar ADD CONSTRAINT "komentar_FK_1" FOREIGN KEY ("user_id") REFERENCES master_user ("user_id");

-----------------------------------------------------------------------------
-- master_bangunan
-----------------------------------------------------------------------------

DROP TABLE master_bangunan CASCADE;


CREATE TABLE master_bangunan
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_bangunan" VARCHAR(200),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_bangunan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_jalan
-----------------------------------------------------------------------------

DROP TABLE master_jalan CASCADE;


CREATE TABLE master_jalan
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_jalan" VARCHAR(100),
	"nama_ujung" VARCHAR(100),
	"nama_pangkal" VARCHAR(100),
	"kecamatan" VARCHAR(30),
	"kelurahan" VARCHAR(30),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_jalan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_puskesmas
-----------------------------------------------------------------------------

DROP TABLE master_puskesmas CASCADE;


CREATE TABLE master_puskesmas
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_puskesmas" VARCHAR(200),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_puskesmas IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_saluran
-----------------------------------------------------------------------------

DROP TABLE master_saluran CASCADE;


CREATE TABLE master_saluran
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_saluran" VARCHAR(200),
	"batasan" VARCHAR(500),
	"kecamatan" VARCHAR(100),
	"panjang" VARCHAR(50),
	"lebar" VARCHAR(50),
	"keterangan" VARCHAR(500),
	"kelurahan" VARCHAR(50),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_saluran IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_schema
-----------------------------------------------------------------------------

DROP TABLE master_schema CASCADE;

DROP SEQUENCE master_schema_seq;

CREATE SEQUENCE master_schema_seq;


CREATE TABLE master_schema
(
	"schema_id" INTEGER  NOT NULL,
	"schema_name" VARCHAR(30),
	PRIMARY KEY ("schema_id")
);

COMMENT ON TABLE master_schema IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_sd
-----------------------------------------------------------------------------

DROP TABLE master_sd CASCADE;


CREATE TABLE master_sd
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_sd" VARCHAR(200),
	"alamat" VARCHAR(200),
	"status" VARCHAR(50),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	"kode_jalan" VARCHAR(16),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_sd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_sma
-----------------------------------------------------------------------------

DROP TABLE master_sma CASCADE;


CREATE TABLE master_sma
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_sma" VARCHAR(200),
	"alamat" VARCHAR(200),
	"status" VARCHAR(50),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	"kode_jalan" VARCHAR(16),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_sma IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_smp
-----------------------------------------------------------------------------

DROP TABLE master_smp CASCADE;


CREATE TABLE master_smp
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_smp" VARCHAR(200),
	"alamat" VARCHAR(200),
	"status" VARCHAR(50),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	"kode_jalan" VARCHAR(16),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_smp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_tk
-----------------------------------------------------------------------------

DROP TABLE master_tk CASCADE;


CREATE TABLE master_tk
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama_tk" VARCHAR(200),
	"alamat" VARCHAR(200),
	"status" VARCHAR(50),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	"kode_jalan" VARCHAR(16),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE master_tk IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_user
-----------------------------------------------------------------------------

DROP TABLE master_user CASCADE;


CREATE TABLE master_user
(
	"user_id" VARCHAR(50)  NOT NULL,
	"user_name" VARCHAR(50),
	"user_default_password" VARCHAR(50),
	"user_password" VARCHAR(100),
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"user_enable" BOOLEAN,
	"nip" VARCHAR(50),
	"eula_budgeting" BOOLEAN,
	"jabatan" VARCHAR(500),
	"email" VARCHAR(100),
	"telepon" VARCHAR(50),
	"jenis_kelamin" VARCHAR(10),
	PRIMARY KEY ("user_id")
);

COMMENT ON TABLE master_user IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_user_v2
-----------------------------------------------------------------------------

DROP TABLE master_user_v2 CASCADE;


CREATE TABLE master_user_v2
(
	"user_id" VARCHAR(50)  NOT NULL,
	"user_name" VARCHAR(50),
	"user_default_password" VARCHAR(50),
	"user_password" VARCHAR(100),
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"user_enable" BOOLEAN,
	"nip" VARCHAR(50),
	"eula_budgeting" BOOLEAN,
	"jabatan" VARCHAR(500),
	"email" VARCHAR(100),
	"telepon" VARCHAR(50),
	"jenis_kelamin" VARCHAR(10),
	PRIMARY KEY ("user_id")
);

COMMENT ON TABLE master_user_v2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_user2
-----------------------------------------------------------------------------

DROP TABLE master_user2 CASCADE;


CREATE TABLE master_user2
(
	"username" VARCHAR(100),
	"passwd" VARCHAR(20),
	"strlike" VARCHAR(50)
);

COMMENT ON TABLE master_user2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_user_3
-----------------------------------------------------------------------------

DROP TABLE master_user_3 CASCADE;


CREATE TABLE master_user_3
(
	"user_id" VARCHAR(20)  NOT NULL,
	"user_name" VARCHAR(50),
	"user_default_password" VARCHAR(50),
	"user_password" VARCHAR(100),
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"user_enable" BOOLEAN,
	PRIMARY KEY ("user_id")
);

COMMENT ON TABLE master_user_3 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- pekerjaan
-----------------------------------------------------------------------------

DROP TABLE pekerjaan CASCADE;


CREATE TABLE pekerjaan
(
	"unit_id" VARCHAR(16),
	"kegiatan_code" VARCHAR(16),
	"komponen_id" VARCHAR(30),
	"unit_name" VARCHAR(200),
	"kegiatan_name" VARCHAR(300),
	"subtitle" VARCHAR(300),
	"komponen_name" VARCHAR(400),
	"detail_name" VARCHAR(400),
	"keterangan_koefisien" VARCHAR(200),
	"kecamatan" VARCHAR(500),
	"kelurahan" VARCHAR(800)
);

COMMENT ON TABLE pekerjaan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- schema_akses
-----------------------------------------------------------------------------

DROP TABLE schema_akses CASCADE;


CREATE TABLE schema_akses
(
	"user_id" VARCHAR(50)  NOT NULL,
	"schema_id" INTEGER  NOT NULL,
	"level_id" INTEGER,
	PRIMARY KEY ("user_id","schema_id")
);

COMMENT ON TABLE schema_akses IS '';


SET search_path TO public;
ALTER TABLE schema_akses ADD CONSTRAINT "schema_akses_FK_1" FOREIGN KEY ("user_id") REFERENCES master_user ("user_id") ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE schema_akses ADD CONSTRAINT "schema_akses_FK_2" FOREIGN KEY ("schema_id") REFERENCES master_schema ("schema_id") ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE schema_akses ADD CONSTRAINT "schema_akses_FK_3" FOREIGN KEY ("level_id") REFERENCES user_level ("level_id") ON UPDATE CASCADE ON DELETE CASCADE;

-----------------------------------------------------------------------------
-- schema_akses_v2
-----------------------------------------------------------------------------

DROP TABLE schema_akses_v2 CASCADE;


CREATE TABLE schema_akses_v2
(
	"user_id" VARCHAR(50)  NOT NULL,
	"schema_id" INTEGER  NOT NULL,
	"level_id" INTEGER,
	PRIMARY KEY ("user_id","schema_id")
);

COMMENT ON TABLE schema_akses_v2 IS '';


SET search_path TO public;
ALTER TABLE schema_akses_v2 ADD CONSTRAINT "schema_akses_v2_FK_1" FOREIGN KEY ("user_id") REFERENCES master_user_v2 ("user_id") ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE schema_akses_v2 ADD CONSTRAINT "schema_akses_v2_FK_2" FOREIGN KEY ("schema_id") REFERENCES master_schema ("schema_id") ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE schema_akses_v2 ADD CONSTRAINT "schema_akses_v2_FK_3" FOREIGN KEY ("level_id") REFERENCES user_level ("level_id") ON UPDATE CASCADE ON DELETE CASCADE;

-----------------------------------------------------------------------------
-- temp_lokasi
-----------------------------------------------------------------------------

DROP TABLE temp_lokasi CASCADE;


CREATE TABLE temp_lokasi
(
	"kode" VARCHAR(10)  NOT NULL,
	"nama" VARCHAR(500),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE temp_lokasi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- unit_kerja
-----------------------------------------------------------------------------

DROP TABLE unit_kerja CASCADE;


CREATE TABLE unit_kerja
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kelompok_id" NUMERIC(2),
	"unit_name" VARCHAR(500),
	"unit_address" VARCHAR(100),
	"kepala_nama" VARCHAR(50),
	"kepala_pangkat" VARCHAR(50),
	"kepala_nip" VARCHAR(50),
	"kode_permen" VARCHAR(10),
	"pagu" DOUBLE PRECISION,
	"jumlah_pns" DOUBLE PRECISION default 0,
	"jumlah_honda" DOUBLE PRECISION,
	"jumlah_nonpns" DOUBLE PRECISION,
	PRIMARY KEY ("unit_id")
);

COMMENT ON TABLE unit_kerja IS '';


SET search_path TO public;
ALTER TABLE unit_kerja ADD CONSTRAINT "unit_kerja_FK_1" FOREIGN KEY ("kelompok_id") REFERENCES kelompok_dinas ("kelompok_id") ON UPDATE CASCADE ON DELETE SET NULL;

-----------------------------------------------------------------------------
-- unit_kerja2
-----------------------------------------------------------------------------

DROP TABLE unit_kerja2 CASCADE;

DROP SEQUENCE unit_kerja2_seq;

CREATE SEQUENCE unit_kerja2_seq;


CREATE TABLE unit_kerja2
(
	"unit_name" VARCHAR(250),
	"id" INTEGER  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE unit_kerja2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- user_handle
-----------------------------------------------------------------------------

DROP TABLE user_handle CASCADE;


CREATE TABLE user_handle
(
	"user_id" VARCHAR(20)  NOT NULL,
	"schema_id" INTEGER  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL
);

COMMENT ON TABLE user_handle IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- user_handle_v2
-----------------------------------------------------------------------------

DROP TABLE user_handle_v2 CASCADE;


CREATE TABLE user_handle_v2
(
	"user_id" VARCHAR(20)  NOT NULL,
	"schema_id" INTEGER  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL
);

COMMENT ON TABLE user_handle_v2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- user_handle_asli
-----------------------------------------------------------------------------

DROP TABLE user_handle_asli CASCADE;


CREATE TABLE user_handle_asli
(
	"user_id" VARCHAR(20)  NOT NULL,
	"schema_id" INTEGER  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	PRIMARY KEY ("user_id","schema_id","unit_id")
);

COMMENT ON TABLE user_handle_asli IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- user_level
-----------------------------------------------------------------------------

DROP TABLE user_level CASCADE;


CREATE TABLE user_level
(
	"level_id" INTEGER  NOT NULL,
	"level_name" VARCHAR(100),
	PRIMARY KEY ("level_id")
);

COMMENT ON TABLE user_level IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- usulan
-----------------------------------------------------------------------------

DROP TABLE usulan CASCADE;


CREATE TABLE usulan
(
	"dari" VARCHAR(100),
	"pekerjaan" VARCHAR(800),
	"kecamatan" VARCHAR(500),
	"kelurahan" VARCHAR(800),
	"nama" VARCHAR(100),
	"tipe" VARCHAR(30),
	"lokasi" VARCHAR(300),
	"volume" VARCHAR(300),
	"keterangan" VARCHAR(100),
	"dana" DOUBLE PRECISION,
	"skpd" VARCHAR(300),
	"tahap" INT2
);

COMMENT ON TABLE usulan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- usulan3
-----------------------------------------------------------------------------

DROP TABLE usulan3 CASCADE;


CREATE TABLE usulan3
(
	"dari" VARCHAR(100),
	"pekerjaan" VARCHAR(800),
	"kecamatan" VARCHAR(500),
	"kelurahan" VARCHAR(800),
	"nama" VARCHAR(100),
	"tipe" VARCHAR(30),
	"lokasi" VARCHAR(300),
	"volume" VARCHAR(300),
	"keterangan" VARCHAR(100),
	"dana" DOUBLE PRECISION,
	"skpd" VARCHAR(300),
	"tahap" INT2
);

COMMENT ON TABLE usulan3 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- usulan_dihapus
-----------------------------------------------------------------------------

DROP TABLE usulan_dihapus CASCADE;


CREATE TABLE usulan_dihapus
(
	"dari" VARCHAR(100),
	"pekerjaan" VARCHAR(800),
	"kecamatan" VARCHAR(500),
	"kelurahan" VARCHAR(800),
	"nama" VARCHAR(100),
	"tipe" VARCHAR(30),
	"lokasi" VARCHAR(300),
	"volume" VARCHAR(300),
	"keterangan" VARCHAR(100),
	"dana" DOUBLE PRECISION,
	"skpd" VARCHAR(300),
	"tahap" INT2,
	"alasan" VARCHAR(400)
);

COMMENT ON TABLE usulan_dihapus IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- pegawai
-----------------------------------------------------------------------------

DROP TABLE pegawai CASCADE;


CREATE TABLE pegawai
(
	"nip" VARCHAR(25),
	"nip_lama" VARCHAR(25),
	"nama" VARCHAR(70),
	"tempat_lahir" VARCHAR(30),
	"tanggal_lahir" VARCHAR(15),
	"nama_pangkat" VARCHAR(25),
	"nama_golongan" VARCHAR(6),
	"tmt_pangkat" VARCHAR(15),
	"tmt_cpns" VARCHAR(15),
	"jenis_kelamin" VARCHAR(10),
	"nama_agama" VARCHAR(12),
	"nama_jabatan" VARCHAR(150),
	"eselon" VARCHAR(8),
	"tmt_jabatan" VARCHAR(15),
	"nama_jurusan_sk" VARCHAR(50),
	"lulus" VARCHAR(5),
	"jenis_pegawai" VARCHAR(14),
	"nama_instansi" VARCHAR(100),
	"nama_unit_kerja" VARCHAR(150),
	"nama_unit_kerja2" VARCHAR(150),
	"unit_id" VARCHAR(10)
);

COMMENT ON TABLE pegawai IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- kelurahan_kecamatan
-----------------------------------------------------------------------------

DROP TABLE kelurahan_kecamatan CASCADE;


CREATE TABLE kelurahan_kecamatan
(
	"oid" INTEGER,
	"nama_kecamatan" VARCHAR(50),
	"nama_kelurahan" VARCHAR(50),
	"id_kecamatan" INTEGER
);

COMMENT ON TABLE kelurahan_kecamatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.murni_bukuputih_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.murni_bukuputih_rka_member CASCADE;


CREATE TABLE ebudget.murni_bukuputih_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.murni_bukuputih_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.murni_bukuputih_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.murni_bukuputih_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.murni_bukuputih_ma_seq_1;

CREATE SEQUENCE ebudget.murni_bukuputih_ma_seq_1;


CREATE TABLE ebudget.murni_bukuputih_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	"user_id_lama" VARCHAR(100),
	"indikator" VARCHAR(300),
	"is_dak" BOOLEAN,
	"kode_kegiatan_asal" VARCHAR(20),
	"kode_keg_keuangan_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"kelompok_sasaran" VARCHAR(500),
	"pagu_bappeko" DOUBLE PRECISION,
	"kode_dpa" VARCHAR(500),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.murni_bukuputih_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.murni_bukuputih_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.murni_bukuputih_rincian CASCADE;


CREATE TABLE ebudget.murni_bukuputih_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.murni_bukuputih_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.murni_bukuputih_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.murni_bukuputih_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.murni_bukuputih_su_seq_2;

CREATE SEQUENCE ebudget.murni_bukuputih_su_seq_2;


CREATE TABLE ebudget.murni_bukuputih_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.murni_bukuputih_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.murni_bukuputih_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.murni_bukuputih_rincian_detail CASCADE;


CREATE TABLE ebudget.murni_bukuputih_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	"ob" BOOLEAN,
	"ob_from_id" INTEGER,
	"is_per_komponen" BOOLEAN,
	"kegiatan_code_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"harga_sebelum_sisa_lelang" DOUBLE PRECISION,
	"is_musrenbang" BOOLEAN default 'f',
	"sub_id_asal" INTEGER,
	"subtitle_asal" VARCHAR(300),
	"kode_sub_asal" VARCHAR(30),
	"sub_asal" VARCHAR(500),
	"last_edit_time" TIMESTAMP,
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	"status_ob" INT2 default 0,
	"ob_parent" TEXT,
	"ob_alokasi_baru" DOUBLE PRECISION,
	"is_hibah" BOOLEAN default 'f',
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.murni_bukuputih_rincian_detail IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.murni_bukuputih_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.murni_bukuputih_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.murni_bukuputih_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.murni_bukuputih_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.history_user
-----------------------------------------------------------------------------

DROP TABLE ebudget.history_user CASCADE;


CREATE TABLE ebudget.history_user
(
	"id" INTEGER  NOT NULL,
	"username" TEXT,
	"ip" TEXT,
	"time_act" TIMESTAMP,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"detail_no" TEXT,
	"description" TEXT,
	"aksi" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.history_user IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.konektor_musrenbang
-----------------------------------------------------------------------------

DROP TABLE ebudget.konektor_musrenbang CASCADE;


CREATE TABLE ebudget.konektor_musrenbang
(
	"id" INTEGER  NOT NULL,
	"kode_devplan" TEXT,
	"nama_devplan" TEXT,
	"satuan_devplan" TEXT,
	"komponen_id" TEXT,
	"komponen_rekening" TEXT,
	"harga_devplan" DOUBLE PRECISION,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.konektor_musrenbang IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rekening_dibuka_belanja_dikunci
-----------------------------------------------------------------------------

DROP TABLE ebudget.rekening_dibuka_belanja_dikunci CASCADE;


CREATE TABLE ebudget.rekening_dibuka_belanja_dikunci
(
	"id" INTEGER  NOT NULL,
	"belanja_dikunci_id" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"rekening_code" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.rekening_dibuka_belanja_dikunci IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belanja_dikunci
-----------------------------------------------------------------------------

DROP TABLE ebudget.belanja_dikunci CASCADE;


CREATE TABLE ebudget.belanja_dikunci
(
	"id" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"kode_belanja" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.belanja_dikunci IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_dibuka
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_dibuka CASCADE;


CREATE TABLE ebudget.komponen_dibuka
(
	"id" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"komponen_id" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.komponen_dibuka IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_dikunci
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_dikunci CASCADE;


CREATE TABLE ebudget.komponen_dikunci
(
	"id" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"komponen_id" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.komponen_dikunci IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rekening_dikunci
-----------------------------------------------------------------------------

DROP TABLE ebudget.rekening_dikunci CASCADE;


CREATE TABLE ebudget.rekening_dikunci
(
	"id" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"rekening_code" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.rekening_dikunci IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- gis_ebudget.geojsonlokasi_rev1
-----------------------------------------------------------------------------

DROP TABLE gis_ebudget.geojsonlokasi_rev1 CASCADE;


CREATE TABLE gis_ebudget.geojsonlokasi_rev1
(
	"idgeolokasi" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"detail_no" INTEGER,
	"satuan" TEXT,
	"volume" DOUBLE PRECISION,
	"nilai_anggaran" DOUBLE PRECISION,
	"tahun" VARCHAR,
	"mlokasi" TEXT,
	"id_kelompok" INTEGER,
	"geojson" TEXT,
	"keterangan" TEXT,
	"nmuser" TEXT,
	"level" INTEGER,
	"komponen_name" TEXT,
	"status_hapus" BOOLEAN,
	"keterangan_alamat" TEXT,
	"unit_name" TEXT,
	"last_edit_time" TIMESTAMP,
	"last_create_time" TIMESTAMP,
	"koordinat" TEXT,
	"lokasi_ke" INTEGER,
	PRIMARY KEY ("idgeolokasi")
);

COMMENT ON TABLE gis_ebudget.geojsonlokasi_rev1 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- gis_ebudget.geojsonlokasi_waitinglist
-----------------------------------------------------------------------------

DROP TABLE gis_ebudget.geojsonlokasi_waitinglist CASCADE;


CREATE TABLE gis_ebudget.geojsonlokasi_waitinglist
(
	"idgeolokasi" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"id_waiting" INTEGER,
	"satuan" TEXT,
	"volume" DOUBLE PRECISION,
	"nilai_anggaran" DOUBLE PRECISION,
	"tahun" VARCHAR,
	"mlokasi" TEXT,
	"id_kelompok" INTEGER,
	"geojson" TEXT,
	"keterangan" TEXT,
	"nmuser" TEXT,
	"level" INTEGER,
	"komponen_name" TEXT,
	"status_hapus" BOOLEAN,
	"keterangan_alamat" TEXT,
	"unit_name" TEXT,
	"last_edit_time" TIMESTAMP,
	"last_create_time" TIMESTAMP,
	"koordinat" TEXT,
	"lokasi_ke" INTEGER,
	PRIMARY KEY ("idgeolokasi")
);

COMMENT ON TABLE gis_ebudget.geojsonlokasi_waitinglist IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_kegiatan_bp
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_kegiatan_bp CASCADE;

DROP SEQUENCE ebudget.master_kegiatan_bp_seq;

CREATE SEQUENCE ebudget.master_kegiatan_bp_seq;


CREATE TABLE ebudget.master_kegiatan_bp
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	"user_id_lama" VARCHAR(100),
	"indikator" VARCHAR(300),
	"is_dak" BOOLEAN,
	"kode_kegiatan_asal" VARCHAR(20),
	"kode_keg_keuangan_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"kelompok_sasaran" VARCHAR(500),
	"pagu_bappeko" DOUBLE PRECISION,
	"kode_dpa" VARCHAR(500),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.master_kegiatan_bp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rincian_bp
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian_bp CASCADE;


CREATE TABLE ebudget.rincian_bp
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.rincian_bp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.subtitle_indikator_bp
-----------------------------------------------------------------------------

DROP TABLE ebudget.subtitle_indikator_bp CASCADE;

DROP SEQUENCE ebudget.subtitle_indikator_seq_3;

CREATE SEQUENCE ebudget.subtitle_indikator_seq_3;


CREATE TABLE ebudget.subtitle_indikator_bp
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.subtitle_indikator_bp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rincian_detail_bp
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian_detail_bp CASCADE;


CREATE TABLE ebudget.rincian_detail_bp
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	"ob" BOOLEAN,
	"ob_from_id" INTEGER,
	"is_per_komponen" BOOLEAN,
	"kegiatan_code_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"harga_sebelum_sisa_lelang" DOUBLE PRECISION,
	"is_musrenbang" BOOLEAN,
	"sub_id_asal" INTEGER,
	"subtitle_asal" VARCHAR(300),
	"kode_sub_asal" VARCHAR(30),
	"sub_asal" VARCHAR(500),
	"last_edit_time" TIMESTAMP,
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.rincian_detail_bp IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.rincian_detail_bp ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.rincian_sub_parameter_bp
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian_sub_parameter_bp CASCADE;


CREATE TABLE ebudget.rincian_sub_parameter_bp
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.rincian_sub_parameter_bp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.tarik_swakelola_bp
-----------------------------------------------------------------------------

DROP TABLE ebudget.tarik_swakelola_bp CASCADE;


CREATE TABLE ebudget.tarik_swakelola_bp
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	"ob" BOOLEAN,
	"ob_from_id" INTEGER,
	"is_per_komponen" BOOLEAN,
	"kegiatan_code_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"harga_sebelum_sisa_lelang" DOUBLE PRECISION,
	"is_musrenbang" BOOLEAN,
	"sub_id_asal" INTEGER,
	"subtitle_asal" VARCHAR(300),
	"kode_sub_asal" VARCHAR(30),
	"sub_asal" VARCHAR(500),
	"last_edit_time" TIMESTAMP,
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.tarik_swakelola_bp IS '';


SET search_path TO public;
CREATE INDEX "tswa_index" ON ebudget.tarik_swakelola_bp ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.musrenbang_rka
-----------------------------------------------------------------------------

DROP TABLE ebudget.musrenbang_rka CASCADE;


CREATE TABLE ebudget.musrenbang_rka
(
	"id" INTEGER  NOT NULL,
	"status_hapus" BOOLEAN,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"detail_no" INTEGER,
	"id_musrenbang" INTEGER,
	"tahun_musrenbang" TEXT,
	"koordinat_musrenbang" TEXT,
	"volume" DOUBLE PRECISION,
	"satuan" TEXT,
	"anggaran" DOUBLE PRECISION,
	"komponen_name" TEXT,
	"komponen_harga" DOUBLE PRECISION,
	"komponen_pajak" INTEGER,
	"detail_name" TEXT,
	"komponen_rekening" TEXT,
	"komponen_tipe" TEXT,
	"subtitle" TEXT,
	"komponen_id" TEXT,
	"created_by" TEXT,
	"updated_by" TEXT,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.musrenbang_rka IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.usulan_dinas
-----------------------------------------------------------------------------

DROP TABLE ebudget.usulan_dinas CASCADE;


CREATE TABLE ebudget.usulan_dinas
(
	"id_usulan" INTEGER  NOT NULL,
	"tgl_usulan" TIMESTAMP,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"penyelia" TEXT,
	"skpd" TEXT,
	"jenis_usulan" TEXT,
	"jumlah_dukungan" INTEGER,
	"jumlah_usulan" INTEGER,
	"status_verifikasi" BOOLEAN,
	"status_hapus" BOOLEAN,
	"filepath" TEXT,
	"filepath_rar" TEXT,
	"komentar_verifikator" TEXT,
	"nomor_surat" TEXT,
	"status_tolak" BOOLEAN,
	PRIMARY KEY ("id_usulan")
);

COMMENT ON TABLE ebudget.usulan_dinas IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.waitinglist_swakelola
-----------------------------------------------------------------------------

DROP TABLE ebudget.waitinglist_swakelola CASCADE;

DROP SEQUENCE ebudget.waitinglist_swakelola_id_swakelola_seq;

CREATE SEQUENCE ebudget.waitinglist_swakelola_id_swakelola_seq;


CREATE TABLE ebudget.waitinglist_swakelola
(
	"id_swakelola" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"subtitle" TEXT,
	"komponen_id" TEXT,
	"komponen_name" TEXT,
	"komponen_lokasi" TEXT,
	"komponen_harga_awal" DOUBLE PRECISION,
	"pajak" INTEGER,
	"komponen_satuan" TEXT,
	"komponen_rekening" TEXT,
	"koefisien" TEXT,
	"volume" DOUBLE PRECISION,
	"nilai_anggaran" DOUBLE PRECISION,
	"tahun_input" TEXT,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"status_hapus" BOOLEAN,
	"status_waiting" INTEGER,
	"prioritas" INTEGER,
	"kode_rka" TEXT,
	"user_pengambil" TEXT,
	"nilai_ee" DOUBLE PRECISION,
	"keterangan" TEXT,
	"kode_jasmas" TEXT,
	"kecamatan" TEXT,
	"kelurahan" TEXT,
	"is_musrenbang" BOOLEAN,
	"nilai_anggaran_semula" DOUBLE PRECISION,
	PRIMARY KEY ("id_swakelola")
);

COMMENT ON TABLE ebudget.waitinglist_swakelola IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.waitinglist_pu
-----------------------------------------------------------------------------

DROP TABLE ebudget.waitinglist_pu CASCADE;

DROP SEQUENCE ebudget.waitinglist_pu_id_waiting_seq;

CREATE SEQUENCE ebudget.waitinglist_pu_id_waiting_seq;


CREATE TABLE ebudget.waitinglist_pu
(
	"id_waiting" INTEGER  NOT NULL,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"subtitle" TEXT,
	"komponen_id" TEXT,
	"komponen_name" TEXT,
	"komponen_lokasi" TEXT,
	"komponen_harga_awal" DOUBLE PRECISION,
	"pajak" INTEGER,
	"komponen_satuan" TEXT,
	"komponen_rekening" TEXT,
	"koefisien" TEXT,
	"volume" DOUBLE PRECISION,
	"nilai_anggaran" DOUBLE PRECISION,
	"tahun_input" TEXT,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"status_hapus" BOOLEAN,
	"status_waiting" INTEGER,
	"prioritas" INTEGER,
	"kode_rka" TEXT,
	"user_pengambil" TEXT,
	"nilai_ee" DOUBLE PRECISION,
	"keterangan" TEXT,
	"kode_jasmas" TEXT,
	"kecamatan" TEXT,
	"kelurahan" TEXT,
	"is_musrenbang" BOOLEAN,
	"nilai_anggaran_semula" DOUBLE PRECISION,
	"metode_waitinglist" INTEGER,
	PRIMARY KEY ("id_waiting")
);

COMMENT ON TABLE ebudget.waitinglist_pu IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_penyelia
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_penyelia CASCADE;


CREATE TABLE ebudget.master_penyelia
(
	"id_penyelia" INTEGER  NOT NULL,
	"username" TEXT,
	"useremail" TEXT,
	"aktif" BOOLEAN,
	PRIMARY KEY ("id_penyelia")
);

COMMENT ON TABLE ebudget.master_penyelia IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.usulan_token
-----------------------------------------------------------------------------

DROP TABLE ebudget.usulan_token CASCADE;


CREATE TABLE ebudget.usulan_token
(
	"id_token" INTEGER  NOT NULL,
	"required_at" TIMESTAMP,
	"expired_at" TIMESTAMP,
	"used_at" TIMESTAMP,
	"penyelia" TEXT,
	"unit_id" TEXT,
	"created_by" TEXT,
	"jumlah_ssh" INTEGER,
	"token" TEXT,
	"status_used" BOOLEAN,
	"nomor_surat" TEXT,
	PRIMARY KEY ("id_token")
);

COMMENT ON TABLE ebudget.usulan_token IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.usulan_spjm
-----------------------------------------------------------------------------

DROP TABLE ebudget.usulan_spjm CASCADE;


CREATE TABLE ebudget.usulan_spjm
(
	"id_spjm" INTEGER  NOT NULL,
	"tgl_spjm" TIMESTAMP,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"penyelia" TEXT,
	"skpd" TEXT,
	"tipe_usulan" TEXT,
	"jenis_usulan" TEXT,
	"jumlah_dukungan" INTEGER,
	"status_verifikasi" BOOLEAN,
	"status_hapus" BOOLEAN,
	"filepath" TEXT,
	"filepath_pdf" TEXT,
	"id_token" INTEGER,
	"nomor_surat" TEXT,
	"komentar_verifikator" TEXT,
	"tahap" TEXT,
	PRIMARY KEY ("id_spjm")
);

COMMENT ON TABLE ebudget.usulan_spjm IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.usulan_ssh
-----------------------------------------------------------------------------

DROP TABLE ebudget.usulan_ssh CASCADE;


CREATE TABLE ebudget.usulan_ssh
(
	"id_usulan" INTEGER  NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"nama" TEXT,
	"spec" TEXT,
	"hidden_spec" TEXT,
	"merek" TEXT,
	"satuan" TEXT,
	"harga" DOUBLE PRECISION,
	"rekening" TEXT,
	"pajak" INTEGER,
	"keterangan" TEXT,
	"kode_barang" TEXT,
	"id_spjm" INTEGER,
	"status_verifikasi" BOOLEAN,
	"status_hapus" BOOLEAN,
	"tipe_usulan" TEXT,
	"unit_id" TEXT,
	"shsd_id" TEXT,
	"status_confirmasi_penyelia" BOOLEAN,
	"komentar_verifikator" TEXT,
	"nama_sebelum" TEXT,
	"harga_sebelum" DOUBLE PRECISION,
	"alasan_perubahan_harga" TEXT,
	"is_perubahan_harga" BOOLEAN,
	"is_perbedaan_pendukung" BOOLEAN,
	"alasan_perbedaan_dinas" TEXT,
	"alasan_perbedaan_penyelia" TEXT,
	"harga_pendukung1" DOUBLE PRECISION,
	"harga_pendukung2" DOUBLE PRECISION,
	"harga_pendukung3" DOUBLE PRECISION,
	"tahap" TEXT,
	"status_baru" BOOLEAN,
	"status_pending" BOOLEAN,
	PRIMARY KEY ("id_usulan")
);

COMMENT ON TABLE ebudget.usulan_ssh IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.usulan_verifikasi
-----------------------------------------------------------------------------

DROP TABLE ebudget.usulan_verifikasi CASCADE;


CREATE TABLE ebudget.usulan_verifikasi
(
	"id_verifikasi" INTEGER  NOT NULL,
	"shsd_id" TEXT,
	"shsd_name" TEXT,
	"harga" DOUBLE PRECISION,
	"nama" TEXT,
	"spec" TEXT,
	"hidden_spec" TEXT,
	"merek" TEXT,
	"satuan" TEXT,
	"rekening" TEXT,
	"pajak" INTEGER,
	"keterangan" TEXT,
	"id_spjm" INTEGER,
	"id_usulan" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"unit_id" TEXT,
	"tipe" TEXT,
	"tahap" TEXT,
	PRIMARY KEY ("id_verifikasi")
);

COMMENT ON TABLE ebudget.usulan_verifikasi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.buku_ssh
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_ssh CASCADE;


CREATE TABLE ebudget.buku_ssh
(
	"id" INTEGER  NOT NULL,
	"kode" TEXT,
	"nama" TEXT,
	"merk" TEXT,
	"spec" TEXT,
	"hidden_spec" TEXT,
	"satuan" TEXT,
	"perubahan" INTEGER,
	"tanggal" TIMESTAMP,
	"perubahan_revisi" VARCHAR,
	"status_hapus" BOOLEAN,
	"harga" DOUBLE PRECISION,
	"tahun" VARCHAR(10),
	"replaced_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.buku_ssh IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.buku_provinsi_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_provinsi_rka_member CASCADE;


CREATE TABLE ebudget.buku_provinsi_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.buku_provinsi_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.buku_provinsi_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_provinsi_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.buku_provinsi_mast_seq_4;

CREATE SEQUENCE ebudget.buku_provinsi_mast_seq_4;


CREATE TABLE ebudget.buku_provinsi_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	"user_id_lama" VARCHAR(100),
	"indikator" VARCHAR(300),
	"is_dak" BOOLEAN,
	"kode_kegiatan_asal" VARCHAR(20),
	"kode_keg_keuangan_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"kelompok_sasaran" VARCHAR(500),
	"pagu_bappeko" DOUBLE PRECISION,
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.buku_provinsi_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.buku_provinsi_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_provinsi_rincian CASCADE;


CREATE TABLE ebudget.buku_provinsi_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.buku_provinsi_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.buku_provinsi_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_provinsi_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.buku_provinsi_subt_seq_5;

CREATE SEQUENCE ebudget.buku_provinsi_subt_seq_5;


CREATE TABLE ebudget.buku_provinsi_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.buku_provinsi_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.buku_provinsi_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_provinsi_rincian_detail CASCADE;


CREATE TABLE ebudget.buku_provinsi_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	"ob" BOOLEAN,
	"ob_from_id" INTEGER,
	"is_per_komponen" BOOLEAN,
	"kegiatan_code_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"harga_sebelum_sisa_lelang" DOUBLE PRECISION,
	"is_musrenbang" BOOLEAN,
	"sub_id_asal" INTEGER,
	"subtitle_asal" VARCHAR(300),
	"kode_sub_asal" VARCHAR(30),
	"sub_asal" VARCHAR(500),
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.buku_provinsi_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.buku_provinsi_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.buku_provinsi_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.buku_provinsi_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.buku_provinsi_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.buku_provinsi_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.rka_member CASCADE;


CREATE TABLE ebudget.rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_kegiatan CASCADE;

DROP SEQUENCE ebudget.master_kegiatan_seq;

CREATE SEQUENCE ebudget.master_kegiatan_seq;


CREATE TABLE ebudget.master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	"user_id_lama" VARCHAR(100),
	"indikator" VARCHAR(300),
	"is_dak" BOOLEAN,
	"kode_kegiatan_asal" VARCHAR(20),
	"kode_keg_keuangan_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"kelompok_sasaran" VARCHAR(500),
	"pagu_bappeko" DOUBLE PRECISION,
	"kode_dpa" VARCHAR(500),
	"user_id_pptk" VARCHAR(100),
	"user_id_kpa" VARCHAR(100),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian CASCADE;


CREATE TABLE ebudget.rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.subtitle_indikator_seq;

CREATE SEQUENCE ebudget.subtitle_indikator_seq;


CREATE TABLE ebudget.subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	"is_nol_anggaran" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian_detail CASCADE;


CREATE TABLE ebudget.rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	"ob" BOOLEAN,
	"ob_from_id" INTEGER,
	"is_per_komponen" BOOLEAN,
	"kegiatan_code_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"harga_sebelum_sisa_lelang" DOUBLE PRECISION,
	"is_musrenbang" BOOLEAN default 'f',
	"sub_id_asal" INTEGER,
	"subtitle_asal" VARCHAR(300),
	"kode_sub_asal" VARCHAR(30),
	"sub_asal" VARCHAR(500),
	"last_edit_time" TIMESTAMP,
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	"status_ob" INT2 default 0,
	"ob_parent" TEXT,
	"ob_alokasi_baru" DOUBLE PRECISION,
	"is_hibah" BOOLEAN default 'f',
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.dinas_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.dinas_rincian_detail CASCADE;


CREATE TABLE ebudget.dinas_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	"ob" BOOLEAN,
	"ob_from_id" INTEGER,
	"is_per_komponen" BOOLEAN,
	"kegiatan_code_asal" VARCHAR(20),
	"th_ke_multiyears" INTEGER,
	"harga_sebelum_sisa_lelang" DOUBLE PRECISION,
	"is_musrenbang" BOOLEAN default 'f',
	"sub_id_asal" INTEGER,
	"subtitle_asal" VARCHAR(300),
	"kode_sub_asal" VARCHAR(30),
	"sub_asal" VARCHAR(500),
	"last_edit_time" TIMESTAMP,
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	"status_ob" INT2 default 0,
	"ob_parent" TEXT,
	"ob_alokasi_baru" DOUBLE PRECISION,
	"is_hibah" BOOLEAN default 'f',
	"status_level" INT2 default 0,
	"status_pernah_tolak" BOOLEAN default 'f',
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.dinas_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.dinas_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belur_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.belur_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.belur_master_kegia_seq_6;

CREATE SEQUENCE ebudget.belur_master_kegia_seq_6;


CREATE TABLE ebudget.belur_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.belur_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belur_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.belur_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.belur_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.belur_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belur_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.belur_rincian CASCADE;


CREATE TABLE ebudget.belur_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.belur_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belur_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.belur_rka_member CASCADE;


CREATE TABLE ebudget.belur_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.belur_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belur_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.belur_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.belur_subtitle_ind_seq_7;

CREATE SEQUENCE ebudget.belur_subtitle_ind_seq_7;


CREATE TABLE ebudget.belur_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.belur_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.belur_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.belur_rincian_detail CASCADE;


CREATE TABLE ebudget.belur_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.belur_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.belur_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.november3_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.november3_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.november3_master_k_seq_8;

CREATE SEQUENCE ebudget.november3_master_k_seq_8;


CREATE TABLE ebudget.november3_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.november3_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.november3_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.november3_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.november3_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.november3_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.november3_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.november3_rincian CASCADE;


CREATE TABLE ebudget.november3_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.november3_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.november3_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.november3_rka_member CASCADE;


CREATE TABLE ebudget.november3_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.november3_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.november3_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.november3_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.november3_subtitle_seq_9;

CREATE SEQUENCE ebudget.november3_subtitle_seq_9;


CREATE TABLE ebudget.november3_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.november3_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.november3_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.november3_rincian_detail CASCADE;


CREATE TABLE ebudget.november3_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	"nilai_anggaran" DOUBLE PRECISION,
	"is_blud" BOOLEAN,
	"lokasi_kecamatan" VARCHAR(500),
	"lokasi_kelurahan" VARCHAR(500),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.november3_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.november3_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.bukuputih_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.bukuputih_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.bukuputih_master__seq_10;

CREATE SEQUENCE ebudget.bukuputih_master__seq_10;


CREATE TABLE ebudget.bukuputih_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.bukuputih_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bukuputih_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.bukuputih_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.bukuputih_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.bukuputih_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bukuputih_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.bukuputih_rincian CASCADE;


CREATE TABLE ebudget.bukuputih_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.bukuputih_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bukuputih_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.bukuputih_rka_member CASCADE;


CREATE TABLE ebudget.bukuputih_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.bukuputih_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bukuputih_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.bukuputih_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.bukuputih_subtitl_seq_11;

CREATE SEQUENCE ebudget.bukuputih_subtitl_seq_11;


CREATE TABLE ebudget.bukuputih_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.bukuputih_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bukuputih_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.bukuputih_rincian_detail CASCADE;


CREATE TABLE ebudget.bukuputih_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.bukuputih_rincian_detail IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi5_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi5_rincian_detail CASCADE;


CREATE TABLE ebudget.revisi5_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.revisi5_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.revisi5_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.revisi4_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi4_rincian_detail CASCADE;


CREATE TABLE ebudget.revisi4_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.revisi4_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.revisi4_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.revisi3_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi3_rincian_detail CASCADE;


CREATE TABLE ebudget.revisi3_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.revisi3_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.revisi3_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.revisi2_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi2_rincian_detail CASCADE;


CREATE TABLE ebudget.revisi2_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.revisi2_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.revisi2_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.revisi1_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi1_rincian_detail CASCADE;


CREATE TABLE ebudget.revisi1_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.revisi1_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.revisi1_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.revisi5_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi5_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.revisi5_subtitle__seq_12;

CREATE SEQUENCE ebudget.revisi5_subtitle__seq_12;


CREATE TABLE ebudget.revisi5_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.revisi5_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi4_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi4_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.revisi4_subtitle__seq_13;

CREATE SEQUENCE ebudget.revisi4_subtitle__seq_13;


CREATE TABLE ebudget.revisi4_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.revisi4_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi3_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi3_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.revisi3_subtitle__seq_14;

CREATE SEQUENCE ebudget.revisi3_subtitle__seq_14;


CREATE TABLE ebudget.revisi3_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.revisi3_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi2_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi2_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.revisi2_subtitle__seq_15;

CREATE SEQUENCE ebudget.revisi2_subtitle__seq_15;


CREATE TABLE ebudget.revisi2_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.revisi2_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi1_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi1_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.revisi1_subtitle__seq_16;

CREATE SEQUENCE ebudget.revisi1_subtitle__seq_16;


CREATE TABLE ebudget.revisi1_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.revisi1_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi5_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi5_rka_member CASCADE;


CREATE TABLE ebudget.revisi5_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.revisi5_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi4_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi4_rka_member CASCADE;


CREATE TABLE ebudget.revisi4_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.revisi4_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi3_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi3_rka_member CASCADE;


CREATE TABLE ebudget.revisi3_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.revisi3_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi2_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi2_rka_member CASCADE;


CREATE TABLE ebudget.revisi2_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.revisi2_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi1_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi1_rka_member CASCADE;


CREATE TABLE ebudget.revisi1_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.revisi1_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi5_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi5_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.revisi5_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.revisi5_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi4_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi4_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.revisi4_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.revisi4_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi3_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi3_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.revisi3_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.revisi3_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi2_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi2_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.revisi2_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.revisi2_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi1_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi1_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.revisi1_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.revisi1_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi5_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi5_rincian CASCADE;


CREATE TABLE ebudget.revisi5_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.revisi5_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi4_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi4_rincian CASCADE;


CREATE TABLE ebudget.revisi4_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.revisi4_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi3_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi3_rincian CASCADE;


CREATE TABLE ebudget.revisi3_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.revisi3_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi2_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi2_rincian CASCADE;


CREATE TABLE ebudget.revisi2_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.revisi2_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi1_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi1_rincian CASCADE;


CREATE TABLE ebudget.revisi1_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.revisi1_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi5_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi5_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.revisi5_master_ke_seq_17;

CREATE SEQUENCE ebudget.revisi5_master_ke_seq_17;


CREATE TABLE ebudget.revisi5_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.revisi5_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi4_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi4_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.revisi4_master_ke_seq_18;

CREATE SEQUENCE ebudget.revisi4_master_ke_seq_18;


CREATE TABLE ebudget.revisi4_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.revisi4_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi3_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi3_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.revisi3_master_ke_seq_19;

CREATE SEQUENCE ebudget.revisi3_master_ke_seq_19;


CREATE TABLE ebudget.revisi3_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.revisi3_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi2_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi2_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.revisi2_master_ke_seq_20;

CREATE SEQUENCE ebudget.revisi2_master_ke_seq_20;


CREATE TABLE ebudget.revisi2_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.revisi2_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.revisi1_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.revisi1_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.revisi1_master_ke_seq_21;

CREATE SEQUENCE ebudget.revisi1_master_ke_seq_21;


CREATE TABLE ebudget.revisi1_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.revisi1_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kib
-----------------------------------------------------------------------------

DROP TABLE ebudget.kib CASCADE;


CREATE TABLE ebudget.kib
(
	"id" VARCHAR(20)  NOT NULL,
	"tipe_kib" VARCHAR(50),
	"unit_id" VARCHAR(5),
	"kode_lokasi" VARCHAR(50),
	"nama_lokasi" VARCHAR(500),
	"no_register" VARCHAR(50),
	"kode_barang" VARCHAR(50),
	"nama_barang" VARCHAR(500),
	"kondisi" VARCHAR(10),
	"merk" VARCHAR(200),
	"tipe" VARCHAR(200),
	"alamat" VARCHAR(500),
	"keterangan" VARCHAR(200),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.kib IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.berita_acara_revisi
-----------------------------------------------------------------------------

DROP TABLE ebudget.berita_acara_revisi CASCADE;


CREATE TABLE ebudget.berita_acara_revisi
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"nomor" VARCHAR(100),
	"kesimpulan" VARCHAR(800),
	"dasar1" VARCHAR(500),
	"dasar2" VARCHAR(500),
	"dasar3" VARCHAR(500),
	"dasar4" VARCHAR(500),
	"dasar5" VARCHAR(500),
	"dasar6" VARCHAR(500),
	"dasar7" VARCHAR(500),
	"dasar8" VARCHAR(500),
	"dasar9" VARCHAR(500),
	"dasar10" VARCHAR(500),
	"pertimbangan1" VARCHAR(500),
	"pertimbangan2" VARCHAR(500),
	"pertimbangan3" VARCHAR(500),
	"pertimbangan4" VARCHAR(500),
	"pertimbangan5" VARCHAR(500),
	"pertimbangan6" VARCHAR(500),
	"pertimbangan7" VARCHAR(500),
	"pertimbangan8" VARCHAR(500),
	"pertimbangan9" VARCHAR(500),
	"pertimbangan10" VARCHAR(500)
);

COMMENT ON TABLE ebudget.berita_acara_revisi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.dpa_import2
-----------------------------------------------------------------------------

DROP TABLE ebudget.dpa_import2 CASCADE;


CREATE TABLE ebudget.dpa_import2
(
	"unit_id" VARCHAR(8)  NOT NULL,
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"detail_no" INTEGER  NOT NULL,
	"nama_kegiatan" VARCHAR(300),
	"subtitle" VARCHAR(300),
	"rekening_code" VARCHAR(100),
	"rekening_name" VARCHAR(300),
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(500),
	"detail_name" VARCHAR(350),
	"komponen_harga_awal" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"volume" DOUBLE PRECISION,
	"total" DOUBLE PRECISION,
	"dt_import" TIMESTAMP,
	"satuan" VARCHAR(300),
	PRIMARY KEY ("unit_id","kegiatan_code","detail_no")
);

COMMENT ON TABLE ebudget.dpa_import2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.dpa_import_lama
-----------------------------------------------------------------------------

DROP TABLE ebudget.dpa_import_lama CASCADE;


CREATE TABLE ebudget.dpa_import_lama
(
	"unit_id" VARCHAR(8)  NOT NULL,
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"detail_no" INTEGER  NOT NULL,
	"nama_kegiatan" VARCHAR(300),
	"subtitle" VARCHAR(300),
	"rekening_code" VARCHAR(100),
	"rekening_name" VARCHAR(300),
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(500),
	"detail_name" VARCHAR(350),
	"komponen_harga_awal" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"volume" DOUBLE PRECISION,
	"total" DOUBLE PRECISION,
	"dt_import" TIMESTAMP,
	"satuan" VARCHAR(300),
	PRIMARY KEY ("unit_id","kegiatan_code","detail_no")
);

COMMENT ON TABLE ebudget.dpa_import_lama IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.dpa_importbeda
-----------------------------------------------------------------------------

DROP TABLE ebudget.dpa_importbeda CASCADE;


CREATE TABLE ebudget.dpa_importbeda
(
	"unit_id" VARCHAR(8)  NOT NULL,
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"detail_no" INTEGER  NOT NULL,
	"nama_kegiatan" VARCHAR(300),
	"subtitle" VARCHAR(300),
	"rekening_code" VARCHAR(100),
	"rekening_name" VARCHAR(300),
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(500),
	"detail_name" VARCHAR(350),
	"komponen_harga_awal" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"volume" DOUBLE PRECISION,
	"total" DOUBLE PRECISION,
	"dt_import" TIMESTAMP,
	"satuan" VARCHAR(300),
	PRIMARY KEY ("unit_id","kegiatan_code","detail_no")
);

COMMENT ON TABLE ebudget.dpa_importbeda IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_est
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_est CASCADE;


CREATE TABLE ebudget.kategori_est
(
	"kategori_est_id" VARCHAR(50)  NOT NULL,
	"kategori_est_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	PRIMARY KEY ("kategori_est_id")
);

COMMENT ON TABLE ebudget.kategori_est IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_hspk
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_hspk CASCADE;


CREATE TABLE ebudget.kategori_hspk
(
	"kategori_hspk_id" VARCHAR(50)  NOT NULL,
	"kategori_hspk_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	PRIMARY KEY ("kategori_hspk_id")
);

COMMENT ON TABLE ebudget.kategori_hspk IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_komponen
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_komponen CASCADE;


CREATE TABLE ebudget.kategori_komponen
(
	"kategori_komponen_id" VARCHAR(50)  NOT NULL,
	"kategori_komponen_name" VARCHAR,
	"komponen_tipe" VARCHAR(10),
	PRIMARY KEY ("kategori_komponen_id")
);

COMMENT ON TABLE ebudget.kategori_komponen IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_sab
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_sab CASCADE;


CREATE TABLE ebudget.kategori_sab
(
	"kategori_sab_id" VARCHAR(50)  NOT NULL,
	"kategori_sab_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	PRIMARY KEY ("kategori_sab_id")
);

COMMENT ON TABLE ebudget.kategori_sab IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_shsd
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_shsd CASCADE;


CREATE TABLE ebudget.kategori_shsd
(
	"kategori_shsd_id" VARCHAR(50)  NOT NULL,
	"kategori_shsd_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	"rekening" VARCHAR
);

COMMENT ON TABLE ebudget.kategori_shsd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_shsd_asli
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_shsd_asli CASCADE;


CREATE TABLE ebudget.kategori_shsd_asli
(
	"kategori_shsd_id" VARCHAR(50)  NOT NULL,
	"kategori_shsd_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	"rekening" VARCHAR
);

COMMENT ON TABLE ebudget.kategori_shsd_asli IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kategori_shsd_temp
-----------------------------------------------------------------------------

DROP TABLE ebudget.kategori_shsd_temp CASCADE;


CREATE TABLE ebudget.kategori_shsd_temp
(
	"kategori_shsd_id" VARCHAR(50)  NOT NULL,
	"kategori_shsd_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	"rekening" VARCHAR
);

COMMENT ON TABLE ebudget.kategori_shsd_temp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kegiatan_file
-----------------------------------------------------------------------------

DROP TABLE ebudget.kegiatan_file CASCADE;


CREATE TABLE ebudget.kegiatan_file
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"urut" INTEGER  NOT NULL,
	"nama_file" VARCHAR,
	"tipe_file" VARCHAR,
	"isi_file" BYTEA,
	PRIMARY KEY ("unit_id","kegiatan_code","urut")
);

COMMENT ON TABLE ebudget.kegiatan_file IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kegiatan_nilai
-----------------------------------------------------------------------------

DROP TABLE ebudget.kegiatan_nilai CASCADE;


CREATE TABLE ebudget.kegiatan_nilai
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"nilai_1" INT2,
	"nilai_2" INT2,
	"nilai_3" INT2,
	"total" INTEGER,
	"keterangan" VARCHAR(300),
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"prioritas" VARCHAR(300),
	PRIMARY KEY ("unit_id","kegiatan_code")
);

COMMENT ON TABLE ebudget.kegiatan_nilai IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kelompok_belanja
-----------------------------------------------------------------------------

DROP TABLE ebudget.kelompok_belanja CASCADE;


CREATE TABLE ebudget.kelompok_belanja
(
	"belanja_id" INT2,
	"belanja_name" VARCHAR(100),
	"belanja_urutan" INT2,
	"belanja_code" VARCHAR(20)
);

COMMENT ON TABLE ebudget.kelompok_belanja IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.kelompok_rekening
-----------------------------------------------------------------------------

DROP TABLE ebudget.kelompok_rekening CASCADE;


CREATE TABLE ebudget.kelompok_rekening
(
	"rekening_code" VARCHAR(20)  NOT NULL,
	"rekening_name" VARCHAR(300),
	PRIMARY KEY ("rekening_code")
);

COMMENT ON TABLE ebudget.kelompok_rekening IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen CASCADE;


CREATE TABLE ebudget.komponen
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	"status_masuk" VARCHAR(10),
	"rka_member" BOOLEAN,
	"maintenance" BOOLEAN,
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	"usulan_skpd" VARCHAR(100),
	"tahap" TEXT,
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.komponen IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.asb_fisik
-----------------------------------------------------------------------------

DROP TABLE ebudget.asb_fisik CASCADE;


CREATE TABLE ebudget.asb_fisik
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	"asb_locked" BOOLEAN default 'f',
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.asb_fisik IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen2
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen2 CASCADE;


CREATE TABLE ebudget.komponen2
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.komponen2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_asb
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_asb CASCADE;


CREATE TABLE ebudget.komponen_asb
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.komponen_asb IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_file
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_file CASCADE;


CREATE TABLE ebudget.komponen_file
(
	"komponen_id" VARCHAR(20),
	"file_name" VARCHAR(100)
);

COMMENT ON TABLE ebudget.komponen_file IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_fisik
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_fisik CASCADE;


CREATE TABLE ebudget.komponen_fisik
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" VARCHAR,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR
);

COMMENT ON TABLE ebudget.komponen_fisik IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_gabung
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_gabung CASCADE;


CREATE TABLE ebudget.komponen_gabung
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" VARCHAR(150),
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"for_detail_no" INTEGER,
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.komponen_gabung IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_member CASCADE;


CREATE TABLE ebudget.komponen_member
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"member_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"member_name" VARCHAR,
	"member_harga" DOUBLE PRECISION,
	"koefisien" DOUBLE PRECISION,
	"member_total" DOUBLE PRECISION,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"member_tipe" VARCHAR(10),
	"subtitle" VARCHAR(250),
	"member_no" DOUBLE PRECISION,
	"tipe" VARCHAR(40),
	"komponen_tipe" VARCHAR(10),
	"from_id" VARCHAR(30),
	"from_koef" DOUBLE PRECISION,
	"hspk_name" VARCHAR(300)
);

COMMENT ON TABLE ebudget.komponen_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_member2
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_member2 CASCADE;


CREATE TABLE ebudget.komponen_member2
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"member_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"member_name" VARCHAR,
	"member_harga" DOUBLE PRECISION,
	"koefisien" DOUBLE PRECISION,
	"member_total" DOUBLE PRECISION,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"member_tipe" VARCHAR(10),
	"subtitle" VARCHAR(250),
	"member_no" INT8,
	"tipe" VARCHAR(40),
	"komponen_tipe" VARCHAR(10),
	"from_id" VARCHAR(30),
	"from_koef" DOUBLE PRECISION,
	"hspk_name" VARCHAR(300)
);

COMMENT ON TABLE ebudget.komponen_member2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_member_asb
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_member_asb CASCADE;


CREATE TABLE ebudget.komponen_member_asb
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"member_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"member_name" VARCHAR,
	"member_harga" DOUBLE PRECISION,
	"koefisien" DOUBLE PRECISION,
	"member_total" DOUBLE PRECISION,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"member_tipe" VARCHAR(10),
	"subtitle" VARCHAR(250),
	"member_no" INT8,
	"tipe" VARCHAR(40),
	"komponen_tipe" VARCHAR(10),
	"from_id" VARCHAR(30),
	"from_koef" DOUBLE PRECISION,
	"hspk_name" VARCHAR(300)
);

COMMENT ON TABLE ebudget.komponen_member_asb IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_member_temp
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_member_temp CASCADE;


CREATE TABLE ebudget.komponen_member_temp
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"member_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"member_name" VARCHAR,
	"member_harga" DOUBLE PRECISION,
	"koefisien" DOUBLE PRECISION,
	"member_total" DOUBLE PRECISION,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"member_tipe" VARCHAR(10),
	"subtitle" VARCHAR(250),
	"member_no" INT8,
	"tipe" VARCHAR(40),
	"komponen_tipe" VARCHAR(10),
	"from_id" VARCHAR(30),
	"from_koef" DOUBLE PRECISION,
	"hspk_name" VARCHAR(300)
);

COMMENT ON TABLE ebudget.komponen_member_temp IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.komponen_rekening
-----------------------------------------------------------------------------

DROP TABLE ebudget.komponen_rekening CASCADE;


CREATE TABLE ebudget.komponen_rekening
(
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"rekening_code29" VARCHAR(15)
);

COMMENT ON TABLE ebudget.komponen_rekening IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.log
-----------------------------------------------------------------------------

DROP TABLE ebudget.log CASCADE;


CREATE TABLE ebudget.log
(
	"log_id" INTEGER  NOT NULL,
	"waktu_access" TIMESTAMP,
	"ip_address" VARCHAR(20),
	"login" VARCHAR(20),
	"tabel_asal" VARCHAR(50),
	"isi1" VARCHAR(100),
	"isi2" VARCHAR(100),
	"isi3" VARCHAR(100),
	"isi4" VARCHAR(100),
	"isi5" VARCHAR(100),
	"isi6" VARCHAR(100),
	"isi7" VARCHAR(100),
	"isi8" VARCHAR(100),
	"isi9" VARCHAR(100),
	"isi10" VARCHAR(100),
	"isi11" VARCHAR(100),
	"isi12" VARCHAR(100),
	"isi13" VARCHAR(100),
	"isi14" VARCHAR(100),
	"isi15" VARCHAR(100),
	"isi16" VARCHAR(100),
	"isi17" VARCHAR(100),
	"isi18" VARCHAR(100),
	"isi19" VARCHAR(100),
	"isi20" VARCHAR(100),
	"isi21" VARCHAR(100),
	"isi22" VARCHAR(100),
	"isi23" VARCHAR(100),
	"isi24" VARCHAR(100),
	"isi25" VARCHAR(100),
	"isi26" VARCHAR(100),
	"isi27" VARCHAR(100),
	"isi28" VARCHAR(100),
	"isi29" VARCHAR(100),
	"isi30" VARCHAR(100),
	"isi31" VARCHAR(100),
	"isi32" VARCHAR(100),
	"isi33" VARCHAR(100),
	"isi34" VARCHAR(100),
	"isi35" VARCHAR(100),
	"isi36" VARCHAR(100),
	"isi37" VARCHAR(100),
	"isi38" VARCHAR(100),
	"isi39" VARCHAR(100),
	"isi40" VARCHAR(100),
	PRIMARY KEY ("log_id")
);

COMMENT ON TABLE ebudget.log IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_bidang
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_bidang CASCADE;


CREATE TABLE ebudget.master_bidang
(
	"kode_bidang" VARCHAR(10)  NOT NULL,
	"nama_bidang" VARCHAR(300),
	PRIMARY KEY ("kode_bidang")
);

COMMENT ON TABLE ebudget.master_bidang IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_indikator CASCADE;


CREATE TABLE ebudget.master_indikator
(
	"kode_program" CHAR(2)  NOT NULL,
	"kode_indikator" CHAR(2)  NOT NULL,
	"nama_indikator" VARCHAR(200),
	"kode_indikator2" VARCHAR(10),
	PRIMARY KEY ("kode_program","kode_indikator")
);

COMMENT ON TABLE ebudget.master_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_indikator2
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_indikator2 CASCADE;

DROP SEQUENCE ebudget.master_indikator2_seq;

CREATE SEQUENCE ebudget.master_indikator2_seq;


CREATE TABLE ebudget.master_indikator2
(
	"kode_program2" VARCHAR(25)  NOT NULL,
	"pk_indikator" INTEGER  NOT NULL,
	"indikator" VARCHAR(200),
	PRIMARY KEY ("kode_program2","pk_indikator")
);

COMMENT ON TABLE ebudget.master_indikator2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_kegiatan2
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_kegiatan2 CASCADE;

DROP SEQUENCE ebudget.master_kegiatan2_seq;

CREATE SEQUENCE ebudget.master_kegiatan2_seq;


CREATE TABLE ebudget.master_kegiatan2
(
	"kode_program2" VARCHAR(25)  NOT NULL,
	"kode_kegiatan2" VARCHAR(25)  NOT NULL,
	"nama_kegiatan2" VARCHAR(300),
	"urut" INTEGER  NOT NULL,
	PRIMARY KEY ("kode_program2","kode_kegiatan2")
);

COMMENT ON TABLE ebudget.master_kegiatan2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_misi
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_misi CASCADE;


CREATE TABLE ebudget.master_misi
(
	"kode_misi" VARCHAR(2)  NOT NULL,
	"nama_misi" VARCHAR(400),
	PRIMARY KEY ("kode_misi")
);

COMMENT ON TABLE ebudget.master_misi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_program
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_program CASCADE;


CREATE TABLE ebudget.master_program
(
	"kode_program" CHAR(2)  NOT NULL,
	"nama_program" VARCHAR(200),
	"bappeko" INT2,
	"kode_tujuan" VARCHAR(10),
	"kode_lutfi" VARCHAR(2),
	"indikator" VARCHAR,
	PRIMARY KEY ("kode_program")
);

COMMENT ON TABLE ebudget.master_program IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_program2
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_program2 CASCADE;


CREATE TABLE ebudget.master_program2
(
	"kode_program" CHAR(2)  NOT NULL,
	"kode_program2" VARCHAR(25)  NOT NULL,
	"nama_program2" VARCHAR(300),
	"ranking" INT2,
	PRIMARY KEY ("kode_program","kode_program2")
);

COMMENT ON TABLE ebudget.master_program2 IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_sasaran
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_sasaran CASCADE;

DROP SEQUENCE ebudget.master_sasaran_seq;

CREATE SEQUENCE ebudget.master_sasaran_seq;


CREATE TABLE ebudget.master_sasaran
(
	"kode_sasaran" CHAR(2)  NOT NULL,
	"nama_sasaran" VARCHAR(200),
	"id" INTEGER  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.master_sasaran IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_sub
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_sub CASCADE;

DROP SEQUENCE ebudget.master_sub_seq;

CREATE SEQUENCE ebudget.master_sub_seq;


CREATE TABLE ebudget.master_sub
(
	"kode_sub" VARCHAR(40),
	"sub" VARCHAR(400),
	"id" INTEGER  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.master_sub IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_tujuan
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_tujuan CASCADE;

DROP SEQUENCE ebudget.master_tujuan_seq;

CREATE SEQUENCE ebudget.master_tujuan_seq;


CREATE TABLE ebudget.master_tujuan
(
	"kode_tujuan" VARCHAR(2)  NOT NULL,
	"nama_tujuan" VARCHAR(200),
	"kode_misi" VARCHAR(2),
	"id" INTEGER  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.master_tujuan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_urusan
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_urusan CASCADE;


CREATE TABLE ebudget.master_urusan
(
	"kode_urusan" VARCHAR(10)  NOT NULL,
	"nama_urusan" VARCHAR(200),
	"ranking" INT2,
	PRIMARY KEY ("kode_urusan")
);

COMMENT ON TABLE ebudget.master_urusan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_urusan_wajib
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_urusan_wajib CASCADE;


CREATE TABLE ebudget.master_urusan_wajib
(
	"kode_urusan_wajib" CHAR(2)  NOT NULL,
	"nama_urusan_wajib" VARCHAR(200),
	PRIMARY KEY ("kode_urusan_wajib")
);

COMMENT ON TABLE ebudget.master_urusan_wajib IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.print_koefisien
-----------------------------------------------------------------------------

DROP TABLE ebudget.print_koefisien CASCADE;


CREATE TABLE ebudget.print_koefisien
(
	"unit_id" VARCHAR(20)  NOT NULL,
	"kegiatan_code" VARCHAR(30)  NOT NULL,
	"print_no" INTEGER  NOT NULL,
	"keterangan" VARCHAR(400),
	"tgl_print" TIMESTAMP,
	PRIMARY KEY ("unit_id","kegiatan_code","print_no")
);

COMMENT ON TABLE ebudget.print_koefisien IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.print_koefisien_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.print_koefisien_detail CASCADE;


CREATE TABLE ebudget.print_koefisien_detail
(
	"unit_id" VARCHAR(20)  NOT NULL,
	"kegiatan_code" VARCHAR(30)  NOT NULL,
	"print_no" INTEGER  NOT NULL,
	"detail_no" INT8  NOT NULL,
	"revisi_no" INTEGER  NOT NULL,
	PRIMARY KEY ("unit_id","kegiatan_code","print_no","detail_no","revisi_no")
);

COMMENT ON TABLE ebudget.print_koefisien_detail IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rekening
-----------------------------------------------------------------------------

DROP TABLE ebudget.rekening CASCADE;


CREATE TABLE ebudget.rekening
(
	"rekening_code" VARCHAR(100)  NOT NULL,
	"user_id" VARCHAR(20),
	"belanja_id" INTEGER,
	"rekening_name" VARCHAR(100),
	"rekening_ppn" INT2,
	"rekening_pph" INT2,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	PRIMARY KEY ("rekening_code")
);

COMMENT ON TABLE ebudget.rekening IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.rincian_detail_masalah
-----------------------------------------------------------------------------

DROP TABLE ebudget.rincian_detail_masalah CASCADE;


CREATE TABLE ebudget.rincian_detail_masalah
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" VARCHAR(150),
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.rincian_detail_masalah IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.satuan
-----------------------------------------------------------------------------

DROP TABLE ebudget.satuan CASCADE;


CREATE TABLE ebudget.satuan
(
	"satuan_id" INTEGER  NOT NULL,
	"user_id" VARCHAR(20),
	"satuan_name" VARCHAR(50),
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	PRIMARY KEY ("satuan_id")
);

COMMENT ON TABLE ebudget.satuan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.shsd
-----------------------------------------------------------------------------

DROP TABLE ebudget.shsd CASCADE;


CREATE TABLE ebudget.shsd
(
	"shsd_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"shsd_name" VARCHAR,
	"shsd_harga" DOUBLE PRECISION,
	"shsd_harga_dasar" DOUBLE PRECISION,
	"shsd_show" BOOLEAN,
	"shsd_faktor_inflasi" NUMERIC(6,4),
	"shsd_locked" BOOLEAN default 'f',
	"rekening_code" VARCHAR(300),
	"shsd_merk" VARCHAR(250),
	"non_pajak" BOOLEAN default 'f',
	"shsd_id_old" VARCHAR(30),
	"nama_dasar" VARCHAR(300),
	"spec" VARCHAR(500),
	"hidden_spec" VARCHAR(500),
	"usulan_skpd" VARCHAR(100),
	"is_potong_bpjs" BOOLEAN default 'f',
	"is_iuran_bpjs" BOOLEAN default 'f',
	"tahap" TEXT,
	PRIMARY KEY ("shsd_id")
);

COMMENT ON TABLE ebudget.shsd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.sub_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.sub_kegiatan CASCADE;


CREATE TABLE ebudget.sub_kegiatan
(
	"sub_kegiatan_id" VARCHAR(50)  NOT NULL,
	"sub_kegiatan_name" VARCHAR,
	"param" VARCHAR,
	"satuan" VARCHAR,
	"status" VARCHAR(15) default 'Open',
	"pembagi" VARCHAR,
	"keterangan" VARCHAR,
	"unit_id" VARCHAR(20),
	"penelitian_skala" VARCHAR(10),
	"penelitian_waktu" INT2,
	"batas_nilai" INT8,
	"tenaga_ahli" INT2,
	PRIMARY KEY ("sub_kegiatan_id")
);

COMMENT ON TABLE ebudget.sub_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.sub_kegiatan_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.sub_kegiatan_member CASCADE;


CREATE TABLE ebudget.sub_kegiatan_member
(
	"sub_kegiatan_id" VARCHAR(50)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"komponen_id" VARCHAR(50)  NOT NULL,
	"rekening_code" VARCHAR(100),
	"detail_name" VARCHAR(150),
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"koefisien" DOUBLE PRECISION,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR,
	"pajak" INTEGER default 0,
	"param" VARCHAR,
	"from_kegiatan_member" VARCHAR(30),
	"temp_komponen_id" VARCHAR(50)
);

COMMENT ON TABLE ebudget.sub_kegiatan_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.sub_kegiatan_member_salah
-----------------------------------------------------------------------------

DROP TABLE ebudget.sub_kegiatan_member_salah CASCADE;


CREATE TABLE ebudget.sub_kegiatan_member_salah
(
	"sub_kegiatan_id" VARCHAR(50)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"komponen_id" VARCHAR(50)  NOT NULL,
	"rekening_code" VARCHAR(100),
	"detail_name" VARCHAR(150),
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"koefisien" DOUBLE PRECISION,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR,
	"pajak" INTEGER default 0,
	"param" VARCHAR,
	"from_kegiatan_member" VARCHAR(30),
	"temp_komponen_id" VARCHAR(50)
);

COMMENT ON TABLE ebudget.sub_kegiatan_member_salah IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.subtitle_nilai
-----------------------------------------------------------------------------

DROP TABLE ebudget.subtitle_nilai CASCADE;

DROP SEQUENCE ebudget.subtitle_nilai_seq;

CREATE SEQUENCE ebudget.subtitle_nilai_seq;


CREATE TABLE ebudget.subtitle_nilai
(
	"unit_id" VARCHAR(10),
	"nilai_1" INT2,
	"nilai_2" INT2,
	"nilai_3" INT2,
	"total" INTEGER,
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(300),
	"keterangan" VARCHAR(300),
	"sub_id" INTEGER  NOT NULL
);

COMMENT ON TABLE ebudget.subtitle_nilai IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.personil_rka
-----------------------------------------------------------------------------

DROP TABLE ebudget.personil_rka CASCADE;


CREATE TABLE ebudget.personil_rka
(
	"nip" VARCHAR(25),
	"nama" VARCHAR(70),
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(70)
);

COMMENT ON TABLE ebudget.personil_rka IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.user_log
-----------------------------------------------------------------------------

DROP TABLE ebudget.user_log CASCADE;

DROP SEQUENCE ebudget.user_log_seq;

CREATE SEQUENCE ebudget.user_log_seq;


CREATE TABLE ebudget.user_log
(
	"id" INTEGER  NOT NULL,
	"waktu" TIMESTAMP,
	"nama_user" VARCHAR(100),
	"deskripsi" TEXT,
	"ip_address" VARCHAR(50),
	"nama_lengkap" VARCHAR(100),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.user_log IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.pdf_file
-----------------------------------------------------------------------------

DROP TABLE ebudget.pdf_file CASCADE;

DROP SEQUENCE ebudget.pdf_file_seq;

CREATE SEQUENCE ebudget.pdf_file_seq;


CREATE TABLE ebudget.pdf_file
(
	"pdf_id" INTEGER  NOT NULL,
	"unit_id" VARCHAR(20)  NOT NULL,
	"kegiatan_code" VARCHAR(30)  NOT NULL,
	"sub_id" INTEGER,
	"subtitle" VARCHAR(300),
	"nama_asli" VARCHAR(200),
	"nama_alias" VARCHAR(200),
	"hash" VARCHAR,
	"user_id" VARCHAR(150),
	PRIMARY KEY ("pdf_id")
);

COMMENT ON TABLE ebudget.pdf_file IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rka_member CASCADE;


CREATE TABLE ebudget.bappeko_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.bappeko_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.bappeko_master_ke_seq_22;

CREATE SEQUENCE ebudget.bappeko_master_ke_seq_22;


CREATE TABLE ebudget.bappeko_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.bappeko_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rincian CASCADE;


CREATE TABLE ebudget.bappeko_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.bappeko_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.bappeko_subtitle__seq_23;

CREATE SEQUENCE ebudget.bappeko_subtitle__seq_23;


CREATE TABLE ebudget.bappeko_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.bappeko_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rincian_detail CASCADE;


CREATE TABLE ebudget.bappeko_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.bappeko_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.bappeko_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.bappeko_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.bappeko_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.bappeko_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_kategori_shsd
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_kategori_shsd CASCADE;


CREATE TABLE ebudget.bappeko_kategori_shsd
(
	"kategori_shsd_id" VARCHAR(50)  NOT NULL,
	"kategori_shsd_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	"rekening" VARCHAR
);

COMMENT ON TABLE ebudget.bappeko_kategori_shsd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_komponen
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_komponen CASCADE;


CREATE TABLE ebudget.bappeko_komponen
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	"status_masuk" VARCHAR(10),
	"rka_member" BOOLEAN,
	"maintenance" BOOLEAN,
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.bappeko_komponen IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_asb_fisik
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_asb_fisik CASCADE;


CREATE TABLE ebudget.bappeko_asb_fisik
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	"asb_locked" BOOLEAN default 'f',
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.bappeko_asb_fisik IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_komponen_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_komponen_member CASCADE;


CREATE TABLE ebudget.bappeko_komponen_member
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"member_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"member_name" VARCHAR,
	"member_harga" DOUBLE PRECISION,
	"koefisien" DOUBLE PRECISION,
	"member_total" DOUBLE PRECISION,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"member_tipe" VARCHAR(10),
	"subtitle" VARCHAR(250),
	"member_no" DOUBLE PRECISION,
	"tipe" VARCHAR(40),
	"komponen_tipe" VARCHAR(10),
	"from_id" VARCHAR(30),
	"from_koef" DOUBLE PRECISION,
	"hspk_name" VARCHAR(300)
);

COMMENT ON TABLE ebudget.bappeko_komponen_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_komponen_rekening
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_komponen_rekening CASCADE;


CREATE TABLE ebudget.bappeko_komponen_rekening
(
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"rekening_code29" VARCHAR(15)
);

COMMENT ON TABLE ebudget.bappeko_komponen_rekening IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_shsd
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_shsd CASCADE;


CREATE TABLE ebudget.bappeko_shsd
(
	"shsd_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"shsd_name" VARCHAR,
	"shsd_harga" DOUBLE PRECISION,
	"shsd_harga_dasar" DOUBLE PRECISION,
	"shsd_show" BOOLEAN,
	"shsd_faktor_inflasi" NUMERIC(6,4),
	"shsd_locked" BOOLEAN default 'f',
	"rekening_code" VARCHAR(300),
	"shsd_merk" VARCHAR(250),
	"non_pajak" BOOLEAN default 'f',
	"shsd_id_old" VARCHAR(30),
	"nama_dasar" VARCHAR(300),
	"spec" VARCHAR(500),
	"hidden_spec" VARCHAR(500),
	PRIMARY KEY ("shsd_id")
);

COMMENT ON TABLE ebudget.bappeko_shsd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_sub_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_sub_kegiatan CASCADE;


CREATE TABLE ebudget.bappeko_sub_kegiatan
(
	"sub_kegiatan_id" VARCHAR(50)  NOT NULL,
	"sub_kegiatan_name" VARCHAR,
	"param" VARCHAR,
	"satuan" VARCHAR,
	"status" VARCHAR(15) default 'Open',
	"pembagi" VARCHAR,
	"keterangan" VARCHAR,
	"unit_id" VARCHAR(20),
	"penelitian_skala" VARCHAR(10),
	"penelitian_waktu" INT2,
	"batas_nilai" INT8,
	"tenaga_ahli" INT2,
	PRIMARY KEY ("sub_kegiatan_id")
);

COMMENT ON TABLE ebudget.bappeko_sub_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_kelompok_gmap
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_kelompok_gmap CASCADE;


CREATE TABLE ebudget.master_kelompok_gmap
(
	"id_kelompok" INTEGER  NOT NULL,
	"kode_kelompok" VARCHAR(20),
	"nama_objek" VARCHAR(100),
	"tipe_objek" INTEGER  NOT NULL,
	"warna" VARCHAR(20),
	"rekening" VARCHAR(10),
	PRIMARY KEY ("id_kelompok")
);

COMMENT ON TABLE ebudget.master_kelompok_gmap IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.master_lokasi_gmap
-----------------------------------------------------------------------------

DROP TABLE ebudget.master_lokasi_gmap CASCADE;


CREATE TABLE ebudget.master_lokasi_gmap
(
	"id" INTEGER  NOT NULL,
	"nama_lokasi" VARCHAR(50),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.master_lokasi_gmap IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.log_perubahan_revisi
-----------------------------------------------------------------------------

DROP TABLE ebudget.log_perubahan_revisi CASCADE;


CREATE TABLE ebudget.log_perubahan_revisi
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"unit_id" VARCHAR(8)  NOT NULL,
	"tahap" VARCHAR(20)  NOT NULL,
	"nilai_anggaran_semula" NUMERIC(20,4)  NOT NULL,
	"nilai_anggaran_menjadi" NUMERIC(20,4)  NOT NULL,
	PRIMARY KEY ("kegiatan_code","detail_no","unit_id","tahap")
);

COMMENT ON TABLE ebudget.log_perubahan_revisi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.log_approval
-----------------------------------------------------------------------------

DROP TABLE ebudget.log_approval CASCADE;


CREATE TABLE ebudget.log_approval
(
	"id" INT8  NOT NULL,
	"unit_id" VARCHAR(8),
	"kegiatan_code" VARCHAR(20),
	"user_id" VARCHAR(100),
	"waktu" TIMESTAMP,
	"kumpulan_detail_no" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE ebudget.log_approval IS '';


SET search_path TO public;