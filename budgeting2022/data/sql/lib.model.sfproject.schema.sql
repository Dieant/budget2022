
-----------------------------------------------------------------------------
-- eproject.belanja
-----------------------------------------------------------------------------

DROP TABLE "eproject.belanja" CASCADE;

DROP SEQUENCE "eproject.belanja_seq";

CREATE SEQUENCE "eproject.belanja_seq";


CREATE TABLE "eproject.belanja"
(
	"id" INTEGER  NOT NULL,
	"kode" VARCHAR(32)  NOT NULL,
	"nama" VARCHAR(64)  NOT NULL,
	"urutan" INTEGER,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.belanja" IS 'Belanja';


COMMENT ON COLUMN "eproject.belanja"."id" IS 'Id';

COMMENT ON COLUMN "eproject.belanja"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.belanja"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.belanja"."urutan" IS 'Urutan';

COMMENT ON COLUMN "eproject.belanja"."state" IS 'State';

COMMENT ON COLUMN "eproject.belanja"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.belanja"."updated_at" IS 'Updated At';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.detail_kegiatan
-----------------------------------------------------------------------------

DROP TABLE "eproject.detail_kegiatan" CASCADE;

DROP SEQUENCE "eproject.detail_kegiatan_seq";

CREATE SEQUENCE "eproject.detail_kegiatan_seq";


CREATE TABLE "eproject.detail_kegiatan"
(
	"id" INTEGER  NOT NULL,
	"kegiatan_id" INTEGER  NOT NULL,
	"nama_subtitle" VARCHAR(256),
	"rekening_kode" VARCHAR(128)  NOT NULL,
	"pajak" INTEGER,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"volume" DOUBLE PRECISION,
	"kode_komponen" VARCHAR(32),
	"id_budgeting" INTEGER,
	"kode_detail_kegiatan" VARCHAR(32) default '0000.0000.0000',
	"harga" DOUBLE PRECISION,
	"nama" TEXT,
	"kode_subtitle" VARCHAR(32),
	"kode_sub2title" VARCHAR(32),
	"updated_value" INTEGER default 0,
	"updated_value_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.detail_kegiatan" IS 'Detail Kegiatan';


COMMENT ON COLUMN "eproject.detail_kegiatan"."id" IS 'Id';

COMMENT ON COLUMN "eproject.detail_kegiatan"."kegiatan_id" IS 'Kegiatan Id';

COMMENT ON COLUMN "eproject.detail_kegiatan"."nama_subtitle" IS 'Nama Subtitle';

COMMENT ON COLUMN "eproject.detail_kegiatan"."rekening_kode" IS 'Rekening  Kode';

COMMENT ON COLUMN "eproject.detail_kegiatan"."pajak" IS 'Pajak';

COMMENT ON COLUMN "eproject.detail_kegiatan"."state" IS 'State';

COMMENT ON COLUMN "eproject.detail_kegiatan"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.detail_kegiatan"."updated_at" IS 'Updated At';

COMMENT ON COLUMN "eproject.detail_kegiatan"."volume" IS 'Volume';

COMMENT ON COLUMN "eproject.detail_kegiatan"."kode_komponen" IS 'Kode Komponen';

COMMENT ON COLUMN "eproject.detail_kegiatan"."id_budgeting" IS 'Id Budgeting';

COMMENT ON COLUMN "eproject.detail_kegiatan"."kode_detail_kegiatan" IS 'Kode Detail Kegiatan';

COMMENT ON COLUMN "eproject.detail_kegiatan"."harga" IS 'Harga';

COMMENT ON COLUMN "eproject.detail_kegiatan"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.detail_kegiatan"."kode_subtitle" IS 'Kode Subtitle';

COMMENT ON COLUMN "eproject.detail_kegiatan"."kode_sub2title" IS 'Kode Sub2title';

COMMENT ON COLUMN "eproject.detail_kegiatan"."updated_value" IS 'Updated Value';

COMMENT ON COLUMN "eproject.detail_kegiatan"."updated_value_at" IS 'Updated Value At';

SET search_path TO public;
ALTER TABLE "eproject.detail_kegiatan" ADD CONSTRAINT "eproject.detail_kegiatan_FK_1" FOREIGN KEY ("kegiatan_id") REFERENCES "eproject.kegiatan" ("id") ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE "eproject.detail_kegiatan" ADD CONSTRAINT "eproject.detail_kegiatan_FK_2" FOREIGN KEY ("rekening_kode") REFERENCES "eproject.rekening" ("kode") ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE "eproject.detail_kegiatan" ADD CONSTRAINT "eproject.detail_kegiatan_FK_3" FOREIGN KEY ("kode_subtitle") REFERENCES "eproject.subtitle" ("kode") ON UPDATE RESTRICT ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.detail_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE "eproject.detail_pekerjaan" CASCADE;


CREATE TABLE "eproject.detail_pekerjaan"
(
	"pekerjaan_id" INTEGER  NOT NULL,
	"detail_kegiatan_id" INTEGER  NOT NULL,
	"alokasi" DOUBLE PRECISION,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("pekerjaan_id","detail_kegiatan_id")
);

COMMENT ON TABLE "eproject.detail_pekerjaan" IS 'Detail Pekerjaan';


COMMENT ON COLUMN "eproject.detail_pekerjaan"."pekerjaan_id" IS 'Pekerjaan Id';

COMMENT ON COLUMN "eproject.detail_pekerjaan"."detail_kegiatan_id" IS 'Detail Kegiatan Id';

COMMENT ON COLUMN "eproject.detail_pekerjaan"."alokasi" IS 'Alokasi';

COMMENT ON COLUMN "eproject.detail_pekerjaan"."state" IS 'State';

COMMENT ON COLUMN "eproject.detail_pekerjaan"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.detail_pekerjaan"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.detail_pekerjaan" ADD CONSTRAINT "eproject.detail_pekerjaan_FK_1" FOREIGN KEY ("pekerjaan_id") REFERENCES "eproject.pekerjaan" ("id") ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.kegiatan
-----------------------------------------------------------------------------

DROP TABLE "eproject.kegiatan" CASCADE;

DROP SEQUENCE "eproject.kegiatan_seq";

CREATE SEQUENCE "eproject.kegiatan_seq";


CREATE TABLE "eproject.kegiatan"
(
	"id" INTEGER  NOT NULL,
	"user_id" INTEGER,
	"skpd_id" INTEGER,
	"tahun" INTEGER  NOT NULL,
	"kode" VARCHAR(32)  NOT NULL,
	"nama" VARCHAR(256)  NOT NULL,
	"kode_budgeting" VARCHAR(24)  NOT NULL,
	"anggaran_non_hitung" DOUBLE PRECISION default 0,
	"kode_kegiatan" VARCHAR(32)  NOT NULL,
	"lanjutan" BOOLEAN default 'f',
	"non_apbd" BOOLEAN default 'f',
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.kegiatan" IS 'Kegiatan';


COMMENT ON COLUMN "eproject.kegiatan"."id" IS 'Id';

COMMENT ON COLUMN "eproject.kegiatan"."user_id" IS 'User Id';

COMMENT ON COLUMN "eproject.kegiatan"."skpd_id" IS 'Skpd Id';

COMMENT ON COLUMN "eproject.kegiatan"."tahun" IS 'Tahun';

COMMENT ON COLUMN "eproject.kegiatan"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.kegiatan"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.kegiatan"."kode_budgeting" IS 'Kode  Budgeting';

COMMENT ON COLUMN "eproject.kegiatan"."anggaran_non_hitung" IS 'Anggaran Non Hitung';

COMMENT ON COLUMN "eproject.kegiatan"."kode_kegiatan" IS 'Kode  Lengkap';

COMMENT ON COLUMN "eproject.kegiatan"."state" IS 'State';

COMMENT ON COLUMN "eproject.kegiatan"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.kegiatan"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.kegiatan" ADD CONSTRAINT "eproject.kegiatan_FK_1" FOREIGN KEY ("user_id") REFERENCES "eproject.application_user" ("id");

ALTER TABLE "eproject.kegiatan" ADD CONSTRAINT "eproject.kegiatan_FK_2" FOREIGN KEY ("skpd_id") REFERENCES "eproject.skpd" ("id");

-----------------------------------------------------------------------------
-- eproject.pekerjaan
-----------------------------------------------------------------------------

DROP TABLE "eproject.pekerjaan" CASCADE;

DROP SEQUENCE "eproject.pekerjaan_seq";

CREATE SEQUENCE "eproject.pekerjaan_seq";


CREATE TABLE "eproject.pekerjaan"
(
	"id" INTEGER  NOT NULL,
	"kegiatan_id" INTEGER  NOT NULL,
	"kode" VARCHAR(32)  NOT NULL,
	"nama" VARCHAR(256)  NOT NULL,
	"jenis" INTEGER,
	"metode" INTEGER,
	"fisik" INTEGER,
	"rencana_mulai" DATE,
	"rencana_selesai" DATE,
	"rencana_kontrak" DATE,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"jenis_anggaran_id" INTEGER default 1 NOT NULL,
	"berisi_target" INTEGER default 0,
	"subtitle_id" VARCHAR(32),
	"jumlah_target" VARCHAR(32),
	"kecamatan_id" VARCHAR(4) default '0000',
	"deleted" INTEGER default 0,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.pekerjaan" IS 'Pekerjaan';


COMMENT ON COLUMN "eproject.pekerjaan"."id" IS 'Id';

COMMENT ON COLUMN "eproject.pekerjaan"."kegiatan_id" IS 'Kegiatan Id';

COMMENT ON COLUMN "eproject.pekerjaan"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.pekerjaan"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.pekerjaan"."jenis" IS 'Jenis';

COMMENT ON COLUMN "eproject.pekerjaan"."metode" IS 'Metode';

COMMENT ON COLUMN "eproject.pekerjaan"."fisik" IS 'Pekerjaan Fisik';

COMMENT ON COLUMN "eproject.pekerjaan"."rencana_mulai" IS 'Rencana Mulai';

COMMENT ON COLUMN "eproject.pekerjaan"."rencana_selesai" IS 'Rencana Selesai';

COMMENT ON COLUMN "eproject.pekerjaan"."rencana_kontrak" IS 'Rencana Kontrak';

COMMENT ON COLUMN "eproject.pekerjaan"."state" IS 'State';

COMMENT ON COLUMN "eproject.pekerjaan"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.pekerjaan"."updated_at" IS 'Updated At';

COMMENT ON COLUMN "eproject.pekerjaan"."jenis_anggaran_id" IS 'Jenis  Anggaran';

COMMENT ON COLUMN "eproject.pekerjaan"."berisi_target" IS 'Berisi Target Pencapaian Peformance';

COMMENT ON COLUMN "eproject.pekerjaan"."subtitle_id" IS 'Subtitle';

COMMENT ON COLUMN "eproject.pekerjaan"."jumlah_target" IS 'Jumlah Target';

COMMENT ON COLUMN "eproject.pekerjaan"."kecamatan_id" IS 'Kecamatan';

COMMENT ON COLUMN "eproject.pekerjaan"."deleted" IS 'Terhapus';

SET search_path TO public;
ALTER TABLE "eproject.pekerjaan" ADD CONSTRAINT "eproject.pekerjaan_FK_1" FOREIGN KEY ("kegiatan_id") REFERENCES "eproject.kegiatan" ("id") ON DELETE RESTRICT;

ALTER TABLE "eproject.pekerjaan" ADD CONSTRAINT "eproject.pekerjaan_FK_2" FOREIGN KEY ("jenis_anggaran_id") REFERENCES "eproject.jenis_anggaran" ("id") ON DELETE RESTRICT;

ALTER TABLE "eproject.pekerjaan" ADD CONSTRAINT "eproject.pekerjaan_FK_3" FOREIGN KEY ("metode") REFERENCES "eproject.metode_lelang" ("id");

ALTER TABLE "eproject.pekerjaan" ADD CONSTRAINT "eproject.pekerjaan_FK_4" FOREIGN KEY ("jenis") REFERENCES "eproject.jenis_pekerjaan" ("id");

ALTER TABLE "eproject.pekerjaan" ADD CONSTRAINT "eproject.pekerjaan_FK_5" FOREIGN KEY ("kecamatan_id") REFERENCES "eproject.kecamatan" ("id");

-----------------------------------------------------------------------------
-- eproject.rekening
-----------------------------------------------------------------------------

DROP TABLE "eproject.rekening" CASCADE;


CREATE TABLE "eproject.rekening"
(
	"kode" VARCHAR(128)  NOT NULL,
	"belanja_id" INTEGER  NOT NULL,
	"nama" VARCHAR(128)  NOT NULL,
	"ppn" INTEGER,
	"pph" INTEGER,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE "eproject.rekening" IS 'RekeningEproject';


COMMENT ON COLUMN "eproject.rekening"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.rekening"."belanja_id" IS 'Belanja Id';

COMMENT ON COLUMN "eproject.rekening"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.rekening"."ppn" IS 'Ppn';

COMMENT ON COLUMN "eproject.rekening"."pph" IS 'Pph';

COMMENT ON COLUMN "eproject.rekening"."state" IS 'State';

COMMENT ON COLUMN "eproject.rekening"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.rekening"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.rekening" ADD CONSTRAINT "eproject.rekening_FK_1" FOREIGN KEY ("belanja_id") REFERENCES "eproject.belanja" ("id");

-----------------------------------------------------------------------------
-- eproject.rencana_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE "eproject.rencana_pekerjaan" CASCADE;


CREATE TABLE "eproject.rencana_pekerjaan"
(
	"pekerjaan_id" INTEGER  NOT NULL,
	"bulan" INTEGER  NOT NULL,
	"fisik" DOUBLE PRECISION,
	"nominal" DOUBLE PRECISION,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("pekerjaan_id","bulan")
);

COMMENT ON TABLE "eproject.rencana_pekerjaan" IS 'Rencana  Pekerjaan';


COMMENT ON COLUMN "eproject.rencana_pekerjaan"."pekerjaan_id" IS 'Pekerjaan Id';

COMMENT ON COLUMN "eproject.rencana_pekerjaan"."bulan" IS 'Bulan';

COMMENT ON COLUMN "eproject.rencana_pekerjaan"."fisik" IS 'Fisik';

COMMENT ON COLUMN "eproject.rencana_pekerjaan"."nominal" IS 'Nominal';

COMMENT ON COLUMN "eproject.rencana_pekerjaan"."state" IS 'State';

COMMENT ON COLUMN "eproject.rencana_pekerjaan"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.rencana_pekerjaan"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.rencana_pekerjaan" ADD CONSTRAINT "eproject.rencana_pekerjaan_FK_1" FOREIGN KEY ("pekerjaan_id") REFERENCES "eproject.pekerjaan" ("id") ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.skpd
-----------------------------------------------------------------------------

DROP TABLE "eproject.skpd" CASCADE;

DROP SEQUENCE "eproject.skpd_seq";

CREATE SEQUENCE "eproject.skpd_seq";


CREATE TABLE "eproject.skpd"
(
	"id" INTEGER  NOT NULL,
	"kode" VARCHAR(8)  NOT NULL,
	"nama" VARCHAR(128)  NOT NULL,
	"state" INTEGER,
	"serap2007" FLOAT,
	"xml_hash" VARCHAR(255),
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"user_id" INTEGER,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.skpd" IS 'Skpd';


COMMENT ON COLUMN "eproject.skpd"."id" IS 'Id';

COMMENT ON COLUMN "eproject.skpd"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.skpd"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.skpd"."state" IS 'State';

COMMENT ON COLUMN "eproject.skpd"."serap2007" IS 'Penyerapan Anggaran 2007';

COMMENT ON COLUMN "eproject.skpd"."xml_hash" IS 'XML Hash';

COMMENT ON COLUMN "eproject.skpd"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.skpd"."updated_at" IS 'Updated At';

COMMENT ON COLUMN "eproject.skpd"."user_id" IS 'User Id';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.user_type
-----------------------------------------------------------------------------

DROP TABLE "eproject.user_type" CASCADE;

DROP SEQUENCE "eproject.user_type_seq";

CREATE SEQUENCE "eproject.user_type_seq";


CREATE TABLE "eproject.user_type"
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR(60)  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.user_type" IS 'Jenis Pengguna';


COMMENT ON COLUMN "eproject.user_type"."id" IS 'Id';

COMMENT ON COLUMN "eproject.user_type"."nama" IS 'Nama';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.application_user
-----------------------------------------------------------------------------

DROP TABLE "eproject.application_user" CASCADE;

DROP SEQUENCE "eproject.application_user_seq";

CREATE SEQUENCE "eproject.application_user_seq";


CREATE TABLE "eproject.application_user"
(
	"id" INTEGER  NOT NULL,
	"username" VARCHAR(32)  NOT NULL,
	"nama" VARCHAR(128)  NOT NULL,
	"email" VARCHAR(96),
	"sandi" VARCHAR(48),
	"pangkat" VARCHAR(10),
	"no_sk" VARCHAR(32),
	"role_id" INTEGER,
	"skpd_id" INTEGER,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "user_username" UNIQUE ("username")
);

COMMENT ON TABLE "eproject.application_user" IS 'Pengguna Aplikasi';


COMMENT ON COLUMN "eproject.application_user"."id" IS 'Id';

COMMENT ON COLUMN "eproject.application_user"."username" IS 'Username';

COMMENT ON COLUMN "eproject.application_user"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.application_user"."email" IS 'Email';

COMMENT ON COLUMN "eproject.application_user"."sandi" IS 'Password';

COMMENT ON COLUMN "eproject.application_user"."pangkat" IS 'Pangkat';

COMMENT ON COLUMN "eproject.application_user"."no_sk" IS 'No.SK';

COMMENT ON COLUMN "eproject.application_user"."role_id" IS 'Role Id';

COMMENT ON COLUMN "eproject.application_user"."skpd_id" IS 'Skpd Id';

COMMENT ON COLUMN "eproject.application_user"."state" IS 'State';

COMMENT ON COLUMN "eproject.application_user"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.application_user"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.application_user" ADD CONSTRAINT "eproject.application_user_FK_1" FOREIGN KEY ("skpd_id") REFERENCES "eproject.skpd" ("id");

ALTER TABLE "eproject.application_user" ADD CONSTRAINT "eproject.application_user_FK_2" FOREIGN KEY ("role_id") REFERENCES "eproject.user_type" ("id");

-----------------------------------------------------------------------------
-- eproject.jenis_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE "eproject.jenis_pekerjaan" CASCADE;

DROP SEQUENCE "eproject.jenis_pekerjaan_seq";

CREATE SEQUENCE "eproject.jenis_pekerjaan_seq";


CREATE TABLE "eproject.jenis_pekerjaan"
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR(60)  NOT NULL,
	"kode" VARCHAR(6),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.jenis_pekerjaan" IS 'Jenis Pekerjaan';


COMMENT ON COLUMN "eproject.jenis_pekerjaan"."id" IS 'Id';

COMMENT ON COLUMN "eproject.jenis_pekerjaan"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.jenis_pekerjaan"."kode" IS 'Kode';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.status_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE "eproject.status_pekerjaan" CASCADE;

DROP SEQUENCE "eproject.status_pekerjaan_seq";

CREATE SEQUENCE "eproject.status_pekerjaan_seq";


CREATE TABLE "eproject.status_pekerjaan"
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR(60)  NOT NULL,
	"kode" VARCHAR(6),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.status_pekerjaan" IS 'Status Pekerjaan';


COMMENT ON COLUMN "eproject.status_pekerjaan"."id" IS 'Id';

COMMENT ON COLUMN "eproject.status_pekerjaan"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.status_pekerjaan"."kode" IS 'Kode';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.metode_lelang
-----------------------------------------------------------------------------

DROP TABLE "eproject.metode_lelang" CASCADE;

DROP SEQUENCE "eproject.metode_lelang_seq";

CREATE SEQUENCE "eproject.metode_lelang_seq";


CREATE TABLE "eproject.metode_lelang"
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR(60)  NOT NULL,
	"kode" VARCHAR(6),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.metode_lelang" IS 'Metode Lelang';


COMMENT ON COLUMN "eproject.metode_lelang"."id" IS 'Id';

COMMENT ON COLUMN "eproject.metode_lelang"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.metode_lelang"."kode" IS 'Kode';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.jenis_informasi
-----------------------------------------------------------------------------

DROP TABLE "eproject.jenis_informasi" CASCADE;

DROP SEQUENCE "eproject.jenis_informasi_seq";

CREATE SEQUENCE "eproject.jenis_informasi_seq";


CREATE TABLE "eproject.jenis_informasi"
(
	"id" INT2  NOT NULL,
	"nama" VARCHAR(60)  NOT NULL,
	"state" INTEGER default 0,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.jenis_informasi" IS 'Jenis Informasi';


COMMENT ON COLUMN "eproject.jenis_informasi"."id" IS 'ID';

COMMENT ON COLUMN "eproject.jenis_informasi"."nama" IS 'Nama Jenis';

COMMENT ON COLUMN "eproject.jenis_informasi"."state" IS 'Status record';

COMMENT ON COLUMN "eproject.jenis_informasi"."created_at" IS 'Waktu record dibuat';

COMMENT ON COLUMN "eproject.jenis_informasi"."updated_at" IS 'Waktu record terakhir diubah';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.informasi
-----------------------------------------------------------------------------

DROP TABLE "eproject.informasi" CASCADE;

DROP SEQUENCE "eproject.informasi_seq";

CREATE SEQUENCE "eproject.informasi_seq";


CREATE TABLE "eproject.informasi"
(
	"id" INTEGER  NOT NULL,
	"judul" VARCHAR(200),
	"tanggal" DATE,
	"penulis" VARCHAR(200),
	"isi_singkat" TEXT,
	"isi" TEXT,
	"jenis" INT2,
	"umum" BOOLEAN,
	"skpd_id" INTEGER  NOT NULL,
	"informasi_filepath" VARCHAR(255),
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.informasi" IS 'Informasi';


COMMENT ON COLUMN "eproject.informasi"."id" IS 'ID';

COMMENT ON COLUMN "eproject.informasi"."judul" IS 'Judul';

COMMENT ON COLUMN "eproject.informasi"."tanggal" IS 'Tanggal';

COMMENT ON COLUMN "eproject.informasi"."penulis" IS 'Penulis';

COMMENT ON COLUMN "eproject.informasi"."isi_singkat" IS 'Deskripsi Singkat';

COMMENT ON COLUMN "eproject.informasi"."isi" IS 'Isi';

COMMENT ON COLUMN "eproject.informasi"."jenis" IS 'Jenis';

COMMENT ON COLUMN "eproject.informasi"."umum" IS 'Bisa dibaca oleh umum';

COMMENT ON COLUMN "eproject.informasi"."skpd_id" IS 'SKPD';

COMMENT ON COLUMN "eproject.informasi"."informasi_filepath" IS 'Download File';

COMMENT ON COLUMN "eproject.informasi"."state" IS 'Status record';

COMMENT ON COLUMN "eproject.informasi"."created_at" IS 'Waktu record dibuat';

COMMENT ON COLUMN "eproject.informasi"."updated_at" IS 'Waktu record terakhir diubah';

SET search_path TO public;
ALTER TABLE "eproject.informasi" ADD CONSTRAINT "eproject.informasi_FK_1" FOREIGN KEY ("skpd_id") REFERENCES "eproject.skpd" ("id") ON DELETE RESTRICT;

ALTER TABLE "eproject.informasi" ADD CONSTRAINT "eproject.informasi_FK_2" FOREIGN KEY ("jenis") REFERENCES "eproject.jenis_informasi" ("id") ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.informasi_user
-----------------------------------------------------------------------------

DROP TABLE "eproject.informasi_user" CASCADE;


CREATE TABLE "eproject.informasi_user"
(
	"informasi_id" INTEGER  NOT NULL,
	"user_type_id" INTEGER  NOT NULL,
	PRIMARY KEY ("informasi_id","user_type_id")
);

COMMENT ON TABLE "eproject.informasi_user" IS 'Daftar user penerima  informasi';


COMMENT ON COLUMN "eproject.informasi_user"."informasi_id" IS 'Informasi';

COMMENT ON COLUMN "eproject.informasi_user"."user_type_id" IS 'Tipe  User';

SET search_path TO public;
ALTER TABLE "eproject.informasi_user" ADD CONSTRAINT "eproject.informasi_user_FK_1" FOREIGN KEY ("informasi_id") REFERENCES "eproject.informasi" ("id") ON DELETE CASCADE;

ALTER TABLE "eproject.informasi_user" ADD CONSTRAINT "eproject.informasi_user_FK_2" FOREIGN KEY ("user_type_id") REFERENCES "eproject.user_type" ("id") ON DELETE CASCADE;

-----------------------------------------------------------------------------
-- eproject.kegiatan_revisi
-----------------------------------------------------------------------------

DROP TABLE "eproject.kegiatan_revisi" CASCADE;

DROP SEQUENCE "eproject.kegiatan_revisi_seq";

CREATE SEQUENCE "eproject.kegiatan_revisi_seq";


CREATE TABLE "eproject.kegiatan_revisi"
(
	"id" INTEGER  NOT NULL,
	"user_id" INTEGER,
	"skpd_id" INTEGER,
	"tahun" INTEGER  NOT NULL,
	"kode" VARCHAR(32)  NOT NULL,
	"nama" VARCHAR(256)  NOT NULL,
	"kode_budgeting" VARCHAR(24)  NOT NULL,
	"anggaran_non_hitung" DOUBLE PRECISION default 0,
	"versi_revisi" INTEGER default 1,
	"status_revisi" INTEGER default 1,
	"keterangan" TEXT  NOT NULL,
	"id_asal" INTEGER,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.kegiatan_revisi" IS 'Kegiatan Revisi';


COMMENT ON COLUMN "eproject.kegiatan_revisi"."id" IS 'Id';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."user_id" IS 'User Id';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."skpd_id" IS 'Skpd Id';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."tahun" IS 'Tahun';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."kode_budgeting" IS 'Kode  Budgeting';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."anggaran_non_hitung" IS 'Anggaran Non Hitung';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."versi_revisi" IS 'Versi Revisi';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."status_revisi" IS 'Status Revisi';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."keterangan" IS 'Keterangan Revisi';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."id_asal" IS 'Id Asal';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."state" IS 'State';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.kegiatan_revisi"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.kegiatan_revisi" ADD CONSTRAINT "eproject.kegiatan_revisi_FK_1" FOREIGN KEY ("user_id") REFERENCES "eproject.application_user" ("id");

ALTER TABLE "eproject.kegiatan_revisi" ADD CONSTRAINT "eproject.kegiatan_revisi_FK_2" FOREIGN KEY ("skpd_id") REFERENCES "eproject.skpd" ("id");

-----------------------------------------------------------------------------
-- eproject.detail_kegiatan_revisi
-----------------------------------------------------------------------------

DROP TABLE "eproject.detail_kegiatan_revisi" CASCADE;

DROP SEQUENCE "eproject.detail_kegiatan_r_seq_8";

CREATE SEQUENCE "eproject.detail_kegiatan_r_seq_8";


CREATE TABLE "eproject.detail_kegiatan_revisi"
(
	"id" INTEGER  NOT NULL,
	"kegiatan_id" INTEGER  NOT NULL,
	"nama_subtitle" VARCHAR(256),
	"rekening_kode" VARCHAR(128)  NOT NULL,
	"pajak" INTEGER,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"volume" DOUBLE PRECISION,
	"kode_komponen" VARCHAR(32),
	"id_budgeting" INTEGER,
	"harga" DOUBLE PRECISION,
	"nama" TEXT,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.detail_kegiatan_revisi" IS 'Detail  Kegiatan Revisi';


COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."id" IS 'Id';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."kegiatan_id" IS 'Kegiatan Id';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."nama_subtitle" IS 'Nama Subtitle';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."rekening_kode" IS 'Rekening  Kode';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."pajak" IS 'Pajak';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."state" IS 'State';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."updated_at" IS 'Updated At';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."volume" IS 'Volume';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."kode_komponen" IS 'Kode Komponen';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."id_budgeting" IS 'Id Budgeting';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."harga" IS 'Harga';

COMMENT ON COLUMN "eproject.detail_kegiatan_revisi"."nama" IS 'Nama';

SET search_path TO public;
ALTER TABLE "eproject.detail_kegiatan_revisi" ADD CONSTRAINT "eproject.detail_kegiatan_re_FK_1" FOREIGN KEY ("kegiatan_id") REFERENCES "eproject.kegiatan_revisi" ("id") ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE "eproject.detail_kegiatan_revisi" ADD CONSTRAINT "eproject.detail_kegiatan_re_FK_2" FOREIGN KEY ("rekening_kode") REFERENCES "eproject.rekening" ("kode") ON UPDATE CASCADE ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.pekerjaan_revisi
-----------------------------------------------------------------------------

DROP TABLE "eproject.pekerjaan_revisi" CASCADE;

DROP SEQUENCE "eproject.pekerjaan_revisi_seq";

CREATE SEQUENCE "eproject.pekerjaan_revisi_seq";


CREATE TABLE "eproject.pekerjaan_revisi"
(
	"id" INTEGER  NOT NULL,
	"kegiatan_id" INTEGER  NOT NULL,
	"kode" VARCHAR(32)  NOT NULL,
	"nama" VARCHAR(256)  NOT NULL,
	"jenis" INTEGER,
	"metode" INTEGER,
	"fisik" INTEGER,
	"rencana_mulai" DATE,
	"rencana_selesai" DATE,
	"rencana_kontrak" DATE,
	"id_asal" INTEGER default 0,
	"versi_revisi" INTEGER default 1,
	"status_revisi" INTEGER default 1,
	"revisi_f1" INTEGER default 0,
	"revisi_alokasi" INTEGER default 0,
	"keterangan" TEXT  NOT NULL,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.pekerjaan_revisi" IS 'Pekerjaan Revisi';


COMMENT ON COLUMN "eproject.pekerjaan_revisi"."id" IS 'Id';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."kegiatan_id" IS 'Kegiatan Id';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."jenis" IS 'Jenis';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."metode" IS 'Metode';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."fisik" IS 'Pekerjaan Fisik';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."rencana_mulai" IS 'Rencana Mulai';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."rencana_selesai" IS 'Rencana Selesai';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."rencana_kontrak" IS 'Rencana Kontrak';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."id_asal" IS 'Id Pekerjaan Asal';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."versi_revisi" IS 'Versi Revisi';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."status_revisi" IS 'Status Revisi';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."revisi_f1" IS 'Banyaknya Revisi F1';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."revisi_alokasi" IS 'Banyaknya Revisi Alokasi';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."keterangan" IS 'Keterangan Revisi';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."state" IS 'State';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.pekerjaan_revisi"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.pekerjaan_revisi" ADD CONSTRAINT "eproject.pekerjaan_revisi_FK_1" FOREIGN KEY ("kegiatan_id") REFERENCES "eproject.kegiatan_revisi" ("id") ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE "eproject.pekerjaan_revisi" ADD CONSTRAINT "eproject.pekerjaan_revisi_FK_2" FOREIGN KEY ("metode") REFERENCES "eproject.metode_lelang" ("id");

ALTER TABLE "eproject.pekerjaan_revisi" ADD CONSTRAINT "eproject.pekerjaan_revisi_FK_3" FOREIGN KEY ("jenis") REFERENCES "eproject.jenis_pekerjaan" ("id");

-----------------------------------------------------------------------------
-- eproject.detail_pekerjaan_revisi
-----------------------------------------------------------------------------

DROP TABLE "eproject.detail_pekerjaan_revisi" CASCADE;


CREATE TABLE "eproject.detail_pekerjaan_revisi"
(
	"pekerjaan_id" INTEGER  NOT NULL,
	"detail_kegiatan_id" INTEGER  NOT NULL,
	"alokasi" DOUBLE PRECISION,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("pekerjaan_id","detail_kegiatan_id")
);

COMMENT ON TABLE "eproject.detail_pekerjaan_revisi" IS 'Detail  Pekerjaan Revisi';


COMMENT ON COLUMN "eproject.detail_pekerjaan_revisi"."pekerjaan_id" IS 'Pekerjaan Id';

COMMENT ON COLUMN "eproject.detail_pekerjaan_revisi"."detail_kegiatan_id" IS 'Detail Kegiatan Id';

COMMENT ON COLUMN "eproject.detail_pekerjaan_revisi"."alokasi" IS 'Alokasi';

COMMENT ON COLUMN "eproject.detail_pekerjaan_revisi"."state" IS 'State';

COMMENT ON COLUMN "eproject.detail_pekerjaan_revisi"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.detail_pekerjaan_revisi"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.detail_pekerjaan_revisi" ADD CONSTRAINT "eproject.detail_pekerjaan_r_FK_1" FOREIGN KEY ("pekerjaan_id") REFERENCES "eproject.pekerjaan_revisi" ("id") ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.rencana_pekerjaan_revisi
-----------------------------------------------------------------------------

DROP TABLE "eproject.rencana_pekerjaan_revisi" CASCADE;


CREATE TABLE "eproject.rencana_pekerjaan_revisi"
(
	"pekerjaan_id" INTEGER  NOT NULL,
	"bulan" INTEGER  NOT NULL,
	"fisik" DOUBLE PRECISION,
	"nominal" DOUBLE PRECISION,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("pekerjaan_id","bulan")
);

COMMENT ON TABLE "eproject.rencana_pekerjaan_revisi" IS 'Rencana Pekerjaan Revisi';


COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."pekerjaan_id" IS 'Pekerjaan Id';

COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."bulan" IS 'Bulan';

COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."fisik" IS 'Fisik';

COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."nominal" IS 'Nominal';

COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."state" IS 'State';

COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.rencana_pekerjaan_revisi"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.rencana_pekerjaan_revisi" ADD CONSTRAINT "eproject.rencana_pekerjaan__FK_1" FOREIGN KEY ("pekerjaan_id") REFERENCES "eproject.pekerjaan_revisi" ("id") ON UPDATE CASCADE ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.subtitle
-----------------------------------------------------------------------------

DROP TABLE "eproject.subtitle" CASCADE;


CREATE TABLE "eproject.subtitle"
(
	"kode" VARCHAR(32)  NOT NULL,
	"nama" TEXT,
	"indikator" TEXT,
	"target" VARCHAR(100),
	"satuan" VARCHAR(100),
	"kegiatan_id" INTEGER  NOT NULL,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE "eproject.subtitle" IS 'Subtitle';


COMMENT ON COLUMN "eproject.subtitle"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.subtitle"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.subtitle"."kegiatan_id" IS 'Kegiatan Id';

COMMENT ON COLUMN "eproject.subtitle"."state" IS 'State';

COMMENT ON COLUMN "eproject.subtitle"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.subtitle"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.subtitle" ADD CONSTRAINT "eproject.subtitle_FK_1" FOREIGN KEY ("kegiatan_id") REFERENCES "eproject.kegiatan" ("id") ON UPDATE RESTRICT ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.sub2title
-----------------------------------------------------------------------------

DROP TABLE "eproject.sub2title" CASCADE;


CREATE TABLE "eproject.sub2title"
(
	"kode" VARCHAR(32)  NOT NULL,
	"nama" TEXT,
	"indikator" TEXT,
	"target" VARCHAR(100),
	"satuan" VARCHAR(100),
	"state" INTEGER,
	"kegiatan_id" INTEGER  NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE "eproject.sub2title" IS 'Subtitle SAB';


COMMENT ON COLUMN "eproject.sub2title"."kode" IS 'Kode';

COMMENT ON COLUMN "eproject.sub2title"."nama" IS 'Nama';

COMMENT ON COLUMN "eproject.sub2title"."state" IS 'State';

COMMENT ON COLUMN "eproject.sub2title"."kegiatan_id" IS 'Kegiatan Id';

COMMENT ON COLUMN "eproject.sub2title"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.sub2title"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.sub2title" ADD CONSTRAINT "eproject.sub2title_FK_1" FOREIGN KEY ("kegiatan_id") REFERENCES "eproject.kegiatan" ("id") ON UPDATE RESTRICT ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.jenis_anggaran
-----------------------------------------------------------------------------

DROP TABLE "eproject.jenis_anggaran" CASCADE;


CREATE TABLE "eproject.jenis_anggaran"
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR(60)  NOT NULL,
	"state" INTEGER default 0,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.jenis_anggaran" IS 'Murni/PAK';


COMMENT ON COLUMN "eproject.jenis_anggaran"."id" IS '1=Murni,2=PAK';

COMMENT ON COLUMN "eproject.jenis_anggaran"."nama" IS 'Nama Jenis Anggaran';

COMMENT ON COLUMN "eproject.jenis_anggaran"."state" IS 'Status record';

COMMENT ON COLUMN "eproject.jenis_anggaran"."created_at" IS 'Waktu record dibuat';

COMMENT ON COLUMN "eproject.jenis_anggaran"."updated_at" IS 'Waktu record terakhir diubah';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.kecamatan
-----------------------------------------------------------------------------

DROP TABLE "eproject.kecamatan" CASCADE;


CREATE TABLE "eproject.kecamatan"
(
	"id" VARCHAR(4)  NOT NULL,
	"nama" VARCHAR(64)  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "eproject.kecamatan" IS 'Kecamatan';


COMMENT ON COLUMN "eproject.kecamatan"."id" IS 'Kode Kecamatan';

COMMENT ON COLUMN "eproject.kecamatan"."nama" IS 'Nama';

SET search_path TO public;
-----------------------------------------------------------------------------
-- eproject.phones
-----------------------------------------------------------------------------

DROP TABLE "eproject.phones" CASCADE;


CREATE TABLE "eproject.phones"
(
	"user_id" INTEGER  NOT NULL,
	"nomor" VARCHAR(16)  NOT NULL,
	"state" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP
);

COMMENT ON TABLE "eproject.phones" IS 'Nomor telepon';


COMMENT ON COLUMN "eproject.phones"."state" IS 'State';

COMMENT ON COLUMN "eproject.phones"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.phones"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.phones" ADD CONSTRAINT "eproject.phones_FK_1" FOREIGN KEY ("user_id") REFERENCES "eproject.application_user" ("id") ON DELETE RESTRICT;

-----------------------------------------------------------------------------
-- eproject.rencana_belanja_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE "eproject.rencana_belanja_pekerjaan" CASCADE;


CREATE TABLE "eproject.rencana_belanja_pekerjaan"
(
	"pekerjaan_id" INTEGER  NOT NULL,
	"belanja_id" INTEGER  NOT NULL,
	"bulan" INTEGER  NOT NULL,
	"nominal" DOUBLE PRECISION,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("pekerjaan_id","belanja_id","bulan")
);

COMMENT ON TABLE "eproject.rencana_belanja_pekerjaan" IS 'Rencana Pekerjaan Per Belanja';


COMMENT ON COLUMN "eproject.rencana_belanja_pekerjaan"."pekerjaan_id" IS 'Pekerjaan Id';

COMMENT ON COLUMN "eproject.rencana_belanja_pekerjaan"."belanja_id" IS 'Belanja  Id';

COMMENT ON COLUMN "eproject.rencana_belanja_pekerjaan"."bulan" IS 'Bulan';

COMMENT ON COLUMN "eproject.rencana_belanja_pekerjaan"."nominal" IS 'Nominal';

COMMENT ON COLUMN "eproject.rencana_belanja_pekerjaan"."created_at" IS 'Created At';

COMMENT ON COLUMN "eproject.rencana_belanja_pekerjaan"."updated_at" IS 'Updated At';

SET search_path TO public;
ALTER TABLE "eproject.rencana_belanja_pekerjaan" ADD CONSTRAINT "eproject.rencana_belanja_pe_FK_1" FOREIGN KEY ("pekerjaan_id") REFERENCES "eproject.pekerjaan" ("id") ON DELETE RESTRICT;

ALTER TABLE "eproject.rencana_belanja_pekerjaan" ADD CONSTRAINT "eproject.rencana_belanja_pe_FK_2" FOREIGN KEY ("belanja_id") REFERENCES "eproject.belanja" ("id");
