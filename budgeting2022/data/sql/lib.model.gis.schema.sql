
-----------------------------------------------------------------------------
-- v_lokasi
-----------------------------------------------------------------------------

DROP TABLE v_lokasi CASCADE;


CREATE TABLE v_lokasi
(
	"kode" VARCHAR(16)  NOT NULL,
	"nama" VARCHAR(300),
	PRIMARY KEY ("kode")
);

COMMENT ON TABLE v_lokasi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- jasmas
-----------------------------------------------------------------------------

DROP TABLE jasmas CASCADE;


CREATE TABLE jasmas
(
	"kode_jasmas" VARCHAR(10)  NOT NULL,
	"nama" VARCHAR(200),
	"alamat" VARCHAR(500),
	PRIMARY KEY ("kode_jasmas")
);

COMMENT ON TABLE jasmas IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- master_lokasi_simbada
-----------------------------------------------------------------------------

DROP TABLE master_lokasi_simbada CASCADE;


CREATE TABLE master_lokasi_simbada
(
	"kode_lokasi_simbada" VARCHAR(16)  NOT NULL,
	"nama_lokasi_simbada" VARCHAR(200),
	"kecamatan" VARCHAR(50),
	"kelurahan" VARCHAR(50),
	"catatan" VARCHAR(500),
	"kode_jasmas" VARCHAR(10),
	"alamat" VARCHAR(500),
	PRIMARY KEY ("kode_lokasi_simbada")
);

COMMENT ON TABLE master_lokasi_simbada IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- baru_history_pekerjaan
-----------------------------------------------------------------------------

DROP TABLE baru_history_pekerjaan CASCADE;


CREATE TABLE baru_history_pekerjaan
(
	"id_history" INTEGER,
	"tahun" TEXT,
	"kode_budgeting" TEXT,
	"komponen_name" TEXT,
	"detail_name" TEXT,
	"satuan" TEXT,
	"volume" DOUBLE PRECISION,
	"nilai_anggaran" DOUBLE PRECISION,
	"unit_id" TEXT,
	"kegiatan_code" TEXT,
	"detail_no" TEXT,
	"schema_name" TEXT,
	"database_name" TEXT
);

COMMENT ON TABLE baru_history_pekerjaan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- history_pekerjaan_v2
-----------------------------------------------------------------------------

DROP TABLE history_pekerjaan_v2 CASCADE;


CREATE TABLE history_pekerjaan_v2
(
	"id_history" INTEGER  NOT NULL,
	"tahun" TEXT,
	"kode_rka" TEXT,
	"status_hapus" BOOLEAN,
	"jalan" TEXT,
	"gang" TEXT,
	"nomor" TEXT,
	"rw" TEXT,
	"rt" TEXT,
	"keterangan" TEXT,
	"tempat" TEXT,
	"komponen" TEXT,
	"kecamatan" TEXT,
	"kelurahan" TEXT,
	"lokasi" TEXT,
	PRIMARY KEY ("id_history")
);

COMMENT ON TABLE history_pekerjaan_v2 IS '';


SET search_path TO public;