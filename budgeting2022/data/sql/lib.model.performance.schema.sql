
-----------------------------------------------------------------------------
-- eperformance.pegawai
-----------------------------------------------------------------------------

DROP TABLE eperformance.pegawai CASCADE;


CREATE TABLE eperformance.pegawai
(
	"id" INTEGER  NOT NULL,
	"nip" VARCHAR(30),
	"nama" VARCHAR(60),
	"golongan" VARCHAR(50),
	"tmt" DATE,
	"pendidikan_terakhir" VARCHAR(256),
	"skpd_id" INTEGER,
	"atasan_id" INTEGER,
	"is_atasan" BOOLEAN default 'f',
	"level_struktural" INT2,
	"password" VARCHAR(36),
	"user_id" INTEGER,
	"jumlah_anak_buah" INTEGER,
	"nip_lama" VARCHAR(30),
	"status" INTEGER default 0 NOT NULL,
	"created_at" TIMESTAMP,
	"deleted_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"username_eproject" VARCHAR(255),
	"id_fp" INTEGER,
	"is_operasional" BOOLEAN,
	"tgl_masuk" DATE,
	"is_out" BOOLEAN default 'f',
	"jabatan_id" INTEGER,
	"tgl_keluar" DATE,
	"bobot_jabatan" NUMERIC,
	"is_ka_uptd" BOOLEAN default 'f' NOT NULL,
	"is_struktural" BOOLEAN default 'f' NOT NULL,
	"is_kunci_aktifitas" BOOLEAN default 't',
	"lock_capaian_indikator" BOOLEAN default 'f',
	"pelayanan_kesekretariatan" INT2 default 100 NOT NULL,
	"jabatan_struktural_id" INTEGER,
	"jabatan_lain" TEXT,
	"is_only_skp" BOOLEAN default 'f' NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE eperformance.pegawai IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- eperformance.jabatan_struktural
-----------------------------------------------------------------------------

DROP TABLE eperformance.jabatan_struktural CASCADE;


CREATE TABLE eperformance.jabatan_struktural
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR,
	"eselon" VARCHAR(5),
	"jenis_indikator_kinerja" INT2,
	"skpd_id" INTEGER,
	"nomer" INT2,
	"is_asisten" BOOLEAN default 'f' NOT NULL,
	"nomer_urut" INT2,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE eperformance.jabatan_struktural IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- eperformance.master_skpd
-----------------------------------------------------------------------------

DROP TABLE eperformance.master_skpd CASCADE;


CREATE TABLE eperformance.master_skpd
(
	"skpd_id" INTEGER  NOT NULL,
	"skpd_kode" VARCHAR(8)  NOT NULL,
	PRIMARY KEY ("skpd_id")
);

COMMENT ON TABLE eperformance.master_skpd IS '';


SET search_path TO public;