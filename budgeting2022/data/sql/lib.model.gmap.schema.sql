
-----------------------------------------------------------------------------
-- geodata_road
-----------------------------------------------------------------------------

DROP TABLE geodata_road CASCADE;


CREATE TABLE geodata_road
(
	"id" INTEGER  NOT NULL,
	"nama" VARCHAR(500),
	"latlong" VARCHAR,
	"tipe" INTEGER,
	"geojson" VARCHAR,
	"tanggal" TIMESTAMP,
	"deleteflag" INTEGER,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE geodata_road IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- geojsonlokasi
-----------------------------------------------------------------------------

DROP TABLE geojsonlokasi CASCADE;


CREATE TABLE geojsonlokasi
(
	"kode_lokasi" VARCHAR(50)  NOT NULL,
	"geojson" VARCHAR  NOT NULL,
	"tipegeografis" VARCHAR(50),
	"tipelokasi" VARCHAR(100),
	"iduser" VARCHAR(100),
	PRIMARY KEY ("kode_lokasi")
);

COMMENT ON TABLE geojsonlokasi IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- loggmap
-----------------------------------------------------------------------------

DROP TABLE loggmap CASCADE;


CREATE TABLE loggmap
(
	"idlog" INTEGER  NOT NULL,
	"kode_lokasi" VARCHAR(500),
	"iduser" VARCHAR(50),
	"datalama" VARCHAR,
	"databaru" VARCHAR,
	"tanggal" TIMESTAMP,
	"status" INTEGER,
	"ipaddress" VARCHAR,
	"forwardedipaddress" VARCHAR,
	PRIMARY KEY ("idlog")
);

COMMENT ON TABLE loggmap IS '';


SET search_path TO public;