
-----------------------------------------------------------------------------
-- ebudget.bappeko_rka_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rka_member CASCADE;


CREATE TABLE ebudget.bappeko_rka_member
(
	"kode_sub" VARCHAR(30)  NOT NULL,
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"komponen_id" VARCHAR(50),
	"komponen_name" VARCHAR(300),
	"detail_name" VARCHAR(200),
	"rekening_asli" VARCHAR(30),
	"tahun" VARCHAR(5),
	"detail_no" INT2  NOT NULL,
	PRIMARY KEY ("kode_sub")
);

COMMENT ON TABLE ebudget.bappeko_rka_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_master_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_master_kegiatan CASCADE;

DROP SEQUENCE ebudget.bappeko_master_seq_14;

CREATE SEQUENCE ebudget.bappeko_master_seq_14;


CREATE TABLE ebudget.bappeko_master_kegiatan
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kode_kegiatan" VARCHAR(20)  NOT NULL,
	"kode_bidang" VARCHAR(2),
	"kode_urusan_wajib" VARCHAR(2),
	"kode_program" VARCHAR(2),
	"kode_sasaran" VARCHAR(2),
	"kode_indikator" VARCHAR(2),
	"alokasi_dana" DOUBLE PRECISION,
	"nama_kegiatan" VARCHAR,
	"masukan" VARCHAR,
	"output" VARCHAR,
	"outcome" VARCHAR,
	"benefit" VARCHAR,
	"impact" VARCHAR,
	"tipe" VARCHAR(10),
	"kegiatan_active" BOOLEAN default 'f',
	"to_kegiatan_code" VARCHAR(12),
	"catatan" VARCHAR,
	"target_outcome" VARCHAR,
	"lokasi" VARCHAR,
	"jumlah_prev" DOUBLE PRECISION,
	"jumlah_now" DOUBLE PRECISION,
	"jumlah_next" DOUBLE PRECISION,
	"kode_program2" VARCHAR(30),
	"kode_urusan" VARCHAR(10),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"kode_misi" VARCHAR(2),
	"kode_tujuan" VARCHAR(2),
	"ranking" INT2,
	"nomor13" VARCHAR(4),
	"ppa_nama" VARCHAR(200),
	"ppa_pangkat" VARCHAR(200),
	"ppa_nip" VARCHAR(30),
	"lanjutan" BOOLEAN,
	"user_id" VARCHAR(100),
	"id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"tambahan_pagu" DOUBLE PRECISION,
	"gender" BOOLEAN default 'f',
	"kode_keg_keuangan" VARCHAR(15),
	PRIMARY KEY ("unit_id","kode_kegiatan","id")
);

COMMENT ON TABLE ebudget.bappeko_master_kegiatan IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_rincian
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rincian CASCADE;


CREATE TABLE ebudget.bappeko_rincian
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"rincian_confirmed" BOOLEAN default 'f',
	"rincian_changed" BOOLEAN default 'f',
	"rincian_selesai" BOOLEAN default 'f',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"target" VARCHAR,
	"unit_id" VARCHAR(8)  NOT NULL,
	"rincian_level" INT2 default 1,
	"lock" BOOLEAN default 'f',
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahun" VARCHAR(5),
	PRIMARY KEY ("kegiatan_code","tipe","unit_id")
);

COMMENT ON TABLE ebudget.bappeko_rincian IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_subtitle_indikator
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_subtitle_indikator CASCADE;

DROP SEQUENCE ebudget.bappeko_subtit_seq_15;

CREATE SEQUENCE ebudget.bappeko_subtit_seq_15;


CREATE TABLE ebudget.bappeko_subtitle_indikator
(
	"unit_id" VARCHAR(10),
	"kegiatan_code" VARCHAR(12),
	"subtitle" VARCHAR(250),
	"indikator" VARCHAR(250),
	"nilai" DOUBLE PRECISION,
	"satuan" VARCHAR(50),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR,
	"sub_id" INTEGER  NOT NULL,
	"tahun" VARCHAR(5),
	"lock_subtitle" BOOLEAN default 'f',
	"prioritas" INTEGER default 0,
	"catatan" VARCHAR,
	"lakilaki" INTEGER,
	"perempuan" INTEGER,
	"dewasa" INTEGER,
	"anak" INTEGER,
	"lansia" INTEGER,
	"inklusi" INTEGER,
	"gender" BOOLEAN default 'f',
	PRIMARY KEY ("sub_id")
);

COMMENT ON TABLE ebudget.bappeko_subtitle_indikator IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_rincian_detail
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rincian_detail CASCADE;


CREATE TABLE ebudget.bappeko_rincian_detail
(
	"kegiatan_code" VARCHAR(20)  NOT NULL,
	"tipe" VARCHAR(5)  NOT NULL,
	"detail_no" INT2  NOT NULL,
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"detail_name" TEXT,
	"volume" DOUBLE PRECISION,
	"keterangan_koefisien" VARCHAR,
	"subtitle" VARCHAR(300),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_harga_awal" DOUBLE PRECISION,
	"komponen_name" VARCHAR(500),
	"satuan" VARCHAR(50),
	"pajak" INTEGER default 0,
	"unit_id" VARCHAR(8)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(50),
	"sub" VARCHAR(500),
	"kode_sub" VARCHAR(30),
	"last_update_user" VARCHAR(100),
	"last_update_time" TIMESTAMP,
	"last_update_ip" VARCHAR(20),
	"tahap" VARCHAR(20),
	"tahap_edit" VARCHAR(20),
	"tahap_new" VARCHAR(20),
	"status_lelang" VARCHAR(30),
	"nomor_lelang" VARCHAR(300),
	"koefisien_semula" VARCHAR(100),
	"volume_semula" INTEGER,
	"harga_semula" DOUBLE PRECISION,
	"total_semula" DOUBLE PRECISION,
	"lock_subtitle" VARCHAR(5),
	"status_hapus" BOOLEAN default 'f',
	"tahun" VARCHAR(5),
	"kode_lokasi" VARCHAR(20),
	"kecamatan" VARCHAR(150),
	"rekening_code_asli" VARCHAR(100),
	"note_skpd" VARCHAR(500),
	"note_peneliti" VARCHAR(500),
	PRIMARY KEY ("kegiatan_code","tipe","detail_no","unit_id")
);

COMMENT ON TABLE ebudget.bappeko_rincian_detail IS '';


SET search_path TO public;
CREATE INDEX "rincian_index" ON ebudget.bappeko_rincian_detail ("kegiatan_code","unit_id","detail_no","subtitle");

-----------------------------------------------------------------------------
-- ebudget.bappeko_rincian_sub_parameter
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_rincian_sub_parameter CASCADE;


CREATE TABLE ebudget.bappeko_rincian_sub_parameter
(
	"unit_id" VARCHAR(10)  NOT NULL,
	"kegiatan_code" VARCHAR(12)  NOT NULL,
	"from_sub_kegiatan" VARCHAR(20)  NOT NULL,
	"sub_kegiatan_name" VARCHAR(300)  NOT NULL,
	"subtitle" VARCHAR(200),
	"detail_name" VARCHAR(200),
	"new_subtitle" VARCHAR(400),
	"param" VARCHAR(400),
	"kecamatan" VARCHAR(100),
	"max_nilai" DOUBLE PRECISION,
	"keterangan" VARCHAR,
	"ket_pembagi" VARCHAR,
	"pembagi" DOUBLE PRECISION,
	"kode_sub" VARCHAR(30),
	"tahun" VARCHAR(5)
);

COMMENT ON TABLE ebudget.bappeko_rincian_sub_parameter IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_kategori_shsd
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_kategori_shsd CASCADE;


CREATE TABLE ebudget.bappeko_kategori_shsd
(
	"kategori_shsd_id" VARCHAR(50)  NOT NULL,
	"kategori_shsd_name" VARCHAR,
	"rekening_code" VARCHAR(50),
	"rekening" VARCHAR
);

COMMENT ON TABLE ebudget.bappeko_kategori_shsd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_komponen
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_komponen CASCADE;


CREATE TABLE ebudget.bappeko_komponen
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	"status_masuk" VARCHAR(10),
	"rka_member" BOOLEAN,
	"maintenance" BOOLEAN,
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.bappeko_komponen IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_asb_fisik
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_asb_fisik CASCADE;


CREATE TABLE ebudget.bappeko_asb_fisik
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"komponen_name" VARCHAR,
	"shsd_id" VARCHAR(50),
	"komponen_harga" DOUBLE PRECISION,
	"komponen_show" BOOLEAN default 't',
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"komponen_tipe" VARCHAR(10),
	"komponen_confirmed" BOOLEAN default 'f',
	"komponen_non_pajak" BOOLEAN default 'f',
	"user_id" VARCHAR(30),
	"rekening" VARCHAR(300),
	"kelompok" VARCHAR(100),
	"pemeliharaan" INTEGER default 0,
	"rek_upah" VARCHAR(20),
	"rek_bahan" VARCHAR(20),
	"rek_sewa" VARCHAR(20),
	"deskripsi" VARCHAR,
	"asb_locked" BOOLEAN default 'f',
	PRIMARY KEY ("komponen_id")
);

COMMENT ON TABLE ebudget.bappeko_asb_fisik IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_komponen_member
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_komponen_member CASCADE;


CREATE TABLE ebudget.bappeko_komponen_member
(
	"komponen_id" VARCHAR(50)  NOT NULL,
	"member_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"member_name" VARCHAR,
	"member_harga" DOUBLE PRECISION,
	"koefisien" DOUBLE PRECISION,
	"member_total" DOUBLE PRECISION,
	"ip_address" VARCHAR(20),
	"waktu_access" TIMESTAMP,
	"member_tipe" VARCHAR(10),
	"subtitle" VARCHAR(250),
	"member_no" DOUBLE PRECISION,
	"tipe" VARCHAR(40),
	"komponen_tipe" VARCHAR(10),
	"from_id" VARCHAR(30),
	"from_koef" DOUBLE PRECISION,
	"hspk_name" VARCHAR(300)
);

COMMENT ON TABLE ebudget.bappeko_komponen_member IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_komponen_rekening
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_komponen_rekening CASCADE;


CREATE TABLE ebudget.bappeko_komponen_rekening
(
	"rekening_code" VARCHAR(100),
	"komponen_id" VARCHAR(50),
	"rekening_code29" VARCHAR(15)
);

COMMENT ON TABLE ebudget.bappeko_komponen_rekening IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_shsd
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_shsd CASCADE;


CREATE TABLE ebudget.bappeko_shsd
(
	"shsd_id" VARCHAR(50)  NOT NULL,
	"satuan" VARCHAR(100),
	"shsd_name" VARCHAR,
	"shsd_harga" DOUBLE PRECISION,
	"shsd_harga_dasar" DOUBLE PRECISION,
	"shsd_show" BOOLEAN,
	"shsd_faktor_inflasi" NUMERIC(6,4),
	"shsd_locked" BOOLEAN default 'f',
	"rekening_code" VARCHAR(300),
	"shsd_merk" VARCHAR(250),
	"non_pajak" BOOLEAN default 'f',
	"shsd_id_old" VARCHAR(30),
	"nama_dasar" VARCHAR(300),
	"spec" VARCHAR(500),
	"hidden_spec" VARCHAR(500),
	PRIMARY KEY ("shsd_id")
);

COMMENT ON TABLE ebudget.bappeko_shsd IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ebudget.bappeko_sub_kegiatan
-----------------------------------------------------------------------------

DROP TABLE ebudget.bappeko_sub_kegiatan CASCADE;


CREATE TABLE ebudget.bappeko_sub_kegiatan
(
	"sub_kegiatan_id" VARCHAR(50)  NOT NULL,
	"sub_kegiatan_name" VARCHAR,
	"param" VARCHAR,
	"satuan" VARCHAR,
	"status" VARCHAR(15) default 'Open',
	"pembagi" VARCHAR,
	"keterangan" VARCHAR,
	"unit_id" VARCHAR(20),
	"penelitian_skala" VARCHAR(10),
	"penelitian_waktu" INT2,
	"batas_nilai" INT8,
	"tenaga_ahli" INT2,
	PRIMARY KEY ("sub_kegiatan_id")
);

COMMENT ON TABLE ebudget.bappeko_sub_kegiatan IS '';


SET search_path TO public;