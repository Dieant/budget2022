
-----------------------------------------------------------------------------
-- simbada
-----------------------------------------------------------------------------

DROP TABLE simbada CASCADE;


CREATE TABLE simbada
(
	"id" VARCHAR(20)  NOT NULL,
	"tipe_kib" VARCHAR(50),
	"unit_id" VARCHAR(5),
	"kode_lokasi" VARCHAR(50),
	"nama_lokasi" VARCHAR(500),
	"no_register" VARCHAR(50),
	"kode_barang" VARCHAR(50),
	"nama_barang" VARCHAR(500),
	"kondisi" VARCHAR(10),
	"merk" VARCHAR(200),
	"tipe" VARCHAR(200),
	"alamat" VARCHAR(500),
	"keterangan" VARCHAR(200),
	"tahun" VARCHAR(5),
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"deleted_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE simbada IS '';


SET search_path TO public;