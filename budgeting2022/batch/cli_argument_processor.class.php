<?php

class cli_argument_processor
{
  private $replacements = array();
  private $arguments = array();
  private static $instance = null;  
  
  private function __construct()
  {}
  
  public static function Instance()
  {
    if(self::$instance===null) {
      $class = __CLASS__;      
      self::$instance = new $class();
    }
    return self::$instance;
  }
  
  public function addReplacement($arg, $replacement = null, $default = null)
  {
    if(is_array($arg))
      $this->replacements = array_merge($this->replacements, $arg);
    else
    {
      $this->replacements[$arg] = $replacement;
      if($default !== null) $this->arguments[$replacement] = $default;
    }
  }
  
  public function replace($arg, $replacement = null, $default = null)
  {
    $this->addReplacement($arg, $replacement, $default);    
  }
  
  public function set_default($key, $value)
  {
    $this->arguments[$key] = $value;
  }
  
  public function getReplacement($arg, $default = null)
  {
    return isset($this->replacements[$arg]) ?
      $this->replacements[$arg] :
      ($default === null ? $arg : $default);
  }
  
  function Parse()
  {
    $pattern1 = "/--(\w+)=?(\w*|\d*)/"; /// --argument=value
    $pattern2 = "/-(\w+)/";  /// -arg value  || -arg
    
    $arguments = $this->arguments;
    $argc = $_SERVER['argc'];
    $argv = $_SERVER['argv'];
    
    $k = 0;
    for($i=1;$i<$argc;$i++)
    {
      if(preg_match($pattern1, $argv[$i], $matches))
      {
        $arguments[$matches[1]] = $matches[2];
      }
      elseif(preg_match($pattern2, $argv[$i], $matches))
      {
        if(($j=$i+1)>=$argc || ($j<$argc && $argv[$j][0] == '-'))
        {
          $arguments[$this->getReplacement($matches[1])] = true;
        } 
        else
        {
          $arguments[$this->getReplacement($matches[1])] = $argv[++$i];
        }
      }
      else
      {
        $key = $this->getReplacement($k);
        $arguments[$key] = $argv[$i];
        if($key == $k) $k++; 
      }
    }
    
    $this->arguments = $arguments;
    return $arguments;
  } 
}

$cli = cli_argument_processor::Instance();