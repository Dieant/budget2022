<?php

require_once('cli_argument_processor.class.php');

$arguments = cli_argument_processor::Instance()->Parse();
extract($arguments);

define('SF_ROOT_DIR',    realpath(dirname(__file__).'/..'));
define('SF_APP',         isset($arguments['app']) ? $arguments['app'] : 'budgeting');

if(!defined('SF_ENVIRONMENT'))
  if(isset($arguments['env']))  
    define('SF_ENVIRONMENT', $arguments['env']);
  else
    define('SF_ENVIRONMENT', 'prod');
if(SF_ENVIRONMENT != 'prod')  
  define('SF_DEBUG',       true);
else
  define('SF_DEBUG',       false);

require_once(SF_ROOT_DIR.DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR.SF_APP.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'config.php');

//initialize database manager
$databaseManager = new sfDatabaseManager();
$databaseManager->initialize();
