<?php
require('cli_argument_processor.class.php');
require('batch_header_dev.php');

list($d, $m, $y, $H, $mn, $s) = array(date("d"), date("m"), date("Y"), date("H"), date("i"), date("s"));
$tgl_now = "$d-$m-$y $H:$mn:$s";

$tgl_for_file_name = 'testing-'."$d-$m-$y-$H:$mn:$s";
$file_name = dirname(__FILE__)."/../log/log_simbada/{$tgl_for_file_name}.log";
$fh = fopen($file_name, 'a') or die("$php_errormsg");
fwrite($fh, "\n\n");
fwrite($fh, "testing {$tgl_now}\n");
fwrite($fh, "############################## END ############################# \n");
fwrite($fh, "################################################################ \n");
fclose($fh) or die("Can't close $file: $php_errormsg");
?>
