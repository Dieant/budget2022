<?php

/*
 * created by Bisma
 * Menarik data dari simbada.surabaya.go.id
 */

require('cli_argument_processor.class.php');
require('batch_header_dev.php');

list($d, $m, $y, $H, $mn, $s) = array(date("d"), date("m"), date("Y"), date("H"), date("i"), date("s"));
$tgl_now = "$d-$m-$y $H:$mn:$s";

$tgl_for_file_name = 'simbada-'."$d-$m-$y-$H:$mn:$s";
$file_name = dirname(__FILE__)."/../log/log_simbada/{$tgl_for_file_name}.log";
$fh = fopen($file_name, 'a') or die("$php_errormsg");

$log_info = true;
//$tanggal = 0;

ini_set('memory_limit', '2048M');
fwrite($fh, "\n\n");
fwrite($fh, "############################## LETS BEGIN TO SYNC SIMBADA TO BUDGETING ######################### \n");
fwrite($fh, "[[[Tanggal: {$tgl_now}]]] \n");

##delete table firs; 
fwrite($fh, "--- Proses Menghapus tabel KIB  --- \n");
$queryDelete="delete from budget2012.kib";
        //print_r($query);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($queryDelete);
        $rs_skpd = $stmt->executeQuery();
##end of delete table;
fwrite($fh, "--- Proses Menghapus tabel KIB SUKSES  --- \n\n\n");
$c_skpd = new Criteria();

/*
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'1000',Criteria::NOT_EQUAL);
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'0500',Criteria::NOT_EQUAL);
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'0307',Criteria::NOT_EQUAL);
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'0100',Criteria::NOT_EQUAL);
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'9999',Criteria::NOT_EQUAL);
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'8888',Criteria::NOT_EQUAL);*/
$c_skpd->add(UnitKerjaPeer::UNIT_ID,'0305');
$rs_skpd= UnitKerjaPeer::doSelect($c_skpd);

$query="select * from unit_kerja
        where unit_id not in ('1000','0500','0307','0100','9999','8888')";
        //print_r($query);exit;
        $con = Propel::getConnection();
        $stmt = $con->prepareStatement($query);
        $rs_skpd = $stmt->executeQuery();
while ($rs_skpd->next())
{
  $unit_id = $rs_skpd->getString('unit_id');
#KIB B
  
  fwrite($fh, "--- Proses Tarik data KIB B  --- \n");
  echo "#################### Kelompok Inventari Barang (KIB) : B  ################### \n";
  echo "#################### SKPD ID: {$unit_id}  ############################## \n";
  fwrite($fh, "--- SKPD : {$unit_id} - Nama SKPD :  {$rs_skpd->getString('unit_name')}--- \n");
  $url="http://simbada.surabaya.go.id/unit/xml_kibb.php?user=".$unit_id;
  $dom=new DOMDocument();
  $dom->load($url);
  if(!$dom){
     echo 'Error while parsing the document';
     fwrite($fh, "--- Terjadi error : Error while parsing the document --- \n");
     exit;
  }
  $xml_unit_kerja=$dom->getElementsByTagName("unit_kerja");
  
  foreach($xml_unit_kerja as $xml_skpd){
      //print_r($xml_skpd->getAttribute("nama_unit"));exit;
      fwrite($fh, "Nama SKPD : ".$xml_skpd->getAttribute("nama_unit")."\n");
      $xml_contents=$xml_skpd->getElementsByTagName("content");
      foreach($xml_contents as $xml_content){
            $con = Propel::getConnection();
            try {
                    $con->begin();
                    $kib = new Kib();
                    $kib->setId("B".$xml_content->getAttribute("id_barang"));
                    $kib->setTipeKib($xml_skpd->getAttribute("aset"));
                    $kib->setUnitId($xml_skpd->getAttribute("unit_id"));
                    $kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setNamaLokasi($xml_content->getAttribute("lokasi"));
                    $kib->setNoRegister($xml_content->getAttribute("no_register"));
                    //$kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setKodeBarang($xml_content->getAttribute("kode_barang"));
                    $kib->setNamaBarang($xml_content->getAttribute("nama_barang"));
                    $kib->setKondisi($xml_content->getAttribute("baik")."-".$xml_content->getAttribute("kb")."-".$xml_content->getAttribute("rb"));
                    $kib->setMerk($xml_content->getAttribute("merk_alamat"));
                    $kib->setTipe($xml_content->getAttribute("tipe"));
                    $kib->setAlamat($xml_content->getAttribute("merk_alamat"));
                    $kib->setKeterangan($xml_content->getAttribute("nopol"));
                    $kib->setTahun($xml_content->getAttribute("thn_pengadaan"));
                    $kib->save($con);
                    $con->commit();
                    fwrite($fh, "Sukses disimpan unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
            } catch (PropelException $e) {
                    fwrite($fh, "Terjadi error : ".$e." unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
                    echo "################ Terjadi Error : {$e}  ################ \n";
                    $con->rollback();
                    throw $e;
            }
      }
      
  }
  fwrite($fh, "--- Akhir dari Proses Tarik data KIB B pada SKPD : {$unit_id} --- \n\n\n");
  
#END OF KIB B

  
#KIB C
  fwrite($fh, "--- Proses Tarik data KIB C  --- \n");
  echo "#################### Kelompok Inventari Barang (KIB) : C  ################### \n";
  echo "#################### SKPD ID: {$unit_id}  ############################## \n";
  fwrite($fh, "--- SKPD : {$unit_id} - Nama SKPD :  {$rs_skpd->getString('unit_name')}--- \n");
  $url="http://simbada.surabaya.go.id/unit/xml_kibc.php?user=".$unit_id;
  $dom=new DOMDocument();
  $dom->load($url);
  if(!$dom){
     echo 'Error while parsing the document';
     fwrite($fh, "--- Terjadi error : Error while parsing the document --- \n");
     exit;
  }
  $xml_unit_kerja=$dom->getElementsByTagName("unit_kerja");
  
  foreach($xml_unit_kerja as $xml_skpd){
      //print_r($xml_skpd->getAttribute("nama_unit"));exit;
      fwrite($fh, "Nama SKPD : ".$xml_skpd->getAttribute("nama_unit")."\n");
      $xml_contents=$xml_skpd->getElementsByTagName("content");
      foreach($xml_contents as $xml_content){
            $con = Propel::getConnection();
            try {
                    $con->begin();
                    $kib = new Kib();
                    $kib->setId("C".$xml_content->getAttribute("id_barang"));
                    $kib->setTipeKib($xml_skpd->getAttribute("aset"));
                    $kib->setUnitId($xml_skpd->getAttribute("unit_id"));
                    $kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setNamaLokasi($xml_content->getAttribute("lokasi"));
                    $kib->setNoRegister($xml_content->getAttribute("no_register"));
                    //$kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setKodeBarang($xml_content->getAttribute("kode_barang"));
                    $kib->setNamaBarang($xml_content->getAttribute("nama_barang"));
                    $kib->setKondisi($xml_content->getAttribute("baik")."-".$xml_content->getAttribute("kb")."-".$xml_content->getAttribute("rb"));
                    $kib->setMerk($xml_content->getAttribute("merk_alamat"));
                    $kib->setTipe($xml_content->getAttribute("tipe"));
                    $kib->setAlamat($xml_content->getAttribute("merk_alamat"));
                    $kib->setKeterangan($xml_content->getAttribute("keterangan"));
                    $kib->setTahun($xml_content->getAttribute("thn_pengadaan"));
                    $kib->save($con);
                    $con->commit();
                    fwrite($fh, "Sukses disimpan unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
            } catch (PropelException $e) {
                    fwrite($fh, "Terjadi error : ".$e." unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
                    echo "################ Terjadi Error : {$e}  ################ \n";
                    $con->rollback();
                    throw $e;
            }
      }
      
  }
  fwrite($fh, "--- Akhir dari Proses Tarik data KIB C pada SKPD : {$unit_id} --- \n\n\n");
  
#END OF KIB C

  
#KIB D
  fwrite($fh, "--- Proses Tarik data KIB D  --- \n");
  echo "#################### Kelompok Inventari Barang (KIB) : D  ################### \n";
  echo "#################### SKPD ID: {$unit_id}  ############################## \n";
  fwrite($fh, "--- SKPD : {$unit_id} - Nama SKPD :  {$rs_skpd->getString('unit_name')}--- \n");
  $url="http://simbada.surabaya.go.id/unit/xml_kibd.php?user=".$unit_id;
  $dom=new DOMDocument();
  $dom->load($url);
  if(!$dom){
     echo 'Error while parsing the document';
     fwrite($fh, "--- Terjadi error : Error while parsing the document --- \n");
     exit;
  }
  $xml_unit_kerja=$dom->getElementsByTagName("unit_kerja");
  
  foreach($xml_unit_kerja as $xml_skpd){
      //print_r($xml_skpd->getAttribute("nama_unit"));exit;
      fwrite($fh, "Nama SKPD : ".$xml_skpd->getAttribute("nama_unit")."\n");
      $xml_contents=$xml_skpd->getElementsByTagName("content");
      foreach($xml_contents as $xml_content){
            $con = Propel::getConnection();
            try {
                    $con->begin();
                    $kib = new Kib();
                    $kib->setId("D".$xml_content->getAttribute("id_barang"));
                    $kib->setTipeKib($xml_skpd->getAttribute("aset"));
                    $kib->setUnitId($xml_skpd->getAttribute("unit_id"));
                    $kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setNamaLokasi($xml_content->getAttribute("lokasi"));
                    $kib->setNoRegister($xml_content->getAttribute("no_register"));
                    //$kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setKodeBarang($xml_content->getAttribute("kode_barang"));
                    $kib->setNamaBarang($xml_content->getAttribute("nama_barang"));
                    $kib->setKondisi($xml_content->getAttribute("baik")."-".$xml_content->getAttribute("kb")."-".$xml_content->getAttribute("rb"));
                    $kib->setMerk($xml_content->getAttribute("merk_alamat"));
                    $kib->setTipe($xml_content->getAttribute("tipe"));
                    $kib->setAlamat($xml_content->getAttribute("merk_alamat"));
                    $kib->setKeterangan($xml_content->getAttribute("keterangan"));
                    $kib->setTahun($xml_content->getAttribute("thn_pengadaan"));
                    $kib->save($con);
                    $con->commit();
                    fwrite($fh, "Sukses disimpan unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
            } catch (PropelException $e) {
                    fwrite($fh, "Terjadi error : ".$e." unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
                    echo "################ Terjadi Error : {$e}  ################ \n";
                    $con->rollback();
                    throw $e;
            }
      }
      
  }
  fwrite($fh, "--- Akhir dari Proses Tarik data KIB D pada SKPD : {$unit_id} --- \n\n\n");
  
#END OF KIB D

#KIB E
  fwrite($fh, "--- Proses Tarik data KIB E  --- \n");
  echo "#################### Kelompok Inventari Barang (KIB) : E  ################### \n";
  echo "#################### SKPD ID: {$unit_id}  ############################## \n";
  fwrite($fh, "--- SKPD : {$unit_id} - Nama SKPD :  {$rs_skpd->getString('unit_name')}--- \n");
  $url="http://simbada.surabaya.go.id/unit/xml_kibe.php?user=".$unit_id;
  $dom=new DOMDocument();
  $dom->load($url);
  if(!$dom){
     echo 'Error while parsing the document';
     fwrite($fh, "--- Terjadi error : Error while parsing the document --- \n");
     exit;
  }
  $xml_unit_kerja=$dom->getElementsByTagName("unit_kerja");
  
  foreach($xml_unit_kerja as $xml_skpd){
      //print_r($xml_skpd->getAttribute("nama_unit"));exit;
      fwrite($fh, "Nama SKPD : ".$xml_skpd->getAttribute("nama_unit")."\n");
      $xml_contents=$xml_skpd->getElementsByTagName("content");
      foreach($xml_contents as $xml_content){
            $con = Propel::getConnection();
            try {
                    $con->begin();
                    $kib = new Kib();
                    $kib->setId("E".$xml_content->getAttribute("id_barang"));
                    $kib->setTipeKib($xml_skpd->getAttribute("aset"));
                    $kib->setUnitId($xml_skpd->getAttribute("unit_id"));
                    $kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setNamaLokasi($xml_content->getAttribute("lokasi"));
                    $kib->setNoRegister($xml_content->getAttribute("no_register"));
                    //$kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setKodeBarang($xml_content->getAttribute("kode_barang"));
                    $kib->setNamaBarang($xml_content->getAttribute("nama_barang"));
                    $kib->setKondisi($xml_content->getAttribute("baik")."-".$xml_content->getAttribute("kb")."-".$xml_content->getAttribute("rb"));
                    $kib->setMerk($xml_content->getAttribute("merk_alamat"));
                    $kib->setTipe($xml_content->getAttribute("tipe"));
                    $kib->setAlamat($xml_content->getAttribute("merk_alamat"));
                    $kib->setKeterangan($xml_content->getAttribute("keterangan"));
                    $kib->setTahun($xml_content->getAttribute("thn_pengadaan"));
                    $kib->save($con);
                    $con->commit();
                    fwrite($fh, "Sukses disimpan unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
            } catch (PropelException $e) {
                    fwrite($fh, "Terjadi error : ".$e." unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
                    echo "################ Terjadi Error : {$e}  ################ \n";
                    $con->rollback();
                    throw $e;
            }
      }
      
  }
  fwrite($fh, "--- Akhir dari Proses Tarik data KIB E pada SKPD : {$unit_id} --- \n\n\n");
  
#END OF KIB E

  
#KIB G
  fwrite($fh, "--- Proses Tarik data KIB G  --- \n");
  echo "#################### Kelompok Inventari Barang (KIB) : G  ################### \n";
  echo "#################### SKPD ID: {$unit_id}  ############################## \n";
  fwrite($fh, "--- SKPD : {$unit_id} - Nama SKPD :  {$rs_skpd->getString('unit_name')}--- \n");
  $url="http://simbada.surabaya.go.id/unit/xml_kibg.php?user=".$unit_id;
  $dom=new DOMDocument();
  $dom->load($url);
  if(!$dom){
     echo 'Error while parsing the document';
     fwrite($fh, "--- Terjadi error : Error while parsing the document --- \n");
     exit;
  }
  $xml_unit_kerja=$dom->getElementsByTagName("unit_kerja");
  
  foreach($xml_unit_kerja as $xml_skpd){
      //print_r($xml_skpd->getAttribute("nama_unit"));exit;
      fwrite($fh, "Nama SKPD : ".$xml_skpd->getAttribute("nama_unit")."\n");
      $xml_contents=$xml_skpd->getElementsByTagName("content");
      foreach($xml_contents as $xml_content){
            $con = Propel::getConnection();
            try {
                    $con->begin();
                    $kib = new Kib();
                    $kib->setId("G".$xml_content->getAttribute("id_barang"));
                    $kib->setTipeKib($xml_skpd->getAttribute("aset"));
                    $kib->setUnitId($xml_skpd->getAttribute("unit_id"));
                    $kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setNamaLokasi($xml_content->getAttribute("lokasi"));
                    $kib->setNoRegister($xml_content->getAttribute("no_register"));
                    //$kib->setKodeLokasi($xml_content->getAttribute("no_lokasi"));
                    $kib->setKodeBarang($xml_content->getAttribute("kode_barang"));
                    $kib->setNamaBarang($xml_content->getAttribute("nama_barang"));
                    $kib->setKondisi($xml_content->getAttribute("baik")."-".$xml_content->getAttribute("kb")."-".$xml_content->getAttribute("rb"));
                    $kib->setMerk($xml_content->getAttribute("merk_alamat"));
                    $kib->setTipe($xml_content->getAttribute("tipe"));
                    $kib->setAlamat($xml_content->getAttribute("merk_alamat"));
                    $kib->setKeterangan($xml_content->getAttribute("keterangan"));
                    $kib->setTahun($xml_content->getAttribute("thn_pengadaan"));
                    $kib->save($con);
                    $con->commit();
                    fwrite($fh, "Sukses disimpan unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
            } catch (PropelException $e) {
                    fwrite($fh, "Terjadi error : ".$e." unit_id : ".$xml_skpd->getAttribute("unit_id")." ID :".$xml_content->getAttribute("bidang_barang").$xml_content->getAttribute("id_barang")."\n");
                    echo "################ Terjadi Error : {$e}  ################ \n";
                    $con->rollback();
                    throw $e;
            }
      }
      
  }
  fwrite($fh, "--- Akhir dari Proses Tarik data KIB G pada SKPD : {$unit_id} --- \n\n\n");
  
#END OF KIB G  
}
fwrite($fh, "########## END ########## \n");
fwrite($fh, "################################################################# \n");
$tgl_selesai = "$d-$m-$y $H:$mn:$s";
fwrite($fh, "data telah berhasil ditarik pada {$tgl_selesai} \n");
fclose($fh) or die("Can't close $file: $php_errormsg");

ini_restore('memory_limit');
?>
