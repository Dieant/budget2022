<?php

/*
 * created by Bisma
 * Menarik data dari musrenbang milik bappeko
 */

require('cli_argument_processor.class.php');
require('batch_header_dev.php');

list($d, $m, $y, $H, $mn, $s) = array(date("d"), date("m"), date("Y"), date("H"), date("i"), date("s"));
$tgl_now = "$d-$m-$y $H:$mn:$s";

$tgl_for_file_name = 'musrenbang-'."$d-$m-$y-$H:$mn:$s";
$file_name = dirname(__FILE__)."/../log/log_musrenbang/{$tgl_for_file_name}.log";
$fh = fopen($file_name, 'a') or die("$php_errormsg");

$log_info = true;
//$tanggal = 0;

ini_set('memory_limit', '2048M');
fwrite($fh, "\n\n");
fwrite($fh, "############################## LETS BEGIN TO SYNC MUSRENBANG TO BUDGETING ######################### \n");
fwrite($fh, "[[[Tanggal: {$tgl_now}]]] \n");


  fwrite($fh, "--- Proses Tarik Musrenbang tipe Fisik  --- \n");
  fwrite($fh, "--- SKPD : {$unit_id} - Nama SKPD :  {$rs_skpd->getString('unit_name')}--- \n");
  $url="http://bappeko.surabaya.go.id/create_xml/";
  $dom=new DOMDocument();
  $dom->load($url);
  if(!$dom){
     echo 'Error while parsing the document';
     fwrite($fh, "--- Terjadi error : Error while parsing the document --- \n");
     exit;
  }
  
  $xml_musrenbang=$dom->getElementsByTagName("xml");
  foreach($xml_musrenbang as $musrenbang){
      $xml_usulans=$musrenbang->getElementsByTagName("usulan");
      //fwrite($fh, "Kode SKPD :".$xml_usulan->getAttribute("unit_id")."Nama SKPD : ".$xml_usulan->getAttribute("unit_name")."\n");
      foreach($xml_usulans as $usulan){
            $con = Propel::getConnection();
            try {
                    $con->begin();
                    /*
                     * content yang akan disimpan ditaruh sini
                     */
                    $xxx->save($con);
                    $con->commit();
                    fwrite($fh, "Sukses disimpan unit_id : ".$usulan->getAttribute("unit_id")."Nama SKPD : ".$usulan->getAttribute("unit_name")."\n");
            } catch (PropelException $e) {
                    fwrite($fh, "Terjadi Error!! unit_id : ".$usulan->getAttribute("unit_id")."Nama SKPD : ".$usulan->getAttribute("unit_name")."\n");
                    echo "################ Terjadi Error : {$e}  ################ \n";
                    $con->rollback();
                    throw $e;
            }
      }
      
  }
  
  
fwrite($fh, "########## END ########## \n");
fwrite($fh, "################################################################# \n");
$tgl_selesai = "$d-$m-$y $H:$mn:$s";
fwrite($fh, "data musrenbang telah berhasil ditarik pada {$tgl_selesai} \n");
fclose($fh) or die("Can't close $file: $php_errormsg");

ini_restore('memory_limit');
?>
